function get_reference(user_type_id, reference_id){
	$('#reference_id').html('<option selected="selected" value="">Loading...</option>');
	var url = site_path + 'reference/' + user_type_id;

	$.getJSON(url+'?callback=?',function(data){
		if(data == null){
			$("#reference_id").attr('required', false);
			$(".reference-area").hide();
		}else{
			$("#reference_id").attr('required', true);

			$('#reference_id').html('<option selected="selected" value="">Select Reference</option>');
			$.each(data, function(key, item) {

				if(reference_id == item.id){
					var selected = 'selected="selected"';
				}else{
					var selected = '';
				}
			    $('#reference_id').append('<option value="'+item.id+'" '+selected+'>'+item.title+'</option>');
			});
			$(".reference-area").show();
		}
	});
}