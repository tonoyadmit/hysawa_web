<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Query Form </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body form">
        <form action="{{route('water.index.post')}}" class="form-horizontal" method="POST" id="searchForm">

            {{csrf_field()}}

            <input type="hidden" name="query" value="{{time()}}" />

            <div class="form-body">

                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Starting Date</label>
                            <div class="col-md-9">

                                <input class="form-control" type="date" name="starting_date"
                                @if(!empty( $old['starting_date']))
                                    value="{{$old['starting_date']}}"
                                @endif >

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Ending Date</label>
                            <div class="col-md-9">

                                <input class="form-control" type="date" name="ending_date"
                                @if(!empty( $old['ending_date']))
                                    value="{{$old['ending_date']}}"
                                @endif >

                            </div>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Created By</label>
                            <div class="col-md-9">

                                <input
                                    type="text"
                                    class="form-control"
                                    name="created_by"
                                    placeholder="Created By: user Id"

                                    @if(!empty( $old['created_by']))
                                        value="{{$old['created_by']}}"
                                    @endif
                                />


                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Word</label>
                            <div class="col-md-9">

                                <input
                                    type="text"
                                    class="form-control"
                                    name="ward_no"
                                    placeholder="Type Ward No"

                                    @if(!empty( $old['ward_no']))
                                        value="{{$old['ward_no']}}"
                                    @endif
                                />

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Village</label>
                            <div class="col-md-9">

                            <input
                                type="text"
                                class="form-control"
                                name="village"
                                placeholder="Type Village Name"

                                @if(!empty( $old['village']))
                                    value="{{$old['village']}}"
                                @endif
                            />

                            </div>
                        </div>
                    </div>


                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="submit" class="btn green" value="Search" />
                                <a href="{{route('water.index')}}" class="btn default">Clear</a>
                            </div>
                        </div>
                    </div>
                </div>


                @if(!empty( $old['query']) )

                    <div class="row">
                        <div class="col-md-12">
                            <a href="{{route('water.download', $old)}}" class="btn btn-info">Download CSV</a>
                        </div>
                    </div>

                @endif


            </div>
        </form>

    </div>
</div>