@extends('layouts.appinside')
@section('content')
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('superadmin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>List of Training:</span>
    </li>
  </ul>
</div>

<div class="col-md-12" style="margin-top: 10px;">
      @include('core.superadmin.training.training._search')
</div>

@include('core.superadmin.training.training._table')
@endsection
