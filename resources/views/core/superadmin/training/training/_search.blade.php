<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>List of Training: <small> view</small> </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body form">
        <form action="{{route('superadmin.training.training.index')}}" class="form-horizontal" method="GET" id="searchForm">

            <input type="hidden" name="query" value="{{time()}}" />

            <div class="form-body">
                <div class="row">

                    <div class="col-md-2">
                        <label class="control-label">Agency</label>
                        <?php $agencies = \DB::table('trg_agency')->get(); ?>

                        <select class="form-control input-sm" name="Agency">
                            <option value="">Choose a Agency</option>
                            @foreach($agencies as $agency )
                                <option value="{{$agency->agency_name}}"

                                @if( isset($_REQUEST['Agency']) && $_REQUEST['Agency'] == $agency->agency_name)
                                    selected="selected"
                                @endif

                                >{{$agency->agency_name}}</option>
                            @endforeach
                        </select>

                    </div>
                    <div class="col-md-2">
                        <label class="control-label">Status</label>
                        <?php
                            $TrgStatus = \DB::table('tbl_training')->groupBy('TrgStatus')->get();
                        ?>
                        <select class="form-control input-sm" name="TrgStatus">
                            <option value="">Choose a Status</option>
                            @foreach($TrgStatus as $ts )
                                <option value="{{$ts->TrgStatus}}"
                                @if(!empty($old) && !empty($old['TrgStatus']) && $old['TrgStatus'] == $ts->TrgStatus)
                                    selected="selected"
                                @endif
                                >{{$ts->TrgStatus}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">From</label>
                        <input
                            class="form-control date-picker input-sm"
                            type="text"
                            name="TrgFrom"
                            data-date-format = "yyyy-mm-dd"
                        @if(!empty( $old['TrgFrom']))
                            value="{{$old['TrgFrom']}}"
                        @endif />
                        <p>YYYY-MM-DD</p>

                    </div>
                    <div class="col-md-2">
                        <label class="control-label">To</label>
                        <input
                            class="form-control date-picker input-sm"
                            type="text"
                            name="TrgTo"
                            data-date-format = "yyyy-mm-dd"
                        @if(!empty( $old['TrgTo']))
                            value="{{$old['TrgTo']}}"
                        @endif />
                        <p>YYYY-MM-DD</p>
                    </div>

                    <div class="col-md-2">
                        <label class="control-label">Region</label>
                        <?php
                            $regions = App\Model\Region::all();
                        ?>
                        <select class="form-control input-sm" name="region_id">
                            <option value="">Choose a Region</option>
                            @foreach($regions as $region )
                                <option value="{{$region->id}}"
                                @if(!empty($old) && !empty($old['region_id']) && $old['region_id'] == $region->id)
                                    selected="selected"
                                @endif
                                >{{$region->region_name}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-2">
                        <label class="control-label col-md-3">Title</label>
                        <input type="text" name="TrgTitle" class="form-control"
                        @if(!empty($old) && $old['TrgTitle'] != "")
                            value="{{$old['TrgTitle']}}"
                        @endif />
                    </div>

                </div>

                <div class="row">


                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="submit" class="btn green" value="Search" />
                                <a href="{{route('superadmin.training.training.index')}}" class="btn default">Clear</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </form>

    </div>
</div>