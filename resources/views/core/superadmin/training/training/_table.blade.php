@if(count($traininglists) > 0)
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">


    <div class="portlet-title">
        <div class="caption">
            {{-- <i class="fa fa-gift"></i> --}}&nbsp;
        </div>

         <div class="actions">
            <a href="{{route('superadmin.training.training.index', ['download' => 'download'])}}" target="_blank" class="btn btn-circle success"> <i class="fa fa-download"></i> Download</a>
          </div>

        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body util-btn-margin-bottom-5">
      <div class="table-responsive">
        <table class="table table-bordered table-hover" id="example0">
          <thead class="flip-content">
            <th style="font-size: 10px;">Action</th>
            <th style="font-size: 10px;">Training Agency / Organized by</th>
            <th style="font-size: 10px;">Training Title</th>
            <th style="font-size: 10px;">Venue</th>
            <th style="font-size: 10px;">Date From</th>
            <th style="font-size: 10px;">Date To</th>
            <th style="font-size: 10px;">Participant type</th>
            <th style="font-size: 10px;">Number of participant</th>
            <th style="font-size: 10px;">Batch no</th>
            <th style="font-size: 10px;">Status</th>
          </thead>
          <tbody>
            @foreach($traininglists as $traininglist)
            <tr>
              <td style="font-size: 10px;">
                <a class="label label-success" href="{{route('superadmin.training.training.edit',$traininglist->TrgCode )}}">
                  <i class="fa fa-pencil"></i>
                </a>
              </td>
              <td style="font-size: 10px;">{{$traininglist->Agency}}</td>
              <td style="font-size: 10px;">
                <a href="{{
                route('superadmin.training.training-participant.create').'?TrgCode='.$traininglist->TrgCode }}">{{$traininglist->TrgTitle}}</a>
              </td>

              <td style="font-size: 10px;">{{$traininglist->TrgVenue}}</td>
              <td style="font-size: 10px;">{{$traininglist->TrgFrom}}</td>
              <td style="font-size: 10px;">{{$traininglist->TrgTo}}</td>
              <td style="font-size: 10px;">{{$traininglist->TrgParticipantsType}}</td>
              <td style="font-size: 10px;">{{$traininglist->TrgParticipantNo}}</td>
              <td style="font-size: 10px;">{{$traininglist->TrgBatchNo}}</td>
              <td style="font-size: 10px;">{{$traininglist->TrgStatus}}</td>

            </tr>
            @endforeach
          </tbody>


        </table>
        <div class="pagination pull-right">
               {!! $traininglists->appends($old)->render() !!}
            </div>

      </div>
    </div>
  </div>
</div>
@else
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <p>
      No Data Found
    </p>
  </div>
</div>
@endif