@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('superadmin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Paricipants</span>
    </li>
  </ul>
</div>

@if(count($participantslists) > 0)

<div class="col-md-12" style="margin-top: 10px;">
  <div class="portlet light tasks-widget bordered">

    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Training Participant
        </div>

         <div class="actions">
            <a href="{{route('superadmin.training.training-participant.index', ['download' => 'download'])}}" target="_blank" class="btn btn-circle success"> <i class="fa fa-download"></i> Download</a>
          </div>

        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body util-btn-margin-bottom-5">
      <div class="table-responsive">

        <table class="table table-bordered table-hover" id="example0">
          <thead class="flip-content">

            <th style="font-size: 10px;">Organized by/Agency</th>
            <th style="font-size: 10px;">Training Title</th>
            <th style="font-size: 10px;">Venue</th>
            <th style="font-size: 10px;">Partcipant name</th>
            <th style="font-size: 10px;">Designation</th>
            <th style="font-size: 10px;">District</th>
            <th style="font-size: 10px;">Upazila</th>
            <th style="font-size: 10px;">Union</th>
            <th style="font-size: 10px;">Mobile no</th>
            <th style="font-size: 10px;">Email</th>

          </thead>
          <tbody>
            @foreach($participantslists as $participantslist)
            <tr>
              <td style="font-size: 10px;">{{ $participantslist->Agency }}</td>
              <td style="font-size: 10px;">{{ $participantslist->TrgTitle }}</td>
              <td style="font-size: 10px;">{{ $participantslist->TrgVenue }}</td>
              <td style="font-size: 10px;">{{ $participantslist->partcipant_name }}</td>
              <td style="font-size: 10px;">{{ $participantslist->designation }}</td>
              <td style="font-size: 10px;">{{ $participantslist->distname }}</td>
              <td style="font-size: 10px;">{{ $participantslist->upname }}</td>
              <td style="font-size: 10px;">{{ $participantslist->unname }}</td>
              <td style="font-size: 10px;">{{ $participantslist->mobile_no }}</td>
              <td style="font-size: 10px;">{{ $participantslist->email_address }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>

      </div>
    </div>
  </div>
</div>

@else
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <p>
      No Data Found
    </p>
  </div>
</div>
@endif

@endsection
