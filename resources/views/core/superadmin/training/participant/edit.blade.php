@extends('layouts.appinside')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('superadmin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('training-agency.trainingParticipant.index') }}">Traning Participants</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Update</span>
        </li>
    </ul>
</div>

<h1 class="page-title"> Traning Participants <small>Update</small></h1>

{!! Form::model($participants, array('route' => ['training-agency.trainingParticipant.update', $participants->participant_id], 'method' => 'put')) !!}

<div class="row">

    @include('partials.errors')

    <div class="col-md-6">

        <div class="form-group">
            <label class="control-label col-md-12 np-lr">Name of participant:</label>
            <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />

            {!! Form::text('partcipant_name', null, ['class' => 'form-control', 'placeholder' => 'Name of participant', 'required' => 'required']) !!}
        </div>

        <div class="form-group">
            <label class="control-label col-md-12 np-lr">District{{ $participants->district_id }}</label>
            {!! Form::select('district_id', array(''=>'Select District')+$district,$participants->district_id, ['class' => 'form-control','id' => 'district_id']) !!}
        </div>
        <div class="form-group">
            <label class="control-label col-md-12 np-lr">Upzilla</label>
            {!! Form::select('upazila_id', array(''=>'Select Upzilla'), $participants->upazila_id, ['class' => 'form-control','id'=>'upazila_id']) !!}
        </div>
        <div class="form-group">
            <label class="control-label col-md-12 np-lr">Union</label>
            {!! Form::select('union_id', array(''=>'Select Union'), $participants->union_id, ['class' => 'form-control','id' =>'union_id']) !!}
        </div>
        <div class="form-group">
            <label class="control-label col-md-12 np-lr">Designation</label>
            {!! Form::select('designation', array(''=>'Select Ward')+$des, null, ['class' => 'form-control',]) !!}
        </div>

        <div class="form-group">
            <label class="control-label">Mobile no</label>
            {!! Form::text('mobile_no', null, ['class' => 'form-control', 'placeholder' => 'Mobile no', ]) !!}
        </div>

        <div class="form-group">
            <label class="control-label">Email:</label>
            {!! Form::text('email_address', null, ['class' => 'form-control', 'placeholder' => 'Email', ]) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Remarks</label>
            {!! Form::text('remarks', null, ['class' => 'form-control', 'placeholder' => 'Remarks', ]) !!}
        </div>
    </div>
</div>
&nbsp;
<div class="row padding-top-10">
    <a href="javascript:history.back()" class="btn default"> Cancel </a>
    {!! Form::submit('Update', ['class' => 'btn green pull-right']) !!}
</div>
{!! Form::close() !!}
</div>

<script type="text/javascript">
    $(document).ready(function () {
        highlight_nav('warehouse-manage', 'shelfs');
    });
</script>

<script type="text/javascript" language="javascript" >
    $(document).ready(function () {
        $('#district_id').on('change', function() {
            var disid=$(this).val();
            var csrftoken = $("#csrf-token").val();
            if(disid==''){
                $('#upazila_id').attr('disabled', 'disabled');
            }else{
                $.getJSON('{{ route('superadmin.training.getUpzilla') }}?disid='+disid+'&_token='+csrftoken, function (data) {
                    $('select[name="upazila_id"]').empty();
                    $.each(data, function(key, value) {
                        $('select[name="upazila_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                    });
                });
            }
        });

        $('#upazila_id').on('change', function() {
            var upid=$(this).val();
            var disid=$("#district_id").val();
            var csrftoken = $("#csrf-token").val();
            if(upid==''){
                $('#union_id').attr('disabled', 'disabled');
            }else{
                $.getJSON('{{ route('superadmin.training.getUnion') }}?disid='+disid+'&_token='+csrftoken+'&upid='+upid, function (data) {
                    $('select[name="union_id"]').empty();
                    $.each(data, function(key, value) {
                        $('select[name="union_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                    });
                });
            }
        });
    });
</script>

@endsection
