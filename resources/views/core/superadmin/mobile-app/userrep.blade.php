@extends('layouts.appinside')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('district-admin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Monthly Report</span>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Report List</span>
        </li>
    </ul>
</div>


 <div class="col-md-12" style="margin-top: 10px">
      @include('core.superadmin.mobile-app._searchrep')
  </div>

  
<div class="portlet light bordered" style="margin-top: 10px;">

    <div class="portlet-title">
        <div class="caption">
            <i class="icon-share font-dark"></i>
            <span class="caption-subject font-dark bold uppercase">Submitted User List From Starting Date:{{ Request()->starting_date}} To Ending Date:{{ Request()->ending_date}}  </span>
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
    </div>

    <div class="portlet-body">

        <p>
            {{--  <a href="{{route('up-admin.fund_list.create')}}" class="btn btn-large btn-success"> Add new Entry</a>
            --}}
           {{--  <a href="{{route('up-admin.finace_demand.print')}}" class="btn btn-large btn-success"> Print</a>  --}}

        </p>

        @if(!empty($Submittedrep) > 0)

        <table class="" id="example0"  border="1">

            <thead class="flip-content">
  
                <th>SL</th>
                <th>Project</th>
                <th>District</th>
                <th>Upazila</th>
                <th>Union</th>
                <th>UserId</th>
                 <th>Submission in query date</th>
       
            </thead>

            <tbody>
        
                @foreach($Submittedrep as $Submitted)
                <tr>
             
                        {{--  <a class="label label-success" href="{{route('superadmin.finance.up-demand.fund_list.edit', $FinanceData->id )}}">
                            <i class="fa fa-pencil"></i> Update
                        </a> 
  
                           <a class="label label-success" href="{{ route('superadmin.finance.up-demand.finace_demand.index',  ['fid' => $FinanceData->id,'fdate='.$FinanceData->fdate,'tdate='.$FinanceData->tdate] )}}">
                            <i class="fa fa-pencil"></i> Demand List
                        </a>
                        --}}

                     <?php
                      $project = App\Model\Project::find($Submitted->proj_id);
                      ?>
                     <?php   
                        $districts = App\Model\District::where('id', $Submitted->distid )->get();
                     ?>
                     <?php                        
                       $upazilas = App\Model\Upazila::where('id',  $Submitted->upid)->get();
                      ?>
                       <?php                        
                        $unions = App\Model\Union::where('id',  $Submitted->unid)->get();
                     ?>


                    <td nowrap="nowrap">{{ $loop->iteration }}</td>
                    <td nowrap="nowrap">{{$project->project}}</td>
                    @foreach($districts as $district )    
                    <td nowrap="nowrap">{{$district->distname}}</td>
                    @endforeach
                    @foreach($upazilas as $upazila )       
                    <td nowrap="nowrap">{{$upazila->upname}}</td>
                    @endforeach
                    @foreach($unions as $union )    
                    <td nowrap="nowrap">{{$union->unname}}</td>
                    @endforeach
                    <td nowrap="nowrap">{{ $Submitted->email }}</td>
                     <td nowrap="nowrap" align="center">{{ $Submitted->Total }}</td>
         
                   

                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <p>No Data Found</p>
        @endif

    </div>
</div>

<div class="portlet light bordered" style="margin-top: 10px;">

<div class="portlet-title">
    <div class="caption">
        <i class="icon-share font-dark"></i>
        <span class="caption-subject font-dark bold uppercase">Total Submitted</span>
    </div>
    <div class="actions">
        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
    </div>
</div>

<div class="portlet-body">

    <p>
        {{--  <a href="{{route('up-admin.fund_list.create')}}" class="btn btn-large btn-success"> Add new Entry</a>
        --}}
       {{--  <a href="{{route('up-admin.finace_demand.print')}}" class="btn btn-large btn-success"> Print</a>  --}}

    </p>

    @if(!empty($NotSubmittedMonth) > 0)

    <table class="" id="example0"  border="1">

        <thead class="flip-content">

            <th>SL</th>
            <th>Project</th>
            <th>District</th>
            <th>Upazila</th>
            <th>Union</th>
            <th>UserId</th>
             <th>Total Submitted</th>
   
        </thead>

        <tbody>
    
            @foreach($NotSubmittedMonth as $NotSubmitted)
            <tr>
         
                    {{--  <a class="label label-success" href="{{route('superadmin.finance.up-demand.fund_list.edit', $FinanceData->id )}}">
                        <i class="fa fa-pencil"></i> Update
                    </a> 

                       <a class="label label-success" href="{{ route('superadmin.finance.up-demand.finace_demand.index',  ['fid' => $FinanceData->id,'fdate='.$FinanceData->fdate,'tdate='.$FinanceData->tdate] )}}">
                        <i class="fa fa-pencil"></i> Demand List
                    </a>
                    --}}

                 <?php
                  $project = App\Model\Project::find($NotSubmitted->proj_id);
                  ?>
                 <?php   
                    $districts = App\Model\District::where('id', $NotSubmitted->distid )->get();
                 ?>
                 <?php                        
                   $upazilas = App\Model\Upazila::where('id',  $NotSubmitted->upid)->get();
                  ?>
                   <?php                        
                    $unions = App\Model\Union::where('id',  $NotSubmitted->unid)->get();
                 ?>


                <td nowrap="nowrap">{{ $loop->iteration }}</td>
                <td nowrap="nowrap">{{$project->project}}</td>
                @foreach($districts as $district )    
                <td nowrap="nowrap">{{$district->distname}}</td>
                @endforeach
                @foreach($upazilas as $upazila )       
                <td nowrap="nowrap">{{$upazila->upname}}</td>
                @endforeach
                @foreach($unions as $union )    
                <td nowrap="nowrap">{{$union->unname}}</td>
                @endforeach
                <td nowrap="nowrap">{{ $NotSubmitted->email }}</td>
                 <td nowrap="nowrap">{{ $NotSubmitted->Total }}</td>
     
               

            </tr>
            @endforeach
        </tbody>
    </table>
    @else
    <p>No Data Found</p>
    @endif

</div>
</div>


<div class="portlet light bordered" style="margin-top: 10px;">

<div class="portlet-title">
    <div class="caption">
        <i class="icon-share font-dark"></i>
        <span class="caption-subject font-dark bold uppercase">User not submitted any report </span>
    </div>
    <div class="actions">
        <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
    </div>
</div>

<div class="portlet-body">

    <p>
        {{--  <a href="{{route('up-admin.fund_list.create')}}" class="btn btn-large btn-success"> Add new Entry</a>
        --}}
       {{--  <a href="{{route('up-admin.finace_demand.print')}}" class="btn btn-large btn-success"> Print</a>  --}}

    </p>

    @if(!empty($Unsubmitted) > 0)

    <table class="" id="example0" border="1">

        <thead class="flip-content">

                <th>SL</th>
                <th>Project</th>
                <th>District</th>
                <th>Upazila</th>
                <th>Union</th>
                <th>UserId</th>
                <th>Total</th>
       
   
        </thead>

        <tbody>
    
            @foreach($Unsubmitted as $UnsubmittedData)
            <tr>
         
                    {{--  <a class="label label-success" href="{{route('superadmin.finance.up-demand.fund_list.edit', $FinanceData->id )}}">
                        <i class="fa fa-pencil"></i> Update
                    </a> 

                       <a class="label label-success" href="{{ route('superadmin.finance.up-demand.finace_demand.index',  ['fid' => $FinanceData->id,'fdate='.$FinanceData->fdate,'tdate='.$FinanceData->tdate] )}}">
                        <i class="fa fa-pencil"></i> Demand List
                    </a>
                    --}}

            
                    <?php
                      $project = App\Model\Project::find($UnsubmittedData->proj_id);
                      ?>
                     <?php   
                        $districts = App\Model\District::where('id', $UnsubmittedData->distid )->get();
                     ?>
                     <?php                        
                       $upazilas = App\Model\Upazila::where('id',  $UnsubmittedData->upid)->get();
                      ?>
                       <?php                        
                        $unions = App\Model\Union::where('id',  $UnsubmittedData->unid)->get();
                     ?>


                    <td nowrap="nowrap">{{ $loop->iteration }}</td>
                    <td nowrap="nowrap">{{$project->project}}</td>
                    @foreach($districts as $district )    
                    <td nowrap="nowrap">{{$district->distname}}</td>
                    @endforeach
                    @foreach($upazilas as $upazila )       
                    <td nowrap="nowrap">{{$upazila->upname}}</td>
                    @endforeach
                    @foreach($unions as $union )    
                    <td nowrap="nowrap">{{$union->unname}}</td>
                    @endforeach

                 <td nowrap="nowrap">{{ $UnsubmittedData->email }}</td>
                 <td nowrap="nowrap">0</td>
               

            </tr>
            @endforeach
        </tbody>
    </table>
    @else
    <p>No Data Found</p>
    @endif

</div>
</div>




@endsection



@section('my_js')
  <script type="text/javascript">
    $(document).ready(function () {

      $('#district_id').on('change', function() {
        var head_id = $(this).val();

        $("#upazila_id").html("Select District");
        $("#union_id").html("Select Upazila");

        $.ajax({
          url: '{{route('superadmin.ajax.upazila')}}?district_id='+head_id+'&not_all=true&choose_one=true',
          type: 'GET',
          success: function(response){
            if(response['status'] == true){
              $("#upazila_id").html(response['upazila_list']);
            }
          }
        });
      });

      $('#upazila_id').on('change', function() {
        var upazila_id = $(this).val();

        $("#union_id").html("Select Upazila");

        $.ajax({
          url: '{{route('superadmin.ajax.union')}}?upazila_id='+upazila_id+'&not_all=true&choose_one=true',
          type: 'GET',
          success: function(response){
            if(response['status'] == true){
              $("#union_id").html(response['union_list']);
            }
          }
        });
      });
    });
  </script>

  @endsection

  