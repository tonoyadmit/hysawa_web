<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Mobile-App : <small> Data-List</small> </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body form">
        <form action="{{route('superadmin.mobile-app.index')}}" class="form-horizontal" method="GET" id="searchForm">



            <input type="hidden" name="query" value="{{time()}}" />

            <div class="form-body">

                <div class="row">

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Starting Date</label>
                            <div class="col-md-9">

                                <input
                                    class="form-control input-sm date-picker"
                                    type="text"
                                    name="starting_date"
                                    data-date-format="yyyy-mm-dd"

                                @if(!empty( $old['starting_date']))
                                    value="{{date('Y-m-d', strtotime($old['starting_date']))}}"
                                @endif >

                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Ending Date</label>
                            <div class="col-md-9">

                                <input
                                    class="form-control input-sm date-picker"
                                    type="text"
                                    name="ending_date"
                                    data-date-format="yyyy-mm-dd"

                                @if(!empty( $old['ending_date']))
                                    value="{{date('Y-m-d', strtotime($old['ending_date']))}}"
                                @endif >

                            </div>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Type</label>
                            <div class="col-md-9">

                                <select name="type" class="form-control input-sm" >
                                    <?php
                                        $data = App\Model\MobAppDataList::groupBy('type')->get();
                                    ?>
                                    <option value="">Select Type</option>

                                    @foreach($data as $type)
                                    <option
                                    value="{{$type->type}}"
                                    @if(
                                        !empty($old['type']) &&
                                        $old['type'] != "" &&
                                        $type->type == $old['type']
                                    )
                                        selected="selected"
                                    @endif
                                    >{{$type->type}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">UserId</label>
                            <div class="col-md-9">

                                <input type="text" name="user_id"

                                    @if(!empty( $old['user_id']))
                                        value="{{$old['user_id']}}"
                                    @endif

                                    class="form-control" />


                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <label class="control-label"></label>

                    </div>



                </div>
                
             <!--     <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Project</label>
                            <div class="col-md-9">
                                <select name="project_id" class="form-control input-sm" id="project">
                                    <?php
                                        $projects = App\Model\Project::orderBy('project', 'DESC')->get();
                                    ?>
                                    <option value="">Select Project</option>
                                    @foreach($projects as $project)
                                    <option
                                    value="{{$project->id}}"
                                    @if(
                                        !empty($old['project_id']) &&
                                        $old['project_id'] != "" &&
                                        $project->id == $old['project_id']
                                    )
                                        selected="selected"
                                    @endif
                                    >{{$project->project}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Region</label>
                            <div class="col-md-9">
                                <select name="region_id" class="form-control input-sm" id="region">
                                    <option value="">Select Project</option>

                                    @if(!empty($old['project_id']))
                                    <?php

                                        $regions = \DB::table('tbl_water')
                                            ->where('tbl_water.region_id', '!=', "")
                                            ->where('tbl_water.region_id', '!=', 0)
                                            ->where('tbl_water.proj_id', $old['project_id'])
                                            ->select('region.region_id', 'region.region_name')
                                            ->leftjoin('region','region.region_id','=','tbl_water.region_id')
                                            ->groupBy('tbl_water.region_id')
                                            ->get();
                                        ?>

                                        @foreach($regions as $region)
                                        <option

                                            value="{{$region->region_id}}"

                                            @if(!empty($old['region_id']) &&  $region->region_id == $old['region_id'])
                                                selected="selected"
                                            @endif
                                        >{{$region->region_name}}</option>
                                        @endforeach

                                    @endif
                                </select>
                            </div>
                        </div>
                    </div> -->
                    
                      <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Project</label>
                            <div class="col-md-9">

                                <?php

                                    $projects = App\Model\Project::all();

                                ?>
                                <select class="form-control" name="project_id">
                                    <option value="">Choose a Project</option>
                                    @foreach($projects as $project )
                                        <option value="{{$project->id}}"

                                        @if(isset($old['project_id']) && $old['project_id'] == $project->id)
                                            selected="selected"
                                        @endif


                                        >{{$project->project}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

               <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Region</label>
                            <div class="col-md-9">
                                <?php

                                    $regions = App\Model\Region::all();

                                ?>
                                <select class="form-control" name="region_id" >
                                    <option value="">Choose a Region</option>
                                    @foreach($regions as $region )
                                        <option value="{{$region->region_id}}"

                                        @if(isset($old['region_id']) && $old['region_id'] == $region->region_id)
                                            selected="selected"
                                        @endif


                                         >{{$region->region_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>


                   <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">District</label>
                            <div class="col-md-9">
                                <select name="district_id" class="form-control input-sm" id="district">
                                    <option value="">Select Region</option>
                                  
                                        <?php
                                        $districts = \DB::table('fdistrict')->get();
                                        ?>
                                        @foreach($districts as $district)
                                        <option
                                            value="{{$district->id}}"
                                            @if($district->id == $old['district_id'])
                                            selected="selected"
                                            @endif
                                        >{{$district->distname}}</option>
                                        @endforeach
                             
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Upazila</label>
                            <div class="col-md-9">
                                <select name="upazila_id" class="form-control input-sm" id="upazila">
                                    <option value="">Select District</option>

                                    @if(!empty($old['district_id']))
                                        <?php
                                        $upazilas = \DB::table('fupazila')->where('disid',$old['district_id'])->get();
                                        ?>
                                        @foreach($upazilas as $upazila)
                                        <option
                                        value="{{$upazila->id}}"
                                        @if(!empty($old['upazila_id']) && $upazila->id == $old['upazila_id'])
                                        selected="selected"
                                        @endif
                                        >{{$upazila->upname}}</option>
                                        @endforeach
                                    @endif

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Union</label>
                            <div class="col-md-9">
                                <select name="union_id" class="form-control input-sm" id="union">
                                    @if(!empty($old['upazila_id']))

                                    <?php
                                        $unions = \DB::table('funion')->where('upid', $old['upazila_id'])->get();
                                    ?>
                                    @foreach($unions as $union)
                                    <option
                                    value="{{$union->id}}"
                                    @if(!empty($old['union_id']) && $union->id == $old['union_id'])
                                    selected="selected"
                                    @endif
                                    >{{$union->unname}}</option>
                                    @endforeach

                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>



                    <div class="col-md-3">
                        <input type="submit" class="btn green" value="Search" />
                        <a href="{{route('superadmin.mobile-app.index')}}" class="btn default">Clear</a>
                        <a href="{{route('superadmin.mobile-app.userrep')}}" class="btn default">Report</a>
                        <?php
                            $old2 = $old;
                            $old2['print'] = 'print';
                        ?>
                        <a href="{{route('superadmin.mobile-app.index', $old2)}}" class="btn btn-info btn-sm pull-right">Print</a>
                    </div>

                </div>


            </div>
        </form>

    </div>
</div>