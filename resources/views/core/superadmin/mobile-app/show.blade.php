@extends('layouts.appinside')
@section('content')
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Mobile App Data</span>
      </li>
    </ul>
  </div>
  <h1 class="page-title"> Mobile-App : <small>Details</small> </h1>

  <div class="col-md-12">
    <div class="portlet light tasks-widget bordered">

      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i>Details
          </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body util-btn-margin-bottom-5">
        <div class="table-responsive">
          <h3>Basic Info</h3>

          <table class="table table-bordered table-hover data-table" id="example0">
            <tr><th>User</th><td>{{$data->user->email}}</td></tr>
            <tr><th>District</th><td>{{$data->union->upazila->district->distname}}</td></tr>
            <tr><th>Upazila</th><td>{{$data->union->upazila->upname}}</td></tr>
            <tr><th>Union</th><td>{{$data->union->unname}}</td></tr>
            <tr><th>Created At</th><td>{{$data->created_at}}</td></tr>

            <tr><th >Image 1</th><td>@if($data->image1 != "") <img class="img img-responsive" src="{{ asset($data->image1)}}" style="width: 100px;"/>@endif  </td></tr>
            <tr><th >Image 2</th><td>@if($data->image2 != "") <img class="img img-responsive" src="{{ asset($data->image2)}}" style="width: 100px;"/>@endif </td></tr>
            <tr><th >Image 3</th><td>@if($data->image3 != "") <img class="img img-responsive" src="{{ asset($data->image3)}}" style="width: 100px;"/>@endif </td></tr>
                
          </table>
  <tr><th width="15%">{{ $data->image1 }}</th></tr> <br>
                          <tr><th width="15%">{{ $data->image2 }}</th></tr> <br>
                          <tr><th width="15%">{{ $data->image3 }}</th></tr> <br>
          <br/>
          <h3>Question Answer</h3>

          <table class="table table-bordered table-hover data-table" id="example0">
            <thead class="flip-content">
              <th >Question with</th>
              <th >Answer</th>
            </thead>
            <tbody>
              <?php
                $lastItem = "";
              ?>
              @foreach($data->items as $item)

                @if($lastItem != "")

                  @if($lastItem->question->id == $item->question->id)
                    <tr>
                      <td>"</td>
                      <td>{{$item->value}}</td>
                    </tr>
                  @else
                  
                  <?php 
                    $lastItem = "";
                  ?>
                    <tr>
                      <td>Question: {{$item->question->title}} @if($item->question->input_type == "select_single" || $item->question->input_type == "select_multiple")  <br/> Option:  {{$item->question->options}} @endif</td>
                      <td>{{$item->value}}</td>
                    </tr>

                  @endif

                @else
                <tr>
                  <td>Question: {{$item->question->title}} @if($item->question->input_type == "select_single" || $item->question->input_type == "select_multiple")  <br/> Option:  {{$item->question->options}} @endif</td>
                  <td>{{$item->value}}</td>
                </tr>
                @endif
                <?php
                $lastItem = $item;
                ?>
              @endforeach
            </tbody>
          </table>


        </div>
      </div>
    </div>
  </div>

@endsection
