@extends('layouts.appinside')


@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Map </span>
      </li>
    </ul>
  </div>
  <h1 class="page-title"> Map Data: <small> infographic</small> </h1>
  <style>
      .gmaps{
        height: 450px;
      }
  </style>
  <div id="map_canvas" class="gmaps"></div>

@endsection


@section('my_js')
<script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDCARbJolbgMGzkfRgRZfpJfKFe_AO76Jo"></script>

<script>
$( window ).on( "load", function() {

    var customIcons = {
      Completed:
      {
        icon: 'http://labs.google.com/ridefinder/images/mm_20_blue.png',
        shadow: 'http://labs.google.com/ridefinder/images/mm_20_shadow.png'
      },
      Submitted:
      {
        icon: 'http://labs.google.com/ridefinder/images/mm_20_red.png',
        shadow: 'http://labs.google.com/ridefinder/images/mm_20_shadow.png'
      }
    };

    function initMap() {
        var map = new google.maps.Map(document.getElementById('map_canvas'), {
                      center: new google.maps.LatLng(23.3145, 90.1418),
                      zoom: 8
                    });
        var infoWindow = new google.maps.InfoWindow;

        $.get( '{{route('superadmin.ajax.map.water')}}', function( data ) {

            markerCollection = [];

            $.each(data, function(key, value){

                var Landowner = value["Landowner"];
                var TW_No     = value["TW_No"];
                var x         = value["x_coord"];
                var y         = value["y_coord"];
                var Village   = value["Village"];
                var upname    = value["upname"];
                var unname    = value["unname"];
                var Caretaker_male = value["Caretaker_male"];
                var HH_benefited = value["HH_benefited"];
                var wq_Arsenic = value["wq_Arsenic"];
                var wq_fe     = value["wq_fe"];
                var wq_cl     = value["wq_cl"];
                var depth     = value["depth"];
                var imp_status = value["imp_status"];
                var point     = new google.maps.LatLng( parseFloat(value["y_coord"]), parseFloat(value["x_coord"]) );

                var html      = "<b>" + "Landowner: " + Landowner + "</b> <br/>" + "Caretaker: " + Caretaker_male + "</b> <br/>" + "Village: " + Village + "</b> <br/>" +" Upazila: " + upname + "</b> <br/>" +" Union: " + unname + "</b> <br/>" +" Arsenic: " + wq_Arsenic + "</b> <br/>" +" Fe: " + wq_fe + "</b> <br/>" +" Cl: " + wq_cl + "</b> <br/>" +" Depth: " + depth + "</b> <br/>" +" TW no: " + TW_No + "</b> <br/>" + "   X:  " + x + "</b> <br/>" +"   Y:  " + y+" <br/>Status :"+imp_status;

               // var icon      = customIcons[imp_status] || {};

                var icon      = customIcons['Completed'] || {};

                var marker    = new google.maps.Marker({
                    map: map,
                    position: point,
                    icon: icon.icon,
                    shadow: icon.shadow
                });

                bindInfoWindow(marker, map, infoWindow, html);
                markerCollection.push(marker);
            });

           // var mc = new MarkerClusterer(map, markerCollection, {imagePath: 'https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/m'});

        });
    }

    function bindInfoWindow(marker, map, infoWindow, html)
    {
      google.maps.event.addListener(marker, 'click', function() {
        infoWindow.setContent(html);
        infoWindow.open(map, marker);
      });
    }

    initMap();
});

</script>
@endsection