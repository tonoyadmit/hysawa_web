@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ URL::to('home') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Water</span>
    </li>
  </ul>
</div>

<h1 class="page-title"> Water
  <small> view</small>
</h1>

<div class="col-md-12">
  @include('core.superadmin.sanitation._search')
</div>

@if(count($sanitations) > 0)

<div class="col-md-12">

  <div class="portlet light tasks-widget bordered">

    <div class="portlet-body util-btn-margin-bottom-5">
      <table class="table table-bordered table-hover" id="example0">
        <thead class="flip-content">
          <th>Action</th>
          <th>Ward No</th>
          <th>Village Name</th>
          <th>TW_No: (6 digit upcode + tw sl)</th>
          <th>Land Owner name</th>
          <th>Caretaker(male) name</th>
          <th>Caretaker (female) name</th>
          <th>Number of Household benefited</th>
          <th>Total Beneficiary_male</th>
          <th>Hardcore Beneficiary</th>

        </thead>
        <tbody>
          @foreach($sanitations as $sanitation)
          <tr>
            <td>
              <a class="label label-success" href="water/{{ $sanitation->id }}/edit">
                <i class="fa fa-pencil"></i> Update
              </a>
            </td>
            <td>{{ $sanitation->Ward_no }}</td>
            <td>{{ $sanitation->Village }}</td>
            <td>{{ $sanitation->TW_No }}</td>
            <td>{{ $sanitation->Landowner }}</td>
            <td>{{ $sanitation->Caretaker_male }}</td>
            <td>{{ $sanitation->Caretaker_female }}</td>
            <td>{{ $sanitation->HH_benefited }}</td>
            <td>{{ $sanitation->beneficiary_male }}</td>
            <td>{{ $sanitation->beneficiary_hardcore }}</td>

          </tr>
          @endforeach
        </tbody>
      </table>

      <div class="pagination pull-right">

      </div>

    </div>
  </div>
</div>
@else
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
   <p>
    No Data Found
  </p>
</div>
</div>
@endIf


<div class="modal fade bs-modal-lg" id="large" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

    </div>
  </div>
</div>


<script type="text/javascript">
  $(document ).ready(function() {
            /**
            * calback filter after modal show
            */
            $("#large").on("show.bs.modal", function(e) {
             setTimeout(function(){
              $('#modalTable').DataTable({
               "order": [],
             });
            }, 1000);
           });
            // End

            // Navigation Highlight
            highlight_nav('warehouse-manage', 'shelfs');

            $('#example0').DataTable({
              "order": [],
            });


            @php
            if( !empty($req) )
            {
              foreach($req as $key => $val)
              {
               echo "document.getElementById('".$key."').value = '".$val."';";
             }
           }
           @endphp
         });
       </script>

       <style media="screen">
        .table-filtter .btn{ width: 100%;}
        .table-filtter {
          margin: 20px 0;
        }
      </style>

      @endsection
