@extends('layouts.appinside')

@section('content')

    <!-- BEGIN PAGE BAR -->
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ URL::to('home') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ URL::to('water') }}">Waters</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Update</span>
            </li>
        </ul>
    </div>

    <h1 class="page-title"> Waters
        <small>create new</small>
    </h1>

            {!! Form::model($WaterSanitation, array('url' => '/water_sanitation/'.$WaterSanitation->id, 'method' => 'put')) !!}

                <div class="row">

                    @include('partials.errors')

                    <div class="col-md-6">

                       <div class="form-group">
                           <label class="control-label col-md-12 np-lr">CDF No:</label>
                           {!! Form::text('cdfno', null, ['class' => 'form-control', 'placeholder' => 'CDF No:', 'required' => 'required']) !!}

                       </div>

                       <div class="form-group">
                           <label class="control-label col-md-12 np-lr">Village Name</label>
                          {!! Form::text('Village', null, ['class' => 'form-control', 'placeholder' => 'Village Name', 'required' => 'required']) !!}
                       </div>

                        <div class="form-group">
                            <label class="control-label">Latrine no. (6 digit upcode+sl)</label>
                            {!! Form::number('latrineno', null, ['class' => 'form-control', 'placeholder' => 'TW_No', 'required' => 'required','min'=>"1",'max'=>"999999" ]) !!}
                        </div>

                        <div class="form-group">
                            <label class="control-label">Type of latrine:</label>
                            {!! Form::select('maintype', array(''=>'Type of latrine:')+$mainype, null, ['class' => 'form-control', 'required' => 'required']) !!}
                        </div>
                        <div class="form-group">
                           <label class="control-label">Type of Institution:</label>
                           {!! Form::select('subtype', array(''=>'Type of Institution')+$subtype, null, ['class' => 'form-control', 'required' => 'required']) !!}
                       </div>

                       <div class="form-group">
                           <label class="control-label">New/Renovation:</label>
                           {!! Form::select('cons_type', array('New'=>'New','Renovation'=>'Renovation'), null, ['class' => 'form-control', 'required' => 'required']) !!}
                       </div>

                       <div class="form-group">
                           <label class="control-label">Name of Institution/Place:</label>
                           {!! Form::number('name', null, ['class' => 'form-control', 'placeholder' => 'Number of Household benefited', 'required' => 'required']) !!}
                       </div>
                      <div class="form-group">
                           <label class="control-label">Chairman of the Mgt Committe:</label>
                           {!! Form::text('ch_comittee', null, ['class' => 'form-control', 'placeholder' => 'Chairman of the Mgt Committe', 'required' => 'required']) !!}
                       </div>
                        <div class="form-group">
                           <label class="control-label">Phone no:</label>
                           {!! Form::text('ch_com_tel', null, ['class' => 'form-control', 'placeholder' => 'Phone no', 'required' => 'required']) !!}
                       </div>
                        <div class="form-group">
                           <label class="control-label">Implementation Status</label>
                           {!! Form::select('imp_status', array(''=>'Select Implementation Status') + $ImplementStatus,null, ['class' => 'form-control', 'required' => 'required']) !!}
                       </div>

                        <div class="form-group">
                           <label class="control-label"> Caretaker name:</label>
                           {!! Form::text('caretakername', null, ['class' => 'form-control', 'placeholder' => 'Caretaker name', 'required' => 'required']) !!}
                       </div>
                        <div class="form-group">
                           <label class="control-label"> Caretaker phone:</label>
                           {!! Form::text('caretakerphone', null, ['class' => 'form-control', 'placeholder' => 'Caretaker phone', 'required' => 'required']) !!}
                       </div>
                        <div class="form-group">
                           <label class="control-label">Approval date (YYYY-MM-DD):</label>
                           {!! Form::text('app_date', null, ['class' => 'form-control', 'placeholder' => 'Approval date', 'required' => 'required']) !!}
                       </div>

                    </div>

                    <div class="col-md-6">


                        <div class="form-group">
                           <label class="control-label">No. of chamber for Male/Boys:</label>
                           {!! Form::text('malechamber', null, ['class' => 'form-control', 'placeholder' => 'No. of chamber for Male/Boys', 'required' => 'required']) !!}
                       </div>
                        <div class="form-group">
                           <label class="control-label">No. of chamber for Female/Girls:</label>
                           {!! Form::text('femalechamber', null, ['class' => 'form-control', 'placeholder' => 'No. of chamber for Female/Girls', 'required' => 'required']) !!}

                       </div>
                        <div class="form-group">
                           <label class="control-label">No. of Male beneficiry:</label>
                           {!! Form::text('male_ben', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'No. of Male beneficiry']) !!}
                       </div>
                        <div class="form-group">
                           <label class="control-label">No. of Female Beneficary:</label>
                           {!! Form::text('fem_ben', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'No. of Disable benficiary']) !!}

                       </div>
                        <div class="form-group">
                           <label class="control-label">No. of Child beneficiary:</label>
                           {!! Form::text('child_ben', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'No. of Female Beneficary']) !!}

                       </div>
                        <div class="form-group">
                           <label class="control-label"> No. of Disable benficiary:</label>

                           {!! Form::text('disb_bene', null, ['class' => 'form-control', 'placeholder' => 'No. of Disable benficiary', 'required' => 'required']) !!}
                       </div>
                        <div class="form-group">
                           <label class="control-label">Proposed/Installed facilities:</label>
                           {!! Form::number('watersource', null, ['class' => 'form-control', 'placeholder' => 'Proposed/Installed facilities', 'required' => 'required']) !!}
                       </div>
                        <div class="form-group">
                           <label class="control-label">Approval Status:</label>
                           {!! Form::select('app_status',array(''=>'Approval Status')+$appstatus, null , ['class' => 'form-control', 'placeholder' => 'Total Beneficiary_female', 'required' => 'required']) !!}
                       </div>

                        <div class="form-group">
                           <label class="control-label">Longitude:</label>
                           {!! Form::number('longitude', null, ['class' => 'form-control', 'placeholder' => 'Longitude', 'required' => 'required']) !!}
                       </div>
                        <div class="form-group">
                           <label class="control-label"> Latitude:</label>
                           {!! Form::number('latitude', null, ['class' => 'form-control', 'placeholder' => 'Latitude', 'required' => 'required']) !!}
                       </div>
                        <div class="form-group">
                           <label class="control-label">Overhead tank:</label>
                           {{ Form::radio('overheadtank', 'yes') }}Yes
                            {{ Form::radio('overheadtank', 'NO') }}NO
                       </div>
                        <div class="form-group">
                           <label class="control-label">Motor/pump:</label>
                           {{ Form::radio('motorpump', 'yes') }} Yes
                            {{ Form::radio('motorpump', 'NO') }} NO
                       </div>
                        <div class="form-group">
                           <label class="control-label">Sock well:</label>
                           {{ Form::radio('sockwell', 'yes') }}Yes
                            {{ Form::radio('sockwell', 'NO') }} NO
                       </div>
                        <div class="form-group">
                           <label class="control-label">Septic tank:</label>
                           {{ Form::radio('seotictank', 'yes') }} Yes
                            {{ Form::radio('seotictank', 'NO') }} NO
                       </div>
                        <div class="form-group">
                           <label class="control-label">Tap outside:</label>
                           {{ Form::radio('tapoutside', 'yes') }} Yes
                            {{ Form::radio('tapoutside', 'NO') }} NO
                       </div>

                    </div>

                </div>

                &nbsp;
                <div class="row padding-top-10">
                    <a href="javascript:history.back()" class="btn default"> Cancel </a>
                    {!! Form::submit('Update', ['class' => 'btn green pull-right']) !!}
                </div>

            {!! Form::close() !!}


    </div>

    <script type="text/javascript">

        $(document ).ready(function() {
            // Navigation Highlight
            highlight_nav('warehouse-manage', 'shelfs');

        });

    </script>

@endsection
