{{-- @extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('superadmin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="#">Sub-project</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="#"> </a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Approval and Implementation Status</span>
    </li>
  </ul>
</div>

@include('partials.errors')

<h1 class="page-title"> Water <small> Approval and Implementation Status</small> </h1>

<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <div class="portlet-body util-btn-margin-bottom-5">


      <table>
        <tr>
          <td><a class="btn btn-large btn-info" href="{{route('superadmin.sub-projects.waters.regions')}}">Region Summary</a></td>
          <td><a class="btn btn-large btn-success" href="{{route('superadmin.sub-projects.waters.download', 'region')}}">Region Download</a></td>
        </tr>
        <tr>
          <td><a class="btn btn-large btn-info" href="{{route('superadmin.sub-projects.waters.districts')}}">District Summary</a></td>
          <td><a class="btn btn-large btn-success" href="{{route('superadmin.sub-projects.waters.download', 'district')}}">District Download</a></td>
        </tr>
        <tr>
          <td><a class="btn btn-large btn-info" href="{{route('superadmin.sub-projects.waters.unions')}}">Union Summary</a></td>
          <td><a class="btn btn-large btn-success" href="{{route('superadmin.sub-projects.waters.download', 'union')}}">Union Download</a></td>
        </tr>
        <tr>
          <td><a class="btn btn-large btn-info" href="{{route('superadmin.sub-projects.waters.approval')}}">Approval Summary</a></td>
          <td><a class="btn btn-large btn-success" href="{{route('superadmin.sub-projects.waters.download', 'approval')}}">Approval Download</a></td>
        </tr>
      </table>

    </div>
  </div>
</div>
@endsection

 --}}





@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{route('superadmin.dashboard')}}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Sub-project</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Water </a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Reports/Summary</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

  @include('core.superadmin.sub-projects.water._search')

  @if(!empty($datas))
    @include('core.superadmin.sub-projects.water._table')
  @endif
@endsection


