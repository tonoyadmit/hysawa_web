@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li><a href="{{route('superadmin.dashboard')}}">Home</a><i class="fa fa-circle"></i></li>
      <li><a href="#">Sub-project</a><i class="fa fa-circle"></i></li>
      <li><a href="#">Water </a><i class="fa fa-circle"></i></li>
      <li><span>Approval Summary</span></li>
    </ul>
  </div>

  @include('partials.errors')

  @include('core.superadmin.sub-projects.water.approval._search')

  @if(!empty($datas))

  <p>&nbsp;</p>
  <h1 class="page-title"> Water <small> Approval and Implementation Status</small> </h1>

  <div class="col-md-12">
    <div class="portlet light tasks-widget bordered">
      <div class="portlet-body util-btn-margin-bottom-5">

        <div class="table-responsive">
          <style>
            #example4 td {
              /*white-space: nowrap;*/
              font-size: 10px;
            }
            #example4 th {
              font-size: 10px;
            }
          </style>

          <table class="table table-bordered table-hover table-condensed" id="example4">
            <thead class="flip-content">
                <tr>
                  <td colspan="6" class="text-center">Approval status</td>
                  <td colspan="7" class="text-center">Implementation status</td>
                </tr>
                <tr>
                  <td>Total Submitted</td>
                  <td>Approved</td>
                  <td>Pending</td>
                  <td>Recomended</td>
                  <td>Cancelled</td>
                  <td>Rejected</td>
                  <td>In Tendering process</td>
                  <td>Under construction</td>
                  <td>Completed</td>
                  <td>WQ tested</td>
                  <td>Platform constructed</td>
                  <td>Depth measured</td>
                  <td>GPS Coordinates</td>
                </tr>

            </thead>

            <tbody>
              @foreach($datas as $rs)
                <tr>
                <td>{{$rs->subappcount}}</td>
                <td>{{$rs->sumappcount}}</td>
                <td>{{$rs->Submitted}}</td>
                <td>{{$rs->Recomended}}</td>
                <td>{{$rs->Cancelled}}</td>
                <td>{{$rs->Rejected}}</td>
                <td>{{$rs->TenderingInProcess}}</td>
                <td>{{$rs->UnderImplementation}}</td>
                <td>{{$rs->Completed}}</td>
                <td>{{$rs->wq_Arsenic}}</td>
                <td>{{$rs->platform}}</td>
                <td>{{$rs->depth}}</td>
                <td>{{$rs->x_coord}}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  @endif
@endsection


