@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('superadmin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="#">Sub-project</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="#">Sanitation </a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>List of Sanitation Schemes</span>
    </li>
  </ul>
</div>

@include('partials.errors')
  <div class="portlet light tasks-widget bordered" style="margin-top: 15px;">
    <div class="portlet-title">
      <div class="caption">Sanitation <small> List of Sanitation Schemes</small></div>
        <div class="actions">
          <a href="{{route('superadmin.sub-projects.sanitation.index', ['download' => 'summary'])}}" class="btn btn-circle success"> <i class="fa fa-download"></i> Download</a>
        </div>
    </div>

    <div class="portlet-body util-btn-margin-bottom-5">

      <div class="table-responsive">
        <style>
          #example0 td {
            white-space: nowrap;
            font-size: 10px;
          }
          #example0 th {
            font-size: 10px;
          }
        </style>

        <table class="table table-bordered table-hover table-condensed" id="example0">
          <thead class="flip-content">
              <tr>
                <td class="text-center"><strong>Region</strong></td>
                <td colspan="3" class="text-center"><strong>Approval/Implementation status</strong></td>
                <td colspan="4" class="text-center"><strong>Types of latrines </strong></td>
                <td colspan="2" nowrap="nowrap" class="text-center"><strong>Types by Nos. of Chamber</strong></td>
                <td colspan="2" class="text-center"><strong>Construction type</strong></td>
              </tr>
              <tr>
                <td>&nbsp;</td>
                <td class="text-center">Total submitted</td>
                <td class="text-center">Approved</td>
                <td class="text-center">Completed</td>

                <td class="text-center">School</td>
                <td class="text-center">Madrasha</td>
                <td class="text-center">Mosque</td>
                <td class="text-center">Public</td>

                <td class="text-center">Two Chamber</td>
                <td class="text-center">Three Chamber</td>

                <td class="text-center">New</td>
                <td class="text-center">renovation</td>
              </tr>

          </thead>

          <tbody>

          <?php

            $aa1 = 0 ;
            $aa2 = 0 ;
            $aa3 = 0 ;
            $aa4 = 0 ;
            $aa5 = 0 ;
            $aa6 = 0 ;

            $aa7 = 0 ;
            $aa8 = 0 ;
            $aa9 = 0 ;
            $aa10 = 0 ;
            $aa11= 0 ;
            $aa12 = 0 ;
          ?>


            @foreach($datas as $d)
              <tr>

                <td class="text-center">{{$d->region_name}}</td>
                <td class="text-center">{{$d->subappcount}}</td>
                <td class="text-center">{{$d->Approved}}</td>
                <td class="text-center">{{$d->Completed}}</td>
                <td class="text-center">{{$d->School}}</td>
                <td class="text-center">{{$d->Madrasha}}</td>

                <td class="text-center">{{$d->Mosque}}</td>
                <td class="text-center">{{$d->Community}}</td>
                <td class="text-center">{{$d->TwoChamber}}</td>
                <td class="text-center">{{$d->ThreeChamber}}</td>
                <td class="text-center">{{$d->New}}</td>
                <td class="text-center">{{$d->Renovation}}</td>
              </tr>

              <?php

                $aa2 += $d->subappcount;
                $aa3 += $d->Approved;
                $aa4 += $d->Completed;
                $aa5 += $d->School;
                $aa6 += $d->Madrasha;

                $aa7 += $d->Mosque;
                $aa8 += $d->Community;
                $aa9 += $d->TwoChamber;
                $aa10 += $d->ThreeChamber;
                $aa11 += $d->New;
                $aa12 += $d->Renovation;

              ?>
            @endforeach

            @if($aa2)

            <tr>
              <td class="text-center">Total</td>
              <th class="text-center">{{$aa2}}</th>
              <th class="text-center">{{$aa3}}</th>
              <th class="text-center">{{$aa4}}</th>
              <th class="text-center">{{$aa5}}</th>
              <th class="text-center">{{$aa6}}</th>
              <th class="text-center">{{$aa7}}</th>
              <th class="text-center">{{$aa8}}</th>
              <th class="text-center">{{$aa9}}</th>
              <th class="text-center">{{$aa10}}</th>
              <th class="text-center">{{$aa11}}</th>
              <th class="text-center">{{$aa12}}</th>
            </tr>

            @endif

          </tbody>
        </table>
      </div>

    </div>
  </div>

@endsection
