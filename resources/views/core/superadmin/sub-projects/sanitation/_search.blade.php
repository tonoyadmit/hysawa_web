<div class="col-md-12" style="border-width: 1px; border-style: solid;">

  <form action="{{route('superadmin.sub-projects.sanitation.index')}}" class="form-horizontal" method="GET" id="createForm">
    <input type="hidden" name="query" value="{{time()}}" />
    <div class="form-body">

      <div class="row">

        <div class="col-md-3">
          <label class="control-label">Project</label>
          <select class="form-control input-sm" name="project_id">
            <option value="">Select Project</option>
            <?php $projects = App\Model\Project::orderBy('project')->get(); ?>
            @if($projects != "")
              @foreach($projects as $project)
                <option value="{{$project->id}}"
                  @if($old['project_id'] == $project->id) selected="selected" @endif
                >{{$project->project}}</option>
              @endforeach
            @endif
          </select>
        </div>

          <div class="col-md-3">

            <label class="control-label">Region</label>
            <select class="form-control input-sm" name="region_id">
              <option value="">Select Region</option>
              <?php
              $regions = App\Model\Region::orderBy('region_name')->get();
              ?>
              @if($regions != "")
              @foreach($regions as $region)
              <option value="{{$region->id}}"
                @if($old['region_id'] == $region->id)
                selected="selected"
                @endif
                >{{$region->region_name}}</option>
                @endforeach
                @endif
              </select>
          </div>

            <div class="col-md-2">

              <label class="control-label">District</label>
              <select class="form-control input-sm" id="district_id" name="district_id">
                <option value="">Select District</option>
                <?php
                $districts = App\Model\District::orderBy('distname')->get();
                ?>
                @foreach( $districts as $district)
                <option value="{{$district->id}}"
                  @if($old['district_id'] == $district->id)
                  selected="selected"
                  @endif
                  >{{$district->distname}}</option>
                  @endforeach


                </select>

              </div>

              <div class="col-md-2">

                <label class="control-label">Upazila</label>
                <select class="form-control input-sm" id="upazila_id" name="upazila_id">
                  <option value="">Select Upazila</option>
                  @if(!empty($old) && !empty($old['district_id']))
                  <?php
                  $upazilas = App\Model\Upazila::where('disid', $old['district_id'])->orderBy('upname')->get();
                  ?>

                  @foreach( $upazilas as $upazila)
                  <option value="{{$upazila->id}}"
                    @if($old['upazila_id'] == $upazila->id)
                    selected="selected"
                    @endif
                    >{{$upazila->upname}}</option>
                    @endforeach
                    @endif
                  </select>

                </div>

                <div class="col-md-2">

                  <label class="control-label">Union</label>
                  <select name="union_id" class="form-control input-sm" id="union_id" name="union_id">
                    <option value="">Select Union</option>
                    @if(!empty($old) && !empty($old['upazila_id']))
                    <?php
                    $unions = App\Model\Union::where('upid', $old['upazila_id'])->orderBy('unname')->get();
                    ?>

                    @foreach($unions as $union)
                    <option value="{{$union->id}}"
                      @if($old['union_id'] == $union->id)
                      selected="selected"
                      @endif
                      >{{$union->unname}}</option>
                    @endforeach
                      @endif
                    </select>
                  </div>

                </div>

                <div class="row">

                  <div class="col-md-2">

                    <label class="control-label">CDF No</label>
                    <input type="text" name="cdf_no" class="form-control input-sm"
                    @if(!empty($old) && !empty($old['cdf_no']))value="{{$old['cdf_no']}}"@endif />

                  </div>

                  <div class="col-md-2">

                    <label class="control-label">Village</label>
                    <input type="text" name="village" class="form-control input-sm"
                    @if(!empty($old) && !empty($old['village']))value="{{$old['village']}}"@endif />

                  </div>

                  <div class="col-md-2">

                    <label class="control-label" style="font-size: 10px;">Approve Date: YYYY-MM-DD</label>
                    <input type="text" class="form-control date-picker" name="starting_date" data-date-format="yyyy-mm-dd"
                    @if(!empty($old) && !empty($old['starting_date']))
                    value="{{$old['starting_date']}}"
                    @endif
                    />

                  </div>

                  <div class="col-md-2">

                    <label class="control-label">Approval Status</label>
                    <select name="app_status" class="form-control input-sm" >
                      <option value="">Select Status</option>
                      <?php
                      $app_status = \DB::table('app_status')->orderBy('app_status')->get();
                      ?>
                      @if($app_status != "")
                      @foreach($app_status as $aa)
                      <option value="{{$aa->app_status}}"
                        @if($old['app_status'] == $aa->app_status)
                        selected="selected"
                        @endif
                        >{{$aa->app_status}}</option>
                        @endforeach
                        @endif
                      </select>

                    </div>

                    <div class="col-md-2">

                      <label class="control-label">Impl. Status</label>
                      <select name="impl_status" class="form-control input-sm" >
                        <option value="">Select Status</option>
                        <?php
                        $imp_status = \DB::table('imp_status')->orderBy('imp_status')->get();
                        ?>
                        @if($imp_status != "")
                        @foreach($imp_status as $aa)
                        <option value="{{$aa->imp_status}}"
                          @if($old['impl_status'] == $aa->imp_status) selected="selected" @endif
                          >{{$aa->imp_status}}</option>
                          @endforeach
                          @endif
                        </select>
                    </div>

                      <div class="col-md-2" style="margin-top: 23px;">
                        <input type="submit" class="btn green btn-sm" value="Filter" />
                        <a href="{{route('superadmin.sub-projects.sanitation.index')}}" class="btn btn-info btn-sm">Clear</a>
                        @if(!empty($old['query']) && !empty($datas) && count($datas))
                        <?php
                        $old3 = $old;
                        $old3['print'] = 'print';
                        ?>
                        <a href="{{route('superadmin.sub-projects.sanitation.index', $old3)}}" class="btn btn-primary btn-sm">Print</a>
                        @endif
                      </div>
                    </div>

      @if(isset($_REQUEST['query']))
      <div class="row">
        <div class="col-md-12">
            <?php
              $projectName = "";
              if(isset($_REQUEST['project_id']) && $_REQUEST['project_id'] != ""){
                $projectName = App\Model\Project::find($_REQUEST['project_id'])->project;
              }

              $regionName  = "";
              if(isset($_REQUEST['region_id']) && $_REQUEST['region_id'] != ""){
                $regionName = App\Model\Region::find($_REQUEST['region_id'])->region_name;
              }


              $districtName = "";
              if(isset($_REQUEST['district_id']) && $_REQUEST['district_id'] != ""){
                $districtName = App\Model\District::find($_REQUEST['district_id'])->distname;
              }

              $upazilaName = "";
              if(isset($_REQUEST['upazila_id']) && $_REQUEST['upazila_id'] != "")
              {
                $upazilaName = App\Model\Upazila::find($_REQUEST['upazila_id'])->upname;
              }

              $unionName = "";
              if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] != "")
              {
                $unionName = App\Model\Union::find($_REQUEST['union_id'])->unname;
              }

            ?>
            <p>
              Query:
              <strong>Project</strong>: {{$projectName}}|
              <strong>Region</strong>: {{$regionName}}|
              <strong>District</strong>: {{$districtName}}|
              <strong>Upazila</strong>: {{$upazilaName}}|
              <strong>Union</strong>: {{$unionName}}|
              <strong>CDF No</strong>: {{$_REQUEST['cdf_no']}}|
              <strong>Village</strong>: {{$_REQUEST['village']}}|
              <strong>Approve Date</strong>: {{$_REQUEST['starting_date']}}|
              <strong>Approval Status</strong>: {{$_REQUEST['app_status']}}|
              <strong>Impl. Status</strong>: {{$_REQUEST['impl_status']}}
            </p>

        </div>
      </div>
      @endif

                  </div>
                </form>
              </div>