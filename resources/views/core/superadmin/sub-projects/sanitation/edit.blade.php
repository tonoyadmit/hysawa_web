@extends('layouts.appinside')

@section('content')
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{route('superadmin.dashboard')}}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="{{route('superadmin.sub-projects.sanitation.index')}}">Sanitation</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Update</span>
    </li>
  </ul>
</div>

<h1 class="page-title"> Sanitation <small>Update</small></h1>

{{ Form::model(
    $sanitation, [
      route('superadmin.sub-projects.sanitation.update', $sanitation->id),
      'method' => 'POST',
      'class' => 'form-horizontal']) }}

<div class="form-body">
  @include('partials.errors')
  <div class="col-md-6">
    <div class="form-group">
      <label class="control-label col-md-3">CDF No:</label>
      <div class="col-md-9">
      {{ Form::text('cdfno', null, ['class' => 'form-control input-sm', 'placeholder' => 'CDF No:', 'required' => 'required'])}}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-3">Village Name</label>
      <div class="col-md-9">
      {{ Form::text('Village', null, ['class' => 'form-control input-sm', 'placeholder' => 'Village Name'])}}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-3">Latrine no. (6 digit upcode+sl)</label>
      <div class="col-md-9">
      {{ Form::number('latrineno', null, ['class' => 'form-control input-sm', 'placeholder' => 'TW_No', 'required' => 'required','min'=>"1",'max'=>"999999" ])}}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-3">Type of latrine:</label>
      <div class="col-md-9">
      {{ Form::select('maintype', array(''=>'Type of latrine:')+$mainype, null, ['class' => 'form-control input-sm', 'required' => 'required'])}}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-3">Type of Institution:</label>
      <div class="col-md-9">
      {{ Form::select('subtype', array(''=>'Type of Institution')+$subtype, null, ['class' => 'form-control input-sm', 'required' => 'required'])}}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-3">New/Renovation:</label>
      <div class="col-md-9">
      {{ Form::select('cons_type', array('New'=>'New','Renovation'=>'Renovation'), null, ['class' => 'form-control input-sm', 'required' => 'required'])}}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-3">Name of Institution/Place:</label>
      <div class="col-md-9">
      {{ Form::number('name', null, ['class' => 'form-control input-sm', 'placeholder' => 'Number of Household benefited', 'required' => 'required'])}}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-3">Chairman of the Mgt Committe:</label>
      <div class="col-md-9">
      {{ Form::text('ch_comittee', null, ['class' => 'form-control input-sm', 'placeholder' => 'Chairman of the Mgt Committe', 'required' => 'required'])}}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-3">Phone no:</label>
      <div class="col-md-9">
      {{ Form::text('ch_com_tel', null, ['class' => 'form-control input-sm', 'placeholder' => 'Phone no', 'required' => 'required'])}}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-3">Implementation Status</label>
      <div class="col-md-9">
      {{ Form::select('imp_status', array(''=>'Choose an Option') + $ImplementStatus,null, ['class' => 'form-control input-sm', 'required' => 'required'])}}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-3"> Caretaker name:</label>
      <div class="col-md-9">
      {{ Form::text('caretakername', null, ['class' => 'form-control input-sm', 'placeholder' => 'Caretaker name', 'required' => 'required'])}}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-3"> Caretaker phone:</label>
      <div class="col-md-9">
      {{ Form::text('caretakerphone', null, ['class' => 'form-control input-sm', 'placeholder' => 'Caretaker phone', 'required' => 'required'])}}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-3">Approval date (YYYY-MM-DD):</label>
      <div class="col-md-9">
      {{ Form::text('app_date', null, ['class' => 'form-control date-picker input-sm', 'placeholder' => 'Approval date', 'required' => 'required', 'data-date-format' =>"yyyy-mm-dd", 'readonly'])}}
      </div>
    </div>
  </div>

  <div class="col-md-6">
    <div class="form-group">
      <label class="control-label col-md-3">No. of chamber for Male/Boys:</label>
      <div class="col-md-9">
      {{ Form::text('malechamber', null, ['class' => 'form-control input-sm', 'placeholder' => 'No. of chamber for Male/Boys', 'required' => 'required'])}}
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3">No. of chamber for Female/Girls:</label>
      <div class="col-md-9">
      {{ Form::text('femalechamber', null, ['class' => 'form-control input-sm', 'placeholder' => 'No. of chamber for Female/Girls', 'required' => 'required'])}}
      </div>

    </div>
    <div class="form-group">
      <label class="control-label col-md-3">No. of Male beneficiry:</label>
      <div class="col-md-9">
      {{ Form::text('male_ben', null, ['class' => 'form-control input-sm', 'required' => 'required', 'placeholder' => 'No. of Male beneficiry'])}}
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3">No. of Female Beneficary:</label>
      <div class="col-md-9">
      {{ Form::text('fem_ben', null, ['class' => 'form-control input-sm', 'required' => 'required', 'placeholder' => 'No. of Female Beneficary'])}}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-3">No. of Child beneficiary:</label>
      <div class="col-md-9">
      {{ Form::text('child_ben', null, ['class' => 'form-control input-sm', 'required' => 'required', 'placeholder' => 'No. of Child beneficiary'])}}
      </div>

    </div>
    <div class="form-group">
      <label class="control-label col-md-3"> No. of Disable benficiary:</label>
      <div class="col-md-9">
      {{ Form::text('disb_bene', null, ['class' => 'form-control input-sm', 'placeholder' => 'No. of Disable benficiary', 'required' => 'required'])}}
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3">Proposed/Installed facilities:</label>
      <div class="col-md-9">
      {{ Form::number('watersource', null, ['class' => 'form-control input-sm', 'placeholder' => 'Proposed/Installed facilities', 'required' => 'required'])}}
      </div>

    </div>
    <div class="form-group">
      <label class="control-label col-md-3">Approval Status:</label>
      <div class="col-md-9">
      {{ Form::select('app_status',array(''=>'Choose an Option')+$appstatus, null , ['class' => 'form-control input-sm', 'required' => 'required'])}}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-3">Longitude:</label>
      <div class="col-md-9">
      {{ Form::number('longitude', null, ['class' => 'form-control input-sm', 'placeholder' => 'Longitude', 'required' => 'required'])}}
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3"> Latitude:</label>
      <div class="col-md-9">
      {{ Form::number('latitude', null, ['class' => 'form-control input-sm', 'placeholder' => 'Latitude', 'required' => 'required'])}}
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3">Overhead tank:</label>
      <div class="col-md-9">
      {{Form::radio('overheadtank', 'yes')}}Yes
      {{Form::radio('overheadtank', 'NO')}}NO
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3">Motor/pump:</label>
      <div class="col-md-9">
      {{Form::radio('motorpump', 'yes')}} Yes
      {{Form::radio('motorpump', 'NO')}} NO
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3">Sock well:</label>
      <div class="col-md-9">
      {{Form::radio('sockwell', 'yes')}}Yes
      {{Form::radio('sockwell', 'NO')}} NO
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3">Septic tank:</label>
      <div class="col-md-9">
      {{Form::radio('seotictank', 'yes')}} Yes
      {{Form::radio('seotictank', 'NO')}} NO
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-3">Tap outside:</label>
      <div class="col-md-9">
      {{Form::radio('tapoutside', 'yes')}} Yes
      {{Form::radio('tapoutside', 'NO')}} NO
      </div>
    </div>
  </div>
</div>

&nbsp;
<div class="form-action">
  <div class="row">
    <div class="col-md-offset-3">
      <a href="javascript:history.back()" class="btn default"> Cancel </a>
      {{Form::submit('Update', ['class' => 'btn green']) }}
    </div>
  </div>

</div>

{{ Form::close() }}

</div>
@endsection
