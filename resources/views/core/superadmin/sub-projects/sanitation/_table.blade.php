<div class="portlet light tasks-widget bordered">
  <div class="portlet-title">
    <div class="caption">List of Sanitation Schemes</div>

    <div class="actions">

    @if(isset($datas) && count($datas) )
        <?php
          $oldN = $old;
          $oldN['download'] = 'list';
        ?>
        <a href="{{route('superadmin.sub-projects.sanitation.index',$oldN)}}" class="btn btn-circle success"> <i class="fa fa-download"></i> Download</a>
    @endif


    </div>
  </div>

  @if(isset($datas) && count($datas) )

  <div class="portlet-body util-btn-margin-bottom-5">
    <div class="table-responsive">
      <style>
        #example0 td {
          white-space: nowrap;
          font-size: 10px;
        }
        #example0 th {
          font-size: 10px;
        }
      </style>
      <table class="table table-bordered table-hover table-condensed" id="example0">
        <thead class="flip-content">
            <tr>
              <td>Action</td>
              <td>Approval Status</td>
              <td>Imple status</td>
              <td>District</td>
              <td>Upazila</td>
              <td>Union</td>
              <td>CDF no</td>
              <td>Cons_type</td>
              <td>Village</td>
              <td>Type of inst.</td>
              <td>subtype</td>
              <td>Name</td>
              <td>Male chamber</td>
              <td>Female chamber</td>
              <td>Overhead tank</td>
              <td>Motor/Pump</td>
              <td>Water source</td>
              <td>Sockwell</td>
              <td>Septic tank</td>
              <td>Tap outside</td>
              <td>longitude</td>
              <td>latitude</td>
              <td>Male users</td>
              <td>Fem users</td>
              <td>Child_users</td>
              <td>Disb_user</td>
              <td>Caretaker</td>
              <td>Caretaker phone</td>
              <td>Chairman of comittee</td>
              <td>Phone</td>
              <td>Approval date</td>

            </tr>
        </thead>

        <tbody>
          <?php $index = 1; ?>
          @foreach($datas as $d)
            <tr>
              <td><a href="{{route('superadmin.sub-projects.sanitation.edit', $d->id)}}" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a></td>
            <td>{{ $d->app_status}}</td>
            <td>{{ $d->imp_status}}</td>
            <td>{{ $d->district->distname or ''}}</td>
            <td>{{ $d->upazila->upname or '' }}</td>
            <td>{{ $d->union->unname or ''}}</td>
            <td>{{ $d->cdfno}}</td>
            <td>{{ $d->cons_type}}</td>
            <td>{{ $d->village}}</td>
            <td>{{ $d->maintype}}</td>
            <td>{{ $d->subtype}}</td>
            <td>{{ $d->name}}</td>
            <td>{{ $d->malechamber}}</td>
            <td>{{ $d->femalechamber}}</td>
            <td>{{ $d->overheadtank}}</td>
            <td>{{ $d->motorpump}}</td>
            <td>{{ $d->watersource}}</td>
            <td>{{ $d->sockwell}}</td>
            <td>{{ $d->seotictank}}</td>
            <td>{{ $d->tapoutside}}</td>
            <td>{{ $d->longitude}}</td>
            <td>{{ $d->latitude}}</td>
            <td>{{ $d->male_ben}}</td>
            <td>{{ $d->fem_ben}}</td>
            <td>{{ $d->child_ben}}</td>
            <td>{{ $d->disb_bene}}</td>
            <td>{{ $d->caretakername}}</td>
            <td>{{ $d->caretakerphone}}</td>
            <td>{{ $d->ch_comittee}}</td>
            <td>{{ $d->ch_com_tel}}</td>
            <td>{{ $d->app_date}}</td>


            </tr>
          @endforeach

        </tbody>
      </table>
      {{$datas->appends($old)->links()}}
    </div>
  </div>

  @endif


</div>
