@extends('layouts.print.app')

@section('my_style')
<style>
    table{
      border-collapse: collapse;
      border-spacing: 0;
    }

    div{
      font-size: 80%;
      display: inline-block;
     /* overflow: visible;

      width: 2.4em;
      line-height: 0.9em;
      margin-top: 9em;

      white-space: nowrap;
      -ms-transform: rotate(270deg);
      -o-transform: rotate(270deg);
      -webkit-transform: rotate(270deg);
      transform: rotate(270deg);
      text-align: left;*/
    }

    .headline th {
      padding: 5px 20px;
    }
  </style>
@endsection

@section('content')

<table class="headline">
  <tr>
    <th style="text-align: left;">Region :</th><td>{{$datas->first()->region->region_name or ''}}</td>
    <th style="text-align: left;">District :</th><td>{{$datas->first()->district->distname or ''}}</td>
    <th style="text-align: left;">Upazila :</th><td>{{$datas->first()->upazila->upname or ''}}</td>
  </tr>
</table>

@if(isset($_REQUEST['query']))

            <?php
              $projectName = "";
              if(isset($_REQUEST['project_id']) && $_REQUEST['project_id'] != ""){
                $projectName = App\Model\Project::find($_REQUEST['project_id'])->project;
              }

              $regionName  = "";
              if(isset($_REQUEST['region_id']) && $_REQUEST['region_id'] != ""){
                $regionName = App\Model\Region::find($_REQUEST['region_id'])->region_name;
              }


              $districtName = "";
              if(isset($_REQUEST['district_id']) && $_REQUEST['district_id'] != ""){
                $districtName = App\Model\District::find($_REQUEST['district_id'])->distname;
              }

              $upazilaName = "";
              if(isset($_REQUEST['upazila_id']) && $_REQUEST['upazila_id'] != "")
              {
                $upazilaName = App\Model\Upazila::find($_REQUEST['upazila_id'])->upname;
              }

              $unionName = "";
              if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] != "")
              {
                $unionName = App\Model\Union::find($_REQUEST['union_id'])->unname;
              }

            ?>
            <p>
              Query:
              <strong>Project</strong>: {{$projectName}}|
              <strong>Region</strong>: {{$regionName}}|
              <strong>District</strong>: {{$districtName}}|
              <strong>Upazila</strong>: {{$upazilaName}}|
              <strong>Union</strong>: {{$unionName}}|
              <strong>CDF No</strong>: {{$_REQUEST['cdf_no']}}|
              <strong>Village</strong>: {{$_REQUEST['village']}}|
              <strong>Approve Date</strong>: {{$_REQUEST['starting_date']}}|
              <strong>Approval Status</strong>: {{$_REQUEST['app_status']}}|
              <strong>Impl. Status</strong>: {{$_REQUEST['impl_status']}}
            </p>

      @endif


<h2>List of Sanitation Schemes(HYSAWA): {{count($datas)}}</h2>

<table class="box" border="1">
    <thead class="rotate">

      <th><div>District</div></th>
      <th><div>Upazila</div></th>
      <th><div>Union</div></th>

      <th><div>Approval</div></th>
      <th><div>Implementation Status</div></th>
      <th><div>Approval Date</div></th>
      <th><div>CDF No</div></th>
      <th><div>Type</div></th>

      <th><div>Village</div></th>
      <th><div>Type of Inst.</div></th>
      <th><div>SubType</div></th>
      <th><div>Name</div></th>
      <th><div>Male Chamber</div></th>

      <th><div>Female Chamber</div></th>
      <th><div>Male Users</div></th>
      <th><div>Female Users</div></th>

    </thead>
    <tbody>
      @foreach($datas as $sanitation)
      <tr>

        <td><div>@if(!empty($sanitation->district)) {{$sanitation->district->distname or ''}} @endif</div></td>
        <td><div>@if(!empty($sanitation->upazila)) {{$sanitation->upazila->upname or ''}} @endif</div></td>
        <td><div>@if(!empty($sanitation->union)) {{$sanitation->union->unname or ''}} @endif</div></td>

        <td ><div>{{ $sanitation->app_status }}</div></td>
        <td ><div>{{ $sanitation->imp_status }}</div></td>
        <td ><div>{{ $sanitation->app_date }}</div></td>
        <td ><div>{{ $sanitation->cdfno }}</div></td>
        <td ><div>{{ $sanitation->cons_type }}</div></td>
        <td ><div>{{ $sanitation->village }}</div></td>
        <td ><div>{{ $sanitation->maintype }}</div></td>
        <td ><div>{{ $sanitation->subtype }}</div></td>
        <td ><div>{{ $sanitation->name }}</div></td>
        <td ><div>{{ $sanitation->malechamber }}</div></td>
        <td ><div>{{ $sanitation->femalechamber }}</div></td>
        <td ><div>{{ $sanitation->male_ben }}</div></td>
        <td ><div>{{ $sanitation->fem_ben }}</div></td>

      </tr>
      @endforeach
    </tbody>
  </table>
@endsection