<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">

    <div class="portlet-title">
          <div class="caption">
            <i class="fa fa-gift"></i>Filter Result
          </div>
          <div class="actions">
            <a href="{{route('superadmin.procurement.list.download',$old)}}" class="btn btn-circle "> <i class="fa fa-download"></i> Download</a>
            &nbsp;&nbsp;
            <a href="{{route('superadmin.procurement.list.print',$old)}}" class="btn btn-circle "> <i class="fa fa-print"></i> Print</a>

          </div>
        <div class="tools">
           {{--  <a href="javascript:;" class="collapse"> </a> --}}
        </div>
    </div>

    <div class="portlet-body util-btn-margin-bottom-5">

      <div class="table-responsive">

        <style>
          #example0 td, #example0 th{font-size: 10px;}
        </style>

        <table class="table table-bordered table-hover table-condensed" id="example0">

          <thead class="flip-content">
            <tr>
              <tr>
                <th class="text-center">Memo No</th>
                <th class="text-center">Package Name</th>
                <th class="text-center">Date of announcement</th>
                <th class="text-center">Last date of receiving</th>
                <th class="text-center">Opening date</th>
                <th class="text-center">Price of Tender Schedule</th>
                <th class="text-center">Estimated Value</th>
                <th class="text-center">Tender Security Amount</th>
                <th class="text-center">Days to complete work</th>
                <th class="text-center">Method:</th>
                <th class="text-center">Last date of selling</th>

                <th class="text-center">Selling Office1:</th>
                <th class="text-center">Selling Office2:</th>
                <th class="text-center">Selling Office3:</th>
                <th class="text-center">Recieving office1:</th>
                <th class="text-center">Recieving office2:</th>
                <th class="text-center">Recieving office3:</th>
              </tr>
            </tr>
          </thead>

          <tbody>
            <?php $index = 1; ?>
            @foreach($lists as $c)
             <tr>
                <td class="text-left">{{ $c->memo_no }}</td>
                <td class="text-left">{{ $c->package }}</td>
                <td class="text-left">{{ $c->d_announce }}</td>
                <td class="text-left">{{ $c->d_receive }}</td>
                <td class="text-left">{{ $c->d_open }}</td>
                <td class="text-left">{{ $c->price_schedule }}</td>
                <td class="text-left">{{ $c->estimate }}</td>
                <td class="text-left">{{ $c->s_money }}</td>
                <td class="text-left">{{ $c->date_com_work }}</td>
                <td class="text-left">{{ $c->method }}</td>
                <td class="text-left">{{ $c->d_sell }}</td>

                <td class="text-left">{{ $c->s_office_1 }}</td>
                <td class="text-left">{{ $c->s_office_2 }}</td>
                <td class="text-left">{{ $c->s_office_3 }}</td>
                <td class="text-left">{{ $c->r_office_1 }}</td>
                <td class="text-left">{{ $c->r_office_2 }}</td>
                <td class="text-left">{{ $c->r_office_3 }}</td>
              </tr>
            @endforeach
          </tbody>

        </table>

        <div class="pagination pull-right">
           {!! $lists->appends($old)->render() !!}
        </div>

      </div>

    </div>
  </div>
</div>