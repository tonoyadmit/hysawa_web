@extends('layouts.print.app')

@section('my_style')
<style>
    table
    {
      border-collapse: collapse;
      border-spacing: 0;
    }

    div{
      font-size: 80%;
      display: inline-block;
    }

    .headline th {
      padding: 5px 20px;
    }
  </style>
@endsection

@section('content')

<h2>Procurement List</h2>

<table class="box" border="1">
    <thead class="rotate">

      <th><div>Project</div></th>
      <th><div>District</div></th>
      <th><div>Upazila</div></th>
      <th><div>Union</div></th>

      <th><div>Memo No</div></th>
      <th><div>Package Name</div></th>
      <th><div>Date of announcement</div></th>
      <th><div>Last date of receiving</div></th>
      <th><div>Opening date</div></th>
      <th><div>Price of Tender Schedule</div></th>
      <th><div>Estimated Value</div></th>
      <th><div>Tender Security Amount</div></th>
      <th><div>Days to complete work</div></th>
      <th><div>Method</div></th>
      <th><div>Last date of selling</div></th>

      <th><div>Selling Office1</div></th>
      <th><div>Selling Office2</div></th>
      <th><div>Selling Office3</div></th>
      <th><div>Recieving office1</div></th>
      <th><div>Recieving office2</div></th>
      <th><div>Recieving office3</div></th>
    </thead>

    <tbody>
      @foreach($lists as $c)
      <tr>
        <td >@if($c->project!=""){{$c->project->project}}@endif</td>
        <td >@if(
          $c->union != "" &&
          $c->union->upazila != "" &&
          $c->union->upazila->district != "") {{$c->union->upazila->district->distname}} @endif</td>
        <td >@if($c->union != "" && $c->union->upazila != ""){{$c->union->upazila->upname}}@endif</td>
        <td >@if($c->union != "" ){{$c->union->unname}} @endif</td>

        <td >{{ $c->memo_no }}</td>
        <td >{{ $c->package }}</td>
        <td >{{ $c->d_announce }}</td>
        <td >{{ $c->d_receive }}</td>
        <td >{{ $c->d_open }}</td>
        <td >{{ $c->price_schedule }}</td>
        <td >{{ $c->estimate }}</td>
        <td >{{ $c->s_money }}</td>
        <td >{{ $c->date_com_work }}</td>
        <td >{{ $c->method }}</td>
        <td >{{ $c->d_sell }}</td>

        <td >{{ $c->s_office_1 }}</td>
        <td >{{ $c->s_office_2 }}</td>
        <td >{{ $c->s_office_3 }}</td>
        <td >{{ $c->r_office_1 }}</td>
        <td >{{ $c->r_office_2 }}</td>
        <td >{{ $c->r_office_3 }}</td>

      </tr>
      @endforeach
    </tbody>
  </table>
@endsection