@extends('layouts.print.app')

@section('my_style')
<style>
    table{
      border-collapse: collapse;
      border-spacing: 0;
    }

    div{
      font-size: 80%;
      display: inline-block;
     /* overflow: visible;

      width: 2.4em;
      line-height: 0.9em;
      margin-top: 9em;

      white-space: nowrap;
      -ms-transform: rotate(270deg);
      -o-transform: rotate(270deg);
      -webkit-transform: rotate(270deg);
      transform: rotate(270deg);
      text-align: left;*/
    }

    .headline th {
      padding: 5px 20px;
    }
  </style>
@endsection

@section('content')

{{-- <table class="headline">
  <tr>
    <th style="text-align: left;">District :</th><td>{{$waters->first()->distname}}</td>
    <th style="text-align: left;">Upazila :</th><td>{{$waters->first()->upname}}</td>
    <th style="text-align: left;">Union :</th><td>{{$waters->first()->unname}}</td>
  </tr>
  <tr>
    <th style="text-align: left;">Approval Date :</th><td>{{$waters->first()->App_date}}</td>
    <th style="text-align: left;">Work Order/Lot No :</th><td>{{$waters->first()->Tend_lot}}</td>
  </tr>
</table> --}}

<h2>TEC</h2>

<table class="box" border="1">
    <thead class="rotate">

      <th><div>Project</div></th>
      <th><div>District</div></th>
      <th><div>Upazila</div></th>
      <th><div>Union</div></th>

      <th><div>Name</div></th>
      <th><div>Designation in Comittee</div></th>
      <th><div>Designation in UP</div></th>
      <th><div>Phone</div></th>
    </thead>
    <tbody>
      @foreach($tecs as $c)
      <tr>
        <td >@if($c->project!=""){{$c->project->project}}@endif</td>
        <td >@if(
          $c->union != "" &&
          $c->union->upazila != "" &&
          $c->union->upazila->district != "") {{$c->union->upazila->district->distname}} @endif</td>
        <td >@if($c->union != "" && $c->union->upazila != ""){{$c->union->upazila->upname}}@endif</td>
        <td >@if($c->union != "" ){{$c->union->unname}} @endif</td>

        <td >{{$c->name}}</td>
        <td >{{$c->deg}}</td>
        <td >{{$c->UP_desg}}</td>
        <td >{{$c->phone}}</td>

      </tr>
      @endforeach
    </tbody>
  </table>
@endsection