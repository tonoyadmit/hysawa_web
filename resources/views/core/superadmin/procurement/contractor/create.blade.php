@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('superadmin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="{{route('superadmin.procurement.contractors.index')}}">Procurement</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Contractor Create</span>
    </li>
  </ul>
</div>

@include('partials.errors')

<h1 class="page-title"> Procurement <small>Contractor List</small> </h1>

<div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i>Filter Result </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body form">
          <form action="{{route('superadmin.procurement.contractors.store')}}" class="form-horizontal" method="POST" id="createForm">
             {{csrf_field()}}

              <div class="form-body">

                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">District</label>
                              <div class="col-md-9">
                                  <select class="form-control" required="required" name="distid">
                                    @foreach($districts as $district)
                                      <option value="{{$district->id}}">{{$district->distname}}</option>
                                    @endforeach
                                  </select>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Contractor (Firm name)</label>
                              <div class="col-md-9">
                                  <input type="text" name="name" class="form-control" required="required" />
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Address:</label>
                              <div class="col-md-9">
                                  <input type="text" name="address" class="form-control" required="required" />
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Contact Person :</label>
                              <div class="col-md-9">
                                  <input type="text" name="contact" class="form-control" required="required" />
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Phone:</label>
                              <div class="col-md-9">
                                  <input type="text" name="phone" class="form-control" />
                              </div>
                          </div>
                      </div>
                  </div>


                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Contractor Type:</label>
                              <div class="col-md-9">
                                  <select class="form-control" required="required" name="cont_type">

                            <option value="Material Supply and Installation">Material Supply and Installation</option>
                            <option value="Only Material Supply">Only Material Supply</option>
                            <option value="Only Installation">Only Installation</option>

                                  </select>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Remarks:</label>
                              <div class="col-md-9">
                                  <input type="text" name="remark" class="form-control" />
                              </div>
                          </div>
                      </div>
                  </div>


                  <div class="row">
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-6">
                            <input type="submit" class="btn green" value="Insert" />

                        </div>
                    </div>
                  </div>
              </div>
          </form>

      </div>
    </div>
  </div>



@endsection
