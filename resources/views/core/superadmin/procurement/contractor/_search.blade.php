<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Procurement <small>Contractor List</small> </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body form">
        <form action="{{route('superadmin.procurement.contractors.index')}}" class="form-horizontal" method="GET" id="searchForm">

            <input type="hidden" name="query" value="{{time()}}" />

            <div class="form-body">


                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">District</label>
                            <div class="col-md-9">
                                <?php
                                    $districts = App\Model\District::all();
                                ?>
                                <select class="form-control" name="district_id" id="district_id">
                                    <option value="">Choose a District</option>
                                    @foreach($districts as $district )
                                        <option value="{{$district->id}}"
                                        @if(!empty($old) && !empty($old['district_id']) && $old['district_id'] == $district->id)
                                            selected="selected"
                                        @endif
                                        >{{$district->distname}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Category</label>
                            <div class="col-md-9">
                                <?php
                                    $c = \DB::table('procurement')
                                            ->where('category', '!=', '')
                                            ->groupBy('category')
                                            ->get();

                                    \Log::info($c->toArray());
                                ?>
                                <select class="form-control" name="category" id="category">
                                    <option value="">Choose a Category</option>
                                    @foreach($c as $cc )

                                        <option value="{{$cc->category}}"
                                        @if(
                                            !empty($old) &&
                                            !empty($old['category']) &&
                                            $old['category'] == $cc->category)

                                            selected="selected"

                                        @endif
                                        >{{$cc->category}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Contractor Name</label>
                            <div class="col-md-9">

                                <input type="text" name="con_name" class="form-control"
                                    @if(!empty($old) && $old['con_name'] != "")
                                        value="{{$old['con_name']}}"
                                    @endif />

                            </div>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="submit" class="btn green" value="Search" />
                                <a href="{{route('superadmin.procurement.contractors.index')}}" class="btn default">Clear</a>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </form>

    </div>
</div>