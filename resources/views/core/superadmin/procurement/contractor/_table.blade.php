<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">

    <div class="portlet-title">
          <div class="caption">
            <i class="fa fa-gift"></i>Filter Result
          </div>
          <div class="actions">
            <a href="{{route('superadmin.procurement.contractors.index', ['download' => 'download'])}}" class="btn btn-circle "> <i class="fa fa-download"></i> Download</a>
          </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body util-btn-margin-bottom-5">

      <a href="{{route('superadmin.procurement.contractors.create')}}" class="btn btn-large btn-success"> Add New Contractor </a>

      <div class="table-responsive">
        <style>
          #example0 td {
            /*white-space: nowrap;*/
            font-size: 10px;
          }
          #example0 th {
            font-size: 10px;
          }
        </style>

        <table class="table table-bordered table-hover table-condensed" id="example0">
          <thead class="flip-content">
            <tr>
              <tr>
                <td class="text-center">Action</td>
                <td>SL.</td>
                <td class="text-center">District</td>
                <td class="text-center">Contractor</td>
                <td class="text-center">Contact Person</td>
                <td class="text-center">Category</td>
                <td class="text-center">Address</td>
                <td class="text-center">Phone</td>
                <td class="text-center">Remarks</td>
              </tr>
            </tr>
          </thead>

          <tbody>
            <?php $index = 1; ?>
            @foreach($contractors as $c)
             <tr>
                <td>
                  <a
                  href="{{route('superadmin.procurement.contractors.edit', $c->id)}}"
                  class="btn btn-green">Edit</a>
                </td>
                <td>{{$index++}}</td>
                <td class="text-left">{{$c->distname}}</td>
                <td class="text-left">{{$c->con_name}}</td>
                <td class="text-left">{{$c->contact}}</td>
                <td class="text-left">{{$c->category}}</td>
                <td class="text-left">{{$c->con_add}}</td>
                <td class="text-left">{{$c->phone}}</td>
                <td class="text-left">{{$c->remarks}}</td>
              </tr>
            @endforeach

          </tbody>
        </table>

        <div class="pagination pull-right">
           {!! $contractors->appends($old)->render() !!}
        </div>
      </div>

    </div>
  </div>
</div>