<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">

    <div class="portlet-title">

          <div class="caption">
            <i class="fa fa-gift"></i>Filter Result
          </div>

          <div class="actions">
            <a href="{{route('superadmin.procurement.pec.download',$old)}}" class="btn btn-circle "> <i class="fa fa-download"></i> Download</a>
            &nbsp;&nbsp;
            <a href="{{route('superadmin.procurement.pec.print',$old)}}" class="btn btn-circle "> <i class="fa fa-print"></i> Print</a>
          </div>

          <div class="tools">
            {{-- <a href="javascript:;" class="collapse">  </a> --}}
          </div>
    </div>

    <div class="portlet-body util-btn-margin-bottom-5">

      <div class="table-responsive">
        <style>
          #example0 td {
            /*white-space: nowrap;*/
            font-size: 10px;
          }
          #example0 th {
            font-size: 10px;
          }
        </style>

        <table class="table table-bordered table-hover table-condensed" id="example0">
          <thead class="flip-content">
            <tr>
                <td class="text-center">Project</td>
                <td class="text-center">District</td>
                <td class="text-center">Upazila</td>
                <td class="text-center">Union</td>

                <td class="text-center">Name</td>
                <td class="text-center">Designation in Comittee</td>
                <td class="text-center">Designation in UP</td>
                <td class="text-center">Phone</td>
            </tr>
          </thead>

          <tbody>
            @foreach($pecs as $c)
             <tr>

                <td class="text-left">@if($c->project!=""){{$c->project->project}}@endif</td>
                <td class="text-left">@if(
                  $c->union != "" &&
                  $c->union->upazila != "" &&
                  $c->union->upazila->district != "") {{$c->union->upazila->district->distname}} @endif</td>
                <td class="text-left">@if($c->union != "" && $c->union->upazila != ""){{$c->union->upazila->upname}}@endif</td>
                <td class="text-left">@if($c->union != "" ){{$c->union->unname}} @endif</td>

                <td class="text-left">{{$c->name}}</td>
                <td class="text-left">{{$c->deg}}</td>
                <td class="text-left">{{$c->UP_desg}}</td>
                <td class="text-left">{{$c->phone}}</td>

              </tr>
            @endforeach

          </tbody>
        </table>

        <div class="pagination pull-right">
           {!! $pecs->appends($old)->render() !!}
        </div>
      </div>

    </div>
  </div>
</div>





