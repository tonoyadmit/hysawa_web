@extends('layouts.print.app')

@section('my_style')
<style>
    table{
      border-collapse: collapse;
      border-spacing: 0;
    }
    div{
      font-size: 80%;
      display: inline-block;
    }
    .headline th {
      padding: 5px 20px;
    }
  </style>
@endsection

@section('content')

<h2>PEC</h2>

<table class="box" border="1">
    <thead class="rotate">

      <th><div>Project</div></th>
      <th><div>District</div></th>
      <th><div>Upazila</div></th>
      <th><div>Union</div></th>

      <th><div>Name</div></th>
      <th><div>Designation in Comittee</div></th>
      <th><div>Designation in UP</div></th>
      <th><div>Phone</div></th>
    </thead>
    <tbody>
      @foreach($pecs as $c)
      <tr>
        <td >@if($c->project!=""){{$c->project->project}}@endif</td>
        <td >@if(
          $c->union != "" &&
          $c->union->upazila != "" &&
          $c->union->upazila->district != "") {{$c->union->upazila->district->distname}} @endif</td>
        <td >@if($c->union != "" && $c->union->upazila != ""){{$c->union->upazila->upname}}@endif</td>
        <td >@if($c->union != "" ){{$c->union->unname}} @endif</td>

        <td >{{$c->name}}</td>
        <td >{{$c->deg}}</td>
        <td >{{$c->UP_desg}}</td>
        <td >{{$c->phone}}</td>

      </tr>
      @endforeach
    </tbody>
  </table>
@endsection