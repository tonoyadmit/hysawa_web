<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">

    <div class="portlet-title">
          <div class="caption">
            <i class="fa fa-gift"></i>Filter Result
          </div>
          <div class="actions">
            <a href="{{route('superadmin.procurement.evaluations.download',$old)}}" class="btn btn-circle "> <i class="fa fa-download"></i> Download</a>
            &nbsp;&nbsp;
            <a href="{{route('superadmin.procurement.evaluations.print',$old)}}" class="btn btn-circle "> <i class="fa fa-print"></i> Print</a>

          </div>
        <div class="tools">
           {{--  <a href="javascript:;" class="collapse"> </a> --}}
        </div>
    </div>

    <div class="portlet-body util-btn-margin-bottom-5">

      <div class="table-responsive">

        <style>
          #example0 td, #example0 th{font-size: 10px;}
        </style>

        <table class="table table-bordered table-hover table-condensed" id="example0">

          <thead class="flip-content">
            <tr>
              <tr>
                <th class="text-center">Project</th>
                <th class="text-center">District</th>
                <th class="text-center">Upazila</th>
                <th class="text-center">Union</th>

                <th class="text-center">package</th>
                <th class="text-center">con_name</th>
                <th class="text-center">con_add</th>
                <th class="text-center">b_detail</th>

                <th class="text-center">amount</th>
                <th class="text-center">quate</th>
                <th class="text-center">quate_perc</th>
                <th class="text-center">rate</th>

                <th class="text-center">m_receipt</th>
                <th class="text-center">security</th>
                <th class="text-center">l_asset</th>
                <th class="text-center">signed</th>

                <th class="text-center">r_status</th>
                <th class="text-center">rank</th>
                <th class="text-center">noa</th>
                <th class="text-center">noa_date</th>

                <th class="text-center">con_date</th>
                <th class="text-center">con_status</th>
                <th class="text-center">remarks</th>

                <th class="text-center">created_by</th>
                <th class="text-center">created_at</th>
                <th class="text-center">updated_by</th>
                <th class="text-center">updated_at</th>

              </tr>
            </tr>
          </thead>

          <tbody>
            <?php $index = 1; ?>
            @foreach($lists as $c)
             <tr>
                <td class="text-left">@if($c->project!=""){{$c->project->project}}@endif</td>
                <td class="text-left">@if(
                  $c->union != "" &&
                  $c->union->upazila != "" &&
                  $c->union->upazila->district != "") {{$c->union->upazila->district->distname}} @endif</td>
                <td class="text-left">@if($c->union != "" && $c->union->upazila != ""){{$c->union->upazila->upname}}@endif</td>
                <td class="text-left">@if($c->union != "" ){{$c->union->unname}} @endif</td>


                <td class="text-left">{{ $c->package }}</td>
                <td class="text-left">{{ $c->con_name }}</td>
                <td class="text-left">{{ $c->con_add }}</td>
                <td class="text-left">{{ $c->b_detail }}</td>

                <td class="text-left">{{ $c->amount }}</td>
                <td class="text-left">{{ $c->quate }}</td>
                <td class="text-left">{{ $c->quate_perc }}</td>
                <td class="text-left">{{ $c->rate }}</td>

                <td class="text-left">{{ $c->m_receipt }}</td>
                <td class="text-left">{{ $c->security }}</td>
                <td class="text-left">{{ $c->l_asset }}</td>
                <td class="text-left">{{ $c->signed }}</td>

                <td class="text-left">{{ $c->r_status }}</td>
                <td class="text-left">{{ $c->rank }}</td>
                <td class="text-left">{{ $c->noa }}</td>
                <td class="text-left">{{ $c->noa_date }}</td>

                <td class="text-left">{{ $c->con_date }}</td>
                <td class="text-left">{{ $c->con_status }}</td>
                <td class="text-left">{{ $c->remarks }}</td>

                <td class="text-left">{{ $c->created_by }}</td>
                <td class="text-left">{{ $c->created_at }}</td>
                <td class="text-left">{{ $c->updated_by }}</td>
                <td class="text-left">{{ $c->updated_at }}</td>
              </tr>
            @endforeach
          </tbody>

        </table>

        <div class="pagination pull-right">
           {!! $lists->appends($old)->render() !!}
        </div>

      </div>

    </div>
  </div>
</div>