@extends('layouts.print.app')

@section('my_style')
<style>
    table
    {
      border-collapse: collapse;
      border-spacing: 0;
    }

    div{
      font-size: 80%;
      display: inline-block;
    }

    .headline th {
      padding: 5px 20px;
    }
  </style>
@endsection

@section('content')

<h2>Procurement List</h2>

<table class="box" border="1">
    <thead class="rotate">

      <th><div>Project</div></th>
      <th><div>District</div></th>
      <th><div>Upazila</div></th>
      <th><div>Union</div></th>

      <th><div>package</div></th>
      <th><div>con_name</div></th>
      <th><div>con_add</div></th>
      <th><div>b_detail</div></th>

      <th><div>amount</div></th>
      <th><div>quate</div></th>
      <th><div>quate_perc</div></th>
      <th><div>rate</div></th>

      <th><div>m_receipt</div></th>
      <th><div>security</div></th>
      <th><div>l_asset</div></th>
      <th><div>signed</div></th>

      <th><div>r_status</div></th>
      <th><div>rank</div></th>
      <th><div>noa</div></th>
      <th><div>noa_date</div></th>

      <th><div>con_date</div></th>
      <th><div>con_status</div></th>
      <th><div>remarks</div></th>

      <th><div>created_by</div></th>
      <th><div>created_at</div></th>
      <th><div>updated_by</div></th>
      <th><div>updated_at</div></th>


    </thead>

    <tbody>
      @foreach($lists as $c)
      <tr>
        <td >@if($c->project!=""){{$c->project->project}}@endif</td>
        <td >@if(
          $c->union != "" &&
          $c->union->upazila != "" &&
          $c->union->upazila->district != "") {{$c->union->upazila->district->distname}} @endif</td>
        <td >@if($c->union != "" && $c->union->upazila != ""){{$c->union->upazila->upname}}@endif</td>
        <td >@if($c->union != "" ){{$c->union->unname}} @endif</td>

        <td >{{ $c->package }}</td>
        <td >{{ $c->con_name }}</td>
        <td >{{ $c->con_add }}</td>
        <td >{{ $c->b_detail }}</td>

        <td >{{ $c->amount }}</td>
        <td >{{ $c->quate }}</td>
        <td >{{ $c->quate_perc }}</td>
        <td >{{ $c->rate }}</td>

        <td >{{ $c->m_receipt }}</td>
        <td >{{ $c->security }}</td>
        <td >{{ $c->l_asset }}</td>
        <td >{{ $c->signed }}</td>

        <td >{{ $c->r_status }}</td>
        <td >{{ $c->rank }}</td>
        <td >{{ $c->noa }}</td>
        <td >{{ $c->noa_date }}</td>

        <td >{{ $c->con_date }}</td>
        <td >{{ $c->con_status }}</td>
        <td >{{ $c->remarks }}</td>

        <td >{{ $c->created_by }}</td>
        <td >{{ $c->created_at }}</td>
        <td >{{ $c->updated_by }}</td>
        <td >{{ $c->updated_at }}</td>

      </tr>
      @endforeach
    </tbody>
  </table>
@endsection