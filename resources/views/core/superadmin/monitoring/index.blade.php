@extends('layouts.appinside')

@section('content')

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('superadmin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
         <li>
            <a href="#">Monitoring</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>UP Capacity</span>
        </li>
    </ul>
</div>

<h1 class="page-title"> Monitoring Report </h1>

<div class="portlet box grey">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>UP Capacity Development progress:
        </div>
        <div class="actions">
            <a href="{{route('superadmin.monitoring.upcapacity.index', ['download' => 'download'])}}" target="_blank" class="btn btn-circle success"> <i class="fa fa-download"></i> Download</a>
        </div>

        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
        </div>

        <div class="portlet-body">



            <div class="row">
                <div class="col-lg-6">
                    <table class="table table-bordered table-hover table-condensed table-stripped">
                        <thead>
                            <tr><th colspan="9"> <25% Score Report </th></tr>
                            <tr>
                                <th> Region Name</th>
                                <th> B</th>
                                <th> 1st</th>
                                <th> 2nd</th>
                                <th> 3rd</th>
                                <th> 4th</th>
                                <th> 5th</th>
                                <th> 6th</th>
                                <th> 7th</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($score25 as $scoredata)
                            <tr>
                                <td>{{$scoredata->region_name}}</td>
                                <td>{{$scoredata->B}}</td>
                                <td>{{$scoredata->B1}}</td>
                                <td>{{$scoredata->B2}}</td>
                                <td>{{$scoredata->B3}}</td>
                                <td>{{$scoredata->B4}}</td>
                                <td>{{$scoredata->B5}}</td>
                                <td>{{$scoredata->B6}}</td>
                                <td>{{$scoredata->B7}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-6">
                    <table class="table table-bordered table-hover table-condensed table-stripped">
                        <thead>
                            <tr><th colspan="9"> 25-50% Score Report </th></tr>
                            <tr>
                                <th> Region Name</th>
                                <th> B</th>
                                <th> 1st</th>
                                <th> 2nd</th>
                                <th> 3rd</th>
                                <th> 4th</th>
                                <th> 5th</th>
                                <th> 6th</th>
                                <th> 7th</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($score50 as $scoredata)
                            <tr>
                                <td>{{$scoredata->region_name}}</td>
                                <td>{{$scoredata->B}}</td>
                                <td>{{$scoredata->B1}}</td>
                                <td>{{$scoredata->B2}}</td>
                                <td>{{$scoredata->B3}}</td>
                                <td>{{$scoredata->B4}}</td>
                                <td>{{$scoredata->B5}}</td>
                                <td>{{$scoredata->B6}}</td>
                                <td>{{$scoredata->B7}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-6">

                    <table class="table table-bordered table-hover table-condensed table-stripped">
                        <thead>
                            <tr><th colspan="9"> 50-75% </th></tr>
                            <tr>
                                <th> Region Name</th>
                                <th> B</th>
                                <th> 1st</th>
                                <th> 2nd</th>
                                <th> 3rd</th>
                                <th> 4th</th>
                                <th> 5th</th>
                                <th> 6th</th>
                                <th> 7th</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($score217 as $scoredata)
                            <tr>
                                <td>{{$scoredata->region_name}}</td>
                                <td>{{$scoredata->B}}</td>
                                <td>{{$scoredata->B1}}</td>
                                <td>{{$scoredata->B2}}</td>
                                <td>{{$scoredata->B3}}</td>
                                <td>{{$scoredata->B4}}</td>
                                <td>{{$scoredata->B5}}</td>
                                <td>{{$scoredata->B6}}</td>
                                <td>{{$scoredata->B7}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="col-lg-6">

                    <table class="table table-bordered table-hover table-condensed table-stripped">
                        <thead>
                            <tr><th colspan="9"> More Than 75% </th></tr>
                            <tr>
                                <th> Region Name</th>
                                <th> B</th>
                                <th> 1st</th>
                                <th> 2nd</th>
                                <th> 3rd</th>
                                <th> 4th</th>
                                <th> 5th</th>
                                <th> 6th</th>
                                <th> 7th</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($score75 as $scoredata)
                            <tr>
                                <td>{{$scoredata->region_name}}</td>
                                <td>{{$scoredata->B}}</td>
                                <td>{{$scoredata->B1}}</td>
                                <td>{{$scoredata->B2}}</td>
                                <td>{{$scoredata->B3}}</td>
                                <td>{{$scoredata->B4}}</td>
                                <td>{{$scoredata->B5}}</td>
                                <td>{{$scoredata->B6}}</td>
                                <td>{{$scoredata->B7}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-condensed table-stripped">
                            <thead>
                                <tr>
                                    <td><div align="left">District</div></td>
                                    <td><div align="left">Upazila</div></td>
                                    <td><div align="left">Union</div></td>
                                    <td><div align="left">Period</div></td>
                                    <td><div align="center">Total score out of 290</div></td>
                                    <td><p>Financial Management (Total  score out of 50)</p></td>
                                    <td>Procurement (Total  score out of 20)</td>
                                    <td>Program Management (Total  score out of 85)</td>
                                    <td>Institutional Management (Total  score out of 45)</td>
                                    <td>Office Management (Total  score out of 75)</td>
                                    <td>Resource Mobilisation  (Total  score out of 15)</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($uniondatas as $uniondata)
                                <?php

                                $total=$uniondata->finance+$uniondata->procurement+$uniondata->program+$uniondata->admin+$uniondata->offmgt+$uniondata->resource;

                                ?>
                                <tr>
                                    <td>{{$uniondata->distname}}</td>
                                    <td>{{$uniondata->upname}}</td>
                                    <td>{{$uniondata->unname}}</td>
                                    <td>{{$uniondata->querter}}</td>
                                    <td>{{ $total }}</td>
                                    <td>{{$uniondata->finance}}</td>
                                    <td>{{$uniondata->procurement}}</td>
                                    <td>{{$uniondata->program}}</td>
                                    <td>{{$uniondata->admin}}</td>
                                    <td>{{$uniondata->offmgt }}</td>
                                    <td>{{$uniondata->resource }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>



    @endsection
