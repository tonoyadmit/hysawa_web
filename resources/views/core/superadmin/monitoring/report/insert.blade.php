@extends('layouts.appinside')
@section('content')
<link href="{{ URL::asset('/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css') }}" rel="stylesheet" type="text/css" />

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('superadmin.dashboard') }}">Home:</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Monitoring</span>
            <i class="fa fa-circle"></i>
        </li>
         <li>
            <span>Field Visit</span>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('superadmin.monitoring.upcapacity.reportlist.index') }}">Report:</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Create</span>
        </li>
    </ul>
</div>


<h1 class="page-title"> Insert New Report:
    <small>create new</small>
</h1>

{!! Form::open(array('route' => 'superadmin.monitoring.upcapacity.reportlist.store', 'method' => 'post' , 'enctype'=>'multipart/form-data', 'class' => "form-horizontal")) !!}

    <div class="form-body">
        @include('partials.errors')

        <div class="form-group">
            <label class="control-label col-md-3 ">District</label>
            <div class="col-md-4">
                {!! Form::select('distid', array(''=>'Select District')+$district, null, ['class' => 'form-control','id' => 'district_id']) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 ">Upzilla</label>
            <div class="col-md-4">
                {!! Form::select('upid', array(''=>'Select Upzilla'), null, ['class' => 'form-control','id'=>'upazila_id']) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3 ">Union</label>
            <div class="col-md-4">
                {!! Form::select('unid', array(''=>'Select Union'), null, ['class' => 'form-control','id' =>'union_id']) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 ">Name of Visitor:</label>
            <div class="col-md-4">
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => '',]) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3 ">Designation of visitor:</label>
            <div class="col-md-4">
                {!! Form::text('des', null, ['class' => 'form-control', 'placeholder' => '',]) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3">Date of Visi(From):</label>
            <div class="col-md-4">
                {!! Form::date('fdate', null, ['class' => 'form-control date-picker', 'placeholder' => '',]) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Date of Visi(TO):</label>
            <div class="col-md-4">
                {!! Form::date('tdate', null, ['class' => 'form-control date-picker', 'placeholder' => '',]) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3">Place of Visit:</label>
            <div class="col-md-9">
                {!! Form::text('place', null, ['class' => 'form-control', 'placeholder' => '',]) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Pupose / Objective(s):</label>
            <div class="col-md-9">
                {!! Form::textarea('porpose', null, ['class' => 'wysihtml5  form-control', 'placeholder' => '',]) !!}
            </div>

        </div>

        <div class="form-group">
            <label class="control-label col-md-3">Findings / Observation(s) </label>
            <div class="col-md-9">
                {!! Form::textarea('findings', null, ['class' => 'wysihtml5 form-control', 'placeholder' => '',]) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3">People Person met</label>
            <div class="col-md-9">
                {!! Form::textarea('peoplemet', null, ['class' => 'wysihtml5 form-control', 'placeholder' => '',]) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Agreed Action Points / Recommendation(s):</label>
            <div class="col-md-9">
                {!! Form::textarea('recomandation', null, ['class' => 'wysihtml5 form-control', 'placeholder' => '',]) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Follow-up actions</label>
            <div class="col-md-9">
                {!! Form::textarea('followup', null, ['class' => 'wysihtml5 form-control', 'placeholder' => '']) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Annex (if any):</label>
            <div class="col-md-9">
                {!! Form::file('anex', null, ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>

    <div class="form-action">
        <div class="row ">
            <div class="col-md-offset-3">
                <a href="javascript:history.back()" class="btn default"> Cancel </a>
                {!! Form::submit('Save', ['class' => 'btn green']) !!}
            </div>
        </div>
    </div>

</div>

{!! Form::close() !!}
@endsection

@section('my_js')
<script type="text/javascript">
    $(document).ready(function () {
    highlight_nav('warehouse-manage', 'shelfs');
    });</script>

<script type="text/javascript" language="javascript" >

$(document).ready(function () {
    $('#district_id').on('change', function() {
        var disid = $(this).val();
        var csrftoken = $("#csrf-token").val();
        if (disid == ''){
            $('#upazila_id').attr('disabled', 'disabled');
        } else{
            $.getJSON('{{ route('training-agency.getUpzilla') }}?disid=' + disid + '&_token=' + csrftoken, function (data) {
                $('select[name="upid"]').empty();
                $.each(data, function(key, value) {
                    $('select[name="upid"]').append('<option value="' + key + '">' + value + '</option>');
                });
            });
        }
    });

    $('#upazila_id').on('change', function() {
        var upid = $(this).val();
        var disid = $("#district_id").val();
        var csrftoken = $("#csrf-token").val();
        if (upid == ''){
            $('#union_id').attr('disabled', 'disabled');
        } else{
            $.getJSON('{{route('training-agency.getUnion')}}?disid=' + disid + '&_token=' + csrftoken + '&upid=' + upid, function (data) {
                $('select[name="unid"]').empty();
                $.each(data, function(key, value) {
                    $('select[name="unid"]').append('<option value="' + key + '">' + value + '</option>');
                });
            });
        }
    });
});
</script>
<script src="{{ URL::asset('/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js') }}" type="text/javascript"></script>
<script src="{{ URL::asset('/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js') }}" type="text/javascript"></script>

 <script src="{{ URL::asset('/assets/pages/scripts/components-editors.min.js') }}" type="text/javascript"></script>
@endsection

