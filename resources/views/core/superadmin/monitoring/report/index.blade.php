@extends('layouts.appinside')

@section('content')

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('superadmin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Monitoring</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="#">Field Visit</a>
            <i class="fa fa-circle"></i>
        </li>
        <li><span>Report List</span></li>
    </ul>
</div>

<div class="portlet box grey" style="margin-top: 15px;">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Report List
        </div>
        <div class="actions">
            <a href="{{route('superadmin.monitoring.upcapacity.reportlist.index', ['download' => 'download'])}}" target="_blank" class="btn btn-circle success"> <i class="fa fa-download"></i> Download</a>
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body">
        <div class="caption">
            <h1></h1>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <table class="table table-bordered table-hover table-condensed table-stripped">
                    <thead>

                        <tr>
                            <th> Report prepared by</th>
                            <th> Date</th>
                            <th> Visited Area</th>

                        </tr>
                    </thead>

                    <tbody>
                         @foreach($tours as $toursdata)
                         <tr>
                            <td nowrap="nowrap"><a href="reportlist/{{ $toursdata->id }}">{{$toursdata->name}}</a></td>
                            <td nowrap="nowrap">From {{$toursdata->fdate }}-{{ $toursdata->tdate }}</td>
                            <td>{{$toursdata->place}}</td>
                         </tr>
                         @endforeach
                    </tbody>
                </table>
            </div>


        </div>



    </div>
</div>



@endsection
