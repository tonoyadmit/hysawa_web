@extends('layouts.appinside')

@section('content')
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('superadmin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>View History</span>
        </li>
    </ul>
</div>

<h1 class="page-title"> Visitor
    <small> View History</small>
</h1>

<div class="row">
    <div class="col-md-12">
        <div class="profile-sidebar">
            <div class="portlet light ">
            </div>

            <div class="portlet-body">
                <table  class="col-lg-12" border="1" cellpadding="3" cellspacing="0"  align="center" >
                    <tr>
                        <td width="163"><p>Name of Visitor
                            <input name="id" type="hidden" value="{{ $info->id }}"/>
                        </p></td>
                        <td width="556">{{ $info->name }}</td>
                    </tr>
                    <tr>
                        <td><p>Designation of visitor</p></td>
                        <td>{{ $info->des }}</td>
                    </tr>
                    <tr>
                        <td>Date of Visit:</td>
                        <td>From
                            {{ $info->fdate }}
                            To
                            {{ $info->tdate }}</td>
                        </tr>
                        <tr>
                            <td>Place of Visit</td>
                            <td>{{ $info->place }}</td>
                        </tr>
                        <tr>
                            <td>Pupose/Objective(s)</td>
                            <td>{!! $info->porpose !!}</td>
                        </tr>
                        <tr>
                            <td>Findings/Observation(s) </td>
                            <td>{!! $info->findings !!}</td>
                        </tr>
                        <tr>
                            <td><p>People Person met</p></td>
                            <td>{!! $info->peoplemet !!}</td>
                        </tr>
                        <tr>
                            <td><p>Agreed Action Points/Recommendation(s)</p></td>
                            <td>{!! $info->recomandation !!}</td>
                        </tr>
                        <tr>
                            <td><p>Follow-up actions</p></td>
                            <td>{!! $info->followup !!}</td>
                        </tr>
                        <tr>
                            <td>Supervisor's Feedback</td>
                            <td>
                                {{ $info->feedback }}
                            </td>
                        </tr>
                        <tr>
                            <td><p>Annex (if any):</p></td>
                            <td>{{ $info->anext }}</td>
                        </tr>
                        <tr>
                            <td><p>Signature and Date</p></td>
                            <td>{{ $info->sign }}</td>
                        </tr>
                    </table>
                </div>
            </div>

        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
highlight_nav('user-manage', 'users');
});
</script>

@endsection
