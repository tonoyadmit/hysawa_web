@extends('layouts.appinside')

@section('content')

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('superadmin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('superadmin.pngo.index') }}">PNGO:</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Update</span>
        </li>
    </ul>
</div>

<h1 class="page-title"> Update New PNGO:
    <small>create new</small>
</h1>

{!! Form::model($pngolists, [ 'route' => ['superadmin.monitoring.upcapacity.reportlist.update', $pngolists->id] , 'method' => 'put']) !!}
<div class="row">

    @include('partials.errors')

    <div class="col-md-3">
    </div>

    <div class="col-md-6">

        <div class="form-group">
            <label class="control-label col-md-12 np-lr">District</label>
            {!! Form::select('distid', array(''=>'Select District')+$district, null, ['class' => 'form-control','id' => 'district_id']) !!}
        </div>
        <div class="form-group">
            <label class="control-label col-md-12 np-lr">Upzilla</label>
            {!! Form::select('upid', array(''=>'Select Upzilla')+$upnamelist, $pngolists->upid, ['class' => 'form-control','id'=>'upazila_id']) !!}
        </div>
        <div class="form-group">
            <label class="control-label col-md-12 np-lr">Union</label>
            {!! Form::select('unid', array(''=>'Select Union')+$unnamelist, $pngolists->unid, ['class' => 'form-control','id' =>'union_id']) !!}
        </div>

        <div class="form-group">
            <label class="control-label col-md-12 np-lr">NGO Name:</label>
            {!! Form::text('ngoname', null, ['class' => 'form-control', 'placeholder' => 'NGO Name',]) !!}
        </div>

        <div class="form-group">
            <label class="control-label col-md-12 np-lr">Address:</label>
            {!! Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Address',]) !!}
        </div>

        <div class="form-group">
            <label class="control-label">Executive Director/Head of NGO:</label>
            {!! Form::text('edname', null, ['class' => 'form-control', 'placeholder' => 'Executive Director/Head of NGO',]) !!}
        </div>

        <div class="form-group">
            <label class="control-label">Mobile number:</label>
            {!! Form::text('edmobile', null, ['class' => 'form-control', 'placeholder' => 'Mobile number',]) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Email:</label>
            {!! Form::text('edemail', null, ['class' => 'form-control', 'placeholder' => 'Email',]) !!}
        </div>

        <div class="form-group">
            <label class="control-label">Contact person:</label>
            {!! Form::text('ContactPerson', null, ['class' => 'form-control', 'placeholder' => 'Contact person',]) !!}
        </div>

        <div class="form-group">
            <label class="control-label">Mobile:</label>
            {!! Form::text('contactmobile', null, ['class' => 'form-control', 'placeholder' => 'Mobile',]) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Email:</label>

            {!! Form::text('contactemail', null, ['class' => 'form-control', 'placeholder' => 'Email',]) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Contract date:</label>
            {!! Form::DATE('contractdate', null, ['class' => 'form-control', 'placeholder' => 'Contract date']) !!}
        </div>

        <div class="form-group">
            <label class="control-label">Remarks:</label>
            {!! Form::text('remarks', null, ['class' => 'form-control', 'placeholder' => 'Remarks']) !!}
        </div>
    </div>
    <div class="col-md-3">
    </div>
</div>
&nbsp;
<div class="row padding-top-10">
    <a href="javascript:history.back()" class="btn default"> Cancel </a>
    {!! Form::submit('Update', ['class' => 'btn green pull-right']) !!}
</div>
{!! Form::close() !!}
</div>

<script type="text/javascript">
    $(document).ready(function () {
        // Navigation Highlight
        highlight_nav('warehouse-manage', 'shelfs');
    });
</script>
<script type="text/javascript" language="javascript" >

            $(document).ready(function () {
    $('#district_id').on('change', function() {
    var disid = $(this).val();
            var csrftoken = $("#csrf-token").val();
            if (disid == ''){
    $('#upazila_id').attr('disabled', 'disabled');
    } else{
    $.getJSON('{{ route('training-agency.getUpzilla') }}?disid=' + disid + '&_token=' + csrftoken, function (data) {
    $('select[name="upid"]').empty();
            $.each(data, function(key, value) {
            $('select[name="upid"]').append('<option value="' + key + '">' + value + '</option>');
            });
    });
    }
    });
            $('#upazila_id').on('change', function() {
    var upid = $(this).val();
            var disid = $("#district_id").val();
            var csrftoken = $("#csrf-token").val();
            if (upid == ''){
    $('#union_id').attr('disabled', 'disabled');
    } else{
    $.getJSON('{{ route('training-agency.getUnion') }}?disid=' + disid + '&_token=' + csrftoken + '&upid=' + upid, function (data) {
    $('select[name="unid"]').empty();
            $.each(data, function(key, value) {
            $('select[name="unid"]').append('<option value="' + key + '">' + value + '</option>');
            });
    });
    }
    });
    });

</script>
@endsection
