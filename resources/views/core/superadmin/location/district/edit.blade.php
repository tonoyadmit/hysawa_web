@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{route('superadmin.dashboard')}}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="{{route('superadmin.location.district.index')}}">District</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Update Info</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

<div class="col-md-12" style="margin-top: 50px;">
  <form
    action="{{route('superadmin.location.district.update', $district->id)}}"
    class="form-horizontal"
    method="POST"
    id="createForm">

    {{csrf_field()}}

    <input type="hidden" name="query" value="{{time()}}" />

    <div class="form-body">

      <div class="form-group">
        <label class="control-label col-md-3">Region</label>

        <div class="col-md-6">
          <select class="form-control input-sm" name="region_id">
            <option value="">Select Region</option>
            <?php $regions = App\Model\Region::orderBy('region_name')->get(); ?>
            @if($regions != "")
              @foreach($regions as $region)
                <option value="{{$region->id}}"

                @if($district->region_id == $region->id)
                  selected="selected"
                @endif

                >{{$region->region_name}}</option>
              @endforeach
            @endif
          </select>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">District *</label>
        <div class="col-md-6">
          <input type="text" name="distname" class="form-control" required="required" value="{{$district->distname}}" required="required" />
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">District Code</label>
        <div class="col-md-6">
          <input type="text" name="distcode" class="form-control" value="{{$district->distcode}}" />
        </div>
      </div>

    </div>

    <div class="form-actions">
      <div class="col-md-offset-3">
      <input type="submit" class="btn green btn-sm" value="Update District" />
      </div>
    </div>
  </form>
</div>

@endsection

