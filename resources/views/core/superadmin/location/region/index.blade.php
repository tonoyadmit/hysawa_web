@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>

      <li>
        <span>Region</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

    @if(count($regions) > 0)

        <div class="col-md-6">
            <div class="portlet light tasks-widget bordered">
                <div class="portlet-body util-btn-margin-bottom-5">
                    <table class="table table-bordered table-hover" id="example0">
                        <thead class="flip-content">
                            <th width="10%">Action</th>
                            <th>Title</th>
                        </thead>
                        <tbody>
                            @foreach($regions as $p)
                                <tr>
                                    <td>
                                      <a class="label label-success" href="{{route('superadmin.location.region.edit', $p->id)}}"><i class="fa fa-pencil"></i></a>
                                    </td>
                                    <td>{{ $p->region_name }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="pagination pull-right">
                      {{$regions->links()}}
                    </div>
                </div>
            </div>
        </div>
    @else
      <div class="col-md-6">
        <div class="portlet light tasks-widget bordered">
           <p>
              No Data Found
           </p>
        </div>
      </div>
    @endif

    <div class="col-md-6">
      <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>Add New </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>

        <div class="portlet-body form">
            <form action="{{route('superadmin.location.region.store')}}" class="form-horizontal" method="POST" id="createForm">

                {{csrf_field()}}

                <input type="hidden" name="query" value="{{time()}}" />

                <div class="form-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Title</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="region_name"
                                    @if(!empty( $old['region_name']))
                                        value="{{$old['region_name']}}"
                                    @endif required="required">
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                      <div class="form-group">
                          <div class="col-md-offset-3 col-md-9">
                              <input type="submit" class="btn green" value="Add New Region" />
                          </div>
                      </div>
                    </div>
                </div>
            </form>

        </div>
      </div>
    </div>
@endsection
