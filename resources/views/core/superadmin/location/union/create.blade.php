@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{route('superadmin.dashboard')}}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="{{route('superadmin.location.union.index')}}">Union</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Union</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

<div class="col-md-12" style="margin-top: 50px;">
  <form
    action="{{route('superadmin.location.union.store')}}"
    class="form-horizontal"
    method="POST"
    id="createForm">

    {{csrf_field()}}

    <input type="hidden" name="query" value="{{time()}}" />

    <div class="form-body">


      <div class="form-group">
        <label class="control-label col-md-3">Project</label>

        <div class="col-md-6">
          <select class="form-control input-sm" name="project_id">
            <option value="">Select Project</option>
            <?php $projects = App\Model\Project::orderBy('project')->get(); ?>
            @if($projects != "")
              @foreach($projects as $project)
                <option value="{{$project->id}}">{{$project->project}}</option>
              @endforeach
            @endif
          </select>
        </div>
      </div>


      <div class="form-group">
        <label class="control-label col-md-3">Region</label>

        <div class="col-md-6">
          <select class="form-control input-sm" name="region_id" id="regionId">
            <option value="">Select Region</option>
            <?php $regions = App\Model\Region::orderBy('region_name')->get(); ?>
            @if($regions != "")
              @foreach($regions as $region)
                <option value="{{$region->id}}">{{$region->region_name}}</option>
              @endforeach
            @endif
          </select>
        </div>
      </div>


      <div class="form-group">
        <label class="control-label col-md-3">District</label>

        <div class="col-md-6">
          <select class="form-control input-sm" id="district_id" name="district_id">
            <option value="">Select District</option>
            <?php $districts = App\Model\District::orderBy('distname')->get(); ?>
            @foreach( $districts as $district)
              <option value="{{$district->id}}">{{$district->distname}}</option>
            @endforeach
          </select>
        </div>
      </div>



      <div class="form-group">
        <label class="control-label col-md-3 ">Upazila</label>

        <div class="col-md-6">
          <select class="form-control input-sm" id="upazila_id" name="upazila_id">
            <option value="">Select Upazila</option>
          </select>
        </div>
      </div>


      <div class="form-group">
        <label class="control-label col-md-3">Union</label>
        <div class="col-md-6">
          <input type="text" name="unname" class="form-control" />
        </div>
      </div>
    </div>

    <div class="form-actions">
      <div class="col-md-offset-3">
      <input type="submit" class="btn green btn-sm" value="Add New Union" />
      </div>
    </div>
  </form>
</div>



@endsection

@section('my_js')
  <script type="text/javascript">
    $(document).ready(function () {

      $('#regionId').on('change', function() {
        var region_id = $(this).val();
        $("#district_id").html("Select Region");
        $.ajax({
          url: '{{route('superadmin.ajax.district')}}?region_id='+region_id+'&not_all=true&choose_one=true',
          type: 'GET',
          success: function(response){
            console.log(response);
            if(response['status'] == true){
              $("#district_id").html(response['district_list']);
            }
          }
        });
      });



      $('#district_id').on('change', function() {

        var district_id = $(this).val();

        $("#upazila_id").html("Select District");

        $.ajax({
          url: '{{route('superadmin.ajax.upazila')}}?district_id='+district_id+'&not_all=true&choose_one=true',
          type: 'GET',
          success: function(response){
            if(response['status'] == true){
              $("#upazila_id").html(response['upazila_list']);
            }
          }
        });
      });
    });
  </script>
@endsection
