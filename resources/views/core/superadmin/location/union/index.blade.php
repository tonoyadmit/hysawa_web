@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{route('superadmin.dashboard')}}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Location</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Union</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

<div class="col-md-12" style="border-width: 1px; border-style: solid;">
  <form
    action="{{route('superadmin.location.union.index')}}"
    class="form-horizontal"
    method="GET"
    id="createForm">

    <input type="hidden" name="query" value="{{time()}}" />

    <div class="form-body">

      <div class="row">

        <div class="col-md-2">
          <label class="control-label">Union Name</label>
          <input type="text" class="form-control input-sm" name="unname" value="{{$old['unname'] or ''}}" />
        </div>

        <div class="col-md-2">
          <label class="control-label">Project</label>
          <select class="form-control input-sm" name="project_id">
            <option value="">Select Project</option>
            <?php $projects = App\Model\Project::orderBy('project')->get(); ?>
            @if($projects != "")
              @foreach($projects as $project)
                <option value="{{$project->id}}"
                  @if($old['project_id'] == $project->id) selected="selected" @endif
                >{{$project->project}}</option>
              @endforeach
            @endif
          </select>
        </div>

        <div class="col-md-2">

          <label class="control-label">Region</label>
          <select class="form-control input-sm" name="region_id">
            <option value="">Select Region</option>
            <?php
              $regions = App\Model\Region::orderBy('region_name')->get();
            ?>
            @if($regions != "")
              @foreach($regions as $region)
                <option value="{{$region->id}}"
                  @if($old['region_id'] == $region->id)
                    selected="selected"
                  @endif
                  >{{$region->region_name}}</option>
              @endforeach
            @endif
            </select>
        </div>

        <div class="col-md-2">

          <label class="control-label">District</label>
          <select class="form-control input-sm" id="district_id" name="district_id">
            <option value="">Select District</option>
            <?php
            $districts = App\Model\District::orderBy('distname')->get();
            ?>
            @foreach( $districts as $district)
            <option value="{{$district->id}}"
              @if($old['district_id'] ==$district->distname)
              selected="selected"
              @endif
              >{{$district->distname}}</option>
              @endforeach


            </select>
        </div>

        <div class="col-md-2">

          <label class="control-label">Upazila</label>
          <select class="form-control input-sm" id="upazila_id" name="upazila_id">
            <option value="">Select Upazila</option>
            @if(!empty($old) && !empty($old['district_id']))
            <?php
              $upazilas = App\Model\Upazila::where('disid', $old['district_id'])->orderBy('upname')->get();
            ?>

            @foreach( $upazilas as $upazila)
            <option value="{{$upazila->id}}"
              @if($old['upazila_id'] == $upazila->id)
              selected="selected"
              @endif
              >{{$upazila->upname}}</option>
              @endforeach
              @endif
            </select>
        </div>

        <div class="col-md-2" style="margin-top: 23px;">
          <input type="submit" class="btn green btn-sm" value="Filter" />
          <a href="{{route('superadmin.location.union.index')}}" class="btn btn-info btn-sm">Clear</a>

          <a href="{{route('superadmin.location.union.create')}}" class="btn btn-primary btn-sm">Add New</a>
        </div>

      </div>

    </div>
  </form>
</div>

<div class="portlet light tasks-widget bordered">

  <div class="portlet-title">
    <div class="caption">List of Unions</div>
  </div>

  <div class="portlet-body util-btn-margin-bottom-5">
    <div class="table-responsive">

      <table class="table table-bordered table-hover table-condensed" id="example0">
        <thead class="flip-content">
            <tr>
              <th>Action</th>
              <th>Project</th>
              <th>Region</th>
              <th>District</th>
              <th>Upazila</th>
              <th>Union</th>
            </tr>
        </thead>

        <tbody>
          @foreach($datas as $d)
            <tr>
              <td class="form-actions"><a href="{{route('superadmin.location.union.edit', $d->id)}}" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
              </td>

              <td>{{ $d->project->project or ''}}</td>
              <td>{{ $d->region->region_name or ''}}</td>
              <td>{{ $d->upazila->district->distname or ''}}</td>
              <td>{{ $d->upazila->upname or '' }}</td>
              <td>{{ $d->unname or ''}}</td>
            </tr>
          @endforeach

        </tbody>
      </table>

      {{$datas->appends($old)->links()}}

    </div>
  </div>
</div>

@endsection

@section('my_js')
  <script type="text/javascript">
    $(document).ready(function () {
      $('#district_id').on('change', function() {
        var head_id = $(this).val();

        $("#upazila_id").html("Select District");
        $("#union_id").html("Select Upazila");

        $.ajax({
          url: '{{route('superadmin.ajax.upazila')}}?district_id='+head_id+'&not_all=true&choose_one=true',
          type: 'GET',
          success: function(response){
            if(response['status'] == true){
              $("#upazila_id").html(response['upazila_list']);
            }
          }
        });
      });

      $('#upazila_id').on('change', function() {

        var upazila_id = $(this).val();
        $("#union_id").html("Select Upazila");

        $.ajax({
          url: '{{route('superadmin.ajax.union')}}?upazila_id='+upazila_id+'&not_all=true&choose_one=true',
          type: 'GET',
          success: function(response){
            if(response['status'] == true){
              $("#union_id").html(response['union_list']);
            }
          }
        });
      });
    });
  </script>
@endsection
