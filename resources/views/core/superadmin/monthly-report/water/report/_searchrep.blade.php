<div class="portlet box blue">

    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Monthly Report <small> view</small> </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body form">
        <form action="{{route('superadmin.monthly-report.report.monthlyrep')}}" class="form-horizontal" method="GET" id="searchForm">

            <input type="hidden" name="query" value="{{time()}}" />

            <div class="form-body">
                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Project</label>
                            <div class="col-md-9">


                                <?php

                                    $projects = App\Model\Project::all();

                                ?>
                                <select class="form-control" name="project_id">
                                    <option value="">Choose a Project</option>
                                    @foreach($projects as $project )
                                        <option value="{{$project->id}}"

                                        @if(isset($old['project_id']) && $old['project_id'] == $project->id)
                                            selected="selected"
                                        @endif


                                        >{{$project->project}}</option>
                                    @endforeach
                                </select>

                               
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Reporting Period</label>
                            <div class="col-md-9">
                                <select class="form-control" name="rep_data_id" required="required" id="rep_data_id">
                                    <option value="">Choose Union First</option>
                                    <option value="all" selected="selected">ALL</option>
                                    <?php
                                        $periods = \DB::table('rep_period')
                                                        ->where('id', '!=', 99)
                                                        ->where('id', '!=', 100)
                                                        ->orderBy('id', 'DESC')
                                                        ->get();
                                    ?>
                                    @foreach($periods as $period)
                                        <option value="{{$period->id}}"
                                            @if(isset($_REQUEST['rep_data_id']) && $_REQUEST['rep_data_id'] == $period->id)
                                                selected="selected"
                                            @endif
                                        >{{$period->period}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>



                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-5">
                                <input type="submit" class="btn green" value="Search" />
                                 <a href="{{route('superadmin.monthly-report.report.monthlyrep')}}" class="btn default">Clear</a> 

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </form>

    </div>
</div>