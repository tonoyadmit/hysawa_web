<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Monthly Report <small> view</small> </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body form">
        <form action="{{route('superadmin.monthly-report.report.show')}}" class="form-horizontal" method="GET" id="searchForm">

            <input type="hidden" name="query" value="{{time()}}" />


                <div class="row">


 <div class="col-md-3">
       
       <div class="form-group">
                          <label class="control-label col-md-3">Project</label>
                          <div class="col-md-9">

        <select class="form-control input-sm" name="project_id" id="project_id">

          <option value="all" @if(isset($_REQUEST['project_id']) && $_REQUEST['project_id'] == "all" ) selected="selected" @endif >All</option>
          <?php
        $projects = App\Model\Project::all();
          ?>
          @if(count($projects))
          @foreach($projects as $project)
          <option value="{{$project->id}}"
            @if(isset($_REQUEST['project_id']) && $_REQUEST['project_id'] == $project->id)
            selected="selected"
            @endif
            >{{$project->project}}</option>
            @endforeach
            @endif
          </select>
          </div>
        </div>
        </div>



        <div class="col-md-3">
       
         <div class="form-group">
                            <label class="control-label col-md-3">Region</label>
                            <div class="col-md-9">

          <select class="form-control input-sm" name="region_id" id="region_id">

            <option value="all" @if(isset($_REQUEST['region_id']) && $_REQUEST['region_id'] == "all" ) selected="selected" @endif >All</option>
            <?php
            $regions = App\Model\Region::orderBy('region_name')->get();
            ?>
            @if(count($regions))
            @foreach($regions as $region)
            <option value="{{$region->id}}"
              @if(isset($_REQUEST['region_id']) && $_REQUEST['region_id'] == $region->id)
              selected="selected"
              @endif
              >{{$region->region_name}}</option>
              @endforeach
              @endif
            </select>
            </div>
          </div>
          </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">District</label>
                            <div class="col-md-9">

                                <select class="form-control input-sm" name="district_id" id="district_id">
                                    <option value="all" selected="selected">All</option>

                                    @foreach($districts as $district )
                                        <option value="{{$district->id}}"


                                        @if(isset($_REQUEST['district_id']) && $district->id == $_REQUEST['district_id'])
                                            selected="selected"
                                        @endif



                                        >{{$district->distname}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Upazilas</label>
                            <div class="col-md-9">
                                <select class="form-control input-sm" name="upazila_id" id="upazila_id">

                                    @if(isset($_REQUEST['district_id']) && $_REQUEST['district_id'] != "")

                                        @if($_REQUEST['district_id'] != "all")
                                            <?php
                                            $upazilas = App\Model\Upazila::where('disid', $_REQUEST['district_id'])->get();
                                            ?>

                                            <option value="">Choose an Option</option>
                                            <option value="all" @if(isset($_REQUEST['upazila_id']) && $_REQUEST['upazila_id'] == "all") selected="selected" @endif >All</option>

                                            @foreach($upazilas as $upazila)
                                                <option value="{{$upazila->id}}"
                                                    @if(isset($_REQUEST['upazila_id']) && $_REQUEST['upazila_id'] == $upazila->id)
                                                        selected="selected"
                                                    @endif
                                                >{{$upazila->upname}}</option>
                                            @endforeach
                                        @endif

                                    @else
                                        <option value="">Choose District First</option>
                                    @endif

                                </select>
                            </div>
                        </div>
                    </div>

                          </div>

     

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Union </label>
                            <div class="col-md-9">
                                <select class="form-control input-sm" name="union_id" id="union_id">


                                    @if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] != "")

                                        @if($_REQUEST['upazila_id'] != "all")
                                            <?php
                                            $unions = App\Model\Union::where('upid', $_REQUEST['upazila_id'])->get();
                                            ?>

                                            <option value="">Choose an Option</option>
                                            <option value="all" @if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] == "all") selected="selected" @endif >All</option>

                                            @foreach($unions as $union)
                                                <option value="{{$union->id}}"
                                                    @if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] == $union->id)
                                                        selected="selected"
                                                    @endif
                                                >{{$union->unname}}</option>
                                            @endforeach
                                        @endif

                                    @else
                                        <option value="">Choose Upazila First</option>
                                    @endif

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Reporting Period</label>
                            <div class="col-md-9">
                                <select class="form-control input-sm" name="rep_data_id" required="required" id="rep_data_id">

                                    <option value="all" selected="selected">ALL</option>
                                    <?php
                                        $periods = \DB::table('rep_period')->where('id', '!=', 99)->where('id', '!=', 100)->orderBy('id', 'DESC')->get();
                                    ?>
                                    @foreach($periods as $period)
                                        <option value="{{$period->id}}"

                                        @if(isset($_REQUEST['rep_data_id']) && $_REQUEST['rep_data_id'] == $period->id) selected="selected" @endif

                                        >{{$period->period}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Type</label>
                            <div class="col-md-9">
                                <select name="report_type" class="form-control input-sm" required="required">

                                @if(isset($_REQUEST['report_type']) && $_REQUEST['report_type'] == "")
                                    <option value="html" @if($_REQUEST['report_type'] == "html") selected="selected" @endif >Generate Report</option>
                                    <option value="print" @if($_REQUEST['report_type'] == "print") selected="selected" @endif>Print</option>
                                @else
                                    <option value="html" selected="selected">Generate Report</option>
                                    <option value="print">Print</option>
                                @endif
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <input type="submit" class="btn green" value="Search" />
                        <a href="{{route('superadmin.monthly-report.report.index')}}" class="btn default">Clear</a>
                        <a href="{{route('superadmin.monthly-report.report.monthlyrep')}}" class="btn default">Report</a>
                    </div>

            </div>

            @if(isset($_REQUEST['query']))
            <div class="row">
                <div class="col-md-12">




                </div>
            </div>
            @endif


        </form>

    </div>
</div>