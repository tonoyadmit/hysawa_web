@extends('layouts.appinside')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('up-admin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Finance</span>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Demand</span>
        </li>
    </ul>
</div>

<div class="portlet light bordered" style="margin-top: 10p x;">

    <div class="portlet-title">
        <div class="caption">
            <i class="icon-share font-dark"></i>
            <span class="caption-subject font-dark bold uppercase">Finance Demand</span>
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
    </div>

    <div class="portlet-body">

        {{--  <p>
            <a href="{{route('superadmin.finance.up-demand.finace_demand.create', 'fundlistid='.app('request')->input('fid'))}}" class="btn btn-large btn-success"> Add new Entry</a>
          
           <a href="{{route('superadmin.finance.up-demand.finace_demand.print',['fundlistid' => app('request')->input('fid'), 'fdate' => app('request')->input('fdate'),'tdate' => app('request')->input('tdate') ] )}}" class="btn btn-large btn-success"> Print</a>

        </p>  --}}

        @if(count($FinanceDemands) > 0)

        <table class="table table-bordered table-hover" id="example0">

            <thead class="flip-content">
                <th>Action</th>
                <th>SL</th>
                <th>ID</th>
                <th>Head</th>
                <th>Sub Head</th>
                <th>Item</th>
                <th>Amount</th>
                {{--  <th>From Date</th>
                 <th>To Date</th>  --}}
                <th>Remarks</th>
            </thead>

            <tbody>
        
                @foreach($FinanceDemands as $FinanceData)
                <tr>
                    <td>
                        {{--  <a class="label label-success" href="{{route('up-admin.finace_demand.edit', $FinanceData->id )}}">
                            <i class="fa fa-pencil"></i> Update
                        </a>  --}}
                    </td>
                    <td nowrap="nowrap">{{ $loop->iteration }}</td>
                      <td nowrap="nowrap">{{ $FinanceData->id}}</td>
                    <td nowrap="nowrap">{{ $FinanceData->getHead->headname }}</td>
                    <td nowrap="nowrap">{{ $FinanceData->getSubhead->sname }}</td>
                    <td nowrap="nowrap">{{ $FinanceData->getItem->itemname }}</td>
                    <td class="text-right">{{ $FinanceData->amount }}</td>
                    {{--  <td nowrap="nowrap">{{ $FinanceData->date }}</td>
                     <td nowrap="nowrap">{{ $FinanceData->todate }}</td>  --}}
                    <td>{{ $FinanceData->remarks }}</td>

                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <p>No Data Found</p>
        @endif

    </div>
</div>

@endsection
