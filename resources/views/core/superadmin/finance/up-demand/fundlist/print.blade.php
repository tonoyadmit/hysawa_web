@extends('layouts.print.app')
@section('my_style')

<style>
    table{border-collapse: collapse;border-spacing: 0;text-align: right;}
    div{font-size: 80%;display: inline-block;}
    .headline th {padding: 5px 20px;}
    .text-right{text-align: right;}
    .text-left{text-align: left;}
</style>

<style media="screen">
    .table-filtter .btn{ width: 100%;}
    .table-filtter {margin: 20px 0;}
    .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td,
    .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {border: 1px solid #000;}
</style>

@endsection
<?php
@$union = App\Model\Union::with('upazila.district')->find(\Auth::user()->unid);
@$districtName = $union->upazila->district->distname;
@$upazilaName = $union->upazila->upname;
@$unionName = $union->unname;
@$projectName = App\Model\Project::find(\Auth::user()->proj_id);
@$region = App\Model\Region::find(\Auth::user()->region_id);
?>

@section('content')

<p style="text-align: center;  font-size: 20px;    font-family: "Times New Roman", Times, serif;"><b>FINANCE DEMAND<b></p>
<table class="table table-bordered table-hover" style="width:100%" border="1">
    <tr>
        <td colspan="8" style="text-align:left;">
        <?php
            $project = App\Model\Project::find(auth()->user()->proj_id);
        ?>
            Project:{{ $project->project ?: '' }}

        </td>
    </tr>

    <tr>
        <td colspan="8" style="text-align:left;">
            @if(\Auth::check())
            District:{{ $districtName ?: '' }}
            @endif
        </td>
    </tr>
    <tr>
        <td colspan="8" style="text-align:left;">
            @if(\Auth::check())
            Upazila: {{ $upazilaName ?: '' }}
            @endif
        </td>
    </tr>
    <tr>
        <td colspan="8" style="text-align:left;">
            @if(\Auth::check())
            Union:{{ $unionName ?: '' }}
            @endif
        </td>
    </tr>

</table>


        @if(count($FinanceDemands) > 0)

  <table class="box" border="1" width="100%">

            <thead>
             
                <th>SL</th>
                <th>ID</th>
                <th>Head</th>
                <th>Sub Head</th>
                <th>Item</th>
                <th>Amount</th>
                <th>From Date</th>
                 <th>To Date</th>
                <th>Remarks</th>
  
            </thead>

            <tbody>
        
                @foreach($FinanceDemands as $FinanceData)
                <tr>
                   
                    <td>{{ $loop->iteration }}</td>
                      <td>{{ $FinanceData->id}}</td>
                    <td>{{ $FinanceData->getHead->headname }}</td>
                    <td>{{ $FinanceData->getSubhead->sname }}</td>
                    <td>{{ $FinanceData->getItem->itemname }}</td>
                    <td>{{ $FinanceData->amount }}</td>
                    <td>{{ $FinanceData->date }}</td>
                     <td>{{ $FinanceData->todate }}</td>
                    <td>{{ $FinanceData->remarks }}</td>
                  

                    </td>

                </tr>
                @endforeach
                  @endif

            </tbody>
        </table>

<br>
<br>
<br>
<br>
<br>
<br>
        <p>Submitted By</p>
           <p>-----------------</p>
           <br>
           <br>
              <p style="text-align: right;">Approved By</p>
              <p style="text-align: right;">-------------------</p>






@endsection