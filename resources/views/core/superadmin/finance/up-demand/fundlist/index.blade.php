@extends('layouts.appinside')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('up-admin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Finance</span>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Demand</span>
        </li>
    </ul>
</div>


 <div class="col-md-12" style="margin-top: 10px">
      @include('core.superadmin.finance.up-demand.fundlist._search')
  </div>

  
<div class="portlet light bordered" style="margin-top: 10px;">

    <div class="portlet-title">
        <div class="caption">
            <i class="icon-share font-dark"></i>
            <span class="caption-subject font-dark bold uppercase">Fund List</span>
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
    </div>

    <div class="portlet-body">

        <p>
            {{--  <a href="{{route('up-admin.fund_list.create')}}" class="btn btn-large btn-success"> Add new Entry</a>
            --}}
           {{--  <a href="{{route('up-admin.finace_demand.print')}}" class="btn btn-large btn-success"> Print</a>  --}}

        </p>

        @if(count($FinanceDemands) > 0)

        <table class="table table-bordered table-hover" id="example0">

            <thead class="flip-content">
                <th>Action</th>
                <th>SL</th>
                <th>ID</th>
                <th>From Date</th>
                 <th>To Date</th>
       
            </thead>

            <tbody>
        
                @foreach($FinanceDemands as $FinanceData)
                <tr>
                    <td>
                        {{--  <a class="label label-success" href="{{route('superadmin.finance.up-demand.fund_list.edit', $FinanceData->id )}}">
                            <i class="fa fa-pencil"></i> Update
                        </a>  --}}
  
                           <a class="label label-success" href="{{ route('superadmin.finance.up-demand.finace_demand.index',  ['fid' => $FinanceData->id,'fdate='.$FinanceData->fdate,'tdate='.$FinanceData->tdate] )}}">
                            <i class="fa fa-pencil"></i> Demand List
                        </a>


                    </td>
                    <td nowrap="nowrap">{{ $loop->iteration }}</td>
                      <td nowrap="nowrap">{{ $FinanceData->id}}</td>
                    <td nowrap="nowrap">{{ $FinanceData->fdate }}</td>
                     <td nowrap="nowrap">{{ $FinanceData->tdate }}</td>
                   

                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <p>No Data Found</p>
        @endif

    </div>
</div>




@endsection



@section('my_js')
  <script type="text/javascript">
    $(document).ready(function () {

      $('#district_id').on('change', function() {
        var head_id = $(this).val();

        $("#upazila_id").html("Select District");
        $("#union_id").html("Select Upazila");

        $.ajax({
          url: '{{route('superadmin.ajax.upazila')}}?district_id='+head_id+'&not_all=true&choose_one=true',
          type: 'GET',
          success: function(response){
            if(response['status'] == true){
              $("#upazila_id").html(response['upazila_list']);
            }
          }
        });
      });

      $('#upazila_id').on('change', function() {
        var upazila_id = $(this).val();

        $("#union_id").html("Select Upazila");

        $.ajax({
          url: '{{route('superadmin.ajax.union')}}?upazila_id='+upazila_id+'&not_all=true&choose_one=true',
          type: 'GET',
          success: function(response){
            if(response['status'] == true){
              $("#union_id").html(response['union_list']);
            }
          }
        });
      });
    });
  </script>

  @endsection

  