@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Finance</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>UP Budget</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

  @include('core.superadmin.finance.up-budget._search')

  @if( $budgets != "")

  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          <i class="fa fa-gift"></i>Filter Result </div>
          <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body">

        <table class="table table-bordered table-hover" id="example0">

          <thead >
            <tr>
              <th>District</th>
              <th>Upazila</th>
              <th>Union</th>

              <th>Head</th>
              <th>Sub Head</th>
              <th>Item Name</th>

              <th>Budget</th>

              <th>Start Year</th>
              <th>End Year</th>

            </tr>
          </thead>

          <tbody>
            @foreach($budgets as $budget)
            <tr>
              <td>{{$old['districtName'] or ''}}</td>
              <td>{{$old['upazilaName'] or ''}}</td>
              <td>{{$old['unionName'] or ''}}</td>

              <td>{{$budget->head}}</td>
              <td>{{$budget->subhead}}</td>

              <td>{{ $budget->itemname}}</td>
              <td style="text-align: right;">{{sprintf("%.2lf",$budget->budget)}}</td>
              <td>{{$budget->s_year}}</td>
              <td>{{$budget->e_year}}</td>

            </tr>
            @endforeach
          </tbody>
        </table>

      </div>
    </div>
  </div>

  @endif

@endsection

@section('my_js')
  <script type="text/javascript">
    $(document).ready(function () {

      $('#district_id').on('change', function() {
        var district_id = $(this).val();

        $("#upazila_id").html("Select District");
        $("#union_id").html("Select Upazila");

        $.ajax({
          url: '{{route('superadmin.ajax.upazila')}}?district_id='+district_id+'&not_all=true&choose_one=true',
          type: 'GET',
          success: function(response){
            if(response['status'] == true){
              $("#upazila_id").html(response['upazila_list']);
            }
          }
        });
      });
      $('#upazila_id').on('change', function() {
        var upazila_id = $(this).val();

        $("#union_id").html("Select Upazila");

        $.ajax({
          url: '{{route('superadmin.ajax.union')}}?upazila_id='+upazila_id+'&not_all=true&choose_one=true',
          type: 'GET',
          success: function(response){
            if(response['status'] == true){
              $("#union_id").html(response['union_list']);
            }
          }
        });
      });
    });
  </script>
@endsection