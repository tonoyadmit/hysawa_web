<div class="col-md-12" style="margin-top: 10px;">
    <div class="portlet box blue">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i>UP Budget <small>Report</small> </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body form">
          <form action="{{route('superadmin.finance.up-budget.index')}}" class="form-horizontal" method="GET" id="createForm">

              <input type="hidden" name="query" value="{{time()}}" />

              <div class="form-body">

                  <div class="row">
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">District</label>
                              <div class="col-md-9">
                                  <select
                                    class="form-control"
                                    id="district_id"
                                    name="district_id"
                                    required="required">
                                      <option value="">Select a District</option>
                                    @foreach($districts as $district)
                                      <option value="{{$district->id}}"

                                      @if(isset($_REQUEST['district_id']) && $_REQUEST['district_id'] == $district->id)
                                        selected="selected"
                                      @endif

                                      >{{$district->distname}}</option>
                                    @endforeach
                                  </select>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">Upazila</label>
                              <div class="col-md-9">
                                  <select
                                    class="form-control"
                                    id="upazila_id"
                                    name="upazila_id"
                                    required="required">
                                    <option value="">Select Upazila</option>
                                  </select>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">Union</label>
                              <div class="col-md-9">
                                <select
                                  name="union_id"
                                  class="form-control"
                                  id="union_id"
                                  required="required">
                                  <option value="">Select Union</option>
                                </select>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-5">
                            <input type="submit" class="btn green" value="Get Data" />
                            <a href="{{route('superadmin.finance.up-budget.index')}}" class="btn btn-info">Clear</a>
                        </div>
                        <div class="col-md-6">

                            @if( !empty($budgets) && count($budgets) )

                              <a href="{{route('superadmin.finance.up-budget.index', ['download' => 'download'])}}" class="btn btn-success pull-right">Download</a>
                            @endif
                        </div>
                    </div>
                  </div>
              </div>
          </form>

      </div>
    </div>
  </div>