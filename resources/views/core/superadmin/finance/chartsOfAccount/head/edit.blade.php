@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Finance</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Charts Of Account </a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="{{ route('superadmin.finance.charts-of-account.heads.index') }}">Head List</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Update Head</span>
      </li>
    </ul>
  </div>

  <h1 class="page-title"> Head <small> Update</small> </h1>

    <div class="col-md-6">
      <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>Edit Head</div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>

        <div class="portlet-body form">
            <form action="{{route('superadmin.finance.charts-of-account.heads.update', $head->id)}}" class="form-horizontal" method="POST" id="updateForm">

                {{csrf_field()}}

                <div class="form-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Title of Head</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="headname"
                                    @if(!empty($head->headname))
                                        value="{{$head->headname}}"
                                    @elseif(!empty( $old['headname']))
                                        value="{{$old['headname']}}"
                                    @endif >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                      <div class="form-group">
                          <div class="col-md-offset-3 col-md-9">
                              <input type="submit" class="btn green" value="Update Head" />
                          </div>
                      </div>
                    </div>
                </div>
            </form>

        </div>
      </div>
    </div>
@endsection
