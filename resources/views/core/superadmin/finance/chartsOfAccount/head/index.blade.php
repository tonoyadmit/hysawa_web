@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Finance</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Charts Of Account </a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Head List</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

  <h1 class="page-title"> Head <small> list</small> </h1>
    @if(count($heads) > 0)

        <div class="col-md-6">
            <div class="portlet light tasks-widget bordered">
                <div class="portlet-body util-btn-margin-bottom-5">
                    <table class="table table-bordered table-hover" id="example0">
                        <thead class="flip-content">
                            <th>Action</th>
                            <th>Title</th>
                        </thead>
                        <tbody>
                            @foreach($heads as $head)
                                <tr>
                                    <td>
                                      <a class="label label-success" href="{{route('superadmin.finance.charts-of-account.heads.edit', $head->id)}}"><i class="fa fa-pencil"></i></a>
                                    </td>
                                    <td>{{ $head->headname }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="pagination pull-right">
                    </div>
                </div>
            </div>
        </div>
    @else
      <div class="col-md-6">
        <div class="portlet light tasks-widget bordered">
           <p>
              No Data Found
           </p>
        </div>
      </div>
    @endif

    <div class="col-md-6">
      <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>Add New </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>

        <div class="portlet-body form">
            <form action="{{route('superadmin.finance.charts-of-account.heads.store')}}" class="form-horizontal" method="POST" id="createForm">

                {{csrf_field()}}

                <input type="hidden" name="query" value="{{time()}}" />

                <div class="form-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Title of Head</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="headname"
                                    @if(!empty( $old['headname']))
                                        value="{{$old['headname']}}"
                                    @endif >
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                      <div class="form-group">
                          <div class="col-md-offset-3 col-md-9">
                              <input type="submit" class="btn green" value="Add New Head" />
                          </div>
                      </div>
                    </div>
                </div>
            </form>

        </div>
      </div>
    </div>
@endsection
