@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Finance</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Charts Of Account </a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="{{ route('superadmin.finance.charts-of-account.items.index') }}">Item</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Update Item</span>
      </li>
    </ul>
  </div>

  <h1 class="page-title"> Item <small> Update</small> </h1>

    <div class="col-md-12">
      <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>Edit Item</div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>

        <div class="portlet-body form">
            <form action="{{route('superadmin.finance.charts-of-account.items.update', $item->id)}}" class="form-horizontal" method="POST" id="updateForm">

                {{csrf_field()}}

                <div class="form-body">

                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Select Head</label>
                              <div class="col-md-9">
                                  <select name="headid" class="form-control" required="required" id="head">
                                    @foreach($heads as $head)
                                      <option
                                        value="{{$head->id}}"

                                        @if($item->headid == $head->id)
                                          selected="selected"
                                        @endif

                                        >{{$head->headname}}</option>
                                    @endforeach
                                  </select>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Sub Head</label>
                              <div class="col-md-9">
                                  <select name="subid" class="form-control" required="required" id="subhead">

                                  @foreach($subHeads as $subhead)
                                    <option
                                        value="{{$subhead->id}}"

                                        @if($item->subid == $subhead->id)
                                          selected="selected"
                                        @endif

                                        >{{$subhead->sname}}</option>
                                  @endforeach

                                  </select>

                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-12">
                          <div class="form-group">
                              <label class="control-label col-md-3">Item Name</label>
                              <div class="col-md-9">
                                 <input class="form-control" type="text" name="itemname"

                                    value="{{$item->itemname}}"
                                    required="required">

                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                        <div class="col-md-offset-3 col-md-9">
                            <input type="submit" class="btn green" value="Update Item" />
                        </div>
                    </div>
                  </div>
                </div>
            </form>

        </div>
      </div>
    </div>
@endsection


@section('my_js')
<script type="text/javascript">
  $(document).ready(function () {

      $('#head').on('change', function() {
          var head_id = $(this).val();

          $("#subhead").html("Select Head");

          $.ajax({
            url: '{{route('superadmin.ajax.subheads')}}?head_id='+head_id,
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#subhead").html(response['subhead_list']);
              }
             // console.log(response);
            }
          });
      });
  });
</script>
@endsection