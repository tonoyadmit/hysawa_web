@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Finance</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Charts Of Account </a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Item List</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

  <h1 class="page-title"> Item <small> list</small> </h1>
  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i>Insert New
          </div>

          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body">

          <form action="{{route('superadmin.finance.charts-of-account.items.store')}}" class="form-horizontal" method="POST" id="createForm">

              {{csrf_field()}}

              <div class="form-body">
                <div class="form-group">
                  <label class="control-label col-md-3">Select Head</label>
                  <div class="col-md-5">
                    <select name="headid" class="form-control" required="required" id="head">
                      @foreach($heads as $head)
                      <option value="{{$head->id}}">{{$head->headname}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3">Sub Head</label>
                  <div class="col-md-5">
                    <select name="subid" class="form-control" required="required" id="subhead">
                      <option>Select Head First</option>
                    </select>
                    </div>
                </div>

                <div class="form-group">
                  <label class="control-label col-md-3">Item Name</label>
                  <div class="col-md-9">
                    <input class="form-control" type="text" name="itemname"
                    @if(!empty( $old['itemname']))
                    value="{{$old['itemname']}}"
                    @endif required="required">
                  </div>
                </div>
              </div>

              <div class="form-action">
                <div class="row">
                  <div class="col-md-offset-3">
                    <input type="submit" class="btn green" value="Add New Item" />
                  </div>
                </div>
              </div>

          </form>

      </div>
    </div>
  </div>



  @if(count($items) > 0)
    <div class="col-md-12">

        <div class="portlet light tasks-widget bordered">
            <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>List of Item
            </div>

            <div class="actions">
              <a href="{{route('superadmin.finance.charts-of-account.items.index', ['download' => 'download'])}}" target="_blank" class="btn btn-circle success"> <i class="fa fa-download"></i> Download</a>
            </div>

            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>

            <div class="portlet-body util-btn-margin-bottom-5">
                <table class="table table-bordered table-hover" id="example0">
                    <thead class="flip-content">
                        <th>Action</th>
                        <th>Head</th>
                        <th>Subhead</th>
                        <th>Item</th>

                    </thead>
                    <tbody>
                        <?php
                          $last = "";
                        ?>
                        @foreach($items as $item)
                            <tr>
                                <td>
                                  <a class="label label-success" href="{{route('superadmin.finance.charts-of-account.items.edit', $item->id)}}">
                                        <i class="fa fa-pencil"></i></a>
                                </td>
                                <td>{{$item->head}}</td>
                                <td>{{$item->subhead }}</td>
                                <td>{{$item->name}}</td>

                            </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class="pagination pull-right">
                </div>
            </div>
        </div>
    </div>
  @else
    <div class="col-md-12">
      <div class="portlet light tasks-widget bordered">
         <p>
            No Data Found
         </p>
      </div>
    </div>
  @endif
@endsection



@section('my_js')
<script type="text/javascript">
  $(document).ready(function () {

      $('#head').on('change', function() {
          var head_id = $(this).val();

          $("#subhead").html("Select Head");

          $.ajax({
            url: '{{route('superadmin.ajax.subheads')}}?head_id='+head_id,
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#subhead").html(response['subhead_list']);
              }
             // console.log(response);
            }
          });
      });
  });
</script>
@endsection