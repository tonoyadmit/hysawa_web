@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Finance</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Charts Of Account </a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="{{ route('superadmin.finance.charts-of-account.sub-heads.index') }}">Sub Head List</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Update Sub Head</span>
      </li>
    </ul>
  </div>

  <h1 class="page-title"> SubHead <small> Update</small> </h1>

    <div class="col-md-12">
      <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>Edit SubHead</div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>

        <div class="portlet-body ">
            <form action="{{route('superadmin.finance.charts-of-account.sub-heads.update', $subHead->id)}}" class="form-horizontal" method="POST" id="updateForm">

                {{csrf_field()}}

                <div class="form-body">

                  <div class="form-group">
                    <label class="control-label col-md-3">Select Head</label>
                    <div class="col-md-4">
                      <select name="headid" class="form-control" required="required">
                        @foreach($heads as $head)
                        <option value="{{$head->id}}"
                          @if($subHead->headid == $head->id)
                          selected="selected"
                          @endif
                          >{{$head->headname}}</option>
                          @endforeach
                        </select>
                      </div>
                    </div>

                    <div class="form-group">
                      <label class="control-label col-md-3">Title </label>
                      <div class="col-md-6">
                        <input class="form-control" type="text" name="sname"
                        value="{{$subHead->sname}}"
                        required="required">
                      </div>
                    </div>
                  </div>

                  <div class="form-action">
                    <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <input type="submit" class="btn green" value="Update Sub Head" />
                      </div>
                    </div>
                  </div>



            </form>

        </div>
      </div>
    </div>
@endsection
