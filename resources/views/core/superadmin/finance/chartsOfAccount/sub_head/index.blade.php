@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Finance</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Charts Of Account </a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Sub Head List</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

  <h1 class="page-title"> Sub Head <small> list</small> </h1>
  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i>Add New </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body form">
          <form action="{{route('superadmin.finance.charts-of-account.sub-heads.store')}}" class="form-horizontal" method="POST" id="createForm">

              {{csrf_field()}}

              <div class="form-body">

                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Select Head</label>
                              <div class="col-md-9">
                                  <select name="headid" class="form-control" required="required">
                                    @foreach($heads as $head)
                                      <option value="{{$head->id}}">{{$head->headname}}</option>
                                    @endforeach
                                  </select>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Sub Head</label>
                              <div class="col-md-9">
                                  <input class="form-control" type="text" name="sname"
                                  @if(!empty( $old['sname']))
                                      value="{{$old['sname']}}"
                                  @endif required="required">
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-6">
                            <input type="submit" class="btn green" value="Add New SubHead" />
                        </div>
                    </div>
                  </div>
              </div>
          </form>

      </div>
    </div>
  </div>



    @if(count($subHeads) > 0)

        <div class="col-md-12">
            <div class="portlet light tasks-widget bordered">
                <div class="portlet-body util-btn-margin-bottom-5">
                    <table class="table table-bordered table-hover" id="example0">
                        <thead class="flip-content">
                            <th>Action</th>
                            <th>Head</th>
                            <th>Subhead</th>
                        </thead>
                        <tbody>
                            <?php
                              $last = "";
                            ?>
                            @foreach($subHeads as $subhead)
                                <tr>
                                    <td>
                                      <a class="label label-success" href="{{route('superadmin.finance.charts-of-account.sub-heads.edit', $subhead->id)}}"><i class="fa fa-pencil"></i></a>
                                    </td>
                                    <td>{{$subhead->headname}}</td>
                                    <td>{{$subhead->sname }}</td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="pagination pull-right">
                    </div>
                </div>
            </div>
        </div>
    @else
      <div class="col-md-12">
        <div class="portlet light tasks-widget bordered">
           <p>
              No Data Found
           </p>
        </div>
      </div>
    @endif


@endsection
