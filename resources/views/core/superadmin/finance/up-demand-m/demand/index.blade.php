@extends('layouts.appinside')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('superadmin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Mobile</span>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Survey</span>
        </li>
    </ul>
</div>

<div class="portlet light bordered" style="margin-top: 10p x;">

    <div class="portlet-title">
        <div class="caption">
            <i class="icon-share font-dark"></i>
            <span class="caption-subject font-dark bold uppercase">HYSAWA Tubewell Info</span>
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
    </div>

    <div class="portlet-body">

        {{--  <p>
            <a href="{{route('superadmin.finance.up-demand-m.finace_demand.create', 'fundlistid='.app('request')->input('fid'))}}" class="btn btn-large btn-success"> Add new Entry</a>
          
           <a href="{{route('superadmin.finance.up-demand-m.finace_demand.print',['fundlistid' => app('request')->input('fid'), 'fdate' => app('request')->input('fdate'),'tdate' => app('request')->input('tdate') ] )}}" class="btn btn-large btn-success"> Print</a>

        </p>  --}}

        @if(count($FinanceDemands) > 0)

        <table class="table table-bordered table-hover" id="example0">

            <tbody>
        
                @foreach($FinanceDemands as $FinanceData)
           

                 -----------------start-------

<div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Mobile App Data</span>
      </li>
    </ul>
  </div>
  <h1 class="page-title"> Mobile-Survey-App : <small>Details</small> </h1>

  <div class="col-md-12">

      <div class="portlet-body util-btn-margin-bottom-5">
        <div class="table-responsive">
       

          <table class="table table-bordered table-hover data-table" id="example0">
            <tr class="bg-danger"><th>User Name</th><td>{{ $FinanceData->name}}</td></tr>
            <tr><th>Mobile</th><td>{{ $FinanceData->mobile}}</td></tr>
            <tr><th>Email</th><td>{{ $FinanceData->email}}</td></tr>
            @foreach($TubDetails as $TubDetail)
              <tr><th>জমির মালিক:</th><td>{{ $TubDetail->Landowner}}</td></tr>
              <tr><th>কেয়ারটেকার পুরুষ:</th><td>{{ $TubDetail->Caretaker_male}}</td></tr>
              <tr><th>কেয়ারটেকার মহিলা:</th><td>{{ $TubDetail->Caretaker_female}}</td></tr>
              <tr><th>নলকুপের নম্বর:</th><td>{{ $TubDetail->id}}</td></tr>
              @endforeach
            <?php
            $project = App\Model\Project::find( $FinanceData->proj_id);
        ?>
         <tr><th>Project</th><td>{{ $project->project }}</td></tr>
         <?php   
                        $districts = App\Model\District::where('id', $FinanceData->distid )->get();
                            ?>
    
                       @foreach($districts as $district )                     
            @if(\Auth::check())
            <tr><th>District</th><td>{{ $district->distname }}</td></tr>
            @endif
        @endforeach

           <?php                        
          $upazilas = App\Model\Upazila::where('id',  $FinanceData->upid)->get();
           ?>
         @foreach($upazilas as $upazila )                     
            @if(\Auth::check())
            <tr><th>Upazila</th><td>{{ $upazila->upname }}</td></tr>
        
            @endif
        @endforeach

 <?php                        
          $unions = App\Model\Union::where('id',  $FinanceData->unid)->get();
           ?>
         @foreach($unions as $union )                     
            @if(\Auth::check())
            <tr><th>Union</th><td>{{ $union->unname }}</td></tr>
            @endif
        @endforeach
        
        <tr><th >Image </th><td>@if($FinanceData->image != "") <img class="img img-responsive" src="{{ $FinanceData->image}}" style="width: 100px;"/>@endif  </td></tr>
      
        @if($FinanceData->functional === 0)
        <tr><th >নলকূপের সমস্যা সম্পর্কিত তথ্য: </th><td> সম্পুর্ন অচল </td></tr>

        @elseif($FinanceData->functional === 1)
        <tr><th >নলকূপের সমস্যা সম্পর্কিত তথ্য: </th><td> সম্পুর্ন সচল </td></tr>
        @elseif($FinanceData->functional === 2)
        <tr><th >নলকূপের সমস্যা সম্পর্কিত তথ্য: </th><td> সচল কিন্তু কিছু সমস্যা আছে </td></tr>

        @else
        <tr><th >নলকূপের সমস্যা সম্পর্কিত তথ্য: </th><td> উত্তর নাই </td></tr>
        @endif
        

     
        <tr><th>সমস্যার ধরন: </th> <td> 
        @if($FinanceData->p1 === 1)
         কোন সমস্যা নাই,
         @endif
        @if($FinanceData->p2 === 2)
        অতিরিক্ত লবন,
         @endif
         @if($FinanceData->p3 === 3)
         অতিরিক্ত আয়রন,
         @endif
         @if($FinanceData->p4 === 4)
         প্লাটফর্ম ভাঙ্গা,
         @endif
         @if($FinanceData->p5 === 5)
         লাটফর্ম নোংরা,
         @endif
         @if($FinanceData->p6 === 6)
         ব্যবস্থাপনা সমস্যা
         @endif
          </td></tr>
    
          <tr><th >কি ধরনের মেরামত প্রয়োজন? </th> <td> 
        @if($FinanceData->p1 === 1)
        বড় ধরনের মেরামত,
         @endif
        @if($FinanceData->p2 === 2)
        পুন:স্থাপন,
         @endif
         @if($FinanceData->p3 === 3)
         সামান্য মেরামত,
         @endif
         @if($FinanceData->p4 === 4)
         ব্যবস্থাপনা উন্নয়ন,
         @endif
          </td></tr>


          </table>
 


        </div>
      </div>
    </div>
  </div>

        -----------------end-------


                @endforeach
            </tbody>
        </table>
        @else
        <p>No Data Found</p>
        @endif


       
    </div>
</div>

@endsection
