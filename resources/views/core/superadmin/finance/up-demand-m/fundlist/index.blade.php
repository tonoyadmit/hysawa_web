@extends('layouts.appinside')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('superadmin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Mobile</span>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Survey</span>
        </li>
    </ul>
</div>


 <div class="col-md-12" style="margin-top: 10px">
      @include('core.superadmin.finance.up-demand-m.fundlist._search')
  </div>

  
<div class="portlet light bordered" style="margin-top: 10px;">

    <div class="portlet-title">
        <div class="caption">
            <i class="icon-share font-dark"></i>
            <span class="caption-subject font-dark bold uppercase">HYSAWA TUBEWELL INFO
</span>
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
    </div>

    <div class="portlet-body">

        <p>
            {{--  <a href="{{route('superadmin.fund_list.create')}}" class="btn btn-large btn-success"> Add new Entry</a>
            --}}
           {{--  <a href="{{route('superadmin.finace_demand.print')}}" class="btn btn-large btn-success"> Print</a>  --}}
           
           
           <a href="{{route('superadmin.finance.up-demand-m.print_data.print',['project_id' => app('request')->input('project_id'),
           'district_id' => app('request')->input('district_id'),'upazila_id' => app('request')->input('upazila_id'),
           'union_id' => app('request')->input('union_id'),'starting_date' => app('request')->input('starting_date'),
           'ending_date' => app('request')->input('ending_date')  ] )}}" class="btn btn-large btn-success"> Print</a>
  
  
        </p>

        @if(count($FinanceDemands) > 0)

        <table class="table table-bordered table-hover" id="example0">

            <thead class="flip-content">
                <th>Action</th>
                <th>SL</th>
                <th>ID</th>
                <th>Tubewell</th>
                <th>Report Date</th>
                <th>Project</th>
                 <th>District</th>
                 <th>Upazila</th>
                 <th>Union</th>
      
       
            </thead>

            <tbody>
        


                @foreach($FinanceDemands as $FinanceData)
                <tr>
                    <td>
                        {{--  <a class="label label-success" href="{{route('superadmin.finance.up-demand-m.survey_list.edit', $FinanceData->id )}}">
                            <i class="fa fa-pencil"></i> Update
                        </a>  --}}
  
                       <a class="label label-success" href="{{ route('superadmin.finance.up-demand-m.survey_details.index',  ['id='.$FinanceData->id,'project_id' => $FinanceData->proj_id,'district_id='.$FinanceData->distid,'upazila_id='.$FinanceData->upid,'union_id='.$FinanceData->unid,'date='.$FinanceData->date,'tubid='.$FinanceData->tubid] )}}">
                            <i class="fa fa-pencil"></i> Details Info
                        </a>


                    </td>
                    <td nowrap="nowrap">{{ $loop->iteration }}</td>
                      <td nowrap="nowrap">{{ $FinanceData->id}}</td>
                      <td nowrap="nowrap">{{ $FinanceData->tubid }}</td>
                      <td nowrap="nowrap">{{ $FinanceData->date }}</td>
                  
                       <?php
            $project = App\Model\Project::find( $FinanceData->proj_id);
        ?>
                   
                            <td nowrap="nowrap">{{ $project->project }}</td>
                          <?php   
                        $districts = App\Model\District::where('id', $FinanceData->distid )->get();
                            ?>
    
                       @foreach($districts as $district )                     
            @if(\Auth::check())
            <td nowrap="nowrap">{{ $district->distname }}</td>
            @endif
        @endforeach

        

            <?php                        
          $upazilas = App\Model\Upazila::where('id',  $FinanceData->upid)->get();
           ?>
         @foreach($upazilas as $upazila )                     
            @if(\Auth::check())

              <td nowrap="nowrap">{{ $upazila->upname }}</td>
            @endif
        @endforeach

 <?php                        
          $unions = App\Model\Union::where('id',  $FinanceData->unid)->get();
           ?>
         @foreach($unions as $union )                     
            @if(\Auth::check())
    
              <td nowrap="nowrap">{{ $union->unname }}</td>
            @endif
        @endforeach
        
        
                    
                   

                </tr>
                @endforeach


            </tbody>
        </table>
        <div class="pagination pull-right">
          {{$FinanceDemands->appends($old)->links()}}
        </div>
        
        @else
        <p>No Data Found</p>
        @endif

    </div>
</div>

  @foreach($rep_datas as $rep_data)



 <p>
     <i class="fa fa-circle"></i>
      <span>নলকূপের সমস্যা সম্পর্কিত তথ্য</span>
      <i class="fa fa-circle"></i>
      </p>     


 <table class="table table-bordered">
    <thead>
      <tr>
        <th>সম্পুর্ন অচল</th>
        <th>সম্পুর্ন সচল</th>
        <th>সচল কিন্তু কিছু সমস্যা আছে</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>{{$rep_data->fc1}}</td>
        <td>{{ $rep_data->fc2 }}</td>
         <td>{{ $rep_data->fc3 }}</td>

      </tr>
    </tbody>
  </table>


   <p>
     <i class="fa fa-circle"></i>
      <span>সমস্যার ধরন:</span>
      <i class="fa fa-circle"></i>
   </p>     

 <table class="table table-bordered">
    <thead>
      <tr>
        <th>কোন সমস্যা নাই</th>
        <th>অতিরিক্ত লবন</th>
        <th>অতিরিক্ত আয়রন</th>
        <th>প্লাটফর্ম ভাঙ্গা</th>
        <th>প্লাটফর্ম নোংরা</th>
        <th>ব্যবস্থাপনা সমস্যা</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>{{$rep_data->p1}}</td>
        <td>{{ $rep_data->p2 }}</td>
         <td>{{ $rep_data->p3 }}</td>
         <td>{{$rep_data->p4}}</td>
        <td>{{ $rep_data->p5 }}</td>
         <td>{{ $rep_data->p6 }}</td>
      </tr>
    </tbody>
  </table>

    <p>
     <i class="fa fa-circle"></i>
      <span>কি ধরনের মেরামত প্রয়োজন?</span>
      <i class="fa fa-circle"></i>
   </p>     

 <table class="table table-bordered">
    <thead >
      <tr>
        <th>বড় ধরনের মেরামত</th>
        <th>পুন:স্থাপন</th>
        <th>সামান্য মেরামত</th>
        <th>ব্যবস্থাপনা উন্নয়ন</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>{{$rep_data->r1}}</td>
        <td>{{ $rep_data->r2 }}</td>
         <td>{{ $rep_data->r3 }}</td>
         <td>{{$rep_data->r4}}</td>
      </tr>
    </tbody>
  </table>


@endforeach
  



@endsection



@section('my_js')
  <script type="text/javascript">
    $(document).ready(function () {

      $('#district_id').on('change', function() {
        var head_id = $(this).val();

        $("#upazila_id").html("Select District");
        $("#union_id").html("Select Upazila");

        $.ajax({
          url: '{{route('superadmin.ajax.upazila')}}?district_id='+head_id+'&not_all=true&choose_one=true',
          type: 'GET',
          success: function(response){
            if(response['status'] == true){
              $("#upazila_id").html(response['upazila_list']);
            }
          }
        });
      });

      $('#upazila_id').on('change', function() {
        var upazila_id = $(this).val();

        $("#union_id").html("Select Upazila");

        $.ajax({
          url: '{{route('superadmin.ajax.union')}}?upazila_id='+upazila_id+'&not_all=true&choose_one=true',
          type: 'GET',
          success: function(response){
            if(response['status'] == true){
              $("#union_id").html(response['union_list']);
            }
          }
        });
      });
    });
  </script>

  @endsection

  