@extends('layouts.print.app')
@section('my_style')

<style>
    table{border-collapse: collapse;border-spacing: 0;text-align: right;}
    div{font-size: 80%;display: inline-block;}
    .headline th {padding: 5px 20px;}
    .text-right{text-align: right;}
    .text-left{text-align: left;}
</style>

<style media="screen">
    .table-filtter .btn{ width: 100%;}
    .table-filtter {margin: 20px 0;}
    .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td,
    .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {border: 1px solid #000;}
</style>

@endsection
<?php
@$union = App\Model\Union::with('upazila.district')->find( app('request')->input('upazila_id'));
@$districtName = $union->upazila->district->distname;
@$upazilaName = $union->upazila->upname;
@$unionName = $union->unname;
@$projectName = App\Model\Project::find(app('request')->input('project_id'));


?>

<?php
if(!empty(app('request')->input('project_id'))){
    $project = App\Model\Project::find( app('request')->input('project_id'));
    $projectNameing = $project->project;
}

?>
 <?php   
 $districts = App\Model\District::where('id', app('request')->input('district_id') )->get();
 foreach($districts as $district ) {
    $districtNameing = $district->distname;
 }
 ?>
    
<?php                        
$upazilas = App\Model\Upazila::where('id',   app('request')->input('upazila_id'))->get();
foreach($upazilas as $upazila )  {
    $upazilaNameing = $upazila->upname;
}
?>

<?php                        
$unions = App\Model\Union::where('id',  app('request')->input('union_id'))->get();
foreach($unions as $union ){
    $unionNameing = $union->unname; 
}
?>
<?php 
$startDating = app('request')->input('starting_date');
$endDating=app('request')->input('ending_date');
?>


@section('content')

<p style="text-align: center;  font-size: 20px;    font-family: "Times New Roman", Times, serif;"><b>HYSAWA tubewell functionality report<b></p>
<table class="table table-hover" style="width:100%" border="0" >
    <tr>
        <td colspan="8" style="text-align:left; font-size: 20px;    font-family: "Times New Roman", Times, serif;">
        <?php
            $project = App\Model\Project::find(app('request')->input('project_id'));
        ?>
            @if(!empty($projectNameing))
            Project:{{ $projectNameing  ?: '' }}
            @endif
        </td>
    </tr>

    <tr>
        <td colspan="8" style="text-align:left; font-size: 20px;    font-family: "Times New Roman", Times, serif;">
        @if(!empty($districtNameing))
            District:{{ $districtNameing ?: '' }}
            @endif
        </td>
    </tr>
    <tr>
        <td colspan="8" style="text-align:left; font-size: 20px;    font-family: "Times New Roman", Times, serif;">
            @if(!empty($upazilaNameing))
            Upazila: {{$upazilaNameing ?: '' }}
            @endif
        </td>
    </tr>
    <tr>
        <td colspan="8" style="text-align:left; font-size: 20px;    font-family: "Times New Roman", Times, serif;">
           @if(!empty($unionNameing))
            Union:{{ $unionNameing ?: '' }}
            @endif
            
        </td>
    </tr>



    </tr>

</table>

<p style="text-align: left;  font-size: 20px;    font-family: "Times New Roman", Times, serif;"><b>Reporting period: {{ $startDating ?: '' }} To: {{ $endDating ?: '' }} <b></p>


 @if(!empty($FinanceDemands))
  <table class="box" border="1" width="100%" style="margin-top: 20px;">


  <thead class="flip-content">
     
                <th>SL</th>
                <th>ID</th>
                <th>Tubewell</th>
                <th>Name</th>
                <th>Mobile</th>
                <th>email</th>
                <th>Report Date</th>
                <th>Project</th>
                 <th>District</th>
                 <th>Upazila</th>
                 <th>Union</th>
                 <th>Village</th>
      
       
            </thead>

            <tbody>
        


                @foreach($FinanceDemands as $FinanceData)
                <tr>
       
                    <td nowrap="nowrap">{{ $loop->iteration }}</td>
                      <td nowrap="nowrap">{{ $FinanceData->id}}</td>
                      <td nowrap="nowrap">{{ $FinanceData->tubid }}</td>
                      <td nowrap="nowrap">{{ $FinanceData->name }}</td>
                      <td nowrap="nowrap">{{ $FinanceData->mobile }}</td>
                      <td nowrap="nowrap">{{ $FinanceData->email }}</td>
                      <td nowrap="nowrap">{{ $FinanceData->date }}</td>
                  
                       <?php
            $project = App\Model\Project::find( $FinanceData->proj_id);
        ?>
                   
                            <td nowrap="nowrap">{{ $project->project }}</td>
                          <?php   
                        $districts = App\Model\District::where('id', $FinanceData->distid )->get();
                            ?>
    
                       @foreach($districts as $district )                     
            @if(\Auth::check())
            <td nowrap="nowrap">{{ $district->distname }}</td>
            @endif
        @endforeach

        

            <?php                        
          $upazilas = App\Model\Upazila::where('id',  $FinanceData->upid)->get();
           ?>
         @foreach($upazilas as $upazila )                     
            @if(\Auth::check())

              <td nowrap="nowrap">{{ $upazila->upname }}</td>
            @endif
        @endforeach

 <?php                        
          $unions = App\Model\Union::where('id',  $FinanceData->unid)->get();
           ?>
         @foreach($unions as $union )                     
            @if(\Auth::check())
    
              <td nowrap="nowrap">{{ $union->unname }}</td>
            @endif
        @endforeach
        
        <td nowrap="nowrap">{{ $FinanceData->Village }}</td>
                    
                   

                </tr>
                @endforeach


            </tbody>
            @endif

    
        </table>



            </tbody>
        </table>

<br>
<br>
<br>
<br>

 @if(!empty($rep_datas))

@foreach($rep_datas as $rep_data)



<p>
    <i class="fa fa-circle"></i>
     <span>নলকূপের সমস্যা সম্পর্কিত তথ্য</span>
     <i class="fa fa-circle"></i>
     </p>     


<table class="box" border="1" width="100%">
   <thead>
     <tr style="text-align: center;">
       <th>সম্পুর্ন অচল</th>
       <th>সম্পুর্ন সচল</th>
       <th>সচল কিন্তু কিছু সমস্যা আছে</th>
     </tr>
   </thead>
   <tbody>
     <tr style="text-align: center;">
       <td>{{$rep_data->fc1}}</td>
       <td>{{ $rep_data->fc2 }}</td>
        <td>{{ $rep_data->fc3 }}</td>

     </tr>
   </tbody>
 </table>


  <p>
    <i class="fa fa-circle"></i>
     <span>সমস্যার ধরন:</span>
     <i class="fa fa-circle"></i>
  </p>     

<table class="box" border="1" width="100%">
   <thead>
     <tr style="text-align: center;">
       <th>কোন সমস্যা নাই</th>
       <th>অতিরিক্ত লবন</th>
       <th>অতিরিক্ত আয়রন</th>
       <th>প্লাটফর্ম ভাঙ্গা</th>
       <th>প্লাটফর্ম নোংরা</th>
       <th>ব্যবস্থাপনা সমস্যা</th>
     </tr>
   </thead>
   <tbody>
     <tr style="text-align: center;">
       <td>{{$rep_data->p1}}</td>
       <td>{{ $rep_data->p2 }}</td>
        <td>{{ $rep_data->p3 }}</td>
        <td>{{$rep_data->p4}}</td>
       <td>{{ $rep_data->p5 }}</td>
        <td>{{ $rep_data->p6 }}</td>
     </tr>
   </tbody>
 </table>

   <p>
    <i class="fa fa-circle"></i>
     <span>কি ধরনের মেরামত প্রয়োজন?</span>
     <i class="fa fa-circle"></i>
  </p>     

<table class="box" border="1" width="100%">
   <thead >
     <tr style="text-align: center;">
       <th>বড় ধরনের মেরামত</th>
       <th>পুন:স্থাপন</th>
       <th>সামান্য মেরামত</th>
       <th>ব্যবস্থাপনা উন্নয়ন</th>
     </tr>
   </thead>
   <tbody>
     <tr style="text-align: center;">
       <td>{{$rep_data->r1}}</td>
       <td>{{ $rep_data->r2 }}</td>
        <td>{{ $rep_data->r3 }}</td>
        <td>{{$rep_data->r4}}</td>
     </tr>
   </tbody>
 </table>


@endforeach

@endif


@endsection