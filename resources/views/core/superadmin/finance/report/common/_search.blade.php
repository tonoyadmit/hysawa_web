<div class="col-md-12" style="margin-top: 10px;">
    <div class="portlet box blue">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i>Finance Report </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body form">
          <form action="{{route('superadmin.finance.report.common.index')}}" class="form-horizontal" method="GET" id="createForm">

              <input type="hidden" name="query" value="{{time()}}" />

              <div class="form-body">

                  <div class="row">
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">District</label>
                              <div class="col-md-9">
                                  <select class="form-control" required="required" id="district_id" name="district_id">
                                    <option value="" selected="selected">Select a District</option>

                                    <?php

                                      $districts = App\Model\District::orderBy('distname')->get();

                                    ?>


                                    @foreach($districts as $district)
                                      <option value="{{$district->id}}">{{$district->distname}}</option>
                                    @endforeach
                                  </select>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">Upazila</label>
                              <div class="col-md-9">
                                  <select class="form-control" required="required" id="upazila_id" name="upazila_id">
                                    <option>Select District First</option>
                                  </select>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">Union</label>
                              <div class="col-md-9">
                                <select name="union_id" class="form-control" required="required" id="union_id">
                                  <option>Select Upazila First</option>
                                </select>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">Starting Date</label>
                              <div class="col-md-9">
                                  <input
                                    type="text"
                                    name="starting_date"
                                    class="form-control date-picker"
                                    data-date-format="yyyy-mm-dd"

                                    @if(count($old) && $old['starting_date'] != "")
                                      value="{{date('Y-m-d', strtotime($old['starting_date']))}}"
                                    @endif
                                  />
                              </div>
                          </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">Ending Date</label>
                              <div class="col-md-9">
                                  <input
                                    type="text"
                                    name="ending_date"
                                    class="form-control date-picker"
                                    data-date-format="yyyy-mm-dd"

                                    @if(count($old) && $old['ending_date'] != "")
                                      value="{{date('Y-m-d', strtotime($old['ending_date']))}}"
                                    @endif
                                  />
                              </div>
                          </div>
                      </div>
                  </div>


                  <div class="row">
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-5">
                            <input type="submit" class="btn green" value="Get Data" />
                            <a href="{{route('superadmin.finance.report.common.index')}}" class="btn btn-info">Clear</a>
                        </div>
                        <div class="col-md-6">

                            @if($old['union_id'] != "")
                            <?php $old['print'] = 'print'; ?>
                            <a href="{{route('superadmin.finance.report.common.index', $old)}}" class="btn btn-success pull-right">print</a>
                            @endif

                        </div>
                    </div>
                  </div>
              </div>
          </form>

      </div>
    </div>
  </div>