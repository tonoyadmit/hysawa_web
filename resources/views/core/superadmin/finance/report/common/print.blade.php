@extends('layouts.print.app')

@section('my_style')
    <style>
        table{border-collapse: collapse;border-spacing: 0;text-align: right;}
        div{font-size: 80%;display: inline-block;}
        .headline th {padding: 5px 20px;}
        .text-right{text-align: right;}
        .text-left{text-align: left;}
        .table-filtter .btn{ width: 100%;}
        .table-filtter {margin: 20px 0;}
        .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
            border: 1px solid #000;
        }
    </style>
@endsection

@section('content')
    {!! $report->generateHeading('print') !!}
    {!! $report->getReportBody('print') !!}
@endsection