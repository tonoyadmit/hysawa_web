@extends('layouts.appinside')

@section('content')
    <style media="screen">
    .table-filtter .btn{ width: 100%;}
    .table-filtter {margin: 20px 0;}
    .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {border: 1px solid #000;}
    </style>

    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li><a href="{{ route('superadmin.dashboard') }}">Home</a><i class="fa fa-circle"></i></li>
            <li><a href="#">Finance</a><i class="fa fa-circle"></i></li>
            <li><a href="#">Report </a><i class="fa fa-circle"></i></li>
            <li><span>Common Report</span></li>
        </ul>
    </div>

    @include('partials.errors')

    @include('core.superadmin.finance.report.common._search')

    @if($old['starting_date'] != "" && $old['ending_date'] != "" && !empty($report))
        {!! $report->generateHeading('html') !!}
        {!! $report->getReportBody('html') !!}
    @endif

@endsection

@section('my_js')
    <script type="text/javascript">
      $(document).ready(function () {

        $('#district_id').on('change', function() {
          var head_id = $(this).val();

          $("#upazila_id").html("Select District");
          $("#union_id").html("Select Upazila");

          $.ajax({
            url: '{{route('superadmin.ajax.upazila')}}?district_id='+head_id,
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#upazila_id").html(response['upazila_list']);
              }
            }
          });
        });
        $('#upazila_id').on('change', function() {
          var upazila_id = $(this).val();

          $("#union_id").html("Select Upazila");

          $.ajax({
            url: '{{route('superadmin.ajax.union')}}?upazila_id='+upazila_id,
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#union_id").html(response['union_list']);
              }
            }
          });
        });
      });
    </script>
@endsection
