<div class="col-md-12" style="margin-top: 15px;">
    <div class="portlet box blue">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i> Upazila <small> finance Report</small> </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body form">
          <form action="{{route('superadmin.finance.report.upazila.index')}}" class="form-horizontal" method="GET" id="createForm">
              <input type="hidden" name="query" value="{{time()}}" />

              <div class="form-body">
                  <div class="row">

                      <div class="col-md-2">
                        <label class="control-label ">District *</label>
                        <select class="form-control input-sm" required="required" id="district_id" name="district_id">
                          <option value="">Choose One</option>
                          @foreach($districts as $district)
                            <option value="{{$district->id}}"

                              @if(isset($_REQUEST['district_id']) && $_REQUEST['district_id'] == $district->id)
                                selected="selected"
                              @endif

                            >{{$district->distname}}</option>
                          @endforeach
                        </select>
                      </div>

                      <div class="col-md-2">
                        <label class="control-label ">Upazila *</label>
                        <select class="form-control input-sm" required="required" id="upazila_id" name="upazila_id">


                          @if(isset($_REQUEST['upazila_id']) && $_REQUEST['upazila_id'] != "")

                            <?php
                              $upazilas = App\Model\Upazila::where('disid', $_REQUEST['district_id'])->get();
                            ?>

                            @foreach($upazilas as $upazila)
                              <option value="{{$upazila->id}}">{{$upazila->upname}}</option>
                            @endforeach

                          @else
                            <option>Select Upazila First</option>
                          @endif


                        </select>
                      </div>

                      <div class="col-md-2">
                        <label class="control-label">Starting Date *</label>
                        <input
                            type="text"
                            name="starting_date"
                            class="form-control date-picker input-sm"
                            data-date-format="yyyy-mm-dd"

                            @if(isset($_REQUEST['starting_date']) && $_REQUEST['starting_date'] != "")
                              value="{{date('Y-m-d', strtotime($_REQUEST['starting_date']))}}"
                            @endif

                            required="required"
                          />
                          <p>YYYY-MM-DD</p>
                      </div>

                      <div class="col-md-2">
                        <label class="control-label">Ending Date *</label>
                        <input
                          type="text"
                          name="ending_date"
                          class="form-control date-picker input-sm"
                          data-date-format="yyyy-mm-dd"
                          @if(isset($_REQUEST['ending_date']) && $_REQUEST['ending_date'] != "")
                            value="{{date('Y-m-d', strtotime($_REQUEST['ending_date']))}}"
                          @endif
                          required="required"
                        />
                        <p>YYYY-MM-DD</p>
                      </div>

                      <div class="col-md-3">
                        <br/>
                        <input type="submit" class="btn green btn-sm" value="Get Data" />
                        <a href="{{route('superadmin.finance.report.upazila.index')}}" class="btn btn-info btn-sm">Clear</a>
                        @if($old['upazila_id'] != "")
                          <?php $old['print'] = 'print'; ?>
                          <a href="{{route('superadmin.finance.report.upazila.index', $old)}}" class="btn btn-success pull-right btn-sm">Print</a>
                        @endif
                      </div>

                  </div>
              </div>
          </form>
      </div>
    </div>
  </div>