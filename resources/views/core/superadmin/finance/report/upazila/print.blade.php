@extends('layouts.print.app')
@section('my_style')
<style>
    table{
        border-collapse: collapse;
        border-spacing: 0;
        text-align: right;
    }
    div{
        font-size: 80%;
        display: inline-block;
    }
    .headline th {
        padding: 5px 20px;
    }
    .text-right{
        text-align: right;
    }
    .text-left{
        text-align: left;
    }
</style>

<style media="screen">
    .table-filtter .btn{ width: 100%;}
    .table-filtter {
        margin: 20px 0;
    }
    .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
        border: 1px solid #000;
    }
</style>

@endsection

@section('content')
<table class="table table-bordered table-hover" border="1" width="100%">
    <tr>
        <td colspan="8" style="text-align:center">
            @if(\Auth::check())
            District:{{ $old['districtName'] ?: '' }}
            @endif
        </td>
        <td colspan="8" style="text-align:center">
            @if(\Auth::check())
            Upazila: {{ $old['upazilaName'] ?: '' }}
            @endif
        </td>
    </tr>
    <tr>
        <td colspan="8" style="text-align:center">
            @if(\Auth::check())
            Starting date:{{ $old['starting_date'] ?: '' }}
            @endif
        </td>
        <td colspan="8" style="text-align:center">
            @if(\Auth::check())
            Ending Date: {{ $old['ending_date'] ?: '' }}
            @endif
        </td>
    </tr>
    </table>


    <table class="table table-bordered table-hover" id="example0">
    <thead class="flip-content">
        <th>Description</th>
        <th>Balance</th>
        <th>Approved Budget (A)</th>
        <th>Current Income (B)</th>
        <th>Current Expenses (C)</th>
        <th>Cumulative Income (D)</th>
        <th>Cumulative Expenses (E) </th>
        <th>Budget Balance (F=A-E)</th>
        <th>Demand</th>
    </thead>
    <tbody>
        <tr>
            <td style="text-align:right;">Opening Balance</td>
            <th style="text-align:right;">{{ sprintf("%.1lf",$data->total) }}</th>
            <td colspan="8">&nbsp;</td>
        </tr>
        <tr>
            <td>Cash in Hand</td>
            <th style="text-align:right;">{{ sprintf("%.1lf",$data->opening_cash) }}</th>
            <td colspan="8">&nbsp;</td>
        </tr>
        <tr>
            <td>Cash at Bank</td>
            <th style="text-align:right;">{{ sprintf("%.1lf",$data->opening_bank) }}</th>
            <td colspan="8">&nbsp;</td>
        </tr>
        <?php
        $gbudget=0;
        $gcurrentIncome=0;
        $gcurrentExpenditure=0;
        $gcomulativeIncome=0;
        $gcomulativeBudget=0;
        $gcomulativeExpenditure=0;
        $gdemand=0;
        ?>
        @foreach($d as $list)
        <?php
        $budget=0;
        $currentIncome=0;
        $currentExpenditure=0;
        $comulativeIncome=0;
        $comulativeBudget=0;
        $comulativeExpenditure=0;
        $demand=0;
        ?>
        <tr>
            <td colspan="9" style="text-align:left">{{ $list["headname"]}}</td>
        </tr>
        @if (count($list["subhead"]) > 0)
        @foreach($list["subhead"] as $sublist)
        <tr>
            <td colspan="9" style="text-align:left">{{ $sublist["sname"]}}</td>
        </tr>
        @if (count($sublist["sub_item"]) > 0)
        @foreach($sublist["sub_item"] as $sub_item)
        <tr>
<?php
$budget+=$cbudget=\App\FinanceData::getBudget(
    $sub_item["id"],
    'upid',
    $old['upazila_id']);
$currentIncome+=$ccurrentIncome=\App\FinanceData::currentIncome(
    $sub_item["id"],
    $old['starting_date'],
    $old['ending_date'],
    'upid',
    $old['upazila_id']);
$currentExpenditure+=$ccurrentExpenditure=\App\FinanceData::currentExpenditure(
    $sub_item["id"],
    $old['starting_date'],
    $old['ending_date'],
    'upid',
    $old['upazila_id']);
$comulativeIncome+=$ccomulativeIncome=\App\FinanceData::comulativeIncome(
    $sub_item["id"],
    $old['ending_date'],
    'upid',
    $old['upazila_id']);
$comulativeExpenditure+=$ccomulativeExpenditure=\App\FinanceData::comulativeExpenditure(
    $sub_item["id"],
    $old['ending_date'],
    'upid',
    $old['upazila_id']);
$comulativeBudget+= $ccomulativeBudget=\App\FinanceData::comulativeBudget();
$demand+=@$cdemand= \App\FinanceData::demand(
    $sub_item["id"],
    'upid',
    $old['upazila_id']);
?>


    <td style="text-align:left">{{ $sub_item["itemname"]}}</td>
    <td>&nbsp;</td>
    <td>{{ sprintf("%.1lf",$cbudget) }}</td>
    <td>{{ sprintf("%.1lf",$ccurrentIncome) }}</td>
    <td>{{ sprintf("%.1lf",$ccurrentExpenditure) }}</td>
    <td>{{ sprintf("%.1lf",$ccomulativeIncome) }}</td>
    <td>{{ sprintf("%.1lf",$ccomulativeExpenditure) }}</td>
    <td>{{ sprintf("%.1lf",$ccomulativeBudget) }}</td>
    <td>{{ sprintf("%.1lf",$cdemand) }}</td>
</tr>
@endforeach
@endif
@endforeach
<tr>
    <td>Sub Total</td>
    <td>&nbsp;</td>
    <td>{{ sprintf("%.1lf",$budget) }}</td>
    <td>{{ sprintf("%.1lf",$currentIncome) }}</td>
    <td>{{ sprintf("%.1lf",$currentExpenditure) }}</td>
    <td>{{ sprintf("%.1lf",$comulativeIncome) }}</td>
    <td>{{ sprintf("%.1lf",$comulativeExpenditure) }}</td>
    <td>{{ sprintf("%.1lf",$comulativeBudget) }}</td>
    <td>{{ sprintf("%.1lf",$demand) }}</td>
</tr>
<?php
$gbudget+=$budget;
$gcurrentIncome+=$currentIncome;
$gcurrentExpenditure+=$currentExpenditure;
$gcomulativeIncome+=$comulativeIncome;
$gcomulativeExpenditure+=$comulativeExpenditure;
$gcomulativeBudget+=$comulativeBudget;
$gdemand+=$demand+=$cdemand;
?>
@endif
@endforeach
<tr>
    <td style="text-align:right">Grand Total</td>
    <td>&nbsp;</td>
    <td style="text-align:right">{{ sprintf("%.1lf",$gbudget)  }}</td>
    <td style="text-align:right">{{ sprintf("%.1lf",$gcurrentIncome)  }}</td>
    <td style="text-align:right">{{ sprintf("%.1lf",$gcurrentExpenditure)  }}</td>
    <td style="text-align:right">{{ sprintf("%.1lf",$gcomulativeIncome)  }}</td>
    <td style="text-align:right">{{ sprintf("%.1lf",$gcomulativeExpenditure)  }}</td>
    <td style="text-align:right">{{ sprintf("%.1lf",$gcomulativeBudget)  }}</td>
    <td style="text-align:right">{{ sprintf("%.1lf",$gdemand ) }}</td>
</tr>
<tr>
    <td style="text-align:right">Clossing Balance</td>
    <td style="text-align:right">{{ sprintf("%.1lf",$data2->total)  }}</td>
    <td colspan="8" >&nbsp;</td>
</tr>
<tr>
    <td style="text-align:right">Cash in Hand</td>
    <td style="text-align:right">{{ sprintf("%.1lf",$data2->opening_cash) }}</td>
    <td colspan="8" >&nbsp;</td>
</tr>
<tr>
    <td style="text-align:right">Cash at Bank</td>
    <td style="text-align:right">{{ sprintf("%.1lf",$data2->opening_bank) }}</td>
    <td colspan="8" >&nbsp;</td>
</tr>
</tbody>
</table>
@endsection