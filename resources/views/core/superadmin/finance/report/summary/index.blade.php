@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Finance</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Report </a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Summary By Union</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

  <h1 class="page-title"> Report <small> Summary By Union</small> </h1>


  @if(count($unions) > 0)
    <div class="col-md-12">
        <div class="portlet light tasks-widget bordered">

          <div class="portlet-title">
              <div class="caption">
                <i class="fa fa-gift"></i>Filter Result
              </div>

              <div class="actions">
                <a href="{{route('superadmin.finance.report.summary.index', ['download' => 'download'])}}" target="_blank" class="btn btn-circle success"> <i class="fa fa-download"></i> Download</a>
              </div>


            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>


            <div class="portlet-body util-btn-margin-bottom-5">
                <div class="table-responsive">
                  <style>
                    #example0 th, #example0 td {
                      white-space: nowrap;
                    }
                  </style>
                  <table class="table table-bordered table-hover" id="example0">
                      <thead class="flip-content">

                          <th style="font-size: 10px;">District</th>
                          <th style="font-size: 10px;">Upazila</th>
                          <th style="font-size: 10px;">Union</th>

                          <th style="font-size: 10px;">Total Income (Tk.)</th>
                          <th style="font-size: 10px;">Total Expenses (Tk.)</th>
                          <th style="font-size: 10px;">Balance (Tk.)</th>
                          <th style="font-size: 10px;">Last Transaction Date</th>

                      </thead>
                      <tbody>
                          @foreach($unions as $union)

                              <tr>
                                  <td style="font-size: 10px;">{{$union->upazila->district->distname or ''}}</td>
                                  <td style="font-size: 10px;">{{$union->upazila->upname or ''}}</td>
                                  <td style="font-size: 10px;">{{$union->unname or ''}}</td>
                                  <?php
                                    $income = $union->financeDatas()->where('trans_type', 'in')->sum('amount');
                                    $exp    = $union->financeDatas()->where('trans_type', 'ex')->sum('amount');
                                    $date   = count($union->financeDatas) ? $union->financeDatas()->orderBy('date', 'DESC')->first()->date : "";
                                  ?>
                                  <td style="font-size: 10px; text-align: right"">{{ sprintf("%.1lf",$income)}}</td>
                                  <td style="font-size: 10px; text-align: right"">{{ sprintf("%.1lf",$exp)}}</td>
                                  <td style="font-size: 10px; text-align: right"">{{ sprintf("%.1lf",$income-$exp)}}</td>

                                  <td style="font-size: 10px;">@if($date != ""){{date('d-m-Y', strtotime($date))}}@else &nbsp; @endif </td>
                              </tr>
                          @endforeach
                      </tbody>
                  </table>

                  <div class="pagination pull-right">
                    {{$unions->links()}}
                  </div>
                </div>
            </div>
        </div>
    </div>
  @else
    <div class="col-md-12">
      <div class="portlet light tasks-widget bordered">
         <p>
            No Data Found
         </p>
      </div>
    </div>
  @endif
@endsection



