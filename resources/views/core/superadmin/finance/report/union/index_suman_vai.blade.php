@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('superadmin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="#">Finance</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="#">Report </a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Union Report</span>
    </li>
  </ul>
</div>

@include('partials.errors')

<h1 class="page-title"> Report <small> Union Report</small> </h1>

@include('core.superadmin.finance.report.union._search')

@if($old['starting_date'] != "" && $old['ending_date'] != "")

<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <div class="portlet-body util-btn-margin-bottom-5">


      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li>
            <a href="#">Starting Date: <span style="color:purple">{{$old['starting_date']}}</span></a>
            <i class="fa fa-circle"></i>
          </li>
          <li>
            <a href="#">Ending Date: <span style="color:purple">{{$old['ending_date']}}</span></a>
            <i class="fa fa-circle"></i>
          </li>
          <li>
            <span>Report</span>
          </li>
        </ul>
      </div>

      <div class="table-responsive">
        <style media="screen">
            .table-filtter .btn{ width: 100%;}
            .table-filtter {
                margin: 20px 0;
            }
            .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
                border: 1px solid #000;
            }
        </style>


        <div class="col-md-12">
          <div class="portlet light tasks-widget bordered">
              <div class="portlet-body util-btn-margin-bottom-5">
                  <table class="table table-bordered table-hover" id="example0">
                      <thead class="flip-content">
                          <th>Description</th>
                          <th>Approved Budget(A)</th>
                          <th>Current Income(B)</th>
                          <th>Current Expenses(C)</th>
                          <th>Cumulative Income(D)</th>
                          <th>Cumulative Expenses(E</th>
                          <th>Budget Balance(F=A-E)</th>
                          <th>Demand</th>
                      </thead>
                      <tbody>
                          <tr>
                              <td>Opening Balance</td>
                              <td colspan="8" style="text-align:center">{{ $data->total }}</td>
                          </tr>
                          <tr>
                              <td>Cash in Hand</td>
                              <td colspan="8" style="text-align:center">{{ $data->opening_cash }}</td>
                          </tr>
                          <tr>
                              <td>Cash at Bank</td>
                              <td colspan="8" style="text-align:center">{{ $data->opening_bank }}</td>
                          </tr>
                          @foreach($d as $list)
                          <tr>
                              <td colspan="8" style="text-align:center">{{ $list["headname"]}}</td>
                          </tr>
                          @if (count($list["subhead"]) > 0)
                          <?php
                          $budget=0;
                          $currentIncome=0;
                          $currentExpenditure=0;
                          $comulativeIncome=0;
                          $comulativeBudget=0;
                          $comulativeExpenditure=0;
                          $demand=0;
                          ?>
                          @foreach($list["subhead"] as $sublist)
                          <tr>
                              <?php
                                $budget += \App\FinanceData::getBudget(
                                  $sublist["headid"],
                                  $sublist["id"],
                                  $old['starting_date'],
                                  $old['ending_date'],
                                  'unid',
                                  $old['union_id']
                                );

                                $currentIncome += \App\FinanceData::currentIncome(
                                    $sublist["headid"],
                                    $sublist["id"],
                                    $old['starting_date'],
                                    $old['ending_date'],
                                    'unid',
                                    $old['union_id']
                                );
                                $currentExpenditure=\App\FinanceData::currentExpenditure(
                                    $sublist["headid"],
                                    $sublist["id"],
                                    $old['starting_date'],
                                    $old['ending_date'],
                                    'unid',
                                    $old['union_id']
                                );
                                $comulativeIncome=\App\FinanceData::comulativeIncome(
                                    $sublist["headid"],
                                    $sublist["id"],
                                    'unid',
                                    auth()->user()->unid);
                                $comulativeExpenditure=\App\FinanceData::comulativeExpenditure(
                                    $sublist["headid"],
                                    $sublist["id"],
                                    'unid',
                                    $old['union_id']
                                );

                                $comulativeBudget= \App\FinanceData::comulativeBudget();

                                $demand= \App\FinanceData::demand(
                                    $sublist["headid"],
                                    $sublist["id"],
                                    'unid',
                                    $old['union_id']
                                  );
                                ?>
                                <td style="text-align:center">{{ $sublist["sname"]}}</td>
                                <td>{{ \App\FinanceData::getBudget(
                                    $sublist["headid"],
                                    $sublist["id"],
                                    $old['starting_date'],
                                    $old['ending_date'],
                                    'unid',
                                    $old['union_id']
                                  ) }}</td>
                                <td>{{ \App\FinanceData::currentIncome(
                                    $sublist["headid"],
                                    $sublist["id"],
                                    $old['starting_date'],
                                    $old['ending_date'],
                                    'unid',
                                    $old['union_id']
                                  ) }}</td>
                                <td>{{ \App\FinanceData::currentExpenditure(
                                    $sublist["headid"],
                                    $sublist["id"],
                                    $old['starting_date'],
                                    $old['ending_date'],
                                    'unid',
                                    $old['union_id']
                                    ) }}</td>
                                <td>{{ \App\FinanceData::comulativeIncome(
                                    $sublist["headid"],
                                    $sublist["id"],
                                    'unid',
                                    $old['union_id']
                                    ) }}</td>
                                <td>{{ \App\FinanceData::comulativeExpenditure(
                                    $sublist["headid"],
                                    $sublist["id"],
                                    'unid',
                                    auth()->user()->unid) }}</td>
                                <td>{{ \App\FinanceData::comulativeBudget() }}</td>
                                <td>{{ \App\FinanceData::demand(
                                    $sublist["headid"],
                                    $sublist["id"],
                                    'unid',
                                    $old['union_id']
                                  ) }}</td>

                          </tr>
                          @endforeach
                          <tr>
                              <td>Sub Total</td>
                              <td>{{ $budget }}</td>
                              <td>{{ $currentIncome }}</td>
                              <td>{{ $currentExpenditure }}</td>
                              <td>{{ $comulativeIncome }}</td>
                              <td>{{ $comulativeExpenditure }}</td>
                              <td>{{ $comulativeBudget }}</td>
                              <td>{{ $demand }}</td>
                          </tr>
                          @endif
                          @endforeach
                      </tbody>
                  </table>
                  <div class="pagination pull-right">
                  </div>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@else
   @if(!empty($old) && !empty($old['query']))
      <div class="col-md-12">
        <div class="portlet light tasks-widget bordered">
          <p>
            Please Select dates
          </p>
        </div>
      </div>
  @endif
@endif
@endsection

@section('my_js')
<script type="text/javascript">
  $(document).ready(function () {

    $('#district_id').on('change', function() {
      var head_id = $(this).val();

      $("#upazila_id").html("Select District");
      $("#union_id").html("Select Upazila");

      $.ajax({
        url: '{{route('superadmin.ajax.upazila')}}?district_id='+head_id,
        type: 'GET',
        success: function(response){
          if(response['status'] == true){
            $("#upazila_id").html(response['upazila_list']);
          }
        }
      });
    });
    $('#upazila_id').on('change', function() {
      var upazila_id = $(this).val();

      $("#union_id").html("Select Upazila");

      $.ajax({
        url: '{{route('superadmin.ajax.union')}}?upazila_id='+upazila_id,
        type: 'GET',
        success: function(response){
          if(response['status'] == true){
            $("#union_id").html(response['union_list']);
          }
        }
      });
    });
  });
</script>
@endsection