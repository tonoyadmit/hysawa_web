<div class="col-md-12" style="margin-top: 15px;">
    <div class="portlet box blue">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i>Union <small>  Report</small> </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body form">
          <form action="{{route('superadmin.finance.report.union.index')}}" class="form-horizontal" method="get" id="createForm">

              <input type="hidden" name="query" value="{{time()}}" />

              <div class="form-body">

                  <div class="row">

                    <div class="col-md-2">
                      <label class="control-label">District</label>
                      <select class="form-control input-sm" required="required" id="district_id" name="district_id">
                        <option value="" selected="selected">Select a District</option>
                        @foreach($districts as $district)
                          <option value="{{$district->id}}"

                            @if(isset($_REQUEST['district_id']) && $_REQUEST['district_id'] == $district->id)
                                selected="selected"
                            @endif

                          >{{$district->distname}}</option>
                        @endforeach
                      </select>
                    </div>

                    <div class="col-md-2">
                      <label class="control-label">Upazila</label>
                      <select class="form-control input-sm" required="required" id="upazila_id" name="upazila_id">

                        @if(isset($_REQUEST['upazila_id']) && $_REQUEST['upazila_id'] != "")
                            <?php
                              $upazilas = App\Model\Upazila::where('disid', $_REQUEST['district_id'])->get();
                            ?>
                            <option value="">Choose One</option>
                            @foreach($upazilas as $upazila)
                              <option value="{{$upazila->id}}"

                                @if(isset($_REQUEST['upazila_id']) && $_REQUEST['upazila_id'] == $upazila->id)
                                  selected="selected"
                                @endif


                              >{{$upazila->upname}}</option>
                            @endforeach

                        @else
                            <option>Select Upazila First</option>
                        @endif




                      </select>
                    </div>


                    <div class="col-md-2">
                      <label class="control-label">Union</label>
                      <select name="union_id" class="form-control input-sm" required="required" id="union_id">

                        @if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] != "")

                          <?php
                            $unions = App\Model\Union::where('upid', $_REQUEST['upazila_id'])->get();
                          ?>
                          <option value="">Choose One</option>
                          @foreach($unions as $union)
                            <option value="{{$union->id}}"

                              @if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] == $union->id)
                                selected="selected"
                              @endif

                            >{{$union->unname}}</option>
                          @endforeach

                        @else
                          <option>Select Upazila First</option>
                        @endif


                      </select>
                    </div>




                  <div class="col-md-2">
                      <label class="control-label">Project</label>
                      <select class="form-control input-sm" required="required" id="project_id"  name="project_id">
                         <option value="" selected="selected">Select a Project</option>

                     

                          <?php
                                        $projects = App\Model\Project::orderBy('project', 'DESC')->get();
                          ?>
                        
                          @foreach($projects as $project)
                            <option value="{{$project->id}}"

                              @if(isset($_REQUEST['project_id']) && $_REQUEST['project_id'] == $project->id)
                                selected="selected"
                              @endif

                            >{{$project->project}}</option>

                          @endforeach

                      </select>
                    </div>
          

                     <div class="col-md-2">
                      <label class="control-label">Region</label>
                      <select class="form-control input-sm" required="required" id="region_id" name="region_id" >
                
                            <?php
                            $regions = App\Model\Region::orderBy('region_name', 'DESC')->get()
      
                            ?>
                            <option value="">Choose One</option>
                            @foreach($regions as $region)
                              <option value="{{$region->region_id}}"

                                @if(isset($_REQUEST['region_id']) && $_REQUEST['region_id'] == $region->region_id)
                                  selected="selected"
                                @endif
                
                              >{{$region->region_name}}</option>
                            @endforeach
      

                      </select>
                    </div>

                    <div class="col-md-3">
                      <label class="control-label">Starting Date</label>
                      <input
                          type="text"
                          name="starting_date"
                          class="form-control date-picker input-sm"
                          data-date-format="yyyy-mm-dd"

                          @if(count($old) && $old['starting_date'] != "")
                            value="{{date('Y-m-d', strtotime($old['starting_date']))}}"
                          @endif
                          required="required"
                        />
                        <p>YYYY-MM-DD</p>
                    </div>

                    <div class="col-md-3">
                      <label class="control-label">Ending Date</label>
                      <input
                        type="text"
                        name="ending_date"
                        class="form-control date-picker input-sm"
                        data-date-format="yyyy-mm-dd"

                        @if(count($old) && $old['ending_date'] != "")
                          value="{{date('Y-m-d', strtotime($old['ending_date']))}}"
                        @endif
                        required="required"
                      />
                      <p>YYYY-MM-DD</p>
                    </div>


                  </div>

                  <div class="row">
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-5">
                            <input type="submit" class="btn green" value="Get Data" />
                            <a href="{{route('superadmin.finance.report.union.index')}}" class="btn btn-info">Clear</a>

                        </div>
                        <div class="col-md-6">

                            @if($old['union_id'] != "")
                            <?php $old['print'] = 'print'; ?>
                            <a href="{{route('superadmin.finance.report.union.index', $old)}}" class="btn btn-success pull-right">print</a>
                            @endif

                        </div>
                    </div>
                  </div>
              </div>
          </form>

      </div>
    </div>
  </div>