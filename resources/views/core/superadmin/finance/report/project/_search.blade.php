<div class="col-md-12" style="margin-top: 15px;">
    <div class="portlet box blue">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i>Project <small>  Report</small> </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body form">
          <form action="{{route('superadmin.finance.report.project.index')}}" class="form-horizontal" method="GET" id="createForm">
              <input type="hidden" name="query" value="{{time()}}" />

              <div class="form-body">

                  <div class="row">

                  <div class="col-md-3">
                    <label class="control-label">Project</label>
                    <select class="form-control input-sm" required="required" id="project_id" name="project_id">
                      @foreach($projects as $project)
                        <option value="{{$project->id}}"

                          @if(isset($_REQUEST['project_id']) && $_REQUEST['project_id'] == $project->id)
                                selected="selected"
                          @endif


                        >{{$project->project}}</option>
                      @endforeach
                    </select>
                  </div>

                  <div class="col-md-2">
                    <label class="control-label">Show As</label>
                    <select class="form-control input-sm" id="project_id" name="download">
                       <option value="" selected>HTML</option>
                       <option value="download">Download</option>
                    </select>
                  </div>

                  <div class="col-md-2">
                    <label class="control-label ">Starting Date</label>
                    <input
                      type="text"
                      name="starting_date"
                      class="form-control date-picker input-sm"
                      data-date-format="yyyy-mm-dd"
                      @if(count($old) && $old['starting_date'] != "")
                        value="{{date('Y-m-d', strtotime($old['starting_date']))}}"
                      @endif
                    />
                    <p>YYYY-MM-DD</p>
                  </div>

                  <div class="col-md-2">
                    <label class="control-label">Ending Date</label>
                    <input
                      type="text"
                      name="ending_date"
                      class="form-control date-picker input-sm"
                      data-date-format="yyyy-mm-dd"
                      @if(count($old) && $old['ending_date'] != "")
                        value="{{date('Y-m-d', strtotime($old['ending_date']))}}"
                      @endif
                    />
                    <p>YYYY-MM-DD</p>
                  </div>

                  <div class="col-md-3">
                    <p style="margin-bottom: 10px;">&nbsp;</p>
                    <input type="submit" class="btn green btn-sm" value="Get Data" />
                    <a href="{{route('superadmin.finance.report.project.index')}}" class="btn btn-info btn-sm">Clear</a>
                    @if($old['project_id'] != "")
                      <?php $old['download'] = 'download'; ?>
                      <a href="{{route('superadmin.finance.report.project.index', $old)}}" class="btn btn-success pull-right btn-sm">Download</a>
                      @endif
                  </div>



                  </div>

              </div>
          </form>

      </div>
    </div>
  </div>