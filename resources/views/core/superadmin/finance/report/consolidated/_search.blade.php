<div class="col-md-12" style="margin-top: 15px;">
    <div class="portlet box blue">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i>Consolidated <small>  Report</small> </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body form">
          <form action="{{route('superadmin.finance.report.consolidated.index')}}" class="form-horizontal" method="GET" id="createForm">

              <input type="hidden" name="query" value="{{time()}}" />

              <div class="form-body">

                  <div class="row">
                    <div class="col-md-3">
                      <label class="control-label">Starting Date</label>
                      <input
                        type="text"
                        name="starting_date"
                        class="form-control date-picker input-sm"
                        data-date-format="yyyy-mm-dd"
                        @if(count($old) && $old['starting_date'] != "")
                          value="{{date('Y-m-d', strtotime( $old['starting_date']))}}"
                        @endif
                      />
                      <p>YYYY-MM-DD</p>
                    </div>
                    <div class="col-md-3">
                      <label class="control-label">Ending Date</label>
                      <input
                        type="text"
                        name="ending_date"
                        class="form-control date-picker input-sm"
                        data-date-format="yyyy-mm-dd"
                        @if(count($old) && $old['ending_date'] != "")
                          value="{{date('Y-m-d', strtotime( $old['ending_date']))}}"
                        @endif
                      />
                      <p>YYYY-MM-DD</p>
                    </div>
                    <div class="col-md-6">
                      <p style="margin-bottom: 10px;">&nbsp;</p>
                      <input type="submit" class="btn green btn-sm" value="Get Data" />
                      <a href="{{route('superadmin.finance.report.consolidated.index')}}" class="btn btn-info btn-sm">Clear</a>
                      @if(isset($old) && isset($old['query']) && $old['query'] != "")
                        <?php
                          $old2 = $old + ['print' => 'print'];
                          $old3 = $old + ['download' => 'download'];
                        ?>
                      <a href="{{route('superadmin.finance.report.consolidated.index', $old3)}}" class="btn btn-info pull-right btn-sm">Download</a>&nbsp;
                      <a href="{{route('superadmin.finance.report.consolidated.index', $old2)}}" class="btn btn-success pull-right btn-sm">Print</a>
                      @endif
                    </div>
                  </div>

              </div>
          </form>

      </div>
    </div>
  </div>