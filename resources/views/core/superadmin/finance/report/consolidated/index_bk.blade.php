@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('superadmin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="#">Finance</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="#">Report </a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Consolidated Report</span>
    </li>
  </ul>
</div>

@include('partials.errors')

<h1 class="page-title"> Report <small> Consolidated Report</small> </h1>

@include('core.superadmin.finance.report.consolidated._search')

@if($old['query'] != "")
  <div class="col-md-12">
    <div class="portlet light tasks-widget bordered">
      <div class="portlet-body util-btn-margin-bottom-5">


        <div class="page-bar">
          <ul class="page-breadcrumb">

            <li>
              <a href="#">Starting Date: <span style="color:purple">{{$old['starting_date']}}</span></a>
              <i class="fa fa-circle"></i>
            </li>
            <li>
              <a href="#">Ending Date: <span style="color:purple">{{$old['ending_date']}}</span></a>
              <i class="fa fa-circle"></i>
            </li>
            <li>
              <span>Consolidated Report</span>
            </li>
          </ul>
        </div>

        <div class="table-responsive">

          <style>
            #example0 td {
              white-space: nowrap;
              font-size: 10px;
            }
            #example0 th {
              font-size: 10px;
            }
          </style>

          <table class="table table-bordered table-hover table-condensed" id="example0">
            <thead class="flip-content">
              <th colspan="2" >Description</th>
              <th >Approved Budget(A)</th>
              <th >Current Income(B)</th>
              <th >Current Expenses(C)</th>
              <th >Cumulative Income(D)</th>
              <th >Cumulative Expenses(E)</th>
              <th >Budget Balance(F=A-E)</th>
              <th >Demand</th>
            </thead>

            <tbody>

              <?php
                $financeData2 = App\Model\FinanceData::orderBy('proid');

                if($old['starting_date'] != ""){
                  $financeData2 = $financeData2->where('date', '<=', $old['starting_date']);
                }
                $financeData2->get();

                $income_bankob = $financeData2->where('mode', 'bank')->where('trans_type', 'in')->sum('amount');
                $ex_bankob     = $financeData2->where('mode', 'bank')->where('trans_type', 'ex')->sum('amount');
                $bincomeob     = $income_bankob-$ex_bankob;
                $incom_cashob  = $financeData2->where('mode', 'cash')->where('trans_type', 'in')->sum('amount');
                $ex_cashob     = $financeData2->where('mode', 'cash')->where('trans_type', 'ex')->sum('amount');

                $cincomeob     = $incom_cashob - $ex_cashob;
                $open_balance  = $cincomeob + $bincomeob;
              ?>

              <tr>
                <th>Opening Balance</th>
                <td class="text-right">{{$open_balance}}</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>

              <tr>
                <th class="text-right">Cash in Hand</th>
                <td class="text-right">{{$cincomeob}}</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>

              <tr>
                <th class="text-right">Cash at Bank</th>
                <td class="text-right">{{$bincomeob}}</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>

              <?php
                $budget_gr   = 0;
                $income_gr   = 0;
                $ex_gr       = 0;
                $cumin_gr    = 0;
                $cumex_gr    = 0;
                $balance_gr  = 0;
                $demand_gr   = 0;
              ?>

              @foreach($heads as $head)

                <?php
                  $budget_total = 0;
                  $income_total = 0;
                  $ex_total     = 0;
                  $cumin_total  = 0;
                  $cumex_total  = 0;
                  $balance_total= 0;
                  $demand_total = 0;
                ?>

                <tr>
                  <th style="color:#C03; font-size: 10px;">{{$head->headname}}</th>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                  <td>&nbsp;</td>
                </tr>

                @foreach($head->subheads as $subHead)

                  <tr>
                    <th style="color:#099; font-size: 10px; ">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$subHead->sname}}</th>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                  </tr>

                  @foreach($subHead->items as $item)

                    <?php
                      $financeDataSet = $item->financeDatas();
                                                      // App\Model\FinanceData::where('head', $head->id)
                                                      //       ->where('subhead', $subHead->id)
                                                      //       ->where('item', $item->id);

                      if($old['starting_date'] !="" && $old['ending_date'] != "")
                      {
                        $financeDataSet->where('date', '<=', $old['ending_date'])
                                       ->where('date', '>=', $old['starting_date']);
                      }

                      $financeDataSet = $financeDataSet->take(1)->get();

                      if(count($financeDataSet) == 0)
                      {
                        continue;
                      }

                    ?>

                    @foreach($financeDataSet as $fd)
                      <?php

                        $itemBudgetAmount = $itemBudgets->where('itemid', $fd->item)->sum('budget');
                        $demandAmount     = $demands ? $demands->where('item', $fd->item)->sum('amount') : 0;

                        $budget_total     = $budget_total+ $itemBudgetAmount;
                        $demand_total     = $demand_total+$demandAmount;


                        $income     = $financeDataMain->where('item', $fd->item)
                                                      ->where('trans_type', 'in')
                                                      ->where('date', '<=', $old['ending_date'])
                                                      ->where('date', '>=', $old['starting_date'])
                                                      ->sum('amount');

                        $income_total = $income_total + $income;
                        $expense    = $financeDataMain->where('item', $fd->item)
                                                      ->where('trans_type', 'ex')
                                                      ->where('date', '<=', $old['ending_date'])
                                                      ->where('date', '>=', $old['starting_date'])
                                                      ->sum('amount');

                        $ex_total = $ex_total + $expense;
                        $cumIncome = $financeDataMain->where('item', $fd->item)
                                                      ->where('trans_type', 'in')
                                                      ->where('date', '<=', $old['ending_date'])
                                                      ->sum('amount');
                        $cumin_total=$cumin_total + $cumIncome;

                        $cumExpense = $financeDataMain->where('item', $fd->item)
                                                      ->where('trans_type', 'ex')
                                                      ->where('date', '<=', $old['ending_date'])
                                                      ->sum('amount');
                        $cumex_total=$cumex_total + $cumExpense;

                        $budget_balance= $itemBudgetAmount - $cumExpense;
                        $balance_total=$balance_total+$budget_balance;
                      ?>

                      <tr>
                        <th style="color:#60F ; font-size: 10px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{$item->itemname}}</th>
                        <td>&nbsp;</td>
                        <td class="text-right">{{$itemBudgetAmount}}</td>
                        <td class="text-right">{{$income}}</td>
                        <td class="text-right">{{$expense}}</td>
                        <td class="text-right">{{$cumIncome}}</td>
                        <td class="text-right">{{$cumExpense}}</td>
                        <td class="text-right">{{$budget_balance}}</td>
                        <td class="text-right">{{$demandAmount}}</td>

                      </tr>

                    @endforeach
                  @endforeach
                @endforeach


              <tr>
                <th style="color:#000">&nbsp;Subtotal:</th>
                <td>&nbsp;</td>
                <td class="text-right">{{$budget_total}}</td>
                <td class="text-right">{{$income_total}}</td>
                <td class="text-right">{{$ex_total}}</td>
                <td class="text-right">{{$cumin_total}}</td>
                <td class="text-right">{{$cumex_total}}</td>
                <td class="text-right">{{$balance_total}}</td>
                <td class="text-right">{{$demand_total}}</td>
              </tr>

              <?php
                $budget_gr   = $budget_total + $budget_gr;
                $income_gr   = $income_total + $income_gr;
                $ex_gr       = $ex_total + $ex_gr;
                $cumin_gr    = $cumin_total + $cumin_gr;
                $cumex_gr    = $cumex_total + $cumex_gr;
                $balance_gr  = $balance_total + $balance_gr;
                $demand_gr   = $demand_total + $demand_gr;
              ?>

              @endforeach

              <tr>
                <th>Grand Total:</th>
                <td>&nbsp;</td>
                <td class="text-right">{{$budget_gr}}</td>
                <td class="text-right">{{$income_gr}}</td>
                <td class="text-right">{{$ex_gr}}</td>
                <td class="text-right">{{$cumin_gr}}</td>
                <td class="text-right">{{$cumex_gr}}</td>
                <td class="text-right">{{$balance_gr}}</td>
                <td class="text-right">{{$demand_gr}}</td>
              </tr>

              <?php
                $financeData = App\Model\FinanceData::orderBy('proid');
                if($old['ending_date'] != ""){
                  $financeData = $financeData->where('date', '<=', $old['ending_date']);
                }
                $financeData = $financeData->get();

                $income_bank = $financeData->where('trans_type', 'in')->where('mode', 'bank')->sum('amount');
                $ex_bank     = $financeData->where('trans_type', 'ex')->where('mode', 'bank')->sum('amount');
                $bincome     = $income_bank-$ex_bank;
                $incom_cash  = $financeData->where('trans_type', 'in')->where('mode', 'cash')->sum('amount');
                $ex_cash     = $financeData->where('trans_type', 'ex')->where('mode', 'cash')->sum('amount');
                $cincome     = $incom_cash-$ex_cash;
                $closing     = $cincome + $bincome;
                $income_cash = $incom_cash - $ex_cash;
              ?>

              <tr>
                <th>Closing Balance</th>
                <td class="text-right">{{$closing}}</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>

              <tr>
                <th class="text-right">Cash in Hand</th>
                <td class="text-right">{{$income_cash}}</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>

              <tr>
                <th class="text-right">Cash at Bank</th>
                <td class="text-right">{{ $bincome }}</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
              </tr>

            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@else
  @if(!empty($old) && empty($old['query']))
    <div class="col-md-12">
      <div class="portlet light tasks-widget bordered">
        <p>
          Please Select Dates
        </p>
      </div>
    </div>
  @endif
@endif
@endsection
