@extends('layouts.appinside')
@section('content')
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Finance</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Report </a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Consolidated Report</span>
      </li>
    </ul>
  </div>
  @include('partials.errors')
  @include('core.superadmin.finance.report.consolidated._search')
  @include('core.superadmin.finance.report.consolidated._table')
@endsection
