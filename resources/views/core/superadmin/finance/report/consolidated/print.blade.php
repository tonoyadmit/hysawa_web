@extends('layouts.print.app')
@section('my_style')
<style>
    table{
        border-collapse: collapse;
        border-spacing: 0;
        text-align: right;
    }
    div{
        font-size: 80%;
        display: inline-block;
    }
    .headline th {
        padding: 5px 20px;
    }
    .text-right{
        text-align: right;
    }
    .text-left{
        text-align: left;
    }
</style>
<style media="screen">
    .table-filtter .btn{ width: 100%;}
    .table-filtter {
        margin: 20px 0;
    }
    .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
        border: 1px solid #000;
    }
</style>
@endsection

@section('content')
<table class="table table-bordered table-hover" style="width:100%" border="1">

    <tr>
        <td colspan="8" style="text-align:left">
            Consolidated Reportleft
        </td>
    </tr>
    <tr>
        <td colspan="8" style="text-align:left">
            @if(\Auth::check())
            Starting date:{{ $old['starting_date'] ?: '' }}
            @endif
        </td>
    </tr>
    <tr>
        <td colspan="8" style="text-align:left">
            @if(\Auth::check())
            Ending Date: {{ $old['ending_date'] ?: '' }}
            @endif
        </td>
    </tr>
</table>

<table border="1">

  <thead>
    <th>Description</th>
    <th>Approved Budget(A)</th>
    <th>Current Income(B)</th>
    <th>Current Expenses(C)</th>
    <th>Cumulative Income(D)</th>
    <th>Cumulative Expenses(E</th>
    <th>Budget Balance(F=A-E)</th>
    <th>Demand</th>
  </thead>

  <tbody>
  <tr><th style="text-align:right;">Opening Balance</th><th style="text-align:right;">{{ sprintf("%.1lf",$data->total) }}</th><th colspan="8">&nbsp;</th></tr>
  <tr><th style="text-align:right;">Cash in Hand</th><th style="text-align:right;">{{ sprintf("%.1lf",$data->opening_cash) }}</th><th colspan="8">&nbsp;</th></tr>
  <tr><th style="text-align:right;">Cash at Bank</th><th style="text-align:right;">{{ sprintf("%.1lf",$data->opening_bank )}}</th><th colspan="8">&nbsp;</th></tr>
  <?php
    $gbudget=0;
    $gcurrentIncome=0;
    $gcurrentExpenditure=0;
    $gcomulativeIncome=0;
    $gcomulativeBudget=0;
    $gcomulativeExpenditure=0;
    $gdemand=0;

  ?>
  @foreach($d as $list)
  <?php
    $budget=0;
    $currentIncome=0;
    $currentExpenditure=0;
    $comulativeIncome=0;
    $comulativeBudget=0;
    $comulativeExpenditure=0;
    $demand=0;
  ?>
    <tr><td colspan="9" style="text-align:left;font-weight: bold;font-size: 20px;color:#C03" >{{ $list["headname"]}}</td></tr>
    @if (count($list["subhead"]) > 0)
      @foreach($list["subhead"] as $sublist)
      <tr><td colspan="9" style="text-align:left;font-weight: bold;font-size: 16px;color: #099;" >{{ $sublist["sname"]}}</td></tr>
      @if (count($sublist["sub_item"]) > 0)
        @foreach($sublist["sub_item"] as $sub_item)
          <tr>
          <?php

          $data11 = \DB::table('item_budget')
              ->select(DB::raw('SUM(budget) as budget'))
              ->where('itemid', $sub_item["id"])
              ->first();
          $budget+=$cbudget=$data11->budget;

          $data12 = \DB::table('fdata')
            ->select(DB::raw('SUM(amount) as cIncome'))
            ->where('item',$sub_item["id"])
            ->where('trans_type','in')
            ->whereBetween('date', array($old['starting_date'], $old['ending_date']))
            ->first();
          $currentIncome+=$ccurrentIncome=$data12->cIncome;

          $data13=DB::table('fdata')
            ->select(DB::raw('SUM(amount) as cExpenditure'))
            ->where('item',$sub_item["id"])
            ->where('trans_type','ex')
            ->whereBetween('date', array($old['starting_date'], $old['ending_date']))
            ->first();
          $currentExpenditure+=$ccurrentExpenditure = $data13->cExpenditure;

          $data14=DB::table('fdata')
            ->select(DB::raw('SUM(amount) as cIncome'))
            ->where('item',$sub_item["id"])
            ->where('trans_type','in')
            ->whereDate('date','<=',$old['ending_date'])
            ->first();
          $comulativeIncome+=$ccomulativeIncome=$data14->cIncome;

          $data15=DB::table('fdata')
            ->select(DB::raw('SUM(amount) as cExpenditure'))
            ->where('item',$sub_item["id"])
            ->whereDate('date','<=',$old['ending_date'])
            ->where('trans_type','ex')
            ->first();
          $comulativeExpenditure+=$ccomulativeExpenditure=$data15->cExpenditure;

          $comulativeBudget += $ccomulativeBudget = ($data11->budget - $data15->cExpenditure) ;

          $data16=DB::table('demand')
            ->select('amount')
            ->where('item', $sub_item["id"])
            ->first();
          $demand += @$cdemand= $data16->amount;

          ?>
          <td style="text-align:left">{{ $sub_item["itemname"]}}</td>
          <td>&nbsp;</td>
          <td style="text-align:right;">{{ sprintf("%.1lf",$cbudget) }}</td>
          <td style="text-align:right;">{{ sprintf("%.1lf",$ccurrentIncome) }}</td>
          <td style="text-align:right;">{{ sprintf("%.1lf",$ccurrentExpenditure) }}</td>
          <td style="text-align:right;">{{ sprintf("%.1lf",$ccomulativeIncome) }}</td>
          <td style="text-align:right;">{{ sprintf("%.1lf",$ccomulativeExpenditure) }}</td>
          <td style="text-align:right;">{{ sprintf("%.1lf",$ccomulativeBudget) }}</td>
          <td style="text-align:right;">{{ sprintf("%.1lf",$cdemand) }}</td>
          </tr>
        @endforeach
      @endif
    @endforeach
    <tr>
      <th style="text-align:right">Sub Total</th>
      <td>&nbsp;</td>
      <td style="text-align:right;">{{ sprintf("%.1lf",$budget) }}</td>
      <td style="text-align:right;">{{ sprintf("%.1lf",$currentIncome) }}</td>
      <td style="text-align:right;">{{ sprintf("%.1lf",$currentExpenditure) }}</td>
      <td style="text-align:right;">{{ sprintf("%.1lf",$comulativeIncome) }}</td>
      <td style="text-align:right;">{{ sprintf("%.1lf",$comulativeExpenditure) }}</td>
      <td style="text-align:right;">{{ sprintf("%.1lf",$comulativeBudget) }}</td>
      <td style="text-align:right;">{{ sprintf("%.1lf",$demand) }}</td>
    </tr>
    <?php
      $gbudget+=$budget;
      $gcurrentIncome+=$currentIncome;
      $gcurrentExpenditure+=$currentExpenditure;
      $gcomulativeIncome+=$comulativeIncome;
      $gcomulativeExpenditure+=$comulativeExpenditure;
      $gcomulativeBudget+=$comulativeBudget;
      $gdemand+=$demand+=$cdemand;
    ?>
    @endif
  @endforeach
    <tr>
        <th style="text-align:right;">Grand Total</th>
        <td>&nbsp;</td>
        <td style="text-align:right;">{{ sprintf("%.1lf",$gbudget) }}</td>
        <td style="text-align:right;">{{ sprintf("%.1lf",$gcurrentIncome) }}</td>
        <td style="text-align:right;">{{ sprintf("%.1lf",$gcurrentExpenditure) }}</td>
        <td style="text-align:right;">{{ sprintf("%.1lf",$gcomulativeIncome) }}</td>
        <td style="text-align:right;">{{ sprintf("%.1lf",$gcomulativeExpenditure) }}</td>
        <td style="text-align:right;">{{ sprintf("%.1lf",$gcomulativeBudget) }}</td>
        <td style="text-align:right;">{{ sprintf("%.1lf",$gdemand) }}</td>
    </tr>
    <tr><th style="text-align:right;">Clossing Balance</th><th style="text-align:right;">{{ sprintf("%.1lf", $data2->total)  }}</th><th colspan="8">&nbsp;</th></tr>
    <tr><th style="text-align:right;">Cash in Hand</th><th style="text-align:right;">{{ sprintf("%.1lf", $data2->opening_cash) }}</th><th colspan="8">&nbsp;</th></tr>
    <tr><th style="text-align:right;">Cash at Bank</th><th style="text-align:right;">{{ sprintf("%.1lf", $data2->opening_bank) }}</th><th colspan="8">&nbsp;</th></tr>

  </tbody>
</table>








    @endsection