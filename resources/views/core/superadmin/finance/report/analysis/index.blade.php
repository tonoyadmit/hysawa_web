@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('superadmin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="#">Finance</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="#">Report </a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Analysis Report</span>
    </li>
  </ul>
</div>

@include('partials.errors')

@include('core.superadmin.finance.report.analysis._search')

@if($old['project_id'] != "")

<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <div class="portlet-body util-btn-margin-bottom-5">


      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li>
            <a href="#">Project: <span style="color:purple"> {{$old['projectName']}}</span></a>
            <i class="fa fa-circle"></i>
          </li>
          <li>
            <a href="#">Starting Date: <span style="color:purple">{{$old['starting_date']}}</span></a>
            <i class="fa fa-circle"></i>
          </li>
          <li>
            <a href="#">Ending Date: <span style="color:purple">{{$old['ending_date']}}</span></a>
            <i class="fa fa-circle"></i>
          </li>
          <li>
            <span>Analysis Report</span>
          </li>
        </ul>
      </div>

      <div class="table-responsive">
        <style>
          #example0 td {
            white-space: nowrap;
            font-size: 10px;
          }
          #example0 th {
            font-size: 10px;
          }
        </style>

        <table class="table table-bordered table-hover table-condensed" id="example0">
          <thead class="flip-content">

            <tr class="fbold">
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td colspan="4" class="text-center">HYSAWA Disbursements</td>
              <td colspan="6" class="text-center">UP Statement</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="text-center">Total</td>
              <td class="text-center">Hardware</td>
              <td class="text-center">PNGO</td>
              <td class="text-center">Others</td>
              <td class="text-center">HYSAWA</td>
              <td class="text-center">Comm. Contri</td>
              <td class="text-center">Oth. Income</td>
              <td class="text-center">TOTAL INCOME</td>
              <td class="text-center">TOTAL EXP.</td>
              <td class="text-center">BALANCE</td>
            </tr>
          </thead>

          <tbody>

          <?php

            // exit();
               $data1_1 = 0;
               $data1_2 = 0;
               $data1_3 = 0;
               $data1_4 = 0;

               $data2_1 = 0;
               $data2_2 = 0;
               $data2_3 = 0;
               $data2_4 = 0;
               $data2_5 = 0;
               $data2_6 = 0;

          ?>

          @foreach($unions as $union)
            <tr>
              <td>{{$union->distname}}</td>
              <td>{{$union->upname}}</td>
              <td>{{$union->unname}}</td>
              <?php
               $data1 = App\Model\Budget::data1($union->unid,  $sql);
               $data2 = App\Model\Budget::data2($union->unid, $sql2);

               //dd($data1);
               // exit();
               $data1_1 += $data1[0]->tdesbus;
               $data1_2 += $data1[0]->cdf_no;
               $data1_3 += $data1[0]->cdf_no2;
               $data1_4 += $data1[0]->cdf_no3;

               $data2_1 += $data2[0]->amount;
               $data2_2 += $data2[0]->amount2;
               $data2_3 += $data2[0]->amount3;
               $data2_4 += $data2[0]->amount4;
               $data2_5 += $data2[0]->amount5;
               $data2_6 += ($data2[0]->amount - $data2[0]->amount3);

              ?>
              <td class="text-center">{{$data1[0]->tdesbus}}</td>
              <td class="text-center">{{$data1[0]->cdf_no}}</td>
              <td class="text-center">{{$data1[0]->cdf_no2}}</td>
              <td class="text-center">{{$data1[0]->cdf_no3}}</td>

              <td class="text-center">{{$data2[0]->amount}}</td>
              <td class="text-center">{{$data2[0]->amount2}}</td>
              <td class="text-center">{{$data2[0]->amount3}}</td>
              <td class="text-center">{{$data2[0]->amount4}}</td>
              <td class="text-center">{{$data2[0]->amount5}}</td>
              <td class="text-center">{{$data2[0]->amount - $data2[0]->amount3}}</td>
            </tr>

          @endforeach

          </tbody>
        </table>


        <h3>Summary</h3>

        <table class="table table-bordered table-hover table-condensed" id="example0">
          <thead class="flip-content">

            <tr class="fbold">
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td colspan="4" class="text-center">HYSAWA Disbursements</td>
              <td colspan="6" class="text-center">UP Statement</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="text-center">Total</td>
              <td class="text-center">Hardware</td>
              <td class="text-center">PNGO</td>
              <td class="text-center">Others</td>
              <td class="text-center">HYSAWA</td>
              <td class="text-center">Comm. Contri</td>
              <td class="text-center">Oth. Income</td>
              <td class="text-center">TOTAL INCOME</td>
              <td class="text-center">TOTAL EXP.</td>
              <td class="text-center">BALANCE</td>
            </tr>
          </thead>

          <tbody>


            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td class="text-center">{{$data1_1}}</td>
              <td class="text-center">{{$data1_2}}</td>
              <td class="text-center">{{$data1_3}}</td>
              <td class="text-center">{{$data1_4}}</td>

              <td class="text-center">{{$data2_1}}</td>
              <td class="text-center">{{$data2_2}}</td>
              <td class="text-center">{{$data2_3}}</td>
              <td class="text-center">{{$data2_4}}</td>
              <td class="text-center">{{$data2_5}}</td>
              <td class="text-center">{{$data2_6}}</td>
            </tr>


          </tbody>
        </table>



      </div>
    </div>
  </div>
</div>

@else
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <p>
      No Data Found
    </p>
  </div>
</div>
@endif
@endsection
