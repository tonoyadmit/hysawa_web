<div class="col-md-12" style="margin-top: 15px;">
    <div class="portlet box blue">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i>Analysis <small>Report</small> </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body form">
          <form action="{{route('superadmin.finance.report.analysis.index')}}" class="form-horizontal" method="GET" id="createForm">

              <input type="hidden" name="query" value="{{time()}}" />

              <div class="form-body">

                <div class="row">
                  <div class="col-md-3">
                    <label class="control-label">Project *</label>
                    <select class="form-control input-sm" required="required" id="project_id" name="project_id" required="required">
                      <option value="">Choose One</option>
                      @foreach($projects as $project)
                        <option value="{{$project->id}}"

                        @if(isset($_REQUEST['project_id']) && $project->id == $_REQUEST['project_id'])
                          selected="selected"
                        @endif
                        >{{$project->project}}</option>
                      @endforeach
                    </select>
                  </div>

                  <div class="col-md-2">
                    <label class="control-label">Starting Date *</label>
                    <input
                        type="text"
                        name="starting_date"
                        class="form-control date-picker input-sm"
                        data-date-format="yyyy-mm-dd"

                        @if(isset($_REQUEST['starting_date']) && $_REQUEST['starting_date'] != "")
                          value="{{date('Y-m-d', strtotime($_REQUEST['starting_date']))}}"
                        @endif

                        required="required"
                      />
                      <p>YYYY-MM-DD</p>
                  </div>

                  <div class="col-md-2">
                    <label class="control-label">Ending Date *</label>
                    <input
                      type="text"
                      name="ending_date"
                      class="form-control date-picker input-sm"
                      data-date-format="yyyy-mm-dd"
                      @if(isset($_REQUEST['ending_date']) && $_REQUEST['ending_date'] != "")
                        value="{{date('Y-m-d', strtotime($_REQUEST['ending_date']))}}"
                      @endif
                      required="required"
                    />
                    <p>YYYY-MM-DD</p>
                  </div>


                  <div class="col-md-3">
                    <br/>
                    <input type="submit" class="btn green" value="Get Data" />
                    <a href="{{route('superadmin.finance.report.analysis.index')}}" class="btn btn-info">Clear</a>

                    @if($old['project_id'] != "")
                    <?php $old['download'] = 'download'; ?>
                    <a href="{{route('superadmin.finance.report.analysis.index', $old)}}" class="btn btn-success pull-right">Download</a>
                    @endif
                  </div>
                </div>

              </div>
          </form>

      </div>
    </div>
  </div>