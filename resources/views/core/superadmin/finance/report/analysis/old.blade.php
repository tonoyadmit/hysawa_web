<?php require_once('admin_header.php'); ?>
<?php require_once('Connections/db_con.php'); ?>
<?php
if (!function_exists("GetSQLValueString")) {
  function GetSQLValueString($theValue, $theType, $theDefinedValue = "", $theNotDefinedValue = "")
  {
    if (PHP_VERSION < 6) {
      $theValue = get_magic_quotes_gpc() ? stripslashes($theValue) : $theValue;
    }

    $theValue = function_exists("mysql_real_escape_string") ? mysql_real_escape_string($theValue) : mysql_escape_string($theValue);

    switch ($theType) {
      case "text":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
      case "long":
      case "int":
      $theValue = ($theValue != "") ? intval($theValue) : "NULL";
      break;
      case "double":
      $theValue = ($theValue != "") ? doubleval($theValue) : "NULL";
      break;
      case "date":
      $theValue = ($theValue != "") ? "'" . $theValue . "'" : "NULL";
      break;
      case "defined":
      $theValue = ($theValue != "") ? $theDefinedValue : $theNotDefinedValue;
      break;
    }
    return $theValue;
  }
}

mysql_select_db($database_db_con, $db_con);
$query_project = "SELECT * FROM project";

$project = mysql_query($query_project, $db_con) or die(mysql_error());
$row_project = mysql_fetch_assoc($project);
$totalRows_project = mysql_num_rows($project);

$colname_mou = "-1";

if (isset($_POST['project'])) {
  $colname_mou = $_POST['project'];
}

mysql_select_db($database_db_con, $db_con);

$query_mou = sprintf("
  SELECT
  fdistrict.id,
  fdistrict.distname,
  fupazila.id,
  fupazila.upname,
  funion.id as unid,
  funion.unname
  FROM
  osm_mou, fdistrict, fupazila, funion
  WHERE
  osm_mou.projid = %s and
  osm_mou.distid = fdistrict.id  and
  osm_mou.upid = fupazila.id and
  osm_mou.unid = funion.id", GetSQLValueString($colname_mou, "int"));

$mou = mysql_query($query_mou, $db_con) or die(mysql_error());
$row_mou = mysql_fetch_assoc($mou);
$totalRows_mou = mysql_num_rows($mou);

$colname_selproj = "-1";
if (isset($_POST['project'])) {
  $colname_selproj = $_POST['project'];
}
mysql_select_db($database_db_con, $db_con);

$query_selproj  = sprintf("SELECT * FROM project WHERE id = %s", GetSQLValueString($colname_selproj, "int"));
$selproj        = mysql_query($query_selproj, $db_con) or die(mysql_error());
$row_selproj    = mysql_fetch_assoc($selproj);
$totalRows_selproj = mysql_num_rows($selproj);

?>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <title>UP Financial Analysis</title>
  <script src="SpryAssets/SpryValidationTextField.js" type="text/javascript"></script>
  <link href="SpryAssets/SpryValidationTextField.css" rel="stylesheet" type="text/css" />
  <link href="css/Level3_3.css" rel="stylesheet" type="text/css" />
  <style type="text/css">
    .bluefont {
      color: #00F;
    }
    .magentafont {
      color: #609;
    }
  </style>
</head>




<body>
  <form id="form1" name="form1" method="post" action="">
    <label for="project"><br />
      Select Project:</label>

      {{-- <select name="project" id="project">
        <?php
        do {
          ?>
          <option value="<?php echo $row_project['id']?>"<?php
            if (!(strcmp($row_project['id'], $row_selproj['id']))) {echo "selected=\"selected\"";} ?>><?php
            echo $row_project['project']?>

          </option>
          <?php
        } while ($row_project = mysql_fetch_assoc($project));

        $rows = mysql_num_rows($project);
        if($rows > 0) {
          mysql_data_seek($project, 0);
          $row_project = mysql_fetch_assoc($project);
        }
        ?>
      </select>


      <label for="from">Enter date (yyyy-mm-dd) : From</label>
      <span id="sprytextfield1">
        <input name="from" type="text" id="from" value="<?php echo $_POST['from'] ?>" />
        <span class="textfieldRequiredMsg">A value is required.</span>
        <span class="textfieldInvalidFormatMsg">Invalid format.</span></span>
        <label for="to">to </label>
        <span id="sprytextfield2">
          <input name="to" type="text" id="to" value="<?php echo $_POST['to'] ?>" />
          <span class="textfieldRequiredMsg">A value is required.</span>
          <span class="textfieldInvalidFormatMsg">Invalid format.</span></span>
          <input type="submit" value="Submit" />
        </form>
        <p>&nbsp;Project Name: <span class="fbold"><?php echo $row_selproj['project']; ?></span>
          UP related Financial Statement between <?php echo $_POST[from]; ?> and <?php echo $_POST[to]; ?></p>


 --}}




          <table border="1">
            <tr class="fbold">
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td colspan="4" align="center">HYSAWA Disbursements</td>
              <td colspan="6" align="center">UP Statement</td>
            </tr>
            <tr>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td>&nbsp;</td>
              <td align="right">Total</td>
              <td align="right">Hardware</td>
              <td align="right">PNGO</td>
              <td align="right">Others</td>
              <td align="right">HYSAWA</td>
              <td align="right">Comm. Contri</td>
              <td align="right">Oth. Income</td>
              <td align="right">TOTAL INCOME</td>
              <td align="right">TOTAL EXP.</td>
              <td align="right">BALANCE</td>
            </tr>
            <?php do { ?>
            <tr>
              <td nowrap="nowrap"><?php echo $row_mou['distname']; ?></td>
              <td nowrap="nowrap"><?php echo $row_mou['upname']; ?></td>
              <td nowrap="nowrap"><a href="hysawa_disb.php?unid=<?php echo $row_mou['unid']; ?>&amp;project=<?php echo $row_selproj['id']; ?>&amp;from=<?php echo $_POST['from']; ?>&amp;to=<?php echo $_POST['to']; ?>"><?php echo $row_mou['unname']; ?></a></td>
              <td align="right" class="redfont"><?php $tdesbusq=mysql_query("select sum(desbus)as tdesbus from budget_view where desdate between '$_POST[from]' and '$_POST[to]' and proid = '$_POST[project]' and unid = '$row_mou[unid]'");
                $desbus_row=mysql_fetch_array($tdesbusq);
                echo number_format ($desbus_row[tdesbus])."<br>";
                ?>

              </td>
              <td align="right"><?php $tdesbusq=mysql_query("select sum(desbus)as tdesbus from budget_view where headid = 9 and desdate between '$_POST[from]' and '$_POST[to]' and proid = '$_POST[project]' and unid = '$row_mou[unid]'");
                $desbus_row=mysql_fetch_array($tdesbusq);
                echo number_format ($desbus_row[tdesbus])."<br>";
                ?>

              </td>
              <td align="right"><?php $tdesbusq=mysql_query("select sum(desbus)as tdesbus from budget_view where headid = 13 and desdate between '$_POST[from]' and '$_POST[to]' and proid = '$_POST[project]' and unid = '$row_mou[unid]'");
                $desbus_row=mysql_fetch_array($tdesbusq);
                echo number_format ($desbus_row[tdesbus])."<br>";
                ?>

              </td>
              <td align="right"><?php $tdesbusq=mysql_query("select sum(desbus)as tdesbus from budget_view where headid  IN (4,5,6,7,8,10) and desdate between '$_POST[from]' and '$_POST[to]' and proid = '$_POST[project]' and unid = '$row_mou[unid]'");
                $desbus_row=mysql_fetch_array($tdesbusq);
                echo number_format ($desbus_row[tdesbus])."<br>";
                ?>

              </td>
              <td align="right" class="redfont"><?php $tdesbusq1=mysql_query("select sum(amount)as tdesbus1 from fdata where `head` IN (4,5,6,7,8,9,10,13)  and trans_type = 'in' and date between '$_POST[from]' and '$_POST[to]' and proid = '$_POST[project]' and unid = '$row_mou[unid]'");
                $desbus_row1=mysql_fetch_array($tdesbusq1);
                echo number_format ($desbus_row1[tdesbus1])."<br>";
                ?>

              </td>
              <td align="right"><?php $tdesbusq2=mysql_query("select sum(amount)as tdesbus2 from fdata where item = 36 and trans_type = 'in' and date between '$_POST[from]' and '$_POST[to]' and proid = '$_POST[project]' and unid = '$row_mou[unid]'");
                $desbus_row2=mysql_fetch_array($tdesbusq2);
                echo number_format ($desbus_row2[tdesbus2])."<br>";
                ?>

              </td>
              <td align="right"><?php $tdesbusq3=mysql_query("select sum(amount)as tdesbus3 from fdata where head = 12 and trans_type = 'in' and date between '$_POST[from]' and '$_POST[to]' and proid = '$_POST[project]' and unid = '$row_mou[unid]'");
                $desbus_row3=mysql_fetch_array($tdesbusq3);
                echo number_format($desbus_row3[tdesbus3] -$desbus_row2[tdesbus2])."<br>";
                ?>

              </td>
              <td align="right"><span class="bluefont">
                <?php $tdesbusq4=mysql_query("select sum(amount)as tdesbus4 from fdata where `head` IN (4,5,6,7,8,9,10,12,13)  and trans_type = 'in' and date between '$_POST[from]' and '$_POST[to]' and proid = '$_POST[project]' and unid = '$row_mou[unid]'");
                $desbus_row4=mysql_fetch_array($tdesbusq4);
                echo number_format ($desbus_row4[tdesbus4])."<br>";
                ?>
              </span>
            </td>
            <td align="right" class="magentafont"><?php $tdesbusq4=mysql_query("select sum(amount)as tdesbus4 from fdata where `head` IN (4,5,6,7,8,9,10,12,13)  and trans_type = 'ex' and date between '$_POST[from]' and '$_POST[to]' and proid = '$_POST[project]' and unid = '$row_mou[unid]'");
              $desbus_row4=mysql_fetch_array($tdesbusq4);
              echo number_format ($desbus_row4[tdesbus4])."<br>";
              ?>

            </td>
            <td align="right" class="bluefont"><?php echo number_format ($desbus_row1[tdesbus1] +$desbus_row3[tdesbus3]-$desbus_row4[tdesbus4])."<br>";
              ?>

            </td>
          </tr>
          <?php } while ($row_mou = mysql_fetch_assoc($mou)); ?>
        </table>
        <p>&nbsp;</p>

        <table border="1">
          <tr class="fbold">
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td colspan="4" align="center">HYSAWA Disbursements</td>
            <td colspan="6" align="center">UP Statement</td>
          </tr>
          <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="right">Total</td>
            <td align="right">Hardware</td>
            <td align="right">PNGO</td>
            <td align="right">Others</td>
            <td align="right">HYSAWA</td>
            <td align="right">Comm. Contri</td>
            <td align="right">Oth. Income</td>
            <td align="right">TOTAL INCOME</td>
            <td align="right">TOTAL EXP.</td>
            <td align="right">BALANCE</td>
          </tr>
          <tr>
            <td>******************</td>
            <td>******************</td>
            <td>****************</td>
            <td align="right"><span class="redfont">
              <?php $tdesbusq5=mysql_query("select sum(desbus)as tdesbus5 from budget_view where desdate between '$_POST[from]' and '$_POST[to]' and proid = '$_POST[project]'");
              $desbus_row5=mysql_fetch_array($tdesbusq5);
              echo number_format ($desbus_row5[tdesbus5])."<br>";
              ?>
            </span>
          </td>
          <td align="right"><?php $tdesbusq6=mysql_query("select sum(desbus)as tdesbus6 from budget_view where headid = 9 and desdate between '$_POST[from]' and '$_POST[to]' and proid = '$_POST[project]'");
            $desbus_row6=mysql_fetch_array($tdesbusq6);
            echo number_format ($desbus_row6[tdesbus6])."<br>";
            ?>

          </td>
          <td align="right"><?php $tdesbusq7=mysql_query("select sum(desbus)as tdesbus7 from budget_view where headid = 13 and desdate between '$_POST[from]' and '$_POST[to]' and proid = '$_POST[project]' ");
            $desbus_row7=mysql_fetch_array($tdesbusq7);
            echo number_format ($desbus_row7[tdesbus7])."<br>";
            ?>

          </td>
          <td align="right"><?php $tdesbusq8=mysql_query("select sum(desbus)as tdesbus8 from budget_view where headid  IN (4,5,6,7,8,10) and desdate between '$_POST[from]' and '$_POST[to]' and proid = '$_POST[project]' ");
            $desbus_row8=mysql_fetch_array($tdesbusq8);
            echo number_format ($desbus_row8[tdesbus8])."<br>";
            ?>

          </td>
          <td align="right"><span class="redfont">
            <?php $tdesbusq9=mysql_query("select sum(amount)as tdesbus9 from fdata where `head` IN (4,5,6,7,8,9,10,13)  and trans_type = 'in' and date between '$_POST[from]' and '$_POST[to]' and proid = '$_POST[project]'");
            $desbus_row9=mysql_fetch_array($tdesbusq9);
            echo number_format ($desbus_row9[tdesbus9])."<br>";
            ?>
          </span>
        </td>
        <td align="right"><?php $tdesbusq10=mysql_query("select sum(amount)as tdesbus10 from fdata where item = 36 and trans_type = 'in' and date between '$_POST[from]' and '$_POST[to]' and proid = '$_POST[project]' ");
          $desbus_row10=mysql_fetch_array($tdesbusq10);
          echo number_format ($desbus_row10[tdesbus10])."<br>";
          ?>

        </td>
        <td align="right"><?php $tdesbusq11=mysql_query("select sum(amount)as tdesbus11 from fdata where head = 12 and trans_type = 'in' and date between '$_POST[from]' and '$_POST[to]' and proid = '$_POST[project]' ");
          $desbus_row11=mysql_fetch_array($tdesbusq11);
          echo number_format($desbus_row11[tdesbus11] -$desbus_row10[tdesbus10])."<br>";
          ?>

        </td>
        <td align="right"><span class="bluefont">
          <?php $tdesbusq12=mysql_query("select sum(amount)as tdesbus12 from fdata where `head` IN (4,5,6,7,8,9,10,12,13)  and trans_type = 'in' and date between '$_POST[from]' and '$_POST[to]' and proid = '$_POST[project]' ");
          $desbus_row12=mysql_fetch_array($tdesbusq12);
          echo number_format ($desbus_row12[tdesbus12])."<br>";
          ?>
        </span>
      </td>
      <td align="right"><span class="magentafont">
        <?php $tdesbusq13=mysql_query("select sum(amount)as tdesbus13 from fdata where `head` IN (4,5,6,7,8,9,10,12,13)  and trans_type = 'ex' and date between '$_POST[from]' and '$_POST[to]' and proid = '$_POST[project]' ");
        $desbus_row13=mysql_fetch_array($tdesbusq13);
        echo number_format ($desbus_row13[tdesbus13])."<br>";
        ?>
      </span>
    </td>
    <td align="right"><span class="bluefont"><?php echo number_format ($desbus_row9[tdesbus9] +$desbus_row11[tdesbus11]-$desbus_row13[tdesbus13])."<br>";
      ?></span>
    </td>
  </tr>
  <?php do { ?>
  <?php } while ($row_mou = mysql_fetch_assoc($mou)); ?>
</table>



<p>&nbsp;</p>
<p>&nbsp; </p>
<script type="text/javascript">
  var sprytextfield1 = new Spry.Widget.ValidationTextField("sprytextfield1", "date", {format:"yyyy-mm-dd"});
  var sprytextfield2 = new Spry.Widget.ValidationTextField("sprytextfield2", "date", {format:"yyyy-mm-dd"});
</script>
</body>
</html>
<?php
mysql_free_result($project);
mysql_free_result($mou);
mysql_free_result($selproj);
?>
