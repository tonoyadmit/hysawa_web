@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Finance</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Disbursment To UP </a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Date wise List</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

  @if(count($budgets) > 0)

  <div class="col-md-12" style="margin-top: 10px;">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          <i class="fa fa-gift"></i>Disbursment <small> Date wise list</small>
        </div>

        <div class="actions">
          <a href="{{route('superadmin.finance.disbursment-to-up.disbursment.index', ['download' => 'download'])}}" target="_blank" class="btn btn-circle success"> <i class="fa fa-download"></i> Download</a>
        </div>

        <div class="tools">
          <a href="javascript:;" class="collapse"> </a>
        </div>
      </div>

      <div class="portlet-body">
        <div class="table-responsive">
          <style>
            #example0 th, #example0 td {
              white-space: nowrap;
            }
          </style>
          <table class="table table-bordered table-hover" id="example0">
            <thead class="flip-content">
              <th style="font-size: 10px;">Action</th>
              <th style="font-size: 10px;">Entry Date</th>
              <th style="font-size: 10px;">Dis Date</th>

              <th style="font-size: 10px;">District</th>
              <th style="font-size: 10px;">Upazila</th>
              <th style="font-size: 10px;">Union</th>

              <th style="font-size: 10px;">Head</th>
              <th style="font-size: 10px;">SubHead</th>
              <th style="font-size: 10px;">Item</th>

              <th style="font-size: 10px;">Voucher</th>
              <th style="font-size: 10px;">Mode</th>
              <th style="font-size: 10px;">Amount</th>
              <th style="font-size: 10px;">Remarks</th>


            </thead>
            <tbody>
              @foreach($budgets as $budget)
              <tr>
                <td style="font-size: 10px;">
                  <a class="label label-success" href="#">
                    <i class="fa fa-pencil"></i> Print
                  </a>
                </td>
                <td style="font-size: 10px;">{{date('d-m-Y', strtotime($budget->entry_date))}}</td>
                <td style="font-size: 10px;">{{date('d-m-Y', strtotime($budget->date))}}</td>

                <td style="font-size: 10px;">{{$budget->district }}</td>
                <td style="font-size: 10px;">{{$budget->upazila }}</td>
                <td style="font-size: 10px;">{{$budget->union }}</td>

                <td style="font-size: 10px;">{{$budget->head }}</td>
                <td style="font-size: 10px;">{{$budget->subhead }}</td>
                <td style="font-size: 10px;">{{$budget->item}}</td>

                <td style="font-size: 10px;">{{$budget->voucher }}</td>
                <td style="font-size: 10px;">{{$budget->mode }}</td>
                <td style="font-size: 10px; text-align: right">{{sprintf("%.2lf", $budget->amount)}}</td>
                <td style="font-size: 10px;">{{$budget->remarks}}</td>


              </tr>
              @endforeach
            </tbody>
          </table>

          <div class="pagination pull-right">
            {{$budgets->links()}}
          </div>
        </div>

      </div>
    </div>
  </div>



  @else
    <div class="col-md-12" style="margin-top: 10px;">
      <div class="portlet light tasks-widget bordered">
         <p>
            No Data Found
         </p>
      </div>
    </div>
  @endif
@endsection



