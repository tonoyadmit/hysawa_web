@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Finance</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Disbursment To UP </a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="{{route('superadmin.finance.disbursment-to-up.disbursment.index')}}">Disbursments </a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Add New</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

  <h1 class="page-title"> Disbursment <small> Add new Item</small> </h1>

  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i>Add New </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body form">
          <form action="{{route('superadmin.finance.disbursment-to-up.disbursment.update', $budget->id)}}" class="form-horizontal" method="POST" id="createForm">

              {{csrf_field()}}

              <input type="hidden" name="query" value="{{time()}}" />

              <div class="form-body">

                  <div class="row">
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">District</label>
                              <div class="col-md-9">
                                  <select name="distname" class="form-control" required="required" id="district_id">
                                    @foreach($districts as $district)
                                      <option value="{{$district->id}}"
                                      @if($district->id == $budget->distid)
                                        selected="selected"
                                      @endif
                                      >{{$district->distname}}</option>
                                    @endforeach
                                  </select>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">Upazila</label>
                              <div class="col-md-9">
                                  <select name="Upname" class="form-control" required="required" id="upazila_id">
                                    @foreach($upazilas as $upazila)
                                      <option
                                        value="{{$upazila->id}}"
                                        @if($upazila->id == $budget->upid)
                                          selected="selected"
                                        @endif
                                      >{{$upazila->upname}}</option>
                                    @endforeach
                                  </select>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">Union</label>
                              <div class="col-md-9">
                                <select name="union_id" class="form-control" required="required" id="union_id">
                                  @foreach($unions as $union)
                                      <option value="{{$union->id}}"
                                      @if($union->id == $budget->unid)
                                        selected="selected"
                                      @endif
                                      >{{$union->unname}}</option>
                                    @endforeach
                                </select>
                              </div>
                          </div>
                      </div>
                  </div>




                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Head</label>
                              <div class="col-md-9">
                                  <select class="form-control" required="required" id="head_id" name="headid">
                                    @foreach($heads as $head)
                                      <option value="{{$head->id}}"

                                      @if($head->id == $headId)
                                        selected ="selected"
                                      @endif

                                      >{{$head->headname}}</option>
                                    @endforeach
                                  </select>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">SubHead</label>
                              <div class="col-md-9">
                                  <select class="form-control" required="required" id="subhead_id">
                                    @foreach($subHeads as $subHead)
                                      <option value="{{$subHead->id}}"

                                      @if($subHead->id == $subHeadId)
                                        selected ="selected"
                                      @endif

                                      >{{$subHead->sname}}</option>
                                    @endforeach
                                  </select>
                              </div>
                          </div>
                      </div>
                  </div>


                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Item</label>
                              <div class="col-md-9">
                                  <select class="form-control" required="required" id="item_id" name="itemid">
                                    @foreach($items as $item)
                                      <option value="{{$item->id}}"

                                      @if($item->id == $budget->itemid)
                                        selected="selected"
                                      @endif

                                      >{{$item->itemname}}</option>
                                    @endforeach
                                  </select>
                              </div>
                          </div>
                      </div>
                  </div>


                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Amount</label>
                              <div class="col-md-9">
                                  <input type="text" class="form-control" required="required" name="amount"

                                      value="{{$budget->desbus}}"

                                   />
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Method of Payment</label>
                              <div class="col-md-9">
                                  <select class="form-control" required="required" id="item_id" name="mode">
                                    <option value="Cheque" @if($budget->mode == "Cheque") selected="selected" @endif >Cheque</option>
                                    <option value="Cash"   @if($budget->mode == "Cash") selected="selected" @endif >Cash</option>
                                    <option value="TT"     @if($budget->mode == "TT") selected="selected" @endif >TT</option>
                                  </select>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Voucher</label>
                              <div class="col-md-9">
                                  <input type="text" class="form-control" required="required" name="vou"

                                      value="{{$budget->vou}}"

                                   />
                              </div>
                          </div>
                      </div>
                  </div>


                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Disburse Date</label>
                              <div class="col-md-9">
                                  <input
                                    type="date"
                                    class="form-control date-picker"
                                    required="required"
                                    name="date"
                                    data-date-format="yyyy-mm-dd" readonly
                                      value="{{date('Y-m-d', strtotime($budget->desdate))}}"

                                   />
                              </div>
                          </div>
                      </div>
                  </div>


                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Remarks</label>
                              <div class="col-md-9">
                                  <textarea class="form-control" name="remarks" row="3">{{$budget->remarks}}</textarea>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-6">
                            <input type="submit" class="btn green" value="Update " />
                        </div>
                    </div>
                  </div>

              </div>
          </form>

      </div>
    </div>
  </div>

@endsection

@section('my_js')
<script type="text/javascript">
  $(document).ready(function () {

      $('#district_id').on('change', function() {
          var head_id = $(this).val();

          $("#upazila_id").html("Select District");
          $("#union_id").html("Select Upazila");

          $.ajax({
            url: '{{route('superadmin.ajax.upazila')}}?district_id='+head_id,
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#upazila_id").html(response['upazila_list']);
              }
             // console.log(response);
            }
          });
      });


      $('#upazila_id').on('change', function() {
          var upazila_id = $(this).val();

          $("#union_id").html("Select Upazila");

          $.ajax({
            url: '{{route('superadmin.ajax.union')}}?upazila_id='+upazila_id,
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#union_id").html(response['union_list']);
              }
             // console.log(response);
            }
          });
      });


      $('#head_id').on('change', function() {
          var head_id = $(this).val();

          $("#subhead_id").html("Select Head");
          $("#item_id").html("Select SubHead");


          $.ajax({
            url: '{{route('superadmin.ajax.subheads')}}?head_id='+head_id,
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#subhead_id").html(response['subhead_list']);
              }
             // console.log(response);
            }
          });
      });


      $('#subhead_id').on('change', function() {
          var subhead_id = $(this).val();

          $("#item_id").html("Select SubHead");

          //console.log('Calling here');

          $.ajax({
            url: '{{route('superadmin.ajax.items')}}?subhead_id='+subhead_id,
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#item_id").html(response['item_list']);
              }
             //console.log(response);
            }
          });
      });



  });
</script>
@endsection