@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Finance</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Disbursment To UP </a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>List</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

  <div class="col-md-12" style="margin-top: 10px;">
    <div class="portlet box blue">
      <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-gift"></i>Disbursment <small> list</small>
            </div>

            <div class="actions">
              <a href="{{route('superadmin.finance.disbursment-to-up.disbursment.index', ['download' => 'download'])}}" target="_blank" class="btn btn-circle success"> <i class="fa fa-download"></i> Download</a>
            </div>


          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body form">
          <form action="{{route('superadmin.finance.disbursment-to-up.disbursment.index')}}" class="form-horizontal" method="GET" id="createForm">

              {{csrf_field()}}

              <input type="hidden" name="query" value="{{time()}}" />

              <div class="form-body">

                  <div class="row">
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">District</label>
                              <div class="col-md-9">
                                  <select class="form-control" required="required" id="district_id" name="district_id">
                                    @foreach($districts as $district)
                                      <option value="{{$district->id}}"

                                        @if(isset($_REQUEST['district_id']) && $district->id == $_REQUEST['district_id'])
                                            selected="selected"
                                        @endif


                                      >{{$district->distname}}</option>
                                    @endforeach
                                  </select>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">Upazila</label>
                              <div class="col-md-9">
                                  <select class="form-control" required="required" id="upazila_id" name="upazila_id">


                                    @if(isset($_REQUEST['district_id']) && $_REQUEST['district_id'] != "")

                                        @if($_REQUEST['district_id'] != "all")
                                            <?php
                                            $upazilas = App\Model\Upazila::where('disid', $_REQUEST['district_id'])->get();
                                            ?>

                                            <option value="">Choose an Option</option>

                                            @foreach($upazilas as $upazila)
                                                <option value="{{$upazila->id}}"
                                                    @if(isset($_REQUEST['upazila_id']) && $_REQUEST['upazila_id'] == $upazila->id)
                                                        selected="selected"
                                                    @endif
                                                >{{$upazila->upname}}</option>
                                            @endforeach
                                        @endif

                                    @else
                                        <option value="">Choose District First</option>
                                    @endif


                                  </select>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">Union</label>
                              <div class="col-md-9">
                                <select name="union_id" class="form-control" required="required" id="union_id">

                                  @if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] != "")

                                        @if($_REQUEST['upazila_id'] != "all")
                                            <?php
                                            $unions = App\Model\Union::where('upid', $_REQUEST['upazila_id'])->get();
                                            ?>

                                            <option value="">Choose an Option</option>
                                            @foreach($unions as $union)
                                                <option value="{{$union->id}}"
                                                    @if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] == $union->id)
                                                        selected="selected"
                                                    @endif
                                                >{{$union->unname}}</option>
                                            @endforeach
                                        @endif

                                    @else
                                        <option value="">Choose Upazila First</option>
                                    @endif




                                </select>
                              </div>
                          </div>
                      </div>


                  </div>

                  <div class="row">
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-6">
                            <input type="submit" class="btn green" value="Filter Data" />
                        </div>
                    </div>
                  </div>
              </div>
          </form>

      </div>
    </div>
  </div>



  @if(count($budgets) > 0)
    <div class="col-md-12">
        <div class="portlet light tasks-widget bordered">
            <div class="portlet-body util-btn-margin-bottom-5">
                <style>
                  #example0 th, #example0 td {
                    white-space: nowrap;
                  }
                </style>
                <div class="table-responsive">
                  <table class="table table-bordered table-hover" id="example0">
                      <thead class="flip-content">
                          <th style="font-size: 10px;">Action</th>
                          <th style="font-size: 10px;">Head</th>
                          <th style="font-size: 10px;">Subhead</th>
                          <th style="font-size: 10px;">Item</th>
                          <th style="font-size: 10px;">Amount</th>
                          <th style="font-size: 10px;">Voucher</th>
                          <th style="font-size: 10px;">Disburse Date</th>
                          <th style="font-size: 10px;">Remarks</th>

                      </thead>
                      <tbody>
                          <?php
                            $last = "";
                          ?>
                          @foreach($budgets as $dis)
                              <tr>
                                  <td style="font-size: 10px;">
                                    <a class="label label-success" href="{{route('superadmin.finance.disbursment-to-up.disbursment.edit', $dis->id)}}">
                                          <i class="fa fa-pencil"></i> Edit
                                    </a>
                                  </td>

                                  <td style="font-size: 10px;">{{$dis->head}}</td>
                                  <td style="font-size: 10px;">{{$dis->subhead }}</td>
                                  <td style="font-size: 10px;">{{$dis->item}}</td>
                                  <td style="font-size: 10px; text-align: right">{{sprintf("%.2lf", $dis->amount)}}</td>
                                  <td style="font-size: 10px;">{{$dis->voucher }}</td>
                                  <td style="font-size: 10px;">{{date('d-m-Y', strtotime($dis->date))}}</td>
                                  <td style="font-size: 10px;">{{$dis->remarks}}</td>

                              </tr>
                          @endforeach
                      </tbody>
                  </table>
                  <div class="pagination pull-right">
                    {{$budgets->appends($old)->links()}}
                  </div>
                </div>
            </div>
        </div>
    </div>
  @else
    <div class="col-md-12">
      <div class="portlet light tasks-widget bordered">
         <p>
            No Data Found
         </p>
      </div>
    </div>
  @endif
@endsection



@section('my_js')
<script type="text/javascript">
  $(document).ready(function () {

      $('#district_id').on('change', function() {
          var head_id = $(this).val();

          $("#upazila_id").html("Select District");
          $("#union_id").html("Select Upazila");

          $.ajax({
            url: '{{route('superadmin.ajax.upazila')}}?district_id='+head_id+'&not_all=false&choose_one=true',
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#upazila_id").html(response['upazila_list']);
              }
             // console.log(response);
            }
          });
      });


      $('#upazila_id').on('change', function() {
          var upazila_id = $(this).val();

          $("#union_id").html("Select Upazila");

          $.ajax({
            url: '{{route('superadmin.ajax.union')}}?upazila_id='+upazila_id+'&not_all=false&choose_one=true',
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#union_id").html(response['union_list']);
              }
             // console.log(response);
            }
          });
      });


  });
</script>
@endsection