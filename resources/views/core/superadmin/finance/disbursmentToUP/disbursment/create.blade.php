@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Finance</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Disbursment To UP </a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="{{route('superadmin.finance.disbursment-to-up.disbursment.index')}}">Disbursments </a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Add New</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

  <h1 class="page-title"> Disbursment <small> Add new Item</small> </h1>

  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i>Add New </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body form">
          <form action="{{route('superadmin.finance.disbursment-to-up.disbursment.store')}}" class="form-horizontal" method="POST" id="createForm">

              {{csrf_field()}}

              <input type="hidden" name="query" value="{{time()}}" />

              <div class="form-body">

                  <div class="row">
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">District</label>
                              <div class="col-md-9">
                                  <select name="distname" class="form-control" required="required" id="district_id">
                                    @foreach($districts as $district)
                                      <option value="{{$district->id}}">{{$district->distname}}</option>
                                    @endforeach
                                  </select>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">Upazila</label>
                              <div class="col-md-9">
                                  <select name="Upname" class="form-control" required="required" id="upazila_id">
                                    <option>Select Head First</option>
                                  </select>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">Union</label>
                              <div class="col-md-9">
                                <select name="union_id" class="form-control" required="required" id="union_id">
                                  <option>Select Head First</option>
                                </select>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Head</label>
                              <div class="col-md-9">
                                  <select class="form-control" required="required" id="head_id" name="headid">
                                    @foreach($heads as $head)
                                      <option value="{{$head->id}}">{{$head->headname}}</option>
                                    @endforeach
                                  </select>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">SubHead</label>
                              <div class="col-md-9">
                                  <select class="form-control" required="required" id="subhead_id">
                                    @foreach($districts as $district)
                                      <option value="{{$district->id}}">{{$district->distname}}</option>
                                    @endforeach
                                  </select>
                              </div>
                          </div>
                      </div>
                  </div>


                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Item</label>
                              <div class="col-md-9">
                                  <select class="form-control" required="required" id="item_id" name="itemid">
                                    @foreach($districts as $district)
                                      <option value="{{$district->id}}">{{$district->distname}}</option>
                                    @endforeach
                                  </select>
                              </div>
                          </div>
                      </div>
                  </div>


                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Amount</label>
                              <div class="col-md-9">
                                  <input type="text" class="form-control" required="required" name="amount"
                                    @if(old('amount'))
                                      value="{{old('amount')}}"
                                    @endif
                                   />
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Method of Payment</label>
                              <div class="col-md-9">
                                  <select class="form-control" required="required" id="item_id" name="mode">
                                    <option value="Cheque" id="cheque">Cheque</option>
                                    <option value="Cash" id="cashid">Cash</option>
                                    <option value="TT" id="tt">TT</option>
                                  </select>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Voucher</label>
                              <div class="col-md-9">
                                  <input type="text" class="form-control" required="required" name="vou"
                                    @if(old('vou'))
                                      value="{{old('vou')}}"
                                    @endif
                                   />
                              </div>
                          </div>
                      </div>
                  </div>


                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Disburse Date</label>
                              <div class="col-md-9">
                                  <input
                                    type="date"
                                    class="form-control date-picker"
                                    required="required"
                                    name="date"
                                    data-date-format="yyyy-mm-dd" readonly
                                    @if(old('date'))
                                      value="{{date('Y-m-d', strtotime(old('date')))}}"
                                    @endif
                                   />
                              </div>
                          </div>
                      </div>
                  </div>


                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                              <label class="control-label col-md-3">Remarks</label>
                              <div class="col-md-9">
                                  <textarea class="form-control" name="remarks" row="3">@if(old('remarks'))
                                    value="{{old('remarks')}}"@endif</textarea>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-6">
                            <input type="submit" class="btn green" value="Insert New " />
                        </div>
                    </div>
                  </div>

              </div>
          </form>

      </div>
    </div>
  </div>

@endsection

@section('my_js')
<script type="text/javascript">
  $(document).ready(function () {

      $('#district_id').on('change', function() {
          var head_id = $(this).val();

          $("#upazila_id").html("Select District");
          $("#union_id").html("Select Upazila");

          $.ajax({
            url: '{{route('superadmin.ajax.upazila')}}?district_id='+head_id,
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#upazila_id").html(response['upazila_list']);
              }
             // console.log(response);
            }
          });
      });


      $('#upazila_id').on('change', function() {
          var upazila_id = $(this).val();

          $("#union_id").html("Select Upazila");

          $.ajax({
            url: '{{route('superadmin.ajax.union')}}?upazila_id='+upazila_id,
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#union_id").html(response['union_list']);
              }
             // console.log(response);
            }
          });
      });


      $('#head_id').on('change', function() {
          var head_id = $(this).val();

          $("#subhead_id").html("Select Head");
          $("#item_id").html("Select SubHead");


          $.ajax({
            url: '{{route('superadmin.ajax.subheads')}}?head_id='+head_id,
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#subhead_id").html(response['subhead_list']);
              }
             // console.log(response);
            }
          });
      });


      $('#subhead_id').on('change', function() {
          var subhead_id = $(this).val();

          $("#item_id").html("Select SubHead");

          //console.log('Calling here');

          $.ajax({
            url: '{{route('superadmin.ajax.items')}}?subhead_id='+subhead_id,
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#item_id").html(response['item_list']);
              }
             //console.log(response);
            }
          });
      });



  });
</script>
@endsection