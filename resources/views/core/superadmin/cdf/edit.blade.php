@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="{{route('superadmin.cdfs.index')}}">CDF</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Edit</span>
      </li>
    </ul>
  </div>

  <h1 class="page-title"> CDF <small> Update</small> </h1>

    <div class="col-md-12">
      <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>Edit Head</div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>

        <div class="portlet-body form">
            <form action="{{route('superadmin.cdfs.update', $cdf->id)}}" class="form-horizontal" method="POST" id="updateForm">

                {{csrf_field()}}

                <div class="form-body">

                  <div class="row">

                      <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-3">Project</label>
                              <div class="col-md-9">
                                  <select name="project_id" class="form-control" required="required" id="project">
                                    <?php
                                      $projects = App\Model\Project::orderBy('project', 'DESC')->get();
                                    ?>
                                    <option value="">Select Project</option>
                                    @foreach($projects as $project)
                                      <option
                                        value="{{$project->id}}"
                                        @if($project->id == $cdf->project_id)
                                          selected="selected"
                                        @endif
                                      >{{$project->project}}</option>
                                    @endforeach
                                  </select>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-3">Region</label>
                              <div class="col-md-9">
                                  <select name="region_id" class="form-control" required="required" id="region">
                                    <option value="">Select Project</option>

                                    <?php
                                    $regions = \DB::table('tbl_water')
                                              ->where('tbl_water.region_id', '!=', "")
                                              ->where('tbl_water.region_id', '!=', 0)
                                              ->where('tbl_water.proj_id', $cdf->project_id)
                                              ->select('region.region_id', 'region.region_name')
                                              ->leftjoin('region','region.region_id','=','tbl_water.region_id')
                                              ->groupBy('tbl_water.region_id')
                                              ->get();
                                    ?>
                                     @foreach($regions as $region)
                                      <option
                                        value="{{$region->region_id}}"
                                        @if($region->region_id == $cdf->region_id)
                                          selected="selected"
                                        @endif
                                      >{{$region->region_name}}</option>
                                    @endforeach

                                  </select>
                              </div>
                          </div>
                      </div>



                      <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-3">District</label>
                              <div class="col-md-9">
                                  <select name="district_id" class="form-control" required="required" id="district">
                                    <option value="">Select Region</option>
                                    <?php
                                      $districts = \DB::table('fdistrict')->where('region_id', $cdf->region_id)->get();
                                    ?>
                                    @foreach($districts as $district)
                                      <option
                                        value="{{$district->id}}"
                                        @if($district->id == $cdf->district_id)
                                          selected="selected"
                                        @endif
                                      >{{$district->distname}}</option>
                                    @endforeach

                                  </select>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-3">Upazila</label>
                              <div class="col-md-9">
                                  <select name="upazila_id" class="form-control" required="required" id="upazila">
                                    <option value="">Select District</option>
                                    <?php
                                    $upazilas = \DB::table('fupazila')->where('disid', $cdf->district_id)->get();
                                    ?>
                                    @foreach($upazilas as $upazila)
                                      <option
                                        value="{{$upazila->id}}"
                                        @if($upazila->id == $cdf->upazila_id)
                                          selected="selected"
                                        @endif
                                      >{{$upazila->upname}}</option>
                                    @endforeach

                                  </select>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-3">Union</label>
                              <div class="col-md-9">
                                  <select name="union_id" class="form-control" required="required" id="union">
                                    <?php
                                      $unions = \DB::table('funion')->where('upid', $cdf->upazila_id)->get();
                                    ?>
                                    @foreach($unions as $union)
                                      <option
                                        value="{{$union->id}}"
                                        @if($union->id == $cdf->union_id)
                                          selected="selected"
                                        @endif
                                      >{{$union->unname}}</option>
                                    @endforeach

                                  </select>
                              </div>
                          </div>
                      </div>


                      <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-3">CDF No</label>
                              <div class="col-md-9">
                                  <input class="form-control" type="text" name="cdf_no" required="required" value="{{$cdf->cdf_no}}"">
                              </div>
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-3">Village</label>
                              <div class="col-md-9">
                                  <input class="form-control" type="text" name="village" required="required" value="{{$cdf->village}}">
                              </div>
                          </div>
                      </div>
                  </div>
                  <div class="row">


                      <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-3">Type</label>
                              <div class="col-md-9">
                                <select name="type" class="form-control" id="typeId">
                                  <option value="school" @if($cdf->type == "school") selected="selected" @endif >School No</option>
                                  <option value="cdf" @if($cdf->type == "cdf") selected="selected" @endif>CDF No</option>
                                </select>
                              </div>
                          </div>
                      </div>

                    <div class="col-md-3" id="schoolTitleDiv" @if($cdf->type == "cdf") style="display: none;" @endif>
                        <div class="form-group">
                            <label class="control-label col-md-3">School Title</label>
                            <div class="col-md-9">
                              <input type="text" name="school_title" placeholder="School Title" class="form-control" value="{{$cdf->school_title}}" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2" id="wordDiv" @if($cdf->type == "school") style="display: none;" @endif>
                      <div class="form-group">
                          <label class="control-label col-md-4">Word No</label>
                          <div class="col-md-8">
                            <select class="form-control" name="word_no" id="word_no">
                              <option value="">Choose</option>
                              @foreach(range(1, 10) as $index )
                                <option value="{{$index}}" @if($cdf->word_no == $index) selected="selected" @endif >{{$index}}</option>
                              @endforeach
                            </select>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3" id="ownerDiv" @if($cdf->type == "school") style="display: none;" @endif >
                        <div class="form-group">
                            <label class="control-label col-md-3">Owner</label>
                            <div class="col-md-9">
                              <input type="text" name="owner" placeholder="owner" class="form-control" id="owner" value="{{$cdf->owner}}" />
                            </div>
                        </div>
                    </div>

                  </div>
                  <div class="row">
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-3">
                            <input type="submit" class="btn green" value="Update CDF" />
                        </div>
                    </div>
                  </div>
                </div>
            </form>

        </div>
      </div>
    </div>
@endsection


@section('my_js')
<script type="text/javascript">

  $(document).ready(function () {

      var type = "school";

      $("#typeId").on('change', function(){
        type = $(this).val();

        console.log(type);

        if(type == "school" )
        {
          $("#wordDiv").hide();
          $("#ownerDiv").hide();
          $("#schoolTitleDiv").show();
        }else
        {
          $("#wordDiv").show();
          $("#ownerDiv").show();
          $("#schoolTitleDiv").hide();
        }
      });
    });
    </script>
@endsection