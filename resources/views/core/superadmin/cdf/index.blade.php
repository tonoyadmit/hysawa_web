@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Mobile App</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>CDF</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')


  <div class="col-md-12" style="margin-top: 10px;">
    <div class="portlet box blue">
      <div class="portlet-title">
          <div class="caption">
              CDF <small> list</small> Add New </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body form">
          <form action="{{route('superadmin.cdfs.store')}}" class="form-horizontal" method="POST" id="createForm">

              {{csrf_field()}}

              <div class="form-body">

                  <div class="row">

                      <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-3">Project</label>
                              <div class="col-md-9">
                                  <select name="project_id" class="form-control" required="required" id="project">
                                    <?php
                                      $projects = App\Model\Project::orderBy('project', 'DESC')->get();
                                    ?>
                                    <option value="">Select Project</option>
                                    @foreach($projects as $project)
                                      <option value="{{$project->id}}">{{$project->project}}</option>
                                    @endforeach
                                  </select>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-3">Region</label>
                              <div class="col-md-9">
                                  <select name="region_id" class="form-control" required="required" id="region">
                                    <option value="">Select Project</option>
                                  </select>
                              </div>
                          </div>
                      </div>



                      <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-3">District</label>
                              <div class="col-md-9">
                                  <select name="district_id" class="form-control" required="required" id="district">
                                    <option value="">Select Region</option>
                                  </select>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-3">Upazila</label>
                              <div class="col-md-9">
                                  <select name="upazila_id" class="form-control" required="required" id="upazila">
                                    <option value="">Select District</option>
                                  </select>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-3">Union</label>
                              <div class="col-md-9">
                                  <select name="union_id" class="form-control" required="required" id="union">
                                   <option value="">Select Upazila</option>
                                  </select>
                              </div>
                          </div>
                      </div>


                      <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-3">CDF No</label>
                              <div class="col-md-9">
                                  <input class="form-control" type="text" name="cdf_no" required="required">
                              </div>
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-3">Village</label>
                              <div class="col-md-9">
                                  <input class="form-control" type="text" name="village" required="required">
                              </div>
                          </div>
                      </div>

                  </div>

                  <div class="row">

                    <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-3">Type</label>
                              <div class="col-md-9">
                                <select name="type" class="form-control" id="typeId">
                                  <option value="school" selected="selected">School No</option>
                                  <option value="cdf" >CDF No</option>
                                </select>
                              </div>
                          </div>
                      </div>

                    <div class="col-md-3" id="schoolTitleDiv">
                        <div class="form-group">
                            <label class="control-label col-md-3">School Title</label>
                            <div class="col-md-9">
                              <input type="text" name="school_title" placeholder="School Title" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="col-md-2" id="wordDiv" style="display: none;">
                      <div class="form-group">
                          <label class="control-label col-md-4">Word No</label>
                          <div class="col-md-8">
                            <select class="form-control" name="word_no" id="word_no">
                              <option value="">Choose</option>
                              @foreach(range(1, 10) as $index )
                                <option value="{{$index}}">{{$index}}</option>
                              @endforeach
                            </select>
                          </div>
                      </div>
                    </div>

                    <div class="col-md-3" id="ownerDiv" style="display: none;">
                        <div class="form-group">
                            <label class="control-label col-md-3">Owner</label>
                            <div class="col-md-9">
                              <input type="text" name="owner" placeholder="owner" class="form-control" id="owner"/>
                            </div>
                        </div>
                    </div>




                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-3">
                            <input type="submit" class="btn green" value="Add New CDF No" />
                        </div>
                    </div>
                  </div>
              </div>
          </form>

      </div>
    </div>
  </div>



    @if(count($cdfs) > 0)

        <div class="col-md-12">
            <div class="portlet light tasks-widget bordered">
                <div class="portlet-body util-btn-margin-bottom-5">
                    <table class="table table-bordered table-hover" id="cdfDataTable">
                        <thead class="flip-content">
                            <th>Action</th>

                            <th>CDF No</th>

                            <th>Type</th>

                            <th>Project</th>
                            <th>Region</th>
                            <th>District</th>
                            <th>Upazila</th>
                            <th>Union</th>
                            <th>Village</th>

                        </thead>
                        <tbody>
                            @foreach($cdfs as $cdf)
                                <tr>
                                    <td>
                                      <a class="label label-success" href="{{route('superadmin.cdfs.edit', $cdf->id)}}">
                                            <i class="fa fa-pencil"></i> Edit
                                      </a>
                                    </td>

                                    <td>{{$cdf->cdf_no}}</td>

                                    <td>@if($cdf->type == "cdf") CDF<br/>Word: {{$cdf->word_no or ''}}, Owner: {{$cdf->owner or ''}}@else School No<br/>Title: {{$cdf->school_title or ''}}@endif </td>


                                    <td>{{$cdf->project->project }}</td>
                                    {{--  <td>{{$cdf->region->region_name }}</td>  --}}

                                    <td>{{$cdf->district->distname }}</td>
                                    <td>{{$cdf->upazila->upname }}</td>
                                    <td>{{$cdf->union->unname }}</td>
                                    <td>{{$cdf->village }}</td>

                                    {{-- <td>{{$cdf->area }}</td> --}}



                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="pagination pull-right">
                    {{$cdfs->links()}}
                    </div>
                </div>
            </div>
        </div>
    @endif


@endsection


@section('my_js')
<script type="text/javascript">

  $(document).ready(function () {

      var type = "school";

      $("#typeId").on('change', function(){
        type = $(this).val();

        console.log(type);

        if(type == "school" )
        {
          $("#wordDiv").hide();
          $("#ownerDiv").hide();
          $("#schoolTitleDiv").show();
        }else
        {
          $("#wordDiv").show();
          $("#ownerDiv").show();
          $("#schoolTitleDiv").hide();
        }
      });




      $('#project').on('change', function() {
          var id = $(this).val();

          $("#region").html("Select Project");
          $("#district").html("Select Region");
          $("#upazila").html("Select District");
          $("#union").html("Select Upazila");
          $.ajax({
            url: '{{route('ajax.regions-from-project')}}?project_id='+id,
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#region").html(response['region_list']);
              }
             // console.log(response);
            }
          });
      });

      $('#region').on('change', function() {
          var id = $(this).val();

          $("#district").html("Select Region");
          $("#upazila").html("Select District");
          $("#union").html("Select Upazila");
          $.ajax({
            url: '{{route('ajax.districts-from-region')}}?region_id='+id,
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#district").html(response['district_list']);
              }
             // console.log(response);
            }
          });
      });

      $('#district').on('change', function() {
          var id = $(this).val();

          $("#upazila").html("Select District");
          $("#union").html("Select Upazila");
          $.ajax({
            url: '{{route('ajax.upazilas-from-district')}}?district_id='+id,
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#upazila").html(response['upazila_list']);
              }
             // console.log(response);
            }
          });
      });

      $('#upazila').on('change', function() {
          var id = $(this).val();

          $.ajax({
            url: '{{route('ajax.unions-from-upazila')}}?upazila_id='+id,
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#union").html(response['union_list']);
              }
             // console.log(response);
            }
          });
      });

      $('#cdfDataTable').DataTable( {responsive: true});

  });
</script>
@endsection

