@extends('layouts.appinside')
@section('content')

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('superadmin.dashboard') }}">PNGO:</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('superadmin.pngo.pngolist.create') }}">Inser New PNGO:</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Insert</span>
        </li>
    </ul>
</div>
<h1 class="page-title"> Inser New PNGO:
    <small>create new</small>
</h1>

{!! Form::open(array('route' => 'superadmin.pngo.pngolist.store', 'method' => 'post', 'class' => 'form-horizontal')) !!}

<div class="row">

    @include('partials.errors')

    <div class="col-md-offset-3 col-md-6">

        <div class="form-group">
            <label class="control-label col-md-3 ">District</label>
            <div class="col-md-9">
            {!! Form::select('distid', array(''=>'Select District')+$district, null, ['class' => 'form-control','id' => 'district_id']) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Upzilla</label>
            <div class="col-md-9">
            {!! Form::select('upid', array(''=>'Select Upzilla'), null, ['class' => 'form-control','id'=>'upazila_id']) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Union</label>
            <div class="col-md-9">
            {!! Form::select('unid', array(''=>'Select Union'), null, ['class' => 'form-control','id' =>'union_id']) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3">NGO Name:</label>
            <div class="col-md-9">
            {!! Form::text('ngoname', null, ['class' => 'form-control', 'placeholder' => 'NGO Name',]) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3">Address:</label>
            <div class="col-md-9">
            {!! Form::text('address', null, ['class' => 'form-control', 'placeholder' => 'Address',]) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3">Executive Director/Head of NGO:</label>
            <div class="col-md-9">
            {!! Form::text('edname', null, ['class' => 'form-control', 'placeholder' => 'Executive Director/Head of NGO',]) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3">Mobile number:</label>
            <div class="col-md-9">
            {!! Form::text('edmobile', null, ['class' => 'form-control', 'placeholder' => 'Mobile number',]) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Email:</label>
            <div class="col-md-9">
            {!! Form::text('edemail', null, ['class' => 'form-control', 'placeholder' => 'Email',]) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3">Contact person:</label>
            <div class="col-md-9">
            {!! Form::text('ContactPerson', null, ['class' => 'form-control', 'placeholder' => 'Contact person',]) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3">Mobile:</label>
            <div class="col-md-9">
            {!! Form::text('contactmobile', null, ['class' => 'form-control', 'placeholder' => 'Mobile',]) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Email:</label>
            <div class="col-md-9">
            {!! Form::text('contactemail', null, ['class' => 'form-control', 'placeholder' => 'Email',]) !!}
            </div>
        </div>
        <div class="form-group">
            <label class="control-label col-md-3">Contract date:</label>
            <div class="col-md-9">
            {!! Form::DATE('contractdate', null, ['class' => 'form-control', 'placeholder' => 'Contract date']) !!}
            </div>
        </div>

        <div class="form-group">
            <label class="control-label col-md-3">Remarks:</label>
            <div class="col-md-9">
            {!! Form::text('remarks', null, ['class' => 'form-control', 'placeholder' => 'Remarks']) !!}
            </div>
        </div>
    </div>

</div>

&nbsp;
<div class="row padding-top-10">
    <a href="javascript:history.back()" class="btn default"> Cancel </a>
    {!! Form::submit('Save', ['class' => 'btn green pull-right']) !!}
</div>

{!! Form::close() !!}


</div>

<script type="text/javascript">
    $(document).ready(function () {
    highlight_nav('warehouse-manage', 'shelfs');
    });</script>

<script type="text/javascript" language="javascript" >

            $(document).ready(function () {
    $('#district_id').on('change', function() {
    var disid = $(this).val();
            var csrftoken = $("#csrf-token").val();
            if (disid == ''){
    $('#upazila_id').attr('disabled', 'disabled');
    } else{
    $.getJSON('{{ route('training-agency.getUpzilla') }}?disid=' + disid + '&_token=' + csrftoken, function (data) {
    $('select[name="upid"]').empty();
            $.each(data, function(key, value) {
            $('select[name="upid"]').append('<option value="' + key + '">' + value + '</option>');
            });
    });
    }
    });
            $('#upazila_id').on('change', function() {
    var upid = $(this).val();
            var disid = $("#district_id").val();
            var csrftoken = $("#csrf-token").val();
            if (upid == ''){
    $('#union_id').attr('disabled', 'disabled');
    } else{
    $.getJSON('{{ route('training-agency.getUnion') }}?disid=' + disid + '&_token=' + csrftoken + '&upid=' + upid, function (data) {
    $('select[name="unid"]').empty();
            $.each(data, function(key, value) {
            $('select[name="unid"]').append('<option value="' + key + '">' + value + '</option>');
            });
    });
    }
    });
    });

</script>
@endsection
