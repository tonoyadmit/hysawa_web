@extends('layouts.appinside')
@section('content')
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>List of PNGO:</span>
      </li>
    </ul>
  </div>

  <div class="col-md-12" style="margin-top: 10px;">
      @include('core.superadmin.pngo._search')
  </div>

  @include('core.superadmin.pngo._table')

@endsection



@section('my_js')
<script type="text/javascript">
  $(document).ready(function () {

    $('#district_id').on('change', function() {
      var district_id = $(this).val();

      $("#upazila_id").html("Select District");
      $("#union_id").html("Select Upazila");

      $.ajax({
        url: '{{route('superadmin.ajax.upazila')}}?district_id='+district_id+'&not_all=true',
        type: 'GET',
        success: function(response){
          if(response['status'] == true){
            $("#upazila_id").html(response['upazila_list']);
          }
        }
      });
    });
    $('#upazila_id').on('change', function() {
      var upazila_id = $(this).val();

      $("#union_id").html("Select Upazila");

      $.ajax({
        url: '{{route('superadmin.ajax.union')}}?upazila_id='+upazila_id+'&not_all=true',
        type: 'GET',
        success: function(response){
          if(response['status'] == true){
            $("#union_id").html(response['union_list']);
          }
        }
      });
    });
  });
</script>
@endsection