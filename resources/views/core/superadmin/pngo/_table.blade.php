@if(count($pngolistss) > 0)
  <div class="col-md-12">
    <div class="portlet light tasks-widget bordered">

      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i>List of PNGO
          </div>
          <div class="actions">
              <a href="{{route('superadmin.pngo.index', ['download' => 'download'])}}" target="_blank" class="btn btn-circle success"> <i class="fa fa-download"></i> Download</a>
          </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body util-btn-margin-bottom-5">
        <div class="table-responsive">

          <table class="table table-bordered table-hover data-table" id="example0">
            <thead class="flip-content">
              <th style="font-size: 10px;">Action</th>
              <th style="font-size: 10px;">District</th>
              <th style="font-size: 10px;">Upazila</th>
              <th style="font-size: 10px;">Union</th>
              <th style="font-size: 10px;">Name of NGO</th>
              <th style="font-size: 10px;">Executive Director / head of NGO</th>
              <th style="font-size: 10px;">Mobile (ED)</th>
              <th style="font-size: 10px;">Email (ED)</th>
              <th style="font-size: 10px;">Contact Address</th>
              <th style="font-size: 10px;">Contact Person</th>
              <th style="font-size: 10px;">Mobile (Contact person)</th>
              <th style="font-size: 10px;">Email (Contact person)</th>
              <th style="font-size: 10px;">Contract date</th>
              <th style="font-size: 10px;">Remarks</th>
            </thead>

            <tbody>
              @foreach($pngolistss as $pngolists)
              <tr>
                <td><a class="label label-success" href="{{route('superadmin.pngo.pngolist.edit', $pngolists->id)}}">
                    <i class="fa fa-pencil"></i> </a></td>
                <td style="font-size: 10px;">{{$pngolists->distname }}</td>
                <td style="font-size: 10px;">{{$pngolists->upname }}</td>
                <td style="font-size: 10px;">{{$pngolists->unname}}</td>
                <td style="font-size: 10px;">{{$pngolists->ngoname}}</td>
                <td style="font-size: 10px;">{{$pngolists->edname}}</td>
                <td style="font-size: 10px;">{{$pngolists->edmobile}}</td>
                <td style="font-size: 10px;">{{$pngolists->edemail}}</td>
                <td style="font-size: 10px;">{{$pngolists->address}}</td>
                <td style="font-size: 10px;">{{$pngolists->ContactPerson}}</td>
                <td style="font-size: 10px;">{{$pngolists->contactmobile}}</td>
                <td style="font-size: 10px;">{{$pngolists->contactemail}}</td>
                <td style="font-size: 10px;">{{$pngolists->contractdate}}</td>
                <td style="font-size: 10px;">{{$pngolists->remarks}}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
            <div class="pagination pull-right">
               {!! $pngolistss->appends($old)->render() !!}
            </div>
        </div>

      </div>
    </div>
  </div>
@else
  <div class="col-md-12">
    <div class="portlet light tasks-widget bordered">
      <p>
        No Data Found
      </p>
    </div>
  </div>
@endif
