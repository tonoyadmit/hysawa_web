<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>List of PNGO: <small> view</small> </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body form">
        <form action="{{route('superadmin.pngo.index')}}" class="form-horizontal" method="GET" id="searchForm">

            <input type="hidden" name="query" value="{{time()}}" />

            <div class="form-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Project</label>
                            <div class="col-md-9">

                                <?php

                                    $projects = App\Model\Project::all();

                                ?>
                                <select class="form-control" name="project_id">
                                    <option value="">Choose a Project</option>
                                    @foreach($projects as $project )
                                        <option value="{{$project->id}}"

                                        @if(!empty($old) && !empty($old['project_id']) && $old['project_id'] == $project->id)
                                            selected="selected"
                                        @endif


                                        >{{$project->project}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Region</label>
                            <div class="col-md-9">
                                <?php
                                    $regions = App\Model\Region::all();
                                ?>
                                <select class="form-control" name="region_id">
                                    <option value="">Choose a Region</option>
                                    @foreach($regions as $region )
                                        <option value="{{$region->id}}"
                                        @if(!empty($old) && !empty($old['region_id']) && $old['region_id'] == $region->id)
                                            selected="selected"
                                        @endif
                                        >{{$region->region_name}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">District</label>
                            <div class="col-md-9">
                                <?php
                                    $districts = App\Model\District::all();
                                ?>
                                <select class="form-control" name="district_id" id="district_id">
                                    <option value="">Choose a District</option>
                                    @foreach($districts as $district )
                                        <option value="{{$district->id}}"
                                        @if(!empty($old) && !empty($old['district_id']) && $old['district_id'] == $district->id)
                                            selected="selected"
                                        @endif
                                        >{{$district->distname}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Upazila Id</label>
                            <div class="col-md-9">

                                <select class="form-control" id="upazila_id" name="upazila_id">
                                    <option value="">Select Upazila</option>
                                </select>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Union Id</label>
                            <div class="col-md-9">

                               <select name="union_id" class="form-control" id="union_id">
                                <option value="">Select Union</option>
                              </select>

                            </div>
                        </div>
                    </div>
                </div>



                <div class="row">
                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="submit" class="btn green" value="Search" />
                                <a href="{{route('superadmin.pngo.index')}}" class="btn default">Clear</a>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </form>

    </div>
</div>