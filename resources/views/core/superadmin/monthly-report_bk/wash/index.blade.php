@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('superadmin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="#">Monthly Reports</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>UP Wise Wash Status</span>
    </li>
  </ul>
</div>

@include('partials.errors')

<h1 class="page-title"> Monthly Reports <small>UP Wise Wash Status</small> </h1>

<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <div class="portlet-body util-btn-margin-bottom-5">


      <div class="table-responsive">
        <style>
          #example0 td {
            /*white-space: nowrap;*/
            font-size: 10px;
          }
          #example0 th {
            font-size: 10px;
          }
        </style>




        <table class="table table-bordered table-hover table-condensed" id="example0">
          <thead class="flip-content">

              <tr>
                <tr>
                  <td >&nbsp;</td>
                  <td >&nbsp;</td>
                  <td >&nbsp;</td>
                  <td >&nbsp;</td>
                  <td class="text-center">&nbsp;</td>
                  <td class="text-center">&nbsp;</td>
                  <td colspan="7" class="text-center">Baseline</td>
                  <td colspan="4" class="text-center">Achievement including baseline</td>
                </tr>
                <tr>
                  <td>SL.</td>
                  <td class="text-center">District</td>
                  <td class="text-center">Upazila</td>
                  <td class="text-center">Union</td>
                  <td class="text-center"># CDF</td>
                  <td class="text-center"># Population</td>
                  <td class="text-center"># HH</td>
                  <td class="text-center"># Hygienic latrine </td>
                  <td class="text-center">%</td>
                  <td class="text-center">M&amp;E Survey(%)</td>
                  <td class="text-center">Water access (pop)</td>
                  <td class="text-center">%</td>
                  <td class="text-center">M&amp;E Survey (%)</td>
                  <td class="text-center"># Hygienic latrine</td>
                  <td class="text-center">%</td>
                  <td class="text-center">Water access (pop)</td>
                  <td class="text-center">%</td>
                </tr>
              </tr>

          </thead>

          <tbody>
          <?php $index = 1; ?>
            @foreach($datas as $d)

             <tr>
                  <td>{{$index++}}</td>
                  <td class="text-left">{{$d->distname}}</td>
                  <td class="text-left">{{$d->upname}}</td>
                  <td class="text-left">{{$d->unname}}</td>
                  <td class="text-right">{{$d->cdf_no}}</td>
                  <td class="text-right">{{$d->cdf_pop}}</td>
                  <td class="text-center">{{$d->cdf_hh}}</td>
                  <td class="text-center">{{$d->Hygieniclatrine}}</td>
                  <td class="text-center">{{$d->per1}}</td>
                  <td class="text-center">{{$d->sa_b}}</td>
                  <td class="text-center">{{$d->ws_50}}</td>
                  <td class="text-center">{{$d->per2}}</td>
                  <td class="text-center">{{$d->wat_b}}</td>
                  <td class="text-center">{{$d->HygienicLatrine}}</td>
                  <td class="text-center">{{$d->per3}}</td>
                  <td class="text-center">{{$d->WaterAccessPop}}</td>
                  <td class="text-center">{{$d->per4}}</td>

              </tr>
            @endforeach

          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>














@endsection
