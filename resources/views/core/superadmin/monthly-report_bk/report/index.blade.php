@extends('layouts.appinside')

@section('content')

    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ route('superadmin.dashboard') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Monthly Report</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Report</span>
            </li>
        </ul>
    </div>

    @include('partials.errors')

    <h1 class="page-title"> Monthly <small> Report</small> </h1>

    @include('core.superadmin.monthly-report.report._search')

    @if($row_gettargets != "")
        <div class="col-md-12">
          <div class="portlet light tasks-widget bordered">
            <div class="portlet-body util-btn-margin-bottom-5">


              <div class="page-bar">
                <ul class="page-breadcrumb">
                  <li>
                    <a href="#">Year: <span style="color:purple"> {{$old['year']}}</span></a>
                    <i class="fa fa-circle"></i>
                  </li>
                  <li>
                    <a href="#">Month: <span style="color:purple">@if($old['month'] !=""){{date('F', mktime(0, 0, 0, $old['month'], 10))}}@endif</span></a>
                    <i class="fa fa-circle"></i>
                  </li>
                  <li>
                    <a href="#">Project: <span style="color:purple">{{$old['projectTitle']}}</span></a>
                    <i class="fa fa-circle"></i>
                  </li>
                  <li>
                    <a href="#">Region: <span style="color:purple">{{$old['regionTitle']}}</span></a>
                    <i class="fa fa-circle"></i>
                  </li>
                  <li>
                    <a href="#">District: <span style="color:purple">{{$old['districtTitle']}}</span></a>
                    <i class="fa fa-circle"></i>
                  </li>
                  <li>
                    <a href="#">Upazila: <span style="color:purple">{{$old['upazilaTitle']}}</span></a>
                    <i class="fa fa-circle"></i>
                  </li>
                  <li>
                    <a href="#">Union: <span style="color:purple">{{$old['unionTitle']}}</span></a>
                    <i class="fa fa-circle"></i>
                  </li>
                  <li>
                    <span>Report</span>
                  </li>
                </ul>
              </div>

              <div class="table-responsive">



            <table class="table table-bordered" style="background:#F5F5F5">
                <thead>
                    <tr>
                        <th>Code</th>
                        <th>5422</th>
                        <th>Baseline</th>
                        <th>Target</th>
                        <th>Progress untill last month</th>
                        <th>Progress this month</th>
                        <th>Total progress</th>
                    </tr>
                </thead>
                <tbody>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right"><div align="center">02</div></td>
                        <td>Number of CDF:</td>
                        <td><div align="right">{{$row_getbaseline['SumOfcdf_no']}}</div></td>
                        <td><div align="right">{{$row_gettargets['SumOfcdf_no']}}</div></td>
                        <td><div align="right">{{$row_totaldata['SumOfcdf_no']-$row_currentmonth['SumOfcdf_no']}}</div></td>
                        <td><div align="right">{{$row_currentmonth['SumOfcdf_no']}}</div></td>
                        <td><div align="right">{{$row_totaldata['SumOfcdf_no']}}</div></td>
                    </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right"><div align="center"></div></td>
                            <td>Poulation under CDF:</td>
                            <td><div align="right">{{$row_getbaseline['SumOfcdf_pop']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfcdf_pop']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcdf_pop']-$row_currentmonth['SumOfcdf_pop']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfcdf_pop']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcdf_pop']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right"><div align="center">03</div></td>
                            <td>Male population:</td>
                            <td><div align="right">{{$row_getbaseline['SumOfcdf_male']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfcdf_male']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcdf_male']-$row_currentmonth['SumOfcdf_male']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfcdf_male']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcdf_male']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right"><div align="center">04</div></td>
                            <td>Female population:</td>
                            <td><div align="right">{{$row_getbaseline['SumOfcdf_female']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfcdf_female']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcdf_female']-$row_currentmonth['SumOfcdf_female']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfcdf_female']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcdf_female']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right"><div align="center"></div></td>
                            <td>Hardcode population:</td>
                            <td><div align="right">{{$row_getbaseline['SumOfcdf_pop_hc']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfcdf_pop_hc']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcdf_pop_hc']-$row_currentmonth['SumOfcdf_pop_hc']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfcdf_pop_hc']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcdf_pop_hc']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right"><div align="center">101</div></td>
                            <td>Number of household</td>
                            <td><div align="right">{{$row_getbaseline['SumOfcdf_hh']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfcdf_hh']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcdf_hh']-$row_currentmonth['SumOfcdf_hh']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfcdf_hh']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcdf_hh']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right"><div align="center">102</div></td>
                            <td>Number of hardcore household</td>
                            <td><div align="right">{{$row_getbaseline['SumOfcdf_hh_hc']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfcdf_hh_hc']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcdf_hh_hc']-$row_currentmonth['SumOfcdf_hh_hc']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfcdf_hh_hc']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcdf_hh_hc']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right"><div align="center">103</div></td>
                            <td>Number of disable people</td>
                            <td><div align="right">{{$row_getbaseline['SumOfcdf_pop_disb']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfcdf_pop_disb']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcdf_pop_disb']-$row_currentmonth['SumOfcdf_pop_disb']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfcdf_pop_disb']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcdf_pop_disb']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right"><div align="center"></div></td>
                            <td>Number of people under social safetynet</td>
                            <td><div align="right">{{$row_getbaseline['SumOfcdf_pop_safety']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfcdf_pop_safety']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcdf_pop_safety']-$row_currentmonth['SumOfcdf_pop_safety']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfcdf_pop_safety']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcdf_pop_safety']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of Community Facilitators identified</td>
                            <td><div align="right">{{$row_getbaseline['SumOfcdf_cf_tot']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfcdf_cf_tot']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcdf_cf_tot']-$row_currentmonth['SumOfcdf_cf_tot']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfcdf_cf_tot']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcdf_cf_tot']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of MALE Community Facilitators identified</td>
                            <td><div align="right">{{$row_getbaseline['SumOfcdf_cf_male']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfcdf_cf_male']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcdf_cf_male']-$row_currentmonth['SumOfcdf_cf_male']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfcdf_cf_male']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcdf_cf_male']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of FEMALE Community Facilitators identified</td>
                            <td><div align="right">{{$row_getbaseline['SumOfcdf_cf_female']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfcdf_cf_female']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcdf_cf_female']-$row_currentmonth['SumOfcdf_cf_female']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfcdf_cf_female']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcdf_cf_female']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                        </tr>
                        <tr valign="baseline" class="header">
                            <td colspan="7" align="right" nowrap="nowrap"><div align="left" class="header">UP Management Information</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of PNGO/Project staff recruited </td>
                            <td><div align="right">{{$row_getbaseline['SumOfup_stf_tot']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfup_stf_tot']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfup_stf_tot']-$row_currentmonth['SumOfup_stf_tot']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfup_stf_tot']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfup_stf_tot']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of Male PNGO/Project staff recruited </td>
                            <td><div align="right">{{$row_getbaseline['SumOfup_stf_male']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfup_stf_male']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfup_stf_male']-$row_currentmonth['SumOfup_stf_male']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfup_stf_male']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfup_stf_male']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of Female PNGO/Project staff recruited </td>
                            <td><div align="right">{{$row_getbaseline['SumOfup_stf_female']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfup_stf_female']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfup_stf_female']-$row_currentmonth['SumOfup_stf_female']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfup_stf_female']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfup_stf_female']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of PNGO engaged by UP</td>
                            <td><div align="right">{{$row_getbaseline['SumOfup_pngo']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfup_pngo']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfup_pngo']-$row_currentmonth['SumOfup_pngo']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfup_pngo']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfup_pngo']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of hardware contratctor engaged by UP</td>
                            <td><div align="right">{{$row_getbaseline['SumOfup_cont']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfup_cont']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfup_cont']-$row_currentmonth['SumOfup_cont']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfup_cont']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfup_cont']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Public disclosure board Established (Yes=1, No = 0)</td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                        </tr>
                        <tr valign="baseline" class="header">
                            <td colspan="7" align="right" nowrap="nowrap"><div align="left" class="header">CDF HYGIENE Information</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of handwashing sessions</td>
                            <td><div align="right">{{$row_getbaseline['SumOfCHY_hw_ses']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfCHY_hw_ses']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_hw_ses']-$row_currentmonth['SumOfCHY_hw_ses']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfCHY_hw_ses']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_hw_ses']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of MALE participated in handwashing sessions</td>
                            <td><div align="right">{{$row_getbaseline['SumOfCHY_hw_male']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfCHY_hw_male']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_hw_male']-$row_currentmonth['SumOfCHY_hw_male']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfCHY_hw_male']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_hw_male']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of FEMALE participated in handwashing sessions</td>
                            <td><div align="right">{{$row_getbaseline['SumOfCHY_hw_female']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfCHY_hw_female']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_hw_female']-$row_currentmonth['SumOfCHY_hw_female']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfCHY_hw_female']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_hw_female']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of menstrual hygiene sessions</td>
                            <td><div align="right">{{$row_getbaseline['SumOfCHY_mn_ses']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfCHY_mn_ses']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_mn_ses']-$row_currentmonth['SumOfCHY_mn_ses']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfCHY_mn_ses']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_mn_ses']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of FEMALE participated inmenstrual hygiene sessions</td>
                            <td><div align="right">{{$row_getbaseline['SumOfCHY_mn_female']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfCHY_mn_female']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_mn_female']-$row_currentmonth['SumOfCHY_mn_female']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfCHY_mn_female']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_mn_female']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of hygienic latrine sessions</td>
                            <td><div align="right">{{$row_getbaseline['SumOfCHY_sa_ses']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfCHY_sa_ses']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_sa_ses']-$row_currentmonth['SumOfCHY_sa_ses']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfCHY_sa_ses']}} </div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_sa_ses']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of household participated in hygienic latrine sessions</td>
                            <td><div align="right">{{$row_getbaseline['SumOfCHY_sa_hh']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfCHY_sa_hh']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_sa_hh']-$row_currentmonth['SumOfCHY_sa_hh']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfCHY_sa_hh']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_sa_hh']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of Food hygiene sessions</td>
                            <td><div align="right">{{$row_getbaseline['SumOfCHY_fh_ses']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfCHY_fh_ses']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_fh_ses']-$row_currentmonth['SumOfCHY_fh_ses']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfCHY_fh_ses']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_fh_ses']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of household participated in Food hygiene sessions</td>
                            <td><div align="right">{{$row_getbaseline['SumOfCHY_fh_hh']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfCHY_fh_hh']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_fh_hh']-$row_currentmonth['SumOfCHY_fh_hh']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfCHY_fh_hh']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_fh_hh']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of household attended TW Maintenance sessions</td>
                            <td><div align="right">{{$row_getbaseline['SumOfTW_maintenance']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfTW_maintenance']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfTW_maintenance']-$row_currentmonth['SumOfTW_maintenance']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfTW_maintenance']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfTW_maintenance']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of household attended WSP sessions</td>
                            <td><div align="right">{{$row_getbaseline['SumOfwsp']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfwsp']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfwsp']-$row_currentmonth['SumOfwsp']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfwsp']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfwsp']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of new garbage hole build</td>
                            <td><div align="right">{{$row_getbaseline['SumOfCHY_gb_new']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfCHY_gb_new']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_gb_new']-$row_currentmonth['SumOfCHY_gb_new']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfCHY_gb_new']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_gb_new']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of  garbage hole repaired</td>
                            <td><div align="right">{{$row_getbaseline['SumOfCHY_gb_rep']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfCHY_gb_rep']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_gb_rep']-$row_currentmonth['SumOfCHY_gb_rep']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfCHY_gb_rep']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_gb_rep']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of DRAMA shows in the community</td>
                            <td><div align="right">{{$row_getbaseline['SumOfCHY_dr']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfCHY_dr']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_dr']-$row_currentmonth['SumOfCHY_dr']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfCHY_dr']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_dr']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of people attended in DRAMA shows</td>
                            <td><div align="right">{{$row_getbaseline['SumOfCHY_dr_pop']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfCHY_dr_pop']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_dr_pop']-$row_currentmonth['SumOfCHY_dr_pop']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfCHY_dr_pop']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_dr_pop']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of VIDEO shows in the community</td>
                            <td><div align="right">{{$row_getbaseline['SumOfCHY_vd']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfCHY_vd']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_vd']-$row_currentmonth['SumOfCHY_vd']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfCHY_vd']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_vd']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of people attended in VIDEO shows</td>
                            <td><div align="right">{{$row_getbaseline['SumOfCHY_vd_pop']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfCHY_vd_pop']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_vd_pop']-$row_currentmonth['SumOfCHY_vd_pop']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfCHY_vd_pop']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfCHY_vd_pop']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                        </tr>
                        <tr valign="baseline" class="header">
                            <td colspan="7" align="right" nowrap="nowrap"><div align="left" class="header">School HYGIENE Information</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Total Schools</td>
                            <td><div align="right">{{$row_getbaseline['SumOfscl_tot']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfscl_tot']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_tot']-$row_currentmonth['SumOfscl_tot']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfscl_tot']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_tot']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Total students</td>
                            <td><div align="right">{{$row_getbaseline['SumOfscl_tot_std']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfscl_tot_std']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_tot_std']-$row_currentmonth['SumOfscl_tot_std']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfscl_tot_std']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_tot_std']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Total boys</td>
                            <td><div align="right">{{$row_getbaseline['SumOfscl_girls']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfscl_girls']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_girls']-$row_currentmonth['SumOfscl_girls']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfscl_girls']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_girls']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Total girls</td>
                            <td><div align="right">{{$row_getbaseline['SumOfscl_girls']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfscl_girls']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_girls']-$row_currentmonth['SumOfscl_girls']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfscl_girls']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_girls']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Primary schools</td>
                            <td><div align="right">{{$row_getbaseline['SumOfscl_pri']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfscl_pri']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_pri']-$row_currentmonth['SumOfscl_pri']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfscl_pri']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_pri']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of students in Primary schools</td>
                            <td><div align="right">{{$row_getbaseline['SumOfscl_pri_std']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfscl_pri_std']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_pri_std']-$row_currentmonth['SumOfscl_pri_std']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfscl_pri_std']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_pri_std']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>High schools</td>
                            <td><div align="right">{{$row_getbaseline['SumOfscl_high']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfscl_high']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_high']-$row_currentmonth['SumOfscl_high']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfscl_high']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_high']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of students in high schools</td>
                            <td><div align="right">{{$row_getbaseline['SumOfscl_high_std']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfscl_high_std']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_high_std']-$row_currentmonth['SumOfscl_high_std']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfscl_high_std']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_high_std']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Total Madrasha</td>
                            <td><div align="right">{{$row_getbaseline['SumOfscl_mad']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfscl_mad']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_mad']-$row_currentmonth['SumOfscl_mad']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfscl_mad']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_mad']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of students in Madrasha</td>
                            <td><div align="right">{{$row_getbaseline['SumOfscl_mad_std']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfscl_mad_std']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_mad_std']-$row_currentmonth['SumOfscl_mad_std']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfscl_mad_std']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_mad_std']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Total hygiene promotion sessions</td>
                            <td><div align="right">{{$row_getbaseline['SumOfscl_hp_ses']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfscl_hp_ses']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_hp_ses']-$row_currentmonth['SumOfscl_hp_ses']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfscl_hp_ses']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_hp_ses']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of boys participated in hygiene promotion sessions</td>
                            <td><div align="right">{{$row_getbaseline['SumOfscl_hp_boys']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfscl_hp_boys']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_hp_boys']-$row_currentmonth['SumOfscl_hp_boys']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfscl_hp_boys']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_hp_boys']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of Girls participated in hygiene promotion sessions</td>
                            <td><div align="right">{{$row_getbaseline['SumOfscl_hp_girls']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfscl_hp_girls']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_hp_girls']-$row_currentmonth['SumOfscl_hp_girls']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfscl_hp_girls']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_hp_girls']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Total Menstrual hygiene  sessions</td>
                            <td><div align="right">{{$row_getbaseline['SumOfscl_mn_ses']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfscl_mn_ses']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_mn_ses']-$row_currentmonth['SumOfscl_mn_ses']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfscl_mn_ses']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_mn_ses']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td height="28" align="right" nowrap="nowrap">&nbsp;</td>
                            <td>Number of Girls participated in Menstrual hygiene  session</td>
                            <td><div align="right">{{$row_getbaseline['SumOfscl_mn_girls']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfscl_mn_girls']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_mn_girls']-$row_currentmonth['SumOfscl_mn_girls']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfscl_mn_girls']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_mn_girls']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of Drama played</td>
                            <td><div align="right">{{$row_getbaseline['SumOfscl_dr']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfscl_dr']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_dr']-$row_currentmonth['SumOfscl_dr']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfscl_dr']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_dr']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of students watched drama</td>
                            <td><div align="right">{{$row_getbaseline['SumOfscl_dr_std']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfscl_dr_std']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_dr_std']-$row_currentmonth['SumOfscl_dr_std']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfscl_dr_std']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_dr_std']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of Video shows</td>
                            <td><div align="right">{{$row_getbaseline['SumOfscl_vd']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfscl_vd']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_vd']-$row_currentmonth['SumOfscl_vd']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfscl_vd']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_vd']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of students watched video shows</td>
                            <td><div align="right">{{$row_getbaseline['SumOfscl_vd_std']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfscl_vd_std']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_vd_std']-$row_currentmonth['SumOfscl_vd_std']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfscl_vd_std']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfscl_vd_std']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                        </tr>
                        <tr valign="baseline" class="header">
                            <td colspan="7" align="right" nowrap="nowrap"><div align="left" class="header">Household Sanitation Information</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of new HH latrine built</td>
                            <td><div align="right">{{$row_getbaseline['SumOfHHS_new']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfHHS_new']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfHHS_new']-$row_currentmonth['SumOfHHS_new']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfHHS_new']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfHHS_new']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of new HH latrine built by hardcore </td>
                            <td><div align="right">{{$row_getbaseline['SumOfHHS_new_hc']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfHHS_new_hc']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfHHS_new_hc']-$row_currentmonth['SumOfHHS_new_hc']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfHHS_new_hc']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfHHS_new_hc']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of  HH latrine built improved</td>
                            <td><div align="right">{{$row_getbaseline['SumOfHHS_rep']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfHHS_rep']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfHHS_rep']-$row_currentmonth['SumOfHHS_rep']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfHHS_rep']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfHHS_rep']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of  HH latrine built improved by hardcore</td>
                            <td><div align="right">{{$row_getbaseline['SumOfHHS_rep_hc']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfHHS_rep_hc']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfHHS_rep_hc']-$row_currentmonth['SumOfHHS_rep_hc']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfHHS_rep_hc']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfHHS_rep_hc']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                        </tr>
                        <tr valign="baseline" class="header">
                            <td colspan="7" align="right" nowrap="nowrap"><div align="left" class="header">Public/Institutional Sanitation Information</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of New Sanitation schemes APPROVED</td>
                            <td><div align="right">{{$row_getbaseline['Sum(sa_approved)']}}</div></td>
                            <td><div align="right">{{$row_gettargets['Sum(sa_approved)']}}</div></td>
                            <td><div align="right">{{$row_totaldata['Sum(sa_approved)']-$row_currentmonth['Sum(sa_approved)']}}</div></td>
                             <td><div align="right">{{$row_currentmonth['Sum(sa_approved)']}}</div></td>
                            <td><div align="right">{{$row_totaldata['Sum(sa_approved)']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of New Sanitation schemes COMPLETED</td>
                            <td><div align="right">{{$row_getbaseline['Sum(sa_completed)']}}</div></td>
                            <td><div align="right">{{$row_gettargets['Sum(sa_completed)']}}</div></td>
                            <td><div align="right">{{$row_totaldata['Sum(sa_renovated)']-$row_currentmonth['Sum(sa_renovated)']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['Sum(sa_completed)']}}</div></td>
                            <td><div align="right">{{$row_totaldata['Sum(sa_completed)']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of  Sanitation schemes REPAIRED</td>
                            <td><div align="right">{{$row_getbaseline['Sum(sa_renovated)']}}</div></td>
                            <td><div align="right">{{$row_gettargets['Sum(sa_renovated)']}}</div></td>
                            <td><div align="right">{{$row_totaldata['Sum(sa_renovated)']-$row_currentmonth['Sum(sa_renovated)']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['Sum(sa_renovated)']}}</div></td>
                            <td><div align="right">{{$row_totaldata['Sum(sa_renovated)']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of Beneficiries from New or Renavated Sanitation schemes</td>
                            <td><div align="right">{{$row_getbaseline['Sum(sa_benef)']}}</div></td>
                            <td><div align="right">{{$row_gettargets['Sum(sa_benef)']}}</div></td>
                            <td><div align="right">{{$row_totaldata['Sum(sa_benef)']-$row_currentmonth['Sum(sa_benef)']}}</div></td>
                             <td><div align="right">{{$row_currentmonth['Sum(sa_benef)']}}</div></td>
                            <td><div align="right">{{$row_totaldata['Sum(sa_benef)']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                        </tr>
                        <tr valign="baseline" class="header">
                            <td colspan="7" align="right" nowrap="nowrap">
                              <div align="left" class="header">Capacity Building/Training Information</div>
                            </td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">37</td>
                            <td>Number training courses organized by HYSAWA</td>
                            <td><div align="right">{{$row_getbaseline['SumOfcb_trg']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfcb_trg']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcb_trg']-$row_currentmonth['SumOfcb_trg']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfcb_trg']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcb_trg']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">38</td>
                            <td nowrap="nowrap">Number of UP functionaries recieved training from HYSAWA</td>
                            <td><div align="right">{{$row_getbaseline['SumOfcb_trg_up_total']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfcb_trg_up_total']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcb_trg_up_total']-$row_currentmonth['SumOfcb_trg_up_total']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfcb_trg_up_total']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcb_trg_up_total']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td nowrap="nowrap">Number of MALE UP functionaries recieved training from HYSAWA</td>
                            <td><div align="right">{{$row_getbaseline['SumOfcb_trg_up_male']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfcb_trg_up_male']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcb_trg_up_male']-$row_currentmonth['SumOfcb_trg_up_male']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfcb_trg_up_male']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcb_trg_up_male']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">39</td>
                            <td nowrap="nowrap">Number of FEMALE UP functionaries recieved training from HYSAWA</td>
                            <td><div align="right">{{$row_getbaseline['SumOfcb_trg_up_female']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfcb_trg_up_female']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcb_trg_up_female']-$row_currentmonth['SumOfcb_trg_up_female']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfcb_trg_up_female']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcb_trg_up_female']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of UP/PNGO staff recieved training from HYSAWA</td>
                            <td><div align="right">{{$row_getbaseline['SumOfcb_trg_stf_total']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfcb_trg_stf_total']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcb_trg_stf_total']-$row_currentmonth['SumOfcb_trg_stf_total']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfcb_trg_stf_total']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcb_trg_stf_total']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of UP/PNGO MALE staff recieved training from HYSAWA</td>
                            <td><div align="right">{{$row_getbaseline['SumOfcb_trg_stf_male']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfcb_trg_stf_male']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcb_trg_stf_male']-$row_currentmonth['SumOfcb_trg_stf_male']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfcb_trg_stf_male']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcb_trg_stf_male']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of UP/PNGO FEMALE staff recieved training from HYSAWA</td>
                            <td><div align="right">{{$row_getbaseline['SumOfcb_trg_stf_female']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfcb_trg_stf_female']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcb_trg_stf_female']-$row_currentmonth['SumOfcb_trg_stf_female']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfcb_trg_stf_female']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcb_trg_stf_female']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of Community facilitators recieved training from UP</td>
                            <td><div align="right">{{$row_getbaseline['SumOfcb_trg_vol_total']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfcb_trg_vol_total']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcb_trg_vol_total']-$row_currentmonth['SumOfcb_trg_vol_total']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfcb_trg_vol_total']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcb_trg_vol_total']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of MALE Community facilitators recieved training from UP</td>
                            <td><div align="right">{{$row_getbaseline['SumOfcb_trg_vol_male']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfcb_trg_vol_male']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcb_trg_vol_male']-$row_currentmonth['SumOfcb_trg_vol_male']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfcb_trg_vol_male']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcb_trg_vol_male']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td nowrap="nowrap">Number of FEMALE Community facilitators recieved training from UP</td>
                            <td><div align="right">{{$row_getbaseline['SumOfcb_trg_vol_female']}}</div></td>
                            <td><div align="right">{{$row_gettargets['SumOfcb_trg_vol_female']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcb_trg_vol_female']-$row_currentmonth['SumOfcb_trg_vol_female']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['SumOfcb_trg_vol_female']}}</div></td>
                            <td><div align="right">{{$row_totaldata['SumOfcb_trg_vol_female']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                            <td><div align="right"></div></td>
                        </tr>
                        <tr valign="baseline" class="header">
                            <td colspan="7" align="right" nowrap="nowrap"><div align="left" class="header">Water Supply Information</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of Water supply schemes APPROVED</td>
                            <td><div align="right">{{$row_getbaseline['Sum(ws_approved)']}}</div></td>
                            <td><div align="right">{{$row_gettargets['Sum(ws_approved)']}}</div></td>
                            <td><div align="right">{{$row_totaldata['Sum(ws_approved)']-$row_currentmonth['Sum(ws_approved)']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['Sum(ws_approved)']}}</div></td>
                            <td><div align="right">{{$row_totaldata['Sum(ws_approved)']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of Water supply schemes COMPLETED</td>
                            <td><div align="right">{{$row_getbaseline['Sum(ws_completed)']}}</div></td>
                            <td><div align="right">{{$row_gettargets['Sum(ws_completed)']}}</div></td>
                            <td><div align="right">{{$row_totaldata['Sum(ws_completed)']-$row_currentmonth['Sum(ws_completed)']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['Sum(ws_completed)']}}</div></td>
                            <td><div align="right">{{$row_totaldata['Sum(ws_completed)']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of people benefited from Completed schemes</td>
                            <td><div align="right">{{$row_getbaseline['Sum(ws_beneficiary)']}}</div></td>
                            <td><div align="right">{{$row_gettargets['Sum(ws_beneficiary)']}}</div></td>
                            <td><div align="right">{{$row_totaldata['Sum(ws_beneficiary)']-$row_currentmonth['Sum(ws_beneficiary)']}}</div></td>
                             <td><div align="right">{{$row_currentmonth['Sum(ws_beneficiary)']}}</div></td>
                            <td><div align="right">{{$row_totaldata['Sum(ws_beneficiary)']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of HARDCORE people benefited from Completed schemes</td>
                            <td><div align="right">{{$row_getbaseline['Sum(ws_hc_benef)']}}</div></td>
                            <td><div align="right">{{$row_gettargets['Sum(ws_hc_benef)']}}</div></td>
                            <td><div align="right">{{$row_totaldata['Sum(ws_hc_benef)']-$row_currentmonth['Sum(ws_hc_benef)']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['Sum(ws_hc_benef)']}}</div></td>
                            <td><div align="right">{{$row_totaldata['Sum(ws_hc_benef)']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td nowrap="nowrap">Number of people have access to safe water within 50 metere or 150 ft</td>
                            <td><div align="right">{{$row_getbaseline['Sum(ws_50)']}}</div></td>
                            <td><div align="right">{{$row_gettargets['Sum(ws_50)']}}</div></td>
                            <td><div align="right">{{$row_totaldata['Sum(ws_50)']-$row_currentmonth['Sum(ws_50)']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['Sum(ws_50)']}}</div></td>
                            <td><div align="right">{{$row_totaldata['Sum(ws_50)']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of CARETAKER trainined</td>
                            <td><div align="right">{{$row_getbaseline['Sum(rep_data.CT_trg)']}}</div></td>
                            <td><div align="right">{{$row_gettargets['Sum(rep_data.CT_trg)']}}</div></td>
                            <td><div align="right">{{$row_totaldata['Sum(rep_data.CT_trg)']-$row_currentmonth['Sum(rep_data.CT_trg)']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['Sum(rep_data.CT_trg)']}}</div></td>
                            <td><div align="right">{{$row_totaldata['Sum(rep_data.CT_trg)']}}</div></td>
                        </tr>
                        <tr valign="baseline">
                            <td nowrap="nowrap" align="right">&nbsp;</td>
                            <td>Number of MECHANICS trainined</td>
                            <td><div align="right">{{$row_getbaseline['Sum(rep_data.pdb)']}}</div></td>
                            <td><div align="right">{{$row_gettargets['Sum(rep_data.pdb)']}}</div></td>
                            <td><div align="right">{{$row_totaldata['Sum(rep_data.pdb)']-$row_currentmonth['Sum(rep_data.pdb)']}}</div></td>
                            <td><div align="right">{{$row_currentmonth['Sum(rep_data.pdb)']}}</div></td>
                            <td><div align="right">{{$row_totaldata['Sum(rep_data.pdb)']}}</div></td>
                        </tr>
                    </tbody>
                </table>


                <style>
                    .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
                        border: 1px solid;
                    }
                </style>







































              </div>
            </div>
          </div>
        </div>

        <hr/>
        <br/>
        <h3>Mobile Data Report</h3>
        <br/>
        <br/>

        <div class="col-md-12">
        <style type="text/css">
          #example3 td, #example3 th, #example2 td, #example2 th{
            font-size: 10px;
            text-align: center;
          }
        </style>

        <h3>Event Summary</h3>
        <?php

            $type = "";
            $type_val = "";

            if($old['union_id'] != "")
            {
                $type = "unid";
                $type_val = $old['union_id'];
            }
            elseif($old['upazila_id'] != "")
            {
                $type = "upid";
                $type_val = $old['upazila_id'];
            }
            elseif($old['district_id'] != "")
            {
                $type = "distid";
                $type_val = $old['district_id'];
            }elseif($old['project_id'] != "")
            {
                $type = "proj_id";
                $type_val = $old['project_id'];
            }elseif($old['region_id'] != "")
            {
                $type = "region_id";
                $type_val = $old['region_id'];
            }

            $starting_date = $old['year'].'-'.$old['month'].'-01';
            $days = cal_days_in_month(CAL_GREGORIAN, $old['month'], $old['year']);
            $ending_date   = $old['year'].'-'.$old['month'].'-'.$days;

            $mob = (new \App\Model\MobileAppReport($type, $type_val))
                    ->generateReport($starting_date, $ending_date);
        ?>
        <table class="table table-bordered table-hover data-table" id="example3">
          <thead>
            <tr>
              <th rowspan="2">Total CDF No/School No</th>
              <th colspan="8">Event Type</th>
              <th colspan="3">Location</th>
              <th rowspan="2">Total Men/Boy</th>
              <th rowspan="2">Total Women/Girl</th>
              <th rowspan="2">Disabled</th>
            </tr>
            <tr>
              <th>Hand Wash</th>
              <th>Latrine Maintenance</th>
              <th>Garbage Disposal</th>
              <th>Menstrual Hygiene</th>
              <th>Water Safety</th>
              <th>Food Hygiene</th>
              <th>Climate Change Awareness</th>
              <th>Volunteer Orientation</th>
              <th>Community</th>
              <th>School</th>
              <th>UP</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{{$mob[0]}}</td>
              <td>{{$mob[1]}}</td>
              <td>{{$mob[2]}}</td>
              <td>{{$mob[3]}}</td>
              <td>{{$mob[4]}}</td>
              <td>{{$mob[5]}}</td>
              <td>{{$mob[6]}}</td>
              <td>{{$mob[7]}}</td>
              <td>{{$mob[8]}}</td>
              <td>{{$mob[9]}}</td>
              <td>{{$mob[10]}}</td>
              <td>{{$mob[11]}}</td>
              <td>{{$mob[12]}}</td>
              <td>{{$mob[13]}}</td>
              <td>{{$mob[14]}}</td>
            </tr>
          </tbody>
        </table>

        <br/>
        <br/>

        <h3> Water / Sanitation Summary</h3>
        <table class="table table-bordered table-hover data-table" id="example2">
          <thead>
            <tr>
              <th rowspan="2">ID No</th>
              <th colspan="6">Type</th>
              <th colspan="3">Functionality</th>
              <th colspan="5">Problem Type</th>
              <th colspan="4">Problem Type</th>
            </tr>
            <tr>

              <th>Water - TW</th>
              <th>Water - Sky H</th>
              <th>Water - RO</th>
              <th>Water - RWH</th>
              <th>School Latrine</th>
              <th>Public Latrine</th>

              <th>Non Functional</th>
              <th>Function</th>
              <th>Functional with problems</th>

              <th>High Saline</th>
              <th>High Iron</th>
              <th>Platform Broken</th>
              <th>Dirty</th>
              <th>Maintenance Issue</th>

              <th>Major Repair</th>
              <th>Minor repair</th>
              <th>Awareness</th>
              <th>Improved Management</th>

            </tr>
          </thead>
          <tbody>
            <tr>
              <td>{{$mob[15]}}</td>
              <td>{{$mob[16]}}</td>
              <td>{{$mob[17]}}</td>
              <td>{{$mob[18]}}</td>
              <td>{{$mob[19]}}</td>
              <td>{{$mob[20]}}</td>
              <td>{{$mob[21]}}</td>
              <td>{{$mob[22]}}</td>
              <td>{{$mob[23]}}</td>
              <td>{{$mob[24]}}</td>
              <td>{{$mob[25]}}</td>
              <td>{{$mob[26]}}</td>
              <td>{{$mob[27]}}</td>
              <td>{{$mob[28]}}</td>
              <td>{{$mob[29]}}</td>
              <td>{{$mob[30]}}</td>
              <td>{{$mob[31]}}</td>
              <td>{{$mob[32]}}</td>
              <td>{{$mob[33]}}</td>
            </tr>
          </tbody>
        </table>
      </div>

    @else
        @if(!empty($old) && !empty($old['query']))

        <div class="col-md-12">
          <div class="portlet light tasks-widget bordered">
            <p>
              No Data Found
            </p>
          </div>
        </div>

        @endif
    @endif
@endsection

@section('my_js')
<script type="text/javascript">
  $(document).ready(function () {

    $('#district_id').on('change', function() {
      var head_id = $(this).val();

      $("#upazila_id").html("Select District");
      $("#union_id").html("Select Upazila");

      $.ajax({
        url: '{{route('superadmin.ajax.upazila')}}?district_id='+head_id,
        type: 'GET',
        success: function(response){
          if(response['status'] == true){
            $("#upazila_id").html(response['upazila_list']);
          }
        }
      });
    });
    $('#upazila_id').on('change', function() {
      var upazila_id = $(this).val();

      $("#union_id").html("Select Upazila");

      $.ajax({
        url: '{{route('superadmin.ajax.union')}}?upazila_id='+upazila_id,
        type: 'GET',
        success: function(response){
          if(response['status'] == true){
            $("#union_id").html(response['union_list']);
          }
        }
      });
    });
  });
</script>
@endsection





