<div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i>Filter Result </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body form">
          <form action="{{route('superadmin.monthly-report.report.index')}}" class="form-horizontal" method="GET" id="createForm">

              <input type="hidden" name="query" value="{{time()}}" />

              <div class="form-body">





                  <div class="row">
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">Year</label>
                              <div class="col-md-9">
                                  <select class="form-control" name="year" required="required">
                                    <option value="">Select Year</option>
                                    @for($index = date('Y'); $index >= 2012; $index--)
                                      <option value="{{$index}}"

                                        @if($old['year'] != "" && $old['year'] == $index)
                                          selected="selected"
                                        @endif

                                      >{{$index}}</option>
                                    @endfor
                                  </select>


                              </div>
                          </div>
                      </div>

    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label col-md-3">Month</label>
            <div class="col-md-9">
              <select class="form-control" name="month" required="required">
                <option value="">Select Month</option>
                <option value="1" @if($old['month'] && $old['month'] == 1) selected="selected" @endif>January</option>
                <option value="2" @if($old['month'] && $old['month'] == 2) selected="selected" @endif>February</option>
                <option value="3" @if($old['month'] && $old['month'] == 3) selected="selected" @endif>March</option>
                <option value="4" @if($old['month'] && $old['month'] == 4) selected="selected" @endif>April</option>

                <option value="5" @if($old['month'] && $old['month'] == 5) selected="selected" @endif>May</option>
                <option value="6" @if($old['month'] && $old['month'] == 6) selected="selected" @endif>June</option>
                <option value="7" @if($old['month'] && $old['month'] == 7) selected="selected" @endif>July</option>
                <option value="8" @if($old['month'] && $old['month'] == 8) selected="selected" @endif>August</option>

                <option value="9" @if($old['month'] && $old['month'] == 9) selected="selected" @endif>September</option>
                <option value="10" @if($old['month'] && $old['month'] == 10) selected="selected" @endif>October</option>
                <option value="11" @if($old['month'] && $old['month'] == 11) selected="selected" @endif>November</option>
                <option value="12" @if($old['month'] && $old['month'] == 12) selected="selected" @endif>December</option>
              </select>
            </div>
        </div>
    </div>

                  </div>



                  <div class="row">

                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">Project</label>
                              <div class="col-md-9">
                                  <select class="form-control" name="project_id">
                                    <option value="">Select Project</option>

                                    @if($projects != "")
                                      @foreach($projects as $project)
                                        <option value="{{$project->id}}"
                                          @if($old['projectTitle'] == $project->project)
                                            selected="selected"
                                          @endif
                                        >{{$project->project}}</option>
                                      @endforeach
                                    @endif



                                  </select>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">Region</label>
                              <div class="col-md-9">
                                  <select class="form-control" name="region_id">

                                    <option value="">Select Region</option>

                                    @if($regions != "")
                                      @foreach($regions as $region)
                                        <option value="{{$region->id}}"
                                          @if($old['regionTitle'] == $region->region_name)
                                            selected="selected"
                                          @endif
                                        >{{$region->region_name}}</option>
                                      @endforeach
                                    @endif

                                  </select>
                              </div>
                          </div>
                      </div>

                  </div>


                  <div class="row">

                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">District</label>
                              <div class="col-md-9">
                                  <select class="form-control" id="district_id" name="district_id">
                                    <option value="">Select District</option>
                                    @foreach($districts as $district)
                                      <option value="{{$district->id}}"

                                      @if($old['districtTitle'] ==$district->distname)
                                        selected="selected"
                                      @endif

                                      >{{$district->distname}}</option>
                                    @endforeach
                                  </select>
                              </div>
                          </div>
                      </div>


                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">Upazila</label>
                              <div class="col-md-9">
                                  <select class="form-control" id="upazila_id" name="upazila_id">
                                    <option value="">Select Upazila</option>

                                    @if($upazilas != "")
                                      @foreach($upazilas as $upazila)
                                        <option value="{{$upazila->id}}"
                                          @if($old['upazilaTitle'] == $upazila->upname)
                                            selected="selected"
                                          @endif
                                        >{{$upazila->upname}}</option>
                                      @endforeach
                                    @endif

                                  </select>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-4">
                        <div class="form-group">
                          <label class="control-label col-md-3">Union</label>
                          <div class="col-md-9">
                            <select name="union_id" class="form-control" id="union_id" name="union_id">
                              <option value="">Select Union</option>

                                @if($unions != "")
                                  @foreach($unions as $union)
                                    <option value="{{$union->id}}"
                                      @if($old['unionTitle'] == $union->unname)
                                        selected="selected"
                                      @endif
                                    >{{$union->unname}}</option>
                                  @endforeach
                                @endif

                            </select>
                          </div>
                        </div>
                      </div>


                  </div>

                  <div class="row">
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-6">
                            <input type="submit" class="btn green" value="Generate Report" />
                            <a href="{{route('superadmin.monthly-report.report.index')}}" class="btn btn-info">Clear</a>

                            @if(!empty($old['query']))
                              <?php
                                $old['print'] = 'print';
                              ?>
                              <a href="{{route('superadmin.monthly-report.report.index', $old)}}" class="btn btn-primary">Print</a>

                            @endif

                        </div>

                    </div>
                  </div>
              </div>
          </form>

      </div>
    </div>
  </div>