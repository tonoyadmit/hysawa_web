@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('superadmin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="{{route('superadmin.user-management.index')}}">User Management</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Add New</span>
    </li>
  </ul>
</div>

@include('partials.errors')

<h1 class="page-title"> User Management <small></small> </h1>

<div class="col-md-12">
  <div class="portlet box blue">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i>Update User </div>
        <div class="tools">
          <a href="javascript:;" class="collapse"> </a>
        </div>
      </div>

      <div class="portlet-body form">
        <form action="{{route('superadmin.user-management.update', $user->id)}}" class="form-horizontal" method="POST" id="createForm">
          {{csrf_field()}}
          <input type="hidden" name="query" value="{{time()}}" />

          <div class="form-body">

            <div class="row">

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">Name</label>
                    <div class="col-md-4">
                      <input
                      type="text"
                      class="form-control"
                      name="name"
                      value="{{$user->name}}"
                      required="required"
                       />
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">UserId</label>
                    <div class="col-md-4">
                      <input
                      type="text"
                      class="form-control"
                      name="email"
                      value="{{$user->email}}"
                      required="required"
                      />
                    </div>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">Supervisor</label>
                    <div class="col-md-4">
                      <input
                      type="text"
                      class="form-control"
                      name="supervisor"
                      value="{{$user->supervisor}}"
                       />
                    </div>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">Contact No</label>
                    <div class="col-md-4">
                      <input
                      type="text"
                      class="form-control"
                      name="msisdn"
                      value="{{$user->msisdn}}"
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">Status</label>
                    <div class="col-md-4">
                      <select class="form-control" id="status" name="status">
                        <option value="1" @if($user->status == 1) selected="selected" @endif >Active</option>
                        <option value="0" @if($user->status == 0) selected="selected" @endif>Inactive</option>
                      </select>
                    </div>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">Role</label>
                    <div class="col-md-4">
                      <select class="form-control" id="role_id" name="role_id" required="required">
                        <option value="">Select Role</option>
                        @foreach($roles as $role)
                          <option value="{{$role->id}}"

                          @if($user->roles->first()->id == $role->id)
                            selected="selected"
                          @endif

                          >{{$role->display_name}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">Region</label>
                    <div class="col-md-4">
                      <select class="form-control" id="region_id" name="region_id">
                        <option value="">Select Region</option>
                        @foreach($regions as $region)
                        <option value="{{$region->region_id}}"

                        @if($user->region_id == $region->region_id)
                          selected="selected"
                        @endif

                        >{{$region->region_name}}</option>
                        @endforeach

                      </select>
                    </div>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">Project</label>
                    <div class="col-md-4">
                      <select name="project_id" class="form-control" id="project_id">
                        <option value="">Select Project</option>
                        @foreach($projects as $project)
                        <option value="{{$project->id}}"

                        @if($user->proj_id == $project->id)
                          selected="selected"
                        @endif
                        >{{$project->project}}</option>
                        @endforeach

                      </select>
                    </div>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">District</label>
                    <div class="col-md-4">
                      <select class="form-control" id="district_id" name="district_id">
                        <option value="">Select District</option>
                        @foreach($districts as $district)
                        <option value="{{$district->id}}"

                          @if($user->distid == $district->id)
                            selected="selected"
                          @endif

                        >{{$district->distname}}</option>
                        @endforeach
                      </select>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">Upazila</label>
                    <div class="col-md-4">
                      <select class="form-control" id="upazila_id" name="upazila_id">
                        <option value="">Select Upazila</option>
                @if(!empty($upazilas))
                        @foreach($upazilas as $upazila)
                          <option value="{{$upazila->id}}"

                          @if($user->upid == $upazila->id)
                            selected="selected"
                          @endif

                          >{{$upazila->upname}}</option>
                        @endforeach
                @endif
                      </select>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">Union</label>
                    <div class="col-md-4">
                      <select name="union_id" class="form-control" id="union_id">
                        <option value="">Select Union</option>

                        @if(!empty($unions))
                         @foreach($unions as $union)
                          <option value="{{$union->id}}"

                          @if($user->unid == $union->id)
                            selected="selected"
                          @endif

                          >{{$union->unname}}</option>
                        @endforeach
                        @endif

                      </select>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="form-group">
                  <div class="col-md-offset-1 col-md-6">
                    <input type="submit" class="btn green" value="Update User" />
                  </div>
                </div>
              </div>


            </div>
          </form>

        </div>
      </div>
    </div>

    @endsection

    @section('my_js')
    <script type="text/javascript">
      $(document).ready(function () {

        $('#district_id').on('change', function() {
          var head_id = $(this).val();

          $("#upazila_id").html("Select District");
          $("#union_id").html("Select Upazila");

          $.ajax({
            url: '{{route('superadmin.ajax.upazila')}}?district_id='+head_id,
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#upazila_id").html(response['upazila_list']);
              }
            }
          });
        });
        $('#upazila_id').on('change', function() {
          var upazila_id = $(this).val();

          $("#union_id").html("Select Upazila");

          $.ajax({
            url: '{{route('superadmin.ajax.union')}}?upazila_id='+upazila_id,
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#union_id").html(response['union_list']);
              }
            }
          });
        });
      });
    </script>
    @endsection