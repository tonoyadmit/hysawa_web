<div class="col-md-12" style="margin-top: 10px;">
    <div class="portlet box blue">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i>User <small> List</small> </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body form">
          <form action="{{route('superadmin.user-management.index')}}" class="form-horizontal" method="GET" id="createForm">

              <input type="hidden" name="query" value="{{time()}}" />

              <div class="form-body">

                   <div class="row">
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">Role</label>
                              <div class="col-md-9">
                                  <select class="form-control" id="role_id" name="role_id">

                                    <option value="">Select Role</option>

                                    @foreach($roles as $role)
                                      <option value="{{$role->id}}">{{$role->display_name}}</option>
                                    @endforeach
                                  </select>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">Region</label>
                              <div class="col-md-9">
                                  <select class="form-control" id="region_id" name="region_id">
                                    <option value="">Select Region</option>
                                    @foreach($regions as $region)
                                      <option value="{{$region->region_id}}">{{$region->region_name}}</option>
                                    @endforeach

                                  </select>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">Project</label>
                              <div class="col-md-9">
                                <select name="project_id" class="form-control" id="project_id">
                                  <option value="">Select Project</option>
                                  @foreach($projects as $project)
                                    <option value="{{$project->id}}">{{$project->project}}</option>
                                  @endforeach

                                </select>
                              </div>
                          </div>
                      </div>
                  </div>


                  <div class="row">
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">District</label>
                              <div class="col-md-9">
                                  <select class="form-control" id="district_id" name="district_id">
                                    <option value="">Select District</option>
                                    @foreach($districts as $district)
                                      <option value="{{$district->id}}">{{$district->distname}}</option>
                                    @endforeach
                                  </select>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">Upazila</label>
                              <div class="col-md-9">
                                  <select class="form-control" id="upazila_id" name="upazila_id">
                                    <option value="">Select Upazila</option>
                                  </select>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">Union</label>
                              <div class="col-md-9">
                                <select name="union_id" class="form-control" id="union_id">
                                  <option value="">Select Union</option>
                                </select>
                              </div>
                          </div>
                      </div>
                  </div>


                  <div class="row">
                      <div class="col-md-12">
                          <div class="form-group">
                              <label class="control-label col-md-1">Name</label>
                              <div class="col-md-11">
                                  <input
                                    type="text"
                                    class="form-control"
                                    name="name"
                                    @if($old['name'])
                                      value="{{$old['name']}}"
                                    @endif />
                              </div>
                          </div>
                      </div>
                  </div>


                  <div class="row">
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-6">
                            <input type="submit" class="btn green" value="Filter Data" />
                            <a href="{{route('superadmin.user-management.index')}}" class="btn btn-info">Clear</a>
                        </div>
                    </div>
                  </div>
              </div>
          </form>

      </div>
    </div>
  </div>