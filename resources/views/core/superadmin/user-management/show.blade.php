@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="{{route('superadmin.user-management.index')}}">User Management</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Details of User</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

  <h1 class="page-title"> User Details </h1>

  <div class="col-md-offset-1 col-md-10">
    <div class="portlet light tasks-widget bordered">
      <div class="portlet-body util-btn-margin-bottom-5">

        <div class="table-responsive">
          <style>
            #example0 td {
              white-space: nowrap;
            }
          </style>

          <table class="table table-bordered table-hover" id="example0">

              <tr><th>Name</th><td>{{$user->name}}</td> </tr>
              <tr><th>Username</th><td>{{$user->email}}</td> </tr>
              <tr><th>Supervisor</th><td>{{$user->supervisor}}</td></tr>
              <tr><th>MSISDN</th><td>{{$user->msisdn}}</td></tr>
              <tr><th>Created At</th><td>{{date('Y-m-d H:i:s', strtotime($user->created_at) ) }}</td></tr>
              <tr><th>Created By</th><td>
                <?php
                  $created = "";

                  if($user->created_by != "")
                  {
                    $created = \DB::table('users')->where('id', $user->created_by)->get();
                    if( count($created) )
                    {
                      $created = $created->first()->name;
                    }
                  }

                ?>@if($created != "") {{$created}}@endif</td></tr>
              <tr><th>Updated At</th><td>{{date('Y-m-d H:i:s', strtotime( $user->updated_at) ) }}</td></tr>
              <tr><th>Updated By</th><td>
                <?php
                  $updated = "";

                  if($user->created_by != "")
                  {
                    $updated = \DB::table('users')->where('id', $user->updated_by)->get();
                    if( count($updated) )
                    {
                      $updated = $updated->first()->name;
                    }
                  }

                ?>@if($updated != "") {{$updated}}@endif</td></tr>

              <tr><th>Status</th><td>{{$user->status}}</td> </tr>
              <tr><th>Role</th><td>@if($user->roles != "") {{$user->roles->first()->display_name}} @endif</td> </tr>
              <tr><th>Region</th><td>@if($user->region_id != "") {{$user->region->region_name}} @endif</td> </tr>
              <tr><th>Project</th><td>@if($user->proj_id != "") {{$user->project->project}} @endif</td> </tr>
              <tr><th>District</th><td>@if($user->distid != "") {{$user->district->distname}} @endif</td> </tr>
              <tr><th>Upazila</th><td>@if($user->upid != "") {{$user->upazila->upname}} @endif</td> </tr>
              <tr><th>Union</th><td>@if($user->unid != "") {{$user->union->unname}} @endif</td> </tr>
              <tr>
                <th>Action</th>
                <td>
                  <div class="btn-group">
                        <a class="btn green" href="javascript:;" data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-user"></i> Option
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu" role="menu">

                            <li>
                                <a href="{{route('superadmin.user-management.edit', $user->id)}}">
                                <i class="fa fa-pencil"></i>
                                Edit </a>
                            </li>
                            <li>
                                <a href="{{route('superadmin.user-management.status-change', $user->id)}}">

                                @if($user->status == 1)
                                <i class="fa fa-thumbs-down"></i>
                                @else
                                <i class="fa fa-thumbs-up"></i>
                                @endif
                                Change Status </a>
                            </li>
                            <li>
                                <a href="{{route('superadmin.user-management.password-change', $user->id)}}">
                                <i class="fa fa-key"></i>
                                Change Password </a>
                            </li>

                        </ul>
                    </div>

                </td>
              </tr>

          </table>
        </div>
      </div>
    </div>
  </div>




@endsection


