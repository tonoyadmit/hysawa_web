@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">User Management</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>User List</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

  @include('core.superadmin.user-management._search')

  @if(count($users))

  <div class="col-md-12">
    <div class="portlet light tasks-widget bordered">

      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i>Filter Result
          </div>

           <div class="actions">
              <a href="{{route('superadmin.user-management.index', $old)}}" target="_blank" class="btn btn-circle success"> <i class="fa fa-download"></i> Download</a>
            </div>

          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body util-btn-margin-bottom-5">

        <div class="table-responsive">
          <style>
            #example0 td {
              white-space: nowrap;
              font-size: 10px;
            }
            #example0 th {
              font-size: 10px;
            }
          </style>

          <table class="table table-bordered table-hover table-condensed" id="example0">
            <thead >
              <tr>
              <th>Action</th>
              <th>Name</th>
              <th>Username</th>
              <th>Role</th>
              <th>Region</th>

              <th>Project</th>
              <th>District</th>
              <th>Upazila</th>
              <th>Union</th>

              <th>Status</th>

              </tr>
            </thead>

            <tbody>
              @foreach($users as $user)
                <tr>
                  <td>
                    <div class="btn-group">
                        <a class="btn green" href="javascript:;" data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-user"></i> Option
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{route('superadmin.user-management.show', $user->id)}}">
                                <i class="fa fa-eye"></i>
                                Show </a>
                            </li>
                            <li>
                                <a href="{{route('superadmin.user-management.edit', $user->id)}}">
                                <i class="fa fa-pencil"></i>
                                Edit </a>
                            </li>

                            <li>
                                <a href="{{route('superadmin.user-management.password-change', $user->id)}}">
                                <i class="fa fa-key"></i>
                                Change Password </a>
                            </li>

                        </ul>
                    </div>
                  </td>
                  <td>{{$user->name}}</td>
                  <td>{{$user->email}}</td>
                  <td>@if($user->roles()->count()) {{$user->roles->first()->display_name or ''}} @endif</td>
                  <td>@if(isset($user->region_id)) {{$user->region->region_name or ''}} @endif</td>

                  <td>@if(isset($user->proj_id)) {{$user->project->project or ''}} @endif</td>
                  <td>@if(isset($user->distid)) {{$user->district->distname or ''}} @endif</td>
                  <td>@if(isset($user->upid)) {{$user->upazila->upname or ''}} @endif</td>
                  <td>@if(isset($user->unid)) {{$user->union->unname or ''}} @endif</td>

                  <td>{{$user->status}}</td>
                </tr>
              @endforeach
            </tbody>
          </table>
          {{$users->appends($old)->links()}}
        </div>
      </div>
    </div>
  </div>



  @else
  <div class="col-md-12">
    <div class="portlet light tasks-widget bordered">
      <p>
        No Data Found
      </p>
    </div>
  </div>
  @endif
@endsection


@section('my_js')
<script type="text/javascript">
  $(document).ready(function () {

    $('#district_id').on('change', function() {
      var head_id = $(this).val();

      $("#upazila_id").html("Select District");
      $("#union_id").html("Select Upazila");

      $.ajax({
        url: '{{route('superadmin.ajax.upazila')}}?district_id='+head_id+'&not_all=true',
        type: 'GET',
        success: function(response){
          if(response['status'] == true){
            $("#upazila_id").html(response['upazila_list']);
          }
        }
      });
    });
    $('#upazila_id').on('change', function() {
      var upazila_id = $(this).val();

      $("#union_id").html("Select Upazila");

      $.ajax({
        url: '{{route('superadmin.ajax.union')}}?upazila_id='+upazila_id+'&not_all=true',
        type: 'GET',
        success: function(response){
          if(response['status'] == true){
            $("#union_id").html(response['union_list']);
          }
        }
      });
    });
  });
</script>
@endsection