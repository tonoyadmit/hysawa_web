@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">UP</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>List of UP/PNGO Project staff</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')
  @include('core.superadmin.up.stuff._search')

  @if( count($users) > 0)

  <div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          <i class="fa fa-gift"></i>Filter Result
        </div>

        <div class="actions">

        </div>

        <div class="tools">
          <a href="javascript:;" class="collapse"> </a>
        </div>
      </div>

      <div class="portlet-body">
        <table class="table table-bordered table-hover" id="example0">

          <thead >
            <tr>
            <th>District</th>
            <th>Upazila</th>
            <th>Union</th>
            <th>Name</th>
            <th>Designation</th>
            <th>Phone</th>
            <th>E-mail</th>
            </tr>
          </thead>

          <tbody>
            @foreach($users as $user)
            <tr>

              <td>{{$user->district->distname or '' }}</td>
              <td>{{$user->upazila->upname or ''}}</td>
              <td>{{$user->union->unname or ''}}</td>
              <td>{{$user->name}}</td>
              <td>{{$user->des}}</td>
              <td>{{$user->phone}}</td>
              <td>{{$user->email}}</td>


            </tr>
            @endforeach
          </tbody>
        </table>
        {{$users->appends($_REQUEST)->links()}}
      </div>
    </div>
  </div>

  @endif

@endsection

@section('my_js')
  <script type="text/javascript">
    $(document).ready(function () {

      $('#district_id').on('change', function() {
        var head_id = $(this).val();

        $("#upazila_id").html("Select District");
        $("#union_id").html("Select Upazila");

        $.ajax({
          url: '{{route('superadmin.ajax.upazila')}}?district_id='+head_id+'&not_all=true&choose_one=true',
          type: 'GET',
          success: function(response){
            if(response['status'] == true){
              $("#upazila_id").html(response['upazila_list']);
            }
          }
        });
      });
      $('#upazila_id').on('change', function() {
        var upazila_id = $(this).val();

        $("#union_id").html("Select Upazila");

        $.ajax({
          url: '{{route('superadmin.ajax.union')}}?upazila_id='+upazila_id+'&not_all=true&choose_one=true',
          type: 'GET',
          success: function(response){
            if(response['status'] == true){
              $("#union_id").html(response['union_list']);
            }
          }
        });
      });
    });
  </script>
@endsection