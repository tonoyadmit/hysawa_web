<div class="col-md-12" style="margin-top: 10px;">
    <div class="portlet box blue">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i>Functionaries <small> Personel List</small> </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body form">
          <form action="{{route('superadmin.up.functionaries.index')}}" class="form-horizontal" method="GET" id="createForm">

              <input type="hidden" name="query" value="{{time()}}" />

              <div class="form-body">

                  <div class="row">
                      <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-3">Project</label>
                              <div class="col-md-9">
                                  <select name="project_id" class="form-control" id="project_id">
                                  <option value="">Select Project</option>
                                  @foreach($projects as $project)
                                    <option value="{{$project->id}}"

                                      @if(isset($_REQUEST['project_id']) && $_REQUEST['project_id'] == $project->id)
                                        selected="selected"
                                      @endif

                                    >{{$project->project}}</option>
                                  @endforeach
                                </select>
                              </div>
                          </div>
                      </div>


                      <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-3">District</label>
                              <div class="col-md-9">
                                  <select class="form-control" id="district_id" name="district_id">
                                      <option value="">Select a District</option>
                                    @foreach($districts as $district)
                                      <option value="{{$district->id}}"

                                        @if(isset($_REQUEST['district_id']) && $_REQUEST['district_id'] == $district->id)
                                          selected="selected"
                                        @endif

                                      >{{$district->distname}}</option>
                                    @endforeach
                                  </select>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-3">Upazila</label>
                              <div class="col-md-9">
                                  <select class="form-control" id="upazila_id" name="upazila_id">
                                    @if(isset($_REQUEST['district_id']) && $_REQUEST['district_id'] != "")

                                        @if($_REQUEST['district_id'] != "all")
                                            <?php
                                            $upazilas = App\Model\Upazila::where('disid', $_REQUEST['district_id'])->get();
                                            ?>

                                            <option value="">Choose an Option</option>

                                            @foreach($upazilas as $upazila)
                                                <option value="{{$upazila->id}}"
                                                    @if(isset($_REQUEST['upazila_id']) && $_REQUEST['upazila_id'] == $upazila->id)
                                                        selected="selected"
                                                    @endif
                                                >{{$upazila->upname}}</option>
                                            @endforeach
                                        @endif

                                    @else
                                        <option value="">Select Upazila</option>
                                    @endif
                                  </select>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-3">Union</label>
                              <div class="col-md-9">
                                <select name="union_id" class="form-control" id="union_id">
                                  @if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] != "")
                                    @if($_REQUEST['upazila_id'] != "all")
                                        <?php
                                            $unions = App\Model\Union::where('upid', $_REQUEST['upazila_id'])->get();
                                        ?>
                                        <option value="">Choose an Option</option>
                                        @foreach($unions as $union)
                                            <option value="{{$union->id}}"
                                                @if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] == $union->id)
                                                    selected="selected"
                                                @endif
                                            >{{$union->unname}}</option>
                                        @endforeach
                                    @endif
                                  @else
                                      <option value="">Choose Upazila First</option>
                                  @endif
                                </select>
                              </div>
                          </div>
                      </div>
                  </div>




                  <div class="row">
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-5">
                            <input type="submit" class="btn green" value="Get Data" />
                            <a href="{{route('superadmin.up.functionaries.index')}}" class="btn btn-info">Clear</a>
                        </div>
                        <div class=" col-md-6">
                            <?php
                              $oldDownload = $old;
                              $oldDownload['download'] = "download";
                            ?>
                            <a href="{{route('superadmin.up.functionaries.index', $oldDownload)}}" class="btn btn-info pull-right">Download</a>
                        </div>
                    </div>
                  </div>
              </div>
          </form>

      </div>
    </div>
  </div>