@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">UP</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="{{route('superadmin.up.mou.index')}}">MOU </a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Region Wise List</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

  <h1 class="page-title"> Region: {{$region->region_name}} </h1>

  @if(count($regions) > 0)
    <div class="col-md-12">
        <div class="portlet light tasks-widget bordered">
          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-gift"></i>Filter Result
            </div>
            <div class="action">
              <a href="{{route('superadmin.finance.report.analysis.index', ['download' => 'download'])}}" class="btn btn-success">Download</a>
            </div>
            <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
            </div>
          </div>

            <div class="portlet-body util-btn-margin-bottom-5">
                <table class="table table-bordered table-hover" id="example0">

                    <thead>
                      <th>Project</th>
                      <th>District</th>
                      <th>Upazila</th>
                      <th>Union</th>
                      <th>MOU Date</th>
                    </thead>

                    <tbody>
                      @foreach($regions as $p)
                        <tr>
                          <td nowrap="nowrap">{{$p->project}}</td>
                          <td >{{$p->distname}}</td>
                          <td >{{$p->upname}}</td>
                          <td nowrap="nowrap">{{$p->unname}}</td>
                          <td nowrap="nowrap">{{$p->moudate}}</td>
                        </tr>
                      @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
  @endif

@endsection