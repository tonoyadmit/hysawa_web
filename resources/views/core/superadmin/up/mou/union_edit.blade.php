@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('superadmin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="#">UP</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Update MOU Information</span>
    </li>
  </ul>
</div>

<h1 class="page-title"> UP <small>Enter MOU Information</small> </h1>

<div class="col-md-12">
    <div class="portlet box blue">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i>Filter Result </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body form">
          <form action="{{route('superadmin.up.mou.update', $mou->id)}}" class="form-horizontal" method="POST" id="createForm">

             {{csrf_field()}}

              <div class="form-body">

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="control-label col-md-3">Union</label>
                            <div class="col-md-9">
                            <label class="control-label col-md-3">{{$mou->unname}}</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="control-label col-md-3">MOU Date:</label>
                            <div class="col-md-9">
                                <input
                                  type="text"
                                  name="contractdate"
                                  class="form-control date-picker"
                                  required="required"
                                  data-date-format="yyyy-mm-dd"
                                  readonly
                                  value="{{date('Y-m-d', strtotime($mou->moudate))}}"/>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="control-label col-md-3">Remarks:</label>
                            <div class="col-md-9">
                                <input type="text" name="remarks" class="form-control" value="{{$mou->remarks}}" />
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                  <div class="form-group">
                      <div class="col-md-offset-1 col-md-8">
                          <input type="submit" class="btn green" value="Update" />

                      </div>
                  </div>
                </div>
              </div>
          </form>

      </div>
    </div>
  </div>



@endsection

@section('my_js')
<script type="text/javascript">
  $(document).ready(function () {

    $('#district_id').on('change', function() {
      var head_id = $(this).val();

      $("#upazila_id").html("Select District");
      $("#union_id").html("Select Upazila");

      $.ajax({
        url: '{{route('superadmin.ajax.upazila')}}?district_id='+head_id,
        type: 'GET',
        success: function(response){
          if(response['status'] == true){
            $("#upazila_id").html(response['upazila_list']);
          }
        }
      });
    });
    $('#upazila_id').on('change', function() {
      var upazila_id = $(this).val();

      $("#union_id").html("Select Upazila");

      $.ajax({
        url: '{{route('superadmin.ajax.union')}}?upazila_id='+upazila_id,
        type: 'GET',
        success: function(response){
          if(response['status'] == true){
            $("#union_id").html(response['union_list']);
          }
        }
      });
    });
  });
</script>
@endsection