@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('superadmin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Finance</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Charts Of Account </a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Sub Head List</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

  <h1 class="page-title"> MOU List <small> list</small> </h1>

  <div class="col-md-6">
      <div class="portlet light tasks-widget bordered">
          <div class="portlet-body util-btn-margin-bottom-5">

          <a href="{{route('superadmin.up.mou.create')}}" class="btn btn-success btn-large">Enter new MOU Information</a>

          </div>
      </div>
  </div>

  @if(count($projects) > 0)
    <div class="col-md-8">
        <div class="portlet light tasks-widget bordered">


            <div class="portlet-body util-btn-margin-bottom-5">
                <table class="table table-bordered table-hover" id="example0">

                  <thead >
                    <th>Project</th>
                    <th>Nos. of Union</th>
                  </thead>

                  <tbody>
                    @foreach($projects as $project)
                      <tr>
                        <td><a href="{{route('superadmin.up.mou.project', $project->id)}}">{{$project->project}}</a></td>
                        <td>{{$project->total}}</td>
                      </tr>
                    @endforeach
                  </tbody>

                </table>

            </div>
        </div>
    </div>
  @endif


  @if(count($regions) > 0)
    <div class="col-md-8">
        <div class="portlet light tasks-widget bordered">
            <div class="portlet-body util-btn-margin-bottom-5">
                <table class="table table-bordered table-hover" id="example0">
                  <thead >
                    <th>Project</th>
                    <th>Region</th>
                    <th>Nos. of Union</th>
                  </thead>

                  <tbody>
                    @foreach($regions as $region)
                      <tr>
                        <td>{{$region->project}}</td>
                        <td><a href="{{route('superadmin.up.mou.region', $region->region_id)}}">{{$region->region_name}}</a></td>
                        <td>{{$region->total}}</td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>

            </div>
        </div>
    </div>
  @endif


  @if(count($districts) > 0)
    <div class="col-md-12">
        <div class="portlet light tasks-widget bordered">
            <div class="portlet-body util-btn-margin-bottom-5">
                <table class="table table-bordered table-hover" id="example0">

                    <thead >
                      <th>Project</th>
                      <th>District</th>
                      <th>Nos. of Union</th>
                    </thead>

                    <tbody>
                      @foreach($districts as $district)
                        <tr>
                          <td nowrap="nowrap">{{$district->project}}</td>
                          <td nowrap="nowrap"><a href="{{route('superadmin.up.mou.district', $district->id)}}">{{$district->distname}}</a></td>
                          <td nowrap="nowrap">{{$district->total}}</td>
                        </tr>
                      @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
  @endif


  @if(count($unions) > 0)
    <div class="col-md-12">
        <div class="portlet light tasks-widget bordered">

          <div class="portlet-title">
            <div class="caption">
              <i class="fa fa-gift"></i> Filter Result
            </div>

            <div class="actions">
              <a href="{{route('superadmin.up.mou.index', ['download' => 'download'])}}" class="btn btn-circle success"> <i class="fa fa-download"></i> Download</a>
            </div>
            <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
            </div>
          </div>

            <div class="portlet-body util-btn-margin-bottom-5">
                <table class="table table-bordered table-hover" id="example0">

                    <thead>
                      <th>Action</th>
                      <th>Project</th>
                      <th>District</th>
                      <th>Upazila</th>
                      <th>Union</th>
                      <th>MOU Date</th>
                      <th>Remarks</th>

                    </thead>

                    <tbody>
                      @foreach($unions as $union)
                        <tr>
                          <td>
                            <a
                              class="label label-success"
                              href="{{route('superadmin.up.mou.edit', $union->id)}}">
                                  <i class="fa fa-pencil"></i> Edit
                            </a>
                          </td>
                          <td nowrap="nowrap">{{$union->project}}</td>
                          <td >{{$union->distname}}</td>
                          <td >{{$union->upname}}</td>
                          <td nowrap="nowrap">{{$union->unname}}</td>
                          <td nowrap="nowrap">{{$union->moudate}}</td>
                          <td >{{$union->remarks}}</td>


                        </tr>
                      @endforeach
                    </tbody>
                </table>

            </div>
        </div>
    </div>
  @endif

@endsection