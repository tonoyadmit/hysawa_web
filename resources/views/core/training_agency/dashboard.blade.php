@extends('layouts.appinside')
@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('training-agency.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span></span>
    </li>
  </ul>
</div>

<h1 class="page-title"> Training Summary: <small> Number of Training Courses and Participants by Region</small> </h1>

@if(count($regionDatatotals) > 0)
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i>List
      </div>
      <div class="tools">
        <a href="javascript:;" class="collapse"> </a>
      </div>
    </div>

    <div class="portlet-body util-btn-margin-bottom-5">
      <div class="table-responsive">
        <table class="table table-bordered table-hover data-table" id="example0">
          <thead class="flip-content">
            <th>Region</th>
            <th>No. of Courses</th>
            <th>Total nos.of Particpants</th>
            <th>Participant List entry</th>
          </thead>
          <tbody>
            @foreach($regionDatatotals as $regionDatatotal)
            <tr>
              <td>{{$regionDatatotal["region_name"] }}</td>
              <td>{{$regionDatatotal["CountOfTrgCode"]}}</td>
              <td>{{$regionDatatotal["SumOfTrgParticipantNo"]}}</td>
              <td>{{$regionDatatotal["rpcount"]}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>

  </div>
</div>
@endif


@if(count($agentDatatotals) > 0)
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i>Number of Training Courses and Participants by HYSAWA/Training agenc
      </div>
      <div class="tools">
        <a href="javascript:;" class="collapse"> </a>
      </div>
    </div>

    <div class="portlet-body util-btn-margin-bottom-5">
      <div class="table-responsive">
        <table class="table table-bordered table-hover data-table" id="example0">
          <thead class="flip-content">
            <th>Agency</th>
            <th>No. of Courses</th>
            <th>Total nos.of Particpants</th>
            <th>Participant List entry</th>
          </thead>
          <tbody>
            @foreach($agentDatatotals as $agentDatatotal)
            <tr>
              <td>{{$agentDatatotal["Agency"] }}</td>
              <td>{{$agentDatatotal["tcount"]}}</td>
              <td>{{$agentDatatotal["tpcount"]}}</td>
              <td>{{$agentDatatotal["CountOfparticipant_id"]}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endif
@if(count($titleDatatotals) > 0)
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">

    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i>Number of Training Courses and Participants by Training Title
      </div>
      <div class="tools">
        <a href="javascript:;" class="collapse"> </a>
      </div>
    </div>

    <div class="portlet-body util-btn-margin-bottom-5">
      <h3>List</h3>
      <div class="table-responsive">
        <table class="table table-bordered table-hover data-table" id="example0">
          <thead class="flip-content">
            <th>TrgTitle</th>
            <th># Courses</th>
            <th>Total</th>
            <th>Chairman</th>
            <th>UP Secretray</th>
            <th>Female member</th>
            <th>Male member</th>
            <th>UP/PNGO staff</th>
            <th>SO STaff</th>
            <th>UP Accountant</th>
            <th>HYSAWA staff/Consultant</th>
            <th>PNGO representative</th>
          </thead>
          <tbody>
            @foreach($titleDatatotals as $titleDatatotal)
            <tr>
              <td>{{$titleDatatotal["TrgTitle"] }}</td>
              <td>{{$titleDatatotal["ttcount"]}}</td>
              <td>{{$titleDatatotal["CountOfparticipant_id"]}}</td>
              <td>{{$titleDatatotal["Chairman"]}}</td>
              <td>{{$titleDatatotal["Secretary"]}}</td>
              <td>{{$titleDatatotal["fem"]}}</td>
              <td>{{$titleDatatotal["Malemember"]}}</td>
              <td>{{$titleDatatotal["Communityorganizer"]}}</td>
              <td>{{$titleDatatotal["SO"]}}</td>
              <td>{{$titleDatatotal["Accountant"]}}</td>
              <td>{{$titleDatatotal["HYSAWA"]}}</td>
              <td>{{$titleDatatotal["PNGO"]}}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endif
@endsection
