<?php

use App\Model\WaterSummary;

?>
@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('training-agency.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Paricipants</span>
      </li>
    </ul>
  </div>

  <h1 class="page-title"> List <small> view</small> </h1>

    @if(count($participantslists) > 0)

        <div class="col-md-12">
            <div class="portlet light tasks-widget bordered">
              <div class="portlet-title">
                <div class="caption">
                  <i class="fa fa-gift"></i>List
                </div>
                <div class="action"></div>
                <div class="tools">
                  <a href="#" class="btn ">Download</a>
                  <a href="javascript:;" class="collapse"> </a>
                </div>
              </div>
                <div class="portlet-body util-btn-margin-bottom-5">
                    <div class="table-responsive">
                      <table class="table table-bordered table-hover" id="example0">
                          <thead class="flip-content">
                            <tr>
                            <th>Organized by/Agency</th>
                            <th>Training Title</th>
                            <th>Venue</th>
                            <th>Partcipant name</th>
                            <th>Designation</th>
                            <th>District</th>
                            <th>Upazila</th>
                            <th>Union</th>
                            <th>Mobile no</th>
                            <th>Email</th>
                            </tr>
                          </thead>
                          <tbody>
                          @foreach($participantslists as $participantslist)
                            <tr>
                              <td>{{ $participantslist->col3 }}</td>
                              <td>{{ $participantslist->col1 }}</td>
                              <td>{{ $participantslist->col2 }}</td>
                              <td>{{ $participantslist->partcipant_name }}</td>
                              <td>{{ $participantslist->col4 }}</td>
                              <td>{{ $participantslist->distname }}</td>
                              <td>{{ $participantslist->upname }}</td>
                              <td>{{ $participantslist->unname }}</td>
                              <td>{{ $participantslist->mobile_no }}</td>
                              <td>{{ $participantslist->email_address }}</td>
                            </tr>
                          @endforeach
                          </tbody>
                      </table>
                    </div>
                </div>
            </div>
        </div>
    @else
      <div class="col-md-12">
        <div class="portlet light tasks-widget bordered">
           <p>
              No Data Found
           </p>
        </div>
      </div>
    @endif

@endsection
