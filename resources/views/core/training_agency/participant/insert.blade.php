@extends('layouts.appinside')
@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('training-agency.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="{{ route('training-agency.trainingParticipant.index') }}">Insert new Participant</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Insert</span>
    </li>
  </ul>
</div>
<div class="row">
    <div class="col-md-12">






        <div class="portlet light tasks-widget bordered">
                <div class="portlet-body util-btn-margin-bottom-5">

                    <div class="table-responsive">


                      <table class="table table-bordered table-hover data-table" id="example0">
                          <thead class="flip-content">
                            <th>Action</th>
                            <th>Partcipant name</th>
                            <th>Designation</th>
                            <th>District</th>
                            <th>Upazila</th>
                            <th>Union</th>
                            <th>Mobile no</th>
                            <th>Email</th>
                            <th>Remarks</th>


                          </thead>
                          <tbody>
                              @foreach($participants as $participant)

                                  <tr>
                                      <td>
                                          <a class="label label-success" href="{{ route("training-agency.trainingParticipant.edit",$participant->participant_id)}}">
                                              <i class="fa fa-pencil"></i> Update
                                          </a>
                                      </td>
                                      <td>{{$participant->partcipant_name}}</td>
                                        <td>{{$participant->desg}}</td>
                                      <td>{{$participant->distname}}</td>
                                      <td>{{$participant->upname}}</td>
                                      <td>{{$participant->unname}}</td>

                                      <td>{{$participant->mobile_no}}</td>
                                      <td>{{$participant->email_address}}</td>
                                      <td>{{$participant->remarks}}</td>



                                  </tr>
                              @endforeach
                          </tbody>
                      </table>



                    </div>

                </div>
            </div>
    </div>

</div>
<h1 class="page-title"> Insert new Participant
  <small>create new</small>
</h1>

{!! Form::open(array('route' => 'training-agency.trainingParticipant.store', 'method' => 'post')) !!}

<div class="row">

  @include('partials.errors')

  <div class="col-md-6">

   <div class="form-group">
     <label class="control-label col-md-12 np-lr">Name of participant:</label>
     <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
     <input type="hidden" name="TrgCode" id="TrgCode" value="{{ $TrgCode }}" />
     {!! Form::text('partcipant_name', null, ['class' => 'form-control', 'placeholder' => 'Name of participant', 'required' => 'required']) !!}
   </div>

   <div class="form-group">
     <label class="control-label col-md-12 np-lr">District</label>
     {!! Form::select('district_id', array(''=>'Select District')+$district, null, ['class' => 'form-control','id' => 'district_id']) !!}
   </div>
   <div class="form-group">
     <label class="control-label col-md-12 np-lr">Upzilla</label>
     {!! Form::select('upazila_id', array(''=>'Select Upzilla'), null, ['class' => 'form-control','id'=>'upazila_id']) !!}
   </div>
   <div class="form-group">
     <label class="control-label col-md-12 np-lr">Union</label>
     {!! Form::select('union_id', array(''=>'Select Union'), null, ['class' => 'form-control','id' =>'union_id']) !!}
   </div>
   <div class="form-group">
     <label class="control-label col-md-12 np-lr">Designation</label>
     {!! Form::select('designation', array(''=>'Select Ward')+$des, null, ['class' => 'form-control',]) !!}
   </div>

   <div class="form-group">
    <label class="control-label">Mobile no</label>
    {!! Form::text('mobile_no', null, ['class' => 'form-control', 'placeholder' => 'Mobile no', ]) !!}
  </div>

  <div class="form-group">
    <label class="control-label">Email:</label>
    {!! Form::text('email_address', null, ['class' => 'form-control', 'placeholder' => 'Email', ]) !!}
  </div>
  <div class="form-group">
   <label class="control-label">Remarks</label>
   {!! Form::text('remarks', null, ['class' => 'form-control', 'placeholder' => 'Remarks', ]) !!}
 </div>



</div>



</div>

&nbsp;
<div class="row padding-top-10">
  <a href="javascript:history.back()" class="btn default"> Cancel </a>
  {!! Form::submit('Save', ['class' => 'btn green pull-right']) !!}
</div>

{!! Form::close() !!}


</div>

<script type="text/javascript">
  $(document ).ready(function() {
    highlight_nav('warehouse-manage', 'shelfs');
  });
</script>


<script type="text/javascript" language="javascript" >

   $(document).ready(function () {
        $('#district_id').on('change', function() {
                     var disid=$(this).val();
                      var csrftoken = $("#csrf-token").val();
                     if(disid==''){
                             $('#upazila_id').attr('disabled', 'disabled');
                     }else{
                             $.getJSON('{{ route('training-agency.getUpzilla') }}?disid='+disid+'&_token='+csrftoken, function (data) {
                                 $('select[name="upazila_id"]').empty();
                                $.each(data, function(key, value) {
                                $('select[name="upazila_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                                });
                             });
                     }
        });

        $('#upazila_id').on('change', function() {
                     var upid=$(this).val();
                     var disid=$("#district_id").val();
                      var csrftoken = $("#csrf-token").val();
                     if(upid==''){
                             $('#union_id').attr('disabled', 'disabled');
                     }else{
                             $.getJSON('{{ route('training-agency.getUnion') }}?disid='+disid+'&_token='+csrftoken+'&upid='+upid, function (data) {
                                 $('select[name="union_id"]').empty();
                                $.each(data, function(key, value) {
                                $('select[name="union_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                                });
                             });
                     }
        });
   });

</script>
@endsection
