@extends('layouts.appinside')
@section('content')
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('training-agency.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>List of Training:</span>
    </li>
  </ul>
</div>
<h1 class="page-title"> List of Training: <small> view</small> </h1>

@if(count($traininglists) > 0)
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i>List
      </div>
      <div class="tools">
        <a href="#" class="btn ">Download</a>
        <a href="javascript:;" class="collapse"> </a>
      </div>
    </div>

    <div class="portlet-body util-btn-margin-bottom-5">

      <a href="{{route('training-agency.trainingAgency.create')}}" class="btn btn-success">Add New</a>

      <div class="table-responsive">
        <table class="table table-bordered table-hover data-table" id="example0">
          <thead class="flip-content">
            <th>Action</th>
            <th>Training Agency/Organized by</th>
            <th>Training Title</th>
            <th>Venue</th>
            <th>Date From</th>
            <th>Date To</th>
            <th>Participant type</th>
            <th>Number of participant</th>
            <th>Batch no</th>
            <th>Status</th>
         

          </thead>
          <tbody>
            @foreach($traininglists as $traininglist)
            <tr>
              <td>
                <a class="label label-success" href="trainingAgency/{{ $traininglist->TrgCode }}/edit">
                  <i class="fa fa-pencil"></i> Update
                </a>
              </td>
              <td>{{$traininglist->col3}}</td>
              <td><a href="{{ route('training-agency.trainingParticipant.create').'?TrgCode='.$traininglist->TrgCode }}">{{$traininglist->col1}}</a></td>
              <td>{{$traininglist->col2}}</td>
              <td>{{$traininglist->TrgFrom}}</td>
              <td>{{$traininglist->TrgTo}}</td>
              <td>{{$traininglist->TrgParticipantsType}}</td>
              <td>{{$traininglist->TrgParticipantNo}}</td>
              <td>{{$traininglist->TrgBatchNo}}</td>
              <td>{{$traininglist->TrgStatus}}</td>
       

            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@else
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <p>
      No Data Found
    </p>
  </div>
</div>
@endif
@endsection
