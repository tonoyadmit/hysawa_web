@extends('layouts.appinside')
@section('content')

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('training-agency.dashboard') }}">Inser New Training:</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('training-agency.trainingAgency.index') }}">Inser New Training:</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Insert</span>
        </li>
    </ul>
</div>
<h1 class="page-title"> Inser New Training:
    <small>create new</small>
</h1>

{!! Form::open(array('route' => 'training-agency.trainingAgency.store', 'method' => 'post')) !!}

<div class="row">

    @include('partials.errors')



    <div class="col-md-6">

        <div class="form-group">
            <label class="control-label col-md-12 np-lr">Training Title:</label>
            {!! Form::select('TrgTitle', array(''=>'Select Training Title') + $trainingTitle, NULL, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>

        <div class="form-group">
            <label class="control-label col-md-12 np-lr">Venue:</label>
            {!! Form::select('TrgVenue', array(''=>'Select Venue') + $venueList,NULL, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>

        <div class="form-group">
            <label class="control-label">Date From: (YYYY-MM-DD)</label>
            {!! Form::text('TrgFrom', null, ['class' => 'form-control', 'placeholder' => 'Date From: (YYYY-MM-DD)', 'required' => 'required']) !!}
        </div>

        <div class="form-group">
            <label class="control-label">Date To: (YYYY-MM-DD)</label>
            {!! Form::text('TrgTo', null, ['class' => 'form-control', 'placeholder' => 'Date To: (YYYY-MM-DD)', 'required' => 'required']) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Batch no:</label>
            {!! Form::text('TrgBatchNo', null, ['class' => 'form-control', 'placeholder' => 'Batch no:', 'required' => 'required']) !!}
        </div>

        <div class="form-group">
            <label class="control-label">Participants types:</label>
            {!! Form::text('TrgParticipantsType', null, ['class' => 'form-control', 'placeholder' => 'Participants types:', 'required' => 'required']) !!}
        </div>

        <div class="form-group">
            <label class="control-label">Number of participants:</label>
            {!! Form::number('TrgParticipantNo', null, ['class' => 'form-control', 'placeholder' => 'Number of participants', 'required' => 'required']) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Training Status:</label>

            {!! Form::select('TrgStatus', array('Completed'=>'Completed','Cancelled'=>'Cancelled','Planned'=>'Planned'), null, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Name of Facilitators:</label>
            {!! Form::text('TrgFacilitators', null, ['class' => 'form-control', 'placeholder' => 'Name of Facilitators', 'required' => 'required']) !!}
        </div>

        <div class="form-group">
            <label class="control-label">Oraganized by / Name of Training Agency:</label>
            {!! Form::select('Agency', array(''=>'Select Ward') + $agency, null, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
    </div>

</div>

&nbsp;
<div class="row padding-top-10">
    <a href="javascript:history.back()" class="btn default"> Cancel </a>
    {!! Form::submit('Save', ['class' => 'btn green pull-right']) !!}
</div>

{!! Form::close() !!}


</div>

<script type="text/javascript">
    $(document).ready(function () {
        highlight_nav('warehouse-manage', 'shelfs');
    });
</script>
@endsection
