@extends('layouts.appinside')
@section('content')
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ URL::to('up-admin.dashboard') }}">Dashboard</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Training</span>
    </li>
  </ul>
</div>
<h1 class="page-title"> Training
  <small> Participants list</small>
</h1>


<div class="col-md-12">
</div>


@if(count($paricipants))
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <div class="portlet-body util-btn-margin-bottom-5 table-responsive">

      <div class="table-responsive">

        <table class="table table-bordered table-condensed " id="example0">
          <thead class="flip-content">
            <th style="font-size: 9px;" nowrap="nowrap">Training Title</th>
            <th style="font-size: 9px;" nowrap="nowrap" >Venue</th>
            <th style="font-size: 9px;" nowrap="nowrap" >Date from</th>
            <th style="font-size: 9px;" nowrap="nowrap" >Date to</th>
            <th style="font-size: 9px;" nowrap="nowrap" >Partcipant name</th>
            <th style="font-size: 9px;" nowrap="nowrap" >Designation</th>
            <th style="font-size: 9px;" nowrap="nowrap" >District</th>
            <th style="font-size: 9px;" nowrap="nowrap" >Upazila</th>
            <th style="font-size: 9px;" nowrap="nowrap" >Union</th>
            <th style="font-size: 9px;" nowrap="nowrap" >Mobile no</th>
            <th style="font-size: 9px;" nowrap="nowrap" >Email</th>
          </thead>

          <tbody>
            @foreach($paricipants as $paricipant)
            <tr>
              <td style="font-size: 9px;" nowrap="nowrap" >{{ $paricipant->title }}</td>
              <td style="font-size: 9px;" nowrap="nowrap" >{{ $paricipant->VenueName }}</td>
              <td style="font-size: 9px;" nowrap="nowrap" >{{ $paricipant->TrgFrom }}</td>
              <td style="font-size: 9px;" nowrap="nowrap" >{{ $paricipant->TrgTo }}</td>
              <td style="font-size: 9px;" nowrap="nowrap" >{{ $paricipant->partcipant_name }}</td>
              <td style="font-size: 9px;" nowrap="nowrap" >{{ $paricipant->desg }}</td>
              <td style="font-size: 9px;" nowrap="nowrap" >{{ $paricipant->distname }}</td>
              <td style="font-size: 9px;" nowrap="nowrap" >{{ $paricipant->upname }}</td>
              <td style="font-size: 9px;" nowrap="nowrap" >{{ $paricipant->unname }}</td>
              <td style="font-size: 9px;" nowrap="nowrap" >{{ $paricipant->mobile_no }}</td>
              <th style="font-size: 9px;" nowrap="nowrap" >{{ $paricipant->email_address}}</th>
            </tr>
            @endforeach
          </tbody>
        </table>

        <div class="pagination pull-right">
        </div>
      </div>

    </div>
  </div>
</div>
@else
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <p>
      No Data Found
    </p>
  </div>
</div>
@endif


<style media="screen">
  .table-filtter .btn{ width: 100%;}
  .table-filtter {
    margin: 20px 0;
  }
</style>
<style>
  .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
    border: 1px solid;
  }
</style>
@endsection
