@extends('layouts.print.app')

@section('my_style')
<style>
  table{border-collapse: collapse;border-spacing: 0; border: 1px;}
  div{font-size: 80%;display: inline-block;}
  .headline th {padding: 5px 20px;}
</style>
@endsection

@section('content')

@if($_REQUEST['print'] == "school") <h3>School List</h3>
@else                               <h3>CDF List</h3>
@endif 

<table class="table table-bordered table-hover" border="1" width="100%">
    <thead class="flip-content">
        <th>CDF No/School No</th>
        <th>Word</th>
        <th>Village</th>
        <th>Description</th>

    </thead>
    <tbody>
        @foreach($cdfs as $cdf)
            <tr>
                <td>{{$cdf->cdf_no}}</td>
                <td>{{$cdf->word_no or ''}}</td>
                 <td>{{$cdf->village or ''}}</td>
                <td>@if($cdf->type == "cdf") CDF Name: {{$cdf->owner or ''}}
                @else School Title: {{$cdf->school_title or ''}}
                @endif </td>

                
            </tr>
        @endforeach
    </tbody>
</table>


@endsection