@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('up-admin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="#">Mobile App</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>CDF</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')


  <div class="col-md-12" style="margin-top: 10px;">
    <div class="portlet box blue">
      <div class="portlet-title">
          <div class="caption">
              CDF <small> list</small> Add New </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body form">
          <form action="{{route('up-admin.cdfs.store')}}" class="form-horizontal" method="POST" id="createForm">

              {{csrf_field()}}

              <div class="form-body">

                  <div class="row">

                      <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-6">CDF No / School Id</label>
                              <div class="col-md-6">
                                  <input class="form-control" type="text" name="cdf_no" required="required">
                              </div>
                          </div>
                      </div>

                      <div class="col-md-2">
                      <div class="form-group">
                          <label class="control-label col-md-4">Ward No</label>
                          <div class="col-md-8">
                            <select class="form-control" name="word_no" required="required">
                                <option value="">Choose</option>
                              @foreach(range(1, 10) as $index )
                                <option value="{{$index}}">{{$index}}</option>
                              @endforeach
                            </select>
                          </div>
                      </div>
                    </div>


                      <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-3">Village</label>
                              <div class="col-md-9">
                                  <input class="form-control" type="text" name="village" required="required">
                              </div>
                          </div>
                      </div>


                  </div>

                  <div class="row">
                    <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-3">Type</label>
                              <div class="col-md-9">
                                <select name="type" class="form-control" id="typeId">
                                  <option value="school" selected="selected">School </option>
                                  <option value="cdf" >CDF </option>
                                </select>
                              </div>
                          </div>
                      </div>

                    <div class="col-md-4" id="schoolTitleDiv">
                        <div class="form-group">
                            <label class="control-label col-md-3">School Title</label>
                            <div class="col-md-9">
                              <input type="text" name="school_title" placeholder="School Title" class="form-control" />
                            </div>
                        </div>
                    </div>



                    <div class="col-md-4" id="ownerDiv" style="display: none;">
                        <div class="form-group">
                            <label class="control-label col-md-3">CDF Name</label>
                            <div class="col-md-9">
                              <input type="text" name="owner" placeholder="CDF Name" class="form-control" id="owner"/>
                            </div>
                        </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-3">
                            <input type="submit" class="btn green" value="Add New" />
                        </div>
                    </div>
                  </div>
              </div>
          </form>

      </div>
    </div>
  </div>



    @if(count($cdfs) > 0)

        <div class="col-md-12">
            <div class="portlet light tasks-widget bordered">
                <div class="portlet-body util-btn-margin-bottom-5">
                  
                  <a href="{{route('up-admin.cdfs.index', ['print' => 'school'])}}" class="btn btn-sm btn-info pull-right">School</a>
                  <a href="{{route('up-admin.cdfs.index', ['print'=> 'cdf'])}}" class="btn btn-sm btn-success pull-right">CDF</a>

                    <table class="table table-bordered table-hover" id="cdfDataTable">
                        <thead class="flip-content">
                            <th width="10%">Action</th>
                            <th>CDF No</th>
                            <th>Type</th>
                            <th>Word</th>
                            <th>Village</th>
                            <th>Description</th>

                        </thead>
                        <tbody>
                            @foreach($cdfs as $cdf)
                                <tr>
                                    <td>
                                      <a class="label label-success" href="{{route('up-admin.cdfs.edit', $cdf->id)}}">
                                            <i class="fa fa-pencil"></i> Edit
                                      </a>
                                    </td>
                                    <td>{{$cdf->cdf_no}}</td>
                                    <td>@if($cdf->type == "school") School @else CDF No @endif </td>
                                    <td>{{$cdf->word_no or ''}}</td>
                                    <td>{{$cdf->village or ''}}</td>
                                    <td>@if($cdf->type == "cdf") CDF Name: {{$cdf->owner or ''}}
                                    @else School Title: {{$cdf->school_title or ''}}
                                    @endif </td>

                                    
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="pagination pull-right">
                    {{$cdfs->links()}}
                    </div>
                </div>
            </div>
        </div>
    @endif


@endsection


@section('my_js')

<script type="text/javascript">

  $(document).ready(function () {

      var type = "school";

      $("#typeId").on('change', function(){
        type = $(this).val();

        console.log(type);

        if(type == "school" )
        {

          $("#ownerDiv").hide();
          $("#schoolTitleDiv").show();
        }else
        {
          $("#ownerDiv").show();
          $("#schoolTitleDiv").hide();
        }
      });
  });
</script>

@endsection
