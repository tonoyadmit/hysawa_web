@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('up-admin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="{{route('up-admin.cdfs.index')}}">CDF</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Edit</span>
      </li>
    </ul>
  </div>

  <h1 class="page-title"> CDF <small> Update</small> </h1>

    <div class="col-md-12">
      <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>Edit CDF</div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>

        <div class="portlet-body form">
            <form action="{{route('up-admin.cdfs.update', $cdf->id)}}" class="form-horizontal" method="POST" id="updateForm">

               {{csrf_field()}}
                <div class="form-body">
                  <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-6">CDF / School </label>
                            <div class="col-md-6">
                                <input class="form-control" type="text" name="cdf_no" required="required" value="{{$cdf->cdf_no}}" readonly="readonly"
                                >
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Village</label>
                            <div class="col-md-9">
                                <input class="form-control" type="text" name="village" required="required" value="{{$cdf->village}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                      <div class="form-group">
                          <label class="control-label col-md-4">Ward No</label>
                          <div class="col-md-8">
                            <select class="form-control" name="word_no" id="word_no">
                              <option value="">Choose</option>
                              @foreach(range(1, 10) as $index )
                                <option value="{{$index}}" @if($cdf->word_no == $index) selected="selected" @endif >{{$index}}</option>
                              @endforeach
                            </select>
                          </div>
                      </div>
                    </div>


                  </div>

                  <div class="row">

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Type</label>
                            <div class="col-md-9">
                              <select name="type" class="form-control" id="typeId">
                                <option value="school" @if($cdf->type == "school") selected="selected" @endif >School </option>
                                <option value="cdf" @if($cdf->type == "cdf") selected="selected" @endif>CDF </option>
                              </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4" id="schoolTitleDiv" @if($cdf->type == "cdf") style="display: none;" @endif>
                        <div class="form-group">
                            <label class="control-label col-md-3">School Title</label>
                            <div class="col-md-9">
                              <input type="text" name="school_title" placeholder="School Title" class="form-control" value="{{$cdf->school_title}}" />
                            </div>
                        </div>
                    </div>



                    <div class="col-md-4" id="ownerDiv" @if($cdf->type == "school") style="display: none;" @endif >
                        <div class="form-group">
                            <label class="control-label col-md-3">CDF Name</label>
                            <div class="col-md-9">
                              <input type="text" name="owner" placeholder="CDF Name" class="form-control" id="owner" value="{{$cdf->owner}}" />
                            </div>
                        </div>
                    </div>

                  </div>

                  <div class="row">
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-3">
                            <input type="submit" class="btn green" value="Update " />
                        </div>
                    </div>
                  </div>
                </div>
            </form>

        </div>
      </div>
    </div>
@endsection

@section('my_js')
<script type="text/javascript">

  $(document).ready(function () {

      var type = "school";

      $("#typeId").on('change', function(){
        type = $(this).val();

        console.log(type);

        if(type == "school" )
        {

          $("#ownerDiv").hide();
          $("#schoolTitleDiv").show();
        }else
        {
          $("#ownerDiv").show();
          $("#schoolTitleDiv").hide();
        }
      });
    });
    </script>
@endsection