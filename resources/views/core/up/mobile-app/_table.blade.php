@if(count($datas) > 0)
  <div class="col-md-12">
    <div class="portlet light tasks-widget bordered">

      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i>Data
          </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body util-btn-margin-bottom-5">
        <div class="table-responsive">

          <table class="table table-bordered table-hover data-table" id="example0">
            <thead class="flip-content">
              <th style="font-size: 10px;">Action</th>
              <th style="font-size: 10px;">Type</th>
              <th style="font-size: 10px;">User</th>
              <th style="font-size: 10px;">Project</th>
              <th style="font-size: 10px;">region</th>
              <th style="font-size: 10px;">District</th>
              <th style="font-size: 10px;">Upazila</th>
              <th style="font-size: 10px;">Union</th>
              <th style="font-size: 10px;">Created At</th>

            </thead>

            <tbody>
              @foreach($datas as $data)
              <tr>
                <td><a class="label label-info" href="{{route('up-admin.mobile-app.show', $data->id)}}">
                    <i class="fa fa-eye"></i> </a></td>
                <td style="font-size: 10px;">{{$data->type }}</td>
                <td style="font-size: 10px;">{{$data->user->email }}</td>

                <td style="font-size: 10px;">{{$data->project->project }}</td>
                <td style="font-size: 10px;">{{$data->region->region_name }}</td>

                <td style="font-size: 10px;">{{$data->union->upazila->district->distname }}</td>
                <td style="font-size: 10px;">{{$data->union->upazila->upname }}</td>
                <td style="font-size: 10px;">{{$data->union->unname }}</td>
                <td style="font-size: 10px;">{{$data->created_at}}</td>

              </tr>
              @endforeach
            </tbody>
          </table>
            <div class="pagination pull-right">
               {!! $datas->appends($old)->links() !!}
            </div>
        </div>

      </div>
    </div>
  </div>
@elseif(!empty($old['query']))
<div class="col-md-12">
    <div class="portlet light tasks-widget bordered">
       <p>
          No Data Found
       </p>
    </div>
  </div>
@endif
