@extends('layouts.print.app')

@section('my_style')
<style>
  table{border-collapse: collapse;border-spacing: 0; border: 1px;}
  div{font-size: 80%;display: inline-block;}
  .headline th {padding: 5px 20px;}
</style>
@endsection

@section('content')
<h3>Mobile App Data Entry List</h3>
<h4>UserID: {{auth()->user()->email}}</h4>
<?php
    $union = App\Model\Union::with('upazila.district')->find(auth()->user()->unid);
?>
<h4>District: {{$union->upazila->district->distname or ''}} Upazila: {{$union->upazila->upname or ''}} Union: {{$union->unname or ''}}</h4>
<table class="table table-bordered table-hover" border="1" width="100%">
    <thead class="flip-content">
      <th width="15%">Inserted At</th>
      <th width="10%">Type</th>
      <th >Details</th>
    </thead>
    <tbody>
        @foreach($datas as $data)
            <tr>
                <td >{{$data->inserted_at }}</td>
                <td >{{$data->type }}</td>
                <td>
                <?php
                  $index = 1;
                  $id = [];
                ?>
                @foreach($data->items as $item)

                  <?php
                    if(!isset($item->question)) continue;
                    $qid = $item->question->id;

                    if( isset( $id[$qid])){
                      $id[$item->question->id]['value'] = $id[$item->question->id]['value'].','.$item->value;
                    }else{
                      $id[$item->question->id] = ['title' => $item->question->title, 'value' => $item->value];
                    }
                  ?>
                @endforeach

                @foreach($id as $i)
                  {{$index++}}. Question: {{$i['title'] or ''}}&nbsp;&nbsp;&nbsp;Answer: {{$i['value'] or ''}}&nbsp;&nbsp;&nbsp;
                @endforeach
                <?php
                  unset($id);
                ?>
                </tr>
            </tr>
        @endforeach
    </tbody>
</table>


@endsection