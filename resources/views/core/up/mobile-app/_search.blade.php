<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Mobile-App : <small> Data-List</small> </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body form">
        <form action="{{route('up-admin.mobile-app.index')}}" class="form-horizontal" method="GET" id="searchForm">



            <input type="hidden" name="query" value="{{time()}}" />

            <div class="form-body" style="margin-bottom: 0px; padding-bottom: 1px;">

                <div class="row">

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-6">Starting Date:</label>
                            <div class="col-md-6">

                                <input
                                    class="form-control input-sm date-picker"
                                    type="text"
                                    name="starting_date"
                                    data-date-format="yyyy-mm-dd"

                                @if(!empty( $old['starting_date']))
                                    value="{{date('Y-m-d', strtotime($old['starting_date']))}}"
                                @endif >
                                <p style="margin-bottom: 0px">YYYY-MM-DD</p>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-6">Ending Date</label>
                            <div class="col-md-6">

                                <input
                                    class="form-control input-sm date-picker"
                                    type="text"
                                    name="ending_date"
                                    data-date-format="yyyy-mm-dd"

                                @if(!empty( $old['ending_date']))
                                    value="{{date('Y-m-d', strtotime($old['ending_date']))}}"
                                @endif >
                                 <p style="margin-bottom: 0px">YYYY-MM-DD</p>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Type</label>
                            <div class="col-md-9">

                                <select name="type" class="form-control input-sm" >
                                    <?php
                                        $data = App\Model\MobAppDataList::groupBy('type')->get();
                                    ?>
                                    <option value="">Select Type</option>

                                    @foreach($data as $type)
                                    <option
                                    value="{{$type->type}}"
                                    @if(
                                        !empty($old['type']) &&
                                        $old['type'] != "" &&
                                        $type->type == $old['type']
                                    )
                                        selected="selected"
                                    @endif
                                    >{{$type->type}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <input type="submit" class="btn green" value="Search" />
                        <a href="{{route('up-admin.mobile-app.index')}}" class="btn default">Clear</a>
                    @if(isset($_REQUEST['query']) && $_REQUEST['query'] != "")
                        <a href="{{route('up-admin.mobile-app.print', $_REQUEST)}}" class="btn btn-info btn-sm pull-right">Print</a>
                     @endif
                    </div>

                </div>

            </div>
        </form>

         @if(isset($_REQUEST['query']) && $_REQUEST['query'] != "")

          <div class="row">
            <div class="col-md-12">

              <p>Query:
                <b>Starting Date:</b> {{$_REQUEST['starting_date']}}|
                <b>Ending Date:</b> {{$_REQUEST['ending_date']}}
                <b>Type:</b> {{$_REQUEST['type'] or ''}}
              </p>

            </div>
          </div>
          @endif


    </div>
</div>