@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('up-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Document/Checklist</span>
    </li>
  </ul>
</div>

<h1 class="page-title"> Document/Checklist </h1>


<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Document/Checklist </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body">
    <table class="table table-bordered table-hover table-condensed table-stripped">
      <thead>
      <tr><th>&nbsp;</th><th>Name of Document/Checklist</th><th>&nbsp;</th></tr>
      </thead>
      <tbody>

        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td class="size12"><strong>Manuals</strong></td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>Project implementation menual</td>
          <td align="left"><a href="{{asset('downloads/Project implementation manual (PIM).pdf')}}" download>Download</a></td>
        </tr>
    
      <tr>
          <td>&nbsp;</td>
          <td class="size12"><strong>Hardware Checklist</strong></td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>Hardware payment checklist</td>
          <td align="left"><a href="{{asset('downloads/UP Fund disbursement_SubProject_TW.pdf')}}" download>Download</a></td>
        </tr>


        <tr>
          <td>&nbsp;</td>
          <td><strong>Direct and Indirect Taxes as proposed/made after Finance Bill 2018-2019</strong></td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>Changes in indirect tax after Finance Bill 2018-2019</td>
          <td align="left"><a href="{{asset('downloads/forup/indirect_tax.doc')}}" download>Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>Changes in Income Tax by Finance Bill 2018-2019</td>
          <td align="left"><a href="{{asset('downloads/forup/income_tax.doc')}}" download>Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td align="left">&nbsp;</td>
        </tr>


      </tbody>
    </table>
    </div>
</div>



@endsection
