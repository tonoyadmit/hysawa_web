<div class="col-md-12" style="margin-top: 10px;">
    <div class="portlet box blue">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i>Filter Result </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body">
          <form action="{{route('up-admin.finance.report.index')}}" class="form-horizontal" method="GET" id="createForm">

              <input type="hidden" name="query" value="{{time()}}" />

              <div class="form-body">
                  <div class="row">
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-4">Starting Date: <br/>YYYY-MM-DD</label>
                              <div class="col-md-8">
                                  <input
                                    type="text"
                                    name="starting_date"
                                    class="form-control date-picker"
                                    data-date-format = "yyyy-mm-dd"


                                    @if(count($old) && $old['starting_date'] != "")
                                      value="{{$old['starting_date']}}"
                                    @endif
                                  />
                              </div>
                          </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-4">Ending Date: <br/>YYYY-MM-DD</label>
                              <div class="col-md-8">
                                  <input
                                  type="text"
                                  name="ending_date"
                                  class="form-control date-picker"
                                  data-date-format = "yyyy-mm-dd"

                                    @if(count($old) && $old['ending_date'] != "")
                                      value="{{$old['ending_date']}}"
                                    @endif
                                  />
                              </div>
                          </div>
                      </div>
                  </div>


                  <div class="row">
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-12">
                            <input type="submit" class="btn green" value="Get Data" />
                            <a href="{{route('up-admin.finance.report.index')}}" class="btn btn-info">Clear</a>
                            @if(isset($_REQUEST['query']) && $_REQUEST['query'] != "")
                              <a href="{{route('up-admin.finance.report.print', $_REQUEST)}}" class="btn btn-success">Print</a>
                            @endif
                        </div>
                    </div>
                  </div>
              </div>
          </form>


          @if( isset($_REQUEST['query']) && $_REQUEST['query'] != "")


            <p>Query: <b>Starting Date: </b> {{$_REQUEST['starting_date']}}| <b>Ending Date: </b> {{$_REQUEST['ending_date']}}</p>

          @endif
      </div>
    </div>
  </div>