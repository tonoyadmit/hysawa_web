@extends('layouts.appinside')

@section('content')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ route('up-admin.dashboard') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Finance</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="#">Report </a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Union Report</span>
            </li>
        </ul>
    </div>

    @include('partials.errors')

    @include('core.up.finance.report._search')

    @if(isset($_REQUEST['starting_date']) && $_REQUEST['starting_date'] != "" && $_REQUEST['ending_date'] && $_REQUEST['ending_date'] != "")


            <div class="portlet light tasks-widget bordered">
                <div class="portlet-body util-btn-margin-bottom-5">
                    <div class="table-responsive">
                        <style media="screen">
                            .table-filtter .btn{ width: 100%;}
                            .table-filtter {
                                margin: 20px 0;
                            }
                            .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
                                border: 1px solid #000;
                            }
                        </style>



                        <table class="table table-bordered table-hover" id="example0">
                            <thead class="flip-content" style="background-color: #32c5d2;">
                                <th>Description</th>
                                <th>&nbsp;</th>
                                <th>Approved Budget (A)</th>
                                <th>Current Income (B)</th>
                                <th>Current Expenses (C)</th>
                                <th>Cumulative Income (D)</th>
                                <th>Cumulative Expenses (E)</th>
                                <th>Budget Balance (F=A-E)</th>
                                <th>Demand</th>
                            </thead>
                            <tbody>

                                <tr>
                                    <td style="text-align:right">Opening Balance</td>
                                    <td style="text-align:right">{{ sprintf("%.1lf", $data->total) }}</td>
                                    <td colspan="8">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="text-align:right">Cash in Hand</td>
                                    <td style="text-align:right">{{ sprintf("%.1lf", $data->opening_cash) }}</td>
                                    <td colspan="8" >&nbsp;</td>
                                </tr>
                                <tr>
                                    <td style="text-align:right">Cash at Bank</td>
                                    <td style="text-align:right">{{ sprintf("%.1lf", $data->opening_bank) }}</td>
                                    <td colspan="8" >&nbsp;</td>
                                </tr>


                                <?php
                                $gbudget=0;
                                $gcurrentIncome=0;
                                $gcurrentExpenditure=0;
                                $gcomulativeIncome=0;
                                $gcomulativeBudget=0;
                                $gcomulativeExpenditure=0;
                                $gdemand=0;
                                ?>
                                @foreach($d as $list)
                                <?php
                                    $budget=0;
                                    $currentIncome=0;
                                    $currentExpenditure=0;
                                    $comulativeIncome=0;
                                    $comulativeBudget=0;
                                    $comulativeExpenditure=0;
                                    $demand=0;
                                ?>
                                <tr>
                                    <td colspan="9" style="text-align:let;font-weight: bold;font-size: 20px;color:#C03" >{{ $list["headname"]}}</td>
                                </tr>
                                @if (count($list["subhead"]) > 0)
                                @foreach($list["subhead"] as $sublist)
                                <tr>
                                    <td colspan="9" style="text-align:let;font-weight: bold;font-size: 16px;color: #099;" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $sublist["sname"]}}</td>
                                </tr>
                                @if (count($sublist["sub_item"]) > 0)
                                @foreach($sublist["sub_item"] as $sub_item)
                                <tr>
                                    <?php
                                    $budget+=$cbudget=\App\FinanceData::getBudgetUN(
                                        $sub_item["id"],
                                        'ubid',
                                        auth()->user()->unid,
                                        auth()->user()->proj_id);
                                    $currentIncome+=$ccurrentIncome=\App\FinanceData::currentIncomeUN(
                                        $sub_item["id"],
                                        $old['starting_date'],
                                        $old['ending_date'],
                                        'unid',
                                        auth()->user()->unid,
                                        auth()->user()->proj_id,
                                        auth()->user()->region_id);
                                    $currentExpenditure+=$ccurrentExpenditure=\App\FinanceData::currentExpenditureUN(
                                        $sub_item["id"],
                                        $old['starting_date'],
                                        $old['ending_date'],
                                        'unid',
                                        auth()->user()->unid,
                                        auth()->user()->proj_id,
                                        auth()->user()->region_id);
                                    $comulativeIncome+=$ccomulativeIncome=\App\FinanceData::comulativeIncomeUN(
                                        $sub_item["id"],
                                        $old['ending_date'],
                                        'unid',
                                        auth()->user()->unid,
                                        auth()->user()->proj_id,
                                        auth()->user()->region_id);
                                    $comulativeExpenditure+=$ccomulativeExpenditure=\App\FinanceData::comulativeExpenditureUN(
                                        $sub_item["id"],
                                        $old['ending_date'],
                                        'unid',
                                        auth()->user()->unid,
                                        auth()->user()->proj_id,
                                        auth()->user()->region_id);

                                    $comulativeBudget+= $ccomulativeBudget=\App\FinanceData::comulativeBudget();

                                    $demand+=@$cdemand= \App\FinanceData::demandUN(
                                        $sub_item["id"],
                                        'unid',
                                        auth()->user()->unid,
                                        auth()->user()->proj_id,
                                        auth()->user()->region_id);
                                        ?>
                                        <td style="text-align:left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $sub_item["itemname"]}}</td>
                                        <td>&nbsp;</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$cbudget )}}</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$ccurrentIncome )}}</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$ccurrentExpenditure )}}</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$ccomulativeIncome )}}</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$ccomulativeExpenditure )}}</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$ccomulativeBudget) }}</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$cdemand) }}</td>
                                    </tr>
                                    @endforeach
                                    @endif
                                    @endforeach
                                    <tr>
                                        <td style="text-align:right">Sub Total</td>
                                        <td>&nbsp;</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$budget) }}</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$currentIncome) }}</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$currentExpenditure) }}</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$comulativeIncome) }}</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$comulativeExpenditure) }}</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$comulativeBudget) }}</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$demand) }}</td>
                                    </tr>
                                    <?php
                                    $gbudget+=$budget;
                                    $gcurrentIncome+=$currentIncome;
                                    $gcurrentExpenditure+=$currentExpenditure;
                                    $gcomulativeIncome+=$comulativeIncome;
                                    $gcomulativeExpenditure+=$comulativeExpenditure;
                                    $gcomulativeBudget+=$comulativeBudget;
                                    $gdemand+=$demand+=$cdemand;
                                    ?>
                                    @endif
                                    @endforeach
                                    <tr>
                                        <td style="text-align:right">Grand Total</td>
                                        <td>&nbsp;</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$gbudget )}}</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$gcurrentIncome) }}</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$gcurrentExpenditure )}}</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$gcomulativeIncome) }}</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf", $gcomulativeExpenditure) }}</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$gcomulativeBudget) }}</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$gdemand) }}</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:right">Clossing Balance</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$data2->total)  }}</td>
                                        <td colspan="8" >&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:right">Cash in Hand</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$data2->opening_cash) }}</td>
                                        <td colspan="8" >&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:right">Cash at Bank</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$data2->opening_bank) }}</td>
                                        <td colspan="8" >&nbsp;</td>
                                    </tr>
                                    <?php
                                    $bankStatement = \DB::table('bank')
                                    ->where('unid', auth()->user()->unid)
                                    ->where('date', $old['ending_date'])
                                    ->orderBy('date', 'DESC')
                                    ->get();
                                    $flag = false;
                                    if(count($bankStatement))
                                    {
                                        $flag = true;
                                        $bankStatementAmount = $bankStatement->first()->balance;
                                    }
                                    ?>
                                    @if($flag)
                                    <tr>
                                        <td style="text-align:right">Bank Statement</td>
                                        <td style="text-align:right">{{ sprintf("%.1lf",$bankStatementAmount) }}</td>
                                        <td colspan="8" >&nbsp;</td>
                                    </tr>
                                    @endif
                                </tbody>
                            </table>
                            <div class="pagination pull-right">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

    @else
        @if(!empty($old) && !empty($old['query']))
            <div class="col-md-12">
                <div class="portlet light tasks-widget bordered">
                    <p>
                        Please Select dates
                    </p>
                </div>
            </div>
        @endif
    @endif
@endsection

