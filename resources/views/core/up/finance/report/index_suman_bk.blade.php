@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('up-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="#">Finance</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="#">Report </a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Union Report</span>
    </li>
  </ul>
</div>

@include('partials.errors')

<h1 class="page-title"> Report <small> Union Report</small> </h1>

@include('core.up.finance.report._search')

@if($old['starting_date'] != "" && $old['ending_date'] != "")

<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <div class="portlet-body util-btn-margin-bottom-5">


      <div class="page-bar">
        <ul class="page-breadcrumb">
          <li>
            <a href="#">Starting Date: <span style="color:purple">{{$old['starting_date']}}</span></a>
            <i class="fa fa-circle"></i>
          </li>
          <li>
            <a href="#">Ending Date: <span style="color:purple">{{$old['ending_date']}}</span></a>
            <i class="fa fa-circle"></i>
          </li>
          <li>
            <span>Report</span>
          </li>
        </ul>
      </div>

      <div class="table-responsive">
        <style media="screen">
    .table-filtter .btn{ width: 100%;}
    .table-filtter {
        margin: 20px 0;
    }
    .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
        border: 1px solid #000;
    }
</style>


        <div class="col-md-12">
    <div class="portlet light tasks-widget bordered">
        <div class="portlet-body util-btn-margin-bottom-5">
            <table class="table table-bordered table-hover" id="example0">
                <thead class="flip-content">
                    <th>Description</th>
                    <th>Approved Budget(A)</th>
                    <th>Current Income(B)</th>
                    <th>Current Expenses(C)</th>
                    <th>Cumulative Income(D)</th>
                    <th>Cumulative Expenses(E</th>
                    <th>Budget Balance(F=A-E)</th>
                    <th>Demand</th>
                </thead>
                <tbody>
                    <tr>
                        <td>Opening Balance</td>
                        <td colspan="8" style="text-align:center">{{ $data->total }}</td>
                    </tr>
                    <tr>
                        <td>Cash in Hand</td>
                        <td colspan="8" style="text-align:center">{{ $data->opening_cash }}</td>
                    </tr>
                    <tr>
                        <td>Cash at Bank</td>
                        <td colspan="8" style="text-align:center">{{ $data->opening_bank }}</td>
                    </tr>
                    <?php   $gbudget=0;
                            $gcurrentIncome=0;
                            $gcurrentExpenditure=0;
                            $gcomulativeIncome=0;
                            $gcomulativeBudget=0;
                            $gcomulativeExpenditure=0;
                            $gdemand=0;
                        ?>
                    @foreach($d as $list)
                        <?php   $budget=0;
                            $currentIncome=0;
                            $currentExpenditure=0;
                            $comulativeIncome=0;
                            $comulativeBudget=0;
                            $comulativeExpenditure=0;
                            $demand=0;
                        ?>
                        <tr>
                            <td colspan="8" style="text-align:center">{{ $list["headname"]}}</td>
                        </tr>
                        @if (count($list["subhead"]) > 0)
                            @foreach($list["subhead"] as $sublist)
                            <tr>
                            <td colspan="8" style="text-align:center">{{ $sublist["sname"]}}</td>
                            </tr>
                                @if (count($sublist["sub_item"]) > 0)


                                    @foreach($sublist["sub_item"] as $sub_item)
                                     <tr>
                                        <?php
                                        $budget+=$cbudget=\App\FinanceData::getBudget($sub_item["id"]);
                                        $currentIncome+=$ccurrentIncome=\App\FinanceData::currentIncome($sub_item["id"],$old['starting_date'],$old['ending_date']);
                                        $currentExpenditure+=$ccurrentExpenditure=\App\FinanceData::currentExpenditure($sub_item["id"],$old['starting_date'],$old['ending_date']);
                                        $comulativeIncome+=$ccomulativeIncome=\App\FinanceData::comulativeIncome($sub_item["id"],$old['ending_date']);
                                        $comulativeExpenditure+=$ccomulativeExpenditure=\App\FinanceData::comulativeExpenditure($sub_item["id"],$old['ending_date']);
                                        $comulativeBudget+= $ccomulativeBudget=\App\FinanceData::comulativeBudget();
                                        $demand+=@$cdemand= \App\FinanceData::demand($sub_item["id"]);
                                        ?>
                                        <td style="text-align:center">{{ $sub_item["itemname"]}}</td>
                                        <td>{{ $cbudget }}</td>
                                        <td>{{ $ccurrentIncome }}</td>
                                        <td>{{ $ccurrentExpenditure }}</td>
                                        <td>{{ $ccomulativeIncome }}</td>
                                        <td>{{ $ccomulativeExpenditure }}</td>
                                        <td>{{ $ccomulativeBudget }}</td>
                                        <td>{{ $cdemand }}</td>

                                    </tr>
                                    @endforeach

                                @endif

                            @endforeach
                            <tr>
                                <td>Sub Total</td>
                                <td>{{ $budget }}</td>
                                <td>{{ $currentIncome }}</td>
                                <td>{{ $currentExpenditure }}</td>
                                <td>{{ $comulativeIncome }}</td>
                                <td>{{ $comulativeExpenditure }}</td>
                                <td>{{ $comulativeBudget }}</td>
                                <td>{{ $demand }}</td>
                            </tr>
                            <?php
                                        $gbudget+=$budget;
                                        $gcurrentIncome+=$currentIncome;
                                        $gcurrentExpenditure+=$currentExpenditure;
                                        $gcomulativeIncome+=$comulativeIncome;
                                        $gcomulativeExpenditure+=$comulativeExpenditure;
                                        $gcomulativeBudget+=$comulativeBudget;
                                        $gdemand+=$demand+=$cdemand;
                                        ?>
                        @endif
                    @endforeach
                    <tr>
                        <td>Grand Total</td>
                        <td>{{ $gbudget }}</td>
                        <td>{{ $gcurrentIncome }}</td>
                        <td>{{ $gcurrentExpenditure }}</td>
                        <td>{{ $gcomulativeIncome }}</td>
                        <td>{{ $gcomulativeExpenditure }}</td>
                        <td>{{ $gcomulativeBudget }}</td>
                        <td>{{ $gdemand }}</td>
                    </tr>
                    <tr>
                        <td>Clossing Balance</td>
                        <td colspan="8" style="text-align:center">{{ $data2->total  }}</td>
                    </tr>
                    <tr>
                        <td>Cash in Hand</td>
                        <td colspan="8" style="text-align:center">{{ $data2->opening_cash }}</td>
                    </tr>
                    <tr>
                        <td>Cash at Bank</td>
                        <td colspan="8" style="text-align:center">{{ $data2->opening_bank }}</td>
                    </tr>
                </tbody>
            </table>

            <div class="pagination pull-right">

            </div>

        </div>
    </div>
</div>


      </div>
    </div>
  </div>
</div>
@else
   @if(!empty($old) && !empty($old['query']))
      <div class="col-md-12">
        <div class="portlet light tasks-widget bordered">
          <p>

            Please Select dates

          </p>
        </div>
      </div>
  @endif
@endif
@endsection

