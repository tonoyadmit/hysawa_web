@extends('layouts.appinside')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('up-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>Finance</span>
        <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="{{ route('up-admin.finace_bankstatement.index') }}">Bank statements</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Update</span>
    </li>
  </ul>
</div>

<div class="portlet box green" style="margin-top: 10px;">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-share "></i>
            <span class="caption-subject bold ">Finance Bank statements <small>Update</small> </span>
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body form">


        {!! Form::open(array('route' => ['up-admin.finace_bankstatement.store'], 'method' => 'post','class' => 'form-horizontal')) !!}
            {{csrf_field()}}

            <div class="form-body">
                @include('partials.errors')


                <div class="form-group">
                    <label class="control-label col-md-3">Amount:</label>
                    <div class="col-md-4">
                        {!! Form::text('balance', null, ['class' => 'form-control', 'placeholder' => 'Balance', 'required' => 'required']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">Date: YYYY-MM-DD</label>
                    <div class="col-md-4">
                        {!! Form::text('date', null, ['class' => 'form-control date-picker', 'placeholder' => 'Date', 'required' => 'required', 'data-date-format' =>"yyyy-mm-dd"]) !!}
                    </div>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Remarks:</label>
                    <div class="col-md-4">
                        {!! Form::text('remarks', null, ['class' => 'form-control', 'placeholder' => 'Remarks']) !!}
                    </div>
                </div>

            </div>

            <div class="form-actions right1">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <a href="javascript:history.back()" class="btn default"> Cancel </a>
                        {!! Form::submit('Save', ['class' => 'btn green']) !!}
                    </div>
                </div>
            </div>

        {!! Form::close() !!}
    </div>
</div>

@endsection