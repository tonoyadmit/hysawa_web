@extends('layouts.appinside')

@section('content')
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('up-admin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
          <span>Finance</span>
          <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Bank Statement</span>
      </li>
    </ul>
  </div>

<div class="portlet light bordered" style="margin-top: 10px;">

    <div class="portlet-title">
        <div class="caption">
            <i class="icon-share font-dark"></i>
            <span class="caption-subject font-dark bold uppercase">Bank Statement</span>
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
    </div>

    <div class="portlet-body">

        <p>
            <a href="{{route('up-admin.finace_bankstatement.create')}}" class="btn btn-large btn-success"> Add new Entry</a>
        </p>

        @if(count($FinanceDemands) > 0)
          <table class="table table-bordered table-hover" id="example0">
              <thead class="flip-content">
                  <th>Action</th>
                  <th>Balance</th>
                  <th>Date</th>
                  <th>Remarks</th>

              </thead>
              <tbody>
                  @foreach($FinanceDemands as $FinanceData)
                  <tr>
                    <td>
                      <a class="label label-success" href="{{route('up-admin.finace_bankstatement.edit', $FinanceData->id)}}">
                        <i class="fa fa-pencil"></i> Update
                      </a>
                    </td>

                    <td class="text-right">{{ $FinanceData->balance }}</td>
                    <td>{{ $FinanceData->date }} </td>
                    <td >{{ $FinanceData->remarks }}</td>


                  </tr>
                  @endforeach
              </tbody>
          </table>
        @else
          <p>No Data Found</p>
        @endif

    </div>
</div>

@endsection
