@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('up-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>Finance</span>
        <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Disbursement</span>
    </li>
  </ul>
</div>

@include('partials.errors')

@include('core.up.finance.disbursement._search')

@if(count($results)>0)
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <div class="portlet-body util-btn-margin-bottom-5">


      <div class="table-responsive">
        <style>
          #example0 td {
            white-space: nowrap;
            font-size: 10px;
          }
          #example0 th {
            font-size: 10px;
          }
        </style>

        <table class="table table-bordered table-hover table-condensed" id="example0">
          <thead class="flip-content">
            <th nowrap="nowrap" >Head</th>
            <th nowrap="nowrap" >SubHead</th>
            <th nowrap="nowrap" >Item</th>
            <th nowrap="nowrap" >Disburse Amount</th>
            <th nowrap="nowrap" >Disburse Date</th>
            <th nowrap="nowrap" >Mode</th>
            <th nowrap="nowrap" >Remarks</th>
          </thead>
          <tbody>

            @foreach($results as $result)
            <tr>
              <td nowrap="nowrap">{{ $result->headname }}</td>
              <td nowrap="nowrap">{{ $result->sname }}</td>
              <td nowrap="nowrap">{{ $result->itemname }}</td>
              <td nowrap="nowrap" class="text-right">{{ $result->desbus }}</td>
              <td nowrap="nowrap">{{ $result->desdate }}</td>
              <td nowrap="nowrap">{{ $result->mode }}</td>
              <td nowrap="nowrap">{{ $result->remarks }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>

        {{ $results->appends($old)->links()}}
      </div>
    </div>
  </div>
</div>
@else
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <p>
      No Data Found
    </p>
  </div>
</div>
@endif
@endsection

@section('my_js')
<script type="text/javascript" language="javascript" >

    $(document).ready(function () {
        $('#head_id').on('change', function() {
            var head=$(this).val();
            var csrftoken = $("#csrf-token").val();
            if(head==''){
                $('#subhead_id').attr('disabled', 'disabled');
            }else{
                $.getJSON('{{ route('up-admin.income.getSubhead') }}?head='+head+'&_token='+csrftoken, function (data) {
                    $('select[name="subhead_id"]').empty();
                    $('select[name="subhead_id"]').append('<option value="">Select SubHead</option>');

                    $.each(data, function(key, value) {
                        $('select[name="subhead_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                    });
                });
            }
        });

        $('#subhead_id').on('change', function() {
            var subhead=$(this).val();
            var head=$("#head_id").val();
            var csrftoken = $("#csrf-token").val();
            if(head==''){
                $('#subhead_id').attr('disabled', 'disabled');
            }else{
                $.getJSON('{{ route('up-admin.income.getSubItem') }}?head='+head+'&_token='+csrftoken+'&subhead='+subhead, function (data) {
                    $('select[name="item_id"]').empty();
                    $('select[name="item_id"]').append('<option value="">Select Item</option>');
                    $.each(data, function(key, value) {
                        $('select[name="item_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                    });
                });
            }
        });
    });
</script>
@endsection