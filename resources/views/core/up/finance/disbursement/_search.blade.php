<div class="col-md-12" style="margin-top: 10px;">
    <div class="portlet box blue">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i>Filter Result </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body form">
          <form action="{{route('up-admin.balancedisbursement')}}" class="form-horizontal" method="GET" id="createForm">

              <input type="hidden" name="query" value="{{time()}}" />

              <div class="form-body">

                  <div class="row">
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">Head</label>
                              <div class="col-md-9">
                                  <select class="form-control" id="head_id" name="head_id">
                                    <option value="">Select Head</option>
                                    @foreach($heads as $head)
                                      <option value="{{$head->id}}"

                                      @if($old['head_id'] == $head->id)
                                        selected="selected"
                                      @endif

                                      >{{$head->headname}}</option>
                                    @endforeach
                                  </select>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">SubHead</label>
                              <div class="col-md-9">
                                  <select class="form-control" id="subhead_id" name="subhead_id">
                                    <option value="">Select Head First</option>
                                    @if( count($old) && $old['head_id'] != "" )
                                      @foreach($subheads as $subhead)
                                        <option value="{{$subhead->id}}"

                                        @if($old['subhead_id'] == $subhead->id)
                                          selected="selected"
                                        @endif

                                        >{{$subhead->sname}}</option>
                                      @endforeach
                                    @endif
                                  </select>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-3">Item</label>
                              <div class="col-md-9">
                                <select class="form-control" id="item_id" name="item_id" >
                                  <option value="">Select Head First</option>
                                  @if( count($old) && $old['subhead_id'] != "")
                                      @foreach($items as $item)
                                        <option value="{{$item->id}}"

                                        @if($old['item_id'] == $item->id)
                                          selected="selected"
                                        @endif

                                        >{{$item->itemname}}</option>
                                      @endforeach
                                    @endif
                                </select>
                              </div>
                          </div>
                      </div>
                  </div>

                  <div class="row">
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-4">Starting Date: YYYY-MM-DD</label>
                              <div class="col-md-8">
                                  <input type="text" name="starting_date" class="form-control date-picker input-sm" data-date-format ="yyyy-mm-dd"
                                    @if(count($old) && $old['starting_date'] != "")
                                      value="{{$old['starting_date']}}"
                                    @endif
                                  />
                              </div>
                          </div>
                      </div>
                      <div class="col-md-4">
                          <div class="form-group">
                              <label class="control-label col-md-4">Ending Date: YYYY-MM-DD</label>
                              <div class="col-md-8">
                                  <input type="text" name="ending_date" class="form-control date-picker input-sm" data-date-format ="yyyy-mm-dd"
                                    @if(count($old) && $old['ending_date'] != "")
                                      value="{{$old['ending_date']}}"
                                    @endif
                                  />
                              </div>
                          </div>
                      </div>
                  </div>


                  <div class="row">
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-6">
                            <input type="submit" class="btn green" value="Get Data" />
                            <a href="{{route('up-admin.balancedisbursement')}}" class="btn btn-info">Clear</a>
                        </div>
                    </div>
                  </div>
              </div>
          </form>

      </div>
    </div>
  </div>