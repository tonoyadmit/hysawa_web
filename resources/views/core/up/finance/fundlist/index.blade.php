@extends('layouts.appinside')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('up-admin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Finance</span>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Demand</span>
        </li>
    </ul>
</div>

<div class="portlet light bordered" style="margin-top: 10px;">

    <div class="portlet-title">
        <div class="caption">
            <i class="icon-share font-dark"></i>
            <span class="caption-subject font-dark bold uppercase">Fund Request</span>
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
    </div>

    <div class="portlet-body">

        <p>
            <a href="{{route('up-admin.fund_list.create')}}" class="btn btn-large btn-success"> Add new Fund Request</a>
          
           {{--  <a href="{{route('up-admin.finace_demand.print')}}" class="btn btn-large btn-success"> Print</a>  --}}

        </p>

        @if(count($FinanceDemands) > 0)

        <table class="table table-bordered table-hover" id="example0">

            <thead class="flip-content">
                <th>Action</th>
                <th>SL</th>
                <th>ID</th>
                <th>From Date</th>
                 <th>To Date</th>
       
            </thead>

            <tbody>
        
                @foreach($FinanceDemands as $FinanceData)
                <tr>
                    <td>
                        <a class="label label-success" href="{{route('up-admin.fund_list.edit', $FinanceData->id )}}">
                            <i class="fa fa-pencil"></i> Edit
                        </a>
         
                           <a class="label label-success" href="{{ route('up-admin.finace_demand.index',  ['fid' => $FinanceData->id,'fdate='.$FinanceData->fdate,'tdate='.$FinanceData->tdate] )}}">
                            <i class="fa fa-pencil"></i> Add Details
                        </a>


                    </td>
                    <td nowrap="nowrap">{{ $loop->iteration }}</td>
                      <td nowrap="nowrap">{{ $FinanceData->id}}</td>
                    <td nowrap="nowrap">{{ $FinanceData->fdate }}</td>
                     <td nowrap="nowrap">{{ $FinanceData->tdate }}</td>
                   

                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <p>No Data Found</p>
        @endif

    </div>
</div>

@endsection
