@extends('layouts.appinside')
@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('up-admin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('up-admin.report_period.index') }}">Set report Month:</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Insert</span>
        </li>
    </ul>
</div>
<h1 class="page-title"> Set report Month:
    <small>create new</small>
</h1>

{!! Form::open(array('route' => ['up-admin.setReportMonthStore', 1], 'method' => 'post', 'class' => 'form-horizontal')) !!}

    @include('partials.errors')
        <div class="form-group">
            <label class="control-label col-md-3">Select Month:</label>
            <div class="col-md-4">
                <?php
                    $months = array();
                    for ($i = -24; $i <= 12; $i++) {
                      array_push($months, date('Y F', strtotime("$i month", time())));
                    }
                   $i=0;
                ?>
                <select name="rep_id" class="form-control" required="required">
                    <option value="">Choose One</option>
                    @foreach($months as $month)
                    <?php
                  
                        $res = \DB::table('rep_period')->where('period', $month)->first();
                        @$id=$res->rep_id;
                      
                        $res2 = \DB::table('rep_data')->where('rep_id', $id)->where('unid',auth()->user()->unid)->count();
                        $res3 = \DB::table('rep_data')->where('rep_id', $id)->where('unid',auth()->user()->unid)->first();
                        //print_r($res->rep_id);exit;
                        
                        if($res2==1){
                            continue;
                        }
                    ?>
                    <option value="{{$id}}">{{$month}}</option>
                    @endforeach
                </select>
            </div>
        </div>

        <div class="form-actions">
            <div class="col-md-offset-2 col-md-4">
                <div class="row padding-top-10">
                    <a href="javascript:history.back()" class="btn default"> Cancel </a>
                    {!! Form::submit('Save', ['class' => 'btn green ']) !!}
                </div>
            </div>
        </div>

{!! Form::close() !!}

@endsection
