@extends('layouts.appinside')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('up-admin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('up-admin.report_period.index') }}">Monthly Report</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Insert</span>
        </li>
    </ul>
</div>

<h1 class="page-title"> Water Sanitation
    <small>create new</small>
</h1>

{!! Form::model($ReportData, array('route' => ['up-admin.entryFormStore', $ReportData->id], 'method' => 'post')) !!}

<div class="row">
    @include('partials.errors')
    <div class="col-md-12">
        <table class="table table-bordered" style="background:#F5F5F5">
            <thead>
                <tr>
                    <th>Code</th>
                    <th>5422</th>
                    <th>Baseline</th>
                    <th>Target</th>
                    <th>Progress untill last month</th>
                    <th>Progress this month</th>
                    <th>Total progress</th>
                </tr>
            </thead>
            <tbody>
                <tr valign="baseline">
                    <td nowrap="nowrap" align="right"><div align="center">02</div></td>
                    <td>Number of CDF:</td>
                    <td><div align="right"><?php echo $row_getbaseline['cdf_no']; ?></div></td>
                    <td><div align="right"><?php echo $row_gettargets['cdf_no']; ?></div></td>
                    <td><div align="right"><?php echo abs($row_lastdata['SumOfcdf_no'] ); ?></div></td>
                    <td><div align="right">
                        {!! Form::text('cdf_no', null, ['class'=>'form-control']) !!}
                    </div></td>
                    <td><div align="right"><?php echo $row_lastdata['SumOfcdf_no'] + $ReportData['cdf_no'] ?></div></td>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right"><div align="center"></div></td>
                        <td>Population under CDF:</td>
                        <td><div align="right"><?php echo $row_getbaseline['cdf_pop']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['cdf_pop']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_pop']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('cdf_pop', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_pop'] + $ReportData['cdf_pop'] ?></div></td>                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right"><div align="center">03</div></td>
                        <td>Male population:</td>
                        <td><div align="right"><?php echo $row_getbaseline['cdf_male']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['cdf_male']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_male']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('cdf_male', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_male'] + $ReportData['cdf_male'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right"><div align="center">04</div></td>
                        <td>Female population:</td>
                        <td><div align="right"><?php echo $row_getbaseline['cdf_female']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['cdf_female']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_female']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('cdf_female', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_female'] + $ReportData['cdf_female'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right"><div align="center"></div></td>
                        <td>Hardcode population:</td>
                        <td><div align="right"><?php echo $row_getbaseline['cdf_pop_hc']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['cdf_pop_hc']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_pop_hc']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('cdf_pop_hc', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_pop_hc'] + $ReportData['cdf_pop_hc'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right"><div align="center">101</div></td>
                        <td>Number of household</td>
                        <td><div align="right"><?php echo $row_getbaseline['cdf_hh']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['cdf_hh']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_hh']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('cdf_hh', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_hh'] + $ReportData['cdf_hh'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right"><div align="center">102</div></td>
                        <td>Number of hardcore household</td>
                        <td><div align="right"><?php echo $row_getbaseline['cdf_hh_hc']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['cdf_hh_hc']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_hh_hc']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('cdf_hh_hc', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_hh_hc'] + $ReportData['cdf_hh_hc'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right"><div align="center">103</div></td>
                        <td>Number of disable people</td>
                        <td><div align="right"><?php echo $row_getbaseline['cdf_pop_disb']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['cdf_pop_disb']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_pop_disb']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('cdf_pop_disb', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_pop_disb'] + $ReportData['cdf_pop_disb'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right"><div align="center"></div></td>
                        <td>Number of people under social safetynet</td>
                        <td><div align="right"><?php echo $row_getbaseline['cdf_pop_safety']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['cdf_pop_safety']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_pop_safety']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('cdf_pop_safety', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_pop_safety'] + $ReportData['cdf_pop_safety'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of Community Facilitators identified</td>
                        <td><div align="right"><?php echo $row_getbaseline['cdf_cf_tot']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['cdf_cf_tot']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_cf_tot']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('cdf_cf_tot', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_cf_tot'] + $ReportData['cdf_cf_tot'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of MALE Community Facilitators identified</td>
                        <td><div align="right"><?php echo $row_getbaseline['cdf_cf_male']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['cdf_cf_male']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_cf_male']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('cdf_cf_male', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_cf_male'] + $ReportData['cdf_cf_male'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of FEMALE Community Facilitators identified</td>
                        <td><div align="right"><?php echo $row_getbaseline['cdf_cf_female']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['cdf_cf_female']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_cf_female']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('cdf_cf_female', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_cf_female'] + $ReportData['cdf_cf_female'] ?></div></td>                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                    </tr>
                    <tr valign="baseline" class="header">
                        <td colspan="7" align="right" nowrap="nowrap"><div align="left" class="header">UP Management Information</div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of PNGO/Project staff recruited </td>
                        <td><div align="right"><?php echo $row_getbaseline['up_stf_tot']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['up_stf_tot']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfup_stf_tot']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('up_stf_tot', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfup_stf_tot'] + $ReportData['up_stf_tot'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of Male PNGO/Project staff recruited </td>
                        <td><div align="right"><?php echo $row_getbaseline['up_stf_male']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['up_stf_male']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfup_stf_male']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('up_stf_male', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfup_stf_male'] + $ReportData['up_stf_male'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of Female PNGO/Project staff recruited </td>
                        <td><div align="right"><?php echo $row_getbaseline['up_stf_female']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['up_stf_female']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfup_stf_female']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('up_stf_female', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfup_stf_female'] + $ReportData['up_stf_female'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of PNGO engaged by UP</td>
                        <td><div align="right"><?php echo $row_getbaseline['up_pngo']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['up_pngo']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfup_pngo']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('up_pngo', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfup_pngo'] + $ReportData['up_pngo'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of hardware contratctor engaged by UP</td>
                        <td><div align="right"><?php echo $row_getbaseline['up_cont']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['up_cont']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfup_cont']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('up_cont', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfup_cont'] + $ReportData['up_cont'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Public disclosure board Established (Yes=1, No = 0)</td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                         <td><div align="right">
                            {!! Form::text('up_board', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                    </tr>
                    <tr valign="baseline" class="header">
                        <td colspan="7" align="right" nowrap="nowrap"><div align="left" class="header">CDF HYGIENE Information</div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of handwashing sessions</td>
                        <td><div align="right"><?php echo $row_getbaseline['CHY_hw_ses']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['CHY_hw_ses']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_hw_ses']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('CHY_hw_ses', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_hw_ses'] + $ReportData['CHY_hw_ses'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of MALE participated in handwashing sessions</td>
                        <td><div align="right"><?php echo $row_getbaseline['CHY_hw_male']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['CHY_hw_male']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_hw_male']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('CHY_hw_male', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_hw_male'] + $ReportData['CHY_hw_male'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of FEMALE participated in handwashing sessions</td>
                        <td><div align="right"><?php echo $row_getbaseline['CHY_hw_female']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['CHY_hw_female']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_hw_female']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('CHY_hw_female', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_hw_female'] + $ReportData['CHY_hw_female'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of menstrual hygiene sessions</td>
                        <td><div align="right"><?php echo $row_getbaseline['CHY_mn_ses']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['CHY_mn_ses']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_mn_ses']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('CHY_mn_ses', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_mn_ses'] + $ReportData['CHY_mn_ses'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of FEMALE participated inmenstrual hygiene sessions</td>
                        <td><div align="right"><?php echo $row_getbaseline['CHY_mn_female']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['CHY_mn_female']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_mn_female']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('CHY_mn_female', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_mn_female'] + $ReportData['CHY_mn_female'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of hygienic latrine sessions</td>
                        <td><div align="right"><?php echo $row_getbaseline['CHY_sa_ses']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['CHY_sa_ses']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_sa_ses']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('CHY_sa_ses', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_sa_ses'] + $ReportData['CHY_sa_ses'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of household participated in hygienic latrine sessions</td>
                        <td><div align="right"><?php echo $row_getbaseline['CHY_sa_hh']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['CHY_sa_hh']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_sa_hh']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('CHY_sa_hh', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_sa_hh'] + $ReportData['CHY_sa_hh'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of Food hygiene sessions</td>
                        <td><div align="right"><?php echo $row_getbaseline['CHY_fh_ses']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['CHY_fh_ses']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_fh_ses']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('CHY_fh_ses', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_fh_ses'] + $ReportData['CHY_fh_ses'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of household participated in Food hygiene sessions</td>
                        <td><div align="right"><?php echo $row_getbaseline['CHY_fh_hh']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['CHY_fh_hh']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_fh_hh']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('CHY_fh_hh', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_fh_hh'] + $ReportData['CHY_fh_hh'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of household attended TW Maintenance sessions</td>
                        <td><div align="right"><?php echo $row_getbaseline['TW_maintenance']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['TW_maintenance']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfTW_maintenance']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('TW_maintenance', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfTW_maintenance'] + $ReportData['TW_maintenance'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of household attended WSP sessions</td>
                        <td><div align="right"><?php echo $row_getbaseline['WSP']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['WSP']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfwsp']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('WSP', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfwsp'] + $ReportData['WSP'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of new garbage hole build</td>
                        <td><div align="right"><?php echo $row_getbaseline['CHY_gb_new']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['CHY_gb_new']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_gb_new']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('CHY_gb_new', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_gb_new'] + $ReportData['CHY_gb_new'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of  garbage hole repaired</td>
                        <td><div align="right"><?php echo $row_getbaseline['CHY_gb_rep']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['CHY_gb_rep']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_gb_rep']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('CHY_gb_rep', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_gb_rep'] + $ReportData['CHY_gb_rep'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of DRAMA shows in the community</td>
                        <td><div align="right"><?php echo $row_getbaseline['CHY_dr']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['CHY_dr']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_dr']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('CHY_dr', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_dr'] + $ReportData['CHY_dr'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of people attended in DRAMA shows</td>
                        <td><div align="right"><?php echo $row_getbaseline['CHY_dr_pop']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['CHY_dr_pop']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_dr_pop']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('CHY_dr_pop', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_dr_pop'] + $ReportData['CHY_dr_pop'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of VIDEO shows in the community</td>
                        <td><div align="right"><?php echo $row_getbaseline['CHY_vd']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['CHY_vd']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_vd']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('CHY_vd', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_vd'] + $ReportData['CHY_vd'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of people attended in VIDEO shows</td>
                        <td><div align="right"><?php echo $row_getbaseline['CHY_vd_pop']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['CHY_vd_pop']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_vd_pop']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('CHY_vd_pop', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfCHY_vd_pop'] + $ReportData['CHY_vd_pop'] ?></div></td>
                    </tr>

                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Total Improved cook stoves</td>
                        <td><div align="right"><?php echo $row_getbaseline['improved_stoves']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['improved_stoves']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfimproved_stoves']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('improved_stoves', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfimproved_stoves'] + $ReportData['improved_stoves'] ?></div></td>
                    </tr>

                         <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Total Solar power</td>
                        <td><div align="right"><?php echo $row_getbaseline['solar_power']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['solar_power']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfsolar_power']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('solar_power', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfsolar_power'] + $ReportData['solar_power'] ?></div></td>
                    </tr>

                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Total Tree</td>
                        <td><div align="right"><?php echo $row_getbaseline['tree']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['tree']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOftree']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('tree', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOftree'] + $ReportData['tree'] ?></div></td>
                    </tr>
                    
                            <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>CDF climate change sessione</td>
                        <td><div align="right"><?php echo $row_getbaseline['cdf_climate']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['cdf_climate']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_climate']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('cdf_climate', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcdf_climate'] + $ReportData['cdf_climate'] ?></div></td>
                    </tr>
                    
                            <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>School climate change session</td>
                        <td><div align="right"><?php echo $row_getbaseline['sc_climate']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['sc_climate']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfsc_climate']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('sc_climate', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfsc_climate'] + $ReportData['sc_climate'] ?></div></td>
                    </tr> 

                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                    </tr>
                    <tr valign="baseline" class="header">
                        <td colspan="7" align="right" nowrap="nowrap"><div align="left" class="header">School HYGIENE Information</div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Total Schools</td>
                        <td><div align="right"><?php echo $row_getbaseline['scl_tot']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['scl_tot']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_tot']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('scl_tot', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_tot'] + $ReportData['scl_tot'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Total students</td>
                        <td><div align="right"><?php echo $row_getbaseline['scl_tot_std']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['scl_tot_std']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_tot_std']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('scl_tot_std', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_tot_std'] + $ReportData['scl_tot_std'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Total boys</td>
                        <td><div align="right"><?php echo $row_getbaseline['scl_boys']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['scl_boys']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_boys']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('scl_boys', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_boys'] + $ReportData['scl_boys'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Total girls</td>
                        <td><div align="right"><?php echo $row_getbaseline['scl_girls']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['scl_girls']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_girls']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('scl_girls', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_girls'] + $ReportData['scl_girls'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Primary schools</td>
                        <td><div align="right"><?php echo $row_getbaseline['scl_pri']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['scl_pri']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_pri']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('scl_pri', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_pri'] + $ReportData['scl_pri'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of students in Primary schools</td>
                        <td><div align="right"><?php echo $row_getbaseline['scl_pri_std']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['scl_pri_std']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_pri_std']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('scl_pri_std', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_pri_std'] + $ReportData['scl_pri_std'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>High schools</td>
                        <td><div align="right"><?php echo $row_getbaseline['scl_high']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['scl_high']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_high']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('scl_high', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_high'] + $ReportData['scl_high'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of students in high schools</td>
                        <td><div align="right"><?php echo $row_getbaseline['scl_high_std']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['scl_high_std']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_high_std']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('scl_high_std', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_high_std'] + $ReportData['scl_high_std'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Total Madrasha</td>
                        <td><div align="right"><?php echo $row_getbaseline['scl_mad']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['scl_mad']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_mad']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('scl_mad', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_mad'] + $ReportData['scl_mad'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of students in Madrasha</td>
                        <td><div align="right"><?php echo $row_getbaseline['scl_mad_std']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['scl_mad_std']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_mad_std']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('scl_mad_std', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_mad_std'] + $ReportData['scl_mad_std'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Total hygiene promotion sessions</td>
                        <td><div align="right"><?php echo $row_getbaseline['scl_hp_ses']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['scl_hp_ses']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_hp_ses']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('scl_hp_ses', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_hp_ses'] + $ReportData['scl_hp_ses'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of boys participated in hygiene promotion sessions</td>
                        <td><div align="right"><?php echo $row_getbaseline['scl_hp_boys']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['scl_hp_boys']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_hp_boys']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('scl_hp_boys', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_hp_boys'] + $ReportData['scl_hp_boys'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of Girls participated in hygiene promotion sessions</td>
                        <td><div align="right"><?php echo $row_getbaseline['scl_hp_girls']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['scl_hp_girls']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_hp_girls']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('scl_hp_girls', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_hp_girls'] + $ReportData['scl_hp_girls'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Total Menstrual hygiene  sessions</td>
                        <td><div align="right"><?php echo $row_getbaseline['scl_mn_ses']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['scl_mn_ses']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_mn_ses']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('scl_mn_ses', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_mn_ses'] + $ReportData['scl_mn_ses'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td height="28" align="right" nowrap="nowrap">&nbsp;</td>
                        <td>Number of Girls participated in Menstrual hygiene  session</td>
                        <td><div align="right"><?php echo $row_getbaseline['scl_mn_girls']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['scl_mn_girls']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_mn_girls']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('scl_mn_girls', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_mn_girls'] + $ReportData['scl_mn_girls'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of Drama played</td>
                        <td><div align="right"><?php echo $row_getbaseline['scl_dr']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['scl_dr']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_dr']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('scl_dr', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_dr'] + $ReportData['scl_dr'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of students watched drama</td>
                        <td><div align="right"><?php echo $row_getbaseline['scl_dr_std']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['scl_dr_std']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_dr_std']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('scl_dr_std', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_dr_std'] + $ReportData['scl_dr_std'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of Video shows</td>
                        <td><div align="right"><?php echo $row_getbaseline['scl_vd']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['scl_vd']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_vd']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('scl_vd', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_vd'] + $ReportData['scl_vd'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of students watched video shows</td>
                        <td><div align="right"><?php echo $row_getbaseline['scl_vd_std']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['scl_vd_std']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_vd_std']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('scl_vd_std', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfscl_vd_std'] + $ReportData['scl_vd_std'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                    </tr>
                    <tr valign="baseline" class="header">
                        <td colspan="7" align="right" nowrap="nowrap"><div align="left" class="header">Household Sanitation Information</div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of new HH latrine built</td>
                        <td><div align="right"><?php echo $row_getbaseline['HHS_new']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['HHS_new']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfHHS_new']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('HHS_new', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfHHS_new'] + $ReportData['HHS_new'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of new HH latrine built by hardcore </td>
                        <td><div align="right"><?php echo $row_getbaseline['HHS_new_hc']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['HHS_new_hc']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfHHS_new_hc']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('HHS_new_hc', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfHHS_new_hc'] + $ReportData['HHS_new_hc'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of new HH latrine built Amount </td>
                        <td><div align="right"><?php echo $row_getbaseline['HHS_new_amount']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['HHS_new_amount']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfHHS_new_amount']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('HHS_new_amount', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfHHS_new_amount'] + $ReportData['HHS_new_amount'] ?></div></td>
                    </tr>
                    
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of  HH latrine built improved</td>
                        <td><div align="right"><?php echo $row_getbaseline['HHS_rep']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['HHS_rep']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfHHS_rep']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('HHS_rep', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfHHS_rep'] + $ReportData['HHS_rep'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of  HH latrine built improved by hardcore</td>
                        <td><div align="right"><?php echo $row_getbaseline['HHS_rep_hc']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['HHS_rep_hc']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfHHS_rep_hc']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('HHS_rep_hc', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfHHS_rep_hc'] + $ReportData['HHS_rep_hc'] ?></div></td>
                    </tr>
                        <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of  HH latrine built improved Amount</td>
                        <td><div align="right"><?php echo $row_getbaseline['HHS_rep_amount']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['HHS_rep_amount']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfHHS_rep_amount']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('HHS_rep_amount', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfHHS_rep_amount'] + $ReportData['HHS_rep_amount'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                    </tr>
                    <tr valign="baseline" class="header">
                        <td colspan="7" align="right" nowrap="nowrap"><div align="left" class="header">Public/Institutional Sanitation Information</div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of New Sanitation schemes APPROVED</td>
                        <td><div align="right"><?php echo $row_getbaseline['sa_approved']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['sa_approved']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['Sum(sa_approved)']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('sa_approved', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['Sum(sa_approved)'] + $ReportData['sa_approved'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of New Sanitation schemes COMPLETED</td>
                        <td><div align="right"><?php echo $row_getbaseline['sa_completed']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['sa_completed']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['Sum(sa_completed)']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('sa_completed', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['Sum(sa_completed)'] + $ReportData['sa_completed'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of  Sanitation schemes REPAIRED</td>
                        <td><div align="right"><?php echo $row_getbaseline['sa_renovated']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['sa_renovated']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['Sum(sa_renovated)']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('sa_renovated', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['Sum(sa_renovated)'] + $ReportData['sa_renovated'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of Beneficiries from New or Renavated Sanitation schemes</td>
                        <td><div align="right"><?php echo $row_getbaseline['sa_benef']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['sa_benef']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['Sum(sa_benef)']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('sa_benef', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['Sum(sa_benef)'] + $ReportData['sa_benef'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                    </tr>
                    <tr valign="baseline" class="header">
                        <td colspan="7" align="right" nowrap="nowrap"><div align="left" class="header">Capacity Building/Training Information</div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">37</td>
                        <td>Number training courses organized by HYSAWA</td>
                        <td><div align="right"><?php echo $row_getbaseline['cb_trg']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['cb_trg']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcb_trg']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('cb_trg', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcb_trg'] + $ReportData['cb_trg'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">38</td>
                        <td nowrap="nowrap">Number of UP functionaries recieved training from HYSAWA</td>
                        <td><div align="right"><?php echo $row_getbaseline['cb_trg_up_total']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['cb_trg_up_total']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcb_trg_up_total']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('cb_trg_up_total', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcb_trg_up_total'] + $ReportData['cb_trg_up_total'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td nowrap="nowrap">Number of MALE UP functionaries recieved training from HYSAWA</td>
                        <td><div align="right"><?php echo $row_getbaseline['cb_trg_up_male']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['cb_trg_up_male']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcb_trg_up_male']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('cb_trg_up_male', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcb_trg_up_male'] + $ReportData['cb_trg_up_male'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">39</td>
                        <td nowrap="nowrap">Number of FEMALE UP functionaries recieved training from HYSAWA</td>
                        <td><div align="right"><?php echo $row_getbaseline['cb_trg_up_female']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['cb_trg_up_female']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcb_trg_up_female']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('cb_trg_up_female', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcb_trg_up_female'] + $ReportData['cb_trg_up_female'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of UP/PNGO staff recieved training from HYSAWA</td>
                        <td><div align="right"><?php echo $row_getbaseline['cb_trg_stf_total']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['cb_trg_stf_total']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcb_trg_stf_total']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('cb_trg_stf_total', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcb_trg_stf_total'] + $ReportData['cb_trg_stf_total'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of UP/PNGO MALE staff recieved training from HYSAWA</td>
                        <td><div align="right"><?php echo $row_getbaseline['cb_trg_stf_male']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['cb_trg_stf_male']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcb_trg_stf_male']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('cb_trg_stf_male', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcb_trg_stf_male'] + $ReportData['cb_trg_stf_male'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of UP/PNGO FEMALE staff recieved training from HYSAWA</td>
                        <td><div align="right"><?php echo $row_getbaseline['cb_trg_stf_female']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['cb_trg_stf_female']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcb_trg_stf_female']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('cb_trg_stf_female', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcb_trg_stf_female'] + $ReportData['cb_trg_stf_female'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of Community facilitators recieved training from UP</td>
                        <td><div align="right"><?php echo $row_getbaseline['cb_trg_vol_total']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['cb_trg_vol_total']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcb_trg_vol_total']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('cb_trg_vol_total', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcb_trg_vol_total'] + $ReportData['cb_trg_vol_total'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of MALE Community facilitators recieved training from UP</td>
                        <td><div align="right"><?php echo $row_getbaseline['cb_trg_vol_male']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['cb_trg_vol_male']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcb_trg_vol_male']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('cb_trg_vol_male', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcb_trg_vol_male'] + $ReportData['cb_trg_vol_male'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td nowrap="nowrap">Number of FEMALE Community facilitators recieved training from UP</td>
                        <td><div align="right"><?php echo $row_getbaseline['cb_trg_vol_female']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['cb_trg_vol_female']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcb_trg_vol_female']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('cb_trg_vol_female', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['SumOfcb_trg_vol_female'] + $ReportData['cb_trg_vol_female'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                        <td><div align="right"></div></td>
                    </tr>
                    <tr valign="baseline" class="header">
                        <td colspan="7" align="right" nowrap="nowrap"><div align="left" class="header">Water Supply Information</div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of Water supply schemes APPROVED</td>
                        <td><div align="right"><?php echo $row_getbaseline['ws_approved']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['ws_approved']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['Sum(ws_approved)']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('ws_approved', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['Sum(ws_approved)'] + $ReportData['ws_approved'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of Water supply schemes COMPLETED</td>
                        <td><div align="right"><?php echo $row_getbaseline['ws_completed']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['ws_completed']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['Sum(ws_completed)']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('ws_completed', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['Sum(ws_completed)'] + $ReportData['ws_completed'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of people benefited from Completed schemes</td>
                        <td><div align="right"><?php echo $row_getbaseline['ws_beneficiary']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['ws_beneficiary']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['Sum(ws_beneficiary)']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('ws_beneficiary', null, ['class'=>'form-control']) !!}

                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['Sum(ws_beneficiary)'] + $ReportData['ws_beneficiary'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of HARDCORE people benefited from Completed schemes</td>
                        <td><div align="right"><?php echo $row_getbaseline['ws_hc_benef']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['ws_hc_benef']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['Sum(ws_hc_benef)']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('ws_hc_benef', null, ['class'=>'form-control']) !!}
                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['Sum(ws_hc_benef)'] + $ReportData['ws_hc_benef'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td nowrap="nowrap">Number of people have access to safe water within 50 metere or 150 ft</td>
                        <td><div align="right"><?php echo $row_getbaseline['ws_50']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['ws_50']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['Sum(ws_50)']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('ws_50', null, ['class'=>'form-control']) !!}
                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['Sum(ws_50)'] + $ReportData['ws_50'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of CARETAKER trainined</td>
                        <td><div align="right"><?php echo $row_getbaseline['CT_trg']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['CT_trg']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['Sum(rep_data.CT_trg)']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('CT_trg', null, ['class'=>'form-control']) !!}
                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['Sum(rep_data.CT_trg)'] + $ReportData['CT_trg'] ?></div></td>
                    </tr>
                    <tr valign="baseline">
                        <td nowrap="nowrap" align="right">&nbsp;</td>
                        <td>Number of MECHANICS trainined</td>
                        <td><div align="right"><?php echo $row_getbaseline['Pdb']; ?></div></td>
                        <td><div align="right"><?php echo $row_gettargets['Pdb']; ?></div></td>
                        <td><div align="right"><?php echo $row_lastdata['Sum(rep_data.pdb)']; ?></div></td>
                        <td><div align="right">
                            {!! Form::text('Pdb', null, ['class'=>'form-control']) !!}
                        </div></td>
                        <td><div align="right"><?php echo $row_lastdata['Sum(rep_data.pdb)'] + $ReportData['Pdb'] ?></div></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    &nbsp;
    <div class="row padding-top-10">
        <a href="javascript:history.back()" class="btn default"> Cancel </a>
        {!! Form::submit('Save', ['class' => 'btn green pull-right']) !!}
    </div>
    {!! Form::close() !!}





@if(isset($_REQUEST['rep_id']) && $_REQUEST['rep_id'] != "")

<?php

$rep_data = $_REQUEST['rep_period'];
if(!empty($rep_data)){
          $d = explode(' ', $rep_data);
          if(count($d) >= 2){
            $start = date('Y-m-d', strtotime($d[1]." 01, ".$d[0]));
           $end   = date('Y-m-t', strtotime($start));
          }
        }

  $old = [
    'starting_date' =>  $start,
    'ending_date'   => $end,
    'project_id'     => auth()->user()->proj_id,
    'region_id'     => auth()->user()->region_id,
    'district_id'   => auth()->user()->distid,
    'upazila_id'    => auth()->user()->upid,
    'union_id'      => auth()->user()->unid,
    'type'          => 'Event'
  ];

//   if(isset($_REQUEST['rep_id']) && $_REQUEST['rep_id'] != "" && $_REQUEST['rep_id'] != "all"){
//     $rep_data = \DB::table('rep_period')->find($_REQUEST['rep_id']);
//     if(!empty($rep_data)){
//       $d = explode(' ', $rep_data->period);
//       if(count($d) >= 2){
//         $old['starting_date'] = date('Y-m-d', strtotime($d[1]." 01, ".$d[0]));
//         $old['ending_date']   = date('Y-m-t', strtotime($old['starting_date']));
//       }
//     }
//   }
?>



  <div class="col-md-12">
    <?php
      function getTotal1($old, $id){
        return \DB::table('mobile_app_data_list_items')
               ->select(\DB::raw( 'COUNT(mobile_app_data_list_items.id) as total' ))
               ->leftJoin('mob_app_data_list', 'mob_app_data_list.id', '=', 'mobile_app_data_list_items.mobile_app_data_list_id')
               ->where('mobile_app_data_list_items.question_id', $id) // CDF No
               ->where(function($query) use ($old){
                  $date_type = "created_at";
                  if($date_type != "")
                  {
                    if(!empty($old['starting_date']) && !empty($old['ending_date'])){
                      $query->whereDate("mob_app_data_list.".$date_type, '>=', $old['starting_date'])
                            ->whereDate("mob_app_data_list.".$date_type, '<=', $old['ending_date']);
                    }elseif(!empty($old['starting_date'])){
                      $query->whereDate("mob_app_data_list.".$date_type, '=', $old['starting_date']);
                    }
                  }
                  //if(!empty($type)){$query->where('mob_app_data_list.type', 'Event');}
                  if(!empty($old['project_id'])){$query->where('mob_app_data_list.proj_id', $old['project_id']);}
                  if(!empty($old['region_id'])){$query->where('mob_app_data_list.region_id', $old['region_id']);}
                  if(!empty($old['district_id'])){$query->where('mob_app_data_list.distid', $old['district_id']);}
                  if(!empty($old['upazila_id'])){$query->where('mob_app_data_list.upid', $old['upazila_id']);}
                  if(!empty($old['union_id'])){$query->where('mob_app_data_list.unid', $old['union_id']);}
               })
               ->get()
               ->first()
               ->total
               ;
      }

      function getTotal($old, $question_id, $value, $type="COUNT")
      {
        return \DB::table('mobile_app_data_list_items')
           ->select(\DB::raw( $type.'(mobile_app_data_list_items.value) as total' ))
           ->leftJoin('mob_app_data_list', 'mob_app_data_list.id', '=', 'mobile_app_data_list_items.mobile_app_data_list_id')
           ->where('mobile_app_data_list_items.question_id', $question_id)
           ->where(function($query) use ($value, $type) {
              if($type == "COUNT")
              {
                $query->where('mobile_app_data_list_items.value', $value);
              }
           })

           ->where(function($query) use ($old){
              $date_type = "created_at";
              if($date_type != "")
              {
                if(!empty($old['starting_date']) && !empty($old['ending_date'])){
                  $query->whereDate("mob_app_data_list.".$date_type, '>=', $old['starting_date'])
                        ->whereDate("mob_app_data_list.".$date_type, '<=', $old['ending_date']);
                }elseif(!empty($old['starting_date'])){
                  $query->whereDate("mob_app_data_list.".$date_type, '=', $old['starting_date']);
                }
              }
              //if(!empty($type)){$query->where('mob_app_data_list.type', 'Event');}
              if(!empty($old['project_id'])){$query->where('mob_app_data_list.proj_id', $old['project_id']);}
              if(!empty($old['region_id'])){$query->where('mob_app_data_list.region_id', $old['region_id']);}
              if(!empty($old['district_id'])){$query->where('mob_app_data_list.distid', $old['district_id']);}
              if(!empty($old['upazila_id'])){$query->where('mob_app_data_list.upid', $old['upazila_id']);}
              if(!empty($old['union_id'])){$query->where('mob_app_data_list.unid', $old['union_id']);}
           })
           ->get()
           ->first()
           ->total;
      }
    ?>

    <?php
      $old['type'] = "Event";
    ?>

    <h2>Mobile App Date Report</h2>
      <h3>Event Summary <small>[Starting Date: {{$old['starting_date'] or 'all' }} | Ending Date: {{$old['ending_date'] or 'all'}}]</small></h3>
      <table class="box" border="1" width="100%">
        <thead>
          <tr>
            <th rowspan="2"><div>Total CDF No/School No</div></th>
            <th colspan="8"><div>Event Type</div></th>
            <th colspan="3"><div>Location</div></th>
            <th rowspan="2"><div>Total Men/Boy</div></th>
            <th rowspan="2"><div>Total Women/Girl</div></th>
            <th rowspan="2"><div>Disabled</div></th>
          </tr>
          <tr>
            <th><div>Hand Wash</div></th>
            <th><div>Latrine Maintenance</div></th>
            <th><div>Garbage Disposal</div></th>
            <th><div>Menstrual Hygiene</div></th>
            <th><div>Water Safety</div></th>
            <th><div>Food Hygiene</div></th>
            <th><div>Climate Change Awareness</div></th>
            <th><div>Volunteer Orientation</div></th>
            <th><div>Community</div></th>
            <th><div>School</div></th>
            <th><div>UP</div></th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th><div>{{getTotal1($old, 1)}}</div></th>

            <th><div>{{getTotal($old, 2, 'Hand Wash') }}</div></th>
            <th><div>{{getTotal($old, 2, 'Latrine Maintenance') }}</div></th>
            <th><div>{{getTotal($old, 2, 'Garbage Disposal') }}</div></th>
            <th><div>{{getTotal($old, 2, 'Menstrual Hygiene') }}</div></th>
            <th><div>{{getTotal($old, 2, 'Water Safety') }}</div></th>
            <th><div>{{getTotal($old, 2, 'Food Hygiene') }}</div></th>
            <th><div>{{getTotal($old, 2, 'Climate Change Awareness') }}</div></th>
            <th><div>{{getTotal($old, 2, 'Volunteer Orientation') }}</div></th>
            <th><div>{{getTotal($old, 3, 'Community') }}</div></th>
            <th><div>{{getTotal($old, 3, 'School') }}</div></th>
            <th><div>{{getTotal($old, 3, 'UP') }}</div></th>
            <th><div>{{getTotal($old, 4, 'Nos of Men/Boy', "SUM") }}</div></th>
            <th><div>{{getTotal($old, 5, 'Nos of Women/Girl', "SUM") }}</div></th>
            <th><div>{{getTotal($old, 6, 'Nos of Disabled', "SUM") }}</div></th>
          </tr>
        </tbody>
      </table>
      <br/>


    <?php
      $old['type'] = "Water";
    ?>

    <h3>Water Summary <small>[Starting Date: {{$old['starting_date'] or 'all'}} | Ending Date: {{$old['ending_date'] or 'all'}}]</small></h3>
    <table class="box" border="1" width="100%">
      <thead>
        <tr>
          <th rowspan="2"><div>ID No</div></th>
          <th colspan="6"><div>Type</div></th>
          <th colspan="3"><div>Functionality</div></th>
          <th colspan="5"><div>Problem Type</div></th>
          <th colspan="4"><div>Problem Type</div></th>
        </tr>
        <tr>

          <th><div>Water - TW</div></th>
          <th><div>Water - Sky H</div></th>
          <th><div>Water - RO</div></th>
          <th><div>Water - RWH</div></th>
          <th><div>School Latrine</div></th>
          <th><div>Public Latrine</div></th>

          <th><div>Non Functional</div></th>
          <th><div>Function</div></th>
          <th><div>Functional with problems</div></th>

          <th><div>High Saline</div></th>
          <th><div>High Iron</div></th>
          <th><div>Platform Broken</div></th>
          <th><div>Dirty</div></th>
          <th><div>Maintenance Issue</div></th>

          <th><div>Major Repair</div></th>
          <th><div>Minor repair</div></th>
          <th><div>Awareness</div></th>
          <th><div>Improved Management</div></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th><div>{{getTotal1($old, 7)}}</div></th>

          <th><div>{{getTotal($old, 8, 'Water - TW')}}</div></th>
          <th><div>{{getTotal($old, 8, 'Water - Sky H')}}</div></th>
          <th><div>{{getTotal($old, 8, 'Water - RO')}}</div></th>
          <th><div>{{getTotal($old, 8, 'Water - RWH')}}</div></th>
          <th><div>{{getTotal($old, 8, 'School Latrine')}}</div></th>
          <th><div>{{getTotal($old, 8, 'Public Latrine')}}</div></th>

          <th><div>{{getTotal($old, 9, 'Non Functional')}}</div></th>
          <th><div>{{getTotal($old, 9, 'Function')}}</div></th>
          <th><div>{{getTotal($old, 9, 'Functional with problems')}}</div></th>

          <th><div>{{getTotal($old, 10, 'High Saline')}}</div></th>
          <th><div>{{getTotal($old, 10, 'High Iron')}}</div></th>
          <th><div>{{getTotal($old, 10, 'Platform Broken')}}</div></th>
          <th><div>{{getTotal($old, 10, 'Dirty')}}</div></th>
          <th><div>{{getTotal($old, 10, 'Maintenance Issue')}}</div></th>

          <th><div>{{getTotal($old, 11, 'Major Repair')}}</div></th>
          <th><div>{{getTotal($old, 11, 'Minor repair')}}</div></th>
          <th><div>{{getTotal($old, 11, 'Awareness')}}</div></th>
          <th><div>{{getTotal($old, 11, 'Improved Management')}}</div></th>
        </tr>
      </tbody>
    </table>
    <br/>

    <?php
      $old['type'] = "Sanitation";
    ?>
    <h3>Sanitation Summary <small>[Starting Date: {{$old['starting_date'] or 'all'}} | Ending Date: {{$old['ending_date'] or 'all'}}]</small></h3>
    <table class="box" border="1" width="100%">
      <thead>
        <tr>
          <th rowspan="2"><div>ID No</th>
          <th colspan="6"><div>Type</th>
          <th colspan="3"><div>Functionality</th>
          <th colspan="5"><div>Problem Type</th>
          <th colspan="4"><div>Problem Type</th>
        </tr>
        <tr>

          <th><div>Water - TW</div></th>
          <th><div>Water - Sky H</div></th>
          <th><div>Water - RO</div></th>
          <th><div>Water - RWH</div></th>
          <th><div>School Latrine</div></th>
          <th><div>Public Latrine</div></th>

          <th><div>Non Functional</div></th>
          <th><div>Function</div></th>
          <th><div>Functional with problems</div></th>

          <th><div>High Saline</div></th>
          <th><div>High Iron</div></th>
          <th><div>Platform Broken</div></th>
          <th><div>Dirty</div></th>
          <th><div>Maintenance Issue</div></th>

          <th><div>Major Repair</div></th>
          <th><div>Minor repair</div></th>
          <th><div>Awareness</div></th>
          <th><div>Improved Management</div></th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th><div>{{getTotal1($old, 12)}}</div></th>

          <th><div>{{getTotal($old, 13, 'Water - TW')}}</div></th>
          <th><div>{{getTotal($old, 13, 'Water - Sky H')}}</div></th>
          <th><div>{{getTotal($old, 13, 'Water - RO')}}</div></th>
          <th><div>{{getTotal($old, 13, 'Water - RWH')}}</div></th>
          <th><div>{{getTotal($old, 13, 'School Latrine')}}</div></th>
          <th><div>{{getTotal($old, 13, 'Public Latrine')}}</div></th>

          <th><div>{{getTotal($old, 14, 'Non Functional')}}</div></th>
          <th><div>{{getTotal($old, 14, 'Function')}}</div></th>
          <th><div>{{getTotal($old, 14, 'Functional with problems')}}</div></th>

          <th><div>{{getTotal($old, 15, 'High Saline')}}</div></th>
          <th><div>{{getTotal($old, 15, 'High Iron')}}</div></th>
          <th><div>{{getTotal($old, 15, 'Platform Broken')}}</div></th>
          <th><div>{{getTotal($old, 15, 'Dirty')}}</div></th>
          <th><div>{{getTotal($old, 15, 'Maintenance Issue')}}</div></th>

          <th><div>{{getTotal($old, 16, 'Major Repair')}}</div></th>
          <th><div>{{getTotal($old, 16, 'Minor repair')}}</div></th>
          <th><div>{{getTotal($old, 16, 'Awareness')}}</div></th>
          <th><div>{{getTotal($old, 16, 'Improved Management')}}</div></th>
        </tr>
      </tbody>
    </table>
    <br/>
  </div>
@endif


@endsection
