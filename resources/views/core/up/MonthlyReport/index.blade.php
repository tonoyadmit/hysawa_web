@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('up-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Report Period</span>
    </li>
  </ul>
</div>

<h1 class="page-title"> Report period
  <small> view</small>
</h1>

<div class="col-md-12">

  <h5>Select Month:</br>
    If reporting month is not displayed below <a href="{{ route('up-admin.setReportMonth') }}">please Click here to set reporting month</a></h5>
  </div>


  @if(count($reportperiods) > 0)
  <div class="col-md-12">
    <div class="portlet light tasks-widget bordered">
      <div class="portlet-body util-btn-margin-bottom-5">
        <table class="table table-bordered table-hover" id="example0">
          <thead class="flip-content">
            <th>Report id</th>
            <th>Month</th>
            <th>Last update</th>
          </thead>
          <tbody>

            @foreach($reportperiods as $reportperiod)
            <tr>
              <td>{{ $reportperiod['rep_id'] }}</td>

              <td>
                <a href="entryForm?id={{ $reportperiod['id'] }}&rep_id={{ $reportperiod['rep_id'] }}&rep_period={{ $reportperiod['period'] }}">{{ $reportperiod['period'] }} </a>&nbsp;&nbsp;
                <a href="{{
                  route(
                    'up-admin.monthly-report.print',
                    [
                      'id' => $reportperiod['id'],
                      'rep_id' => $reportperiod['rep_id'],
                      'rep_period' => $reportperiod['period']

                    ]
                    )  }}">[Print]</a></td>

              <td>{{ $reportperiod['updated_at'] }}</td>
            </tr>
            @endforeach

          </tbody>
        </table>

        <div class="pagination pull-right">
        </div>


      </div>
    </div>
  </div>

  @else
  <div class="col-md-12">
    <div class="portlet light tasks-widget bordered">
      <p>
        No Data Found
      </p>
    </div>
  </div>
  @endif

  @endsection
