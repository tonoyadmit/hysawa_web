@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('up-admin.dashboard') }}">Dashboard</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Sanitation</span>
    </li>
  </ul>
</div>

<div class="col-md-12" style="margin-top: 10px;">
  @include('core.up.household._search')
</div>

@if(count($sanitations) > 0)

<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <div class="portlet-body util-btn-margin-bottom-5">

        @if(isset($_REQUEST['query']))
      <p>Query:
        <strong>Approval Date</strong>:{{$_REQUEST['starting_date'] or ''}} &nbsp;|
        <strong>Implement Status</strong>:{{$_REQUEST['imp_status'] or ''}} &nbsp;|
        <strong>Approval Status</strong>:{{$_REQUEST['app_status'] or ''}} &nbsp;
      </p>
      @endif

      <div class="table-responsive">
        <table class="table table-bordered table-hover" id="example0">

          <thead class="flip-content">
            <th style="font-size: 10px;">Action</th>
            <th style="font-size: 10px;">ID</th>
            <th style="font-size: 10px;">Approval</th>
            <th style="font-size: 10px;">Implementation Status</th>
            <th style="font-size: 10px;">Approval Date</th>
            <th style="font-size: 10px;">CDF No</th>
            <th style="font-size: 10px;">HH Name</th>
            <th style="font-size: 10px;">Village</th>
            <th style="font-size: 10px;">Latrine Type</th>
            <th style="font-size: 10px;">Total Cost</th>
    <th style="font-size: 10px;">Nos. Beneficiary</th>
          </thead>

          <tbody>
            @foreach($sanitations as $sanitation)
            <tr>
              <td style="font-size: 10px;" >
                <a class="label label-success" href="{{route('up-admin.water_household.edit', $sanitation->id)}}">
                  <i class="fa fa-pencil"></i></a>
              </td>
              <td style="font-size: 10px;">{{ $sanitation->id }}</td>
              <td style="font-size: 10px;">{{ $sanitation->app_status }}</td>
              <td style="font-size: 10px;">{{ $sanitation->imp_status }}</td>
              <td style="font-size: 10px;">{{ $sanitation->app_date }}</td>
              <td style="font-size: 10px;">{{ $sanitation->cdfno }}</td>
              <td style="font-size: 10px;">{{ $sanitation->hh_name }}</td>
              <td style="font-size: 10px;">{{ $sanitation->village }}</td>
              <td style="font-size: 10px;">{{ $sanitation->latrine_type }}</td>
              <td style="font-size: 10px;">{{ $sanitation->total_cost }}</td>
               <td style="font-size: 10px;">{{ $sanitation->male+$sanitation->female }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>

        <div class="pagination pull-right">
          @if(count($sanitations))
            {{$sanitations->appends($old)->links()}}
          @endif
        </div>

      </div>
    </div>
  </div>
</div>
@else
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <p>
      No Data Found
    </p>
  </div>
</div>
@endIf

@endsection
