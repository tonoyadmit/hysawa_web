@extends('layouts.appinside')
@section('content')
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('up-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="{{ URL::to('up-admin.ProcurementEvaluationEntry.index') }}">Tenderers </a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Insert</span>
    </li>
  </ul>
</div>
<h1 class="page-title"> CS Entry:
  <small>create new</small>
</h1>
{!! Form::open(array('route' =>  ['up-admin.ProcurementEvaluationEntry.store'], 'method' => 'post')) !!}
<div class="row">

  @include('partials.errors')

  <div class="col-md-6">
    <div class="form-group">
      <label class="control-label">Name of Work:</label>
      {!! Form::select('package', array(''=>'Select Name of Work') + $projectList,null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
      <label class="control-label">Contractor name:</label>
      {!! Form::select('con_name', array(''=>'Select Contractor name') + $contractorLists,null, ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
      <label class="control-label">Tender Security Amount:</label>
      {!! Form::text('amount', null, ['class' => 'form-control', 'placeholder' => 'Designation in Comittee']) !!}
    </div>

    <div class="form-group">
      <label class="control-label">Quoted % of estimate:</label>
      {!! Form::text('quate', null, ['class' => 'form-control', 'placeholder' => 'Phone']) !!}
    </div>

    <div class="form-group">
      <label class="control-label">Responsive status:</label>
      {!! Form::text('r_status', null, ['class' => 'form-control', 'placeholder' => 'Phone']) !!}
    </div>

    <div class="form-group">
      <label class="control-label">NOA Issued:</label>
      {!! Form::text('noa', null, ['class' => 'form-control', 'placeholder' => 'Phone']) !!}
    </div>

  </div>
  <div class="col-md-6">
    <div class="form-group">
      <label class="control-label">Contract date (YYYY-MM-DD):</label>
      {!! Form::text('con_date', null, ['class' => 'form-control date-picker', 'placeholder' => 'Name', 'data-date-format' =>"yyyy-mm-dd"]) !!}
    </div>

    <div class="form-group">
      <label class="control-label">PO/DD No:</label>
      {!! Form::text('b_detail', null, ['class' => 'form-control', 'placeholder' => 'Designation in UP']) !!}
    </div>
    <div class="form-group">
      <label class="control-label">Qouted Amount:</label>
      {!! Form::text('amount', null, ['class' => 'form-control', 'placeholder' => 'Designation in Comittee']) !!}
    </div>
    <div class="form-group">
      <label class="control-label">Rank:</label>
      {!! Form::text('rank', null, ['class' => 'form-control', 'placeholder' => 'Phone']) !!}
    </div>
    <div class="form-group">
      <label class="control-label">NOA date (YYYY-MM-DD):</label>
      {!! Form::text('noa_date', null, ['class' => 'form-control date-picker', 'placeholder' => 'Phone', 'data-date-format' =>"yyyy-mm-dd"]) !!}
    </div>
    <div class="form-group">
      <label class="control-label">Remarks:</label>
      {!! Form::text('remarks', null, ['class' => 'form-control', 'placeholder' => 'Phone']) !!}
    </div>

  </div>

</div>

&nbsp;
<div class="row padding-top-10">
  <a href="javascript:history.back()" class="btn default"> Cancel </a>
  {!! Form::submit('Save', ['class' => 'btn green pull-right']) !!}
</div>
{!! Form::close() !!}
</div>

<script type="text/javascript" language="javascript" >

  $(document).ready(function () {
    $('#head').on('change', function() {
      var head=$(this).val();
      var csrftoken = $("#csrf-token").val();
      if(head==''){
        $('#subhead').attr('disabled', 'disabled');
      }else{
        $.getJSON('{{ route('up-admin.income.getSubhead') }}?head='+head+'&_token='+csrftoken, function (data) {
          $('select[name="subhead"]').empty();
          $.each(data, function(key, value) {
            $('select[name="subhead"]').append('<option value="'+ key +'">'+ value +'</option>');
          });
        });
      }
    });

    $('#subhead').on('change', function() {
      var subhead=$(this).val();
      var head=$("#head").val();
      var csrftoken = $("#csrf-token").val();
      if(head==''){
        $('#subhead').attr('disabled', 'disabled');
      }else{
        $.getJSON('{{ route('up-admin.income.getSubItem') }}?head='+head+'&_token='+csrftoken+'&subhead='+subhead, function (data) {
          $('select[name="item"]').empty();
          $.each(data, function(key, value) {
            $('select[name="item"]').append('<option value="'+ key +'">'+ value +'</option>');
          });
        });
      }
    });
  });

</script>

@endsection
