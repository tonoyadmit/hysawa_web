@extends('layouts.appinside')
@section('content')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ route('up-admin.dashboard') }}">Home</a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Tenderers List:</span>
            </li>
        </ul>
    </div>
    <h1 class="page-title"> Tenderers List:
        <small> view</small>
    </h1>

    @if(count($procurementEvaluationEntrys) > 0)
        <div class="col-md-12">
            <div class="portlet light tasks-widget bordered">
                <div class="portlet-body util-btn-margin-bottom-5">
                    <table class="table table-bordered table-hover" id="example0">
                        <thead class="flip-content">
                            <th>Action</th>
                            <th>Name of Work</th>
                            <th>Contractor name</th>
                            <th>Tender Security</th>
                            <th>TS Amount</th>
                            <th>% Quoted</th>

                            <th>Responsive?</th>
                            <th>Rank</th>
                            <th>NOA Issued?</th>
                            <th>NOA date</th>
                            <th>Contract date</th>

                        </thead>
                        <tbody>
                            @foreach($procurementEvaluationEntrys as $procurementEvaluationEntry)
                                <tr>
                                    <td>
                                        <a class="label label-success" href="{{route('up-admin.tenderEvaluationComittee.edit', $procurementEvaluationEntry->id)}}">
                                            <i class="fa fa-pencil"></i> Update
                                        </a>
                                    </td>
                                    <td>{{ $procurementEvaluationEntry->package }}</td>
                                    <td>{{ $procurementEvaluationEntry->con_name }}</td>
                                    <td>{{ $procurementEvaluationEntry->security }}</td>
                                    <td>{{ $procurementEvaluationEntry->amount }}</td>
                                    <td>{{ $procurementEvaluationEntry->quate }}</td>
                                    <td>{{ $procurementEvaluationEntry->r_status }}</td>
                                    <td>{{ $procurementEvaluationEntry->rank }}</td>
                                    <td>{{ $procurementEvaluationEntry->noa }}</td>
                                    <td>{{ $procurementEvaluationEntry->noa_date }}</td>
                                    <td>{{ $procurementEvaluationEntry->con_date }}</td>

                                </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <div class="pagination pull-right">
                    </div>

                </div>
            </div>
        </div>
    @else
      <div class="col-md-12">
        <div class="portlet light tasks-widget bordered">
           <p>
              No Data Found
           </p>
        </div>
      </div>
    @endif



@endsection
