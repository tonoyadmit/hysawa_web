@extends('layouts.appinside')

@section('content')
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('up-admin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Proposal Evaluation Comittee (PEC):</span>
      </li>
    </ul>
  </div>
  <h1 class="page-title"> Proposal Evaluation Comittee (PEC):
    <small> view</small>
  </h1>

<div class="col-md-12">
  <a href="{{route('up-admin.proposalEvaluationComittee.create')}}" class="btn btn-large btn-success"> Add new Entry</a>
</div>


  @if(count($proposalEvaluationComittees) > 0)
    <div class="col-md-12">
      <div class="portlet light tasks-widget bordered">
        <div class="portlet-body util-btn-margin-bottom-5">
          <table class="table table-bordered table-hover" id="example0">

            <thead class="flip-content">
              <th>Action</th>
              <th>Name</th>
              <th>Designation in Comittee</th>
              <th>Designation in UP:</th>
              <th>Phone:</th>
            </thead>

            <tbody>
              @foreach($proposalEvaluationComittees as $proposalEvaluationComittee)
              <tr>
                <td>
                  <a class="label label-success" href="{{route('up-admin.proposalEvaluationComittee.edit', $proposalEvaluationComittee->id)}}">
                    <i class="fa fa-pencil"></i> Update
                  </a>
                </td>
                <td>{{ $proposalEvaluationComittee->name }}</td>
                <td>{{ $proposalEvaluationComittee->deg }}</td>
                <td>{{ $proposalEvaluationComittee->UP_Desg }}</td>
                <td>{{ $proposalEvaluationComittee->phone }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
          <div class="pagination pull-right">
          </div>
        </div>
      </div>
    </div>
  @else
    <div class="col-md-12">
      <div class="portlet light tasks-widget bordered">
        <p>
          No Data Found
        </p>
      </div>
    </div>
  @endif
@endsection
