@extends('layouts.appinside')
@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('up-admin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('up-admin.proposalEvaluationComittee.index') }}">PEC</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Update</span>
        </li>
    </ul>
</div>

<h1 class="page-title"> PEC
    <small>Edit new</small>
</h1>

{!! Form::model($proposalEvaluationComittee, array('route' => ['up-admin.proposalEvaluationComittee.update', $proposalEvaluationComittee->id], 'method' => 'put')) !!}
<div class="row">
    @include('partials.errors')
    <div class="col-md-6">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Name:</label>
                {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label class="control-label">Designation in UP:</label>
                {!! Form::text('deg', null, ['class' => 'form-control', 'placeholder' => 'Designation in UP', 'required' => 'required']) !!}
            </div>
            <div class="form-group">
                <label class="control-label">Designation in Comittee:</label>
                {!! Form::text('UP_Desg', null, ['class' => 'form-control', 'placeholder' => 'Designation in Comittee']) !!}
            </div>
            <div class="form-group">
                <label class="control-label">Phone:</label>
                {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Phone']) !!}
            </div>
        </div>
    </div>
</div>
&nbsp;
<div class="row padding-top-10">
    <a href="javascript:history.back()" class="btn default"> Cancel </a>
    {!! Form::submit('Update', ['class' => 'btn green pull-right']) !!}
</div>
{!! Form::close() !!}
</div>
@endsection
