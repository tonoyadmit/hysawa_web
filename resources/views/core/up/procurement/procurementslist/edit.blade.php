@extends('layouts.appinside')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('up-admin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('up-admin.procurementEntry.index') }}">PEC</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Update</span>
        </li>
    </ul>
</div>
<h1 class="page-title"> PEC
    <small>Edit new</small>
</h1>
{!! Form::model($procurementEntry, array('route' => ['up-admin.procurementEntry.update', $procurementEntry->id], 'method' => 'put')) !!}
<div class="row">

    @include('partials.errors')

    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Memo No:</label>
            {!! Form::text('memo_no', null, ['class' => 'form-control', 'placeholder' => 'Memo No', 'required' => 'required']) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Package Name:</label>
            {!! Form::text('package', null, ['class' => 'form-control', 'placeholder' => 'Package Name',]) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Date of announcement:</label>
            {!! Form::text('d_announce', null, ['class' => 'form-control date-picker', 'placeholder' => 'Date of announcement', 'data-date-format' =>"yyyy-mm-dd", 'readonly']) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Last date of receiving:</label>
            {!! Form::text('d_receive', null, ['class' => 'form-control', 'placeholder' => 'Last date of receiving', 'data-date-format' =>"yyyy-mm-dd"]) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Selling Office1:</label>
            {!! Form::text('s_office_1', null, ['class' => 'form-control', 'placeholder' => 'Selling Office1']) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Selling Office2:</label>
            {!! Form::text('s_office_2', null, ['class' => 'form-control', 'placeholder' => 'Selling Office2']) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Selling Office3:</label>
            {!! Form::text('s_office_3', null, ['class' => 'form-control', 'placeholder' => 'Selling Office3']) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Price of Tender Schedule:</label>
            {!! Form::text('price_schedule', null, ['class' => 'form-control', 'placeholder' => 'Price of Tender Schedule']) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Days to complete work:</label>
            {!! Form::text('date_com_work', null, ['class' => 'form-control', 'placeholder' => 'Days to complete work']) !!}
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Method:</label>
            {!! Form::text('method', null, ['class' => 'form-control', 'placeholder' => 'Method', 'required' => 'required']) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Last date of selling</label>
            {!! Form::text('d_sell', null, ['class' => 'form-control date-picker', 'placeholder' => 'Last date of selling', 'required' => 'required', 'data-date-format' =>"yyyy-mm-dd", 'readonly']) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Opening date: YYYY-MM-DD</label>
            {!! Form::text('d_open', null, ['class' => 'form-control date-picker', 'placeholder' => 'Opening date']) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Recieving office1:</label>
            {!! Form::text('r_office_1', null, ['class' => 'form-control', 'placeholder' => 'Recieving office1']) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Recieving office2:</label>
            {!! Form::text('r_office_2', null, ['class' => 'form-control', 'placeholder' => 'Recieving office2']) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Recieving office3:</label>
            {!! Form::text('r_office_3', null, ['class' => 'form-control', 'placeholder' => 'Recieving office3']) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Estimated Value:</label>
            {!! Form::text('estimate', null, ['class' => 'form-control', 'placeholder' => 'Estimated Value']) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Tender Security amount:</label>
            {!! Form::text('s_money', null, ['class' => 'form-control', 'placeholder' => 'Tender Security amount']) !!}
        </div>
    </div>
</div>
&nbsp;
<div class="row padding-top-10">
    <a href="javascript:history.back()" class="btn default"> Cancel </a>
    {!! Form::submit('Update', ['class' => 'btn green pull-right']) !!}
</div>
{!! Form::close() !!}
</div>
@endsection
