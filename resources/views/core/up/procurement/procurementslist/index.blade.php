@extends('layouts.appinside')

@section('content')
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('up-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Procurement List</span>
    </li>
  </ul>
</div>
<h1 class="page-title">Procurement List
  <small> view</small>
</h1>

<div class="col-md-12">
  <a href="{{route('up-admin.procurementEntry.create')}}" class="btn btn-large btn-success"> Add new Entry</a>
</div>


@if(count($procurementEntrys) > 0)
<div class="col-md-12">
  <table class="table table-bordered table-hover table-condensed" id="example0">
    <thead>
      <th style="font-size:10px;">&nbsp;</th>
      <th style="font-size:10px;">Memo No</th>
      <th style="font-size:10px;">Package Name</th>
      <th style="font-size:10px;">Date of announcement</th>
      <th style="font-size:10px;">Last date of receiving</th>
      <th style="font-size:10px;">Opening date</th>
      <th style="font-size:10px;">Price of Tender Schedule</th>
      <th style="font-size:10px;">Estimated Value</th>
      <th style="font-size:10px;">Tender Security Amount</th>
      <th style="font-size:10px;">Days to complete work</th>

      <th style="font-size:10px;">Selling Office1:</th>
      <th style="font-size:10px;">Selling Office2:</th>
      <th style="font-size:10px;">Selling Office3:</th>
      <th style="font-size:10px;">Method:</th>

      <th style="font-size:10px;">Last date of selling</th>
      <th style="font-size:10px;">Recieving office1:</th>
      <th style="font-size:10px;">Recieving office2:</th>
      <th style="font-size:10px;">Recieving office3:</th>

    </thead>
    <tbody>
      @foreach($procurementEntrys as $procurementEntry)
      <tr>
        <td style="font-size:10px;">
          <a class="label label-success" href="{{route('up-admin.procurementEntry.edit', $procurementEntry->id)}}">
            <i class="fa fa-pencil"></i>
          </a>
        </td>
        <td style="font-size:10px;">{{ $procurementEntry->memo_no }}</td>
        <td style="font-size:10px;">{{ $procurementEntry->package }}</td>
        <td style="font-size:10px;">{{ $procurementEntry->d_announce }}</td>
        <td style="font-size:10px;">{{ $procurementEntry->d_receive }}</td>
        <td style="font-size:10px;">{{ $procurementEntry->d_open }}</td>
        <td style="font-size:10px;">{{ $procurementEntry->price_schedule }}</td>
        <td style="font-size:10px;">{{ $procurementEntry->estimate }}</td>
        <td style="font-size:10px;">{{ $procurementEntry->s_money }}</td>
        <td style="font-size:10px;">{{ $procurementEntry->date_com_work }}</td>

        <td style="font-size:10px;">{{ $procurementEntry->s_office_1 }}</td>
        <td style="font-size:10px;">{{ $procurementEntry->s_office_2 }}</td>
        <td style="font-size:10px;">{{ $procurementEntry->s_office_3 }}</td>
        <td style="font-size:10px;">{{ $procurementEntry->method }}</td>

        <td style="font-size:10px;">{{ $procurementEntry->d_sell }}</td>
        <td style="font-size:10px;">{{ $procurementEntry->r_office_1 }}</td>
        <td style="font-size:10px;">{{ $procurementEntry->r_office_2 }}</td>
        <td style="font-size:10px;">{{ $procurementEntry->r_office_3 }}</td>

      </tr>
      @endforeach
    </tbody>
  </table>
</div>
@else
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <p>
      No Data Found
    </p>
  </div>
</div>
@endIf
@endsection
