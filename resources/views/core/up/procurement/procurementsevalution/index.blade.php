@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('up-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Technical Evaluation Comittee (TEC):</span>
    </li>
  </ul>
</div>

<h1 class="page-title"> Technical Evaluation Comittee (TEC):
  <small> view</small>
</h1>

<div class="col-md-12">
</div>

<div class="col-md-12">
  <a href="{{route('up-admin.tenderEvaluationComittee.create')}}" class="btn btn-large btn-success"> Add new Entry</a>
</div>


@if(count($tenderEvaluationComittees) > 0)
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <div class="portlet-body util-btn-margin-bottom-5">
      <table class="table table-bordered table-hover" id="example0">
        <thead class="flip-content">
          <th>Action</th>
          <th>Name</th>
          <th>Designation in Comittee</th>
          <th>Designation in UP:</th>
          <th>Phone:</th>

        </thead>
        <tbody>
          @foreach($tenderEvaluationComittees as $tenderEvaluationComittee)
          <tr>
            <td>
              <a class="label label-success" href="{{route('up-admin.tenderEvaluationComittee.edit', $tenderEvaluationComittee->id) }}/edit">
                <i class="fa fa-pencil"></i> Update
              </a>
            </td>
            <td>{{ $tenderEvaluationComittee->name }}</td>
            <td>{{ $tenderEvaluationComittee->deg }}</td>
            <td>{{ $tenderEvaluationComittee->UP_desg }}</td>
            <td>{{ $tenderEvaluationComittee->phone }}</td>

          </tr>
          @endforeach
        </tbody>
      </table>
      <div class="pagination pull-right">
      </div>
    </div>
  </div>
</div>
@else
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <p>
      No Data Found
    </p>
  </div>
</div>
@endif
<style media="screen">
  .table-filtter .btn{ width: 100%;}
  .table-filtter {
    margin: 20px 0;
  }
</style>

@endsection
