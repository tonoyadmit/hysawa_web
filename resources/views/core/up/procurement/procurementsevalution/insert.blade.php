@extends('layouts.appinside')
@section('content')

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('up-admin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('up-admin.tenderEvaluationComittee.index') }}">Technical Evaluation Comittee (TEC):</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Insert</span>
        </li>
    </ul>
</div>

<h1 class="page-title"> Member Entry:
    <small>create new</small>
</h1>
{!! Form::open(array('route' => ['up-admin.tenderEvaluationComittee.store'], 'method' => 'post')) !!}
<div class="row">
    @include('partials.errors')
    <div class="col-md-6">
        <div class="form-group">
            <label class="control-label">Name:</label>
            {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name', 'required' => 'required']) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Designation in UP:</label>
             <?php
                $degList = \DB::table('trg_desg')->orderBy('desg')->get();
                foreach($degList as $item)
                {
                    $list[$item->desg] = $item->desg;
                }
            ?>

            {!! Form::select('deg', $list, null, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Designation in Comittee:</label>
            {!! Form::select('UP_desg', $list, null, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
        <div class="form-group">
            <label class="control-label">Phone:</label>
            {!! Form::text('phone', null, ['class' => 'form-control', 'placeholder' => 'Phone']) !!}
        </div>
    </div>
</div>
&nbsp;
<div class="row padding-top-10">
    <a href="javascript:history.back()" class="btn default"> Cancel </a>
    {!! Form::submit('Save', ['class' => 'btn green pull-right']) !!}
</div>
{!! Form::close() !!}
</div>

@endsection
