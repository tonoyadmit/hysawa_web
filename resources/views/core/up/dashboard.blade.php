@extends('layouts.appinside')

@section('content')

<?php
  $union = App\Model\Union::with('upazila.district')->find(\Auth::user()->unid);
  $districtName = $union->upazila->district->distname;
  $upazilaName = $union->upazila->upname;
  $unionName = $union->unname;
  $projectName = App\Model\Project::find(\Auth::user()->proj_id);
  $region = App\Model\Region::find(\Auth::user()->region_id);
?>

<div class="row widget-row">

  <div class="col-md-8">
      <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
          <h4 class="widget-thumb-heading">Project Name</h4>
          <div class="widget-thumb-wrap">
              <div class="widget-thumb-body">
                  <span class="widget-thumb-body-stat" >{{$projectName->project}}</span>
              </div>
          </div>
      </div>
  </div>

</div>


<?php
  $wSubmitted = App\Model\Water::where('unid', \Auth::user()->unid)->count();
  $aSubmitted = App\Model\Water::where('unid', \Auth::user()->unid)->where('app_status', 'Approved')->get()->count();
  $asSubmitted = App\Model\Water::where('unid', \Auth::user()->unid)->where('app_status', 'Assessed')->get()->count();
  $cSubmitted = App\Model\Water::where('unid', \Auth::user()->unid)->where('imp_status', 'Completed')->get()->count();
?>


<div class="row widget-row">
  
<div class="col-lg-4 col-md-6">
    <div class="panel panel-primary">
        <div class="panel-heading" style="padding: 20px;">
            <div class="row">
                <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                    <i class="fa fa-inbox fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <div class="huge">{{$wSubmitted}}</div>
                    <div># of Water Scheme Submitted</div>
                </div>
            </div>
        </div>
    </div>
</div>  
<div class="col-lg-4 col-md-6">
    <div class="panel panel-primary">
        <div class="panel-heading" style="padding: 20px;">
            <div class="row">
                <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                    <i class="fa fa-list-alt fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <div class="huge">{{$aSubmitted}}</div>
                    <div># of Water Scheme Approved</div>
                </div>
            </div>
        </div>
    </div>
</div>  
<div class="col-lg-4 col-md-6">
    <div class="panel panel-primary">
        <div class="panel-heading" style="padding: 20px;">
            <div class="row">
                <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                    <i class="fa fa-compress fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <div class="huge">{{$cSubmitted}}</div>
                    <div># of Water Scheme Implemented</div>
                </div>
            </div>
        </div>
    </div>
</div> 
              
  
</div>

<div class="row widget-row">
  
    

<?php
  $swAmount = App\Model\Sanitation::where('unid', \Auth::user()->unid)->count();
  $saAmount = App\Model\Sanitation::where('unid', \Auth::user()->unid)->where('app_status', 'Approved')->get()->count();
  $sasAmount = App\Model\Sanitation::where('unid', \Auth::user()->unid)->where('app_status', 'Assessed')->get()->count();
  $scAmount = App\Model\Sanitation::where('unid', \Auth::user()->unid)->where('imp_status', 'Completed')->get()->count();
?>
   <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 20px;">
                <div class="row">
                    <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                        <i class="fa fa-inbox fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{$swAmount}}</div>
                        <div># of Sanitary Scheme Submitted</div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 20px;">
                <div class="row">
                    <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                        <i class="fa fa-inbox fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{$saAmount}}</div>
                        <div># of Sanitary Scheme Approved</div>
                    </div>
                </div>
            </div>
        </div>
    </div> 
  <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 20px;">
                <div class="row">
                    <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                        <i class="fa fa-inbox fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{$scAmount}}</div>
                        <div># of Sanitary Scheme Implemented</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  
  
</div>

<div class="row widget-row">
  
    

  <?php
    $swAmount = App\Model\Household::where('unid', \Auth::user()->unid)->count();
    $saAmount = App\Model\Household::where('unid', \Auth::user()->unid)->where('app_status', 'Approved')->get()->count();
    $sasAmount = App\Model\Household::where('unid', \Auth::user()->unid)->where('app_status', 'Assessed')->get()->count();
    $scAmount = App\Model\Household::where('unid', \Auth::user()->unid)->where('imp_status', 'Completed')->get()->count();
  ?>
     <div class="col-lg-4 col-md-6">
          <div class="panel panel-primary">
              <div class="panel-heading" style="padding: 20px;">
                  <div class="row">
                      <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                          <i class="fa fa-inbox fa-5x"></i>
                      </div>
                      <div class="col-xs-9 text-right">
                          <div class="huge">{{$swAmount}}</div>
                          <div># of Sanitary Scheme Submitted</div>
                      </div>
                  </div>
              </div>
          </div>
      </div>  
      <div class="col-lg-4 col-md-6">
          <div class="panel panel-primary">
              <div class="panel-heading" style="padding: 20px;">
                  <div class="row">
                      <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                          <i class="fa fa-inbox fa-5x"></i>
                      </div>
                      <div class="col-xs-9 text-right">
                          <div class="huge">{{$saAmount}}</div>
                          <div># of Sanitary Scheme Approved</div>
                      </div>
                  </div>
              </div>
          </div>
      </div> 
    <div class="col-lg-4 col-md-6">
          <div class="panel panel-primary">
              <div class="panel-heading" style="padding: 20px;">
                  <div class="row">
                      <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                          <i class="fa fa-inbox fa-5x"></i>
                      </div>
                      <div class="col-xs-9 text-right">
                          <div class="huge">{{$scAmount}}</div>
                          <div># of Sanitary Scheme Implemented</div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
    
    
  </div>
  


<div class="row widget-row">
  <?php
    $totalIncome = App\Model\FinanceData::where('unid', \Auth::user()->unid)->where('trans_type', 'in')->sum('amount');
    $totalExp = App\Model\FinanceData::where('unid', \Auth::user()->unid)->where('trans_type', 'ex')->sum('amount');
  ?>
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 20px;">
                <div class="row">
                    <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                        <i class="fa fa-shopping-cart fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{$totalIncome}} /=</div>
                        <div>Total Income</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 20px;">
                <div class="row">
                    <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                        <i class="fa icon-bar-chart fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{$totalExp}} /=</div>
                        <div>Total Expenditure</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
 
  


</div>
@endsection