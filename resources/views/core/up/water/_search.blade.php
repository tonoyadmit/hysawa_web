<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Water <small> view</small> </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body form">
        <form action="{{route('up-admin.water.index')}}" class="form-horizontal" method="GET" id="searchForm">
            <input type="hidden" name="query" value="{{time()}}" />

            <div class="form-body">

                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-4">Approval Date: <br/>YYYY-MM-DD</label>
                            <div class="col-md-8">

                                <input
                                    class="form-control date-picker"
                                    type="text"
                                    name="starting_date"
                                    data-date-format = "yyyy-mm-dd"

                                @if(!empty( $old['starting_date']))
                                    value="{{$old['starting_date']}}"
                                @endif >

                            </div>
                        </div>
                    </div>


                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Implement Status</label>
                            <div class="col-md-9">
                                {!! Form::select('imp_status', array(''=>'Select Implementation Status') + $ImplementStatus,$old['imp_status'], ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Approved Status</label>
                            <div class="col-md-9">
                                {!! Form::select('app_status', array(''=>'Select Approve Status') + $approveStatus,$old['app_status'], ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>


                </div>

                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Ward</label>
                            <div class="col-md-9">

                                <input
                                    type="text"
                                    class="form-control"
                                    name="ward_no"
                                    placeholder="Type Ward No"

                                    @if(!empty( $old['ward_no']))
                                        value="{{$old['ward_no']}}"
                                    @endif
                                />

                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Village</label>
                            <div class="col-md-9">

                            <input
                                type="text"
                                class="form-control"
                                name="village"
                                placeholder="Type Village Name"

                                @if(!empty( $old['village']))
                                    value="{{$old['village']}}"
                                @endif
                            />

                            </div>
                        </div>
                    </div>


                    <div class="col-md-2">
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="submit" class="btn green" value="Search" />
                                <a href="{{route('up-admin.water.index')}}" class="btn default">Clear</a>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    @if(isset($waters) && count($waters))
                        <div class="col-md-2">
                            <a href="{{route('up-admin.water.download', $old)}}" class="btn btn-info">Download CSV</a>
                        </div>
                         <div class="col-md-2">
                            <a href="{{route('up-admin.water.print', $old)}}" class="btn btn-info">Print</a>
                        </div>
                    @endif

                </div>

            </div>
        </form>

    </div>
</div>