@extends('layouts.print.app')

@section('my_style')
<style>
    table{
      border-collapse: collapse;
      border-spacing: 0;
    }
    div{
      font-size: 80%;
      display: inline-block;
    }
    .headline th {
      padding: 5px 20px;
    }
  </style>
@endsection

@section('content')
@if (count($waters)>0)

<h2>List of Water Schemes(HYSAWA)</h2>
<p><b>Project: </b> {{App\Model\Project::find(auth()->user()->proj_id)->project}}</p>
<p><b>District</b>:{{$waters->first()->distname or ''}} |<b>Upazila</b>: {{$waters->first()->upname or ''}}|<b>Union</b>: {{$waters->first()->unname or ''}}</p>

@if(isset($_REQUEST['query']) && $_REQUEST['query'] != "")
<p>Query:

    <strong>Approval Date :</strong> {{ $_REQUEST['starting_date'] or ''}} |
    <strong>Implement Status :</strong> {{ $_REQUEST['imp_status'] or ''}} |
    <strong>Approved Status : </strong>{{ $_REQUEST['app_status'] or ''}} |
    <strong>Ward : </strong>{{ $_REQUEST['ward_no'] or ''}} |
    <strong>Village :</strong>{{ $_REQUEST['village'] or ''}}
  </p>
@endif

<h3>Site list of {{count($waters)}} water points</h3>

<table class="box" border="1" width="100%">
    <thead>
    <tr>
      <td><div>SL</div></td>
      <td><div>ID</div></td>
      <td><div>Word No</div></td>
      <td><div>CDF No</div></td>
      <td><div>Village</div></td>
      <td><div>TW No</div></td>
      <td><div>Technology</div></td>
      <td><div>Landowner</div></td>
      <td><div>Caretaker Male</div></td>
      <td><div>Care Female</div></td>
      <td><div>Total HH</div></td>
      <td><div>Harccode HH</div></td>
    </tr>
    </thead>
    <tbody>
      <?php
        $index = 1;
      ?>
        @foreach($waters as $water)
        <tr>
            <td>{{$index++}}</td>
          <td>{{$water->id}}</td>
          <td>{{$water->Ward_no}}</td>
          <td>{{$water->CDF_no}}</td>
          <td>{{$water->Village}}</td>
          <td>{{$water->TW_No}}</td>
          <td>{{$water->Technology_Type}}</td>

          <td>{{$water->Landowner}}</td>
          <td>{{$water->Caretaker_male}}</td>
          <td>{{$water->Caretaker_female}}</td>
          <td>{{$water->HH_benefited}}</td>
          <td>{{$water->HCHH_benefited}}</td>
        </tr>
        @endforeach
     @endif

    </tbody>
  </table>
@endsection