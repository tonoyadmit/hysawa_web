@extends('layouts.appinside')
@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('up-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="{{ route('up-admin.water.index') }}">Water Program</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Insert</span>
    </li>
  </ul>
</div>


<style type="text/css">
.form-group
{
  margin-bottom: 1px;
}
.control-label
{
  font-size: 15px;
  padding-right: 0px;
  padding-left: 0px;
}
</style>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-share "></i>
            <span class="caption-subject bold ">Water Entry <small></small> </span>
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body form">

{!! Form::open(array('route' => 'up-admin.water.store', 'method' => 'post')) !!}
<div class="form-body">
<div class="row">

  @include('partials.errors')

  <div class="col-md-6">

   <div class="form-group">
     <label class="control-label col-md-6">Ward No</label>
     <div class="col-md-6">
     {!! Form::select('Ward_no', array(''=>'Select Ward') + $up, null, ['class' => 'form-control', 'required' => 'required']) !!}
      </div>
   </div>

   <div class="form-group">
     <label class="control-label col-md-6">Village Name</label>
     <div class="col-md-6">
     {!! Form::text('Village', null, ['class' => 'form-control', 'placeholder' => 'Village Name', 'required' => 'required']) !!}
     </div>
   </div>

      <div class="form-group">
   <label class="control-label col-md-6">CDF_no: (3 digits):</label>
   <div class="col-md-6">
   {!! Form::number('CDF_no', null, ['class' => 'form-control', 'placeholder' => 'CDF no', 'required' => 'required','max'=>"999"]) !!}
   </div>
 </div>
 
 

   <div class="form-group">
    <label class="control-label col-md-6">TW_No: (6 digit upcode + tw sl)</label>
    <div class="col-md-6">
    {!! Form::number('TW_No', null, ['class' => 'form-control', 'placeholder' => 'TW_No', 'required' => 'required','min'=>"1000000", ]) !!}
    </div>
  </div>

  <div class="form-group">
    <label class="control-label col-md-6">Land Owner name:</label>
    <div class="col-md-6">
    {!! Form::text('Landowner', null, ['class' => 'form-control', 'placeholder' => 'Land Owner name', 'required' => 'required']) !!}
    </div>
  </div>

  <div class="form-group">
   <label class="control-label col-md-6">Caretaker(male) name:</label>
   <div class="col-md-6">
   {!! Form::text('Caretaker_male', null, ['class' => 'form-control', 'placeholder' => 'Caretaker(male) name', 'required' => 'required']) !!}
   </div>
 </div>

 <div class="form-group">
   <label class="control-label col-md-6">Caretaker (female) name:</label>
   <div class="col-md-6">
   {!! Form::text('Caretaker_female', null, ['class' => 'form-control', 'placeholder' => 'Caretaker (female) name', 'required' => 'required']) !!}
   </div>
 </div>

 

</div>

<div class="col-md-6">

 <div class="form-group">
   <label class="control-label col-md-6">TechnologyType:</label>
   <div class="col-md-6">

   {!! Form::select('Technology_Type', array(''=>'Select Technology') + $tech,null, ['class' => 'form-control', 'required' => 'required']) !!}
   </div>
 </div>
 
 <div class="form-group">
   <label class="control-label col-md-6">Number of Household benefited:</label>
   <div class="col-md-6">
   {!! Form::number('HH_benefited', null, ['class' => 'form-control', 'placeholder' => 'Number of Household benefited', 'required' => 'required','min' => 0, 'max' => 1000]) !!}
   </div>
 </div>
 
 <div class="form-group">
   <label class="control-label col-md-6">Number of Hardcore HH benefited:</label>
   <div class="col-md-6">
   {!! Form::number('HCHH_benefited', null, ['class' => 'form-control', 'placeholder' => 'Number of Hardcore HH benefited', 'required' => 'required','min' => 0, 'max' => 1000]) !!}
   </div>
 </div>
 
 
 <div class="form-group">
   <label class="control-label col-md-6">Total Beneficiary Male:</label>
   <div class="col-md-6">
   {!! Form::number('beneficiary_male', null, ['class' => 'form-control', 'placeholder' => 'Total Beneficiary Male', 'required' => 'required','min' => 0, 'max' => 1000]) !!}
   </div>
 </div>
 
 <div class="form-group">
   <label class="control-label col-md-6">Total Beneficiary_female:</label>
   <div class="col-md-6">
   {!! Form::number('beneficiary_female', null, ['class' => 'form-control', 'placeholder' => 'Total Beneficiary_female', 'required' => 'required','min' => 0, 'max' => 1000]) !!}
   </div>
 
 
 <div class="form-group">
   <label class="control-label col-md-6">Hardcore Beneficiary:</label>
   <div class="col-md-6">
   {!! Form::number('beneficiary_hardcore', null, ['class' => 'form-control', 'placeholder' => 'Hardcore Beneficiary', 'required' => 'required','min' => 0, 'max' => 1000]) !!}
   </div>
 </div>

<input type="hidden" name="imp_status" value="Submitted" />
<input type="hidden" name="app_status" value="Submitted" />

 
 </div>
 
 <div class="form-group">
   <label class="control-label col-md-6">Number of Beneficiary under safetynet:</label>
   <div class="col-md-6">
   {!! Form::number('beneficiary_safetynet', null, ['class' => 'form-control', 'placeholder' => 'Number of Beneficiary under safetynet', 'required' => 'required','min' => 0, 'max' => 1000]) !!}
   </div>
 </div>


 <div class="form-group">
   <label class="control-label col-md-6">Depth:</label>
   <div class="col-md-6">
   {!! Form::number('depth', null, ['class' => 'form-control', 'placeholder' => 'depth', 'required' => 'required','min' => 0, 'max' => 2000]) !!}
   </div>
 </div>

  <div class="form-group">
      <label class="control-label col-md-6"> Latitude(DD.mmmmmm):</label>
      <div class="col-md-6">
      {!! Form::number('y_coord', null, ['class' => 'form-control input-sm', 'placeholder' => 'Latitude', 'required' => 'required','step'=>"any",'min' =>20, 'max' => 30]) !!}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-6">Longitude(DD.mmmmmm):</label>
      <div class="col-md-6">
      {!! Form::number('x_coord', null, ['class' => 'form-control input-sm', 'placeholder' => 'Longitude', 'required' => 'required','step'=>"any",'min' =>80, 'max' => 100]) !!}
      </div>
    </div>
    

</div>

</div>

&nbsp;
<div class="row padding-top-10">
  <div class="col-md-offset-5">
  <a href="javascript:history.back()" class="btn default"> Cancel </a>
  {!! Form::submit('Save', ['class' => 'btn green pull-left']) !!}
  </div>
</div>

{!! Form::close() !!}

    </div>
</div>
</div>
<script type="text/javascript">
  $(document ).ready(function() {
    highlight_nav('warehouse-manage', 'shelfs');
  });
</script>
<style>
    input:required:focus {
  border: 1px solid red;
  outline: none;
  }
   select:required:focus {
 border: 1px solid red;
 outline: none;
 }
</style>
@endsection
