<?php

use App\Model\WaterSummary;

?>
@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('up-admin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Water</span>
      </li>
    </ul>
  </div>

  <div class="col-md-12" style="margin-top: 10px;">
    @include('core.up.water._search')
  </div>

  @if(count($waters) > 0)

    <h3>List</h3>
    @if(isset($_REQUEST['query']))
    <p>Query: 
      <strong>Approval Date</strong>:{{$_REQUEST['starting_date'] or ''}} &nbsp;|
      <strong>Implement Status</strong>:{{$_REQUEST['imp_status'] or ''}} &nbsp;|
      <strong>Approval Status</strong>:{{$_REQUEST['app_status'] or ''}} &nbsp;|
      <strong>Ward</strong>: {{$_REQUEST['ward_no'] or ''}} &nbsp;|
      <strong>Village</strong>:{{$_REQUEST['village'] or ''}}
    </p>
    @endif 
    <div class="table-responsive">
      <table class="table table-bordered table-hover table-condensed" id="example0">

        <thead>
          <th style="font-size: 10px;" >Action</th>
          <th style="font-size: 10px;" >ID</th>
          <th style="font-size: 10px;"  >App.{{-- Approval --}} date</th>
          <th style="font-size: 10px;"  >Work order / Lot no.</th>
          <th style="font-size: 10px;"  >W. No{{-- Ward no --}}</th>
          <th style="font-size: 10px;"  >CDF_no</t h>
          <th style="font-size: 10px;"  >App. status</th>

          <th style="font-size: 10px;"  >Impl. status</th>
          <th style="font-size: 10px;"  >Com. year</th>
          <th style="font-size: 10px;"  >Vill.</th>
          <th style="font-size: 10px;"  >TW No</th>
          <th style="font-size: 10px;"  >Tech Type</th>

          <th style="font-size: 10px;"  >Land Owner</th>
          <th style="font-size: 10px;"  >Care. male</th>
          <th style="font-size: 10px;"  >Care. fe</th>
          <th style="font-size: 10px;"  >HHB{{-- HH benefited --}}</th>
          <th style="font-size: 10px;"  >HHHB{{-- Harccode HH benefited --}}</th>

          <th style="font-size: 10px;"  >BM{{-- beneficiary male --}}</th>
          <th style="font-size: 10px;"  >BF{{-- beneficiary female --}}</th>
          <th style="font-size: 10px;"  >BH{{-- beneficiary hardcore --}}</th>
          <th style="font-size: 10px;"  >BUS{{-- beneficiary under safetynet --}}</th>
          <th style="font-size: 10px;"  >dpt</th>
          <th style="font-size: 10px;"  >X_Cr</th>

          <th style="font-size: 10px;" >Y_Cr</th>
          <th style="font-size: 10px;" >As</th>
          <th style="font-size: 10px;" >Fe</th>
          <th style="font-size: 10px;" >Mn</th>
          <th style="font-size: 10px;" >Cl</th>

          <th style="font-size: 10px;" >Ph</th>
          <th style="font-size: 10px;" >Pb</th>
          <th style="font-size: 10px;" >Zinc</th>
          <th style="font-size: 10px;" >Fc</th>
          <th style="font-size: 10px;" >Td</th>

          <th style="font-size: 10px;" >Tur{{-- Turbidity --}}</th>

        </thead>

        <tbody>
          @foreach($waters as $water)
          <tr>
            <td>
              <a class="label label-success" href="{{route('up-admin.water.edit', $water->id)}}">
                <i class="fa fa-pencil"></i>
              </a>
            </td>
            <td style="font-size: 10px;"  >{{$water->id}}</td>
            <td style="font-size: 10px;"  >{{$water->App_date}}</td>
            <td style="font-size: 10px;"  >{{$water->Tend_lot}}</td>
            <td style="font-size: 10px;"  >{{$water->Ward_no}}</td>
            <td style="font-size: 10px;"  >{{$water->CDF_no}}</td>
            <td style="font-size: 10px;"  >{{$water->app_status}}</td>

            <td style="font-size: 10px;"  >{{$water->imp_status}}</td>
            <td style="font-size: 10px;"  >{{$water->year}}</td>
            <td style="font-size: 10px;"  >{{$water->Village}}</td>
            <td style="font-size: 10px;"  >{{$water->TW_No}}</td>
            <td style="font-size: 10px;"  >{{$water->Technology_Type}}</td>

            <td style="font-size: 10px;"  >{{$water->Landowner}}</td>
            <td style="font-size: 10px;"  >{{$water->Caretaker_male}}</td>
            <td style="font-size: 10px;"  >{{$water->Caretaker_female}}</td>
            <td style="font-size: 10px;"  >{{$water->HH_benefited}}</td>
            <td style="font-size: 10px;"  >{{$water->HCHH_benefited}}</td>

            <td style="font-size: 10px;"  >{{$water->beneficiary_male}}</td>
            <td style="font-size: 10px;"  >{{$water->beneficiary_female}}</td>
            <td style="font-size: 10px;"  >{{$water->beneficiary_hardcore}}</td>
            <td style="font-size: 10px;"  >{{$water->beneficiary_safetynet}}</td>
            <td style="font-size: 10px;"  >{{$water->depth}}</td>
            <td style="font-size: 10px;"  >{{$water->x_coord}}</td>

            <td style="font-size: 10px;"  >{{$water->y_coord}}</td>
            <td style="font-size: 10px;"  >{{$water->wq_Arsenic}}</td>
            <td style="font-size: 10px;"  >{{$water->wq_fe}}</td>
            <td style="font-size: 10px;"  >{{$water->wq_mn}}</td>
            <td style="font-size: 10px;"  >{{$water->wq_cl}}</td>

            <td style="font-size: 10px;"  >{{$water->wq_ph}}</td>
            <td style="font-size: 10px;"  >{{$water->wq_pb}}</td>
            <td style="font-size: 10px;"  >{{$water->wq_zinc}}</td>
            <td style="font-size: 10px;"  >{{$water->wq_fc}}</td>
            <td style="font-size: 10px;"  >{{$water->wq_td}}</td>
            <td style="font-size: 10px;"  >{{$water->wq_turbidity}}</td>
          </tr>
          @endforeach
        </tbody>
      </table>
      <div class="pagination pull-right">
        {{$waters->appends($old)->links()}}
      </div>
    </div>

    @if(empty($old['query']))
      <h3>Water points: Approval and Implementation summary:</h3>
      {!! (new WaterSummary())->generateReportTable() !!}
    @endif

  @else
    <div class="col-md-12">
      <div class="portlet light tasks-widget bordered">
        <p>
          No Data Found
        </p>
      </div>
    </div>
  @endif
@endsection


































