@extends('layouts.appinside')
@section('content')
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('up-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="{{ route('up-admin.water.index') }}">Waters</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Update</span>
    </li>
  </ul>
</div>
<h1 class="page-title"> Waters
  <small>create new</small>
</h1>

<style type="text/css">
.form-group
{
  margin-bottom: 1px;
}
.control-label
{
  font-size: 10px;
  padding-right: 0px;
  padding-left: 0px;
}
</style>


{!! Form::model($water, array('route' => ['up-admin.water.update', $water], 'method' => 'put', 'class'=> 'form-horizontal')) !!}

<div class="form-body">
  <div class="row">
    @include('partials.errors')
    <div class="col-md-6">
      <div class="form-group">
        <label class="control-label col-md-3">Ward No</label>
        <div class="col-md-9">
        {!! Form::select('Ward_no', array(''=>'Select Ward') + $up, null, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">Village Name</label>
        <div class="col-md-9">
        {!! Form::text('Village', null, ['class' => 'form-control', 'placeholder' => 'Village Name', 'required' => 'required']) !!}
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">TW_No: (6 digit upcode + tw sl)</label>
        <div class="col-md-9">
        {!! Form::number('TW_No', null, ['class' => 'form-control', 'placeholder' => 'TW_No', 'required' => 'required','min'=>"100000", ]) !!}
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Land Owner name:</label>
        <div class="col-md-9">
        {!! Form::text('Landowner', null, ['class' => 'form-control', 'placeholder' => 'Land Owner name', 'required' => 'required']) !!}
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">Caretaker(male) name:</label>
        <div class="col-md-9">
        {!! Form::text('Caretaker_male', null, ['class' => 'form-control', 'placeholder' => 'Caretaker(male) name', 'required' => 'required']) !!}
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Caretaker (female) name:</label>
        <div class="col-md-9">
        {!! Form::text('Caretaker_female', null, ['class' => 'form-control', 'placeholder' => 'Caretaker (female) name', 'required' => 'required']) !!}
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Number of Household benefited:</label>
        <div class="col-md-9">
        {!! Form::number('HH_benefited', null, ['class' => 'form-control', 'placeholder' => 'Number of Household benefited', 'required' => 'required','min' => 0, 'max' => 1000]) !!}
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">Total Beneficiary_male:</label>
        <div class="col-md-9">
        {!! Form::number('beneficiary_male', null, ['class' => 'form-control', 'placeholder' => 'Total Beneficiary_male', 'required' => 'required','min' => 0, 'max' => 1000]) !!}
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">Hardcore Beneficiary:</label>
        <div class="col-md-9">
        {!! Form::number('beneficiary_hardcore', null, ['class' => 'form-control', 'placeholder' => 'Hardcore Beneficiary', 'required' => 'required','min' => 0, 'max' => 1000]) !!}
        </div>
      </div>
    </div>
    <div class="col-md-6">
      <div class="form-group">
        <label class="control-label col-md-3">CDF_no: (3 digits):</label>
        <div class="col-md-9">
        {!! Form::number('CDF_no', null, ['class' => 'form-control', 'placeholder' => 'CDF no', 'required' => 'required','max'=>"999"]) !!}
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">Technology Type:</label>
        <div class="col-md-9">
        {!! Form::select('Technology_Type', array(''=>'Select Technology') + $tech,null, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">Approval date (YYYY-MM-DD), if approved:</label>
        <div class="col-md-9">
        {!! Form::text('App_date', null, ['class' => 'form-control date-picker', 'data-date-format' => "yyyy-mm-dd"]) !!}
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">Work Order/Lot No.:</label>
        <div class="col-md-9">
        {!! Form::select('Tend_lot', array(''=>'Select Work Order') + $wordOrderNO, null, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Implementation Status:</label>
        <div class="col-md-9">

        @if($water->imp_status == "Completed")
          <input type="text" disabled="disabled" value="Completed" class="form-control" name="imp_status"/>
        @else
          {!! Form::select('imp_status', array(''=>'Select Status') + $ImplementStatus, null, ['class' => 'form-control', 'required' => 'required']) !!}
        @endif

        </div>
      </div>



      <div class="form-group">
        <label class="control-label col-md-3">Year of Completion: (4 digits):</label>
        <div class="col-md-9">
        {!! Form::text('year', null, ['class' => 'form-control', 'placeholder' => 'Year', ]) !!}
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">Number of Hardcore HH benefited:</label>
        <div class="col-md-9">
        {!! Form::number('HCHH_benefited', null, ['class' => 'form-control', 'placeholder' => 'Number of Hardcore HH benefited', 'required' => 'required','min' => 0, 'max' => 1000]) !!}
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">Total Beneficiary_female:</label>
        <div class="col-md-9">
        {!! Form::number('beneficiary_female', null, ['class' => 'form-control', 'placeholder' => 'Total Beneficiary_female', 'required' => 'required','min' => 0, 'max' => 1000]) !!}
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">Number of Beneficiary under safetynet:</label>
        <div class="col-md-9">
        {!! Form::number('beneficiary_safetynet', null, ['class' => 'form-control', 'placeholder' => 'Height', 'required' => 'required','min' => 0, 'max' => 1000]) !!}
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">Depth:</label>
        <div class="col-md-9">
        {!! Form::number('depth', null, ['class' => 'form-control', 'placeholder' => 'depth', 'required' => 'required','min' => 0, 'max' => 2000]) !!}
        </div>
      </div>
      
    <div class="form-group">
      <label class="control-label col-md-3"> Latitude(DD.mmmmmm):</label>
      <div class="col-md-9">
      {!! Form::number('y_coord', null, ['class' => 'form-control input-sm', 'placeholder' => 'Latitude', 'required' => 'required','step'=>"any",'min' =>20, 'max' => 30]) !!}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-3">Longitude(DD.mmmmmm):</label>
      <div class="col-md-9">
      {!! Form::number('x_coord', null, ['class' => 'form-control input-sm', 'placeholder' => 'Longitude', 'required' => 'required','step'=>"any",'min' => 80, 'max' => 100]) !!}
      </div>
    </div>
    
    </div>
  </div>
</div>

<div class="row padding-top-10">
  <div class="col-md-offset-5">
  <a href="javascript:history.back()" class="btn default"> Cancel </a>
  {!! Form::submit('Update', ['class' => 'btn green pull-left']) !!}
  </div>
</div>
{!! Form::close() !!}
</div>

<style>
    input:required:focus {
  border: 1px solid red;
  outline: none;
  }
   select:required:focus {
 border: 1px solid red;
 outline: none;
 }
</style>
@endsection
