@extends('layouts.appinside')

@section('content')


<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('up-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="{{ route('up-admin.water_sanitation.index') }}">Sanitation</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Update</span>
    </li>
  </ul>
</div>

<h1 class="page-title"> Sanitation
  <small>Update Info</small>
</h1>
<style type="text/css">
.form-group
{
  margin-bottom: 1px;
}
.control-label
{
  font-size: 10px;
  padding-right: 0px;
  padding-left: 0px;
}
</style>

{!! Form::model($WaterSanitation, array('url' => route('up-admin.water_sanitation.update', $WaterSanitation->id ), 'method' => 'put', 'class' => 'form-horizontal')) !!}

<div class="form-body">

  <div class="row">

    @include('partials.errors')

    <div class="col-md-6">

      <div class="form-group">
        <label class="control-label col-md-3">CDF No:</label>
        <div class="col-md-9">
        {!! Form::text('cdfno', null, ['class' => 'form-control', 'placeholder' => 'CDF No:', 'required' => 'required']) !!}
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Village Name</label>
        <div class="col-md-9">
        {!! Form::text('village', null, ['class' => 'form-control', 'placeholder' => 'Village Name', 'required' => 'required']) !!}
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Latrine no. (6 digit upcode+sl)</label>
        <div class="col-md-9">
        {!! Form::number('latrineno', null, ['class' => 'form-control', 'placeholder' => 'TW_No', 'required' => 'required','min'=>"100000" ]) !!}
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Type of latrine:</label>
        <div class="col-md-9">
        {!! Form::select('maintype', array(''=>'Type of latrine:')+$mainype, null, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Type of Institution:</label>
        <div class="col-md-9">
        {!! Form::select('subtype', array(''=>'Type of Institution')+$subtype, null, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">New/Renovation:</label>
        <div class="col-md-9">
        {!! Form::select('cons_type', array('New'=>'New','Renovation'=>'Renovation'), null, ['class' => 'form-control', 'required' => 'required']) !!}
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Name of Institution/Place:</label>
        <div class="col-md-9">
        {!! Form::text('name', null, ['class' => 'form-control', 'placeholder' => 'Name of Institution/Place', 'required' => 'required']) !!}
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Chairman of the Mgt Committe:</label>
        <div class="col-md-9">
        {!! Form::text('ch_comittee', null, ['class' => 'form-control', 'placeholder' => 'Chairman of the Mgt Committe', 'required' => 'required']) !!}
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Phone no:</label>
        <div class="col-md-9">
        {!! Form::text('ch_com_tel', null, ['class' => 'form-control', 'placeholder' => 'Phone no', 'required' => 'required']) !!}
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Implementation Status</label>
        <div class="col-md-9">

        @if($WaterSanitation->imp_status == "Completed")

          <input type="text" name="imp_status" class="form-control" disabled="disabled" value="Completed" />

        @else
          {!! Form::select('imp_status', array(''=>'Select Implementation Status') + $ImplementStatus,null, ['class' => 'form-control', 'required' => 'required']) !!}

        @endif


        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3"> Caretaker name:</label>
        <div class="col-md-9">
        {!! Form::text('caretakername', null, ['class' => 'form-control', 'placeholder' => 'Caretaker name', 'required' => 'required']) !!}
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3"> Caretaker phone:</label>
        <div class="col-md-9">
        {!! Form::text('caretakerphone', null, ['class' => 'form-control', 'placeholder' => 'Caretaker phone', 'required' => 'required']) !!}
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Approval date (YYYY-MM-DD):</label>
        <div class="col-md-9">
        {!! Form::text('app_date', null, ['class' => 'form-control date-picker', 'placeholder' => 'Approval date', 'data-date-format' =>"yyyy-mm-dd"]) !!}
        </div>
      </div>
    </div>

    <div class="col-md-6">


      <div class="form-group">
        <label class="control-label col-md-3">No. of chamber for Male/Boys:</label>
        <div class="col-md-9">
        {!! Form::number('malechamber', null, ['class' => 'form-control', 'placeholder' => 'No. of chamber for Male/Boys', 'required' => 'required','min' => 0, 'max' => 5]) !!}
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">No. of chamber for Female/Girls:</label>
        <div class="col-md-9">
        {!! Form::number('femalechamber', null, ['class' => 'form-control', 'placeholder' => 'No. of chamber for Female/Girls', 'required' => 'required','min' => 0, 'max' => 5]) !!}
        </div>

      </div>
      <div class="form-group">
        <label class="control-label col-md-3">No. of Male beneficiry:</label>
        <div class="col-md-9">
        {!! Form::number('male_ben', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'No. of Male beneficiry','min' => 0, 'max' => 500]) !!}
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">No. of Female Beneficary:</label>
        <div class="col-md-9">
        {!! Form::number('fem_ben', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'No. of Female Beneficary','min' => 0, 'max' => 500]) !!}
        </div>

      </div>
      <div class="form-group">
        <label class="control-label col-md-3">No. of Child beneficiary:</label>
        <div class="col-md-9">
        {!! Form::number('child_ben', null, ['class' => 'form-control', 'required' => 'required', 'placeholder' => 'No. of Child beneficiary','min' => 0, 'max' => 500]) !!}
        </div>

      </div>
      <div class="form-group">
        <label class="control-label col-md-3"> No. of Disable benficiary:</label>

        <div class="col-md-9">
        {!! Form::number('disb_bene', null, ['class' => 'form-control', 'placeholder' => 'No. of Disable benficiary', 'required' => 'required','min' => 0, 'max' => 100]) !!}
        </div>
      </div>
         <h4>Proposed/Installed facilities:</h4>
      <div class="form-group">
        <label class="control-label col-md-3">Water source:</label>
        <div class="col-md-9">
        {!! Form::text('watersource', null, ['class' => 'form-control', 'placeholder' => 'Proposed/Installed facilities', 'required' => 'required']) !!}
        </div>
      </div>
        <div class="form-group">
        <label class="control-label col-md-3">Overhead tank:</label>
        <div class="col-md-9">
        {{ Form::radio('overheadtank', 'yes') }} Yes
        {{ Form::radio('overheadtank', 'NO') }} NO
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">Motor/pump:</label>
        <div class="col-md-9">
        {{ Form::radio('motorpump', 'yes') }} Yes
        {{ Form::radio('motorpump', 'NO') }} NO
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">Sock well:</label>
        <div class="col-md-9">
        {{ Form::radio('sockwell', 'Yes') }}Yes
        {{ Form::radio('sockwell', 'NO') }} NO
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">Septic tank:</label>
        <div class="col-md-9">
        {{ Form::radio('seotictank', 'Yes') }} Yes
        {{ Form::radio('seotictank', 'NO') }} NO
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">Tap outside:</label>
        <div class="col-md-9">
        {{ Form::radio('tapoutside', 'Yes') }} Yes
        {{ Form::radio('tapoutside', 'NO') }} NO
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">Approval Status:</label>
        <div class="col-md-9">
        {!! Form::select('app_status',array(''=>'Approval Status')+$appstatus, null , ['class' => 'form-control', 'placeholder' => 'Total Beneficiary_female', 'required' => 'required']) !!}
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Longitude:</label>
        <div class="col-md-9">
        {!! Form::number('longitude', null, ['class' => 'form-control', 'placeholder' => 'Longitude', 'required' => 'required','step'=>"any",'min' => 80, 'max' => 100]) !!}
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3"> Latitude:</label>
        <div class="col-md-9">
        {!! Form::number('latitude', null, ['class' => 'form-control', 'placeholder' => 'Latitude', 'required' => 'required','step'=>"any",'min' =>20, 'max' => 30]) !!}
        </div>
      </div>
    </div>

  </div>

  <div class="form-actions" style="margin-top: 10px;">
    <div class="col-md-offset-3 col-md-9">
      <a href="javascript:history.back()" class="btn default"> Cancel </a>
      {!! Form::submit('Update', ['class' => 'btn green pull-left']) !!}
    </div>
  </div>

</div>

{!! Form::close() !!}


</div>

<script type="text/javascript">
  $(document ).ready(function() {
    highlight_nav('warehouse-manage', 'shelfs');
  });
</script>

@endsection
