@extends('layouts.print.app')

@section('my_style')
<style>
    table{
      border-collapse: collapse;
      border-spacing: 0;
    }
    div{font-size: 80%;display: inline-block;}
    .headline th {padding: 5px 20px;}
  </style>
@endsection

@section('content')

<h2>List of Sanitation Schemes(HYSAWA)</h2>
<table class="headline">
  <tr>
    <th style="text-align: left;">Project: </th><td>{{App\Model\Project::find(auth()->user()->proj_id)->project}}</td>
  </tr>
  <tr>
    <th style="text-align: left;">District :</th><td>{{$sanitations->first()->distname}}</td>
    <th style="text-align: left;">Upazila :</th><td>{{$sanitations->first()->upname}}</td>
    <th style="text-align: left;">Union :</th><td>{{$sanitations->first()->unname}}</td>
  </tr>
</table>
<br/>
<br/>

@if(isset($_REQUEST['query']) && $_REQUEST['query'] != "")
<p>Query:
    <strong>Approval Date :</strong> {{ $_REQUEST['starting_date'] or ''}} |
    <strong>Implement Status :</strong> {{ $_REQUEST['imp_status'] or ''}} |
    <strong>Approved Status : </strong>{{ $_REQUEST['app_status'] or ''}}

  </p>
@endif




<table class="box" border="1">
  <tr>
      <td><div>ID</div></td>
    <td><div>Approval Status</div></td>
    <td><div>Implimentation Status</div></td>
    <td><div>Approval Date</div></td>
    <td><div>CDF No</div></td>
    <td><div>Type</div></td>
    <td><div>Village</div></td>
    <td><div>Type of Inst.</div></td>
    <td><div>Sub Type</div></td>
    <td><div>Name</div></td>
    <td><div>Male Chamber</div></td>
    <td><div>Female Chamber</div></td>
    <td><div>Male User</div></td>
    <td><div>Female User</div></td>

  </tr>
  <tbody>
     @foreach($sanitations as $sanitation)
      <tr>
        <td>{{$sanitation->id}}</td>
        <td>{{$sanitation->app_status}}</td>
        <td>{{$sanitation->imp_status}}</td>
        <td>@if($sanitation->app_date != "")
          {{date('d-m-Y', strtotime($sanitation->app_date))}}
          @endif</td>
        <td>{{$sanitation->cdfno}}</td>
        <td>{{$sanitation->cons_type}}</td>
        <td>{{$sanitation->village}}</td>
        <td>{{$sanitation->maintype}}</td>
        <td>{{$sanitation->subtype}}</td>
        <td>{{$sanitation->name}}</td>
        <td>{{$sanitation->malechamber}}</td>
        <td>{{$sanitation->femalechamber}}</td>
        <td>{{$sanitation->male_ben}}</td>
        <td>{{$sanitation->fem_ben}}</td>

      </tr>
      @endforeach
  </tbody>
</table>
@endsection