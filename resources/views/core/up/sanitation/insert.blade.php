@extends('layouts.appinside')

@section('content')
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('up-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="{{ route('up-admin.water_sanitation.index') }}">Water Sanitation Program</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Insert</span>
    </li>
  </ul>
</div>

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-share "></i>
            <span class="caption-subject bold ">Sanitation Entry <small></small> </span>
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body form">
{!!
  Form::open([
    'route' => 'up-admin.water_sanitation.store',
    'method' => 'post',  'class' => 'form-horizontal'
  ])
!!}
<div class="form-body">
<div class="row">

  @include('partials.errors')

  <style type="text/css">
    .form-group
    {
      margin-bottom: 1px;
    }
    .control-label
    {
      font-size: 15px;
      padding-right: 0px;
      padding-left: 0px;
    }
  </style>


  <div class="col-md-6">

    <div class="form-group">
      <label class="control-label col-md-6">CDF No:</label>
      <div class="col-md-6">
      {!! Form::text('cdfno', null, ['class' => 'form-control input-sm', 'placeholder' => 'CDF No:', 'required' => 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-6">Village Name</label>
      <div class="col-md-6">
      {!! Form::text('Village', null, ['class' => 'form-control input-sm', 'placeholder' => 'Village Name', 'required' => 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-6">Latrine no. (6 digit upcode + sl)</label>
      <div class="col-md-6">
      {!! Form::number('latrineno', null, ['class' => 'form-control input-sm', 'placeholder' => 'Latrine no. (6 digit upcode+sl)', 'required' => 'required','min'=>"100000" ]) !!}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-6">Type of latrine:</label>
      <div class="col-md-6">
      {!! Form::select('maintype', array(''=>'Type of latrine:')+$mainype, null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-6">Type of Institution:</label>
      <div class="col-md-6">
      {!! Form::select('subtype', array(''=>'Type of Institution')+$subtype, null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-6">New/Renovation:</label>
      <div class="col-md-6">
      {!! Form::select('cons_type', array('New'=>'New','Renovation'=>'Renovation'), null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-6">Name of Institution/Place:</label>
      <div class="col-md-6">
      {!! Form::text('name', null, ['class' => 'form-control input-sm', 'placeholder' => 'Name of Institution/Place', 'required' => 'required']) !!}
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-6">Chairman of the Mgt Committe:</label>
      <div class="col-md-6">
      {!! Form::text('ch_comittee', null, ['class' => 'form-control input-sm', 'placeholder' => 'Chairman of the Mgt Committe', 'required' => 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-6">Phone no:</label>
      <div class="col-md-6">
      {!! Form::text('ch_com_tel', null, ['class' => 'form-control input-sm', 'placeholder' => 'Phone no', 'required' => 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-6">Implementation Status</label>
      <div class="col-md-6">
      {!! Form::select('imp_status', array(''=>'Select Implementation Status') + $ImplementStatus,null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-6"> Caretaker name:</label>
      <div class="col-md-6">
      {!! Form::text('caretakername', null, ['class' => 'form-control input-sm', 'placeholder' => 'Caretaker name', 'required' => 'required']) !!}
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-6"> Caretaker phone:</label>
      <div class="col-md-6">
      {!! Form::text('caretakerphone', null, ['class' => 'form-control input-sm', 'placeholder' => 'Caretaker phone', 'required' => 'required']) !!}
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-6">Approval date (YYYY-MM-DD):</label>
      <div class="col-md-6">
      {!! Form::text('app_date', null, ['class' => 'form-control date-picker input-sm', 'placeholder' => 'Approval date', 'required' => 'required', 'data-date-format' =>"yyyy-mm-dd"]) !!}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-6">No. of chamber for Male/Boys:</label>
      <div class="col-md-6">
      {!! Form::number('malechamber', null, ['class' => 'form-control input-sm', 'placeholder' => 'No. of chamber for Male/Boys', 'required' => 'required','min' => 0, 'max' => 5]) !!}
      </div>
    </div>

  </div>

  <div class="col-md-6">
    <div class="form-group">
      <label class="control-label col-md-6">No. of chamber for Female / Girls:</label>
      <div class="col-md-6">
      {!! Form::number('femalechamber', null, ['class' => 'form-control input-sm', 'placeholder' => 'No. of chamber for Female/Girls', 'required' => 'required','min' => 0, 'max' => 5]) !!}
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-6">No. of Male beneficiry:</label>
      <div class="col-md-6">
      {!! Form::number('male_ben', null, ['class' => 'form-control input-sm', 'required' => 'required', 'placeholder' => 'No. of Male beneficiry','min' => 0, 'max' => 500]) !!}
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-6">No. of Female Beneficary:</label>
      <div class="col-md-6">
      {!! Form::number('fem_ben', null, ['class' => 'form-control input-sm', 'required' => 'required', 'placeholder' => 'No. of Female Beneficary:','min' => 0, 'max' => 500]) !!}
      </div>

    </div>
    <div class="form-group">
      <label class="control-label col-md-6">No. of Child beneficiary:</label>
      <div class="col-md-6">
      {!! Form::number('child_ben', null, ['class' => 'form-control input-sm', 'required' => 'required', 'placeholder' => 'No. of Child beneficiary:','min' => 0, 'max' => 500]) !!}
      </div>

    </div>
     <div class="form-group">
      <label class="control-label col-md-6"> No. of Disable benficiary:</label>
      <div class="col-md-6">
      {!! Form::number('disb_bene', null, ['class' => 'form-control input-sm', 'placeholder' => 'No. of Disable benficiary', 'required' => 'required','min' => 0, 'max' => 100]) !!}
      </div>
    </div>
    <h4>Proposed/Installed facilities:</h4>
    <div class="form-group">
      <label class="control-label col-md-6">Water source:</label>
      <div class="col-md-6">
      {!! Form::text('watersource', null, ['class' => 'form-control input-sm', 'placeholder' => 'Water source', 'required' => 'required']) !!}
      </div>
    </div>
      <div class="form-group">
        <label class="control-label col-md-6">Overhead tank:</label>
        <div class="col-md-6">
        {{ Form::radio('overheadtank', 'yes') }} Yes
        {{ Form::radio('overheadtank', 'NO') }} NO
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-6">Motor/pump:</label>
        <div class="col-md-6">
        {{ Form::radio('motorpump', 'yes') }} Yes
        {{ Form::radio('motorpump', 'NO') }} NO
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-6">Sock well:</label>
        <div class="col-md-6">
        {{ Form::radio('sockwell', 'Yes') }}Yes
        {{ Form::radio('sockwell', 'NO') }} NO
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-6">Septic tank:</label>
        <div class="col-md-6">
        {{ Form::radio('seotictank', 'Yes') }} Yes
        {{ Form::radio('seotictank', 'NO') }} NO
        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-6">Tap outside:</label>
        <div class="col-md-6">
        {{ Form::radio('tapoutside', 'Yes') }} Yes
        {{ Form::radio('tapoutside', 'NO') }} NO
        </div>
      </div>

    <div class="form-group">
      <label class="control-label col-md-6">Approval Status:</label>
      <div class="col-md-6">
      {!! Form::select('app_status',array(''=>'Approval Status')+$appstatus, null , ['class' => 'form-control input-sm', 'placeholder' => 'Approval Status', 'required' => 'required']) !!}
      </div>
    </div>

    <div class="form-group">
      <label class="control-label col-md-6">Longitude:</label>
      <div class="col-md-6">
      {!! Form::number('longitude', null, ['class' => 'form-control input-sm', 'placeholder' => 'Longitude', 'required' => 'required','step'=>"any",'min' => 80, 'max' => 100]) !!}
      </div>
    </div>
    <div class="form-group">
      <label class="control-label col-md-6"> Latitude:</label>
      <div class="col-md-6">
      {!! Form::number('latitude', null, ['class' => 'form-control input-sm', 'placeholder' => 'Latitude', 'required' => 'required','step'=>"any",'min' => 20, 'max' => 30]) !!}
      </div>
    </div>


  </div>
</div>
  <div class="form-actions">
    <div class="col-md-offset-3">
      <a href="javascript:history.back()" class="btn default"> Cancel </a>
      {!! Form::submit('Save', ['class' => 'btn green pull-left']) !!}
    </div>
  </div>
</div>
&nbsp;

{!! Form::close() !!}
</div>
</div>
<style>
    input:required:focus {
  border: 1px solid red;
  outline: none;
  }
   select:required:focus {
 border: 1px solid red;
 outline: none;
 }
</style>
@endsection
