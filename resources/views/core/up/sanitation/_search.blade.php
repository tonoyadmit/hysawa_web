<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Water Sanitation
  <small> List</small> </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body form">
        <form action="{{route('up-admin.water_sanitation.index')}}" class="form-horizontal" method="GET" id="searchForm">

            <input type="hidden" name="query" value="{{time()}}" />

            <div class="form-body">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-4">Approval Date: <br/>YYYY-MM-DD</label>
                            <div class="col-md-8">
                                <input
                                    class="form-control date-picker"
                                    type="text"
                                    name="starting_date"
                                    data-date-format = "yyyy-mm-dd"
                                @if(!empty( $old['starting_date']))
                                    value="{{date('Y-m-d', strtotime($old['starting_date']))}}"
                                @endif >
                            </div>
                        </div>
                    </div>




                     <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Implement Status</label>
                            <div class="col-md-9">
                                {!! Form::select('imp_status', array(''=>'Select Implementation Status') + $ImplementStatus,$old['imp_status'], ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Approved Status</label>
                            <div class="col-md-9">
                                {!! Form::select('app_status', array(''=>'Select Approve Status') + $appstatus,$old['app_status'], ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">



                    <div class="col-md-4">
                        <div class="form-group">
                            <input type="submit" class="btn green" value="Search" />
                            <a href="{{route('up-admin.water_sanitation.index')}}" class="btn default">Clear</a>
                        </div>
                    </div>

                    @if(!empty( $old['query']) )
                        <div class="col-md-2">
                            <a href="{{route('up-admin.water_sanitation.download', $old)}}" class="btn btn-info">Download CSV</a>
                        </div>
                        <div class="col-md-2">
                            <a href="{{route('up-admin.water_sanitation.print', $old)}}" class="btn btn-info">Print</a>
                        </div>
                    @else
                        <div class="col-md-2">
                            <a href="{{route('up-admin.water_sanitation.print', $old)}}" class="btn btn-info">Print</a>
                        </div>
                    @endif
                </div>
            </div>
        </form>
    </div>
</div>