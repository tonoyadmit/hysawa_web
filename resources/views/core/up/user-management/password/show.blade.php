@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('up-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="">User Management</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Add New</span>
    </li>
  </ul>
</div>

@include('partials.errors')

<h1 class="page-title"> User Management <small></small> </h1>

<div class="col-md-12">
  <div class="portlet box blue">
    <div class="portlet-title">
      <div class="caption">
        <i class="fa fa-gift"></i>Update User </div>
        <div class="tools">
          <a href="javascript:;" class="collapse"> </a>
        </div>
      </div>

      <div class="portlet-body form">
        <form action="{{route('up-admin.user-management.password-change.post')}}" class="form-horizontal" method="POST" id="createForm">
          {{csrf_field()}}
          <input type="hidden" name="action" value="{{time()}}" />

          <div class="form-body">

            <div class="row">

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">Name</label>
                    <div class="col-md-4">
                      <input
                      type="text"
                      class="form-control "
                      value="{{$user->name}}"
                      required="required"
                      disabled
                       />
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">Email</label>
                    <div class="col-md-4">
                      <input
                      type="email"
                      class="form-control"
                      value="{{$user->email}}"
                      disabled
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">Supervisor</label>
                    <div class="col-md-4">
                      <input
                      type="text"
                      class="form-control"
                      disabled
                      value="{{$user->supervisor}}"
                       />
                    </div>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">MSISDN</label>
                    <div class="col-md-4">
                      <input
                      type="text"
                      class="form-control"
                      disabled
                      value="{{$user->msisdn}}"
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">Status</label>
                    <div class="col-md-4">
                      <input
                      type="text"
                      class="form-control"
                      disabled
                      @if($user->status == 1) value="Active" @else value="Inactive" @endif
                      />
                    </div>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">Role</label>
                    <div class="col-md-4">
                      <input
                      type="text"
                      class="form-control"
                      disabled
                      @if($user->roles != "")
                      value="{{$user->roles->first()->display_name}}"
                       @endif
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">Region</label>
                    <div class="col-md-4">
                      <input
                      type="text"
                      class="form-control"
                      disabled
                      @if($user->region_id != "")
                        value="{{$user->region->region_name}}"
                      @endif
                      />
                    </div>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">Project</label>
                    <div class="col-md-4">
                      <input
                      type="text"
                      class="form-control"
                      disabled
                      @if($user->proj_id != "")
                        value="{{$user->project->project}}"
                      @endif
                      />

                    </div>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">District</label>
                    <div class="col-md-4">
                      <input
                      type="text"
                      class="form-control"
                      disabled
                      @if($user->distid != "")
                      value="{{$user->district->distname}}"
                      @endif
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">Upazila</label>
                    <div class="col-md-4">
                      <input
                      type="text"
                      class="form-control"
                      disabled
                      @if($user->upid != "")
                      value="{{$user->upazila->upname}}"
                      @endif
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">Union</label>
                    <div class="col-md-4">
                      <input
                      type="text"
                      class="form-control"
                      disabled
                      @if($user->unid != "")
                      value="{{$user->union->unname}}"
                      @endif
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">New Password</label>
                    <div class="col-md-4">
                      <input
                      type="password"
                      class="form-control"
                      name="password"
                      required="required"
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <label class="control-label col-md-3">Password Confirmation</label>
                    <div class="col-md-4">
                      <input
                      type="password"
                      class="form-control"
                      name="confirm_password"
                      required="required"
                      />
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="form-group">
                  <div class="col-md-offset-1 col-md-6">
                    <input type="submit" class="btn green" value="Update Password" />
                  </div>
                </div>
              </div>


            </div>
          </form>

        </div>
      </div>
    </div>

    @endsection
