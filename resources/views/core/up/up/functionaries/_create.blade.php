<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
        <i class="fa fa-gift"></i>Add UP Functionaries </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>

        <div class="portlet-body form">

            <form action="{{route('up-admin.up.functionaries.store')}}" class="form-horizontal" method="POST" id="createForm">

                {{csrf_field()}}

                <input type="hidden" name="query" value="{{time()}}" />

                <div class="form-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Name</label>
                                <div class="col-md-9">

                                    <input class="form-control" type="text" min = "0" name="name" >

                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Designation</label>
                                <div class="col-md-9">

                                    <select class="form-control" name="des">
                                        <option value="Chairman">Chairman</option>
                                        <option value="Secretary">Secretary</option>
                                        <option value="Member-Ward-1">Member-Ward-1</option>
                                        <option value="Member-Ward-2">Member-Ward-2</option>
                                        <option value="Member-Ward-3">Member-Ward-3</option>
                                        <option value="Member-Ward-4">Member-Ward-4</option>
                                        <option value="Member-Ward-5">Member-Ward-5</option>
                                        <option value="Member-Ward-6">Member-Ward-6</option>
                                        <option value="Member-Ward-7">Member-Ward-7</option>
                                        <option value="Member-Ward-8">Member-Ward-8</option>
                                        <option value="Member-Ward-9">Member-Ward-9</option>
                                        <option value="Female member Ward123">Female member Ward123</option>
                                        <option value="Female member Ward456">Female member Ward456</option>
                                        <option value="Female member Ward789">Female member Ward789</option>
                                    </select>


                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Mobile</label>
                                <div class="col-md-9">

                                    <input class="form-control" type="text" min = "0" name="phone" >

                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Email</label>
                                <div class="col-md-9">

                                    <input class="form-control" type="email" min = "0" name="email" >

                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="form-action">
                        <div class="row">

                            <div class="form-group">
                                <div class="col-md-offset-3 col-md-6">
                                    <input type="submit" class="btn green" value="Save" />

                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </form>

        </div>
    </div>