@extends('layouts.appinside')

@section('content')

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('up-admin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('up-admin.up.functionaries.index') }}">UP Functionaries</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>UP</span>
        </li>
    </ul>
</div>

<h1 class="page-title"> UP <small> Basic Info</small> </h1>

<div class="row">
    <div class="col-md-5">
        <div class="portlet box blue">
            <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-gift"></i>Update UP Functionaries </div>
                    <div class="tools">
                        <a href="javascript:;" class="collapse"> </a>
                    </div>
                </div>

                <div class="portlet-body form">

                    <form action="{{route('up-admin.up.functionaries.update', $stuff->id)}}" class="form-horizontal" method="POST" id="createForm">

                        {{csrf_field()}}

                        <input type="hidden" name="query" value="{{time()}}" />

                        <div class="form-body">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Name</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="text" min = "0" name="name" value="{{$stuff->name}}">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Designation</label>
                                        <div class="col-md-9">

                                            <select class="form-control" name="des">
                                                <option value="Chairman" @if($stuff->des == "Chairman") selected="selected" @endif  >Chairman</option>
                                                <option value="Secretary" @if($stuff->des == "Secretary") selected="selected" @endif  >Secretary</option>
                                                <option value="Member-Ward-1" @if($stuff->des == "Member-Ward-1") selected="selected" @endif  >Member-Ward-1</option>
                                                <option value="Member-Ward-2" @if($stuff->des == "Member-Ward-2") selected="selected" @endif  >Member-Ward-2</option>
                                                <option value="Member-Ward-3" @if($stuff->des == "Member-Ward-3") selected="selected" @endif  >Member-Ward-3</option>
                                                <option value="Member-Ward-4" @if($stuff->des == "Member-Ward-4") selected="selected" @endif  >Member-Ward-4</option>
                                                <option value="Member-Ward-5" @if($stuff->des == "Member-Ward-5") selected="selected" @endif  >Member-Ward-5</option>
                                                <option value="Member-Ward-6" @if($stuff->des == "Member-Ward-6") selected="selected" @endif  >Member-Ward-6</option>
                                                <option value="Member-Ward-7" @if($stuff->des == "Member-Ward-7") selected="selected" @endif  >Member-Ward-7</option>
                                                <option value="Member-Ward-8" @if($stuff->des == "Member-Ward-8") selected="selected" @endif  >Member-Ward-8</option>
                                                <option value="Member-Ward-9" @if($stuff->des == "Member-Ward-9") selected="selected" @endif  >Member-Ward-9</option>
                                                <option value="Female member Ward123" @if($stuff->des == "Female member Ward123") selected="selected" @endif>   Female member Ward123</option>
                                                <option value="Female member Ward456" @if($stuff->des == "Female member Ward456") selected="selected" @endif  >Female member Ward456</option>
                                                <option value="Female member Ward789" @if($stuff->des == "Female member Ward789") selected="selected" @endif  >Female member Ward789</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Mobile</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="text" min = "0" name="phone" value="{{$stuff->phone}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Email</label>
                                        <div class="col-md-9">
                                            <input class="form-control" type="email" min = "0" name="email" value="{{$stuff->email}}">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-action">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="col-md-offset-3 col-md-6">
                                            <input type="submit" class="btn green" value="Update" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
