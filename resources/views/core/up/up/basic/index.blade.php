@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('up-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>UP</span>
    </li>
  </ul>
</div>

<h1 class="page-title"> UP <small> Basic Info</small> </h1>


<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>UP Basic info </div>

        <div class="tools">
        @if(!empty($up))
            District: {{$up->upazila->district->distname}} &nbsp;&nbsp; Upazila: {{$up->upazila->upname}} &nbsp;&nbsp; Union: {{$up->unname}}
        @endif
        </div>
        <div class="actions"><a href="javascript:;" class="collapse"> </a></div>
    </div>

    <div class="portlet-body form">
        <form action="{{route('up-admin.up.basic-info.update')}}" class="form-horizontal" method="POST" id="createForm">

            {{csrf_field()}}

            <input type="hidden" name="query" value="{{time()}}" />

            <input type="hidden" name="distid" value="{{\Auth::user()->distid}}" />
            <input type="hidden" name="upid" value="{{\Auth::user()->upid}}" />
            <input type="hidden" name="unid" value="{{\Auth::user()->unid}}" />
            <input type="hidden" name="proid" value="{{\Auth::user()->proj_id}}" />



            <div class="form-body">

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3">Population</label>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-1 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Total</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="tpop"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->tpop}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-1 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Male</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="nmale"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->nmale}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-1 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Female</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="nfem"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->nfem}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-1 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Childred under 18</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="nchil"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->nchil}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-1 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">People With Disabilities: Male &amp; Women</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="pmale"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->pmale}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>

                {{-- <div class="row">
                  <div class="col-md-offset-2 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Female</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="pfemale"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->pfemale}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div> --}}

                {{-- <div class="row">
                  <div class="col-md-offset-2 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-3">Religion</label>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-2 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Muslims</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="rmuslim"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->rmuslim}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-2 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Hindus</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="rhindu"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->rhindu}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-2 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Others</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="rother"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->rother}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div> --}}

                <div class="row">
                  <div class="col-md-offset-1 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Hardcore Poor</label>
                        <div class="col-md-3">
                            <input class="form-control" type="number" min = "0" name="hpoor"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->hpoor}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-1 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">No of Household (HH)</label>
                        <div class="col-md-3">
                            <input class="form-control" type="number" min = "0" name="household_hh"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->household_hh}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-1 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Hardcore Poor HH</label>
                        <div class="col-md-3">
                            <input class="form-control" type="number" min = "0" name="hardcore_poor_hh"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->hardcore_poor_hh}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-3">Infrastructure</label>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-1 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Schools</label>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-2 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Primary</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="sprimary"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->sprimary}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-md-offset-2 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Secondary</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="secondary"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->secondary}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-2 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Other</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="sother"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->sother}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-1 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Colleges</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="ncollege"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->ncollege}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-1 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Hat/Bazar</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="nbazar"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->nbazar}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-md-offset-1 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Hospitals/Healtcare Center</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="nhospital"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->nhospital}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-md-offset-1 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Religious places</label>
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-md-offset-2 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Mosque</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="mosque"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->mosque}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-2 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Mondir/Temple</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="mondir"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->mondir}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-2 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Other</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="uother"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->uother}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-1 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">No.of Ponds</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="npond"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->npond}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-md-offset-1 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">No of Pipe Water Sypply System</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="pwater"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->pwater}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-md-offset-1 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">No of Dug Well</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="ditch"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->ditch}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-1 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">No of Shallow Tube-well</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="water_shallow_tw"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->water_shallow_tw}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-md-offset-1 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">No of Deep Tubewell / Deep Set Pump</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="water_deep_tw"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->water_deep_tw}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>






                <div class="row">
                  <div class="col-md-offset-1 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Other</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="water_other"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->water_other}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-1 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">No of Public Latrine</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="latrine"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->latrine}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-1 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Launch/Fery Ghat</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="launch_ferry_ghat"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->launch_ferry_ghat}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>




                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                        <label class="control-label col-md-4">Socio Economy</label>
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-md-offset-1 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Occupation</label>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-2 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Agriculture</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="agriculture"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->agriculture}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-2 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Business</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="business"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->business}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-2 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Service</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="service"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->service}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>



                <div class="row">
                  <div class="col-md-offset-2 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Other</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="ocother"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->ocother}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>


                <div class="row">
                  <div class="col-md-offset-2 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-2">Households</label>
                        <div class="col-md-3">

                            <input class="form-control" type="number" min = "0" name="nhouse"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->nhouse}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-md-offset-1 col-md-9">
                    <div class="form-group">
                        <label class="control-label col-md-5">Development Projects(i.e.LGSP,HYSAWA)</label>
                        <div class="col-md-3">

                            <input class="form-control" type="text" min = "0" name="dproject"
                            @if(!empty( $upBasicInfo))
                                value="{{$upBasicInfo->dproject}}"
                            @endif >

                        </div>
                    </div>
                  </div>
                </div>


                <div class="form-action">
                  <div class="row">

                        <div class="form-group">
                            <div class="col-md-offset-3 col-md-6">
                                <input type="submit" class="btn green" value="Create Or Update" />

                            </div>
                        </div>

                  </div>
                </div>

            </div>
        </form>

    </div>
</div>



@endsection
