<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
        <i class="fa fa-gift"></i>Add UP Functionaries </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>

        <div class="portlet-body form">

            <form action="{{route('up-admin.up.staff.store')}}" class="form-horizontal" method="POST" id="createForm">

                {{csrf_field()}}

                <input type="hidden" name="query" value="{{time()}}" />

                <div class="form-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Name</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" min = "0" name="name" >
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Designation</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="des">
                                        <option value="Accountant">Accountant</option>
                                        <option value="Community Organizer">Community Organizer</option>
                                        <option value="Community Facilitators">Community Facilitators</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Sex</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="sex">
                                        <option value="Male">Male</option>
                                        <option value="Female">Female</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Waorking wards/CDF</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="word" >
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Staff type</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="type">
                                        <option value="UP Staff">UP Staff</option>
                                        <option value="PNGO staff">PNGO staff</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Mobile</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="phone" >
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Email</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="email" name="email" >
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="form-action">
                        <div class="row">

                            <div class="form-group">
                                <div class="col-md-offset-3 col-md-6">
                                    <input type="submit" class="btn green" value="Save" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>