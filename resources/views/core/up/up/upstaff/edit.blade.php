@extends('layouts.appinside')

@section('content')

<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('up-admin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('up-admin.up.staff.index') }}">Union/PNGO staff </a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>List</span>
        </li>
    </ul>
</div>

<h1 class="page-title"> Union/PNGO staff </h1>

<div class="row">
    <div class="col-md-5">



<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
        <i class="fa fa-gift"></i>Update Union/PNGO staff</div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>

        <div class="portlet-body form">

            <form action="{{route('up-admin.up.staff.update', $stuff->id)}}" class="form-horizontal" method="POST" id="createForm">

                {{csrf_field()}}

                <input type="hidden" name="query" value="{{time()}}" />

                <div class="form-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Name</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="name" value="{{$stuff->name}}">
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Designation</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="des">
                                        <option value="Accountant" @if($stuff->des == "Accountant") selected="selected" @endif >Accountant</option>
                                        <option value="Community Organizer" @if($stuff->des == "Community Organizer") selected="selected" @endif >Community Organizer</option>
                                        <option value="Community Facilitators" @if($stuff->des == "Community Facilitators") selected="selected" @endif >Community Facilitators</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Sex</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="sex">
                                        <option value="Male" @if($stuff->sex == "Male") selected="selected" @endif >Male</option>
                                        <option value="Female" @if($stuff->sex == "Female") selected="selected" @endif >Female</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Waorking wards/CDF</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="word" value="{{$stuff->word}}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Staff type</label>
                                <div class="col-md-9">
                                    <select class="form-control" name="type">
                                        <option value="UP Staff" @if($stuff->type == "UP Staff") selected="selected" @endif >UP Staff</option>
                                        <option value="PNGO staff" @if($stuff->type == "PNGO staff") selected="selected" @endif >PNGO staff</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Mobile</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="phone" value="{{$stuff->phone}}">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Email</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="email" name="email" value="{{$stuff->email}}">
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="form-action">
                        <div class="row">

                            <div class="form-group">
                                <div class="col-md-offset-3 col-md-6">
                                    <input type="submit" class="btn green" value="Update" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>

    </div>
</div>
@endsection