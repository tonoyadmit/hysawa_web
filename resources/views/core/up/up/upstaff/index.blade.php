@extends('layouts.appinside')

@section('content')
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('up-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Union/PNGO staff</span>
    </li>
  </ul>
</div>
<h1 class="page-title"> UP <small> Union/PNGO staff</small> </h1>
<div class="row">
  <div class="col-md-4">
    @include('core.up.up.upstaff._create')
  </div>
  <div class="col-md-8">
    <div class="portlet box blue">
      <div class="portlet-title">
        <div class="caption">
          <i class="fa fa-gift"></i>List of UP/PNGO staff</div>
          <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
          </div>
        </div>
        <div class="portlet-body">
          @if(count($stuffs))
          <div class="table-responsive">
            <table class="table table-bordered table-hover" id="example0">
              <thead class="flip-content">
                <th>Action</th>
                <th style="font-size: 12px;" >Name</th>
                <th style="font-size: 12px;" >Sex</th>
                <th style="font-size: 12px;" >Designation</th>
                <th style="font-size: 12px;" >Working wards</th>
                <th style="font-size: 12px;" >Mobile No</th>
                <th style="font-size: 12px;" >Email</th>
                <th style="font-size: 12px;" >Staff type</th>

              </thead>
              <tbody>
                @foreach($stuffs as $stuff)
                <tr>
                  <td>
                    <a class="label label-success" href="{{route('up-admin.up.staff.edit', $stuff->id)}}">
                      <i class="fa fa-pencil"></i> Details
                    </a>
                  </td>
                  <td style="font-size: 12px;" >{{$stuff->name}}</td>
                  <td style="font-size: 12px;" >{{$stuff->sex}}</td>
                  <td style="font-size: 12px;" >{{$stuff->des}}</td>
                  <td style="font-size: 12px;" >{{$stuff->word}}</td>
                  <td style="font-size: 12px;" >{{$stuff->phone}}</td>
                  <td style="font-size: 12px;" >{{$stuff->email}}</td>
                  <td style="font-size: 12px;" >{{$stuff->type}}</td>

                </tr>
                @endforeach
              </tbody>
            </table>
          </div>
          @else
          <p>Sorry! No Data Found</p>
          @endif
        </div>
      </div>
    </div>
  </div>
  @endsection
