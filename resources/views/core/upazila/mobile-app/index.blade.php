@extends('layouts.appinside')
@section('content')
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('upazila-admin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Mobile App Data:</span>
      </li>
    </ul>
  </div>

  <div class="col-md-12" style="margin-top: 10px;">
      @include('core.upazila.mobile-app._search')
  </div>

  @include('core.upazila.mobile-app._table')

  <br/>
  <br/>


  <div class="col-md-12">
    <style type="text/css">
      #example3 td, #example3 th, #example2 td, #example2 th{
        font-size: 10px;
        text-align: center;
      }
    </style>


<?php
    function getTotal1($old, $id)
    {
      return DB::table('mobile_app_data_list_items')
             ->select(\DB::raw( 'COUNT(mobile_app_data_list_items.id) as total' ))
             ->leftJoin('mob_app_data_list', 'mob_app_data_list.id', '=', 'mobile_app_data_list_items.mobile_app_data_list_id')
             ->where('mobile_app_data_list_items.question_id', $id) // CDF No
             ->where(function($query) use ($old){
                $date_type = "created_at";
                if($date_type != "")
                {
                  if(!empty($old['starting_date']) && !empty($old['ending_date'])){
                    $query->whereDate("mob_app_data_list.".$date_type, '>=', $old['starting_date'])
                          ->whereDate("mob_app_data_list.".$date_type, '<=', $old['ending_date']);
                  }elseif(!empty($old['starting_date'])){
                    $query->whereDate("mob_app_data_list.".$date_type, '=', $old['starting_date']);
                  }
                }
                //if(!empty($type)){$query->where('mob_app_data_list.type', 'Event');}
                if(!empty($old['project_id'])){$query->where('mob_app_data_list.proj_id', $old['project_id']);}
                if(!empty($old['region_id'])){$query->where('mob_app_data_list.region_id', $old['region_id']);}
                if(!empty($old['district_id'])){$query->where('mob_app_data_list.distid', $old['district_id']);}
                if(!empty($old['upazila_id'])){$query->where('mob_app_data_list.upid', $old['upazila_id']);}
                if(!empty($old['union_id'])){$query->where('mob_app_data_list.unid', $old['union_id']);}
             })
             ->get()
             ->first()
             ->total
             ;
    }

    function getTotal($old, $question_id, $value, $type="COUNT")
    {
      return DB::table('mobile_app_data_list_items')
         ->select(\DB::raw( $type.'(mobile_app_data_list_items.value) as total' ))
         ->leftJoin('mob_app_data_list', 'mob_app_data_list.id', '=', 'mobile_app_data_list_items.mobile_app_data_list_id')
         ->where('mobile_app_data_list_items.question_id', $question_id)
         ->where(function($query) use ($value, $type) {
            if($type == "COUNT")
            {
              $query->where('mobile_app_data_list_items.value', $value);
            }
         })

         ->where(function($query) use ($old){
            $date_type = "created_at";
            if($date_type != "")
            {
              if(!empty($old['starting_date']) && !empty($old['ending_date'])){
                $query->whereDate("mob_app_data_list.".$date_type, '>=', $old['starting_date'])
                      ->whereDate("mob_app_data_list.".$date_type, '<=', $old['ending_date']);
              }elseif(!empty($old['starting_date'])){
                $query->whereDate("mob_app_data_list.".$date_type, '=', $old['starting_date']);
              }
            }
           // if(!empty($type)){$query->where('mob_app_data_list.type', 'Event');}
            if(!empty($old['project_id'])){$query->where('mob_app_data_list.proj_id', $old['project_id']);}
            if(!empty($old['region_id'])){$query->where('mob_app_data_list.region_id', $old['region_id']);}
            if(!empty($old['district_id'])){$query->where('mob_app_data_list.distid', $old['district_id']);}
            if(!empty($old['upazila_id'])){$query->where('mob_app_data_list.upid', $old['upazila_id']);}
            if(!empty($old['union_id'])){$query->where('mob_app_data_list.unid', $old['union_id']);}
         })
         ->get()
         ->first()
         ->total;
    }
?>

    @if($old['type'] == "" || $old['type'] == "Event")
      <h3>Event Summary</h3>

      <table class="table table-bordered table-hover data-table" id="example3">
        <thead>
          <tr>
            <th rowspan="2">Total CDF No/School No</th>
            <th colspan="8">Event Type</th>
            <th colspan="3">Location</th>
            <th rowspan="2">Total Men/Boy</th>
            <th rowspan="2">Total Women/Girl</th>
            <th rowspan="2">Disabled</th>
          </tr>
          <tr>
            <th>Hand Wash</th>
            <th>Latrine Maintenance</th>
            <th>Garbage Disposal</th>
            <th>Menstrual Hygiene</th>
            <th>Water Safety</th>
            <th>Food Hygiene</th>
            <th>Climate Change Awareness</th>
            <th>Volunteer Orientation</th>
            <th>Community</th>
            <th>School</th>
            <th>UP</th>
          </tr>
        </thead>
        <tbody>
          <tr>


            <th>{{getTotal1($old, 1)}}</th>

            <th>{{getTotal($old, 2, 'Hand Wash') }}</th>
            <th>{{getTotal($old, 2, 'Latrine Maintenance') }}</th>
            <th>{{getTotal($old, 2, 'Garbage Disposal') }}</th>
            <th>{{getTotal($old, 2, 'Menstrual Hygiene') }}</th>
            <th>{{getTotal($old, 2, 'Water Safety') }}</th>
            <th>{{getTotal($old, 2, 'Food Hygiene') }}</th>
            <th>{{getTotal($old, 2, 'Climate Change Awareness') }}</th>
            <th>{{getTotal($old, 2, 'Volunteer Orientation') }}</th>
            <th>{{getTotal($old, 3, 'Community') }}</th>
            <th>{{getTotal($old, 3, 'School') }}</th>
            <th>{{getTotal($old, 3, 'UP') }}</th>
            <th>{{getTotal($old, 4, 'Nos of Men/Boy', "SUM") }}</th>
            <th>{{getTotal($old, 5, 'Nos of Women/Girl', "SUM") }}</th>
            <th>{{getTotal($old, 6, 'Nos of Disabled', "SUM") }}</th>
          </tr>
        </tbody>
      </table>

      <br/>
      <br/>
    @endif

    @if($old['type'] == "" || $old['type'] == "Water")
    <h3>Water Summary</h3>
    <table class="table table-bordered table-hover data-table" id="example2">
      <thead>
        <tr>
          <th rowspan="2">ID No</th>
          <th colspan="6">Type</th>
          <th colspan="3">Functionality</th>
          <th colspan="5">Problem Type</th>
          <th colspan="4">Problem Type</th>
        </tr>
        <tr>

          <th>Water - TW</th>
          <th>Water - Sky H</th>
          <th>Water - RO</th>
          <th>Water - RWH</th>
          <th>School Latrine</th>
          <th>Public Latrine</th>

          <th>Non Functional</th>
          <th>Function</th>
          <th>Functional with problems</th>

          <th>High Saline</th>
          <th>High Iron</th>
          <th>Platform Broken</th>
          <th>Dirty</th>
          <th>Maintenance Issue</th>

          <th>Major Repair</th>
          <th>Minor repair</th>
          <th>Awareness</th>
          <th>Improved Management</th>

        </tr>
      </thead>
      <tbody>
        <tr>
          <th>{{getTotal1($old, 7)}}</th>

          <th>{{getTotal($old, 8, 'Water - TW') }}</th>
          <th>{{getTotal($old, 8, 'Water - Sky H') }}</th>
          <th>{{getTotal($old, 8, 'Water - RO') }}</th>
          <th>{{getTotal($old, 8, 'Water - RWH') }}</th>
          <th>{{getTotal($old, 8, 'School Latrine') }}</th>
          <th>{{getTotal($old, 8, 'Public Latrine') }}</th>

          <th>{{getTotal($old, 9, 'Non Functional') }}</th>
          <th>{{getTotal($old, 9, 'Function') }}</th>
          <th>{{getTotal($old, 9, 'Functional with problems') }}</th>

          <th>{{getTotal($old, 10, 'High Saline') }}</th>
          <th>{{getTotal($old, 10, 'High Iron') }}</th>
          <th>{{getTotal($old, 10, 'Platform Broken') }}</th>
          <th>{{getTotal($old, 10, 'Dirty') }}</th>
          <th>{{getTotal($old, 10, 'Maintenance Issue') }}</th>

          <th>{{getTotal($old, 11, 'Major Repair') }}</th>
          <th>{{getTotal($old, 11, 'Minor repair') }}</th>
          <th>{{getTotal($old, 11, 'Awareness') }}</th>
          <th>{{getTotal($old, 11, 'Improved Management') }}</th>
        </tr>
      </tbody>
    </table>
    <br/>
    <br/>

    @endif

    @if($old['type'] == "" || $old['type'] == "Sanitation")

    <h3>Sanitation Summary</h3>
    <table class="table table-bordered table-hover data-table" id="example2">
      <thead>
        <tr>
          <th rowspan="2">ID No</th>
          <th colspan="6">Type</th>
          <th colspan="3">Functionality</th>
          <th colspan="5">Problem Type</th>
          <th colspan="4">Problem Type</th>
        </tr>
        <tr>

          <th>Water - TW</th>
          <th>Water - Sky H</th>
          <th>Water - RO</th>
          <th>Water - RWH</th>
          <th>School Latrine</th>
          <th>Public Latrine</th>

          <th>Non Functional</th>
          <th>Function</th>
          <th>Functional with problems</th>

          <th>High Saline</th>
          <th>High Iron</th>
          <th>Platform Broken</th>
          <th>Dirty</th>
          <th>Maintenance Issue</th>

          <th>Major Repair</th>
          <th>Minor repair</th>
          <th>Awareness</th>
          <th>Improved Management</th>

        </tr>
      </thead>
      <tbody>
        <tr>
          <th>{{getTotal1($old, 12)}}</th>

          <th>{{getTotal($old, 13, 'Water - TW') }}</th>
          <th>{{getTotal($old, 13, 'Water - Sky H') }}</th>
          <th>{{getTotal($old, 13, 'Water - RO') }}</th>
          <th>{{getTotal($old, 13, 'Water - RWH') }}</th>
          <th>{{getTotal($old, 13, 'School Latrine') }}</th>
          <th>{{getTotal($old, 13, 'Public Latrine') }}</th>

          <th>{{getTotal($old, 14, 'Non Functional') }}</th>
          <th>{{getTotal($old, 14, 'Function') }}</th>
          <th>{{getTotal($old, 14, 'Functional with problems') }}</th>

          <th>{{getTotal($old, 15, 'High Saline') }}</th>
          <th>{{getTotal($old, 15, 'High Iron') }}</th>
          <th>{{getTotal($old, 15, 'Platform Broken') }}</th>
          <th>{{getTotal($old, 15, 'Dirty') }}</th>
          <th>{{getTotal($old, 15, 'Maintenance Issue') }}</th>

          <th>{{getTotal($old, 16, 'Major Repair') }}</th>
          <th>{{getTotal($old, 16, 'Minor repair') }}</th>
          <th>{{getTotal($old, 16, 'Awareness') }}</th>
          <th>{{getTotal($old, 16, 'Improved Management') }}</th>
        </tr>
      </tbody>
    </table>

    <br/>
    <br/>
    @endif

  </div>

@endsection



@section('my_js')
<script type="text/javascript">
  $(document).ready(function () {

      $('#project').on('change', function() {
          var id = $(this).val();

          $("#region").html("Select Project");
          $("#district").html("Select Region");
          $("#upazila").html("Select District");
          $("#union").html("Select Upazila");
          $.ajax({
            url: '{{route('ajax.regions-from-project')}}?project_id='+id,
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#region").html(response['region_list']);
              }
             // console.log(response);
            }
          });
      });

      $('#region').on('change', function() {
          var id = $(this).val();

          $("#district").html("Select Region");
          $("#upazila").html("Select District");
          $("#union").html("Select Upazila");
          $.ajax({
            url: '{{route('ajax.districts-from-region')}}?region_id='+id,
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#district").html(response['district_list']);
              }
             // console.log(response);
            }
          });
      });

      $('#district').on('change', function() {
          var id = $(this).val();

          $("#upazila").html("Select District");
          $("#union").html("Select Upazila");
          $.ajax({
            url: '{{route('ajax.upazilas-from-district')}}?district_id='+id,
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#upazila").html(response['upazila_list']);
              }
             // console.log(response);
            }
          });
      });

      $('#upazila').on('change', function() {
          var id = $(this).val();

          $.ajax({
            url: '{{route('ajax.unions-from-upazila')}}?upazila_id='+id,
            type: 'GET',
            success: function(response){
              if(response['status'] == true){
                $("#union").html(response['union_list']);
              }
             // console.log(response);
            }
          });
      });

  });
</script>
@endsection