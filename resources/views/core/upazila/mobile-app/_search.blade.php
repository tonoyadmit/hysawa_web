<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> Mobile-App : <small> Data-List</small> </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body form">
        <form action="{{route('upazila-admin.mobile-app.index')}}" class="form-horizontal" method="GET" id="searchForm">

            <input type="hidden" name="query" value="{{time()}}" />

            <div class="form-body">

                <div class="row">

                    <div class="col-md-2">
                        <label class="control-label">Union</label>
                        <select name="union_id" class="form-control input-sm" id="union">
                            <?php
                                $unions = App\Model\Union::where('proid', auth()->user()->proj_id)->where('upid', auth()->user()->upid)->get();
                            ?>

                            <option value="all">All</option>
                            @foreach($unions as $union)
                            <option
                            value="{{$union->id}}"
                            @if(!empty($old['union_id']) && $union->id == $old['union_id'])
                            selected="selected"
                            @endif
                            >{{$union->unname}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="col-md-2">
                        <label class="control-label">Starting Date</label>
                        <input
                                    class="form-control input-sm date-picker"
                                    type="text"
                                    name="starting_date"
                                    data-date-format="yyyy-mm-dd"

                                    @if(!empty( $old['starting_date']))
                                        value="{{date('Y-m-d', strtotime($old['starting_date']))}}"
                                    @endif
                                />
                                <p style="margin-bottom: 0px;">YYYY-MM-DD</p>
                    </div>

                    <div class="col-md-2">
                        <label class="control-label">Ending Date</label>
                        <input
                            class="form-control input-sm date-picker"
                            type="text"
                            name="ending_date"
                            data-date-format="yyyy-mm-dd"

                            @if(!empty( $old['ending_date']))
                                value="{{date('Y-m-d', strtotime($old['ending_date']))}}"
                            @endif
                        />
                        <p style="margin-bottom: 0px;">YYYY-MM-DD</p>
                    </div>


                    <div class="col-md-2">
                        <label class="control-label">Type</label>
                        <select name="type" class="form-control input-sm" >
                                    <?php
                                        $data = App\Model\MobAppDataList::groupBy('type')->get();

                                        \Log::info($data->toArray());
                                    ?>
                                    <option value="">Select Type</option>

                                    @foreach($data as $type)
                                    <option
                                    value="{{$type->type}}"
                                    @if(
                                        !empty($old['type']) &&
                                        $old['type'] != "" &&
                                        $type->type == $old['type']
                                    )
                                        selected="selected"
                                    @endif
                                    >{{$type->type}}</option>
                                    @endforeach
                                </select>
                    </div>

                    <div class="col-md-2">
                        <label class="control-label">UserId</label>
                        <input type="text" name="user_id" value="{{old('user_id')}}" class="form-control" />
                    </div>


                </div>

                <div class="row">
                    <div class="col-md-3">
                        <input type="submit" class="btn green" value="Search" />
                        <a href="{{route('upazila-admin.mobile-app.index')}}" class="btn default">Clear</a>
                        <?php
                            $old2 = $old;
                            $old2['print'] = 'print';
                        ?>
                        <a href="{{route('upazila-admin.mobile-app.index', $old2)}}" class="btn btn-info btn-sm pull-right">Print</a>
                    </div>
                </div>

            </div>
        </form>


        @if(isset($_REQUEST['query']) && $_REQUEST['query'] != "")

          <div class="row">
            <div class="col-md-12">

              <p>Query:
              <?php
                $unionName    = isset($_REQUEST['union_id'])    ? $_REQUEST['union_id']     : "";

                $districtName = App\Model\District::find(auth()->user()->distid)->distname;
                $upazilaName = App\Model\Upazila::find(auth()->user()->upid)->upname;

                if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] != "" && $_REQUEST['union_id'] != "all")
                {
                  $unionName = App\Model\Union::find($_REQUEST['union_id'])->unname;
                }


              ?>

                <b>Starting Date:</b> {{$_REQUEST['starting_date']}}|
                <b>Ending Date:</b> {{$_REQUEST['ending_date']}}
                <b>Type:</b> {{$_REQUEST['type'] or ''}}
                <b>District:</b> {{$districtName or ''}}|
                <b>Upazila:</b> {{$upazilaName or ''}}|
                <b>Union:</b> {{$unionName or ''}}|

              </p>

            </div>
          </div>
          @endif



    </div>
</div>