@extends('layouts.print.app')

@section('my_style')
<style>
  table{border-collapse: collapse;border-spacing: 0; border: 1px;}
  div{font-size: 80%;display: inline-block;}
  .headline th {padding: 5px 20px;}
</style>
@endsection

@section('content')
<h3>Mobile App Data Entry List</h3>
<table class="table table-bordered table-hover" border="1" width="100%">
    <thead class="flip-content">
      <th width="15%">Created At</th>
      <th width="10%">Type</th>
      <th >Union</th>
      <th >User (UserId)</th>
    </thead>
    <tbody>
        @foreach($datas as $data)
            <tr>
                <td >{{$data->inserted_at }}</td>
                <td >{{$data->type }}</td>
                <td >{{$data->union->unname or ''}}</td>
                <td>{{ $data->user->name or ''}} ({{$data->user->email or ''}}) </td>
            </tr>
        @endforeach
    </tbody>
</table>


@endsection