@extends('layouts.print.app')
@section('my_style')
<style>
    table{
        border-collapse: collapse;
        border-spacing: 0;
        text-align: right;
    }
    div{
        font-size: 80%;
        display: inline-block;
    }
    .headline th {
        padding: 5px 20px;
    }
    .text-right{
        text-align: right;
    }
    .text-left{
        text-align: left;
    }
</style>
<style media="screen">
    .table-filtter .btn{ width: 100%;}
    .table-filtter {
        margin: 20px 0;
    }
    .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
        border: 1px solid #000;
    }
</style>
@endsection



@section('content')
<?php
    $projectTitle = App\Model\Project::find(auth()->user()->proj_id)->project;
?>
<p><b>Project</b>: {{$projectTitle or ''}}</p>

@if(isset($_REQUEST['query']) && $_REQUEST['query'] != "")
      <p>Query:
      <?php
        $regionName = App\Model\Region::find(auth()->user()->region_id)->region_name;
        $districtName = App\Model\District::find(auth()->user()->distid)->distname;
        $upazilaName = App\Model\Upazila::find(auth()->user()->upid)->upname;
        $unionName = $_REQUEST['union_id'];

        if($unionName != "" && $unionName != "all")
        {
          $unionName = App\Model\Union::find($_REQUEST['union_id'])->unname;
        }

      ?>

        <b>Region: </b> {{$regionName or ''}}|
        <b>District:</b> {{$districtName or ''}}|
        <b>Upazila:</b> {{$upazilaName or ''}}|
        <b>Union:</b> {{$unionName or ''}}|
        <b>Starting Date:</b> {{$_REQUEST['starting_date'] or ''}}|
        <b>Ending Date:</b> {{$_REQUEST['ending_date'] or ''}}
      </p>

  @endif

<table class="table table-bordered table-hover" id="example0" border="1">

    <tr>
        <td>Description</td>
        <td>Balance</td>
        <td>Approved Budget(A)</td>
        <td>Current Income(B)</td>
        <td>Current Expenses(C)</td>
        <td>Cumulative Income(D)</td>
        <td>Cumulative Expenses(E</td>
        <td>Budget Balance(F=A-E)</td>
        <td>Demand</td>
    </tr>

    <tr><td style="text-align:right">Opening Balance</td><td style="text-align:right">{{ sprintf("%.1lf", $data->total) }}</td><td colspan="8" >&nbsp;</td></tr>
    <tr><td style="text-align:right">Cash in Hand</td><td style="text-align:right">{{ sprintf("%.1lf", $data->opening_cash) }}</td><td colspan="8" >&nbsp;</td></tr>
    <tr><td style="text-align:right">Cash at Bank</td><td style="text-align:right">{{sprintf("%.1lf",  $data->opening_bank) }}</td><td colspan="8" >&nbsp;</td></tr>
    <?php
        $gbudget=0;
        $gcurrentIncome=0;
        $gcurrentExpenditure=0;
        $gcomulativeIncome=0;
        $gcomulativeBudget=0;
        $gcomulativeExpenditure=0;
        $gdemand=0;

        $up = App\Model\Upazila::with('unions')->find(auth()->user()->upid);
        $unIds = [];
        foreach($up->unions as $un) $unIds[] = $un->id;
    ?>

    @foreach($d as $list)

    <?php

        $budget=0;
        $currentIncome=0;
        $currentExpenditure=0;
        $comulativeIncome=0;
        $comulativeBudget=0;
        $comulativeExpenditure=0;
        $demand=0;

    ?>
    <tr>
        <td colspan="9" style="text-align:left">{{ $list["headname"]}}</td>
    </tr>
    @if (count($list["subhead"]) > 0)
    @foreach($list["subhead"] as $sublist)
    <tr>
        <td colspan="9" style="text-align:left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $sublist["sname"]}}</td>
    </tr>
    @if (count($sublist["sub_item"]) > 0)
    @foreach($sublist["sub_item"] as $sub_item)
    <tr>
        <?php

        $budget+=$cbudget=\App\FinanceData::getBudget($sub_item["id"],'ubid',$unIds);

        $currentIncome+=$ccurrentIncome=\App\FinanceData::currentIncome(
            $sub_item["id"],
            $old['starting_date'],
            $old['ending_date'],
            'upid',
            auth()->user()->upid);
        $currentExpenditure+=$ccurrentExpenditure=\App\FinanceData::currentExpenditure(
            $sub_item["id"],
            $old['starting_date'],
            $old['ending_date'],
            'upid',
            auth()->user()->upid);
        $comulativeIncome+=$ccomulativeIncome=\App\FinanceData::comulativeIncome(
            $sub_item["id"],
            $old['ending_date'],
            'upid',
            auth()->user()->upid);
        $comulativeExpenditure+=$ccomulativeExpenditure=\App\FinanceData::comulativeExpenditure(
            $sub_item["id"],
            $old['ending_date'],
            'upid',
            auth()->user()->upid);
        $comulativeBudget+= $ccomulativeBudget=\App\FinanceData::comulativeBudget();
        $demand+=@$cdemand= \App\FinanceData::demand(
            $sub_item["id"],
            'upid',
            auth()->user()->upid);
            ?>


            <td style="text-align:left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ $sub_item["itemname"]}}</td>
            <td>&nbsp;</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $cbudget ) }}</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $ccurrentIncome ) }}</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $ccurrentExpenditure ) }}</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $ccomulativeIncome ) }}</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $ccomulativeExpenditure ) }}</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $ccomulativeBudget ) }}</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $cdemand ) }}</td>
        </tr>
        @endforeach
        @endif
        @endforeach
        <tr>
            <td style="text-align:right">Sub Total</td>
            <td>&nbsp;</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $budget) }}</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $currentIncome) }}</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $currentExpenditure) }}</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $comulativeIncome) }}</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $comulativeExpenditure )}}</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $comulativeBudget) }}</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $demand) }}</td>
        </tr>
        <?php
        $gbudget+=$budget;
        $gcurrentIncome+=$currentIncome;
        $gcurrentExpenditure+=$currentExpenditure;
        $gcomulativeIncome+=$comulativeIncome;
        $gcomulativeExpenditure+=$comulativeExpenditure;
        $gcomulativeBudget+=$comulativeBudget;
        $gdemand+=$demand+=$cdemand;
        ?>
        @endif
        @endforeach
        <tr>
            <td style="text-align:right">Grand Total</td>
            <td>&nbsp;</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $gbudget) }}</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $gcurrentIncome) }}</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $gcurrentExpenditure) }}</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $gcomulativeIncome) }}</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $gcomulativeExpenditure )}}</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $gcomulativeBudget) }}</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $gdemand) }}</td>
        </tr>
        <tr>
            <td style="text-align:right">Clossing Balance</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $data2->total)  }}</td>
            <td colspan="8" >&nbsp;</td>
        </tr>
        <tr>
            <td style="text-align:right">Cash in Hand</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $data2->opening_cash) }}</td>
            <td colspan="8">&nbsp;</td>
        </tr>
        <tr>
            <td style="text-align:right">Cash at Bank</td>
            <td style="text-align:right">{{ sprintf("%.1lf", $data2->opening_bank) }}</td>
            <td colspan="8">&nbsp;</td>
        </tr>

    </table>
    @endsection