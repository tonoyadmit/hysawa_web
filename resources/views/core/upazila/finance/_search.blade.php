<div class="col-md-12" style="margin-top: 10px;">
    <div class="portlet box blue">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i>Report <small> Upazila Report</small> </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body form">
          <form action="{{route('upazila-admin.finance.report.index')}}" class="form-horizontal" method="GET" id="createForm">

              <input type="hidden" name="query" value="{{time()}}" />

              <div class="form-body">
                  <div class="row">


                    <div class="col-md-3">
                      <div class="form-group">
                        <label class="control-label col-md-3">Union</label>
                        <div class="col-md-9">
                          <select name="union_id" class="form-control input-sm" id="union_id" required="required">

                            <option value="all"
                              @if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] == "all")
                                selected="selected"
                              @endif
                            >All</option>

                            <?php
                              $unions = App\Model\Union::where('upid', auth()->user()->upid)->get();
                            ?>
                            @foreach($unions as $union)
                              <option value="{{$union->id}}"
                              @if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] == $union->id)
                                selected="selected"
                              @endif
                              >{{$union->unname}}</option>
                            @endforeach
                          </select>
                        </div>
                      </div>
                    </div>



                      <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-6">Starting Date</label>
                              <div class="col-md-6">
                                  <input
                                    type="text"
                                    name="starting_date"
                                    class="form-control date-picker"
                                    data-date-format = "yyyy-mm-dd"


                                    @if(count($old) && $old['starting_date'] != "")
                                      value="{{$old['starting_date']}}"
                                    @endif
                                    required="required"
                                  />
                                  <p>YYYY-MM-DD</p>
                              </div>
                          </div>
                      </div>

                      <div class="col-md-3">
                          <div class="form-group">
                              <label class="control-label col-md-6">Ending Date</label>
                              <div class="col-md-6">
                                  <input
                                    type="text"
                                    name="ending_date"
                                    class="form-control date-picker"
                                    data-date-format = "yyyy-mm-dd"

                                    @if(count($old) && $old['ending_date'] != "")
                                      value="{{$old['ending_date']}}"
                                    @endif
                                    required="required"
                                  />
                                  <p>YYYY-MM-DD</p>
                              </div>
                          </div>
                      </div>

                  </div>


                  <div class="row">
                    <div class="form-group">
                        <div class="col-md-offset-1 col-md-12">
                            <input type="submit" class="btn green" value="Get Data" />
                            <a href="{{route('upazila-admin.finance.report.index')}}" class="btn btn-info">Clear</a>
                            @if(isset($_REQUEST['query']))
                              <a href="{{route('upazila-admin.finance.report.print', $old)}}" class="btn btn-success">Print</a>
                            @endif
                        </div>
                    </div>
                  </div>
              </div>
          </form>


           @if(isset($_REQUEST['query']) && $_REQUEST['query'] != "")

          <div class="row">
            <div class="col-md-12">

              <p>Query:
              <?php
                $regionName = App\Model\Region::find(auth()->user()->region_id)->region_name;
                $districtName = App\Model\District::find(auth()->user()->distid)->distname;
                $upazilaName = App\Model\Upazila::find(auth()->user()->upid)->upname;
                $unionName = $_REQUEST['union_id'];

                if($unionName != "" && $unionName != "all")
                {
                  $unionName = App\Model\Union::find($_REQUEST['union_id'])->unname;
                }

              ?>

                <b>Region: </b> {{$regionName or ''}}|
                <b>District:</b> {{$districtName or ''}}|
                <b>Upazila:</b> {{$upazilaName or ''}}|
                <b>Union:</b> {{$unionName or ''}}|
                <b>Starting Date:</b> {{$_REQUEST['starting_date']}}|
                <b>Ending Date:</b> {{$_REQUEST['ending_date']}}
              </p>

            </div>
          </div>
          @endif


      </div>
    </div>
  </div>