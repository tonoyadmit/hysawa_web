@extends('layouts.print.app')

@section('my_style')
<style>
    table{
      border-collapse: collapse;
      border-spacing: 0;
    }

    div{
      font-size: 80%;
      display: inline-block;
     /* overflow: visible;

      width: 2.4em;
      line-height: 0.9em;
      margin-top: 9em;

      white-space: nowrap;
      -ms-transform: rotate(270deg);
      -o-transform: rotate(270deg);
      -webkit-transform: rotate(270deg);
      transform: rotate(270deg);
      text-align: left;*/
    }

    .headline th {
      padding: 5px 20px;
    }
  </style>
@endsection

@section('content')



<?php
  $unionName = "";
  if($_REQUEST['union_id'] != "" ){
    if($_REQUEST['union_id'] != "all")
    {
      $union = App\Model\Union::find($_REQUEST['union_id']);
      if(count($union)){
        $unionName = $union->unname;
      }
    }elseif($_REQUEST['union_id'] == "all"){
      $unionName = "all";
    }
  }
?>



@if(isset($_REQUEST['query']))
  <p>Query:
    <strong>Union</strong>:{{ $unionName }}&nbsp;|
    <strong>App Status</strong>: {{$_REQUEST['app_status'] or ''}}&nbsp;|
    <strong>Implement Status</strong>: {{$_REQUEST['imp_status'] or ''}}&nbsp;|
    <strong>Approval Date</strong>: {{$_REQUEST['starting_date'] or ''}}&nbsp;
  </p>
@endif


<h2>Site list of {{count($waters)}} water points</h2>

<table class="box" border="1">
    <thead class="rotate">

      <th><div>District</div></th>
      <th><div>Upazila</div></th>
      <th><div>Union</div></th>

      <th><div>Ward No</div></th>
      <th><div>CDF No</div></th>
      <th><div>Village</div></th>
      <th><div>TW No</div></th>
      <th><div>Technology</div></th>
      <th><div>Landowner</div></th>
      <th><div>Caretaker Male</div></th>
      <th><div>Care Female</div></th>
      <th><div>Total HH</div></th>
      <th><div>Harccode HH</div></th>
    </thead>
    <tbody>
      @foreach($waters as $water)
      <tr>
        <td><div>@if(!empty($water->district)) {{$water->district->distname}} @endif</div></td>
        <td><div>@if(!empty($water->upazila)) {{$water->upazila->upname}} @endif</div></td>
        <td><div>@if(!empty($water->union)) {{$water->union->unname}} @endif</div></td>

        <td><div>{{$water->Ward_no}}</div></td>
        <td><div>{{$water->CDF_no}}</div></td>
        <td><div>{{$water->Village}}</div></td>
        <td><div>{{$water->TW_No}}</div></td>
        <td><div>{{$water->Technology_Type}}</div></td>

        <td><div>{{$water->Landowner}}</div></td>
        <td><div>{{$water->Caretaker_male}}</div></td>
        <td><div>{{$water->Caretaker_female}}</div></td>
        <td><div>{{$water->HH_benefited}}</div></td>
        <td><div>{{$water->HCHH_benefited}}</div></td>
      </tr>
      @endforeach
    </tbody>
  </table>
@endsection