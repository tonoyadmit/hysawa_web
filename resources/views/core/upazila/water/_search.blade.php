<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> Water <small> view</small>
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body form">

        <style type="text/css">
            .form-group{
                margin-bottom: 5px;
            }
            .control-label{
                font-size: 10px;
            }
        </style>

        <form action="{{route('upazila-admin.water.index')}}" class="form-horizontal" method="GET" id="searchForm">
            <input type="hidden" name="query" value="{{time()}}" />

            <div class="form-body">

                <div class="row">

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Union</label>
                            <div class="col-md-9">
                                <select class="form-control" name="union_id" id="union_id">

                                <?php
                                    $funions = \DB::table('funion')
                                        ->where('upid', \Auth::user()->upid)
                                        ->where('proid', \Auth::user()->proj_id)
                                        ->get();
                                ?>

                                <option value="all"
                                @if(isset($old['union_id']) && $old['union_id'] == "all") selected="selected" @endif
                                >All</option>
                                @foreach($funions as $funion)
                                    <option value="{{$funion->id}}"
                                        @if($old['union_id'] == $funion->id)
                                        selected="selected"
                                        @endif
                                        >{{$funion->unname}}</option>
                                @endforeach

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">App Status</label>
                            <div class="col-md-9">
                                <select name="app_status" class="form-control">
                                    <option value="">Choose one</option>
                                    <?php
                                        $appStatus = \DB::table('app_status')->orderBy('app_status')->get();
                                    ?>
                                    @foreach($appStatus as $status)
                                        <option value="{{$status->app_status}}" @if($old['app_status'] == $status->app_status) selected="selected" @endif >{{$status->app_status}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Imp Status</label>
                            <div class="col-md-9">
                                <select name="imp_status" class="form-control">
                                    <option value="" >Choose one</option>
                                    <?php
                                        $impStatus = \DB::table('imp_status')->orderBy('imp_status')->get();
                                    ?>
                                    @foreach($impStatus as $status)
                                        <option value="{{$status->imp_status}}"
                                        @if($old['imp_status'] == $status->imp_status)
                                            selected="selected"
                                        @endif >{{$status->imp_status}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>



                </div>

                <div class="row">

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Approval Date(YYY-MM-DD)</label>
                            <div class="col-md-9">
                                <input
                                    class="form-control date-picker"
                                    type="text"
                                    name="starting_date"
                                    data-date-format = "yyyy-mm-dd"

                                @if(!empty( $old['starting_date']))
                                value="{{$old['starting_date']}}"
                                @endif >

                            </div>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-12 pull-left" style="margin-top: 4px;">

                                <input type="submit" class="btn green btn-sm" value="Search" />
                                <a href="{{route('upazila-admin.water.index')}}" class="btn default btn-sm">Clear</a>

                                @if(isset($_REQUEST['query']))
                                <?php
                                    $old2 = $old;
                                    $old2['download'] = 'download';
                                    $old3 = $old;
                                    $old3['print'] = 'print';
                                ?>
                                <a href="{{route('upazila-admin.water.index', $old2)}}" class="btn blue btn-sm">Download</a>
                                <a href="{{route('upazila-admin.water.index', $old3)}}" class="btn default btn-sm">Print</a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>