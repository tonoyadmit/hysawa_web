@if(count($waters) > 0)
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <div class="portlet-title">
      <div class="caption">

        Water Report



      </div>
    </div>

    <div class="portlet-body util-btn-margin-bottom-5">
      @if(isset($_REQUEST['query']))
          <p>Query:
            <?php
              $unionName = "";
              if($_REQUEST['union_id'] != "" ){

                  if($_REQUEST['union_id'] != "all")
                  {
                    $union = App\Model\Union::find($_REQUEST['union_id']);
                    if(count($union)){
                      $unionName = $union->unname;
                    }

                  }elseif($_REQUEST['union_id'] == "all")
                  {
                    $unionName = "all";
                  }
              }
            ?>

            <strong>Union</strong>:{{ $unionName }}&nbsp;|
            <strong>App Status</strong>: {{$_REQUEST['app_status'] or ''}}&nbsp;|
            <strong>Implement Status</strong>: {{$_REQUEST['imp_status'] or ''}}&nbsp;|
            <strong>Approval Date</strong>: {{$_REQUEST['starting_date'] or ''}}&nbsp;
          </p>
        @endif
      <h4 class="text-red">If no site list is displayed, ask UP to enter TW site list</h4>

      <div class="table-responsive">
        <table class="table table-bordered table-hover" id="example0">

          <thead class="flip-content">


            <th style="font-size: 12px;">Action</th>
            <th style="font-size: 12px;" nowrap="nowrap">App Date</th>
            <th style="font-size: 12px;" >App. Status</th>
            <th style="font-size: 12px;" >Imp. Status</th>

            <th style="font-size: 12px;" >Ward no</th>
            <th style="font-size: 12px;" >CDF No</th>


            <th style="font-size: 12px;" >District</th>
            <th style="font-size: 12px;" >Upazila</th>
            <th style="font-size: 12px;" >Union</th>

            <th style="font-size: 12px;" >Village</th>
            <th style="font-size: 12px;" >TW No</th>
            <th style="font-size: 12px;" >Technology</th>
            <th style="font-size: 12px;" >Landowner</th>
            <th style="font-size: 12px;" >Caretaker Male</th>
            <th style="font-size: 12px;" >Care Female</th>
            <th style="font-size: 12px;" >Total HH</th>
            <th style="font-size: 12px;" >Hardcode HH</th>




          </thead>

          <tbody>
            @foreach($waters as $water)
            <tr>

              <td>
                <a class="label label-success" href="{{route('upazila-admin.water.edit', $water->id)}}">
                  <i class="fa fa-pencil"></i>
                </a>
              </td>

              <td style="font-size: 12px;" nowrap="nowrap">{{$water->App_date}}</td>
              <td style="font-size: 12px;" ><span class="label label-sm label-info"> {{$water->app_status}}</span></td>
              <td style="font-size: 12px;" ><span class="label label-sm label-success"> {{$water->imp_status}} </span></td>

              <td style="font-size: 12px;" >{{$water->Ward_no}}</td>
              <td style="font-size: 12px;" >{{$water->CDF_no}}</td>

              <td style="font-size: 12px;" >{{$water->district->distname or ''}}</td>
              <td style="font-size: 12px;" >{{$water->upazila->upname or ''}}</td>
              <td style="font-size: 12px;" >{{$water->union->unname or ''}}</td>


              <td style="font-size: 12px;" >{{$water->Village}}</td>
              <td style="font-size: 12px;" >{{$water->Village}}</td>


              <td style="font-size: 12px;" >{{$water->Village}}</td>
              <td style="font-size: 12px;" >{{$water->TW_No}}</td>
              <td style="font-size: 12px;" >{{$water->Technology_Type}}</td>
              <td style="font-size: 12px;" >{{$water->Landowner}}</td>
              <td style="font-size: 12px;" >{{$water->Caretaker_male}}</td>
              <td style="font-size: 12px;" >{{$water->Caretaker_female}}</td>
              <td style="font-size: 12px;" >{{$water->HH_benefited}}</td>
              <td style="font-size: 12px;" >{{$water->HCHH_benefited}}</td>

            </tr>
            @endforeach
          </tbody>
        </table>

        <div class="pagination pull-right">
          {{$waters->appends($old)->links()}}
        </div>

      </div>
    </div>
  </div>
</div>

@else
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <p>
      No Data Found
    </p>
  </div>
</div>
@endif