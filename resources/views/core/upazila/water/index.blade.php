@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('upazila-admin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Water</span>
      </li>
    </ul>
  </div>

  <div class="col-md-12" style="margin-top: 10px;">
  @include('core.upazila.water._search')
  </div>
  @include('core.upazila.water._table')

@endsection

@section('my_js')

@endsection