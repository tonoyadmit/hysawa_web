@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('upazila-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="{{ route('upazila-admin.water.index') }}">Waters</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Update</span>
    </li>
  </ul>
</div>

<h1 class="page-title"> Waters
  <small>create new</small>
</h1>


<div class="portlet box blue">
  <div class="portlet-title">
    <div class="caption">
      <i class="fa fa-gift"></i>Update Data</div>
      <div class="tools">
        <a href="javascript:;" class="collapse"> </a>
      </div>
    </div>

    <div class="portlet-body form">
      <form action="{{route('upazila-admin.water.update', $water->id)}}" class="form-horizontal" method="POST" id="updateForm">
        {{csrf_field()}}

        <div class="form-body">
          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-6">Ward _no: (1 digit)</label>
                <div class="col-md-6">
                  <label class="control-label"><b>{{$water->Ward_no}}</b></label>
                </div>
              </div>
            </div>
          </div>


          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-6">Village:</label>
                <div class="col-md-6">
                  <label class="control-label"><b>{{$water->Village}}</b></label>
                </div>

              </div>
            </div>
          </div>


          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-6">TW_No: (6 digit upcode + tw sl)</label>
                <div class="col-md-6">
                  <label class="control-label"><b>{{$water->TW_No}}</b></label>
                </div>

              </div>
            </div>

          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-6">Land Owner name:</label>
                <div class="col-md-6">
                  <label class="control-label"><b>{{$water->Landowner}}</b></label>
                </div>
              </div>
            </div>
          </div>


          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-6">Caretaker(male) name:</label>
                <div class="col-md-6">
                  <label class="control-label"><b>{{$water->Caretaker_male}}</b></label>
                </div>

              </div>
            </div>

          </div>


          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-6">Caretaker (female) name:</label>
                <div class="col-md-6">
                  <label class="control-label"><b>{{$water->Caretaker_female}}</b></label>
                </div>

              </div>
            </div>

          </div>


          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-6">CDF_no: (3 digits)</label>
                <div class="col-md-6">
                  <label class="control-label"><b>{{$water->CDF_no}}</b></label>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6">
              <div class="form-group">
                <label class="control-label col-md-6">Approval Status</label>
                <div class="col-md-6">


                  <select name="app_status" class="form-control">
                    <option value="">Choose one</option>
                    <?php
                      $appStatus = \DB::table('app_status')
                        //->where('app_status', '!=', 'Approved')
                        //->where('app_status', '!=', 'Submitted')
                        ->orderBy('app_status')
                        ->get();
                    ?>
                    @foreach($appStatus as $status)
                    <option value="{{$status->app_status}}"
                        @if($water->app_status == $status->app_status)
                          selected="selected"
                        @endif
                    >{{$status->app_status}}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            </div>
          </div>

          <div class="row">
            <div class="col-md-12">
              <div class="form-group">
                <label class="control-label col-md-3">Remarks</label>
                <div class="col-md-9">
                  <textarea class="form-control" name="remarks">{{$water->remarks}}</textarea>
                </div>
              </div>
            </div>
          </div>

        </div>
        <div class="form-actions">
          <div class="col-md-offset-2 col-md-10">
            <input type="submit" class="btn green" value="Update" />
          </div>
        </div>
      </form>
    </div>
  </div>

</div>
@endsection
