<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> Monthly Report <small> view</small> </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body form">
        <form action="{{route('upazila-admin.periods.show')}}" class="form-horizontal" method="GET" id="searchForm">

            <input type="hidden" name="query" value="{{time()}}" />

            <div class="form-body">
                <div class="row">

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Union </label>
                            <div class="col-md-9">
                                <select class="form-control" name="union_id" required="required" id="union_id">
                                    <option value="all" @if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] == "all") selected="selected" @endif >All</option>
                                    <?php
                                        $unions = App\Model\Union::where('proid', auth()->user()->proj_id)->where('upid', auth()->user()->upid)->get();
                                    ?>
                                    @foreach($unions as $union )
                                        <option value="{{$union->id}}"

                                            @if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] == $union->id)
                                                selected="selected"
                                            @endif

                                        >{{$union->unname}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Reporting Period</label>
                            <div class="col-md-9">
                                <select class="form-control" name="rep_data_id" required="required" id="rep_data_id">
                                    <option value="all" selected="selected">ALL</option>
                                    <?php
                                        $periods = \DB::table('rep_period')->where('id', '!=', 99)->where('id', '!=', 100)->orderBy('id', 'DESC')->get();
                                    ?>
                                    @foreach($periods as $period)
                                        <option value="{{$period->id}}"

                                        @if(isset($_REQUEST['rep_data_id']) && $_REQUEST['rep_data_id'] == $period->id)
                                            selected="selected"
                                        @endif
                                        >{{$period->period}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Type</label>
                            <div class="col-md-9">
                                <select name="report_type" class="form-control" required="required">
                                    <option value="html" selected="selected">Generate Report</option>
                                    <option value="print">Print</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-5">
                                <input type="submit" class="btn green" value="Search" />
                                <a href="{{route('upazila-admin.periods.index')}}" class="btn default">Clear</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </form>

    </div>
</div>