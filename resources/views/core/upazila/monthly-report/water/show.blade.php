@extends('layouts.appinside')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('district-admin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <a href="{{ route('district-admin.periods.index') }}">Monthly Report</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Insert</span>
        </li>
    </ul>
</div>

<div class="row">
    @include('partials.errors')

    <div class="col-md-12" style="margin-top: 10px;">
      @include('core.upazila.monthly-report.water._search')
    </div>

    <div class="col-md-12">
        {!! $reportHeader !!}
        <table class="table table-bordered" style="background:#F5F5F5">
            <thead>
                <tr>
                    <th>Code</th>
                    <th>5422</th>
                    <th>Baseline</th>
                    <th>Target</th>
                    <th>Progress untill last month</th>
                    <th>Progress this month</th>
                    <th>Total progress</th>
                </tr>
            </thead>
            <tbody>
      <tr valign="baseline">
        <td align="right"><div align="center">02</div></td>
        <td>Number of CDF:</td>
        <td><div align="right">{{$row_getbaseline['cdf_no']}}</div></td>
        <td><div align="right">{{$row_gettargets['cdf_no']}}</div></td>
        <td><div align="right">{{$row_lastdata['cdf_no']}}</div></td>
        <td><div align="right">{{$ReportData['cdf_no']}}</div></td>
        <td><div align="right">{{$row_totaldata['cdf_no']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td align="right"><div align="center"></div></td>
        <td>Poulation under CDF:</td>
        <td><div align="right">{{$row_getbaseline['cdf_pop']}}</div></td>
        <td><div align="right">{{$row_gettargets['cdf_pop']}}</div></td>
        <td><div align="right">{{$row_lastdata['cdf_pop']}}</div></td>
        <td><div align="right">{{$ReportData['cdf_pop']}}</div></td>
        <td><div align="right">{{$row_totaldata['cdf_pop']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td align="right"><div align="center">03</div></td>
        <td>Male population:</td>
        <td><div align="right">{{$row_getbaseline['cdf_male']}}</div></td>
        <td><div align="right">{{$row_gettargets['cdf_male']}}</div></td>
        <td><div align="right">{{$row_lastdata['cdf_male']}}</div></td>
        <td><div align="right">{{$ReportData['cdf_male']}}</div></td>
        <td><div align="right">{{$row_totaldata['cdf_male']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td align="right"><div align="center">04</div></td>
        <td>Female population:</td>
        <td><div align="right">{{$row_getbaseline['cdf_female']}}</div></td>
        <td><div align="right">{{$row_gettargets['cdf_female']}}</div></td>
        <td><div align="right">{{$row_lastdata['cdf_female']}}</div></td>
        <td><div align="right">{{$ReportData['cdf_female']}}</div></td>
        <td><div align="right">{{$row_totaldata['cdf_female']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right"><div align="center"></div></td>
        <td>Hardcode population:</td>
        <td><div align="right">{{$row_getbaseline['cdf_pop_hc']}}</div></td>
        <td><div align="right">{{$row_gettargets['cdf_pop_hc']}}</div></td>
        <td><div align="right">{{$row_lastdata['cdf_pop_hc']}}</div></td>
        <td><div align="right">{{$ReportData['cdf_pop_hc']}}</div></td>
        <td><div align="right">{{$row_totaldata['cdf_pop_hc']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right"><div align="center">101</div></td>
        <td>Number of household</td>
        <td><div align="right">{{$row_getbaseline['cdf_hh']}}</div></td>
        <td><div align="right">{{$row_gettargets['cdf_hh']}}</div></td>
        <td><div align="right">{{$row_lastdata['cdf_hh']}}</div></td>
        <td><div align="right">{{$ReportData['cdf_hh']}}</div></td>
        <td><div align="right">{{$row_totaldata['cdf_hh']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right"><div align="center">102</div></td>
        <td>Number of hardcore household</td>
        <td><div align="right">{{$row_getbaseline['cdf_hh_hc']}}</div></td>
        <td><div align="right">{{$row_gettargets['cdf_hh_hc']}}</div></td>
        <td><div align="right">{{$row_lastdata['cdf_hh_hc']}}</div></td>
        <td><div align="right">{{$ReportData['cdf_hh_hc']}}</div></td>
        <td><div align="right">{{$row_totaldata['cdf_hh_hc']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right"><div align="center">103</div></td>
        <td>Number of disable people</td>
        <td><div align="right">{{$row_getbaseline['cdf_pop_disb']}}</div></td>
        <td><div align="right">{{$row_gettargets['cdf_pop_disb']}}</div></td>
        <td><div align="right">{{$row_lastdata['cdf_pop_disb']}}</div></td>
        <td><div align="right">{{$ReportData['cdf_pop_disb']}}</div></td>
        <td><div align="right">{{$row_totaldata['cdf_pop_disb']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right"><div align="center"></div></td>
        <td>Number of people under social safetynet</td>
        <td><div align="right">{{$row_getbaseline['cdf_pop_safety']}}</div></td>
        <td><div align="right">{{$row_gettargets['cdf_pop_safety']}}</div></td>
        <td><div align="right">{{$row_lastdata['cdf_pop_safety']}}</div></td>
        <td><div align="right">{{$ReportData['cdf_pop_safety']}}</div></td>
        <td><div align="right">{{$row_totaldata['cdf_pop_safety']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of Community Facilitators identified</td>
        <td><div align="right">{{$row_getbaseline['cdf_cf_tot']}}</div></td>
        <td><div align="right">{{$row_gettargets['cdf_cf_tot']}}</div></td>
        <td><div align="right">{{$row_lastdata['cdf_cf_tot']}}</div></td>
        <td><div align="right">{{$ReportData['cdf_cf_tot']}}</div></td>
        <td><div align="right">{{$row_totaldata['cdf_cf_tot']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of MALE Community Facilitators identified</td>
        <td><div align="right">{{$row_getbaseline['cdf_cf_male']}}</div></td>
        <td><div align="right">{{$row_gettargets['cdf_cf_male']}}</div></td>
        <td><div align="right">{{$row_lastdata['cdf_cf_male']}}</div></td>
        <td><div align="right">{{$ReportData['cdf_cf_male']}}</div></td>
        <td><div align="right">{{$row_totaldata['cdf_cf_male']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of FEMALE Community Facilitators identified</td>
        <td><div align="right">{{$row_getbaseline['cdf_cf_female']}}</div></td>
        <td><div align="right">{{$row_gettargets['cdf_cf_female']}}</div></td>
        <td><div align="right">{{$row_lastdata['cdf_cf_female']}}</div></td>
        <td><div align="right">{{$ReportData['cdf_cf_female']}}</div></td>
        <td><div align="right">{{$row_totaldata['cdf_cf_female']}}</div></td>
      </tr>
      <tr valign="baseline" class="header">
        <td colspan="7" align="left" >UP Management Information</td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of PNGO/Project staff recruited </td>
        <td><div align="right">{{$row_getbaseline['up_stf_tot']}}</div></td>
        <td><div align="right">{{$row_gettargets['up_stf_tot']}}</div></td>
        <td><div align="right">{{$row_lastdata['up_stf_tot']}}</div></td>
        <td><div align="right">{{$ReportData['up_stf_tot']}}</div></td>
        <td><div align="right">{{$row_totaldata['up_stf_tot']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of Male PNGO/Project staff recruited </td>
        <td><div align="right">{{$row_getbaseline['up_stf_male']}}</div></td>
        <td><div align="right">{{$row_gettargets['up_stf_male']}}</div></td>
        <td><div align="right">{{$row_lastdata['up_stf_male']}}</div></td>
        <td><div align="right">{{$ReportData['up_stf_male']}}</div></td>
        <td><div align="right">{{$row_totaldata['up_stf_male']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of Female PNGO/Project staff recruited </td>
        <td><div align="right">{{$row_getbaseline['up_stf_female']}}</div></td>
        <td><div align="right">{{$row_gettargets['up_stf_female']}}</div></td>
        <td><div align="right">{{$row_lastdata['up_stf_female']}}</div></td>
        <td><div align="right">{{$ReportData['up_stf_female']}}</div></td>
        <td><div align="right">{{$row_totaldata['up_stf_female']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of PNGO engaged by UP</td>
        <td><div align="right">{{$row_getbaseline['up_pngo']}}</div></td>
        <td><div align="right">{{$row_gettargets['up_pngo']}}</div></td>
        <td><div align="right">{{$row_lastdata['up_pngo']}}</div></td>
        <td><div align="right">{{$ReportData['up_pngo']}}</div></td>
        <td><div align="right">{{$row_totaldata['up_pngo']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of hardware contratctor engaged by UP</td>
        <td><div align="right">{{$row_getbaseline['up_cont']}}</div></td>
        <td><div align="right">{{$row_gettargets['up_cont']}}</div></td>
        <td><div align="right">{{$row_lastdata['up_cont']}}</div></td>
        <td><div align="right">{{$ReportData['up_cont']}}</div></td>
        <td><div align="right">{{$row_totaldata['up_cont']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Public disclosure board Established (Yes=1, No = 0)</td>
        <td><div align="right"></div></td>
        <td><div align="right"></div></td>
        <td><div align="right"></div></td>
        <td><div align="right"></div></td>
        <td><div align="right"></div></td>
      </tr>
      <tr valign="baseline" class="header">
        <td colspan="7" align="left" >CDF HYGIENE Information</td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of handwashing sessions</td>
        <td><div align="right">{{$row_getbaseline['CHY_hw_ses']}}</div></td>
        <td><div align="right">{{$row_gettargets['CHY_hw_ses']}}</div></td>
        <td><div align="right">{{$row_lastdata['CHY_hw_ses']}}</div></td>
        <td><div align="right">{{$ReportData['CHY_hw_ses']}}</div></td>
        <td><div align="right">{{$row_totaldata['CHY_hw_ses']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of MALE participated in handwashing sessions</td>
        <td><div align="right">{{$row_getbaseline['CHY_hw_male']}}</div></td>
        <td><div align="right">{{$row_gettargets['CHY_hw_male']}}</div></td>
        <td><div align="right">{{$row_lastdata['CHY_hw_male']}}</div></td>
        <td><div align="right">{{$ReportData['CHY_hw_male']}}</div></td>
        <td><div align="right">{{$row_totaldata['CHY_hw_male']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of FEMALE participated in handwashing sessions</td>
        <td><div align="right">{{$row_getbaseline['CHY_hw_female']}}</div></td>
        <td><div align="right">{{$row_gettargets['CHY_hw_female']}}</div></td>
        <td><div align="right">{{$row_lastdata['CHY_hw_female']}}</div></td>
        <td><div align="right">{{$ReportData['CHY_hw_female']}}</div></td>
        <td><div align="right">{{$row_totaldata['CHY_hw_female']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of menstrual hygiene sessions</td>
        <td><div align="right">{{$row_getbaseline['CHY_mn_ses']}}</div></td>
        <td><div align="right">{{$row_gettargets['CHY_mn_ses']}}</div></td>
        <td><div align="right">{{$row_lastdata['CHY_mn_ses']}}</div></td>
        <td><div align="right">{{$ReportData['CHY_mn_ses']}}</div></td>
        <td><div align="right">{{$row_totaldata['CHY_mn_ses']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of FEMALE participated inmenstrual hygiene sessions</td>
        <td><div align="right">{{$row_getbaseline['CHY_mn_female']}}</div></td>
        <td><div align="right">{{$row_gettargets['CHY_mn_female']}}</div></td>
        <td><div align="right">{{$row_lastdata['CHY_mn_female']}}</div></td>
        <td><div align="right">{{$ReportData['CHY_mn_female']}}</div></td>
        <td><div align="right">{{$row_totaldata['CHY_mn_female']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of hygienic latrine sessions</td>
        <td><div align="right">{{$row_getbaseline['CHY_sa_ses']}}</div></td>
        <td><div align="right">{{$row_gettargets['CHY_sa_ses']}}</div></td>
        <td><div align="right">{{$row_lastdata['CHY_sa_ses']}}</div></td>
        <td><div align="right">{{$ReportData['CHY_sa_ses']}}</div></td>
        <td><div align="right">{{$row_totaldata['CHY_sa_ses']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of household participated in hygienic latrine sessions</td>
        <td><div align="right">{{$row_getbaseline['CHY_sa_hh']}}</div></td>
        <td><div align="right">{{$row_gettargets['CHY_sa_hh']}}</div></td>
        <td><div align="right">{{$row_lastdata['CHY_sa_hh']}}</div></td>
        <td><div align="right">{{$ReportData['CHY_sa_hh']}}</div></td>
        <td><div align="right">{{$row_totaldata['CHY_sa_hh']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of Food hygiene sessions</td>
        <td><div align="right">{{$row_getbaseline['CHY_fh_ses']}}</div></td>
        <td><div align="right">{{$row_gettargets['CHY_fh_ses']}}</div></td>
        <td><div align="right">{{$row_lastdata['CHY_fh_ses']}}</div></td>
        <td><div align="right">{{$ReportData['CHY_fh_ses']}}</div></td>
        <td><div align="right">{{$row_totaldata['CHY_fh_ses']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of household participated in Food hygiene sessions</td>
        <td><div align="right">{{$row_getbaseline['CHY_fh_hh']}}</div></td>
        <td><div align="right">{{$row_gettargets['CHY_fh_hh']}}</div></td>
        <td><div align="right">{{$row_lastdata['CHY_fh_hh']}}</div></td>
        <td><div align="right">{{$ReportData['CHY_fh_hh']}}</div></td>
        <td><div align="right">{{$row_totaldata['CHY_fh_hh']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of household attended TW Maintenance sessions</td>
        <td><div align="right">{{$row_getbaseline['TW_maintenance']}}</div></td>
        <td><div align="right">{{$row_gettargets['TW_maintenance']}}</div></td>
        <td><div align="right">{{$row_lastdata['TW_maintenance']}}</div></td>
        <td><div align="right">{{$ReportData['TW_maintenance']}}</div></td>
        <td><div align="right">{{$row_totaldata['TW_maintenance']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of household attended WSP sessions</td>
        <td><div align="right">{{$row_getbaseline['wsp']}}</div></td>
        <td><div align="right">{{$row_gettargets['wsp']}}</div></td>
        <td><div align="right">{{$row_lastdata['wsp']}}</div></td>
        <td><div align="right">{{$ReportData['wsp']}}</div></td>
        <td><div align="right">{{$row_totaldata['wsp']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of new garbage hole build</td>
        <td><div align="right">{{$row_getbaseline['CHY_gb_new']}}</div></td>
        <td><div align="right">{{$row_gettargets['CHY_gb_new']}}</div></td>
        <td><div align="right">{{$row_lastdata['CHY_gb_new']}}</div></td>
        <td><div align="right">{{$ReportData['CHY_gb_new']}}</div></td>
        <td><div align="right">{{$row_totaldata['CHY_gb_new']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of  garbage hole repaired</td>
        <td><div align="right">{{$row_getbaseline['CHY_gb_rep']}}</div></td>
        <td><div align="right">{{$row_gettargets['CHY_gb_rep']}}</div></td>
        <td><div align="right">{{$row_lastdata['CHY_gb_rep']}}</div></td>
        <td><div align="right">{{$ReportData['CHY_gb_rep']}}</div></td>
        <td><div align="right">{{$row_totaldata['CHY_gb_rep']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of DRAMA shows in the community</td>
        <td><div align="right">{{$row_getbaseline['CHY_dr']}}</div></td>
        <td><div align="right">{{$row_gettargets['CHY_dr']}}</div></td>
        <td><div align="right">{{$row_lastdata['CHY_dr']}}</div></td>
        <td><div align="right">{{$ReportData['CHY_dr']}}</div></td>
        <td><div align="right">{{$row_totaldata['CHY_dr']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of people attended in DRAMA shows</td>
        <td><div align="right">{{$row_getbaseline['CHY_dr_pop']}}</div></td>
        <td><div align="right">{{$row_gettargets['CHY_dr_pop']}}</div></td>
        <td><div align="right">{{$row_lastdata['CHY_dr_pop']}}</div></td>
        <td><div align="right">{{$ReportData['CHY_dr_pop']}}</div></td>
        <td><div align="right">{{$row_totaldata['CHY_dr_pop']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of VIDEO shows in the community</td>
        <td><div align="right">{{$row_getbaseline['CHY_vd']}}</div></td>
        <td><div align="right">{{$row_gettargets['CHY_vd']}}</div></td>
        <td><div align="right">{{$row_lastdata['CHY_vd']}}</div></td>
        <td><div align="right">{{$ReportData['CHY_vd']}}</div></td>
        <td><div align="right">{{$row_totaldata['CHY_vd']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of people attended in VIDEO shows</td>
        <td><div align="right">{{$row_getbaseline['CHY_vd_pop']}}</div></td>
        <td><div align="right">{{$row_gettargets['CHY_vd_pop']}}</div></td>
        <td><div align="right">{{$row_lastdata['CHY_vd_pop']}}</div></td>
        <td><div align="right">{{$ReportData['CHY_vd_pop']}}</div></td>
        <td><div align="right">{{$row_totaldata['CHY_vd_pop']}}</div></td>
      </tr>
      <tr valign="baseline" class="header">
        <td colspan="7" align="left" >School HYGIENE Information</td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Total Schools</td>
        <td><div align="right">{{$row_getbaseline['scl_tot']}}</div></td>
        <td><div align="right">{{$row_gettargets['scl_tot']}}</div></td>
        <td><div align="right">{{$row_lastdata['scl_tot']}}</div></td>
         <td><div align="right">{{$ReportData['scl_tot']}}</div></td>
        <td><div align="right">{{$row_totaldata['scl_tot']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Total students</td>
        <td><div align="right">{{$row_getbaseline['scl_tot_std']}}</div></td>
        <td><div align="right">{{$row_gettargets['scl_tot_std']}}</div></td>
        <td><div align="right">{{$row_lastdata['scl_tot_std']}}</div></td>
        <td><div align="right">{{$ReportData['scl_tot_std']}}</div></td>
        <td><div align="right">{{$row_totaldata['scl_tot_std']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Total boys</td>
        <td><div align="right">{{$row_getbaseline['scl_boys']}}</div></td>
        <td><div align="right">{{$row_gettargets['scl_boys']}}</div></td>
        <td><div align="right">{{$row_lastdata['scl_boys']}}</div></td>
        <td><div align="right">{{$ReportData['scl_boys']}}</div></td>
        <td><div align="right">{{$row_totaldata['scl_boys']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Total girls</td>
        <td><div align="right">{{$row_getbaseline['scl_girls']}}</div></td>
        <td><div align="right">{{$row_gettargets['scl_girls']}}</div></td>
        <td><div align="right">{{$row_lastdata['scl_girls']}}</div></td>
        <td><div align="right">{{$ReportData['scl_girls']}}</div></td>
        <td><div align="right">{{$row_totaldata['scl_girls']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Primary schools</td>
        <td><div align="right">{{$row_getbaseline['scl_pri']}}</div></td>
        <td><div align="right">{{$row_gettargets['scl_pri']}}</div></td>
        <td><div align="right">{{$row_lastdata['scl_pri']}}</div></td>
        <td><div align="right">{{$ReportData['scl_pri']}}</div></td>
        <td><div align="right">{{$row_totaldata['scl_pri']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of students in Primary schools</td>
        <td><div align="right">{{$row_getbaseline['scl_pri_std']}}</div></td>
        <td><div align="right">{{$row_gettargets['scl_pri_std']}}</div></td>
        <td><div align="right">{{$row_lastdata['scl_pri_std']}}</div></td>
        <td><div align="right">{{$ReportData['scl_pri_std']}}</div></td>
        <td><div align="right">{{$row_totaldata['scl_pri_std']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>High schools</td>
        <td><div align="right">{{$row_getbaseline['scl_high']}}</div></td>
        <td><div align="right">{{$row_gettargets['scl_high']}}</div></td>
        <td><div align="right">{{$row_lastdata['scl_high']}}</div></td>
        <td><div align="right">{{$ReportData['scl_high']}}</div></td>
        <td><div align="right">{{$row_totaldata['scl_high']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of students in high schools</td>
        <td><div align="right">{{$row_getbaseline['scl_high_std']}}</div></td>
        <td><div align="right">{{$row_gettargets['scl_high_std']}}</div></td>
        <td><div align="right">{{$row_lastdata['scl_high_std']}}</div></td>
        <td><div align="right">{{$ReportData['scl_high_std']}}</div></td>
        <td><div align="right">{{$row_totaldata['scl_high_std']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Total Madrasha</td>
        <td><div align="right">{{$row_getbaseline['scl_mad']}}</div></td>
        <td><div align="right">{{$row_gettargets['scl_mad']}}</div></td>
        <td><div align="right">{{$row_lastdata['scl_mad']}}</div></td>
        <td><div align="right">{{$ReportData['scl_mad']}}</div></td>
        <td><div align="right">{{$row_totaldata['scl_mad']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of students in Madrasha</td>
        <td><div align="right">{{$row_getbaseline['scl_mad_std']}}</div></td>
        <td><div align="right">{{$row_gettargets['scl_mad_std']}}</div></td>
        <td><div align="right">{{$row_lastdata['scl_mad_std']}}</div></td>
        <td><div align="right">{{$ReportData['scl_mad_std']}}</div></td>
        <td><div align="right">{{$row_totaldata['scl_mad_std']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Total hygiene promotion sessions</td>
        <td><div align="right">{{$row_getbaseline['scl_hp_ses']}}</div></td>
        <td><div align="right">{{$row_gettargets['scl_hp_ses']}}</div></td>
        <td><div align="right">{{$row_lastdata['scl_hp_ses']}}</div></td>
        <td><div align="right">{{$ReportData['scl_hp_ses']}}</div></td>
        <td><div align="right">{{$row_totaldata['scl_hp_ses']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of boys participated in hygiene promotion sessions</td>
        <td><div align="right">{{$row_getbaseline['scl_hp_boys']}}</div></td>
        <td><div align="right">{{$row_gettargets['scl_hp_boys']}}</div></td>
        <td><div align="right">{{$row_lastdata['scl_hp_boys']}}</div></td>
        <td><div align="right">{{$ReportData['scl_hp_boys']}}</div></td>
        <td><div align="right">{{$row_totaldata['scl_hp_boys']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of Girls participated in hygiene promotion sessions</td>
        <td><div align="right">{{$row_getbaseline['scl_hp_girls']}}</div></td>
        <td><div align="right">{{$row_gettargets['scl_hp_girls']}}</div></td>
        <td><div align="right">{{$row_lastdata['scl_hp_girls']}}</div></td>
        <td><div align="right">{{$ReportData['scl_hp_girls']}}</div></td>
        <td><div align="right">{{$row_totaldata['scl_hp_girls']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Total Menstrual hygiene  sessions</td>
        <td><div align="right">{{$row_getbaseline['scl_mn_ses']}}</div></td>
        <td><div align="right">{{$row_gettargets['scl_mn_ses']}}</div></td>
        <td><div align="right">{{$row_lastdata['scl_mn_ses']}}</div></td>
        <td><div align="right">{{$ReportData['scl_mn_ses']}}</div></td>
        <td><div align="right">{{$row_totaldata['scl_mn_ses']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td height="28" align="right" >&nbsp;</td>
        <td>Number of Girls participated in Menstrual hygiene  session</td>
        <td><div align="right">{{$row_getbaseline['scl_mn_girls']}}</div></td>
        <td><div align="right">{{$row_gettargets['scl_mn_girls']}}</div></td>
        <td><div align="right">{{$row_lastdata['scl_mn_girls']}}</div></td>
        <td><div align="right">{{$ReportData['scl_mn_girls']}}</div></td>
        <td><div align="right">{{$row_totaldata['scl_mn_girls']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of Drama played</td>
        <td><div align="right">{{$row_getbaseline['scl_dr']}}</div></td>
        <td><div align="right">{{$row_gettargets['scl_dr']}}</div></td>
        <td><div align="right">{{$row_lastdata['scl_dr']}}</div></td>
        <td><div align="right">{{$ReportData['scl_dr']}}</div></td>
        <td><div align="right">{{$row_totaldata['scl_dr']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of students watched drama</td>
        <td><div align="right">{{$row_getbaseline['scl_dr_std']}}</div></td>
        <td><div align="right">{{$row_gettargets['scl_dr_std']}}</div></td>
        <td><div align="right">{{$row_lastdata['scl_dr_std']}}</div></td>
        <td><div align="right">{{$ReportData['scl_dr_std']}}</div></td>
        <td><div align="right">{{$row_totaldata['scl_dr_std']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of Video shows</td>
        <td><div align="right">{{$row_getbaseline['scl_vd']}}</div></td>
        <td><div align="right">{{$row_gettargets['scl_vd']}}</div></td>
        <td><div align="right">{{$row_lastdata['scl_vd']}}</div></td>
        <td><div align="right">{{$ReportData['scl_vd']}}</div></td>
        <td><div align="right">{{$row_totaldata['scl_vd']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of students watched video shows</td>
        <td><div align="right">{{$row_getbaseline['scl_vd_std']}}</div></td>
        <td><div align="right">{{$row_gettargets['scl_vd_std']}}</div></td>
        <td><div align="right">{{$row_lastdata['scl_vd_std']}}</div></td>
        <td><div align="right">{{$ReportData['scl_vd_std']}}</div></td>
        <td><div align="right">{{$row_totaldata['scl_vd_std']}}</div></td>
      </tr>
      <tr valign="baseline" class="header">
        <td colspan="7" align="left" >Household Sanitation Information</td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of new HH latrine built</td>
        <td><div align="right">{{$row_getbaseline['HHS_new']}}</div></td>
        <td><div align="right">{{$row_gettargets['HHS_new']}}</div></td>
        <td><div align="right">{{$row_lastdata['HHS_new']}}</div></td>
         <td><div align="right">{{$ReportData['HHS_new']}}</div></td>
        <td><div align="right">{{$row_totaldata['HHS_new']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of new HH latrine built by hardcore </td>
        <td><div align="right">{{$row_getbaseline['HHS_new_hc']}}</div></td>
        <td><div align="right">{{$row_gettargets['HHS_new_hc']}}</div></td>
        <td><div align="right">{{$row_lastdata['HHS_new_hc']}}</div></td>
        <td><div align="right">{{$ReportData['HHS_new_hc']}}</div></td>
        <td><div align="right">{{$row_totaldata['HHS_new_hc']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of  HH latrine built improved</td>
        <td><div align="right">{{$row_getbaseline['HHS_rep']}}</div></td>
        <td><div align="right">{{$row_gettargets['HHS_rep']}}</div></td>
        <td><div align="right">{{$row_lastdata['HHS_rep']}}</div></td>
        <td><div align="right">{{$ReportData['HHS_rep']}}</div></td>
        <td><div align="right">{{$row_totaldata['HHS_rep']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of  HH latrine built improved by hardcore</td>
        <td><div align="right">{{$row_getbaseline['HHS_rep_hc']}}</div></td>
        <td><div align="right">{{$row_gettargets['HHS_rep_hc']}}</div></td>
        <td><div align="right">{{$row_lastdata['HHS_rep_hc']}}</div></td>
        <td><div align="right">{{$ReportData['HHS_rep_hc']}}</div></td>
        <td><div align="right">{{$row_totaldata['HHS_rep_hc']}}</div></td>
      </tr>
      <tr valign="baseline" class="header">
        <td colspan="7" align="left" >Public/Institutional Sanitation Information</td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of New Sanitation schemes APPROVED</td>
        <td><div align="right">{{$row_getbaseline['sa_approved']}}</div></td>
        <td><div align="right">{{$row_gettargets['sa_approved']}}</div></td>
        <td><div align="right">{{$row_lastdata['sa_approved']}}</div></td>
        <td><div align="right">{{$ReportData['sa_approved']}}</div></td>
        <td><div align="right">{{$row_totaldata['sa_approved']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of New Sanitation schemes COMPLETED</td>
        <td><div align="right">{{$row_getbaseline['sa_completed']}}</div></td>
        <td><div align="right">{{$row_gettargets['sa_completed']}}</div></td>
        <td><div align="right">{{$row_lastdata['sa_completed']}}</div></td>
        <td><div align="right">{{$ReportData['sa_completed']}}</div></td>
        <td><div align="right">{{$row_totaldata['sa_completed']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of  Sanitation schemes REPAIRED</td>
        <td><div align="right">{{$row_getbaseline['sa_renovated']}}</div></td>
        <td><div align="right">{{$row_gettargets['sa_renovated']}}</div></td>
        <td><div align="right">{{$row_lastdata['sa_renovated']}}</div></td>
        <td><div align="right">{{$ReportData['sa_renovated']}}</div></td>
        <td><div align="right">{{$row_totaldata['sa_renovated']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of Beneficiries from New or Renavated Sanitation schemes</td>
        <td><div align="right">{{$row_getbaseline['sa_benef']}}</div></td>
        <td><div align="right">{{$row_gettargets['sa_benef']}}</div></td>
        <td><div align="right">{{$row_lastdata['sa_benef']}}</div></td>
        <td><div align="right">{{$ReportData['sa_benef']}}</div></td>
        <td><div align="right">{{$row_totaldata['sa_benef']}}</div></td>
      </tr>
      <tr valign="baseline" class="header">
        <td colspan="7" align="left" >Capacity Building/Training Information</td>
      </tr>
      <tr valign="baseline">
        <td  align="right">37</td>
        <td>Number training courses organized by HYSAWA</td>
        <td><div align="right">{{$row_getbaseline['cb_trg']}}</div></td>
        <td><div align="right">{{$row_gettargets['cb_trg']}}</div></td>
        <td><div align="right">{{$row_lastdata['cb_trg']}}</div></td>
        <td><div align="right">{{$ReportData['cb_trg']}}</div></td>
        <td><div align="right">{{$row_totaldata['cb_trg']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">38</td>
        <td >Number of UP functionaries recieved training from HYSAWA</td>
        <td><div align="right">{{$row_getbaseline['cb_trg_up_total']}}</div></td>
        <td><div align="right">{{$row_gettargets['cb_trg_up_total']}}</div></td>
        <td><div align="right">{{$row_lastdata['cb_trg_up_total']}}</div></td>
        <td><div align="right">{{$ReportData['cb_trg_up_total']}}</div></td>
        <td><div align="right">{{$row_totaldata['cb_trg_up_total']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td >Number of MALE UP functionaries recieved training from HYSAWA</td>
        <td><div align="right">{{$row_getbaseline['cb_trg_up_male']}}</div></td>
        <td><div align="right">{{$row_gettargets['cb_trg_up_male']}}</div></td>
        <td><div align="right">{{$row_lastdata['cb_trg_up_male']}}</div></td>
        <td><div align="right">{{$ReportData['cb_trg_up_male']}}</div></td>
        <td><div align="right">{{$row_totaldata['cb_trg_up_male']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">39</td>
        <td >Number of FEMALE UP functionaries recieved training from HYSAWA</td>
        <td><div align="right">{{$row_getbaseline['cb_trg_up_female']}}</div></td>
        <td><div align="right">{{$row_gettargets['cb_trg_up_female']}}</div></td>
        <td><div align="right">{{$row_lastdata['cb_trg_up_female']}}</div></td>
        <td><div align="right">{{$ReportData['cb_trg_up_female']}}</div></td>
        <td><div align="right">{{$row_totaldata['cb_trg_up_female']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of UP/PNGO staff recieved training from HYSAWA</td>
        <td><div align="right">{{$row_getbaseline['cb_trg_stf_total']}}</div></td>
        <td><div align="right">{{$row_gettargets['cb_trg_stf_total']}}</div></td>
        <td><div align="right">{{$row_lastdata['cb_trg_stf_total']}}</div></td>
        <td><div align="right">{{$ReportData['cb_trg_stf_total']}}</div></td>
        <td><div align="right">{{$row_totaldata['cb_trg_stf_total']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of UP/PNGO MALE staff recieved training from HYSAWA</td>
        <td><div align="right">{{$row_getbaseline['cb_trg_stf_male']}}</div></td>
        <td><div align="right">{{$row_gettargets['cb_trg_stf_male']}}</div></td>
        <td><div align="right">{{$row_lastdata['cb_trg_stf_male']}}</div></td>
        <td><div align="right">{{$ReportData['cb_trg_stf_male']}}</div></td>
        <td><div align="right">{{$row_totaldata['cb_trg_stf_male']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of UP/PNGO FEMALE staff recieved training from HYSAWA</td>
        <td><div align="right">{{$row_getbaseline['cb_trg_stf_female']}}</div></td>
        <td><div align="right">{{$row_gettargets['cb_trg_stf_female']}}</div></td>
        <td><div align="right">{{$row_lastdata['cb_trg_stf_female']}}</div></td>
        <td><div align="right">{{$ReportData['cb_trg_stf_female']}}</div></td>
        <td><div align="right">{{$row_totaldata['cb_trg_stf_female']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of Community facilitators recieved training from UP</td>
        <td><div align="right">{{$row_getbaseline['cb_trg_vol_total']}}</div></td>
        <td><div align="right">{{$row_gettargets['cb_trg_vol_total']}}</div></td>
        <td><div align="right">{{$row_lastdata['cb_trg_vol_total']}}</div></td>
        <td><div align="right">{{$ReportData['cb_trg_vol_total']}}</div></td>
        <td><div align="right">{{$row_totaldata['cb_trg_vol_total']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of MALE Community facilitators recieved training from UP</td>
        <td><div align="right">{{$row_getbaseline['cb_trg_vol_male']}}</div></td>
        <td><div align="right">{{$row_gettargets['cb_trg_vol_male']}}</div></td>
        <td><div align="right">{{$row_lastdata['cb_trg_vol_male']}}</div></td>
        <td><div align="right">{{$ReportData['cb_trg_vol_male']}}</div></td>
        <td><div align="right">{{$row_totaldata['cb_trg_vol_male']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td >Number of FEMALE Community facilitators recieved training from UP</td>
        <td><div align="right">{{$row_getbaseline['cb_trg_vol_female']}}</div></td>
        <td><div align="right">{{$row_gettargets['cb_trg_vol_female']}}</div></td>
        <td><div align="right">{{$row_lastdata['cb_trg_vol_female']}}</div></td>
        <td><div align="right">{{$ReportData['cb_trg_vol_female']}}</div></td>
        <td><div align="right">{{$row_totaldata['cb_trg_vol_female']}}</div></td>
      </tr>
      <tr valign="baseline" class="header">
        <td colspan="7" align="left" >Water Supply Information</td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of Water supply schemes APPROVED</td>
        <td><div align="right">{{$row_getbaseline['ws_approved']}}</div></td>
        <td><div align="right">{{$row_gettargets['ws_approved']}}</div></td>
        <td><div align="right">{{$row_lastdata['ws_approved']}}</div></td>
        <td><div align="right">{{$ReportData['ws_approved']}}</div></td>
        <td><div align="right">{{$row_totaldata['ws_approved']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of Water supply schemes COMPLETED</td>
        <td><div align="right">{{$row_getbaseline['ws_completed']}}</div></td>
        <td><div align="right">{{$row_gettargets['ws_completed']}}</div></td>
        <td><div align="right">{{$row_lastdata['ws_completed']}}</div></td>
        <td><div align="right">{{$ReportData['ws_completed']}}</div></td>
        <td><div align="right">{{$row_totaldata['ws_completed']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of people benefited from Completed schemes</td>
        <td><div align="right">{{$row_getbaseline['ws_beneficiary']}}</div></td>
        <td><div align="right">{{$row_gettargets['ws_beneficiary']}}</div></td>
        <td><div align="right">{{$row_lastdata['ws_beneficiary']}}</div></td>
        <td><div align="right">{{$ReportData['ws_beneficiary']}}</div></td>
        <td><div align="right">{{$row_totaldata['ws_beneficiary']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of HARDCORE people benefited from Completed schemes</td>
        <td><div align="right">{{$row_getbaseline['ws_hc_benef']}}</div></td>
        <td><div align="right">{{$row_gettargets['ws_hc_benef']}}</div></td>
        <td><div align="right">{{$row_lastdata['ws_hc_benef']}}</div></td>
        <td><div align="right">{{$ReportData['ws_hc_benef']}}</div></td>
        <td><div align="right">{{$row_totaldata['ws_hc_benef']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td >Number of people have access to safe water within 50 metere or 150 ft</td>
        <td><div align="right">{{$row_getbaseline['ws_50']}}</div></td>
        <td><div align="right">{{$row_gettargets['ws_50']}}</div></td>
        <td><div align="right">{{$row_lastdata['ws_50']}}</div></td>
        <td><div align="right">{{$ReportData['ws_50']}}</div></td>
        <td><div align="right">{{$row_totaldata['ws_50']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of CARETAKER trainined</td>
        <td><div align="right">{{$row_getbaseline['CT_trg']}}</div></td>
        <td><div align="right">{{$row_gettargets['CT_trg']}}</div></td>
        <td><div align="right">{{$row_lastdata['CT_trg']}}</div></td>
        <td><div align="right">{{$ReportData['CT_trg']}}</div></td>
        <td><div align="right">{{$row_totaldata['CT_trg']}}</div></td>
      </tr>
      <tr valign="baseline">
        <td  align="right">&nbsp;</td>
        <td>Number of MECHANICS trainined</td>
        <td><div align="right">{{$row_getbaseline['pdb']}}</div></td>
        <td><div align="right">{{$row_gettargets['pdb']}}</div></td>
        <td><div align="right">{{$row_lastdata['pdb']}}</div></td>
        <td><div align="right">{{$ReportData['pdb']}}</div></td>
        <td><div align="right">{{$row_totaldata['pdb']}}</div></td>
      </tr>
    </tbody>
            </table>
        </div>
    </div>

<style>
    .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
        border: 1px solid;
    }
</style>


@if(
  isset($_REQUEST['query']) && $_REQUEST['query'] != "" &&
  isset($_REQUEST['report_type']) && $_REQUEST['report_type'] == "html"
)

<?php
  $old = [
    'starting_date' => '',
    'ending_date'   => '',
    'report_type'   => $_REQUEST['report_type'],
    'project_id'    => auth()->user()->proj_id,
    'region_id'     => auth()->user()->region_id,
    'district_id'   => isset($_REQUEST['district_id']) && $_REQUEST['district_id'] != "all" ? $_REQUEST['district_id'] : "",
    'upazila_id'    => isset($_REQUEST['upazila_id']) && $_REQUEST['upazila_id'] != "all" ? $_REQUEST['upazila_id'] : "",
    'union_id'      => isset($_REQUEST['union_id']) && $_REQUEST['union_id'] != "all" ? $_REQUEST['union_id'] : "",
    'type'          => 'Event'
  ];

  if(isset($_REQUEST['rep_data_id']) && $_REQUEST['rep_data_id'] != "" && $_REQUEST['rep_data_id'] != "all")
  {
    $rep_data = \DB::table('rep_period')->find($_REQUEST['rep_data_id']);
    if(count($rep_data))
    {
      $d = explode(' ', $rep_data->period);

      if(count($d) >= 2)
      {
        $old['starting_date'] = date('Y-m-d', strtotime($d[1]." 01, ".$d[0]));
        $old['ending_date']   = date('Y-m-t', strtotime($old['starting_date']));
      }
    }
  }

  if($old['district_id'] == "")
  {
      $old['district_id'] = auth()->user()->distid;
  }
?>


  <div class="col-md-12">
    <style type="text/css">
      #example3 td, #example3 th, #example2 td, #example2 th{
        font-size: 10px;
        text-align: center;
      }
    </style>


    <?php
      function getTotal1($old, $id)
      {
        return \DB::table('mobile_app_data_list_items')
               ->select(\DB::raw( 'COUNT(mobile_app_data_list_items.id) as total' ))
               ->leftJoin('mob_app_data_list', 'mob_app_data_list.id', '=', 'mobile_app_data_list_items.mobile_app_data_list_id')
               ->where('mobile_app_data_list_items.question_id', $id) // CDF No
               ->where(function($query) use ($old){
                  $date_type = "created_at";
                  if($date_type != "")
                  {
                    if(!empty($old['starting_date']) && !empty($old['ending_date'])){
                      $query->whereDate("mob_app_data_list.".$date_type, '>=', $old['starting_date'])
                            ->whereDate("mob_app_data_list.".$date_type, '<=', $old['ending_date']);
                    }elseif(!empty($old['starting_date'])){
                      $query->whereDate("mob_app_data_list.".$date_type, '=', $old['starting_date']);
                    }
                  }
                  //if(!empty($type)){$query->where('mob_app_data_list.type', 'Event');}
                  if(!empty($old['project_id'])){$query->where('mob_app_data_list.proj_id', $old['project_id']);}
                  if(!empty($old['region_id'])){$query->where('mob_app_data_list.region_id', $old['region_id']);}
                  if(!empty($old['district_id'])){$query->where('mob_app_data_list.distid', $old['district_id']);}
                  if(!empty($old['upazila_id'])){$query->where('mob_app_data_list.upid', $old['upazila_id']);}
                  if(!empty($old['union_id'])){$query->where('mob_app_data_list.unid', $old['union_id']);}
               })
               ->get()
               ->first()
               ->total
               ;
      }

      function getTotal($old, $question_id, $value, $type="COUNT")
      {
        return \DB::table('mobile_app_data_list_items')
           ->select(\DB::raw( $type.'(mobile_app_data_list_items.value) as total' ))
           ->leftJoin('mob_app_data_list', 'mob_app_data_list.id', '=', 'mobile_app_data_list_items.mobile_app_data_list_id')
           ->where('mobile_app_data_list_items.question_id', $question_id)
           ->where(function($query) use ($value, $type) {
              if($type == "COUNT")
              {
                $query->where('mobile_app_data_list_items.value', $value);
              }
           })

           ->where(function($query) use ($old){
              $date_type = "created_at";
              if($date_type != "")
              {
                if(!empty($old['starting_date']) && !empty($old['ending_date'])){
                  $query->whereDate("mob_app_data_list.".$date_type, '>=', $old['starting_date'])
                        ->whereDate("mob_app_data_list.".$date_type, '<=', $old['ending_date']);
                }elseif(!empty($old['starting_date'])){
                  $query->whereDate("mob_app_data_list.".$date_type, '=', $old['starting_date']);
                }
              }
              //if(!empty($type)){$query->where('mob_app_data_list.type', 'Event');}
              if(!empty($old['project_id'])){$query->where('mob_app_data_list.proj_id', $old['project_id']);}
              if(!empty($old['region_id'])){$query->where('mob_app_data_list.region_id', $old['region_id']);}
              if(!empty($old['district_id'])){$query->where('mob_app_data_list.distid', $old['district_id']);}
              if(!empty($old['upazila_id'])){$query->where('mob_app_data_list.upid', $old['upazila_id']);}
              if(!empty($old['union_id'])){$query->where('mob_app_data_list.unid', $old['union_id']);}
           })
           ->get()
           ->first()
           ->total;
      }
    ?>

    <?php
      $old['type'] = "Event";
    ?>

    <h2>Mobile App Date Report</h2>
      <h3>Event Summary <small>Starting Date: {{$old['starting_date'] or 'all' }} | Ending Date: {{$old['ending_date'] or 'all'}}</small></h3>
      <table class="table table-bordered table-hover data-table" id="example3">
        <thead>
          <tr>
            <th rowspan="2">Total CDF No/School No</th>
            <th colspan="8">Event Type</th>
            <th colspan="3">Location</th>
            <th rowspan="2">Total Men/Boy</th>
            <th rowspan="2">Total Women/Girl</th>
            <th rowspan="2">Disabled</th>
          </tr>
          <tr>
            <th>Hand Wash</th>
            <th>Latrine Maintenance</th>
            <th>Garbage Disposal</th>
            <th>Menstrual Hygiene</th>
            <th>Water Safety</th>
            <th>Food Hygiene</th>
            <th>Climate Change Awareness</th>
            <th>Volunteer Orientation</th>
            <th>Community</th>
            <th>School</th>
            <th>UP</th>
          </tr>
        </thead>
        <tbody>
          <tr>
            <th>{{getTotal1($old, 1)}}</th>

            <th>{{getTotal($old, 2, 'Hand Wash') }}</th>
            <th>{{getTotal($old, 2, 'Latrine Maintenance') }}</th>
            <th>{{getTotal($old, 2, 'Garbage Disposal') }}</th>
            <th>{{getTotal($old, 2, 'Menstrual Hygiene') }}</th>
            <th>{{getTotal($old, 2, 'Water Safety') }}</th>
            <th>{{getTotal($old, 2, 'Food Hygiene') }}</th>
            <th>{{getTotal($old, 2, 'Climate Change Awareness') }}</th>
            <th>{{getTotal($old, 2, 'Volunteer Orientation') }}</th>
            <th>{{getTotal($old, 3, 'Community') }}</th>
            <th>{{getTotal($old, 3, 'School') }}</th>
            <th>{{getTotal($old, 3, 'UP') }}</th>
            <th>{{getTotal($old, 4, 'Nos of Men/Boy', "SUM") }}</th>
            <th>{{getTotal($old, 5, 'Nos of Women/Girl', "SUM") }}</th>
            <th>{{getTotal($old, 6, 'Nos of Disabled', "SUM") }}</th>
          </tr>
        </tbody>
      </table>
      <br/>


    <?php
      $old['type'] = "Water";
    ?>

    <h3>Water Summary <small>Starting Date: {{$old['starting_date'] or 'all'}} | Ending Date: {{$old['ending_date'] or 'all'}}</small></h3>
    <table class="table table-bordered table-hover data-table" id="example2">
      <thead>
        <tr>
          <th rowspan="2">ID No</th>
          <th colspan="6">Type</th>
          <th colspan="3">Functionality</th>
          <th colspan="5">Problem Type</th>
          <th colspan="4">Problem Type</th>
        </tr>
        <tr>

          <th>Water - TW</th>
          <th>Water - Sky H</th>
          <th>Water - RO</th>
          <th>Water - RWH</th>
          <th>School Latrine</th>
          <th>Public Latrine</th>

          <th>Non Functional</th>
          <th>Function</th>
          <th>Functional with problems</th>

          <th>High Saline</th>
          <th>High Iron</th>
          <th>Platform Broken</th>
          <th>Dirty</th>
          <th>Maintenance Issue</th>

          <th>Major Repair</th>
          <th>Minor repair</th>
          <th>Awareness</th>
          <th>Improved Management</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>{{getTotal1($old, 7)}}</th>

          <th>{{getTotal($old, 8, 'Water - TW') }}</th>
          <th>{{getTotal($old, 8, 'Water - Sky H') }}</th>
          <th>{{getTotal($old, 8, 'Water - RO') }}</th>
          <th>{{getTotal($old, 8, 'Water - RWH') }}</th>
          <th>{{getTotal($old, 8, 'School Latrine') }}</th>
          <th>{{getTotal($old, 8, 'Public Latrine') }}</th>

          <th>{{getTotal($old, 9, 'Non Functional') }}</th>
          <th>{{getTotal($old, 9, 'Function') }}</th>
          <th>{{getTotal($old, 9, 'Functional with problems') }}</th>

          <th>{{getTotal($old, 10, 'High Saline') }}</th>
          <th>{{getTotal($old, 10, 'High Iron') }}</th>
          <th>{{getTotal($old, 10, 'Platform Broken') }}</th>
          <th>{{getTotal($old, 10, 'Dirty') }}</th>
          <th>{{getTotal($old, 10, 'Maintenance Issue') }}</th>

          <th>{{getTotal($old, 11, 'Major Repair') }}</th>
          <th>{{getTotal($old, 11, 'Minor repair') }}</th>
          <th>{{getTotal($old, 11, 'Awareness') }}</th>
          <th>{{getTotal($old, 11, 'Improved Management') }}</th>
        </tr>
      </tbody>
    </table>
    <br/>

    <?php
      $old['type'] = "Sanitation";
    ?>
    <h3>Sanitation Summary <small>Starting Date: {{$old['starting_date'] or 'all'}} | Ending Date: {{$old['ending_date'] or 'all'}}</small></h3>
    <table class="table table-bordered table-hover data-table" id="example2">
      <thead>
        <tr>
          <th rowspan="2">ID No</th>
          <th colspan="6">Type</th>
          <th colspan="3">Functionality</th>
          <th colspan="5">Problem Type</th>
          <th colspan="4">Problem Type</th>
        </tr>
        <tr>

          <th>Water - TW</th>
          <th>Water - Sky H</th>
          <th>Water - RO</th>
          <th>Water - RWH</th>
          <th>School Latrine</th>
          <th>Public Latrine</th>

          <th>Non Functional</th>
          <th>Function</th>
          <th>Functional with problems</th>

          <th>High Saline</th>
          <th>High Iron</th>
          <th>Platform Broken</th>
          <th>Dirty</th>
          <th>Maintenance Issue</th>

          <th>Major Repair</th>
          <th>Minor repair</th>
          <th>Awareness</th>
          <th>Improved Management</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th>{{getTotal1($old, 12)}}</th>

          <th>{{getTotal($old, 13, 'Water - TW') }}</th>
          <th>{{getTotal($old, 13, 'Water - Sky H') }}</th>
          <th>{{getTotal($old, 13, 'Water - RO') }}</th>
          <th>{{getTotal($old, 13, 'Water - RWH') }}</th>
          <th>{{getTotal($old, 13, 'School Latrine') }}</th>
          <th>{{getTotal($old, 13, 'Public Latrine') }}</th>

          <th>{{getTotal($old, 14, 'Non Functional') }}</th>
          <th>{{getTotal($old, 14, 'Function') }}</th>
          <th>{{getTotal($old, 14, 'Functional with problems') }}</th>

          <th>{{getTotal($old, 15, 'High Saline') }}</th>
          <th>{{getTotal($old, 15, 'High Iron') }}</th>
          <th>{{getTotal($old, 15, 'Platform Broken') }}</th>
          <th>{{getTotal($old, 15, 'Dirty') }}</th>
          <th>{{getTotal($old, 15, 'Maintenance Issue') }}</th>

          <th>{{getTotal($old, 16, 'Major Repair') }}</th>
          <th>{{getTotal($old, 16, 'Minor repair') }}</th>
          <th>{{getTotal($old, 16, 'Awareness') }}</th>
          <th>{{getTotal($old, 16, 'Improved Management') }}</th>
        </tr>
      </tbody>
    </table>
    <br/>
  </div>
@endif

  <style>
      .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {
          border: 1px solid;
      }
  </style>



@endsection


@section('my_js')
<script type="text/javascript">
  $(document).ready(function () {

    $('#district_id').on('change', function() {
      var head_id = $(this).val();

      if(head_id != "all")
      {
        $("#upazila_id").html("Select District");
        $("#union_id").html("Select Upazila");

        $.ajax({
          url: '{{route('superadmin.ajax.upazila')}}?district_id='+head_id,
          type: 'GET',
          success: function(response){
            if(response['status'] == true){
              var options = response['upazila_list'];
              $("#upazila_id").html(options);
            }
          }
        });
      }else{
        $("#upazila_id").html("Select District");
        $("#union_id").html("Select Upazila");
      }

    });

    $('#upazila_id').on('change', function() {
      var upazila_id = $(this).val();

      if(upazila_id != "all")
      {
        $("#union_id").html("Select Upazila");
        $.ajax({
          url: '{{route('superadmin.ajax.union')}}?upazila_id='+upazila_id,
          type: 'GET',
          success: function(response){
            if(response['status'] == true){
              var options = response['union_list'];
              $("#union_id").html(options);
            }
          }
        });
      }else{
        $("#union_id").html("Select Upazila");
      }

    });


  });
</script>
@endsection