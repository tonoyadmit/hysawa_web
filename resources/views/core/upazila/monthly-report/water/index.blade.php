@extends('layouts.appinside')
@section('content')
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('upazila-admin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Monthly Report</span>
      </li>
    </ul>
  </div>
  <div class="col-md-12" style="margin-top: 10px;">
      @include('core.upazila.monthly-report.water._search')
  </div>

@endsection



@section('my_js')
<script type="text/javascript">
  $(document).ready(function () {

    $('#union_id').on('change', function() {
      var union_id = $(this).val();

      $.ajax({
        url: '{{route('upazila-admin.periods.get-period')}}?union_id='+union_id,
        type: 'GET',
        success: function(response){
          if(response['status'] == true){
            $("#rep_data_id").html(response['data']);
          }
        }
      });
    });


  });
</script>
@endsection