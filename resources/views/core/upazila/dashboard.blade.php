@extends('layouts.appinside')

@section('content')
<div class="row widget-row"></div>
<?php
  $wSubmitted = App\Model\Water::where('upid', \Auth::user()->upid)->count();
  $aSubmitted = App\Model\Water::where('upid', \Auth::user()->upid)->where('app_status', 'Approved')->get()->count();
  $cSubmitted = App\Model\Water::where('upid', \Auth::user()->upid)->where('imp_status', 'Completed')->get()->count();
?>
<div class="row widget-row">

<div class="col-lg-4 col-md-6">
    <div class="panel panel-primary">
        <div class="panel-heading" style="padding: 20px;">
            <div class="row">
                <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                    <i class="fa fa-inbox fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <div class="huge">{{$wSubmitted}}</div>
                    <div># of Water Scheme Submitted</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-4 col-md-6">
    <div class="panel panel-primary">
        <div class="panel-heading" style="padding: 20px;">
            <div class="row">
                <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                    <i class="fa fa-list-alt fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <div class="huge">{{$aSubmitted}}</div>
                    <div># of Water Scheme Approved</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-4 col-md-6">
    <div class="panel panel-primary">
        <div class="panel-heading" style="padding: 20px;">
            <div class="row">
                <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                    <i class="fa fa-compress fa-5x"></i>
                </div>
                <div class="col-xs-9 text-right">
                    <div class="huge">{{$cSubmitted}}</div>
                    <div># of Water Scheme Implemented</div>
                </div>
            </div>
        </div>
    </div>
</div>


</div>

<div class="row widget-row">


<?php
  $swAmount = App\Model\Sanitation::where('upid', \Auth::user()->upid)->count();
  $saAmount = App\Model\Sanitation::where('upid', \Auth::user()->upid)->where('app_status', 'Approved')->get()->count();
  $scAmount = App\Model\Sanitation::where('upid', \Auth::user()->upid)->where('imp_status', 'Completed')->get()->count();
?>
   <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 20px;">
                <div class="row">
                    <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                        <i class="fa fa-inbox fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{$swAmount}}</div>
                        <div># of Sanitary Scheme Submitted</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 20px;">
                <div class="row">
                    <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                        <i class="fa fa-inbox fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{$saAmount}}</div>
                        <div># of Sanitary Scheme Approved</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
  <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 20px;">
                <div class="row">
                    <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                        <i class="fa fa-inbox fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{$scAmount}}</div>
                        <div># of Sanitary Scheme Implemented</div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>



@endsection