@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('upazila-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Document/Checklist</span>
    </li>
  </ul>
</div>

<div class="portlet box blue" style="margin-top: 10px;">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Document/Checklist </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body">
    <table class="table table-bordered table-hover table-condensed table-stripped">
      <thead>
      <tr><th>&nbsp;</th><th>Name of Document/Checklist</th><th>&nbsp;</th></tr>
      </thead>
      <tbody>

        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td class="size12"><strong>Manuals</strong></td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>UP Manual</td>
          <td align="left"><a href="{{asset('downloads/UP Manual.doc')}}" download>Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>UP Finance Manual</td>
          <td align="left"><a href="{{asset('downloads/UP Finance Manual  28 Mar 2013.doc')}}" download>Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>UP Procurement Manual</td>
          <td align="left"><a href="{{asset('downloads/UP Procurement Manual-Final.pdf')}}" download>Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>PNGO Manual</td>
          <td align="left"><a href="{{asset('downloads/PNGO Manual.doc')}}" download>Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>Support organization Manual</td>
          <td align="left"><a href="{{asset('downloads/SO Manual.doc')}}" download>Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><strong>PNGO Checklists for UP</strong></td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>PNGO Fund Disbursement checklist - 1st installment</td>
          <td align="left"><a href="{{asset('downloads/forup/PNGO Fund disbursement_1st Instalment.doc')}}" download>Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>PNGO Fund disbursement_Ongoing Installments</td>
          <td align="left"><a href="{{asset('downloads/forup/PNGO Fund disbursement_Ongoing Instalment.doc')}}" download>Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><strong>PNGO Checklists for HYSAWA</strong></td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>PNGO Fund Disbursement checklist - 1st installments</td>
          <td align="left"><a href="{{asset('downloads/PNGO Fund disbursement_1st Instalment.doc')}}" download>Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>PNGO Fund disbursement_Ongoing Installments</td>
          <td align="left"><a href="{{asset('downloads/PNGO Fund disbursement_Ongoing Instalment.doc')}}" download>Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><strong>HARDWARE Payment checklists For UP</strong></td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>UP Fund disbursement_Sub-Project_TW_1st Installment</td>
          <td align="left"><a href="{{asset('downloads/forup/UP Fund disbursement_SubProject_TW_1st Instalment.doc')}}" download>Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>UP Fund disbursement_Sub-Project_TW_2nd Installment</td>
          <td align="left"><a href="{{asset('downloads/forup/UP Fund disbursement_SubProject_TW_2nd Instalment.doc')}}" download>Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>UP Fund disbursement_Sub-Project TW Final Installment</td>
          <td align="left"><a href="{{asset('downloads/forup/UP Fund disbursement_SubProject(TW)_Final Instalment.doc')}}" download >Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>UP Fund disbursement_Sub-Project_Latrine_1st Installment</td>
          <td align="left"><a href="{{asset('downloads/forup/UP Fund disbursement_SubProject_Latrine_1st Instalment.doc')}}" download >Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>UP Fund disbursement_Sub-Project_Latrine_Final Installment</td>
          <td align="left"><a href="{{asset('downloads/forup/UP Fund disbursement_SubProject_Latrine_Final Instalment.doc')}}" download >Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><strong>HARDWARE Payment checklists For HYSAWA</strong></td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>UP Fund disbursement_Sub-Project_TW_1st Installment</td>
          <td align="left"><a href="{{asset('downloads/UP Fund disbursement_SubProject_TW_1st Instalment.doc')}}" download >Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>UP Fund disbursement_Sub-Project_TW_2nd Installment</td>
          <td align="left"><a href="{{asset('downloads/UP Fund disbursement_SubProject_TW_2nd Instalment.doc')}}" download >Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>UP Fund disbursement_Sub-Project TW Final Installment</td>
          <td align="left"><a href="{{asset('downloads/UP Fund disbursement_SubProject(TW)_Final Instalment.doc')}}" download >Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>UP Fund disbursement_Sub-Project_Latrine_1st Installment</td>
          <td align="left"><a href="{{asset('downloads/UP Fund disbursement_SubProject_Latrine_1st Instalment.doc')}}" download >Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>UP Fund disbursement_Sub-Project_Latrine_Final Installment</td>
          <td align="left"><a href="{{asset('downloads/UP Fund disbursement_SubProject_Latrine_Final Instalment.doc')}}" download >Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><strong>SO Payment checklists</strong></td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>Support Organization (SO)_Ongoing Installment</td>
          <td align="left"><a href="{{asset('downloads/Support Organization_Ongoing Instalment.doc')}}" download >Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><strong>UP Cost Checklists</strong></td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>UP Fund disbursement_ongoing Installment</td>
          <td align="left"><a href="{{asset('downloads/UP Fund disbursement_ongoing Instalment.doc')}}" download >Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td><strong>Templates </strong></td>
          <td align="left">&nbsp;</td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>Sub-project template - Water Supply</td>
          <td align="left"><a href="{{asset('downloads/SP Ttemplate Water.pdf')}}" download >Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>Water point site visit checklist</td>
          <td align="left"><a href="{{asset('downloads/Checklist Water.pdf')}}" download >Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>Sub-project templete - Sanitation</td>
          <td align="left"><a href="{{asset('downloads/SP template latrine.pdf')}}" download >Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>Sub-project summary, budget and forwarding letter</td>
          <td align="left"><a href="{{asset('downloads/Sub-project summary.pdf')}}" download >Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>UP Capacity Building Indicators</td>
          <td align="left"><a href="{{asset('downloads/capacity building indicators.pdf')}}" download >Download</a></td>
        </tr>
        <tr>
          <td>&nbsp;</td>
          <td>CDF Register</td>
          <td align="left"><a href="{{asset('downloads/CDF registrar.pdf')}}" download >Download</a></td>
        </tr>


      </tbody>
    </table>
    </div>
</div>



@endsection
