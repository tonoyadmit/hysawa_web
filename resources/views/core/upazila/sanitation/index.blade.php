@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('upazila-admin.dashboard') }}">Dashboard</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Sanitation</span>
      </li>
    </ul>
  </div>

  <div class="col-md-12" style="margin-top: 10px;">
    @include('core.upazila.sanitation._search')
  </div>
  @include('core.upazila.sanitation._table')

@endsection
