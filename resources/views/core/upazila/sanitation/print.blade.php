@extends('layouts.print.app')

@section('my_style')
<style>
    table{
      border-collapse: collapse;
      border-spacing: 0;
    }

    div{
      font-size: 80%;
      display: inline-block;
     /* overflow: visible;

      width: 2.4em;
      line-height: 0.9em;
      margin-top: 9em;

      white-space: nowrap;
      -ms-transform: rotate(270deg);
      -o-transform: rotate(270deg);
      -webkit-transform: rotate(270deg);
      transform: rotate(270deg);
      text-align: left;*/
    }

    .headline th {
      padding: 5px 20px;
    }
  </style>
@endsection

@section('content')


<p> <strong>Region</strong>: {{$sanitations->first()->region->region_name or ''}}&nbsp;|
    <strong>District</strong>: {{$sanitations->first()->district->distname or ''}}&nbsp;|
    <strong>Upazila</strong>: {{$sanitations->first()->upazila->upname or ''}}&nbsp;

    <?php
        $union_id = "all";
        if( isset($_REQUEST['union_id']) && $_REQUEST['union_id'] != "all"){
          $union_id = App\Model\Union::find($_REQUEST['union_id']);
          if(count($union_id)){
            $union_id = $union_id->unname;
          }
        }
    ?>
    <strong>Union</strong>: {{$union_id or ''}}&nbsp;
</p>

@if(isset($_REQUEST['query']))
  <p>Query:
      <strong>App Status</strong>: {{$_REQUEST['app_status'] or ''}}&nbsp;|
      <strong>Implement Status</strong>: {{$_REQUEST['imp_status'] or ''}}&nbsp;|
      <strong>Approval Date</strong>: {{$_REQUEST['starting_date'] or ''}}&nbsp;
  </p>
@endif


<h2>List of Sanitation Schemes(HYSAWA): {{count($sanitations)}}</h2>

<table class="box" border="1">
    <thead class="rotate">

      <th><div>District</div></th>
      <th><div>Upazila</div></th>
      <th><div>Union</div></th>

      <th><div>Approval</div></th>
      <th><div>Implementation Status</div></th>
      <th><div>Approval Date</div></th>
      <th><div>CDF No</div></th>
      <th><div>Type</div></th>

      <th><div>Village</div></th>
      <th><div>Type of Inst.</div></th>
      <th><div>SubType</div></th>
      <th><div>Name</div></th>
      <th><div>Male Chamber</div></th>

      <th><div>Female Chamber</div></th>
      <th><div>Male Users</div></th>
      <th><div>Female Users</div></th>

    </thead>
    <tbody>
      @foreach($sanitations as $sanitation)
      <tr>

        <td><div>@if(!empty($sanitation->district)) {{$sanitation->district->distname}} @endif</div></td>
        <td><div>@if(!empty($sanitation->upazila)) {{$sanitation->upazila->upname}} @endif</div></td>
        <td><div>@if(!empty($sanitation->union)) {{$sanitation->union->unname}} @endif</div></td>

        <td ><div>{{ $sanitation->app_status }}</div></td>
        <td ><div>{{ $sanitation->imp_status }}</div></td>
        <td ><div>{{ $sanitation->app_date }}</div></td>
        <td ><div>{{ $sanitation->cdfno }}</div></td>
        <td ><div>{{ $sanitation->cons_type }}</div></td>
        <td ><div>{{ $sanitation->village }}</div></td>
        <td ><div>{{ $sanitation->maintype }}</div></td>
        <td ><div>{{ $sanitation->subtype }}</div></td>
        <td ><div>{{ $sanitation->name }}</div></td>
        <td ><div>{{ $sanitation->malechamber }}</div></td>
        <td ><div>{{ $sanitation->femalechamber }}</div></td>
        <td ><div>{{ $sanitation->male_ben }}</div></td>
        <td ><div>{{ $sanitation->fem_ben }}</div></td>

      </tr>
      @endforeach
    </tbody>
  </table>
@endsection