@if(count($sanitations) > 0)

<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">

    <div class="portlet-title">
      <div class="caption">
        Sanitation Report
      </div>
    </div>

    <div class="portlet-body util-btn-margin-bottom-5">



      <div class="table-responsive">
        <table class="table table-bordered table-hover" id="example0">
          <thead class="flip-content">
            <th style="font-size: 10px;">Action</th>
            <th style="font-size: 10px;" nowrap="nowrap">App Date</th>
            <th style="font-size: 10px;">App Status</th>
            <th style="font-size: 10px;">Imp Status</th>


            <th style="font-size: 10px;">CDF No</th>
            <th style="font-size: 10px;">Latrine No</th>
            <th style="font-size: 10px;">Cons Type</th>
            <th style="font-size: 10px;">Village</th>
            <th style="font-size: 10px;">Main type</th>

            <th style="font-size: 10px;">Sub Type</th>
            <th style="font-size: 10px;">Name</th>
            <th style="font-size: 10px;">Longitude</th>
            <th style="font-size: 10px;">Latitude</th>
            <th style="font-size: 10px;">Caretaker Name</th>
            <th style="font-size: 10px;">Caretaker Phone</th>
            <th style="font-size: 10px;">Ch comittee</th>
            <th style="font-size: 10px;">Ch com_tel</th>



          </thead>
          <tbody>
            @foreach($sanitations as $sanitation)
            <tr>

              <td style="font-size: 10px;">
                <a class="label label-success" href="{{route('upazila-admin.sanitation.edit', $sanitation->id)}}">
                  <i class="fa fa-pencil"></i>
                </a>
              </td>
              <td style="font-size: 10px;" nowrap="nowrap">{{ $sanitation->app_date }}</td>
              <td style="font-size: 10px;">{{ $sanitation->app_status }}</td>
              <td style="font-size: 10px;">{{ $sanitation->imp_status }}</td>

              <td style="font-size: 10px;">{{ $sanitation->cdfno }}</td>
              <td style="font-size: 10px;">{{ $sanitation->latrineno }}</td>
              <td style="font-size: 10px;">{{ $sanitation->cons_type }}</td>
              <td style="font-size: 10px;">{{ $sanitation->village }}</td>
              <td style="font-size: 10px;">{{ $sanitation->maintype }}</td>

              <td style="font-size: 10px;">{{ $sanitation->subtype }}</td>
              <td style="font-size: 10px;">{{ $sanitation->name }}</td>

              <td style="font-size: 10px;">{{ $sanitation->longitude }}</td>
              <td style="font-size: 10px;">{{ $sanitation->latitude }}</td>

              <td style="font-size: 10px;">{{ $sanitation->caretakername }}</td>
              <td style="font-size: 10px;">{{ $sanitation->caretakerphone }}</td>
              <td style="font-size: 10px;">{{ $sanitation->ch_comittee }}</td>
              <td style="font-size: 10px;">{{ $sanitation->ch_com_tel }}</td>



            </tr>
            @endforeach
          </tbody>
        </table>

        <div class="pagination pull-right">
          @if(count($sanitations))
          {{$sanitations->appends($old)->links()}}
          @endif
        </div>

      </div>


    </div>
  </div>
</div>
@else
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <p>
      No Data Found
    </p>
  </div>
</div>
@endif