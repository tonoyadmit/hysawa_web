@extends('layouts.appinside')

@section('content')


<div class="page-bar" style="margin-bottom: 10px;">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('upazila-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="{{ route('upazila-admin.sanitation.index') }}">Sanitation</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Update</span>
    </li>
  </ul>
</div>

<style type="text/css">
.form-group
{
  margin-bottom: 1px;
}
.control-label
{
  font-size: 10px;
  padding-right: 0px;
  padding-left: 0px;
}
</style>
<form action="{{route('upazila-admin.sanitation.update1', $WaterSanitation->id)}}" class="form-horizontal" method="POST" id="updateForm">
        {{csrf_field()}}
<div class="form-body">
  <div class="row">
    @include('partials.errors')

    <div class="col-md-6">
      <div class="form-group">
        <label class="control-label col-md-3">CDF No:</label>
        <div class="col-md-9">
          <label class="control-label col-md-3">{{ $WaterSanitation->cdfno }}</label>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Village Name</label>
        <div class="col-md-9">
            <label class="control-label col-md-3">{{ $WaterSanitation->Village }}</label>

        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Latrine no. (6 digit upcode+sl)</label>
        <div class="col-md-9">
          <label class="control-label col-md-3">{{ $WaterSanitation->latrineno }}</label>

        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Type of latrine:</label>
        <div class="col-md-9">
          <label class="control-label col-md-3">{{ $WaterSanitation->maintype }}</label>

        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Type of Institution:</label>
        <div class="col-md-9">
          <label class="control-label col-md-3">{{ $WaterSanitation->subtype }}</label>

        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">New/Renovation:</label>
        <div class="col-md-9">
         <label class="control-label col-md-3">{{ $WaterSanitation->cons_type }}</label>

        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Name of Institution/Place:</label>
        <div class="col-md-9">
          <label class="control-label col-md-3">{{ $WaterSanitation->name }}</label>

        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Chairman of the Mgt Committe:</label>
        <div class="col-md-9">
            <label class="control-label col-md-3">{{ $WaterSanitation->ch_comittee }}</label>

        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Phone no:</label>
        <div class="col-md-9">
            <label class="control-label col-md-3">{{ $WaterSanitation->ch_com_tel }}</label>

        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Implementation Status</label>
        <div class="col-md-9">
             <label class="control-label col-md-3">{{ $WaterSanitation->imp_status }}</label>

        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3"> Caretaker name:</label>
        <div class="col-md-9">
            <label class="control-label col-md-3">{{ $WaterSanitation->caretakername }}</label>

        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3"> Caretaker phone:</label>
        <div class="col-md-9">
            <label class="control-label col-md-3">{{ $WaterSanitation->caretakerphone }}</label>

        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Approval date (YYYY-MM-DD):</label>
        <div class="col-md-9">
            <label class="control-label col-md-3">{{ $WaterSanitation->app_date }}</label>

        </div>
      </div>
    </div>

    <div class="col-md-6">
      <div class="form-group">
        <label class="control-label col-md-3">No. of chamber for Male/Boys:</label>
        <div class="col-md-9">
            <label class="control-label col-md-3">{{ $WaterSanitation->malechamber }}</label>

        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">No. of chamber for Female/Girls:</label>
        <div class="col-md-9">
            <label class="control-label col-md-3">{{ $WaterSanitation->femalechamber }}</label>

        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">No. of Male beneficiry:</label>
        <div class="col-md-9">
            <label class="control-label col-md-3">{{ $WaterSanitation->male_ben }}</label>

        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">No. of Female Beneficary:</label>
        <div class="col-md-9">
            <label class="control-label col-md-3">{{ $WaterSanitation->fem_ben }}</label>

        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">No. of Child beneficiary:</label>
        <div class="col-md-9">
            <label class="control-label col-md-3">{{ $WaterSanitation->child_ben }}</label>

        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3"> No. of Disable benficiary:</label>
        <div class="col-md-9">
            <label class="control-label col-md-3">{{ $WaterSanitation->disb_bene }}</label>

        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">Proposed/Installed facilities:</label>
        <div class="col-md-9">
            <label class="control-label col-md-3">{{ $WaterSanitation->watersource }}</label>

        </div>
      </div>



      <div class="form-group">
        <label class="control-label col-md-3">Overhead tank:</label>
        <div class="col-md-9">
           <label class="control-label col-md-3">{{ $WaterSanitation->overheadtank }}</label>

        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">Motor/pump:</label>
        <div class="col-md-9">
            <label class="control-label col-md-3">{{ $WaterSanitation->motorpump }}</label>

        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">Sock well:</label>
        <div class="col-md-9">
            <label class="control-label col-md-3">{{ $WaterSanitation->sockwell }}</label>

        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">Septic tank:</label>
        <div class="col-md-9">
            <label class="control-label col-md-3">{{ $WaterSanitation->seotictank }}</label>

        </div>
      </div>
      <div class="form-group">
        <label class="control-label col-md-3">Tap outside:</label>
        <div class="col-md-9">
            <label class="control-label col-md-3">{{ $WaterSanitation->tapoutside }}</label>

        </div>
      </div>
        <div class="form-group">
        <label class="control-label col-md-3">Approval Status:</label>
        <div class="col-md-5">





        <select name="app_status" class="form-control" required="required">
          <option value="">Choose one</option>
          @foreach($appstatus as $status)
          <option value="{{$status->app_status}}"
              @if($WaterSanitation->app_status == $status->app_status)
                selected="selected"
              @endif
          >{{$status->app_status}}</option>
          @endforeach
        </select>




        </div>
      </div>
    </div>

  </div>
</div>

<div class="form-actions">
  <div class="row">
    <div class="col-md-offset-5">
          <a href="javascript:history.back()" class="btn default"> Cancel </a>
          {!! Form::submit('Sanitation Update', ['class' => 'btn green pull-left']) !!}
    </div>
  </div>
</div>


 </form>
</div>
@endsection
