<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> Sanitation <small>List</small> </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body form">
        <style type="text/css">
            .form-group{margin-bottom: 5px;}
            .control-label{font-size: 10px;}
        </style>
        <form action="{{route('upazila-admin.sanitation.index')}}" class="form-horizontal" method="GET" id="searchForm">

            <input type="hidden" name="query" value="{{time()}}" />

            <div class="form-body">

                <div class="row">

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">App Status</label>
                            <div class="col-md-9">

                                <select name="app_status" class="form-control input-sm">
                                    <option value="">Choose one</option>
                                    <?php
                                        $appStatus = \DB::table('app_status')->orderBy('app_status')->get();
                                    ?>
                                    @foreach($appStatus as $status)
                                        <option value="{{$status->app_status}}"
                                        @if($old['app_status'] == $status->app_status)
                                            selected="selected"
                                        @endif >{{$status->app_status}}</option>
                                    @endforeach

                                </select>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Imp. Status</label>
                            <div class="col-md-9">

                                <select name="imp_status" class="form-control input-sm">
                                    <option value="">Choose one</option>
                                    <?php
                                    $implStatus = \DB::table('imp_status')->orderBy('imp_status')->get();
                                    ?>
                                    @foreach($implStatus as $is)
                                    <option value="{{$is->imp_status}}" @if($old['imp_status'] == $is->imp_status) selected="selected" @endif >{{$is->imp_status}}</option>
                                    @endforeach

                                </select>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Approve Date</label>
                            <div class="col-md-9">

                                <input
                                    class="form-control date-picker input-sm"
                                    type="text"
                                    name="starting_date"
                                    data-date-format = "yyyy-mm-dd"
                                @if(!empty( $old['starting_date']))
                                    value="{{$old['starting_date']}}"
                                @endif >

                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Union </label>
                            <div class="col-md-9">
                                <select class="form-control input-sm" name="union_id" required="required" id="union_id">
                                    <option value="all" @if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] == "all") selected="selected" @endif >All</option>
                                    <?php
                                        $unions = App\Model\Union::where('proid', auth()->user()->proj_id)->where('upid', auth()->user()->upid)->get();
                                    ?>
                                    @foreach($unions as $union )
                                        <option value="{{$union->id}}"

                                            @if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] == $union->id)
                                                selected="selected"
                                            @endif

                                        >{{$union->unname}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-12">
                        <input type="submit" class="btn green" value="Search" />
                        <a href="{{route('upazila-admin.sanitation.index')}}" class="btn default">Clear</a>

                        @if(isset($_REQUEST['query']))
                            <?php
                                $old2 = $old;
                                $old2['download'] = 'download';
                                $old3 = $old;
                                $old3['print'] = 'print';
                            ?>
                            <a href="{{route('upazila-admin.sanitation.index', $old2)}}" class="btn blue btn-sm">Download</a>
                            <a href="{{route('upazila-admin.sanitation.index', $old3)}}" class="btn default btn-sm">Print</a>
                        @endif

                    </div>
                </div>
            </div>
        </form>

        @if(isset($_REQUEST['query']))
        <p>Query:
            <?php
                $union_id = "all";
                if( isset($_REQUEST['union_id']) && $_REQUEST['union_id'] != "all")
                {
                    $union_id = App\Model\Union::find($_REQUEST['union_id']);

                    if(count($union_id))
                    {
                        $union_id = $union_id->unname;
                    }
                }
            ?>
            <strong>Union</strong>: {{$union_id or ''}}&nbsp;|
            <strong>App Status</strong>: {{$_REQUEST['app_status'] or ''}}&nbsp;|
            <strong>Implement Status</strong>: {{$_REQUEST['imp_status'] or ''}}&nbsp;|
            <strong>Approval Date</strong>: {{$_REQUEST['starting_date'] or ''}}&nbsp;
        </p>
      @endif

    </div>
</div>