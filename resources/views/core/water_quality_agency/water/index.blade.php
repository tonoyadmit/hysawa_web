@extends('layouts.appinside')

@section('content')


<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('water-quality-agency.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Water</span>
    </li>
  </ul>
</div>

<div class="col-md-12" style="margin-top: 10px;">
  @include('core.water_quality_agency.water._search')
</div>

@include('core.water_quality_agency.water._table')

@endsection

@section('my_js')

<script type="text/javascript">
    $(document).ready(function () {

        $('#region_id').on('change', function() {
            var region_id = $(this).val();

            $("#district_id").html("Select District")
            $("#upazila_id").html("Select District")
            $("#union_id").html("Select Upazila");

            $.ajax({
              url: '{{route('ajax.districts-from-region')}}?region_id='+region_id+'&choose_one=true&not_all=true',
              type: 'GET',
              success: function(response){
                if(response['status'] == true){
                  $("#district_id").html(response['district_list']);
                }
              }
            });
        });



        $('#district_id').on('change', function() {
            var district_id = $(this).val();
            $("#upazila_id").html("Select District")
            $("#union_id").html("Select Upazila");
            $.ajax({
              url: '{{route('water-quality-agency.water.ajax.upazila')}}?district_id='+district_id,
              type: 'GET',
              success: function(response){
                if(response['status'] == true){
                  $("#upazila_id").html(response['upazila_list']);
                }
              }
            });
        });

        $('#upazila_id').on('change', function() {
          var upazila_id= $(this).val();
          $("#union_id").html("Select Upazila");
          $.ajax({
              url: '{{route('water-quality-agency.water.ajax.union')}}?upazila_id='+upazila_id,
              type: 'GET',
              success: function(response){
                if(response['status'] == true){
                  $("#union_id").html(response['union_list']);
                }
              }
            });
        });

    });
</script>
@endsection