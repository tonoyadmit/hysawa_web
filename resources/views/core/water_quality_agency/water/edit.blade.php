@extends('layouts.appinside')

@section('content')


<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('water-quality-agency.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Water Quality Report Edit</span>
    </li>
  </ul>
</div>

<div class="portlet box green-seagreen" style="margin-top: 10px">
  <div class="portlet-title">
      <div class="caption">
          <i class="fa fa-gift"></i>Edit Report Data
      </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
  </div>

  <div class="portlet-body form">
    <style type="text/css">
    .form-group
    {
      margin-bottom: 1px;
    }
    .control-label
    {
      font-size: 15px;
      padding-right: 0px;
      padding-left: 0px;
    }
    </style>

      <form action="{{route('water-quality-agency.water.update', $waterReport->id)}}" class="form-horizontal" method="POST" id="updateForm">

          {{csrf_field()}}

          <div class="form-body">
              <div class="row">
                <div class="col-md-5">&nbsp;</div>
                <div class="col-md-7">
                    <div class="col-md-offset-4 col-md-4">Field test</div>
                    <div class="col-md-4">Lab test</div>
                </div>
              </div>



              <div class="row">
                <div class="col-md-offset-5 col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Arsenic</label>
                    <div class="col-md-4">
                      <input type="number" class="form-control input-sm" value="{{$waterReport->arsenic}}" name="arsenic" max="5"  step="any" />
                    </div>
                    <div class="col-md-4">
                      <input type="number" class="form-control input-sm" value="{{$waterReport->as_lab}}" name="as_lab" max="5"  step="any" />
                    </div>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-offset-5 col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Iron</label>
                    <div class="col-md-4">
                      <input type="number" class="form-control input-sm" value="{{$waterReport->fe}}" name="fe" max="2000"  step="any"/>
                    </div>
                    <div class="col-md-4">
                      <input type="number" class="form-control input-sm" value="{{$waterReport->fe_lab}}" name="fe_lab" max="2000"  step="any"/>
                    </div>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-offset-5 col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Manganese:</label>
                    <div class="col-md-4">
                      <input type="text" class="form-control input-sm" value="{{$waterReport->mn}}" name="mn" />
                    </div>
                    <div class="col-md-4">
                      <input type="text" class="form-control input-sm" value="{{$waterReport->mn_lab}}" name="mn_lab" />
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-offset-5 col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Chloride (Cl):</label>
                    <div class="col-md-4">
                      <input type="number" class="form-control input-sm" value="{{$waterReport->cl}}" name="cl" max="3000"  step="any"/>
                    </div>
                    <div class="col-md-4">
                      <input type="number" class="form-control input-sm" value="{{$waterReport->cl_lab}}" name="cl_lab" max="3000"  step="any"/>
                    </div>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    {{--  <label class="control-label col-md-6">Caretaker(male) name:</label>  --}}
                    <div class="col-md-6">
                      {{--  <label class="control-label"><b>{{$waterReport->Caretaker_male}}</b></label>  --}}
                    </div>

                  </div>
                </div>
                <div class="col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Ph</label>
                    <div class="col-md-4">
                      <input type="text" class="form-control input-sm" value="{{$waterReport->ph}}" name="ph" />
                    </div>

                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    {{--  <label class="control-label col-md-6">Caretaker (female) name:</label>  --}}
                    <div class="col-md-6">
                      {{--  <label class="control-label"><b>{{$waterReport->Caretaker_female}}</b></label>  --}}
                    </div>

                  </div>
                </div>
                <div class="col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Pb:</label>
                    <div class="col-md-4">
                      <input type="text" class="form-control input-sm" value="{{$waterReport->pb}}" name="pb" />
                    </div>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    {{--  <label class="control-label col-md-6 input-sm">CDF_no: (3 digits)</label>  --}}
                    <div class="col-md-6">
                      {{--  <label class="control-label"><b>{{$waterReport->CDF_no}}</b></label>  --}}
                    </div>

                  </div>
                </div>
                <div class="col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Zinc</label>
                    <div class="col-md-4">
                      <input type="text" class="form-control input-sm" value="{{$waterReport->zinc}}" name="zinc" />
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    <label class="control-label col-md-6"></label>
                    <div class="col-md-6">
                    </div>

                  </div>
                </div>
                <div class="col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">FC</label>
                    <div class="col-md-4">
                      <input type="text" class="form-control input-sm" value="{{$waterReport->fc}}" name="fc" />
                    </div>

                  </div>
                </div>
              </div>

              <div class="row">

                <div class="col-md-offset-5 col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Turbidity</label>
                    <div class="col-md-4">
                      <input type="text" class="form-control input-sm" value="{{$waterReport->turbidity}}" name="turbidity" />
                    </div>

                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-offset-5 col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Td</label>
                    <div class="col-md-4">
                      <input type="text" class="form-control input-sm" value="{{$waterReport->td}}" name="td" />
                    </div>

                  </div>
                </div>
              </div>

                <div class="row">
                <div class="col-md-offset-5 col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Latitude</label>
                    <div class="col-md-4">
                      <input type="number" step="any" min=15 max=30 class="form-control input-sm" value="{{$waterReport->lat}}" name="lat" />
                    </div>

                  </div>
                </div>
              </div>


                     <div class="row">
                <div class="col-md-offset-5 col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Longitude</label>
                    <div class="col-md-4">
                      <input type="number" step="any" min=80 max=100 class="form-control input-sm" value="{{$waterReport->lon}}" name="lon" />
                    </div>

                  </div>
                </div>
              </div>



               <div class="row">
                <div class="col-md-offset-5 col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Date Of Test: YYYY-MM-DD <font color="red"> * </font></label>
                    <div class="col-md-4">
                      <input type="text" class="form-control input-sm date-picker" data-date-format="yyyy-mm-dd" name="report_date" required="required" value="{{$waterReport->report_date}}"/>
                     </div>

                  </div>
                </div>
              </div>

             

              <div class="row">
                <div class="form-group">
                  <label class="control-label col-md-1">&nbsp;</label>
                  <p class="col-md-8 form-control-static"> Pls collect approval date from UP if it is blank or N/A <br/> Approval date means the date HYSAWA approved this TW </p>
                </div>
              </div>
          </div>
          <div class="form-actions">
              <div class="col-md-offset-2 col-md-10">
                  <input type="submit" class="btn green" value="Update Report Data" />
              </div>
          </div>
      </form>
  </div>
</div>

@endsection 
