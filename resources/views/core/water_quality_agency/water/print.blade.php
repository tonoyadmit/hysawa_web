@extends('layouts.print.app')

@section('my_style')

<style>
    table{border-collapse: collapse;border-spacing: 0;}
    div{font-size: 80%;display: inline-block;}
    .headline th {padding: 5px 20px;}
  </style>
@endsection


@section('content')



@if(isset($water) && count($water))

<h3>Tubewell Info:</h3>

<table class="box" border="1" width="100%">
    <tr>
      <th width="20%">Ward _no: (1 digit)</th>
      <td>{{$water->Ward_no or ''}}</td>
    </tr>

    <tr>
      <th width="20%">Village:</th>
      <td>{{$water->Village}}</td>
    </tr>

    <tr>
      <th width="20%">TW_No: (6 digit upcode + tw sl)</th>
      <td>{{$water->TW_No}}</td>
    </tr>

    <tr>
      <th width="20%">Land Owner name:</th>
      <td>{{$water->Landowner}}</td>
    </tr>

    <tr>
      <th width="20%">Caretaker(male) name:</th>
      <td>{{$water->Caretaker_male}}</td>
    </tr>

    <tr>
      <th width="20%">Caretaker (female) name:</th>
      <td>{{$water->Caretaker_female}}</td>
    </tr>
    
    <tr>
      <th width="20%">CDF_no: (3 digits)</th>
      <td>{{$water->CDF_no}}</td>
    </tr>
</table> 

@endif 

@if( isset($water->qualityResults) && count($water->qualityResults) > 0)
<h3>Water Quality Report List</h3>
<table class="box" border="1" width="100%">
    <thead>
    <tr>
      <th>Created At</th>
      
      <th  >Ward no</th>
      <th  >CDF_no</th>
      <th  >Village</th>
      <th  >TW_No</th>
      <th  >Land owner</th>
      <th  >Caretaker</th>
      <th  >X Coordinate</th>
      <th  >Y Coordinate</th>
      <th >Arsenic</th>
      <th >Arsenic Lab</th>
      <th >Iron (Fe)</th>
      <th >Fe Lab</th>
      <th >Mn</th>
      <th >Mn Lab</th>
      <th >Cl</th>
      <th >Cl Lab</th>
      <th >Ph</th>
      <th >Pb</th>
      <th >Zinc</th>
      <th >Fc</th>
      <th >Td</th>
      <th >Turbidity</th>
    </tr>


    </thead>
    <tbody>
      @foreach($waters as $water)
      <tr>
        <td> {{$water->created_at}}</td>
       
              <td  >{{$water->Ward_no}}</td>
              <td  >{{$water->CDF_no}}</td>
              <td  >{{$water->Village}}</td>
              <td  >{{$water->TW_No}}</td>
              <td  >{{$water->Landowner}}</td>

              <td  >{{$water->Caretaker_male}}</td>
              <td  >{{$water->x_coord}}</td>
              <td  >{{$water->y_coord}}</td>
              <td  >{{$water->wq_Arsenic}}</td>
              <td  >{{$water->wq_fe}}</td>

              <td  >{{$water->wq_mn}}</td>
              <td  >{{$water->wq_cl}}</td>
              <td  >{{$water->wq_ph}}</td>
              <td  >{{$water->wq_pb}}</td>
              <td  >{{$water->wq_zinc}}</td>

              <td  >{{$water->wq_fc}}</td>
              <td  >{{$water->wq_td}}</td>
              <td >{{$water->wq_turbidity}}</td>





      </tr>
      @endforeach
    </tbody>
  </table>

@endif 

@endsection
