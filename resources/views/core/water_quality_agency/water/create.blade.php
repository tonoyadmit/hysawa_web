@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('water-quality-agency.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="{{ route('water-quality-agency.water.index') }}">Waters</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Add New Entry</span>
    </li>
  </ul>
</div>

<div class="portlet box green-seagreen" style="margin-top: 10px">
  <div class="portlet-title">
      <div class="caption">
          <i class="fa fa-gift"></i>Add New Entry</div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body form">

          <form action="{{route('water-quality-agency.water.store')}}" class="form-horizontal" method="POST" id="updateForm">

              {{csrf_field()}}

              <div class="form-body">
                  <div class="row">
                    <div class="col-md-5">&nbsp;</div>
                    <div class="col-md-7">
                        <div class="col-md-offset-4 col-md-4">Field test</div>
                        <div class="col-md-4">Lab test</div>
                    </div>
                  </div>



                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label class="control-label col-md-6">Ward _no: (1 digit)</label>
                        <div class="col-md-6">
                          <label class="control-label"><b>{{$water->Ward_no}}</b></label>
                        </div>

                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="form-group">
                        <label class="control-label col-md-4">Arsenic</label>
                        <div class="col-md-4">
                          <input type="text" class="form-control" value="{{$water->wq_Arsenic}}" name="wq_Arsenic" />
                        </div>
                        <div class="col-md-4">
                          <input type="text" class="form-control" value="{{$water->wq_as_lab}}" name="wq_as_lab" />
                        </div>
                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label class="control-label col-md-6">Village:</label>
                        <div class="col-md-6">
                          <label class="control-label"><b>{{$water->Village}}</b></label>
                        </div>

                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="form-group">
                        <label class="control-label col-md-4">Iron</label>
                        <div class="col-md-4">
                          <input type="text" class="form-control" value="{{$water->wq_fe}}" name="wq_fe" />
                        </div>
                        <div class="col-md-4">
                          <input type="text" class="form-control" value="{{$water->wq_fe_lab}}" name="wq_fe_lab" />
                        </div>
                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label class="control-label col-md-6">TW_No: (6 digit upcode + tw sl)</label>
                        <div class="col-md-6">
                          <label class="control-label"><b>{{$water->TW_No}}</b></label>
                        </div>

                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="form-group">
                        <label class="control-label col-md-4">Manganese:</label>
                        <div class="col-md-4">
                          <input type="text" class="form-control" value="{{$water->wq_mn}}" name="wq_mn" />
                        </div>
                        <div class="col-md-4">
                          <input type="text" class="form-control" value="{{$water->wq_mn_lab}}" name="wq_mn_lab" />
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label class="control-label col-md-6">Land Owner name:</label>
                        <div class="col-md-6">
                          <label class="control-label"><b>{{$water->Landowner}}</b></label>
                        </div>

                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="form-group">
                        <label class="control-label col-md-4">Chloride (Cl):</label>
                        <div class="col-md-4">
                          <input type="text" class="form-control" value="{{$water->wq_cl}}" name="wq_cl" />
                        </div>
                        <div class="col-md-4">
                          <input type="text" class="form-control" value="{{$water->wq_cl_lab}}" name="wq_cl_lab" />
                        </div>
                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label class="control-label col-md-6">Caretaker(male) name:</label>
                        <div class="col-md-6">
                          <label class="control-label"><b>{{$water->Caretaker_male}}</b></label>
                        </div>

                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="form-group">
                        <label class="control-label col-md-4">Ph</label>
                        <div class="col-md-4">
                          <input type="text" class="form-control" value="{{$water->wq_ph}}" name="wq_ph" />
                        </div>

                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label class="control-label col-md-6">Caretaker (female) name:</label>
                        <div class="col-md-6">
                          <label class="control-label"><b>{{$water->Caretaker_female}}</b></label>
                        </div>

                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="form-group">
                        <label class="control-label col-md-4">Pb:</label>
                        <div class="col-md-4">
                          <input type="text" class="form-control" value="{{$water->wq_pb}}" name="wq_pb" />
                        </div>
                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="col-md-5">
                      <div class="form-group">
                        <label class="control-label col-md-6">CDF_no: (3 digits)</label>
                        <div class="col-md-6">
                          <label class="control-label"><b>{{$water->CDF_no}}</b></label>
                        </div>

                      </div>
                    </div>
                    <div class="col-md-7">
                      <div class="form-group">
                        <label class="control-label col-md-4">Zinc</label>
                        <div class="col-md-4">
                          <input type="text" class="form-control" value="{{$water->wq_zinc}}" name="wq_zinc" />
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="row">

                    <div class="col-md-offset-5 col-md-7">
                      <div class="form-group">
                        <label class="control-label col-md-4">FC</label>
                        <div class="col-md-4">
                          <input type="text" class="form-control" value="{{$water->wq_fc}}" name="wq_fc" />
                        </div>

                      </div>
                    </div>
                  </div>

                  <div class="row">

                    <div class="col-md-offset-5 col-md-7">
                      <div class="form-group">
                        <label class="control-label col-md-4">Turbidity</label>
                        <div class="col-md-4">
                          <input type="text" class="form-control" value="{{$water->wq_turbidity}}" name="wq_turbidity" />
                        </div>

                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="col-md-offset-5 col-md-7">
                      <div class="form-group">
                        <label class="control-label col-md-4">Td</label>
                        <div class="col-md-4">
                          <input type="text" class="form-control" value="{{$water->wq_td}}" name="wq_td" />
                        </div>

                      </div>
                    </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                      <label class="control-label col-md-1">&nbsp;</label>
                      <p class="col-md-8 form-control-static"> Pls collect approval date from UP if it is blank or N/A <br/> Approval date means the date HYSAWA approved this TW </p>
                    </div>
                  </div>
              </div>
              <div class="form-actions">
                  <div class="col-md-offset-2 col-md-10">
                      <input type="submit" class="btn green" value="Update" />
                  </div>
              </div>
          </form>
      </div>
  </div>

</div>
@endsection
