@extends('layouts.appinside')

@section('content')


<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('water-quality-agency.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Water</span>
    </li>
  </ul>
</div>

<div class="portlet box green-seagreen" style="margin-top: 10px">
  <div class="portlet-title">
      <div class="caption">
          <i class="fa fa-gift"></i>Add New Report Data
      </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
  </div>

  <div class="portlet-body form">
    <style type="text/css">
    .form-group
    {
      margin-bottom: 1px;
    }
    .control-label
    {
      font-size: 15px;
      padding-right: 0px;
      padding-left: 0px;
    }
    </style>

      <form action="{{route('water-quality-agency.water.store', $water->id)}}" class="form-horizontal" method="POST" id="updateForm">

          {{csrf_field()}}

          <div class="form-body">
              <div class="row">
                <div class="col-md-5">&nbsp;</div>
                <div class="col-md-7">
                    <div class="col-md-offset-4 col-md-4">Field test</div>
                    <div class="col-md-4">Lab test</div>
                </div>
              </div>



              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    <label class="control-label col-md-6">Ward _no: (1 digit)</label>
                    <div class="col-md-6">
                      <label class="control-label"><b>{{$water->Ward_no}}</b></label>
                    </div>

                  </div>
                </div>
                
                <div class="col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Arsenic</label>
                    <div class="col-md-4">
                      <input type="number" class="form-control input-sm" value="" name="arsenic" max="5"  step="any" />
                    </div>
                    <div class="col-md-4">
                      <input type="number" class="form-control input-sm" value="" name="as_lab" max="5"  step="any" />
                    </div>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    <label class="control-label col-md-6">Village:</label>
                    <div class="col-md-6">
                      <label class="control-label"><b>{{$water->Village}}</b></label>
                    </div>

                  </div>
                </div>
                <div class="col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Iron</label>
                    <div class="col-md-4">
                      <input type="number" class="form-control input-sm" value="" name="fe" max="2000"  step="any"/>
                    </div>
                    <div class="col-md-4">
                      <input type="number" class="form-control input-sm" value="" name="fe_lab" max="2000"  step="any"/>
                    </div>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    <label class="control-label col-md-6">TW_No: (6 digit upcode + tw sl)</label>
                    <div class="col-md-6">
                      <label class="control-label"><b>{{$water->TW_No}}</b></label>
                    </div>

                  </div>
                </div>
                <div class="col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Manganese:</label>
                    <div class="col-md-4">
                      <input type="text" class="form-control input-sm" value="" name="mn" />
                    </div>
                    <div class="col-md-4">
                      <input type="text" class="form-control input-sm" value="" name="mn_lab" />
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    <label class="control-label col-md-6">Land Owner name:</label>
                    <div class="col-md-6">
                      <label class="control-label"><b>{{$water->Landowner}}</b></label>
                    </div>

                  </div>
                </div>
                <div class="col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Chloride (Cl):</label>
                    <div class="col-md-4">
                      <input type="number" class="form-control input-sm" value="" name="cl" max="3000"  step="any"/>
                    </div>
                    <div class="col-md-4">
                      <input type="number" class="form-control input-sm" value="" name="cl_lab" max="3000"  step="any"/>
                    </div>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    <label class="control-label col-md-6">Caretaker(male) name:</label>
                    <div class="col-md-6">
                      <label class="control-label"><b>{{$water->Caretaker_male}}</b></label>
                    </div>

                  </div>
                </div>
                <div class="col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Ph</label>
                    <div class="col-md-4">
                      <input type="text" class="form-control input-sm" value="" name="ph" />
                    </div>

                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    <label class="control-label col-md-6">Caretaker (female) name:</label>
                    <div class="col-md-6">
                      <label class="control-label"><b>{{$water->Caretaker_female}}</b></label>
                    </div>

                  </div>
                </div>
                <div class="col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Pb:</label>
                    <div class="col-md-4">
                      <input type="text" class="form-control input-sm" value="" name="pb" />
                    </div>
                  </div>
                </div>
              </div>


              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    <label class="control-label col-md-6 input-sm">CDF_no: (3 digits)</label>
                    <div class="col-md-6">
                      <label class="control-label"><b>{{$water->CDF_no}}</b></label>
                    </div>

                  </div>
                </div>
                <div class="col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Zinc</label>
                    <div class="col-md-4">
                      <input type="text" class="form-control input-sm" value="" name="zinc" />
                    </div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-5">
                  <div class="form-group">
                    <label class="control-label col-md-6"></label>
                    <div class="col-md-6">
                    </div>

                  </div>
                </div>
                <div class="col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">FC</label>
                    <div class="col-md-4">
                      <input type="text" class="form-control input-sm" value="" name="fc" />
                    </div>

                  </div>
                </div>
              </div>

              <div class="row">

                <div class="col-md-offset-5 col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Turbidity</label>
                    <div class="col-md-4">
                      <input type="text" class="form-control input-sm" value="" name="turbidity" />
                    </div>

                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-offset-5 col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Td</label>
                    <div class="col-md-4">
                      <input type="text" class="form-control input-sm" value="" name="td" />
                    </div>

                  </div>
                </div>
              </div>

       <div class="row">
                <div class="col-md-offset-5 col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Latitude</label>
                    <div class="col-md-4">
                      <input type="number" step="any" min=15 max=30 class="form-control input-sm" value="" name="lat" />
                    </div>

                  </div>
                </div>
              </div>


                     <div class="row">
                <div class="col-md-offset-5 col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Longitude</label>
                    <div class="col-md-4">
                      <input type="number"  step="any" min=80 max=100 class="form-control input-sm" value="" name="lon" />
                    </div>

                  </div>
                </div>
              </div>
    <div class="row">
                <div class="col-md-offset-5 col-md-7">
                  <div class="form-group">
                    <label class="control-label col-md-4">Date Of Test: YYYY-MM-DD <font color="red"> * </font></label>
                    <div class="col-md-4">
                      <input type="text" class="form-control input-sm date-picker" data-date-format="yyyy-mm-dd" value="" name="report_date" required="required" />
                    </div>

                  </div>
                </div>
              </div>

              <div class="row">
                <div class="form-group">
                  <label class="control-label col-md-1">&nbsp;</label>
                  <p class="col-md-8 form-control-static"> Pls collect approval date from UP if it is blank or N/A <br/> Approval date means the date HYSAWA approved this TW </p>
                </div>
              </div>
          </div>
          <div class="form-actions">
              <div class="col-md-offset-2 col-md-10">
                  <input type="submit" class="btn green" value="Add New Report Data" />
              </div>
          </div>
      </form>
  </div>
</div>

@if( isset($water->qualityResults) && count($water->qualityResults) > 0)

  <div class="portlet light tasks-widget bordered">
    <div class="portlet-body util-btn-margin-bottom-5">

      <p>Report Data History</p>
      <div class="table-responsive">
        <table class="table table-bordered table-hover" id="example0">
          <thead class="flip-content">
            <th style="font-size: 12px; white-space: nowrap;" >Action</th>

            <th style="font-size: 12px; white-space: nowrap; " >Date</th>

            <th style="font-size: 12px; white-space: nowrap; " >Arsenic</th>
            <th style="font-size: 12px; white-space: nowrap; " >Arsenic Lab</th>

            <th style="font-size: 12px; white-space: nowrap; " >Iron (Fe)</th>
            <th style="font-size: 12px; white-space: nowrap; " >Fe Lab</th>

            <th style="font-size: 12px; white-space: nowrap;" >Mn</th>
            <th style="font-size: 12px; white-space: nowrap;" >Mn Lab</th>

            <th style="font-size: 12px; white-space: nowrap;" >Cl</th>
            <th style="font-size: 12px; white-space: nowrap;" >Cl Lab</th>

            <th style="font-size: 12px; white-space: nowrap;" >Ph</th>
            <th style="font-size: 12px; white-space: nowrap;" >Pb</th>
            <th style="font-size: 12px; white-space: nowrap;" >Zinc</th>

            <th style="font-size: 12px; white-space: nowrap;" >Fc</th>
            <th style="font-size: 12px; white-space: nowrap;" >Td</th>
            <th style="font-size: 12px; white-space: nowrap;" >Turbidity</th>
            <th style="font-size: 12px; white-space: nowrap;" >Latitude</th>
            <th style="font-size: 12px; white-space: nowrap;" >Longitude</th>


          </thead>
          <tbody>
            @foreach($water->qualityResults as $water)
            <tr>
              <td nowrap="nowrap">

                @if($water->report_date > '2017-12-31')
                <a class="label label-info" href="{{route('water-quality-agency.water.edit', $water->id)}}" >
                  <i class="fa fa-pensil" ></i>Edit
                </a>
      @endif
              </td>

              <td style="font-size: 12px;" >{{$water->report_date}}</td>

              <td style="font-size: 12px;" >{{$water->arsenic}}</td>
              <td style="font-size: 12px;" >{{$water->as_lab}}</td>

              <td style="font-size: 12px;" >{{$water->fe}}</td>
              <td style="font-size: 12px;" >{{$water->fe_lab}}</td>

              <td style="font-size: 12px;" >{{$water->mn}}</td>
              <td style="font-size: 12px;" >{{$water->mn_lab}}</td>

              <td style="font-size: 12px;" >{{$water->cl}}</td>
              <td style="font-size: 12px;" >{{$water->cl_lab}}</td>

              <td style="font-size: 12px;" >{{$water->ph}}</td>
              <td style="font-size: 12px;" >{{$water->pb}}</td>
              <td style="font-size: 12px;" >{{$water->zinc}}</td>

              <td style="font-size: 12px;" >{{$water->fc}}</td>
              <td style="font-size: 12px;" >{{$water->td}}</td>

              <td style="font-size: 12px;" >{{$water->turbidity}}</td>

             <td style="font-size: 12px;" >{{$water->lat}}</td>

            <td style="font-size: 12px;" >{{$water->lon}}</td>

            </tr>
            @endforeach
          </tbody>
        </table>


      </div>
    </div>
  </div>


@else
  <div class="portlet light tasks-widget bordered">
    <p>
      No Data Found
    </p>
  </div>
@endif




@endsection

