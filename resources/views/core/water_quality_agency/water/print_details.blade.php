@extends('layouts.print.app')

@section('my_style')

<style>
    table{border-collapse: collapse;border-spacing: 0;}
    div{font-size: 80%;display: inline-block;}
    .headline th {padding: 5px 20px;}
  </style>
@endsection


@section('content')



@if(isset($water) && count($water))

<h1>Water Info:</h1>

<table class="box" border="1" width="100%">
    <tr>
      <th width="20%">Ward _no: (1 digit)</th>
      <td>{{$water->Ward_no or ''}}</td>
    </tr>

    <tr>
      <th width="20%">Village:</th>
      <td>{{$water->Village}}</td>
    </tr>

    <tr>
      <th width="20%">TW_No: (6 digit upcode + tw sl)</th>
      <td>{{$water->TW_No}}</td>
    </tr>

    <tr>
      <th width="20%">Land Owner name:</th>
      <td>{{$water->Landowner}}</td>
    </tr>

    <tr>
      <th width="20%">Caretaker(male) name:</th>
      <td>{{$water->Caretaker_male}}</td>
    </tr>

    <tr>
      <th width="20%">Caretaker (female) name:</th>
      <td>{{$water->Caretaker_female}}</td>
    </tr>
    
    <tr>
      <th width="20%">CDF_no: (3 digits)</th>
      <td>{{$water->{{$water->CDF_no}}}}</td>
    </tr>

</table> 



@endif 

@if( isset($water->qualityResults) && count($water->qualityResults) > 0)
<h3>Water Quality Report List</h3>
<table class="box" border="1" width="100%">
    <thead>
    <tr>
      <th >Date</th>
      <th >Arsenic</th>
      <th >Arsenic Lab</th>
      <th >Iron (Fe)</th>
      <th >Fe Lab</th>
      <th >Mn</th>
      <th >Mn Lab</th>
      <th >Cl</th>
      <th >Cl Lab</th>
      <th >Ph</th>
      <th >Pb</th>
      <th >Zinc</th>
      <th >Fc</th>
      <th >Td</th>
      <th >Turbidity</th>
    </tr>
    </thead>
    <tbody>
      @foreach($water->qualityResults as $water)
      <tr>
        <td >{{$water->report_date}}</td>
        <td  >{{$water->arsenic}}</td>
        <td  >{{$water->as_lab}}</td>
        <td  >{{$water->fe}}</td>
        <td  >{{$water->fe_lab}}</td>
        <td  >{{$water->mn}}</td>
        <td  >{{$water->mn_lab}}</td>
        <td  >{{$water->cl}}</td>
        <td  >{{$water->cl_lab}}</td>
        <td  >{{$water->ph}}</td>
        <td  >{{$water->pb}}</td>
        <td  >{{$water->zinc}}</td>
        <td  >{{$water->fc}}</td>
        <td  >{{$water->td}}</td>
        <td  >{{$water->turbidity}}</td>
      </tr>
      @endforeach
    </tbody>
  </table>

@endif 

@endsection
