@if(count($waters) > 0)
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <div class="portlet-body util-btn-margin-bottom-5">
      <h4 class="text-red">If no site list is displayed, ask UP to enter TW site list</h4>
      <div class="table-responsive">
        <table class="table table-bordered table-hover" id="example0">
          <thead class="flip-content">
            <th style="font-size: 12px; white-space: nowrap;" >Action</th>

            <th style="font-size: 12px; white-space: nowrap; " >Ward no</th>
            <th style="font-size: 12px; white-space: nowrap; " >CDF_no</th>
            <th style="font-size: 12px; white-space: nowrap; " >Village</th>
            <th style="font-size: 12px; white-space: nowrap; " >TW NO</th>
                    <th style="font-size: 12px; white-space: nowrap; " >Date</th>
            <th style="font-size: 12px; white-space: nowrap; " >Land owner</th>

            <th style="font-size: 12px; white-space: nowrap; " >Caretaker</th>
            <th style="font-size: 12px; white-space: nowrap; " >X Coordinate</th>
            <th style="font-size: 12px; white-space: nowrap; " >Y Coordinate</th>
            <th style="font-size: 12px; white-space: nowrap; " >Arsenic</th>
            <th style="font-size: 12px; white-space: nowrap; " >Iron (Fe)</th>

            <th style="font-size: 12px; white-space: nowrap;" >Mn</th>
            <th style="font-size: 12px; white-space: nowrap;" >Cl</th>
            <th style="font-size: 12px; white-space: nowrap;" >Ph</th>
            <th style="font-size: 12px; white-space: nowrap;" >Pb</th>
            <th style="font-size: 12px; white-space: nowrap;" >Zinc</th>

            <th style="font-size: 12px; white-space: nowrap;" >Fc</th>
            <th style="font-size: 12px; white-space: nowrap;" >Td</th>
            <th style="font-size: 12px; white-space: nowrap;" >Turbidity</th>


          </thead>
          <tbody>
            @foreach($waters as $water)
            <tr>
              <td nowrap="nowrap">

                <a class="label label-info" href="{{route('water-quality-agency.water.show', $water->id)}}" >
                  <i class="fa fa-eye" ></i>
                </a>

              </td>

              <td style="font-size: 12px;" >{{$water->Ward_no}}</td>
              <td style="font-size: 12px;" >{{$water->CDF_no}}</td>
              <td style="font-size: 12px;" >{{$water->Village}}</td>
              <td style="font-size: 12px;" >{{$water->id}}</td>
              <td style="font-size: 12px;" >{{$water->App_date}}</td>
              <td style="font-size: 12px;" >{{$water->Landowner}}</td>

              <td style="font-size: 12px;" >{{$water->Caretaker_male}}</td>
              <td style="font-size: 12px;" >{{$water->x_coord}}</td>
              <td style="font-size: 12px;" >{{$water->y_coord}}</td>
              <td style="font-size: 12px;" >{{$water->wq_Arsenic}}</td>
              <td style="font-size: 12px;" >{{$water->wq_fe}}</td>

              <td style="font-size: 12px;" >{{$water->wq_mn}}</td>
              <td style="font-size: 12px;" >{{$water->wq_cl}}</td>
              <td style="font-size: 12px;" >{{$water->wq_ph}}</td>
              <td style="font-size: 12px;" >{{$water->wq_pb}}</td>
              <td style="font-size: 12px;" >{{$water->wq_zinc}}</td>

              <td style="font-size: 12px;" >{{$water->wq_fc}}</td>
              <td style="font-size: 12px;" >{{$water->wq_td}}</td>
              <td style="font-size: 12px;">{{$water->wq_turbidity}}</td>

            </tr>
            @endforeach
          </tbody>
        </table>

        <div class="pagination pull-right">
          {{$waters->appends($old)->links()}}
        </div>

      </div>
    </div>
  </div>
</div>

@else
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <p>
      No Data Found
    </p>
  </div>
</div>
@endif