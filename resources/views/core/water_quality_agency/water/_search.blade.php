<div class="portlet box green-seagreen">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Water <small> view</small></div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>

        <div class="portlet-body form">
        <style type="text/css">
        .form-group{
            margin-bottom: 5px;
        }
        .control-label{
            font-size: 10px;
        }
    </style>
            <form action="{{route('water-quality-agency.water.index')}}" class="form-horizontal" method="GET" id="searchForm">
                <input type="hidden" name="query" value="{{time()}}" />

                <div class="form-body">

                <div class="row">

                    <div class="col-md-2">
                        <label>Region</label>
                        <select class="form-control input-sm" name="region_id" id="region_id">
                            <?php
                                $regions = \DB::table('region')->get();
                            ?>
                            <option value="">Select Region</option>
                            @foreach($regions as $region)
                                <option value="{{$region->id}}"
                                @if(isset($old['region_id']) && $old['region_id'] == $region->id)
                                    selected="selected"
                                @endif
                                >{{$region->region_name}}</option>
                            @endforeach
                        </select>
                    </div>


                    <div class="col-md-2">
                        <label>District</label>
                        <select class="form-control input-sm" name="district_id" id="district_id">
                            @if(isset($_REQUEST['region_id']) && $_REQUEST['region_id'] != "")

                                <?php
                                    $districts = \DB::table('fdistrict')->where('region_id', $_REQUEST['region_id'])->get();
                                ?>

                                <option value="">Select Region</option>
                                @foreach($districts as $district)
                                    <option value="{{$district->id}}">{{$district->distname}}</option>
                                @endforeach


                            @else
                                <option value="">Select Region</option>
                            @endif

                        </select>
                    </div>

                    <div class="col-md-2">
                        <label>Upazila</label>
                        <select class="form-control input-sm" name="upazila_id" id="upazila_id">
                            @if(empty($old['upazila_id']))
                                <?php
                                    $fupazilas = \DB::table('fupazila')
                                                    ->where('disid', $old['district_id'])
                                                    ->get();
                                ?>
                                @foreach($fupazilas as $fupazila)
                                    <option value="{{$fupazila->id}}"
                                    @if($old['upazila_id'] == $fupazila->id)
                                        selected="selected"
                                    @endif
                                    >{{$fupazila->upname}}</option>
                                @endforeach
                            @else
                                <option value="">Select District </option>
                            @endif
                        </select>

                    </div>

                    <div class="col-md-2">
                        <label>Union</label>
                        <select class="form-control input-sm" name="union_id" id="union_id">
                            @if(empty($old['district_id']))
                                <?php
                                $funions = \DB::table('funion')
                                ->where('upid', $old['upazila_id'])
                                ->get();
                                ?>

                                @foreach($funions as $funion)
                                <option value="{{$funion->id}}"
                                    @if($old['union_id'] == $funion->id)
                                    selected="selected"
                                    @endif
                                    >{{$funion->unname}}</option>
                                @endforeach

                                @else
                                    <option value="">Select Upazila </option>
                                @endif
                        </select>
                    </div>

                    <div class="col-md-2">
                        <label>ID No</label>
                        <input
                            type="text"
                            class="form-control input-sm"
                            name="TW_No"
                            placeholder="ID No"

                            @if(!empty( $old['TW_No']))
                            value="{{$old['TW_No']}}"
                            @endif
                        />
                    </div>

                    <div class="col-md-2">
                        <label>CDF No</label>
                        <input
                            type="text"
                            class="form-control input-sm"
                            name="CDF_no"
                            placeholder="CDF No"

                            @if(!empty( $old['CDF_no']))
                            value="{{$old['CDF_no']}}"
                            @endif
                        />
                    </div>



  <div class="col-md-0">
                        <label> </label>
                       
                    </div>

                        <div class="form-group">
                            <label class="col-md-2">Approval Date(YYY-MM-DD)</label>
                            <div class="col-md-2">

                                <input class="form-control date-picker input-sm" type="text" name="App_date" data-date-format="yyyy-mm-dd"
                                @if(!empty( $old['App_date']))
                                value="{{date('Y-m-d', strtotime($old['App_date']))}}"
                                @endif >

                            </div>
                        </div>
                    </div>

           

                    
                <div class="row">
                    <div class="col-md-6">
                        <input type="submit" class="btn green btn-sm" value="Search" />
                        <a href="{{route('water-quality-agency.water.index')}}" class="btn default btn-sm">Clear</a>
                   @if(isset($_REQUEST['query']) && $_REQUEST['query'] != "")
                        <a href="{{route('water-quality-agency.water.printdata', $_REQUEST)}}" class="btn btn-info btn-sm pull-right">Print</a>
                    
                     @endif
                    </div>
                </div>

            </div>

            </form>
        </div>
    </div>