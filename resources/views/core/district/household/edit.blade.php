@extends('layouts.appinside')

@section('content')


<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('district-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="{{ route('district-admin.water_household.index') }}">Sanitation</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Update</span>
    </li>
  </ul>
</div>

<h1 class="page-title"> Sanitation
  <small>Update Info</small>
</h1>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-share "></i>
            <span class="caption-subject bold ">Household Latrine Entry <small></small> </span>
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body form">
  <style type="text/css">
    .form-group
    {
      margin-bottom: 1px;
    }
    .control-label
    {
      font-size: 15px;
      padding-right: 0px;
      padding-left: 0px;
    }
  </style>
<script>
function valaditionF(){
var data = $("#verify_num").val();
if(data.length >= 12){

  alert("Please Enter a valid number !");
}else{
  
}
console.log(data.length);
//;
}
</script>

{!! Form::model($WaterHousehold, array('url' => route('district-admin.water_household.update', $WaterHousehold->id ), 'method' => 'put', 'class' => 'form-horizontal')) !!}

<div class="form-body">

  <div class="row">

    @include('partials.errors')

   
  <div class="col-md-6">

<div class="form-group">
  <label class="control-label col-md-6">CDF No:</label>
  <div class="col-md-6">
  {!! Form::number('cdfno', null, ['class' => 'form-control input-sm', 'placeholder' => 'CDF No:', 'required' => 'required']) !!}
  </div>
</div>

<div class="form-group">
  <label class="control-label col-md-6">Village Name</label>
  <div class="col-md-6">
  {!! Form::text('village', null, ['class' => 'form-control input-sm', 'placeholder' => 'Village Name', 'required' => 'required']) !!}
  </div>
</div>

<div class="form-group">
  <label class="control-label col-md-6">HH Head Name </label>
  <div class="col-md-6">
  {!! Form::text('hh_name', null, ['class' => 'form-control input-sm', 'placeholder' => 'HH Head Name', 'required' => 'required']) !!}
  </div>
</div>

   <div class="form-group">
  <label class="control-label col-md-6">Father/Husband</label>
  <div class="col-md-6">
  {!! Form::text('father_husband', null, ['class' => 'form-control input-sm', 'placeholder' => 'Father/Husband', 'required' => 'required']) !!}
  </div>
</div>


<div class="form-group">
  <label class="control-label col-md-6">Age:</label>
  <div class="col-md-6">
  {!! Form::number('age', null, ['class' => 'form-control input-sm', 'placeholder' => 'Age', 'required' => 'required', 'max'=>'100']) !!}
  </div>
</div>

<div class="form-group">
  <label class="control-label col-md-6">Occupation</label>
  <div class="col-md-6">
  {!! Form::text('occupation', null, ['class' => 'form-control input-sm', 'placeholder' => 'Occupation', 'required' => 'required' ]) !!}
  </div>
</div>

 <div class="form-group">
  <label class="control-label col-md-6">Mobile:</label>
  <div class="col-md-6">
  {!! Form::number('mobile', null, ['id' => 'verify_num','onkeyup' => 'valaditionF()', 'class' => 'form-control input-sm', 'placeholder' => 'Phone no', 'required' => 'required']) !!}
  </div>
</div>

<div class="form-group">
    <label class="control-label col-md-6">Economic Status:</label>
    <div class="col-md-6">
    {{ Form::radio('economic_status', 'Rich') }}Rich
    {{ Form::radio('economic_status', 'Mid Income') }} Mid Income
    {{ Form::radio('economic_status', 'Poor') }} Poor
    {{ Form::radio('economic_status', 'Hc Poor') }} Hc Poor
    </div>
  </div>

    <div class="form-group">
    <label class="control-label col-md-6">Under social safety net:</label>
    <div class="col-md-6">
    {{ Form::radio('social_safetynet', 'YES') }}YES
    {{ Form::radio('social_safetynet', 'NO') }} NO
    </div>
  </div>

<div class="form-group">
  <label class="control-label col-md-6">Male:</label>
  <div class="col-md-6">
  {!! Form::number('male', null, ['class' => 'form-control input-sm', 'placeholder' => 'Male', 'required' => 'required','min'=>"0" ]) !!}
  </div>
</div>

<div class="form-group">
  <label class="control-label col-md-6">Female:</label>
  <div class="col-md-6">
  {!! Form::number('female', null, ['class' => 'form-control input-sm', 'placeholder' => 'Female', 'required' => 'required','min'=>"0" ]) !!}
  </div>
</div>

<div class="form-group">
  <label class="control-label col-md-6">Children:</label>
  <div class="col-md-6">
  {!! Form::number('children', null, ['class' => 'form-control input-sm', 'placeholder' => 'Children', 'required' => 'required','min'=>"0" ]) !!}
  </div>
</div>


<div class="form-group">
  <label class="control-label col-md-6">Disable:</label>
  <div class="col-md-6">
  {!! Form::number('disable', null, ['class' => 'form-control input-sm', 'placeholder' => 'Disable', 'required' => 'required','min'=>"0" ]) !!}
  </div>
</div>



</div>

<div class="col-md-6">
<div class="form-group">
 
<div class="form-group">
    <label class="control-label col-md-6">Ownership type:</label>
    <div class="col-md-6">
    {{ Form::radio('ownership_type', 'Personal') }}Personal
    {{ Form::radio('ownership_type', 'Joint') }} Joint
    {{ Form::radio('ownership_type', 'Other') }} Other
    </div>
  </div>



<div class="form-group">
    <label class="control-label col-md-6">Present Latrine type:</label>
    <div class="col-md-6">
    {{ Form::radio('latrine_type', 'Kutcha') }}Kutcha
    {{ Form::radio('latrine_type', 'Pacca') }} Pacca
    {{ Form::radio('latrine_type', 'Other') }} Other
    </div>
  </div>

  <div class="form-group">
  <label class="control-label col-md-6">Latrine Details:</label>
  <div class="col-md-6">
  {!! Form::text('latrine_details', null, ['class' => 'form-control input-sm', 'placeholder' => 'Latrine Details', 'required' => 'required']) !!}
  </div>
</div>

 <div class="form-group">
  <label class="control-label col-md-6">Total Cost Required:</label>
  <div class="col-md-6">
  {!! Form::number('total_cost', null, ['class' => 'form-control input-sm', 'placeholder' => ' Cost Required', 'required' => 'required','max'=>'100000']) !!}
  </div>
</div>

<div class="form-group">
  <label class="control-label col-md-6">Contribution by HH(TK):</label>
  <div class="col-md-6">
  {!! Form::number('contribute_amount', null, ['class' => 'form-control input-sm', 'placeholder' => ' Contribution by HH', 'required' => 'required','max'=>'10000']) !!}
  </div>
</div>


<div class="form-group">
  <label class="control-label col-md-6"> Latitude(DD.mmmmmm):</label>
  <div class="col-md-6">
  {!! Form::number('latitude', null, ['class' => 'form-control input-sm', 'placeholder' => 'Latitude', 'required' => 'required','step'=>"any",'max'=>'31']) !!}
  </div>
</div>

<div class="form-group">
  <label class="control-label col-md-6">Longitude(DD.mmmmmm):</label>
  <div class="col-md-6">
  {!! Form::number('longitude', null, ['class' => 'form-control input-sm', 'placeholder' => 'Longitude', 'required' => 'required','step'=>"any",'max'=>'101']) !!}
  </div>
</div>

<div class="form-group">
  <label class="control-label col-md-6">Approval date (YYYY-MM-DD):</label>
  <div class="col-md-6">
  {!! Form::text('app_date', null, ['class' => 'form-control date-picker input-sm', 'placeholder' => 'Approval date', 'required' => 'required', 'data-date-format' =>"yyyy-mm-dd"]) !!}
  </div>
</div>

<div class="form-group">
  <label class="control-label col-md-6">Approval Status:</label>
  <div class="col-md-6">
  {!! Form::select('app_status',array(''=>'Approval Status')+$appstatus, null , ['class' => 'form-control input-sm', 'placeholder' => 'Approval Status', 'required' => 'required']) !!}
  </div>
</div>

<div class="form-group">
  <label class="control-label col-md-6">Implementation Status</label>
  <div class="col-md-6">
  {!! Form::select('imp_status', array(''=>'Select Implementation Status') + $ImplementStatus,null, ['class' => 'form-control input-sm', 'required' => 'required']) !!}
  </div>
</div>


</div>

  </div>




</div>
</div>



    </div>

  </div>

  <div class="form-actions">
    <div class="col-md-offset-3">
      <a href="javascript:history.back()" class="btn default"> Cancel </a>
      {!! Form::submit('Update', ['class' => 'btn green pull-left']) !!}
    </div>
  </div>

</div>

{!! Form::close() !!}


</div>

<script type="text/javascript">
  $(document ).ready(function() {
    highlight_nav('warehouse-manage', 'shelfs');
  });
</script>

@endsection
