@extends('layouts.appinside')

@section('content')


<?php
  $wSubmitted = App\Model\Water::select(\DB::raw('count(*) as c'))
        ->where('region_id', \Auth::user()->region_id)
        ->get()
        ->first();
  $aSubmitted = App\Model\Water::select(\DB::raw('count(*) as c'))
        ->where('app_status', 'Approved')
        ->where('region_id', \Auth::user()->region_id)
        ->get()
        ->first();

 $asSubmitted = App\Model\Water::select(\DB::raw('count(*) as c'))
        ->where('app_status', 'Assessed')
        ->where('region_id', \Auth::user()->region_id)
        ->get()
        ->first();
  $cSubmitted = App\Model\Water::select(\DB::raw('count(*) as c'))
        ->where('imp_status', 'Completed')
        ->where('region_id', \Auth::user()->region_id)
        ->get()
        ->first();
?>


<div class="row widget-row">
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 20px;">
                <div class="row">
                    <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                        <i class="fa fa-inbox fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{$wSubmitted->c}}</div>
                        <div># of Water Scheme Submitted</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 20px;">
                <div class="row">
                    <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                        <i class="fa fa-list-alt fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{$aSubmitted->c}}</div>
                        <div># of Water Scheme Approved</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 20px;">
                <div class="row">
                    <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                        <i class="fa fa-compress fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{$cSubmitted->c}}</div>
                        <div># of Water Scheme Implemented</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row widget-row">


<?php
  $swAmount = App\Model\Sanitation::select(\DB::raw('count(*) as c'))
    ->where('region_id', \Auth::user()->region_id)
    ->get()
    ->first();
  $saAmount = App\Model\Sanitation::select(\DB::raw('count(*) as c'))
    ->where('app_status', 'Approved')
    ->where('region_id', \Auth::user()->region_id)
    ->get()
    ->first();

    $sasAmount = App\Model\Sanitation::select(\DB::raw('count(*) as c'))
    ->where('app_status', 'Assessed')
    ->where('region_id', \Auth::user()->region_id)
    ->get()
    ->first();

  $scAmount = App\Model\Sanitation::select(\DB::raw('count(*) as c'))
    ->where('imp_status', 'Completed')
    ->where('region_id', \Auth::user()->region_id)
    ->get()
    ->first();

?>
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 20px;">
                <div class="row">
                    <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                        <i class="fa fa-inbox fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{$swAmount->c}}</div>
                        <div># of Sanitary Scheme Submitted</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 20px;">
                <div class="row">
                    <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                        <i class="fa fa-inbox fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{$saAmount->c}}</div>
                        <div># of Sanitary Scheme Approved</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 20px;">
                <div class="row">
                    <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                        <i class="fa fa-inbox fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{$scAmount->c}}</div>
                        <div># of Sanitary Scheme Implemented</div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>
<div class="row widget-row">


<?php
  $swAmount = App\Model\Household::select(\DB::raw('count(*) as c'))
    ->where('region_id', \Auth::user()->region_id)
    ->get()
    ->first();
  $saAmount = App\Model\Household::select(\DB::raw('count(*) as c'))
    ->where('app_status', 'Approved')
    ->where('region_id', \Auth::user()->region_id)
    ->get()
    ->first();

    $sasAmount = App\Model\Household::select(\DB::raw('count(*) as c'))
    ->where('app_status', 'Assessed')
    ->where('region_id', \Auth::user()->region_id)
    ->get()
    ->first();

  $scAmount = App\Model\Household::select(\DB::raw('count(*) as c'))
    ->where('imp_status', 'Completed')
    ->where('region_id', \Auth::user()->region_id)
    ->get()
    ->first();

?>
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 20px;">
                <div class="row">
                    <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                        <i class="fa fa-inbox fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{$swAmount->c}}</div>
                        <div># of Household Latrine Scheme Submitted</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 20px;">
                <div class="row">
                    <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                        <i class="fa fa-inbox fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{$saAmount->c}}</div>
                        <div># of Household Latrine Scheme Approved</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading" style="padding: 20px;">
                <div class="row">
                    <div class="col-xs-3" style="padding: 10px 0px 0px 0px">
                        <i class="fa fa-inbox fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{{$scAmount->c}}</div>
                        <div># of Household Latrine Scheme Implemented</div>
                    </div>
                </div>
            </div>
        </div>
    </div>


</div>


@endsection