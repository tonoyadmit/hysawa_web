@extends('layouts.appinside')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('district-admin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Finance</span>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Demand</span>
        </li>
    </ul>
</div>


 <div class="col-md-12" style="margin-top: 10px">
      @include('core.district.up-demand.fundlist._search')
  </div>

  
<div class="portlet light bordered" style="margin-top: 10px;">

    <div class="portlet-title">
        <div class="caption">
            <i class="icon-share font-dark"></i>
            <span class="caption-subject font-dark bold uppercase">FUND REQUEST LIST</span>
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
    </div>

    <div class="portlet-body">

        <p>
            {{--  <a href="{{route('district-admin.fundlist.up-demand.fund_list.create')}}" class="btn btn-large btn-success"> Add new Entry</a>  --}}
          
           {{--  <a href="{{route('district-admin.fundlist.up-demand.finace_demand.print')}}" class="btn btn-large btn-success"> Print</a>  --}}

        </p>

        @if(count($FinanceDemands) > 0)

        <table class="table table-bordered table-hover" id="example0">

            <thead class="flip-content">
                <th>Action</th>
                <th>SL</th>
                <th>Project</th>
                <th>District</th>
                <th>Upazila</th>
                <th>Union</th>
                <th>ID</th>
                <th>From Date</th>
                 <th>To Date</th>
       
            </thead>

            <tbody>
        
                @foreach($FinanceDemands as $FinanceData)
                <tr>
                    <td>
                        {{--  <a class="label label-success" href="{{route('district-admin.fundlist.up-demand.fund_list.edit', $FinanceData->id )}}">
                            <i class="fa fa-pencil"></i> Edit
                        </a>  --}}
  
                           <a class="label label-success" href="{{ route('district-admin.fundlist.up-demand.finace_demand.index',  ['fid' => $FinanceData->id,'fdate='.$FinanceData->fdate,'tdate='.$FinanceData->tdate,'proj_id='.$FinanceData->proj_id,'distid='.$FinanceData->distid,'upid='.$FinanceData->upid,'unid='.$FinanceData->unid] )}}">
                            <i class="fa fa-pencil"></i> View Details
                        </a>


                    </td>


     

                    <td nowrap="nowrap">{{ $loop->iteration }}</td>
         <?php
            $project = App\Model\Project::find(auth()->user()->proj_id);
        ?>

                            <td nowrap="nowrap">{{ $project->project }}</td>
                          <?php   
                        $districts = App\Model\District::where('id', $FinanceData->distid )->get();
                            ?>
    
                       @foreach($districts as $district )                     
            @if(\Auth::check())
            <td nowrap="nowrap">{{ $district->distname }}</td>
            @endif
        @endforeach

           
            <?php                        
          $upazilas = App\Model\Upazila::where('id',  $FinanceData->upid)->get();
           ?>
         @foreach($upazilas as $upazila )                     
            @if(\Auth::check())

              <td nowrap="nowrap">{{ $upazila->upname }}</td>
            @endif
        @endforeach

 <?php                        
          $unions = App\Model\Union::where('id',  $FinanceData->unid)->get();
           ?>
         @foreach($unions as $union )                     
            @if(\Auth::check())
    
              <td nowrap="nowrap">{{ $union->unname }}</td>
            @endif
        @endforeach


                      <td nowrap="nowrap">{{ $FinanceData->id}}</td>
                    <td nowrap="nowrap">{{ $FinanceData->fdate }}</td>
                     <td nowrap="nowrap">{{ $FinanceData->tdate }}</td>
                   

                </tr>
                @endforeach
            </tbody>
        </table>
               <div class="pagination pull-right">
          {{$FinanceDemands->appends($old)->links()}}
        </div>
        
        @else
        <p>No Data Found</p>
        @endif

    </div>
</div>




@endsection



@section('my_js')
  <script type="text/javascript">
    $(document).ready(function () {

      $('#district_id').on('change', function() {
        var head_id = $(this).val();

        $("#upazila_id").html("Select District");
        $("#union_id").html("Select Upazila");

        $.ajax({
          url: '{{route('district-admin.ajax.district-upazila')}}?district_id='+head_id+'&not_all=true&choose_one=true',
          type: 'GET',
          success: function(response){
            if(response['status'] == true){
              $("#upazila_id").html(response['upazila_list']);
            }
          }
        });
      });

      $('#upazila_id').on('change', function() {
        var upazila_id = $(this).val();

        $("#union_id").html("Select Upazila");

        $.ajax({
          url: '{{route('district-admin.ajax.district-union')}}?upazila_id='+upazila_id+'&not_all=true&choose_one=true',
          type: 'GET',
          success: function(response){
            if(response['status'] == true){
              $("#union_id").html(response['union_list']);
            }
          }
        });
      });
    });
  </script>

  @endsection

  