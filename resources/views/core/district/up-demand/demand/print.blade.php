@extends('layouts.print.app')
@section('my_style')
<style>
.footer {
    position: fixed;
    left: 0;
    bottom: 0;
    width: 100%;

    text-align: center;
}
</style>
<style>
    table{border-collapse: collapse;border-spacing: 0;text-align: right;}
    div{font-size: 80%;display: inline-block;}
    .headline th {padding: 5px 20px;}
    .text-right{text-align: right;}
    .text-left{text-align: left;}
</style>

<style media="screen">
    .table-filtter .btn{ width: 100%;}
    .table-filtter {margin: 20px 0;}
    .table-bordered, .table-bordered > tbody > tr > td, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > td,
    .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > thead > tr > th {border: 1px solid #000;}
</style>

@endsection
<?php
@$union = App\Model\Union::with('upazila.district')->find(\Auth::user()->unid);
@$districtName = $union->upazila->district->distname;
@$upazilaName = $union->upazila->upname;
@$unionName = $union->unname;
@$projectName = App\Model\Project::find(\Auth::user()->proj_id);
@$region = App\Model\Region::find(\Auth::user()->region_id);
?>

@section('content')

<p style="text-align: center;  font-size: 20px;    font-family: "Times New Roman", Times, serif;"><b>FUND REQUEST<b></p>
<table class="table table-bordered table-hover" style="width:100%" border="1">
    <tr>
        <td colspan="8" style="text-align:left;">
        <?php
            $project = App\Model\Project::find(auth()->user()->proj_id);
        ?>
            Project:{{ $project->project ?: '' }}

        </td>
    </tr>

 
      @if(count($fundlistdata) > 0)
   @foreach($fundlistdata as $fdates)

      <tr>
        <td colspan="8" style="text-align:left;">

         <?php                        
          $districts = App\Model\District::where('id', $fdates->distid )->get();
           ?>
         @foreach($districts as $district )                     
            @if(\Auth::check())
            District:{{  $district->distname }}
            @endif
        @endforeach
           
        </td>
    </tr>
    <tr>
        <td colspan="8" style="text-align:left;">

         <?php                        
          $upazilas = App\Model\Upazila::where('id',  $fdates->upid)->get();
           ?>
         @foreach($upazilas as $upazila )                     
            @if(\Auth::check())
            Upazila:{{  $upazila->upname }}
            @endif
        @endforeach

    
        </td>
    </tr>
    <tr>
        <td colspan="8" style="text-align:left;">

            <?php                        
          $unions = App\Model\Union::where('id',  $fdates->unid)->get();
           ?>
         @foreach($unions as $union )                     
            @if(\Auth::check())
            Union:{{  $union->unname }}
            @endif
        @endforeach

          
        </td>
    </tr>


 <tr>
        <td colspan="8" style="text-align:left;">
            @if(\Auth::check())
            From Date:{{ $fdates->fdate }}
            @endif
        </td>
    </tr>

     <tr>
        <td colspan="8" style="text-align:left;">
            @if(\Auth::check())
            To Date:{{$fdates->tdate }}
            @endif
        </td>
    </tr>
           @endforeach
           @endif
 
</table>


        @if(count($FinanceDemands) > 0)

  <table class="box" border="1" width="100%">

            <thead>
             
                <th>SL</th>
                <th>ID</th>
                <th>Head</th>
                <th>Sub Head</th>
                <th>Item</th>
                <th>Amount</th>
                {{--  <th>From Date</th>
                 <th>To Date</th>  --}}
                <th>Details</th>
                 <th>Status</th>
  
            </thead>

            <tbody>
               
                 <?php $sum = 0 ?>
                @foreach($FinanceDemands as $FinanceData)
                <tr>
                   
                    <td>{{ $loop->iteration }}</td>
                      <td>{{ $FinanceData->id}}</td>
                    <td>{{ $FinanceData->getHead->headname }}</td>
                    <td>{{ $FinanceData->getSubhead->sname }}</td>
                    <td>{{ $FinanceData->getItem->itemname }}</td>
                    <td>{{ $FinanceData->amount }}</td>
                    {{--  <td>{{ $FinanceData->date }}</td>
                     <td>{{ $FinanceData->todate }}</td>  --}}
                    <td>{{ $FinanceData->remarks }}</td>
                  <td>{{ $FinanceData->app_status }}</td>

                    </td>

                </tr>

                <?php 
                if ( $FinanceData->app_status != "Rejected")
                $sum += $FinanceData->amount 
                
                ?>

                @endforeach
                  @endif
                    <tr>
                    <td> </td>
                     <td> </td>
                      <td> </td>
                       <td> </td>
                  
                      
                     <td>Amount Recommended (Excluding Rejected))</td>
                       <td>{{ $sum }}</td>

                          <td>  </td>
                         <td>  </td>
                   </tr> 
                       
                   
            </tbody>
        </table>
    <p>*Online submission by UP. Signed hard copy preserved at UP*</p>
 <p> </p>
    <p>Commented By:</p>
    @if(!empty($Comments))

<table class="box" border="1" width="100%">

          <thead>
           
          <th style="text-align:left;">Name - Designation</th>
        <th style="text-align:left;">Comment</th>

          </thead>

          <tbody>
             
               <?php $sum = 0 ?>
               @foreach($Comments as $CommentsData)
              <tr>
                 
                  <td style="text-align:left;">{{ $CommentsData->names }}</td>
                    <td style="text-align:left;">{{ $CommentsData->comment }}</td>

              </tr>

              @endforeach
              @endif
                     
                 
          </tbody>



@endsection

