@extends('layouts.appinside')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="{{ route('district-admin.dashboard') }}">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Finance</span>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span>Demand</span>
        </li>
    </ul>
</div>



<div class="portlet light bordered" style="margin-top: 10p x;">

    <div class="portlet-title">
        <div class="caption">
            <i class="icon-share font-dark"></i>
            <span class="caption-subject font-dark bold uppercase">Fund Request</span>
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
    </div>

    <div class="portlet-body">

       <p>
            {{--  <a href="{{route('superadmin.finance.up-demand.finace_demand.create', 'fundlistid='.app('request')->input('fid'))}}" class="btn btn-large btn-success"> Add new Entry</a> 
            --}}
           <a href="{{route('district-admin.fundlist.up-demand.finace_demand.print',['fundlistid' => app('request')->input('fid'), 'fdate' => app('request')->input('fdate'),'tdate' => app('request')->input('tdate'),'proj_id' => app('request')->input('proj_id'),'distid' => app('request')->input('distid'),'upid' => app('request')->input('upid'),'unid' => app('request')->input('unid')  ] )}}" class="btn btn-large btn-success"> Print</a>

        </p> 

        @if(count($FinanceDemands) > 0)

        <table class="table table-bordered table-hover" id="example0">

            <thead class="flip-content">
                <th>Action</th>
                <th>SL</th>
                <th>ID</th>
                {{--  <th>App Date</th>  --}}
                <th>App. Status</th>
                <th>Head</th>
                <th>Sub Head</th>
                <th>Item</th>
                <th>Amount</th>
                {{--  <th>From Date</th>
                 <th>To Date</th>  --}}
                <th>Details</th>
            </thead>

            <tbody>
        
                @foreach($FinanceDemands as $FinanceData)
                <tr>
                    <td>
                        <a class="label label-success" href="{{ route('district-admin.fundlist.up-demand.finace_demand.edit',[$FinanceData->id,'fundlistid'=>app('request')->input('fid'),'fdate'=>app('request')->input('fdate'),'tdate'=>app('request')->input('tdate'),'proj_id'=>app('request')->input('proj_id'),'distid'=>app('request')->input('distid'),'upid'=>app('request')->input('upid'),'unid'=>app('request')->input('unid')]) }}">
                            <i class="fa fa-pencil"></i> Edit
                        </a>
                    </td>
                    <td nowrap="nowrap">{{ $loop->iteration }}</td>
                      <td nowrap="nowrap">{{ $FinanceData->id}}</td>
                         {{--  <td nowrap="nowrap"> <span class="label label-sm label-info"> {{ $FinanceData->App_date }}</span></td>  --}}
                            <td nowrap="nowrap"><span class="label label-sm label-success"> {{ $FinanceData->app_status }}</span></td>
                    <td nowrap="nowrap">{{ $FinanceData->getHead->headname }}</td>
                    <td nowrap="nowrap">{{ $FinanceData->getSubhead->sname }}</td>
                    <td nowrap="nowrap">{{ $FinanceData->getItem->itemname }}</td>
                    <td class="text-right">{{ $FinanceData->amount }}</td>
                    {{--  <td nowrap="nowrap">{{ $FinanceData->date }}</td>
                     <td nowrap="nowrap">{{ $FinanceData->todate }}</td>  --}}
                    <td>{{ $FinanceData->remarks }}</td>

                </tr>
                @endforeach
            </tbody>
        </table>
        @else
        <p>No Data Found</p>
        @endif


   
        
 <span class="caption-subject font-dark bold uppercase">Commented By:</span>
   @if(count($Comments ) > 0)

<table class="table table-bordered table-hover" id="example0">

    <thead class="flip-content">
        <th>Name - Desig</th>
        <th>Comment</th>
    </thead>

    <tbody>

        @foreach($Comments as $CommentsData)
        <tr>
            <td nowrap="nowrap">{{ $CommentsData->names }}</td>
            <td nowrap="nowrap">{{ $CommentsData->comment }}</td>

        </tr>
        @endforeach
    </tbody>
</table>
@else
<p>No Data Found</p>
@endif



    </div>
</div>


    
    <div class="portlet-body form">


        {!! Form::open(array('route' => ['district-admin.fundlist.up-demand.store_comment','fid='.app('request')->input('fid')], 'method' => 'post','class' => 'form-horizontal', 'files' => true, 'enctype'=>'multipart/form-data')) !!}

            {{csrf_field()}}

            <div class="form-body">
                @include('partials.errors')

             <div class="form-group">
                    <label class="control-label col-md-3">Comments:</label>
                    <div class="col-md-4">
                        {!! Form::text('comment', null, ['class' => 'form-control', 'placeholder' => 'Comment']) !!}
                       
      @foreach($Filelists as $FilelistsData)
        <tr>
       
           
            <td nowrap="nowrap"> <a href="{{  url('uploads/fundchecklist/'.$FilelistsData->names) }}">Download</a></td>
        </tr>
        @endforeach
                         {!! Form::file('files', null) !!}
 
                    </div>
                </div>


            </div>
            
      <div class="form-actions right1">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <a href="javascript:history.back()" class="btn default">Back </a>
                        {!! Form::submit('Save', ['class' => 'btn green']) !!}
                    </div>
                </div>
            </div>

        {!! Form::close() !!}

           </div>




@endsection
