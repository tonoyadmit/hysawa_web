@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('district-admin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="{{ route('district-admin.location.region.index') }}">Regions</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Region Update</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

    <div class="col-md-6">
      <div class="portlet box blue">
        <div class="portlet-title">
            <div class="caption">
                <i class="fa fa-gift"></i>Update Region </div>
            <div class="tools">
                <a href="javascript:;" class="collapse"> </a>
            </div>
        </div>

        <div class="portlet-body form">
            <form action="{{route('district-admin.location.region.update', $region->id)}}" class="form-horizontal" method="POST" id="createForm">

                {{csrf_field()}}

                <input type="hidden" name="query" value="{{time()}}" />

                <div class="form-body">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label col-md-3">Title</label>
                                <div class="col-md-9">
                                    <input class="form-control" type="text" name="region_name"

                                        value="{{$region->region_name}}"
                                    required="required" >
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="row">
                      <div class="form-group">
                          <div class="col-md-offset-3 col-md-9">
                              <input type="submit" class="btn green" value="Update Region" />
                          </div>
                      </div>
                    </div>
                </div>
            </form>

        </div>
      </div>
    </div>
@endsection
