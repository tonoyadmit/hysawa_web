@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{route('district-admin.dashboard')}}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="{{route('district-admin.location.upazila.index')}}">Upazila</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>List</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

<div class="col-md-12" style="margin-top: 50px;">
  <form
    action="{{route('district-admin.location.upazila.store')}}"
    class="form-horizontal"
    method="POST"
    id="createForm">

    {{csrf_field()}}

    <input type="hidden" name="query" value="{{time()}}" />

    <div class="form-body">


      <div class="form-group">
        <label class="control-label col-md-3">Region</label>

        <div class="col-md-6">
          <select class="form-control input-sm" name="region_id" id="regionId">
            <option value="">Select Region</option>
            <?php $regions = App\Model\Region::orderBy('region_name')->get(); ?>
            @if($regions != "")
              @foreach($regions as $region)
                <option value="{{$region->id}}">{{$region->region_name}}</option>
              @endforeach
            @endif
          </select>
        </div>
      </div>


      <div class="form-group">
        <label class="control-label col-md-3">District</label>

        <div class="col-md-6">
          <select class="form-control input-sm" id="district_id" name="district_id">
            <option value="">Select District</option>
            <?php $districts = App\Model\District::orderBy('distname')->get(); ?>
            @foreach( $districts as $district)
              <option value="{{$district->id}}">{{$district->distname}}</option>
            @endforeach
          </select>
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Upazila Name *</label>
        <div class="col-md-6">
          <input type="text" name="upname" class="form-control" required="required" />
        </div>
      </div>

      <div class="form-group">
        <label class="control-label col-md-3">Upazila Code</label>
        <div class="col-md-6">
          <input type="text" name="upcode" class="form-control" />
        </div>
      </div>


    </div>

    <div class="form-actions">
      <div class="col-md-offset-3">
      <input type="submit" class="btn green btn-sm" value="Add New Upazila" />
      </div>
    </div>
  </form>
</div>



@endsection

@section('my_js')
  <script type="text/javascript">
    $(document).ready(function () {
      $('#regionId').on('change', function() {

        var region_id = $(this).val();

        $("#district_id").html("Select Region");

        $.ajax({
          url: '{{route('district-admin.ajax.district')}}?region_id='+region_id+'&not_all=true&choose_one=true',
          type: 'GET',
          success: function(response){
            console.log(response);
            if(response['status'] == true){
              $("#district_id").html(response['district_list']);
            }
          }
        });
      });
    });
  </script>
@endsection
