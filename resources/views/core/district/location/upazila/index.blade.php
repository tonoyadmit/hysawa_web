@extends('layouts.appinside')

@section('content')

  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{route('district-admin.dashboard')}}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <a href="{{route('district-admin.location.upazila.index')}}">Upazila</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>List</span>
      </li>
    </ul>
  </div>

  @include('partials.errors')

<div class="col-md-12" style="border-width: 1px; border-style: solid;">
  <form
    action="{{route('district-admin.location.upazila.index')}}"
    class="form-horizontal"
    method="GET"
    id="createForm">

    <input type="hidden" name="query" value="{{time()}}" />

    <div class="form-body">

      <div class="row">

        <div class="col-md-2">
          <label class="control-label">Upazila Name</label>
          <input type="text" class="form-control input-sm" name="upname" value="{{$old['upname'] or ''}}" />
        </div>


        <div class="col-md-2">
          <label class="control-label">Region</label>
          <select class="form-control input-sm" name="region_id">
            <option value="">Select Region</option>
            <?php
              $regions = App\Model\Region::orderBy('region_name')->get();
            ?>
            @if($regions != "")
              @foreach($regions as $region)
                <option value="{{$region->id}}"
                  @if(isset($old['region_id']) && $old['region_id'] == $region->id)
                    selected="selected"
                  @endif
                  >{{$region->region_name}}</option>
              @endforeach
            @endif
            </select>
        </div>

        <div class="col-md-2">

          <label class="control-label">District</label>
          <select class="form-control input-sm" id="district_id" name="district_id">
            <option value="">Select District</option>
            <?php
            $districts = App\Model\District::orderBy('distname')->get();
            ?>
            @foreach( $districts as $district)
            <option value="{{$district->id}}"
              @if(isset($old['district_id']) && $old['district_id'] ==$district->distname)
              selected="selected"
              @endif
              >{{$district->distname}}</option>
              @endforeach


            </select>
        </div>

        <div class="col-md-4" style="margin-top: 23px;">
          <input type="submit" class="btn green btn-sm" value="Filter" />
          <a href="{{route('district-admin.location.upazila.index')}}" class="btn btn-info btn-sm">Clear</a>

          <a href="{{route('district-admin.location.upazila.create')}}" class="btn btn-primary btn-sm">Add New Upazila</a>
        </div>

      </div>

    </div>
  </form>
</div>

<div class="portlet light tasks-widget bordered">

  <div class="portlet-title">
    <div class="caption">List of Unions</div>
  </div>

  <div class="portlet-body util-btn-margin-bottom-5">
    <div class="table-responsive">

      <table class="table table-bordered table-hover table-condensed" id="example0">
        <thead class="flip-content">
            <tr>
              <th width="10%">Action</th>
              <th>Region</th>
              <th>District</th>
              <th>Upazila</th>
              <th>Upazila Code</th>
            </tr>
        </thead>

        <tbody>
          @foreach($datas as $d)
            <tr>
              <td class="form-actions"><a href="{{route('district-admin.location.upazila.edit', $d->id)}}" class="btn btn-sm btn-info"><i class="fa fa-pencil"></i></a>
              </td>
              <td>{{ $d->district->region->region_name or ''}}</td>
              <td>{{ $d->district->distname or ''}}</td>
              <td>{{ $d->upname}}</td>
              <td>{{ $d->upcode}}</td>
            </tr>
          @endforeach

        </tbody>
      </table>

      {{$datas->appends($old)->links()}}

    </div>
  </div>
</div>
@endsection