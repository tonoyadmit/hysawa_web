<div class="portlet box blue">

    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Monthly Report <small> view</small> </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body form">
        <form action="{{route('district-admin.periods.show')}}" class="form-horizontal" method="GET" id="searchForm">

            <input type="hidden" name="query" value="{{time()}}" />

            <div class="form-body">
                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">District</label>
                            <div class="col-md-9">

                                <select class="form-control" name="district_id" id="district_id">
                                    {{-- <option value="">Choose a District</option> --}}
                                    <option value="all"

                                    @if(isset($_REQUEST['district_id']) && $_REQUEST['district_id'] == 'all' || !isset($_REQUEST['district_id']))
                                        selected="selected"
                                    @endif
                                    >All</option>
                                    <?php
                                        // $districts = App\Model\District::where('region_id', auth()->user()->region_id)->get();
                                        
                                        $districts = App\Model\District::all();
                                        
                                    ?>
                                    @foreach($districts as $district )
                                        <option value="{{$district->id}}"

                                        @if(isset($_REQUEST['district_id']) && $_REQUEST['district_id'] == $district->id)
                                            selected="selected"
                                        @endif
                                        >{{$district->distname}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Upazilas</label>
                            <div class="col-md-9">
                                <select class="form-control" name="upazila_id" id="upazila_id">

                                    @if(isset($_REQUEST['upazila_id']) && $_REQUEST['upazila_id'] != "all")
                                    <?php
                                        $upazilas = App\Model\Upazila::where('disid', $_REQUEST['district_id'])->get();
                                    ?>
                                       {{--  <option value="">Choose Option</option> --}}
                                        <option value="all"

                                            @if( isset($_REQUEST['upazila_id']) && $_REQUEST['upazila_id'] == "all")
                                                selected="selected"
                                            @endif

                                        >All</option>
                                        @foreach($upazilas as $upazila)
                                            <option value="{{$upazila->id}}"

                                            @if(isset($_REQUEST['upazila_id']) && $_REQUEST['upazila_id'] == $upazila->id)
                                                selected="selected"
                                            @endif
                                            >{{$upazila->upname}}</option>
                                        @endforeach
                                    @else
                                        <option value="">Choose District First</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Union </label>
                            <div class="col-md-9">
                                <select class="form-control" name="union_id" id="union_id">

                                @if(isset($_REQUEST['upazila_id']) && $_REQUEST['upazila_id'] != "all" )
                                    <?php
                                        $unions = App\Model\Union::where('upid', $_REQUEST['upazila_id'])->get();
                                    ?>

                 {{--                    <option value="">Choose Option</option> --}}

                                    <option value="all"
                                        @if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] == "all")
                                            selected="selected"
                                        @endif
                                    >All</option>

                                    @foreach($unions as $union)
                                        <option value="{{$union->id}}"
                                        @if($_REQUEST['union_id'] == $union->id)
                                            selected="selected"
                                        @endif
                                        >{{$union->unname}}</option>
                                    @endforeach

                                @else
                                    <option value="">Choose Upazila First</option>
                                @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-3">Reporting Period</label>
                            <div class="col-md-9">
                                <select class="form-control" name="rep_data_id" required="required" id="rep_data_id">
                                    <option value="">Choose Union First</option>
                                    <option value="all" selected="selected">ALL</option>
                                    <?php
                                        $periods = \DB::table('rep_period')
                                                        ->where('id', '!=', 99)
                                                        ->where('id', '!=', 100)
                                                        ->orderBy('id', 'DESC')
                                                        ->get();
                                    ?>
                                    @foreach($periods as $period)
                                        <option value="{{$period->id}}"
                                            @if(isset($_REQUEST['rep_data_id']) && $_REQUEST['rep_data_id'] == $period->id)
                                                selected="selected"
                                            @endif
                                        >{{$period->period}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Type</label>
                            <div class="col-md-9">
                                <select name="report_type" class="form-control" required="required">
                                    <option value="html" selected="selected">Generate Report</option>
                                    <option value="print">Print</option>
                                </select>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-5">
                                <input type="submit" class="btn green" value="Search" />
                                <a href="{{route('district-admin.periods.index')}}" class="btn default">Clear</a>
                                 <a href="{{route('district-admin.periods.monthlyrep')}}" class="btn default">Report</a>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </form>

    </div>
</div>