@extends('layouts.appinside')
@section('content')
  <div class="page-bar">
    <ul class="page-breadcrumb">
      <li>
        <a href="{{ route('district-admin.dashboard') }}">Home</a>
        <i class="fa fa-circle"></i>
      </li>
      <li>
        <span>Monthly Report</span>
      </li>
    </ul>
  </div>

  <div class="col-md-12" style="margin-top: 10px;">
      @include('core.district.monthly-report.water.period._search')
  </div>

@endsection



@section('my_js')
<script type="text/javascript">
  $(document).ready(function () {
    $('#district_id').on('change', function() {
      var district_id = $(this).val();
      if(district_id != "all")
      {
        $("#upazila_id").html("Select District");
        $("#union_id").html("Select Upazila");
        $.ajax({
          url: '{{route('ajax.upazilas-from-district')}}?district_id='+district_id+'&choose_one=false',
          type: 'GET',
          success: function(response){



            if(response['status'] == true){
              var options = response['upazila_list'];
              $("#upazila_id").html(options);
            }
          }
        });
      }else{
        $("#upazila_id").html("Select District");
        $("#union_id").html("Select Upazila");
      }
    });

    $('#upazila_id').on('change', function() {
      var upazila_id = $(this).val();
      if(upazila_id != "all"){
        $("#union_id").html("Select Upazila");
        $.ajax({
          url: '{{route('ajax.unions-from-upazila')}}?upazila_id='+upazila_id+'&choose_one=false',
          type: 'GET',
          success: function(response){



            if(response['status'] == true){
              var options = response['union_list'];
              $("#union_id").html(options);
            }
          }
        });
      }else{
        $("#union_id").html("Select Upazila");
      }
    });
  });
</script>
@endsection