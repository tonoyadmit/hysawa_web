@extends('layouts.print.app')

@section('my_style')
<style>
    table{
      border-collapse: collapse;
      border-spacing: 0;
    }

    div{
      font-size: 80%;
      display: inline-block;
     /* overflow: visible;

      width: 2.4em;
      line-height: 0.9em;
      margin-top: 9em;

      white-space: nowrap;
      -ms-transform: rotate(270deg);
      -o-transform: rotate(270deg);
      -webkit-transform: rotate(270deg);
      transform: rotate(270deg);
      text-align: left;*/
    }

    .headline th {
      padding: 5px 20px;
    }
  </style>
@endsection

@section('content')
<?php
$projectTitle = App\Model\Project::find(auth()->user()->proj_id)->project;

?>
<p><b>Project</b>: {{ $projectTitle or ''}}</p>

@if(isset($_REQUEST['query']))
<p>Query:
    <?php
        $districtName = isset($_REQUEST['district_id']) ? $_REQUEST['district_id'] : "";
        $upazilaName = isset($_REQUEST['upazila_id']) ?$_REQUEST['upazila_id'] : "";
        $unionName = isset($_REQUEST['union_id']) ? $_REQUEST['union_id'] : "";

        if($districtName != "" && $districtName != "all")
        {
            $districtName = App\Model\District::find($_REQUEST['district_id'])->distname;

            if($upazilaName != "" && $upazilaName != "all")
            {
                $upazilaName = App\Model\Upazila::find($_REQUEST['upazila_id'])->upname;

                if($unionName != "" && $unionName != "all")
                {
                    $unionName = App\Model\Union::find($_REQUEST['union_id'])->unname;
                }
            }
        }

    ?>

    <b>District</b>: {{$districtName}}|
    <b>Upazila</b>: {{$upazilaName}}|
    <b>Union</b>: {{$unionName}}|

    <b>App Status</b>: {{$_REQUEST['app_status'] or ''}}|
    <b>Imp. Status</b>: {{$_REQUEST['imp_status'] or ''}}|
    <b>Approval Date</b>: {{$_REQUEST['starting_date'] or ''}}
</p>
@endif
<h2>Site list of {{count($sanitations) or 0}} sanitation points</h2>

<table class="box" border="1">
    <thead class="rotate">

      <th><div>District</div></th>
      <th><div>Upazila</div></th>
      <th><div>Union</div></th>

      <th><div>Approval</div></th>
      <th><div>Implementation Status</div></th>
      <th><div>Approval Date</div></th>
      <th><div>CDF No</div></th>
      <th><div>Type</div></th>

      <th><div>Village</div></th>
      <th><div>Type of Inst.</div></th>
      <th><div>SubType</div></th>
      <th><div>Name</div></th>
      <th><div>Male Chamber</div></th>

      <th><div>Female Chamber</div></th>
      <th><div>Male Users</div></th>
      <th><div>Female Users</div></th>
    </thead>
    <tbody>
      @foreach($sanitations as $sanitation)
      <tr>

        <td><div>{{$sanitation->district->distname or ''}} </div></td>
        <td><div>{{$sanitation->upazila->upname or ''}} </div></td>
        <td><div>{{$sanitation->union->unname or ''}} </div></td>

        <td ><div>{{ $sanitation->app_status }}</div></td>
        <td ><div>{{ $sanitation->imp_status }}</div></td>
        <td ><div>{{ $sanitation->app_date }}</div></td>
        <td ><div>{{ $sanitation->cdfno }}</div></td>
        <td ><div>{{ $sanitation->cons_type }}</div></td>
        <td ><div>{{ $sanitation->village }}</div></td>
        <td ><div>{{ $sanitation->maintype }}</div></td>
        <td ><div>{{ $sanitation->subtype }}</div></td>
        <td ><div>{{ $sanitation->name }}</div></td>
        <td ><div>{{ $sanitation->malechamber }}</div></td>
        <td ><div>{{ $sanitation->femalechamber }}</div></td>
        <td ><div>{{ $sanitation->male_ben }}</div></td>
        <td ><div>{{ $sanitation->fem_ben }}</div></td>

      </tr>
      @endforeach
    </tbody>
  </table>
@endsection