@extends('layouts.appinside')

@section('content')


<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('up-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="{{ route('up-admin.water_sanitation.index') }}">Sanitation</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Update</span>
    </li>
  </ul>
</div>

<h1 class="page-title"> Sanitation
  <small>Update Info</small>
</h1>

{!! Form::model($WaterSanitation, array('url' => route('district-admin.water_sanitation.update', $WaterSanitation->id ), 'method' => 'put')) !!}

<div class="row">
  @include('partials.errors')
  <div class="col-md-6">
    <div class="form-group">
      <label class="control-label ">CDF No: {{$WaterSanitation->cdfno}}</label>
    </div>

    <div class="form-group">
      <label class="control-label ">Village Name: {{$WaterSanitation->Village}}</label>
    </div>

    <div class="form-group">
      <label class="control-label">Latrine no. (6 digit upcode+sl): {{$WaterSanitation->latrineno}}</label>
    </div>

    <div class="form-group">
      <label class="control-label">Type of latrine: {{$WaterSanitation->maintype}}</label>
    </div>

    <div class="form-group">
      <label class="control-label">Type of Institution: {{$WaterSanitation->subtype}}</label>
    </div>

    <div class="form-group">
      <label class="control-label">New/Renovation: {{$WaterSanitation->cons_type}}</label>
    </div>

    <div class="form-group">
      <label class="control-label">Name of Institution/Place: {{$WaterSanitation->name}}</label>
    </div>

    <div class="form-group">
      <label class="control-label">Chairman of the Mgt Committe: {{$WaterSanitation->ch_comittee}}</label>
    </div>

    <div class="form-group">
      <label class="control-label">Phone no: {{$WaterSanitation->ch_com_tel}}</label>
    </div>

    <div class="form-group">
      <label class="control-label"> Caretaker name: {{$WaterSanitation->caretakername}}</label>
    </div>

    <div class="form-group">
      <label class="control-label"> Caretaker phone: {{$WaterSanitation->caretakerphone}}</label>
    </div>

    <div class="form-group">
      <label class="control-label">Approval date (YYYY-MM-DD): {{$WaterSanitation->app_date}}</label>
    </div>
    <div class="form-group">
      <label class="control-label">No. of chamber for Male/Boys: {{$WaterSanitation->malechamber}}</label>
    </div>
    <div class="form-group">
      <label class="control-label">No. of chamber for Female/Girls: {{$WaterSanitation->femalechamber}}</label>
    </div>
  </div>

  <div class="col-md-6">



    <div class="form-group">
      <label class="control-label">No. of Male beneficiry: {{$WaterSanitation->male_ben}}</label>
    </div>
    <div class="form-group">
      <label class="control-label">No. of Female Beneficary: {{$WaterSanitation->fem_ben}}</label>
    </div>
    <div class="form-group">
      <label class="control-label">No. of Child beneficiary: {{$WaterSanitation->child_ben}}</label>
    </div>
    <div class="form-group">
      <label class="control-label"> No. of Disable benficiary: {{$WaterSanitation->disb_bene}}</label>
    </div>
    <div class="form-group">
      <label class="control-label">Proposed/Installed facilities:{{$WaterSanitation->watersource}} </label>
    </div>

    <div class="form-group">
      <label class="control-label">Longitude: {{$WaterSanitation->longitude}}</label>
    </div>
    <div class="form-group">
      <label class="control-label"> Latitude: {{$WaterSanitation->latitude}}</label>
    </div>
    <div class="form-group">
      <label class="control-label">Overhead tank: {{$WaterSanitation->overheadtank}}</label>
    </div>
    <div class="form-group">
      <label class="control-label">Motor/pump: {{$WaterSanitation->motorpump}}</label>
    </div>
    <div class="form-group">
      <label class="control-label">Sock well: {{$WaterSanitation->sockwell}}</label>
    </div>
    <div class="form-group">
      <label class="control-label">Septic tank: {{$WaterSanitation->seotictank}}</label>
    </div>
    <div class="form-group">
      <label class="control-label">Tap outside: {{$WaterSanitation->tapoutside}}</label>
    </div>

    <div class="form-group">
      <label class="control-label">Approval Status:</label>

      <select class="form-control" required="required" name="app_status">
        <option value="Submitted"  @if($WaterSanitation->app_status == "Submitted") selected="selected" @endif >Submitted</option>
        <option value="Recomended" @if($WaterSanitation->app_status == "Recomended") selected="selected" @endif >Recomended</option>
        <option value="Approved" @if($WaterSanitation->app_status == "Approved") selected="selected" @endif >Approved</option>
        <option value="Cancelled" @if($WaterSanitation->app_status == "Cancelled") selected="selected" @endif >Cancelled</option>
        <option value="Rejected" @if($WaterSanitation->app_status == "Rejected") selected="selected" @endif >Rejected</option>
        <option value="Unsuccessful" @if($WaterSanitation->app_status == "Unsuccessful") selected="selected" @endif >Unsuccessful</option>
        <option value="Assessed" @if($WaterSanitation->app_status == "Assessed") selected="selected" @endif >Assessed</option>
      </select>


    </div>
    
    
           <div class="row">
                      <div class="form-group">
                        <label class="control-label col-md-3">Approval date: </label>
                        <div class="col-md-6">
                          <input type="text" class="form-control date-picker"
                            value="{{$WaterSanitation->app_date}}"
                            name="app_date"
                            data-date-format="yyyy-mm-dd"
                            readonly
                            />
                          <span class="help-block"> (YYYY-MM-DD) </span>
                        </div>
                      </div>
                  </div>

                   <div class="row">
                    <div class="form-group">
                      <label class="control-label col-md-3">Implementation Status</label>
                      <div class="col-md-6">

                        <select name="imp_status" class="form-control">
                          <option value="">Choose one</option>
                          <?php
                            $appStatus = \DB::table('imp_status')->orderBy('imp_status')->get();
                          ?>
                          @foreach($appStatus as $status)
                          <option value="{{$status->imp_status}}"
                            @if($WaterSanitation->imp_status == $status->imp_status) selected="selected"
                            @endif >{{$status->imp_status}}</option>
                          @endforeach

                        </select>

                      </div>
                    </div>
                  </div>
                  

  </div>

</div>

&nbsp;
<div class="row padding-top-10">
  <a href="javascript:history.back()" class="btn default"> Cancel </a>
  {!! Form::submit('Update', ['class' => 'btn green pull-right']) !!}
</div>

{!! Form::close() !!}


</div>

<script type="text/javascript">
  $(document ).ready(function() {
    highlight_nav('warehouse-manage', 'shelfs');
  });
</script>

@endsection
