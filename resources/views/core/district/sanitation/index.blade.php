@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('district-admin.dashboard') }}">Dashboard</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Sanitation</span>
    </li>
  </ul>
</div>

<div class="col-md-12" style="margin-top: 10px;">
  @include('core.district.sanitation._search')
</div>

@if(count($sanitations) > 0)

<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <div class="portlet-body util-btn-margin-bottom-5">

      <div class="table-responsive">
        <table class="table table-bordered table-hover" id="example0">

          <thead class="flip-content">
            <th style="font-size: 10px;">Action</th>
            <th style="font-size: 10px;">Approval</th>
            <th style="font-size: 10px;">Implementation Status</th>
            <th style="font-size: 10px;">Approval Date</th>
            <th style="font-size: 10px;">CDF No</th>
            <th style="font-size: 10px;">Type</th>
            <th style="font-size: 10px;">Village</th>
            <th style="font-size: 10px;">Type of Inst.</th>
            <th style="font-size: 10px;">SubType</th>
            <th style="font-size: 10px;">Name</th>
            <th style="font-size: 10px;">Male Chamber</th>
            <th style="font-size: 10px;">Female Chamber</th>
            <th style="font-size: 10px;">Male Users</th>
            <th style="font-size: 10px;">Female Users</th>
          </thead>

          <tbody>
            @foreach($sanitations as $sanitation)
            <tr>
              <td style="font-size: 10px;" >
                <a class="label label-success" href="{{route('district-admin.water_sanitation.edit', $sanitation->id)}}">
                  <i class="fa fa-pencil"></i></a>
              </td>
              <td style="font-size: 10px;">{{ $sanitation->app_status }}</td>
              <td style="font-size: 10px;">{{ $sanitation->imp_status }}</td>
              <td style="font-size: 10px;">{{ $sanitation->app_date }}</td>
              <td style="font-size: 10px;">{{ $sanitation->cdfno }}</td>
              <td style="font-size: 10px;">{{ $sanitation->cons_type }}</td>
              <td style="font-size: 10px;">{{ $sanitation->village }}</td>
              <td style="font-size: 10px;">{{ $sanitation->maintype }}</td>
              <td style="font-size: 10px;">{{ $sanitation->subtype }}</td>
              <td style="font-size: 10px;">{{ $sanitation->name }}</td>
              <td style="font-size: 10px;">{{ $sanitation->malechamber }}</td>
              <td style="font-size: 10px;">{{ $sanitation->femalechamber }}</td>
              <td style="font-size: 10px;">{{ $sanitation->male_ben }}</td>
              <td style="font-size: 10px;">{{ $sanitation->fem_ben }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>

        <div class="pagination pull-right">
          @if(count($sanitations))
            {{$sanitations->appends($old)->links()}}
          @endif
        </div>

      </div>
    </div>
  </div>
</div>
@else
<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <p>
      No Data Found
    </p>
  </div>
</div>
@endif

@endsection

@section('my_js')

<script type="text/javascript">
    $(document).ready(function () {

        $('#district_id').on('change', function() {
            var district_id = $(this).val();

            $("#upazila_id").html("Select District")
            $("#union_id").html("Select Upazila");

            $.ajax({
              url: '{{route('ajax.upazilas-from-district')}}?district_id='+district_id,
              type: 'GET',
              success: function(response){
                if(response['status'] == true){
                  $("#upazila_id").html(response['upazila_list']);
                }
               // console.log(response);
              }
            });
        });

        $('#upazila_id').on('change', function() {
          var upazila_id= $(this).val();
          $("#union_id").html("Select Upazila");
          $.ajax({
              url: '{{route('ajax.unions-from-upazila')}}?upazila_id='+upazila_id,
              type: 'GET',
              success: function(response){
                if(response['status'] == true){
                  $("#union_id").html(response['union_list']);
                }
                //console.log(response);
              }
            });
        });

    });
</script>
@endsection