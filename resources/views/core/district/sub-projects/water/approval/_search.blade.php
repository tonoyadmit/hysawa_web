<div class="col-md-12" style="border-width: 1px; border-style: solid;">

  <form action="{{route('district-admin.sub-projects.waters.approval')}}" class="form-horizontal" method="GET" id="createForm">

    <input type="hidden" name="query" value="{{time()}}" />

    <div class="form-body">

      <div class="row">

        <div class="col-md-2">
          <label class="control-label">Approval Date</label>
          <input
          type="text"
          class="form-control date-picker"
          data-date-format="yyyy-mm-dd"
          name="approval_date"
          @if(isset($_REQUEST['approval_date']) && $_REQUEST['approval_date'] != "")
          value="{{$_REQUEST['approval_date']}}"
          @endif  />
          <p style="margin-bottom: 0px; margin-top: 0px;">YYYY-MM-DD</p>
        </div>

        <div class="col-md-2">
          <label class="control-label">Region</label>
          <select class="form-control input-sm" name="region_id" id="region_id">

            <option value="all" @if(isset($_REQUEST['region_id']) && $_REQUEST['region_id'] == "all" ) selected="selected" @endif >All</option>
            <?php
            $regions = App\Model\Region::orderBy('region_name')->get();
            ?>
            @if(count($regions))
            @foreach($regions as $region)
            <option value="{{$region->id}}"
              @if(isset($_REQUEST['region_id']) && $_REQUEST['region_id'] == $region->id)
              selected="selected"
              @endif
              >{{$region->region_name}}</option>
              @endforeach
              @endif
            </select>
          </div>

          <div class="col-md-2">

            <label class="control-label">District</label>
            <select class="form-control input-sm" id="district_id" name="district_id">
              <option value="">Select District</option>
              <?php
              $districts = [];

              if(isset($_REQUEST['region_id']) && $_REQUEST['region_id'] != "")
              {
                if($_REQUEST['region_id'] != "all")
                {
                  $districts = App\Model\District::where('region_id', $_REQUEST['region_id'])->orderBy('distname')->get();
                }else{
                  $districts = App\Model\District::orderBy('distname')->get();
                }
              }else{
                $districts = App\Model\District::orderBy('distname')->get();
              }
              ?>
              <option value="all">All</option>
              @foreach( $districts as $district)
              <option value="{{$district->id}}"
                @if(isset($_REQUEST['district_id']) && $_REQUEST['district_id'] == $district->id)
                selected="selected"
                @endif
                >{{$district->distname}}</option>
                @endforeach
              </select>

            </div>

            <div class="col-md-2">

              <label class="control-label">Upazila</label>
              <select class="form-control input-sm" id="upazila_id" name="upazila_id">
                <option value="">Select Upazila</option>
                @if(isset($_REQUEST['upazila_id']))
                <?php
                $upazilas = [];

                if($_REQUEST['district_id'] != "" && $_REQUEST['district_id'] != "all")
                {
                  $upazilas = App\Model\Upazila::where('disid', $_REQUEST['district_id'])->orderBy('upname')->get();
                }
                ?>

                @foreach( $upazilas as $upazila)

                <option value="{{$upazila->id}}"
                  @if(isset($_REQUEST['upazila_id']) && $_REQUEST['upazila_id'] == $upazila->id)
                  selected="selected"
                  @endif
                  >{{$upazila->upname}}</option>
                  @endforeach

                  @endif
                </select>

              </div>

              <div class="col-md-2">
                <label class="control-label">Union</label>
                <select name="union_id" class="form-control input-sm" id="union_id" name="union_id">
                  <option value="">Select Union</option>
                  @if(isset($_REQUEST['union_id']))
                  <?php
                  $unions = [];
                  if($_REQUEST['upazila_id'] != "" && $_REQUEST['upazila_id'] != "all")
                  {
                    $unions = App\Model\Union::where('upid', $_REQUEST['upazila_id'])->orderBy('unname')->get();
                  }
                  ?>

                  @foreach($unions as $union)
                  <option value="{{$union->id}}"
                    @if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] == $union->id)
                    selected="selected"
                    @endif
                    >{{$union->unname}}</option>
                    @endforeach
                    @endif
                  </select>
                </div>


                <div class="col-md-2">
                  <label class="control-label">Output Type</label>
                  <select name="output" class="form-control input-sm" id="output">
                    <option value="download">Download</option>
                    <option value="html" selected="selected">Screen</option>
                    <option value="print" >Print</option>
                  </select>
                </div>


              </div>

                <div class="row" >
                      <div class="col-md-12" style="margin-top: 10px;">
                        <input type="submit" class="btn green btn-sm" value="Get Approval Summary" />
                        <a href="{{route('district-admin.sub-projects.waters.approval')}}" class="btn btn-info btn-sm">Clear</a>
                      </div>
                </div>

                @if(isset($_REQUEST['query']))
                <div class="row">
                <p>
                  <?php

                    $region = "";
                    if(isset($_REQUEST['region_id']) && $_REQUEST['region_id'] != "")
                    {
                      if($_REQUEST['region_id'] == "all"){$region = "All";}
                      else{
                        $region = App\Model\Region::find($_REQUEST['region_id'])->region_name;
                      }
                    }

                    $district = "";
                    if(isset($_REQUEST['district_id']) && $_REQUEST['district_id'] != "")
                    {
                      if($_REQUEST['district_id'] == "all") $district = "All";
                      else{
                        $district = App\Model\District::find($_REQUEST['district_id'])->distname;
                      }
                    }

                    $upazila = "";

                    if(isset($_REQUEST['upazila_id']) && $_REQUEST['upazila_id'] != "")
                    {
                      if($_REQUEST['upazila_id'] == "all") $upazila = "All";
                      else{
                        $upazila = App\Model\Upazila::find($_REQUEST['upazila_id'])->upname;
                      }
                    }

                    $union = "";

                    if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] != "")
                    {
                      if($_REQUEST['union_id'] == "all") $union = "All";
                      else{
                        $union = App\Model\Union::find($_REQUEST['union_id'])->unname;
                      }
                    }
                  ?>

                  <strong>Approval Date</strong> : {{$_REQUEST['approval_date']}}

                  <strong>Region</strong> : {{$region}}|
                  <strong>District</strong> : {{$district}}|
                  <strong>Upazila</strong> : {{$upazila}}|
                  <strong>Union</strong> : {{$union}}
                </p>
                </div>
                @endif
                  </div>
                </form>
              </div>



  <script type="text/javascript">
    $(document).ready(function () {


      $('#region_id').on('change', function() {
        var region_id = $(this).val();

        // $("#district_id").html("Select Region");
        // $("#upazila_id").html("Select District");
        // $("#union_id").html("Select Upazila");

        $.ajax({
          url: '{{route('ajax.districts-from-region')}}?region_id='+region_id+'&not_all=true&choose_one=true',
          type: 'GET',
          success: function(response){
            // if(response['status'] == true){
            //   $("#district_id").html(response['district_list']);
            // }
          }
        });
      });



      $('#district_id').on('change', function() {
        var head_id = $(this).val();

        $("#upazila_id").html("Select District");
        $("#union_id").html("Select Upazila");

        $.ajax({
          url: '{{route('ajax.upazilas-from-district')}}?district_id='+head_id+'&not_all=true&choose_one=true',
          type: 'GET',
          success: function(response){
            if(response['status'] == true){
              $("#upazila_id").html(response['upazila_list']);
            }
          }
        });
      });

      $('#upazila_id').on('change', function() {

        var upazila_id = $(this).val();
        $("#union_id").html("Select Upazila");

        $.ajax({
          url: '{{route('ajax.unions-from-upazila')}}?upazila_id='+upazila_id+'&not_all=true&choose_one=true',
          type: 'GET',
          success: function(response){
            if(response['status'] == true){
              $("#union_id").html(response['union_list']);
            }
          }
        });
      });

    });
  </script>