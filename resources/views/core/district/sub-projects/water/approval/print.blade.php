@extends('layouts.print.app')

@section('my_style')
<style>
  table{border-collapse: collapse;border-spacing: 0; border: 1px;}
  div{font-size: 80%;display: inline-block;}
  .headline th {padding: 5px 20px;}
</style>
@endsection

@section('content')

@if(isset($_REQUEST['query']))
<div class="row">
<p>
  <?php

    $region = "";
    if(isset($_REQUEST['region_id']) && $_REQUEST['region_id'] != "")
    {
      if($_REQUEST['region_id'] == "all"){$region = "All";}
      else{
        $region = App\Model\Region::find($_REQUEST['region_id'])->region_name;
      }
    }

    $district = "";
    if(isset($_REQUEST['district_id']) && $_REQUEST['district_id'] != "")
    {
      if($_REQUEST['district_id'] == "all") $district = "All";
      else{
        $district = App\Model\District::find($_REQUEST['district_id'])->distname;
      }
    }

    $upazila = "";

    if(isset($_REQUEST['upazila_id']) && $_REQUEST['upazila_id'] != "")
    {
      if($_REQUEST['upazila_id'] == "all") $upazila = "All";
      else{
        $upazila = App\Model\Upazila::find($_REQUEST['upazila_id'])->upname;
      }
    }

    $union = "";

    if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] != "")
    {
      if($_REQUEST['union_id'] == "all") $union = "All";
      else{
        $union = App\Model\Union::find($_REQUEST['union_id'])->unname;
      }
    }
  ?>

  <strong>Region</strong> : {{$region}}|
  <strong>District</strong> : {{$district}}|
  <strong>Upazila</strong> : {{$upazila}}|
  <strong>Union</strong> : {{$union}}
</p>
</div>
@endif



@if($type == "union")
  <h1 class="page-title"> Water <small> Approval and Implementation Status</small> </h1>
  @include('core.district.sub-projects.water.approval._union')
@elseif($type == "region")
  <h1 class="page-title"> Region Summary <small> Approval and Implementation Status</small> </h1>
  @include('core.district.sub-projects.water.approval._region')
@elseif($type == "district")
  <h1 class="page-title"> District Summary <small> Approval and Implementation Status</small> </h1>
  @include('core.district.sub-projects.water.approval._district')
@endif

@endsection