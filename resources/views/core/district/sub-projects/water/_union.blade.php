<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <div class="portlet-body util-btn-margin-bottom-5">


      <div class="table-responsive">
        <style>
          #example3 td {
            /*white-space: nowrap;*/
            font-size: 10px;
          }
          #example3 th {
            font-size: 10px;
          }
        </style>

        <table class="table table-bordered table-hover table-condensed" id="example3" border="1px">
          <thead class="flip-content">

              <td>&nbsp;</td>

              <td>District</td>
              <td>Upazila</td>
              <td>Union</td>
              <td>Approval date</td>
              <td>Work order/ Lot No.</td>

              <td>Total HH</td>
              <td>Hardcore HH</td>
              <td>Hardcore %</td>
              <td>Expected Contribution (%)</td>


              <td>Approved</td>
              <td>Pending</td>
              <td>Recomended</td>
              <td>Cancelled</td>
              <td>Rejected</td>
              <td>In Tendering process</td>
              <td>Under construction</td>
              <td>Completed</td>
              <td>WQ tested</td>
              <td>Platform constructed</td>
              <td>Depth measured</td>
              <td>GPS Coordinates</td>

          </thead>

          <tbody>
          <?php $index = 1; ?>
            @foreach($unionSummary as $rs)
              <tr>

              <td>{{$index++}}</td>

              <td>{{$rs->distname}}</td>
              <td>{{$rs->upname}}</td>
              <td>{{$rs->unname}}</td>
              <td nowrap="nowrap">{{$rs->App_date}}</td>
              <td>{{$rs->Tend_lot}}</td>

              <td>{{$rs->hhcount}}</td>
              <td>{{$rs->hchhcount}}</td>
              <td>{{$rs->hcPcount}}</td>
              <td>{{$rs->cccount}}</td>

              <td>{{$rs->sumappcount}}</td>
              <td>{{$rs->Submitted}}</td>
              <td>{{$rs->Recomended}}</td>
              <td>{{$rs->Cancelled}}</td>
              <td>{{$rs->Rejected}}</td>
              <td>{{$rs->TenderingInProcess}}</td>
              <td>{{$rs->UnderImplementation}}</td>
              <td>{{$rs->Completed}}</td>
              <td>{{$rs->wq_Arsenic}}</td>
              <td>{{$rs->platform}}</td>
              <td>{{$rs->depth}}</td>
              <td>{{$rs->x_coord}}</td>

              </tr>
            @endforeach

          </tbody>

        </table>
      </div>
    </div>
  </div>
</div>

