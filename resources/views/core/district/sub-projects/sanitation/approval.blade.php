@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('district-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="#">Sub-project</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="#">Sanitation </a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="{{route('district-admin.sub-projects.sanitation.index')}}">Approval and Implementation Status </a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Approval Summary</span>
    </li>
  </ul>
</div>

@include('partials.errors')

@include('core.district-admin.sub-projects.sanitation._search')

<p>&nbsp;</p>
<h1 class="page-title"> Sanitation <small> Approval and Implementation Status</small> </h1>

@include('core.district-admin.sub-projects.sanitation._summary')

@endsection

