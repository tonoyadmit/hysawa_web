@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('district-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="#">Sub-project</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="#">Water </a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Approval and Implementation Status</span>
    </li>
  </ul>
</div>

@include('partials.errors')

<h1 class="page-title"> Water <small> Approval and Implementation Status</small> </h1>

<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <div class="portlet-body util-btn-margin-bottom-5">
      <table>
        <tr>
          <td><a class="btn btn-large btn-info" href="{{route('district-admin.sub-projects.sanitation.regions')}}">Region Summary</a></td>
          <td><a class="btn btn-large btn-success" href="{{route('district-admin.sub-projects.sanitation.download', 'region')}}">Region Download</a></td>
        </tr>
        <tr>
          <td><a class="btn btn-large btn-info" href="{{route('district-admin.sub-projects.sanitation.districts')}}">District Summary</a></td>
          <td><a class="btn btn-large btn-success" href="{{route('district-admin.sub-projects.sanitation.download', 'district')}}">District Download</a></td>
        </tr>
        <tr>
          <td><a class="btn btn-large btn-info" href="{{route('district-admin.sub-projects.sanitation.unions')}}">Union Summary</a></td>
          <td><a class="btn btn-large btn-success" href="{{route('district-admin.sub-projects.sanitation.download', 'union')}}">Union Download</a></td>
        </tr>
        <tr>
          <td><a class="btn btn-large btn-info" href="{{route('district-admin.sub-projects.sanitation.approval')}}">Approval Summary</a></td>
          <td><a class="btn btn-large btn-success" href="{{route('district-admin.sub-projects.sanitation.download', 'approval')}}">Approval Download</a></td>
        </tr>
      </table>

    </div>
  </div>
</div>
@endsection

