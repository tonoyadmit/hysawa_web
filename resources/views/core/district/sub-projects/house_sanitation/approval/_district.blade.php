<div class="col-md-12">
  <div class="portlet light tasks-widget bordered">
    <div class="portlet-body util-btn-margin-bottom-5">


      <div class="table-responsive">
        <style>
          #example2 td {
            /*white-space: nowrap;*/
            font-size: 10px;
          }
          #example2 th {
            font-size: 10px;
          }
        </style>

        <table class="table table-bordered table-hover table-condensed" id="example2" border="1px">
          <thead class="flip-content">
                <tr>
                  <td rowspan="2">District</td>
                  <td colspan="6" class="text-center">Approval status</td>
                  <td colspan="7" class="text-center">Implementation status</td>
                </tr>
                <tr>
                  <td>Total Submitted</td>
                  <td>Approved</td>
                  <td>Pending</td>
                  <td>Recomended</td>
                  <td>Cancelled</td>
                  <td>Rejected</td>
                  <td>In Tendering process</td>
                  <td>Under construction</td>
                  <td>Completed</td>
                  <td>WQ tested</td>
                  <td>Platform constructed</td>
                  <td>Depth measured</td>
                  <td>GPS Coordinates</td>
                </tr>


          </thead>

          <tbody>
          <?php $index = 1; ?>
            @foreach($districtSummary as $rs)
              <tr>
              
              <td>{{$rs->distname or ''}}</td>
              <td>{{$rs->subappcount}}</td>
              <td>{{$rs->sumappcount}}</td>
              <td>{{$rs->Submitted}}</td>
              <td>{{$rs->Recomended}}</td>
              <td>{{$rs->Cancelled}}</td>
              <td>{{$rs->Rejected}}</td>
              <td>{{$rs->TenderingInProcess}}</td>
              <td>{{$rs->UnderImplementation}}</td>
              <td>{{$rs->Completed}}</td>
              <td>{{$rs->wq_Arsenic}}</td>
              <td>{{$rs->platform}}</td>
              <td>{{$rs->depth}}</td>
              <td>{{$rs->x_coord}}</td>
              
              </tr>
            @endforeach

          </tbody>

        </table>
      </div>
    </div>
  </div>
</div>

