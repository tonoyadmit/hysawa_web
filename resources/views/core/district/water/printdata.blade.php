@extends('layouts.print.app')

@section('my_style')

<style>
    table{border-collapse: collapse;border-spacing: 0;}
    div{font-size: 80%;display: inline-block;}
    .headline th {padding: 5px 20px;}
  </style>
@endsection

<?php
@$union = App\Model\Union::with('upazila.district')->find(\Auth::user()->unid);
@$districtName = $union->upazila->district->distname;
@$upazilaName = $union->upazila->upname;
@$unionName = $union->unname;
@$projectName = App\Model\Project::find(\Auth::user()->proj_id);
@$region = App\Model\Region::find(\Auth::user()->region_id);
?>
@section('content')



@if(isset($waters) )

<h1></h1>

<p style="text-align: center;  font-size: 20px;    font-family: "Times New Roman", Times, serif;"><b>WATER QUALITY RESULT<b></p>
<table class="table table-bordered table-hover" style="width:100%" border="1">
    <tr>
        <td colspan="8" style="text-align:left;">
        <?php
            $project = App\Model\Project::find(auth()->user()->proj_id);
        ?>
            Project:{{ $project->project ?: '' }}

        </td>
    </tr>

 

      @if(!empty($location))
   @foreach($location as $fdates)

      <tr>
        <td colspan="8" style="text-align:left;">

         <?php                        
          $districts = App\Model\District::where('id', $fdates->distid )->get();
           ?>
         @foreach($districts as $district )                     
            @if(\Auth::check())
            District:{{  $district->distname }}
            @endif
        @endforeach
           
        </td>
    </tr>
    <tr>
        <td colspan="8" style="text-align:left;">

         <?php                        
          $upazilas = App\Model\Upazila::where('id',  $fdates->upid)->get();
           ?>
         @foreach($upazilas as $upazila )                     
            @if(\Auth::check())
            Upazila:{{  $upazila->upname }}
            @endif
        @endforeach

    
        </td>
    </tr>
    <tr>
        <td colspan="8" style="text-align:left;">

            <?php                        
          $unions = App\Model\Union::where('id',  $fdates->unid)->get();
           ?>
         @foreach($unions as $union )                     
            @if(\Auth::check())
            Union:{{  $union->unname }}
            @endif
        @endforeach

          
        </td>
    </tr>


           @endforeach
           @endif
 
</table>

<?php
          $index = 1;
           ?>
<table class="box" border="1" width="100%">
 <thead>
    <tr>
      <th >SL</th>
    
            <th >TW NO</th>
       <th >Ward</th>
      <th >Village</th>
     
      <th >Land Owner Name</th>
      <th >Caretaker(M) Name</th>
      <th >Caretaker (F) Name</th>
       <th > CDF NO</th>
      <th >Date</th>
      <th >Arsenic</th>
      <th >Arsenic Lab</th>
      <th >Iron (Fe)</th>
      <th >Fe Lab</th>
      <th >Mn</th>
      <th >Mn Lab</th>
      <th >Cl</th>
      <th >Cl Lab</th>
      <th >Ph</th>
      <th >Pb</th>
      <th >Zinc</th>
      <th >Fc</th>
      <th >Td</th>
      <th >Turbidity</th>
    </tr>
    </thead>
    

        <tbody>
     @foreach($waters as $water)
           <tr>
     <td >{{ $index++ }}</td>
         
         <td  >{{$water->water_id}}</td>
     <td >{{$water->Ward_no or ''}}</td>
        <td  >{{$water->Village}}</td>
  
        <td  >{{$water->Landowner}}</td>
        <td  >{{$water->Caretaker_male}}</td>
        <td  >{{$water->Caretaker_female}}</td>
        <td  >{{$water->CDF_no}}</td>

        <td >{{$water->report_date}}</td>
        <td  >{{$water->arsenic}}</td>
        <td  >{{$water->as_lab}}</td>
        <td  >{{$water->fe}}</td>
        <td  >{{$water->fe_lab}}</td>
        <td  >{{$water->mn}}</td>
        <td  >{{$water->mn_lab}}</td>
        <td  >{{$water->cl}}</td>
        <td  >{{$water->cl_lab}}</td>
        <td  >{{$water->ph}}</td>
        <td  >{{$water->pb}}</td>
        <td  >{{$water->zinc}}</td>
        <td  >{{$water->fc}}</td>
        <td  >{{$water->td}}</td>
        <td  >{{$water->turbidity}}</td>
      </tr>

      @endforeach
          </tbody>

    </table> 

     


@endif 


@endsection
