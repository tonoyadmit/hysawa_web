@extends('layouts.appinside')

@section('content')
<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('district-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Water</span>
    </li>
  </ul>
</div>
  <div class="col-md-12" style="margin-top: 10px;">
  @include('core.district.water._search')
  </div>
  @include('core.district.water._table')

@endsection

@section('my_js')

<script type="text/javascript">
    $(document).ready(function () {

        $('#district_id').on('change', function() {
            var district_id = $(this).val();

            $("#upazila_id").html("Select District")
            $("#union_id").html("Select Upazila");

            $.ajax({
              url: '{{route('ajax.upazilas-from-district')}}?district_id='+district_id,
              type: 'GET',
              success: function(response){
                if(response['status'] == true){
                  $("#upazila_id").html(response['upazila_list']);
                }
               // console.log(response);
              }
            });
        });

        $('#upazila_id').on('change', function() {
          var upazila_id= $(this).val();
          $("#union_id").html("Select Upazila");
          $.ajax({
              url: '{{route('ajax.unions-from-upazila')}}?upazila_id='+upazila_id,
              type: 'GET',
              success: function(response){
                if(response['status'] == true){
                  $("#union_id").html(response['union_list']);
                }
                //console.log(response);
              }
            });
        });

    });
</script>
@endsection