@extends('layouts.print.app')

@section('my_style')
<style>
    table{
      border-collapse: collapse;
      border-spacing: 0;
    }

    div{
      font-size: 80%;
      display: inline-block;
     /* overflow: visible;

      width: 2.4em;
      line-height: 0.9em;
      margin-top: 9em;

      white-space: nowrap;
      -ms-transform: rotate(270deg);
      -o-transform: rotate(270deg);
      -webkit-transform: rotate(270deg);
      transform: rotate(270deg);
      text-align: left;*/
    }

    .headline th {
      padding: 5px 20px;
    }
  </style>
@endsection

@section('content')

<p>Project: {{App\Model\Project::find(auth()->user()->proj_id)->project}}</p>

@if(isset($_REQUEST['query']))

    <p>Query:

    <?php
        $districtName = isset($_REQUEST['district_id']) ? $_REQUEST['district_id'] : "";
        $upazilaName = isset($_REQUEST['upazila_id']) ? $_REQUEST['upazila_id'] : "";
        $unionName = isset($_REQUEST['union_id']) ? $_REQUEST['union_id'] : "";
        if($districtName != "" && $districtName != "all"){
            $districtName = App\Model\District::find($_REQUEST['district_id'])->distname;
            if($upazilaName != "" && $upazilaName != "all"){
                $upazilaName = App\Model\Upazila::find($_REQUEST['upazila_id'])->upname;
                if($unionName != "" && $unionName != "all"){
                    $unionName = App\Model\Union::find($_REQUEST['union_id'])->unname;
                }
            }
        }
    ?>

    <b>District</b>: {{$districtName}}|
    <b>Upazila</b>: {{$upazilaName}}|
    <b>Union</b>: {{$unionName}}|
    <b>Ward</b>: {{$_REQUEST['ward_no']}}|
    <b>Village</b>: {{$_REQUEST['village']}}|
    <b>App Status</b>: {{$_REQUEST['app_status']}}|
    <b>Approval Date</b>: {{$_REQUEST['starting_date']}}|
    <b>Imp. Status</b>: {{$_REQUEST['imp_status']}}
    </p>

@endif
<h2>Site list of {{count($waters)}} water points</h2>

<table class="box" border="1">
    <thead class="rotate">

      <th><div>District</div></th>
      <th><div>Upazila</div></th>
      <th><div>Union</div></th>

      <th><div>Word No</div></th>
      <th><div>CDF No</div></th>
      <th><div>Village</div></th>
      <th><div>TW No</div></th>
      <th><div>Technology</div></th>
      <th><div>Landowner</div></th>
      <th><div>Caretaker Male</div></th>
      <th><div>Care Female</div></th>
      <th><div>Total HH</div></th>
      <th><div>Harccode HH</div></th>
    </thead>
    <tbody>
      @foreach($waters as $water)
      <tr>
        <td><div> {{$water->district->distname or ''}} </div></td>
        <td><div> {{$water->upazila->upname or ''}} </div></td>
        <td><div> {{$water->union->unname or ''}} </div></td>

        <td><div>{{$water->Ward_no}}</div></td>
        <td><div>{{$water->CDF_no}}</div></td>
        <td><div>{{$water->Village}}</div></td>
        <td><div>{{$water->TW_No}}</div></td>
        <td><div>{{$water->Technology_Type}}</div></td>

        <td><div>{{$water->Landowner}}</div></td>
        <td><div>{{$water->Caretaker_male}}</div></td>
        <td><div>{{$water->Caretaker_female}}</div></td>
        <td><div>{{$water->HH_benefited}}</div></td>
        <td><div>{{$water->HCHH_benefited}}</div></td>
      </tr>
      @endforeach
    </tbody>
  </table>
@endsection