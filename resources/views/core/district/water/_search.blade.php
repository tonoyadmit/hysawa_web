<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Water
        </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body form">
    <style type="text/css">
        .form-group{
            margin-bottom: 5px;
        }
        .control-label{
            font-size: 10px;
        }
    </style>

        <form action="{{route('district-admin.water.index')}}" class="form-horizontal" method="GET" id="searchForm">

            <input type="hidden" name="query" value="{{time()}}" />

            <div class="form-body">

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">District</label>
                            <div class="col-md-9">

                                <select class="form-control input-sm" name="district_id" id="district_id">
                                    <?php

                                        // $districts = \DB::table('fdistrict')
                                        //                 ->where('region_id', \Auth::user()->region_id)
                                        //                 ->get();
                                                        
                                                        $districts = App\Model\District::all();
                                    ?>

                                    <option value="all"

                                    @if(isset($_REQUEST['district_id']) && $_REQUEST['district_id'] == 'all' || !isset($_REQUEST['district_id']))
                                        selected="selected"
                                    @endif

                                    >All</option>
                                    @foreach($districts as $district)
                                        <option value="{{$district->id}}"
                                        @if($old['district_id'] == $district->id)
                                            selected="selected"
                                        @endif
                                        >{{$district->distname}}</option>
                                    @endforeach

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Upazila</label>
                            <div class="col-md-9">
                                <select class="form-control input-sm" name="upazila_id" id="upazila_id">
                                    @if(empty($old['district_id']) && $old['district_id'] != "all")
                                        <?php
                                            $fupazilas = \DB::table('fupazila')
                                                            ->where('disid', $old['district_id'])
                                                            ->get();
                                        ?>
                                        <option value="all">All</option>
                                        @foreach($fupazilas as $fupazila)
                                            <option value="{{$fupazila->id}}"
                                            @if($old['upazila_id'] == $fupazila->id)
                                                selected="selected"
                                            @endif
                                            >{{$fupazila->upname}}</option>
                                        @endforeach

                                    @else
                                        <option value="">Select District </option>
                                    @endif

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Union</label>
                            <div class="col-md-9">
                                <select class="form-control input-sm" name="union_id" id="union_id">
                                    @if(empty($old['upazila_id']) && $old['upazila_id'] != "all")
                                        <?php
                                        $funions = \DB::table('funion')
                                        ->where('upid', $old['upazila_id'])
                                        ->get();
                                        ?>
                                        <option value="all">All</option>
                                        @foreach($funions as $funion)
                                        <option value="{{$funion->id}}"
                                            @if($old['union_id'] == $funion->id)
                                            selected="selected"
                                            @endif
                                            >{{$funion->unname}}</option>
                                        @endforeach

                                        @else
                                        <option value="">Select Upazila </option>
                                        @endif

                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Ward</label>
                            <div class="col-md-9">

                                <select class="form-control input-sm" name="ward_no">
                                    <option value="">All</option>
                                    <option value="1" @if(count($old) && $old['ward_no'] == "1") selected="selected" @endif >1</option>
                                    <option value="2" @if(count($old) && $old['ward_no'] == "2") selected="selected" @endif >2</option>
                                    <option value="3" @if(count($old) && $old['ward_no'] == "3") selected="selected" @endif >3</option>
                                    <option value="4" @if(count($old) && $old['ward_no'] == "4") selected="selected" @endif >4</option>
                                    <option value="5" @if(count($old) && $old['ward_no'] == "5") selected="selected" @endif >5</option>
                                    <option value="6" @if(count($old) && $old['ward_no'] == "6") selected="selected" @endif >6</option>
                                    <option value="7" @if(count($old) && $old['ward_no'] == "7") selected="selected" @endif >7</option>
                                    <option value="8" @if(count($old) && $old['ward_no'] == "8") selected="selected" @endif >8</option>
                                    <option value="9" @if(count($old) && $old['ward_no'] == "9") selected="selected" @endif >9</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Village</label>
                            <div class="col-md-9">
                                <input
                                type="text"
                                class="form-control input-sm"
                                name="village"
                                placeholder="Type Village Name"

                                @if(!empty( $old['village']))
                                value="{{$old['village']}}"
                                @endif
                                />

                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">App Status</label>
                            <div class="col-md-9">

                                <select name="app_status" class="form-control input-sm">
                                    <option value="">Choose one</option>
                                    <?php
                                    $appStatus = \DB::table('app_status')->orderBy('app_status')->get();
                                    ?>
                                    @foreach($appStatus as $status)
                                    <option value="{{$status->app_status}}" @if($old['app_status'] == $status->app_status) selected="selected" @endif >{{$status->app_status}}</option>
                                    @endforeach

                                </select>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Approval Date(YYY-MM-DD)</label>
                            <div class="col-md-9">

                                <input class="form-control date-picker input-sm" type="text" name="starting_date" data-date-format="yyyy-mm-dd"
                                @if(!empty( $old['starting_date']))
                                value="{{date('Y-m-d', strtotime($old['starting_date']))}}"
                                @endif >

                            </div>
                        </div>
                    </div>

                    {{--<div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3 input-sm">App. Ending Date</label>
                            <div class="col-md-9">

                                <input class="form-control date-picker input-sm"
                                    type="text"
                                    name="ending_date"
                                    data-date-format="yyyy-mm-dd"
                                @if(!empty( $old['ending_date']))
                                    value="{{date('Y-m-d', strtotime($old['ending_date']))}}"
                                @endif >

                            </div>
                        </div>
                    </div> --}}
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="control-label col-md-3">Imp. Status</label>
                            <div class="col-md-9">

                                <select name="imp_status" class="form-control input-sm">
                                    <option value="">Choose one</option>
                                    <?php
                                    $implStatus = \DB::table('imp_status')->orderBy('imp_status')->get();
                                    ?>
                                    @foreach($implStatus as $is)
                                    <option value="{{$is->imp_status}}" @if($old['imp_status'] == $is->imp_status) selected="selected" @endif >{{$is->imp_status}}</option>
                                    @endforeach

                                </select>

                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="col-md-12">
                                <input type="submit" class="btn green btn-sm" value="Search" />
                                <a href="{{route('district-admin.water.index')}}" class="btn default btn-sm">Clear</a>

                                @if(isset($_REQUEST['query']))
                                    <?php
                                        $old2 = $old;
                                        $old2['download'] = 'download';
                                        $old3 = $old;
                                        $old3['print'] = 'print';
                                    ?>
                                    <a href="{{route('district-admin.water.index', $old2)}}" class="btn blue btn-sm">Download</a>
                                    <a href="{{route('district-admin.water.index', $old3)}}" class="btn default btn-sm">Print</a>
                                     <a href="{{route('district-admin.water.printdata', $_REQUEST)}}" class="btn default btn-sm">Print Water Result</a>
                    
                                @endif
                        <a href="{{route('district-admin.sub-projects.waters.approval')}}" class="btn btn-primary btn-sm pull-right">Approval Summary</a>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </form>

        @if(isset($_REQUEST['query']))
            <p>Query:

            <?php
                $districtName = isset($_REQUEST['district_id']) ? $_REQUEST['district_id'] : "";
                $upazilaName = isset($_REQUEST['upazila_id']) ?$_REQUEST['upazila_id'] : "";
                $unionName = isset($_REQUEST['union_id']) ? $_REQUEST['union_id'] : "";

                if($districtName != "" && $districtName != "all")
                {
                    $districtName = App\Model\District::find($_REQUEST['district_id'])->distname;

                    if($upazilaName != "" && $upazilaName != "all")
                    {
                        $upazilaName = App\Model\Upazila::find($_REQUEST['upazila_id'])->upname;

                        if($unionName != "" && $unionName != "all")
                        {
                            $unionName = App\Model\Union::find($_REQUEST['union_id'])->unname;
                        }
                    }
                }

            ?>

            <b>District</b>: {{$districtName}}|
            <b>Upazila</b>: {{$upazilaName}}|
            <b>Union</b>: {{$unionName}}|
            <b>Ward</b>: {{$_REQUEST['ward_no']}}|
            <b>Village</b>: {{$_REQUEST['village']}}|
            <b>App Status</b>: {{$_REQUEST['app_status']}}|
            <b>Approval Date</b>: {{$_REQUEST['starting_date']}}|
            <b>Imp. Status</b>: {{$_REQUEST['imp_status']}}
            </p>

        @endif

    </div>
</div>