@extends('layouts.appinside')

@section('content')

<div class="page-bar">
  <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('district-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="{{ route('district-admin.water.index') }}">Waters</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Update</span>
    </li>
  </ul>
</div>

<h1 class="page-title"> Waters
  <small>create new</small>
</h1>


<div class="portlet box blue">
  <div class="portlet-title">
      <div class="caption">
          <i class="fa fa-gift"></i>Update Data </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body form">
          <form
            action="{{route('district-admin.water.update', $water->id)}}"
            class="form-horizontal"
            method="POST"
            id="updateForm"

            >
              <input name="_method" type="hidden" value="PUT"/>

              {{csrf_field()}}

              <div class="form-body">

                  <div class="row">
                      <div class="form-group">
                        <label class="control-label col-md-3">CDF_no: (3 digits)</label>
                        <div class="col-md-6">
                          <label class="control-label"><b>{{$water->CDF_no}}</b></label>
                        </div>
                      </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                      <label class="control-label col-md-3">Village:</label>
                      <div class="col-md-6">
                        <label class="control-label"><b>{{$water->Village}}</b></label>
                      </div>
                    </div>
                  </div>


                  <div class="row">
                      <div class="form-group">
                        <label class="control-label col-md-3">Land Owner name:</label>
                        <div class="col-md-6">
                          <label class="control-label"><b>{{$water->Landowner}}</b></label>
                        </div>
                      </div>
                  </div>


                  <div class="row">
                      <div class="form-group">
                        <label class="control-label col-md-3">Caretaker(male) name:</label>
                        <div class="col-md-6">
                          <label class="control-label"><b>{{$water->Caretaker_male}}</b></label>
                        </div>
                      </div>
                  </div>

                  <div class="row">
                    <div class="form-group">
                      <label class="control-label col-md-3">Approval Status</label>
                      <div class="col-md-6">

                        <select name="app_status" class="form-control">
                          <option value="">Choose one</option>
                          <?php
                            $appStatus = \DB::table('app_status')->orderBy('app_status')->get();
                          ?>
                          @foreach($appStatus as $status)
                          <option value="{{$status->app_status}}"
                            @if($water->app_status == $status->app_status) selected="selected"
                            @endif >{{$status->app_status}}</option>
                          @endforeach

                        </select>

                      </div>
                    </div>
                  </div>

                  <div class="row">
                      <div class="form-group">
                        <label class="control-label col-md-3">Approval date: </label>
                        <div class="col-md-6">
                          <input type="text" class="form-control date-picker"
                            value="{{$water->App_date}}"
                            name="App_date"
                            data-date-format="yyyy-mm-dd"
                            readonly
                            />
                          <span class="help-block"> (YYYY-MM-DD) </span>
                        </div>
                      </div>
                  </div>

             <div class="row">
                    <div class="form-group">
                      <label class="control-label col-md-3">Implementation Status</label>
                      <div class="col-md-6">

                        <select name="imp_status" class="form-control">
                          <option value="">Choose one</option>
                          <?php
                            $appStatus = \DB::table('imp_status')->orderBy('imp_status')->get();
                          ?>
                          @foreach($appStatus as $status)
                          <option value="{{$status->imp_status}}"
                            @if($water->imp_status == $status->imp_status) selected="selected"
                            @endif >{{$status->imp_status}}</option>
                          @endforeach

                        </select>

                      </div>
                    </div>
                  </div>


                  <div class="row">
                    <div class="form-group">
                      <label class="control-label col-md-3">Work Order / Log No:</label>
                      <div class="col-md-6">
                        <select name="Tend_lot" class="form-control">
                        <option value="1" @if($water->Tend_lot == "1") selected="selected" @endif>1</option>
                          <option value="2" @if($water->Tend_lot == "2") selected="selected" @endif>2</option>
                          <option value="3" @if($water->Tend_lot == "3") selected="selected" @endif>3</option>
                          <option value="4" @if($water->Tend_lot == "4") selected="selected" @endif>4</option>

                        </select>
                      </div>

                    </div>
                  </div>


              </div>
              <div class="form-actions">
                  <div class="col-md-offset-2 col-md-10">
                      <input type="submit" class="btn green" value="Update" />
                  </div>
              </div>
          </form>
      </div>
  </div>

</div>
@endsection
