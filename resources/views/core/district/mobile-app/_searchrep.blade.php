<div class="portlet box blue">

    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i>Monthly Report <small> view</small> </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body form">
        <form action="{{route('district-admin.mobile-app.userrep')}}" class="form-horizontal" method="GET" id="searchForm">

            <input type="hidden" name="query" value="{{time()}}" />

            <div class="form-body">
                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Project</label>
                            <div class="col-md-9">


                                <?php

                                    $projects = App\Model\Project::all();

                                ?>
                                <select class="form-control" name="project_id">
                                    <option value="">Choose a Project</option>
                                    @foreach($projects as $project )
                                        <option value="{{$project->id}}"

                                        @if(isset($old['project_id']) && $old['project_id'] == $project->id)
                                            selected="selected"
                                        @endif


                                        >{{$project->project}}</option>
                                    @endforeach
                                </select>

                               
                            </div>
                        </div>
                    </div>

                     <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Region</label>
                            <div class="col-md-9">


                       <?php
                            $regions = App\Model\Region::all();
                        ?>
                                <select class="form-control" name="region_id">
                                <option value="">Choose a Region</option>
                            @foreach($regions as $region )
                                <option value="{{$region->id}}"
                                @if(!empty($old) && !empty($old['region_id']) && $old['region_id'] == $region->id)
                                    selected="selected"
                                @endif
                                >{{$region->region_name}}</option>
                            @endforeach
                                </select>

                               
                            </div>
                        </div>
                    </div>



                     <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Starting Date</label>
                            <div class="col-md-9">
                                <input class="form-control date-picker input-sm" type="text" name="starting_date" data-date-format="yyyy-mm-dd"
                                @if(!empty( $old['starting_date']))
                                value="{{date('Y-m-d', strtotime($old['starting_date']))}}"
                                @endif >

                              </div>
                        </div>
                    </div>

                       <div class="col-md-4">
                        <div class="form-group">
                            <label class="control-label col-md-3">Ending Date</label>
                            <div class="col-md-9">
                            <input class="form-control date-picker input-sm"
                                    type="text"
                                    name="ending_date"
                                    data-date-format="yyyy-mm-dd"
                                @if(!empty( $old['ending_date']))
                                    value="{{date('Y-m-d', strtotime($old['ending_date']))}}"
                                @endif >

                              </div>
                        </div>
                    </div>


                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <div class="col-md-5">
                                <input type="submit" class="btn green" value="Search" />
                                 <a href="{{route('district-admin.mobile-app.userrep')}}" class="btn default">Clear</a> 

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </form>

    </div>
</div>