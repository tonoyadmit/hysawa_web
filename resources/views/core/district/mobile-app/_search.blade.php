<div class="portlet box blue">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-gift"></i> Mobile-App : <small> Data-List</small> </div>
        <div class="tools">
            <a href="javascript:;" class="collapse"> </a>
        </div>
    </div>

    <div class="portlet-body form">
        <form action="{{route('district-admin.mobile-app.index')}}" class="form-horizontal" method="GET" id="searchForm">



            <input type="hidden" name="query" value="{{time()}}" />

            <div class="form-body">

                <div class="row">

                    <div class="col-md-2">
                        <label class="control-label">Starting Date</label>
                        <input
                            class="form-control input-sm date-picker"
                            type="text"
                            name="starting_date"
                            data-date-format="yyyy-mm-dd"

                        @if(!empty( $old['starting_date']))
                            value="{{date('Y-m-d', strtotime($old['starting_date']))}}"
                        @endif />
                        <p style="margin-bottom: 10px;">YYYY-MM-DD</p>
                    </div>

                    <div class="col-md-2">
                        <label class="control-label">Ending Date</label>
                        <input
                            class="form-control input-sm date-picker"
                            type="text"
                            name="ending_date"
                            data-date-format="yyyy-mm-dd"

                        @if(!empty( $old['ending_date']))
                            value="{{date('Y-m-d', strtotime($old['ending_date']))}}"
                        @endif />

                        <p style="margin-bottom: 10px;">YYYY-MM-DD</p>
                    </div>


                    <div class="col-md-2">
                        <label class="control-label">Type</label>
                        <select name="type" class="form-control input-sm" >
                            <?php
                                $data = App\Model\MobAppDataList::groupBy('type')->get();
                            ?>
                            <option value="">Select Type</option>

                            @foreach($data as $type)
                            <option
                            value="{{$type->type}}"
                            @if(
                                !empty($old['type']) &&
                                $old['type'] != "" &&
                                $type->type == $old['type']
                            )
                                selected="selected"
                            @endif
                            >{{$type->type}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <label class="control-label">District</label>
                        <select name="district_id" class="form-control input-sm" id="district">
                                    <option value="">Select Region</option>

                                        <?php
                                        //    $districts = \DB::table('fdistrict')->where('region_id', auth()->user()->region_id)->get();
                                       $districts = App\Model\District::all();
                                        ?>
                                        @foreach($districts as $district)
                                        <option
                                            value="{{$district->id}}"
                                            @if($district->id == $old['district_id'])
                                            selected="selected"
                                            @endif
                                        >{{$district->distname}}</option>
                                        @endforeach

                                </select>



                    </div>

                    <div class="col-md-2">
                        <label class="control-label">Upazila</label>
                        <select name="upazila_id" class="form-control input-sm" id="upazila">
                                    <option value="">Select District</option>

                                    @if(!empty($old['district_id']))
                                        <?php
                                        $upazilas = \DB::table('fupazila')->where('disid',$old['district_id'])->get();
                                        ?>
                                        @foreach($upazilas as $upazila)
                                        <option
                                        value="{{$upazila->id}}"
                                        @if(!empty($old['upazila_id']) && $upazila->id == $old['upazila_id'])
                                        selected="selected"
                                        @endif
                                        >{{$upazila->upname}}</option>
                                        @endforeach
                                    @endif

                                </select>



                    </div>

                    <div class="col-md-2">
                        <label class="control-label">Union</label>
                        <select name="union_id" class="form-control input-sm" id="union">
                                    @if(!empty($old['upazila_id']))

                                    <?php
                                        $unions = \DB::table('funion')->where('upid', $old['upazila_id'])->get();
                                    ?>
                                    @foreach($unions as $union)
                                    <option
                                    value="{{$union->id}}"
                                    @if(!empty($old['union_id']) && $union->id == $old['union_id'])
                                    selected="selected"
                                    @endif
                                    >{{$union->unname}}</option>
                                    @endforeach

                                    @endif
                                </select>

                    </div>

                </div>
                <div class="row">
                    <div class="col-md-2">
                        <label class="control-label">UserId</label>
                        <input type="text" name="user_id" @if($old['user_id'] != "") value="{{$old['user_id']}}" @endif class="form-control" />
                    </div>

                    <div class="col-md-4">
                        <input type="submit" class="btn green" value="Search" style="margin-top: 25px;" />
                        <a href="{{route('district-admin.mobile-app.index')}}" class="btn default" style="margin-top: 25px;">Clear</a>
                                                <a href="{{route('district-admin.mobile-app.userrep')}}" class="btn default" style="margin-top: 25px;">Report</a>
                        <?php
                            $old2 = $old;
                            $old2['print'] = 'print';
                        ?>
                        <a href="{{route('district-admin.mobile-app.index', $old2)}}" class="btn btn-info btn-sm pull-right" style="margin-top: 25px;">Print</a>

                    </div>

                </div>


            </div>
        </form>

        @if(isset($_REQUEST['query']) && $_REQUEST['query'] != "")

          <div class="row">
            <div class="col-md-12">

              <p>Query:
              <?php
                $regionName = App\Model\Region::find(auth()->user()->region_id)->region_name;
                $districtName = isset($_REQUEST['district_id']) ? $_REQUEST['district_id']  : "";
                $upazilaName  = isset($_REQUEST['upazila_id'])  ? $_REQUEST['upazila_id']   : "";
                $unionName    = isset($_REQUEST['union_id'])    ? $_REQUEST['union_id']     : "";

                if($districtName != "" && $districtName != "all")
                {
                  $districtName = App\Model\District::find($_REQUEST['district_id'])->distname;

                  if(isset($_REQUEST['upazila_id']) && $_REQUEST['upazila_id'] != "" && $_REQUEST['upazila_id'] != "all")
                  {
                    $upazilaName = App\Model\Upazila::find($_REQUEST['upazila_id'])->upname;

                    if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] != "" && $_REQUEST['union_id'] != "all")
                    {
                      $unionName = App\Model\Union::find($_REQUEST['union_id'])->unname;
                    }
                  }
                }

              ?>

                <b>Starting Date:</b> {{$_REQUEST['starting_date']}}|
                <b>Ending Date:</b> {{$_REQUEST['ending_date']}}
                <b>Type:</b> {{$_REQUEST['type'] or ''}}
                <b>District:</b> {{$districtName or ''}}|
                <b>Upazila:</b> {{$upazilaName or ''}}|
                <b>Union:</b> {{$unionName or ''}}|

              </p>

            </div>
          </div>
          @endif


    </div>
</div>