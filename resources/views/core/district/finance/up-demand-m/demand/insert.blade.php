@extends('layouts.appinside')

@section('content')
<div class="page-bar">
    <ul class="page-breadcrumb">
    <li>
      <a href="{{ route('up-admin.dashboard') }}">Home</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
        <span>Finance</span>
        <i class="fa fa-circle"></i>
    </li>
    <li>
      <a href="{{ route('up-admin.survey_details.index') }}">Demand</a>
      <i class="fa fa-circle"></i>
    </li>
    <li>
      <span>Update</span>
    </li>
  </ul>
</div>

<div class="portlet box green" style="margin-top: 10px;">
    <div class="portlet-title">
        <div class="caption">
            <i class="icon-share "></i>
            <span class="caption-subject bold ">Demand <small>Update</small> </span>
        </div>
        <div class="actions">
            <a class="btn btn-circle btn-icon-only btn-default fullscreen" href="javascript:;" data-original-title="" title=""> </a>
        </div>
    </div>
    <div class="portlet-body form">

        {!! Form::open(array('route' => ['up-admin.survey_details.store','fundlistid='.app('request')->input('fundlistid')], 'method' => 'post','class' => 'form-horizontal')) !!}
            {{csrf_field()}}

            <div class="form-body">
                @include('partials.errors')

                <div class="form-group">
                    <label class="control-label col-md-3">Head</label>
                    <div class="col-md-4">
                        {!! Form::select('head', array(''=>'Select Head') + $head, null, ['class' => 'form-control', 'required' => 'required','id'=>'head']) !!}
                    </div>

                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">Sub Head</label>
                    <div class="col-md-4">
                        {!! Form::select('subhead', array(''=>'Select Sub Head'), null, ['class' => 'form-control', 'required' => 'required','id'=>'subhead']) !!}
                    </div>

                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">Item</label>
                    <div class="col-md-4">
                        {!! Form::select('item', array(''=>'Select Item'), null, ['class' => 'form-control', 'required' => 'required','id'=>'item']) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">Amount:</label>
                    <div class="col-md-4">
                        {!! Form::text('amount', null, ['class' => 'form-control', 'placeholder' => 'Amount', 'required' => 'required']) !!}
                    </div>
                </div>

                {{--  <div class="form-group">
                    <label class="control-label col-md-3">From Date: YYYY-MM-DD</label>
                    <div class="col-md-4">
                        {!! Form::text('date', null, ['class' => 'form-control date-picker', 'placeholder' => 'Date', 'required' => 'required', 'data-date-format' =>"yyyy-mm-dd"]) !!}
                    </div>
                </div>

                 <div class="form-group">
                    <label class="control-label col-md-3">To Date: YYYY-MM-DD</label>
                    <div class="col-md-4">
                        {!! Form::text('todate', null, ['class' => 'form-control date-picker', 'placeholder' => 'todate', 'required' => 'required', 'data-date-format' =>"yyyy-mm-dd"]) !!}
                    </div>
                </div>  --}}

                <div class="form-group">
                    <label class="control-label col-md-3">Remarks:</label>
                    <div class="col-md-4">
                        {!! Form::text('remarks', null, ['class' => 'form-control', 'placeholder' => 'Remarks']) !!}
                    </div>
                </div>

            </div>

            <div class="form-actions right1">
                <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                        <a href="javascript:history.back()" class="btn default"> Cancel </a>
                        {!! Form::submit('Save', ['class' => 'btn green']) !!}
                    </div>
                </div>
            </div>

        {!! Form::close() !!}
    </div>
</div>

@endsection


@section('my_js')
<script type="text/javascript" language="javascript" >
    $(document).ready(function () {
        $('#head').on('change', function() {
            var head=$(this).val();
            var csrftoken = $("#csrf-token").val();
            if(head==''){
                $('#subhead').attr('disabled', 'disabled');
            }else{
                $.getJSON('{{ route('up-admin.income.getSubhead') }}?head='+head+'&_token='+csrftoken, function (data) {
                    $('select[name="subhead"]').empty();
                    $.each(data, function(key, value) {
                        $('select[name="subhead"]').append('<option value="'+ key +'">'+ value +'</option>');
                    });
                });
            }
        });
        $('#subhead').on('change', function() {
            var subhead=$(this).val();
            var head=$("#head").val();
            var csrftoken = $("#csrf-token").val();
            if(head==''){
                $('#subhead').attr('disabled', 'disabled');
            }else{
                $.getJSON('{{ route('up-admin.income.getSubItem') }}?head='+head+'&_token='+csrftoken+'&subhead='+subhead, function (data) {
                    $('select[name="item"]').empty();
                    $.each(data, function(key, value) {
                        $('select[name="item"]').append('<option value="'+ key +'">'+ value +'</option>');
                    });
                });
            }
        });
    });
</script>
@endsection