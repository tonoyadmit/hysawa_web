<div class="col-md-12" style="margin-top: 10px;">
    <div class="portlet box blue">
      <div class="portlet-title">
          <div class="caption">
              <i class="fa fa-gift"></i>Report <small> Region Report</small> </div>
          <div class="tools">
              <a href="javascript:;" class="collapse"> </a>
          </div>
      </div>

      <div class="portlet-body form">

          <form action="{{route('district-admin.finance.report.index')}}" class="form-horizontal" method="GET" id="createForm">

              <input type="hidden" name="query" value="{{time()}}" />

              <div class="form-body">
                  <div class="row">

                      <div class="col-md-2">
                        <label class="control-label col-md-4">District</label>

                        <select name="district_id" class="form-control input-sm" id="district_id" required="required">
                          <option value="all"
                            @if(isset($_REQUEST['district_id']) && $_REQUEST['district_id'] == "all")
                              selected="selected"
                            @endif
                          >All</option>
                          <?php
                            // $districts = App\Model\District::where('region_id', auth()->user()->region_id)->get();
                            
                            $districts = App\Model\District::all();
                          ?>
                          @foreach($districts as $dist)
                            <option value="{{$dist->id}}"
                            @if(isset($_REQUEST['district_id']) && $_REQUEST['district_id'] == $dist->id)
                              selected="selected"
                            @endif
                            >{{$dist->distname}}</option>
                          @endforeach
                        </select>

                      </div>

                      <div class="col-md-2">
                        <label class="control-label">Upazila</label>
                        <select name="upazila_id" class="form-control input-sm" id="upazila_id">

                        @if(isset($_REQUEST['district_id']) && $_REQUEST['district_id'] != "all")
                          <option value="">Choose One</option>
                          <option value="all"
                            @if(isset($_REQUEST['upazila_id']) && $_REQUEST['upazila_id'] == "all")
                              selected="selected"
                            @endif
                          >All</option>
                          <?php
                            $upazilas = App\Model\Upazila::where('disid', $_REQUEST['district_id'])->get();
                          ?>
                          @foreach($upazilas as $upazila)
                            <option value="{{$upazila->id}}"
                            @if(isset($_REQUEST['upazila_id']) && $_REQUEST['upazila_id'] == $upazila->id)
                              selected="selected"
                            @endif
                            >{{$upazila->upname}}</option>
                          @endforeach
                        @endif
                        </select>

                      </div>

                      <div class="col-md-2">
                        <label class="control-label">Union</label>
                        <select name="union_id" class="form-control input-sm" id="union_id">
                          @if(isset($_REQUEST['upazila_id']) && $_REQUEST['upazila_id'] != "")

                            <option value="">Choose One</option>

                            <option value="all"
                              @if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] == "all")
                                selected="selected"
                              @endif
                            >All</option>

                            <?php
                              $unions = App\Model\Union::where('upid', $_REQUEST['upazila_id'])->get();
                            ?>
                            @foreach($unions as $union)
                              <option value="{{$union->id}}"
                              @if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] == $union->id)
                                selected="selected"
                              @endif
                              >{{$union->unname}}</option>
                            @endforeach

                          @endif

                        </select>

                      </div>



                      <div class="col-md-2">
                        <label class="control-label">Starting Date</label>
                        <input
                          type="text"
                          name="starting_date"
                          class="form-control date-picker input-sm"
                          data-date-format = "yyyy-mm-dd"

                          @if(isset($_REQUEST['starting_date']) && $_REQUEST['starting_date'] != "")
                            value="{{$_REQUEST['starting_date']}}"
                          @endif

                          required="required"

                        />
                        <p style="margin-bottom: 0px;">YYYY-MM-DD</p>
                      </div>


                      <div class="col-md-2">
                        <label class="control-label ">Ending Date</label>
                        <input
                          type="text"
                          name="ending_date"
                          class="form-control date-picker input-sm"
                          data-date-format = "yyyy-mm-dd"

                            @if(isset($_REQUEST['ending_date']) && $_REQUEST['ending_date'] != "")
                              value="{{$_REQUEST['ending_date']}}"
                            @endif
                            required="required"
                          />
                          <p style="margin-bottom: 0px;">YYYY-MM-DD</p>
                      </div>


                      <div class="col-md-4">
                          <input type="submit" class="btn green" value="Get Data" />
                            <a href="{{route('district-admin.finance.report.index')}}" class="btn btn-info">Clear</a>
                            @if(isset($_REQUEST['query']) )
                              <a href="{{route('district-admin.finance.report.print', $_REQUEST)}}" class="btn btn-success">Print</a>
                            @endif
                      </div>
                  </div>


              </div>
          </form>

          @if(isset($_REQUEST['query']) && $_REQUEST['query'] != "")

          <div class="row">
            <div class="col-md-12">

              <p>Query:
              <?php
                $regionName = App\Model\Region::find(auth()->user()->region_id)->region_name;
                $districtName = isset($_REQUEST['district_id']) ? $_REQUEST['district_id']  : "";
                $upazilaName  = isset($_REQUEST['upazila_id'])  ? $_REQUEST['upazila_id']   : "";
                $unionName    = isset($_REQUEST['union_id'])    ? $_REQUEST['union_id']     : "";

                if($districtName != "" && $districtName != "all")
                {
                  $districtName = App\Model\District::find($_REQUEST['district_id'])->distname;

                  if(isset($_REQUEST['upazila_id']) && $_REQUEST['upazila_id'] != "" && $_REQUEST['upazila_id'] != "all")
                  {
                    $upazilaName = App\Model\Upazila::find($_REQUEST['upazila_id'])->upname;

                    if(isset($_REQUEST['union_id']) && $_REQUEST['union_id'] != "" && $_REQUEST['union_id'] != "all")
                    {
                      $unionName = App\Model\Union::find($_REQUEST['union_id'])->unname;
                    }
                  }
                }

              ?>

                <b>Region: </b> {{$regionName or ''}}|
                <b>District:</b> {{$districtName or ''}}|
                <b>Upazila:</b> {{$upazilaName or ''}}|
                <b>Union:</b> {{$unionName or ''}}|
                <b>Starting Date:</b> {{$_REQUEST['starting_date']}}|
                <b>Ending Date:</b> {{$_REQUEST['ending_date']}}
              </p>

            </div>
          </div>
          @endif
      </div>
    </div>
  </div>


  <script type="text/javascript">

  $(document).ready(function(){

    $('#district_id').on('change', function() {
      var district_id = $(this).val();

      $("#upazila_id").html("Select District");
      $("#union_id").html("Select Upazila");

      $.ajax({
        url: '{{route('ajax.upazilas-from-district')}}?district_id='+district_id+'&not_all=false&choose_one=true',
        type: 'GET',
        success: function(response){
          if(response['status'] == true){
            $("#upazila_id").html(response['upazila_list']);
          }
        }
      });
    });

    $('#upazila_id').on('change', function() {
      var upazila_id = $(this).val();
      $("#union_id").html("Select Upazila");
      $.ajax({
        url: '{{route('ajax.unions-from-upazila')}}?upazila_id='+upazila_id+'&not_all=false&choose_one=true',
        type: 'GET',
        success: function(response){
          if(response['status'] == true){
            $("#union_id").html(response['union_list']);
          }
        }
      });
    });

  });

  </script>