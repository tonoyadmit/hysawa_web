<li><a href="{{ route('upazila-admin.dashboard') }}"> Home </a></li>
<li><a href="{{ route('upazila-admin.periods.index') }}"> Monthly Report </a></li>
<li class="classic-menu-dropdown">
    <a href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true"> Sub-projects
        <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu pull-left">
        <li>
            <a href="{{route('upazila-admin.water.index')}}">
            <i class="fa fa-bookmark-o"></i> Water Supply </a>
        </li>
        <li>
            <a href="{{route('upazila-admin.sanitation.index')}}">
            <i class="fa fa-bookmark-o"></i> Sanitation </a>
        </li>

    </ul>
</li>
<li><a href="{{ route('upazila-admin.finance.report.index') }}"> Finance Report </a></li>
<li><a href="{{ route('upazila-admin.download.index') }}"> Downloads </a></li>

<li><a href="{{route('upazila-admin.mobile-app.index')}}"> Mobile App </a></li>
