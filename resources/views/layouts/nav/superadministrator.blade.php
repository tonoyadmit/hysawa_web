<li><a href="{{ route('superadmin.dashboard') }}"> Home </a></li>

<li class="classic-menu-dropdown">
    <a href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true"> Sub-projects
        <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu pull-left">
        <li>
            <a href="{{route('superadmin.sub-projects.waters.index')}}">
            <i class="fa fa-bookmark-o"></i> Water Supply </a>
        </li>

        <li class="dropdown-submenu">
            <a href="javascript:;">
                <i class="fa fa-envelope-o"></i> Sanitation </a>
                <ul class="dropdown-menu">
                    <li><a href="{{route('superadmin.sub-projects.sanitation.summary')}}"> Summary </a></li>
                    <li><a href="{{route('superadmin.sub-projects.sanitation.index')}}"> List </a></li>
            </ul>
        </li>
    </ul>
</li>

<li class="classic-menu-dropdown">
    <a href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true"> Monthly Report
        <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu pull-left">
        <li>
            <a href="{{route('superadmin.monthly-report.report.index')}}">
            <i class="fa fa-bookmark-o"></i> Report </a>
        </li>
        <li>
            <a href="{{route('superadmin.monthly-report.wash')}}">
            <i class="fa fa-bookmark-o"></i> UP wise Wash Status </a>
        </li>
    </ul>
</li>


<li class="classic-menu-dropdown">
    <a href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true"> Monitoring
        <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu pull-left">
        <li class="dropdown-submenu">
            <a href="javascript:;">
                <i class="fa fa-envelope-o"></i> Field Visit
            </a>
            <ul class="dropdown-menu">
                <li><a href="{{route('superadmin.monitoring.upcapacity.reportlist.index')}}"> Report List </a></li>
                <li><a href="{{route('superadmin.monitoring.upcapacity.reportlist.create')}}"> Report Entry </a></li>
            </ul>
        </li>
        <li>
            <a href="{{route('superadmin.monitoring.upcapacity.index')}}">
            <i class="fa fa-bookmark-o"></i> UP Capacity </a>
        </li>

    </ul>
</li>


<li class="classic-menu-dropdown">
    <a href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true"> Finance
        <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu pull-left">
        <li class="dropdown-submenu">
            <a href="javascript:;">
                <i class="fa fa-envelope-o"></i> Charts of Accounts
            </a>
            <ul class="dropdown-menu">
                <li><a href="{{route('superadmin.finance.charts-of-account.heads.index')}}"> Head List </a></li>
                <li><a href="{{route('superadmin.finance.charts-of-account.sub-heads.index')}}"> Sub-head List </a></li>
                <li><a href="{{route('superadmin.finance.charts-of-account.items.index')}}"> Items </a></li>
            </ul>
        </li>

        <li class="dropdown-submenu">
            <a href="javascript:;">
                <i class="fa fa-envelope-o"></i> Report
            </a>
            <ul class="dropdown-menu">
                <li><a href="{{route('superadmin.finance.report.summary.index')}}"> Summary by Union  </a></li>
                <li><a href="{{route('superadmin.finance.report.analysis.index')}}"> Analysis by Project/date/UP  </a></li>
                <li><a href="{{route('superadmin.finance.report.union.index')}}"> Union report  </a></li>
                <li><a href="{{route('superadmin.finance.report.upazila.index')}}"> Upazila report  </a></li>
                <li><a href="{{route('superadmin.finance.report.district.index')}}"> District report  </a></li>
                <li><a href="{{route('superadmin.finance.report.project.index')}}"> Project report  </a></li>
                <li><a href="{{route('superadmin.finance.report.consolidated.index')}}"> Consolidated report  </a></li>
                {{-- <li><a href="{{route('superadmin.finance.report.common.index')}}"> Common Report  </a></li> --}}
            </ul>
        </li>


        <li class="dropdown-submenu">
            <a href="javascript:;">
                <i class="fa fa-envelope-o"></i> UP Budget
            </a>
            <ul class="dropdown-menu">
                <li><a href="{{route('superadmin.finance.up-budget.index')}}"> List </a></li>

            </ul>
        </li>

     <li class="nav-item">
       
                       <li class="nav-item"><a class="nav-link " href="{{route('superadmin.finance.up-demand.fund_list.index')}}">UP Demand</a></li>
            </a>
            
        </li>
        <li class="dropdown-submenu">
            <a href="javascript:;">
                <i class="fa fa-envelope-o"></i> Disbursement To UP
            </a>
            <ul class="dropdown-menu">
                <!--<li><a href="{{route('superadmin.finance.disbursment-to-up.disbursment.index')}}"> List  </a></li>-->
                <!--<li><a href="{{route('superadmin.finance.disbursment-to-up.disbursment.create')}}"> Create  </a></li>-->
                <!--<li><a href="{{route('superadmin.finance.disbursment-to-up.fund-disbursment.index')}}"> Entry Track By Date  </a></li>-->
                <!--<li><a href="{{route('superadmin.finance.disbursment-to-up.fund-disbursment.index2')}}"> Entry Track By UP  </a></li>-->
                
                 <li><a href="http://www.hysawa.org/mis/public/superadmin/finance/disbursment-to-up/disbursment"> List  </a></li>
                <li><a href="http://www.hysawa.org/mis/public/superadmin/finance/disbursment-to-up/disbursment/create"> Create  </a></li>
                <li><a href="http://www.hysawa.org/mis/public/superadmin/finance/disbursment-to-up/fund-disbursment/entry-track-by-date"> Entry Track By Date  </a></li>
                <li><a href="http://www.hysawa.org/mis/public/superadmin/finance/disbursment-to-up/fund-disbursment/entry-track-by-up"> Entry Track By UP  </a></li>
            </ul>
        </li>

    </ul>
</li>


<li><a href="{{ route('superadmin.pngo.index') }}"> PNGO List </a></li>


<li class="classic-menu-dropdown">
    <a href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true"> Training
        <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu pull-left">

        <li>
            <a href="{{route('superadmin.training.training.index')}}">
            <i class="fa fa-bookmark-o"></i> Training list </a>
        </li>
        <li>
            <a href="{{route('superadmin.training.training-participant.index')}}">
            <i class="fa fa-bookmark-o"></i> Participant list </a>
        </li>
        <li>
            <a href="{{route('superadmin.training.summary')}}">
            <i class="fa fa-bookmark-o"></i> Summary </a>
        </li>

    </ul>
</li>


<li class="classic-menu-dropdown">
    <a href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true"> Procurement
        <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu pull-left">

        <li>
            <a href="{{route('superadmin.procurement.contractors.index')}}">
            <i class="fa fa-bookmark-o"></i> List of Contractors </a>
        </li>
        <li>
            <a href="{{route('superadmin.procurement.pec.index')}}">
            <i class="fa fa-bookmark-o"></i> PEC </a>
        </li>
        <li>
            <a href="{{route('superadmin.procurement.tec.index')}}">
            <i class="fa fa-bookmark-o"></i> TEC </a>
        </li>
        <li>
            <a href="{{route('superadmin.procurement.list.index')}}">
            <i class="fa fa-bookmark-o"></i> Procurement List </a>
        </li>
        <li>
            <a href="{{route('superadmin.procurement.evaluations.index')}}">
            <i class="fa fa-bookmark-o"></i> Evaluation </a>
        </li>
    </ul>
</li>


<li class="classic-menu-dropdown">
    <a href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true"> UP
        <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu pull-left">

        <li>
            <a href="{{route('superadmin.up.mou.index')}}">
            <i class="fa fa-bookmark-o"></i> List of UPs signed MOU </a>
        </li>
        <li>
            <a href="{{route('superadmin.up.functionaries.index')}}">
            <i class="fa fa-bookmark-o"></i> UP functionaries </a>
        </li>
        <li>
            <a href="{{route('superadmin.up.stuff.index')}}">
            <i class="fa fa-bookmark-o"></i> UP/PNGO Project staff </a>
        </li>

    </ul>
</li>


<li class="classic-menu-dropdown">
    <a href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true"> Other
        <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu pull-left">

        <li><a href="{{ route('superadmin.download.index') }}"> Downloads </a></li>
        <li><a href="{{route('superadmin.water.map.show')}}"> Map </a></li>
        <li><a href="{{route('superadmin.water.map.show2')}}"> Map 2</a></li>
         <li><a href="{{route('superadmin.water.map.mobile_map')}}"> Mobile Map 1</a></li>
        <li><a href="{{route('superadmin.water.map.mobile_map_2')}}"> Mobile Map 2</a></li>
        <li><a href="{{route('superadmin.cdfs.index')}}"> CDF </a></li>
        <li><a href="{{route('superadmin.mobile-app.index')}}"> Mobile App </a></li>
          <li class="nav-item"><a class="nav-link " href="{{route('superadmin.finance.up-demand-m.survey_list.index')}}">HYSAWA TUBEWELL INFO
</a></li>

        <li class="dropdown-submenu">
            <a href="javascript:;">
                <i class="fa fa-envelope-o"></i> User Management
            </a>
            <ul class="dropdown-menu">
                 <li>
                    <a href="{{route('superadmin.user-management.index')}}">
                    <i class="fa fa-bookmark-o"></i> List</a>
                </li>
                <li>
                    <a href="{{route('superadmin.user-management.create')}}">
                    <i class="fa fa-bookmark-o"></i> Add New </a>
                </li>
            </ul>
        </li>

        <li class="dropdown-submenu">
            <a href="javascript:;">
                <i class="fa fa-box"></i> Location
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="{{route('superadmin.location.project.index')}}">
                    <i class="fa fa-bookmark-o"></i> Projects</a>
                </li>
                <li>
                    <a href="{{route('superadmin.location.region.index')}}">
                    <i class="fa fa-bookmark-o"></i> Regions</a>
                </li>
                <li>
                    <a href="{{route('superadmin.location.district.index')}}">
                    <i class="fa fa-bookmark-o"></i> District</a>
                </li>
                <li>
                    <a href="{{route('superadmin.location.upazila.index')}}">
                    <i class="fa fa-bookmark-o"></i> Upazila</a>
                </li>
                <li>
                    <a href="{{route('superadmin.location.union.index')}}">
                    <i class="fa fa-bookmark-o"></i> Unions</a>
                </li>
            </ul>
        </li>

    </ul>
</li>