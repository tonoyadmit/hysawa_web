<li><a href="{{ route('district-admin.dashboard') }}"> Home </a></li>
<li><a href="{{ route('district-admin.periods.index') }}"> Monthly Report </a></li>
<li class="classic-menu-dropdown">
    <a href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true"> Sub-projects
        <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu pull-left">
        <li>
            <a href="{{route('district-admin.water.index')}}">
            <i class="fa fa-bookmark-o"></i> Water Supply </a>
        </li>
        <li>
            <a href="{{route('district-admin.water_sanitation.index')}}">
            <i class="fa fa-bookmark-o"></i> Sanitation </a>
        </li>
        
        <li>
            <a href="{{route('district-admin.water_household.index')}}">
            <i class="fa fa-bookmark-o"></i> Household Latrine </a>
        </li>

    </ul>
</li>
<!-- <li><a href="{{ route('district-admin.finance.report.index') }}"> Finance Report </a></li>-->
<li><a href="https://www.hysawa.org/mis/public/district/finance/report"> Finance Report </a></li>
<li><a href="{{ route('district-admin.download.index') }}"> Downloads </a></li>

<li><a href="{{route('district-admin.mobile-app.index')}}"> Mobile App </a></li>

<li><a href="{{route('district-admin.fundlist.up-demand.fund_list.index')}}"> Fund Request List </a></li>
  <li class="nav-item"><a class="nav-link " href="{{route('district-admin.up-demand-m.survey_list.index')}}">HYSAWA TUBEWELL INFO
</a></li>