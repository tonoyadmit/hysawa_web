<li><a href="{{ route('up-admin.dashboard') }}"> Home </a></li>
<li><a href="{{ route('up-admin.report_period.index') }}"> Report </a></li>
<li class="classic-menu-dropdown">
    <a href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true"> Sub-projects
        <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu pull-left">
        <li class="dropdown-submenu">
            <a href="javascript:;">
                <i class="fa fa-envelope-o"></i> Water Supply
            </a>
            <ul class="dropdown-menu">
                <li><a href="{{route('up-admin.water.create')}}"> Add Entry </a></li>
                <li><a href="{{route('up-admin.water.index')}}"> Site List </a></li>
            </ul>
        </li>
        <li class="dropdown-submenu">
            <a href="javascript:;">
                <i class="fa fa-envelope-o"></i> Sanitation
            </a>
            <ul class="dropdown-menu">
                <li><a href="{{route('up-admin.water_sanitation.create')}}"> Add Entry </a></li>
                <li><a href="{{route('up-admin.water_sanitation.index')}}"> Site List </a></li>
            </ul>
        </li>

        <li class="dropdown-submenu">
            <a href="javascript:;">
                <i class="fa fa-envelope-o"></i> Household Latrine
            </a>
            <ul class="dropdown-menu">
                <li><a href="{{route('up-admin.water_household.create')}}"> Add Entry </a></li>
                <li><a href="{{route('up-admin.water_household.index')}}"> Site List </a></li>
            </ul>
        </li>
        
    </ul>
</li>

<li class="classic-menu-dropdown">
    <a href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true"> Finance
        <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu pull-left">
        <li>
            <!--<a href="{{route('up-admin.finace_income.index')}}"> Income Entry/Edit </a>-->
               <a href="https://www.hysawa.org/mis/public/up/finace_income"> Income Entry/Edit </a>
        </li>
        <li>
            <!--<a href="{{route('up-admin.finace_expenditure.index')}}"> Expenditure Entry/Edit </a>-->
            <a href="https://www.hysawa.org/mis/public/up/finace_expenditure"> Expenditure Entry/Edit </a>
        </li>
        <li>
            <a href="{{route('up-admin.fund_list.index')}}"> Demand Entry/Edit </a>
        </li>
        <li>
            <!--<a href="{{route('up-admin.finace_bankstatement.index')}}"> Bank statements Entry/Edit </a>-->
              <a href="https://www.hysawa.org/mis/public/up/finace_bankstatement"> Bank statements Entry/Edit </a>
        </li>

        <li>
           <!--   <a href="{{route('up-admin.finance.report.index')}}"> Report </a>  -->
            <a href="https://www.hysawa.org/mis/public/up/finance/report"> Report </a>
        </li>

        <li>
            <!--<a href="{{route('up-admin.balancedisbursement')}}"> Disbursement from HYSAWA </a>-->
            <a href="https://www.hysawa.org/mis/public/up/balancedisbursement"> Disbursement from HYSAWA </a>
        </li>
    </ul>
</li>

<li class="classic-menu-dropdown">
    <a href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true"> Training
        <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu pull-left">
    <li><a href="{{ route('up-admin.participants.index') }}"> Participant List </a></li>
    </ul>
</li>

<li class="classic-menu-dropdown">
    <a href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true"> Procurement
        <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu pull-left">
        <li>
            <a href="{{route('up-admin.tenderEvaluationComittee.index')}}">
            <i class="fa fa-bookmark-o"></i> Tender Evaluation Comittee (TEC) </a>
        </li>
        <li>
            <a href="{{route('up-admin.proposalEvaluationComittee.index')}}">
            <i class="fa fa-bookmark-o"></i> Proposal Evaluation Comittee(PEC) </a>
        </li>
        <li>
            <a href="{{route('up-admin.procurementEntry.index')}}">
            <i class="fa fa-bookmark-o"></i>List of Procurements</a>
        </li>
        <li>
            <a href="{{route('up-admin.ProcurementEvaluationEntry.index')}}">
            <i class="fa fa-bookmark-o"></i> Procurement Evaluations </a>
        </li>
    </ul>
</li>

<li class="classic-menu-dropdown">
    <a href="javascript:;" data-hover="megamenu-dropdown" data-close-others="true"> UP
        <i class="fa fa-angle-down"></i>
    </a>
    <ul class="dropdown-menu pull-left">
        <li>
            <a href="{{route('up-admin.up.basic-info.index')}}">
            <i class="fa fa-bookmark-o"></i>Basic Information</a>
        </li>
        <li>
            <a href="{{route('up-admin.up.functionaries.index')}}">
            <i class="fa fa-bookmark-o"></i> UP functionaries </a>
        </li>
        <li>
            <a href="{{route('up-admin.up.staff.index')}}">
            <i class="fa fa-bookmark-o"></i> UP/PNGO staff/CF </a>
        </li>
    </ul>
</li>

<li><a href="{{ route('up-admin.download.index') }}"> Downloads </a></li>

<li><a href="{{route('up-admin.mobile-app.index')}}"> Mobile App </a></li>
<li><a href="{{route('up-admin.cdfs.index')}}"> CDF </a></li>
<li><a href="{{route('up-admin.user-management.password-change')}}"> Update Password </a></li>


