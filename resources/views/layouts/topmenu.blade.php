<div class="top-menu" style="background-color: #404040;">
    <ul class="nav navbar-nav pull-right">

    @if($userRole == 'superadministrator')
        <li><a style="color:#FFF">Superadmin</a></li>
    @elseif($userRole == 'district_admin')
        <?php
            $adminUser = \Auth::user();
            $adminUser->load('region');
        ?>
        <li><a style="color:#FFF">Region: {{$adminUser->region->region_name or ''}}</a></li>
    @elseif($userRole == 'upazila_admin')
        <?php
            $adminUser = \Auth::user();
            $adminUser->load('region');
            $adminUser->load('district');
            $adminUser->load('upazila');
        ?>
        <li><a style="color:#FFF">Region: {{$adminUser->region->region_name or ''}}</a></li>
        <li><a style="color:#FFF">District: {{$adminUser->district->distname or ''}}</a></li>
        <li><a style="color:#FFF">Upazila: {{$adminUser->upazila->upname or ''}}</a></li>
    @elseif($userRole == 'up_admin')
        <?php
            $adminUser = \Auth::user();
            $adminUser->load('region');
            $adminUser->load('district');
            $adminUser->load('upazila');
            $adminUser->load('union');
        ?>

       <li><a style="padding: 10px 10px;">{{$adminUser->region->region_name or ''}}</a></li>
<li><a style="padding: 10px 10px;">District: {{$adminUser->district->distname or ''}}</a></li>
<li><a style="padding: 10px 10px;">Upazila: {{$adminUser->upazila->upname or ''}}</a></li>
<li><a style="padding: 10px 10px;">Union: {{$adminUser->union->unname or ''}}</a></li>
        {{-- <li class="dropdown dropdown-user">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                 <span class="username username-hide-on-mobile">
                 Basic Info{{ \Auth::user()->name }}
                 </span>
                <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-default">
                <li><a >Region:{{$adminUser->region->region_name or ''}}</a></li>
                <li><a >District: {{$adminUser->district->distname or ''}}</a></li>
                <li><a >Upazila: {{$adminUser->upazila->upname or ''}}</a></li>
                <li><a >Union Name: {{$adminUser->union->unname or ''}}</a></li>
            </ul>
        </li> --}}


    @elseif($userRole == 'training_agency')
        <?php
            $adminUser = \Auth::user();
            $adminUser->load('region');
            $adminUser->load('project');
        ?>
        <li><a style="color:#FFF">Project: {{$adminUser->project->project or ''}}</a></li>
        <li><a style="color:#FFF">Region: {{$adminUser->region->region_name or ''}}</a></li>
    @elseif($userRole == 'water_quality_agency')
        <?php
            $adminUser = \Auth::user();
            $adminUser->load('region');
            $adminUser->load('project');
        ?>
        <li><a style="color:#FFF">Project: {{$adminUser->project->project or ''}}</a></li>
    @endif

        <li class="dropdown dropdown-user">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                 <span class="username username-hide-on-mobile">
                 @if(\Auth::check())
                    {{\Auth::user()->name}}
                 @endif
                 </span>
                <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-default">
                <li>
                    <a href="{{ route('logout') }}">
                    <i class="icon-key"></i> Sign Out </a>
                </li>
            </ul>
        </li>


    </ul>
</div>
<style>
    li > a:hover {

    text-decoration: none;
    background-color: #337ab7 !important;
    color: #FFF;

}
</style>