<div class="top-menu">
    <ul class="nav navbar-nav pull-right">

    @if($userRole == 'superadministrator')
        <li><a style="color:#FFF">Superadmin</a></li>
    @elseif($userRole == 'district_admin')
        <?php
            $adminUser = \Auth::user();
            $adminUser->load('region');
        ?>
         <li><a style="color:#FFF">User Role: {{ $adminUser->roles->first()->display_name }}</a></li>
        <li><a style="color:#FFF">Region: {{$adminUser->region->region_name}}</a></li>
    @elseif($userRole == 'upazila_admin')
        <?php
            $adminUser = \Auth::user();
            $adminUser->load('region');
            $adminUser->load('district');
            $adminUser->load('upazila');
        ?>
        <li><a style="color:#FFF">User Role: {{ $adminUser->roles->first()->display_name }}</a></li>
        <li><a style="color:#FFF">Region: {{$adminUser->region->region_name}}</a></li>
        <li><a style="color:#FFF">District: {{$adminUser->district->distname}}</a></li>
        <li><a style="color:#FFF">Upazila: {{$adminUser->upazila->upname}}</a></li>
    @elseif($userRole == 'up_admin')
        <?php
            $adminUser = \Auth::user();
            $adminUser->load('region');
            $adminUser->load('district');
            $adminUser->load('upazila');
            $adminUser->load('union');
        ?>
        <li><a style="color:#FFF">User Role: {{ $adminUser->roles->first()->display_name }}</a></li>
        <li><a style="color:#FFF">Region: {{$adminUser->region->region_name}}</a></li>
        <li><a style="color:#FFF">District: {{$adminUser->district->distname}}</a></li>
        <li><a style="color:#FFF">Upazila: {{$adminUser->upazila->upname}}</a></li>
        <li><a style="color:#FFF">Union Name: {{$adminUser->union->unname}}</a></li>
    @elseif($userRole == 'training_agency')
        <?php
            $adminUser = \Auth::user();
            $adminUser->load('region');
            $adminUser->load('project');
        ?>
         <li><a style="color:#FFF">User Role: {{ $adminUser->roles->first()->display_name }}</a></li>
        <li><a style="color:#FFF">Project: {{$adminUser->project->project}}</a></li>
        <li><a style="color:#FFF">Region: {{$adminUser->region->region_name}}</a></li>
    @elseif($userRole == 'water_quality_agency')
        <?php
            $adminUser = \Auth::user();
            $adminUser->load('region');
            $adminUser->load('project');
        ?>
        <li><a style="color:#FFF">User Role: {{ $adminUser->roles->first()->display_name }}</a></li>
        <li><a style="color:#FFF">Project: {{$adminUser->project->project}}</a></li>
    @endif

        <li class="dropdown dropdown-user">
            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                 <span class="username username-hide-on-mobile">
                 @if(\Auth::check())
                    {{\Auth::user()->name}}
                 @endif
                 </span>
                <i class="fa fa-angle-down"></i>
            </a>
            <ul class="dropdown-menu dropdown-menu-default">
                <li class="divider"> </li>
                <li>
                    <a href="{{ route('logout') }}">
                    <i class="icon-key"></i> Sign Out </a>
                </li>
            </ul>
        </li>


    </ul>
</div>
<style>
    li > a:hover {

    text-decoration: none;
    background-color:black !important;
    }
    table > thead{
        background-color: #32c5d2;
        color: #FFF;
    }


</style>