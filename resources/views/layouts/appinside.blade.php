<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HYSAWA &#8211; Hygiene Sanitation and Water Supply</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="Hygiene Sanitation and Water Supply" />
        <meta name="Author" content="Suman Chandra Das" />

        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
       <link href="{{ asset('assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/bootstrap-toastr/toastr.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/global/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ asset('assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/layouts/layout/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('assets/layouts/layout/css/themes/darkblue.min.css') }}" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{ asset('assets/layouts/layout/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
		    <link rel="shortcut icon" href="{{ asset('assets/pages/img/favicon2.ico') }}" type="image/x-icon">
		    <link rel="icon" href="{{ asset('assets/pages/img/favicon2.ico') }}" type="image/x-icon">
        <link href="{{asset('custom/css/common.css') }}" rel="stylesheet" type="text/css" />

        @yield('select2CSS')
        <script src="{{ asset('assets/global/plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>

        <!--[if lt IE 9]>
        <script src="{{ asset('assets/global/plugins/respond.min.js') }}"></script>
        <script src="{{ asset('assets/global/plugins/excanvas.min.js') }}"></script>
        <![endif]-->

        <script src="{{ asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/moment.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootbox/bootbox.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-toastr/toastr.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/scripts/datatable.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/pages/scripts/table-datatables-responsive.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-confirmation/bootstrap-confirmation.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/select2/js/select2.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/scripts/app.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/pages/scripts/dashboard.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/layouts/layout/scripts/layout.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/layouts/layout/scripts/demo.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>

        <style type="text/css">
            .page-header.navbar .page-logo{
                height: 50px;
            }
            .page-header.navbar .page-logo .logo-default{
                margin: 0px;
            }
        </style>
    </head>

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white page-full-width">
        <div class="page-wrapper">
            <?php
                $role = "";

                if(\Auth::check())
                {
                  $role = \Auth::user()->roles->first()->name;
                }
                //\Log::info($role);
            ?>

            <div class="page-header navbar navbar-fixed-top">
                <div class="page-header-inner ">
                    <div class="hor-menu  hor-menu-light hidden-sm hidden-xs">
                        <ul class="nav navbar-nav">

                            @if($role == 'superadministrator')
                                @include('layouts.nav.superadministrator')

                            @elseif($role == 'district_admin')
                                @include('layouts.nav.district_admin')

                            @elseif($role == 'upazila_admin')
                                @include('layouts.nav.upazila_admin')

                            @elseif($role == 'up_admin')
                                @include('layouts.nav.up_admin')

                             @elseif($role == 'administrator')
                                @include('layouts.nav.administrator')

                            @elseif($role == 'training_agency')
                                @include('layouts.nav.training_agency')

                            @elseif($role == 'water_quality_agency')
                                @include('layouts.nav.water_quality_agency')

                            @elseif($role == 'default_user')
                                @include('layouts.nav.default_user')
                            @endif

                        </ul>
                    </div>




                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    @include('layouts.topmenu', ['userRole' => $role])
                </div>
            </div>


            <div class="clearfix"> </div>
            <div class="page-container">


                <div class="page-sidebar-wrapper">
                    <!-- BEGIN SIDEBAR -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <div class="page-sidebar navbar-collapse collapse">
                        <div class="page-sidebar-wrapper">
                            <ul class="page-sidebar-menu visible-sm visible-xs  page-header-fixed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">

                                  @if($role == 'superadministrator')
                                      @include('layouts.nav_mob.superadministrator')

                                  @elseif($role == 'district_admin')
                                      @include('layouts.nav_mob.district_admin')

                                  @elseif($role == 'upazila_admin')
                                      @include('layouts.nav_mob.upazila_admin')

                                  @elseif($role == 'up_admin')
                                      @include('layouts.nav_mob.up_admin')

                                   @elseif($role == 'administrator')
                                      @include('layouts.nav_mob.administrator')

                                  @elseif($role == 'training_agency')
                                      @include('layouts.nav_mob.training_agency')

                                  @elseif($role == 'water_quality_agency')
                                      @include('layouts.nav_mob.water_quality_agency')

                                  @elseif($role == 'default_user')
                                      @include('layouts.nav_mob.default_user')
                                  @endif
                            </ul>
                        </div>
                    </div>
                </div>







                <div class="page-content-wrapper">
                    <div class="page-content">

                        @if (\Session::has('message'))
                            <script type="text/javascript">
                                $(document ).ready(function() {
                                    // Toast Alert
                                    Command: toastr['success']("{{ \Session::get('message') }}", "Success")
                                    toastr.options = {
                                      "closeButton": true,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "1000",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    }
                                });
                            </script>
                        @endif

                        @if (\Session::has('success'))
                            <script type="text/javascript">
                                $(document ).ready(function() {
                                    // Toast Alert
                                    Command: toastr['success']("{{ \Session::get('message') }}", "Success")
                                    toastr.options = {
                                      "closeButton": true,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "1000",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    }
                                });
                            </script>
                        @endif

                        @if (\Session::has('error'))
                            <script type="text/javascript">
                                $(document ).ready(function() {
                                    // Toast Alert
                                    Command: toastr['error']("{{ \Session::get('error') }}", 'Error!')
                                    toastr.options = {
                                      "closeButton": true,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "1000",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    }
                                });
                            </script>
                        @endif

                        @if (\Session::has('warning'))
                            <script type="text/javascript">
                                $(document ).ready(function() {
                                    // Toast Alert
                                    Command: toastr['warning']("{{ \Session::get('message') }}")
                                    toastr.options = {
                                      "closeButton": true,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "1000",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    }
                                });
                            </script>
                        @endif

                        @if (\Session::has('info'))
                            <script type="text/javascript">
                                $(document ).ready(function() {
                                    // Toast Alert
                                    Command: toastr['info']("{{ \Session::get('message') }}")
                                    toastr.options = {
                                      "closeButton": true,
                                      "debug": false,
                                      "positionClass": "toast-top-right",
                                      "onclick": null,
                                      "showDuration": "1000",
                                      "hideDuration": "1000",
                                      "timeOut": "5000",
                                      "extendedTimeOut": "1000",
                                      "showEasing": "swing",
                                      "hideEasing": "linear",
                                      "showMethod": "fadeIn",
                                      "hideMethod": "fadeOut"
                                    }
                                });
                            </script>
                        @endif

                        @yield('content')
                    </div>
                </div>

            </div>
            <div class="page-footer">
                <div class="page-footer-inner"> <?php echo date('Y'); ?> &copy; HYSAWA &#8211; Hygiene Sanitation and Water Supply. System By
                    <a target="_blank" href="http://sslwireless.com">SSL Wireless</a>
                </div>
                <div class="scroll-to-top">
                    <i class="icon-arrow-up"></i>
                </div>
            </div>
        </div>

        <script type="text/javascript">
            var site_path = '{{ env("APP_URL") }}';
        </script>
        <script src="{{ asset('custom/js/highlight-nav.js') }}" type="text/javascript"></script>
        <script src="{{ asset('custom/js/location-list.js') }}" type="text/javascript"></script>

        <style type="text/css"         src="https://printjs-4de6.kxcdn.com/print.min.css"></style>
        <script type="text/javascript" src="https://printjs-4de6.kxcdn.com/print.min.js"></script>

        @yield('select2JS')
        @yield('my_js')
    </body>