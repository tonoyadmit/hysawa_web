<li class="nav-item "><a class="nav-link" href="{{ route('superadmin.dashboard') }}"> Home </a></li>

<li class="nav-item">
    <a href="javascript:;" class="nav-link nav-toggle"> Sub-projects
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu">
        <li class="nav-item">
            <a href="{{route('superadmin.sub-projects.waters.index')}}" class="nav-link"> Water Supply </a>
        </li>
        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle"> Sanitation<span class="arrow "></span></a>
            <ul class="sub-menu">

                <li class="nav-item">
                    <a href="{{route('superadmin.sub-projects.sanitation.summary')}}" class="nav-link"> Summary </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('superadmin.sub-projects.sanitation.index')}}" class="nav-link"> List </a>
                </li>
            </ul>
        </li>

    </ul>
</li>


<li class="nav-item">
    <a href="#" class="nav-link nav-toggle"> Monthly Report
        <span class="selected"> </span>
        <span class="arrow open"> </span>
    </a>
    <ul class="sub-menu">
        <li class="nav-item">
            <a href="{{route('superadmin.monthly-report.report.index')}}" class="nav-link ">
                <i class="icon-layers"></i>
                <span class="title"> Report</span>
            </a>
        </li>
        <li class="nav-item">
            <a href="{{route('superadmin.monthly-report.wash')}}" class="nav-link ">
                <i class="icon-layers"></i>
                <span class="title"> UP wise Wash Status</span>
            </a>
        </li>
    </ul>
</li>

<li class="nav-item">
    <a href="#" class="nav-link nav-toggle"> Monitoring
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu">
        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle"> Field Visit
                <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item"><a class="nav-link " href="{{route('superadmin.monitoring.upcapacity.reportlist.index')}}">Report List</a></li>
                <li class="nav-item"><a class="nav-link " href="{{route('superadmin.monitoring.upcapacity.reportlist.create')}}">Report Entry </a></li>
            </ul>
        </li>

        <li class="nav-item">
            <a href="{{route('superadmin.monitoring.upcapacity.index')}}" class="nav-link">UP Capacity</a>
        </li>

    </ul>
</li>

<li class="nav-item">
    <a href="#" class="nav-link nav-toggle"> Finance
        <span class="arrow"> </span>
    </a>
    <ul class="sub-menu">
        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                 Charts of Accounts
                <span class="arrow"> </span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item"><a class="nav-link " href="{{route('superadmin.finance.charts-of-account.heads.index')}}">Head List</a></li>
                <li class="nav-item"><a class="nav-link " href="{{route('superadmin.finance.charts-of-account.sub-heads.index')}}">Sub-head List </a></li>
                <li class="nav-item"><a class="nav-link " href="{{route('superadmin.finance.charts-of-account.items.index')}}">Items </a></li>
            </ul>
        </li>

        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                 Report <span class="arrow"> </span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item"><a class="nav-link " href="{{route('superadmin.finance.report.summary.index')}}"> Summary by Union  </a></li>
                <li class="nav-item"><a class="nav-link " href="{{route('superadmin.finance.report.analysis.index')}}"> Analysis by Project/date/UP  </a></li>
                <li class="nav-item"><a class="nav-link " href="{{route('superadmin.finance.report.union.index')}}"> Union report  </a></li>
                <li class="nav-item"><a class="nav-link " href="{{route('superadmin.finance.report.upazila.index')}}"> Upazila report  </a></li>
                <li class="nav-item"><a class="nav-link " href="{{route('superadmin.finance.report.district.index')}}"> District report  </a></li>
                <li class="nav-item"><a class="nav-link " href="{{route('superadmin.finance.report.project.index')}}"> Project report  </a></li>
                <li class="nav-item"><a class="nav-link " href="{{route('superadmin.finance.report.consolidated.index')}}"> Consolidated report  </a></li>
                {{-- <li class="nav-item"><a class="nav-link " href="{{route('superadmin.finance.report.common.index')}}"> Commin report  </a></li> --}}
            </ul>
        </li>

        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                 UP Budget <span class="arrow"> </span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item"><a class="nav-link " href="{{route('superadmin.finance.up-budget.index')}}">List</a></li>
            </ul>
        </li>

        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                 Disbursement To UP <span class="arrow"> </span>
            </a>
            <ul class="sub-menu">
                <!--<li class="nav-item"><a class="nav-link " href="{{route('superadmin.finance.disbursment-to-up.disbursment.index')}}"> List  </a></li>-->
                <!--<li class="nav-item"><a class="nav-link " href="{{route('superadmin.finance.disbursment-to-up.disbursment.create')}}"> Create  </a></li>-->
                <!--<li class="nav-item"><a class="nav-link " href="{{route('superadmin.finance.disbursment-to-up.fund-disbursment.index')}}"> Entry Track By Date  </a></li>-->
                <!--<li class="nav-item"><a class="nav-link " href="{{route('superadmin.finance.disbursment-to-up.fund-disbursment.index2')}}"> Entry Track By UP  </a></li>-->
                
                <li><a href="http://www.hysawa.org/mis/public/superadmin/finance/disbursment-to-up/disbursment"> List  </a></li>
                <li><a href="http://www.hysawa.org/mis/public/superadmin/finance/disbursment-to-up/disbursment/create"> Create  </a></li>
                <li><a href="http://www.hysawa.org/mis/public/superadmin/finance/disbursment-to-up/fund-disbursment/entry-track-by-date"> Entry Track By Date  </a></li>
                <li><a href="http://www.hysawa.org/mis/public/superadmin/finance/disbursment-to-up/fund-disbursment/entry-track-by-up"> Entry Track By UP  </a></li>
            </ul>
        </li>
    </ul>
</li>

<li class="nav-item "><a href="{{ route('superadmin.pngo.index') }}"> PNGO List </a></li>

<li class="nav-item">
    <a href="javascript:;" class="nav-link nav-toggle"> Training <span class="arrow"> </span></a>
    <ul class="sub-menu">
        <li class="nav-item"><a class="nav-link " href="{{route('superadmin.training.training.index')}}">Training list </a></li>
        <li class="nav-item"><a class="nav-link " href="{{route('superadmin.training.training-participant.index')}}">Participant list </a></li>
        <li class="nav-item"><a class="nav-link " href="{{route('superadmin.training.summary')}}">Summary </a></li>
    </ul>
</li>

<li class="nav-item">
    <a href="javascript:;" class="nav-link nav-toggle"> Procurement <span class="arrow"> </span></a>
    <ul class="sub-menu">
        <li class="nav-item"><a class="nav-link " href="{{route('superadmin.procurement.contractors.index')}}">List of Contractors </a></li>
        <li class="nav-item"><a class="nav-link " href="{{route('superadmin.procurement.pec.index')}}">PEC </a></li>
        <li class="nav-item"><a class="nav-link " href="{{route('superadmin.procurement.tec.index')}}">TEC </a></li>
        <li class="nav-item"><a class="nav-link " href="{{route('superadmin.procurement.list.index')}}">Procurement List </a></li>
        <li class="nav-item"><a class="nav-link " href="{{route('superadmin.procurement.evaluations.index')}}">Evaluation </a></li>
    </ul>
</li>

<li class="nav-item">
    <a href="javascript:;" class="nav-link nav-toggle"> UP <span class="arrow"> </span></a>
    <ul class="sub-menu">
        <li class="nav-item"><a class="nav-link " href="{{route('superadmin.up.mou.index')}}">List of UPs signed MOU </a></li>
        <li class="nav-item"><a class="nav-link " href="{{route('superadmin.up.functionaries.index')}}">UP functionaries </a></li>
        <li class="nav-item"><a class="nav-link " href="{{route('superadmin.up.stuff.index')}}">UP/PNGO Project staff </a></li>
    </ul>
</li>

<li class="nav-item">
    <a href="javascript:;" class="nav-link nav-toggle"> Other <span class="arrow"> </span></a>
    <ul class="sub-menu">
        <li class="nav-item"><a class="nav-link" href="{{route('superadmin.download.index')}}"> Downloads</a></li>
        <li class="nav-item"><a class="nav-link" href="{{route('superadmin.water.map.show')}}"> Map</a></li>
        <li class="nav-item"><a class="nav-link" href="{{route('superadmin.water.map.show2')}}"> Map 2</a></li>
        <li class="nav-item"><a class="nav-link" href="{{route('superadmin.water.map.mobile_map')}}">Mobile Map 1</a></li>
        <li class="nav-item"><a class="nav-link" href="{{route('superadmin.water.map.mobile_map_2')}}">Mobile Map 2</a></li>

        <li class="nav-item"><a class="nav-link " href="{{route('superadmin.cdfs.index')}}" > CDF</a></li>
        <li class="nav-item"><a class="nav-link " href="{{route('superadmin.mobile-app.index')}}" > Mobile App</a></li>

        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                User Management <span class="arrow"> </span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item"><a class="nav-link " href="{{route('superadmin.user-management.index')}}">List</a></li>
                <li class="nav-item"><a class="nav-link " href="{{route('superadmin.user-management.create')}}">Add New </a></li>
            </ul>
        </li>
    </ul>
</li>