<li class="nav-item start"><a class="nav-link" href="{{ route('upazila-admin.dashboard') }}"> Home </a></li>
<li class="nav-item"><a class="nav-link" href="{{ route('upazila-admin.periods.index') }}"> Monthly Report </a></li>
<li  class="nav-item">
    <a href="javascript:;" class="nav-link nav-toggle"> Sub-projects <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        <li>
            <a class="nav-link" href="{{route('upazila-admin.water.index')}}">
             Water Supply </a>
        </li>
        <li>
            <a class="nav-link" href="{{route('upazila-admin.sanitation.index')}}">
             Sanitation </a>
        </li>

    </ul>
</li>
<li class="nav-item"><a class="nav-link" href="{{ route('upazila-admin.finance.report.index') }}"> Finance Report </a></li>
<li class="nav-item"><a class="nav-link" href="{{ route('upazila-admin.download.index') }}"> Downloads </a></li>

<li class="nav-item"><a class="nav-link" href="{{ route('upazila-admin.mobile-app.index') }}"> Mobile App </a></li>

