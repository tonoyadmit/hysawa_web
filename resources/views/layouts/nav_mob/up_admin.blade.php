<li class="nav-item start"><a class="nav-link" href="{{ route('up-admin.dashboard') }}"> Home </a></li>
<li class="nav-item"><a class="nav-link" href="{{ route('up-admin.report_period.index') }}"> Report </a></li>

<li class="nav-item">
    <a href="javascript:;" class="nav-link nav-toggle"> Sub-projects <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                 Water Supply <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item"><a href="{{route('up-admin.water.create')}}"> Add Entry </a></li>
                <li class="nav-item"><a href="{{route('up-admin.water.index')}}"> Site List </a></li>
            </ul>
        </li>
        <li class="nav-item">
            <a href="javascript:;" class="nav-link nav-toggle">
                 Sanitation <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item"><a href="{{route('up-admin.water_sanitation.create')}}"> Add Entry </a></li>
                <li class="nav-item"><a href="{{route('up-admin.water_sanitation.index')}}"> Site List </a></li>
            </ul>
        </li>

    </ul>
</li>

<li class="nav-item">
    <a href="javascript:;" class="nav-link nav-toggle"> Finance <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        <li class="nav-item">
            <!--<a class="nav-link" href="{{route('up-admin.finace_income.index')}}"> Income Entry/Edit </a>-->
            <a class="nav-link" href="https://www.hysawa.org/mis/public/up/finace_income"> Income Entry/Edit </a>
        </li>
        <li class="nav-item">
            <!--<a class="nav-link" href="{{route('up-admin.finace_expenditure.index')}}"> Expenditure Entry/Edit </a>-->
             <a class="nav-link" href="https://www.hysawa.org/mis/public/up/finace_expenditure"> Expenditure Entry/Edit </a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="{{route('up-admin.finace_demand.index')}}"> Demand Entry/Edit </a>
        </li>
        <li class="nav-item">
            <!--<a class="nav-link" href="{{route('up-admin.finace_bankstatement.index')}}"> Bank statements Entry/Edit </a>-->
              <a class="nav-link" href="https://www.hysawa.org/mis/public/up/finace_bankstatement"> Bank statements Entry/Edit </a>
        </li>

        <li class="nav-item">
            <!--<a class="nav-link" href="{{route('up-admin.finance.report.index')}}"> Report </a>-->
             <a class="nav-link" href="https://www.hysawa.org/mis/public/up/finance/report"> Report </a>
        </li>

        <li class="nav-item">
            <!--<a class="nav-link" href="{{route('up-admin.balancedisbursement')}}"> Disbursement from HYSAWA </a>-->
              <a class="nav-link" href="https://www.hysawa.org/mis/public/up/balancedisbursement"> Disbursement from HYSAWA </a>
        </li>
    </ul>
</li>

<li class="nav-item">
    <a href="javascript:;" class="nav-link nav-toggle"> Training <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        <li class="nav-item"><a class="nav-link" href="{{ route('up-admin.participants.index') }}"> Participant List </a></li>
    </ul>
</li>

<li class="nav-item">
    <a href="javascript:;" class="nav-link nav-toggle"> Procurement <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        <li class="nav-item"><a class="nav-link" href="{{route('up-admin.tenderEvaluationComittee.index')}}">Tender Evaluation Comittee (TEC) </a></li>
        <li class="nav-item"><a class="nav-link" href="{{route('up-admin.proposalEvaluationComittee.index')}}">Proposal Evaluation Comittee(PEC) </a></li>
        <li class="nav-item"><a class="nav-link" href="{{route('up-admin.procurementEntry.index')}}">List of Procurements</a></li>
        <li class="nav-item"><a class="nav-link" href="{{route('up-admin.ProcurementEvaluationEntry.index')}}">Procurement Evaluations </a></li>
    </ul>
</li>

<li class="nav-item">
    <a href="javascript:;" class="nav-link nav-toggle"> UP <span class="arrow "></span>
    </a>
    <ul class="sub-menu">
        <li class="nav-item"><a class="nav-link" href="{{route('up-admin.up.basic-info.index')}}">Basic Information</a></li>
        <li class="nav-item"><a class="nav-link" href="{{route('up-admin.up.functionaries.index')}}">UP functionaries </a></li>
        <li class="nav-item"><a class="nav-link" href="{{route('up-admin.up.staff.index')}}">UP/PNGO staff/CF </a></li>
    </ul>
</li>

<li class="nav-item"><a class="nav-link" href="{{ route('up-admin.download.index') }}"> Downloads </a></li>

<li class="nav-item"><a class="nav-link" href="{{route('up-admin.mobile-app.index') }}"> Mobile App </a></li>
<li class="nav-item"><a class="nav-link" href="{{route('up-admin.cdfs.index')}}"> CDF </a></li>