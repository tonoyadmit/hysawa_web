<li class="nav-item start active open"><a href="{{ route('district-admin.dashboard') }}"> Home </a></li>
<li class="nav-item "><a href="{{ route('district-admin.periods.index') }}"> Monthly Report </a></li>
<li class="nav-item">
    <a href="#" class="nav-link nav-toggle"> Sub-projects
        <span class="selected"> </span>
        <span class="arrow open"> </span>
    </a>
    <ul class="sub-menu">
        <li class="nav-item  ">
            <a href="{{route('district-admin.water.index')}}" class="nav-link ">
                <i class="icon-layers"></i>
                <span class="title">Water Supply</span>
            </a>
        </li>
        <li class="nav-item  ">
            <a href="{{route('district-admin.water_sanitation.index')}}" class="nav-link ">
                <i class="icon-layers"></i>
                <span class="title">Sanitation</span>
            </a>
        </li>
    </ul>
</li>
<!--<li class="nav-item "><a href="{{ route('district-admin.finance.report.index') }}"> Finance Report </a></li>-->
<li class="nav-item "><a href="https://www.hysawa.org/mis/public/district/finance/report"> Finance Report </a></li>
<li class="nav-item "><a href="{{ route('district-admin.download.index') }}"> Downloads </a></li>

<li class="nav-item"><a class="nav-link" href="{{ route('district-admin.mobile-app.index') }}"> Mobile App </a></li>