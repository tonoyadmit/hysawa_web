@extends('layouts.app')

<!-- Main Content -->
@section('content')
    
        
        <div class="content">
            <h1>Forgot Password ?</h1>
            
            <!-- BEGIN FORGOT PASSWORD FORM -->
            <form class="login-form" action="{{ url('/password/email') }}" method="post">
                 {{ csrf_field() }}
                {{--<h3>Forgot Password ?</h3>--}}
                <p> Enter your e-mail address below to reset your password. </p>
                <div class="form-group">
                    <input class="form-control placeholder-no-fix" type="email" autocomplete="off" placeholder="Email" name="email" value="{{ old('email') }}" /> </div>
                    @if ($errors->has('email'))
                        <span class="help-block">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                    @endif
                <div class="form-actions">
                    <a href="{{ URL::to('/login') }}" id="back-btn" class="btn blue btn-outline">Back</a>
                    <button type="submit" class="btn blue uppercase pull-right">Submit</button>
                </div>
            </form>
            <!-- END FORGOT PASSWORD FORM -->
              <div class="login-footer">
            <div class="row bs-reset">
               
                <div class="col-xs-12 bs-reset">
                    <div class="login-copyright text-right">
                        <p>Copyright &copy; SSL Wireless <?php echo date('Y'); ?></p>
                    </div>
                </div>
            </div>
        </div>
        </div>
      
   
@endsection
