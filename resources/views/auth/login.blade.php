<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <head>
        <meta charset="utf-8" />
        <title>HYSAWA &#8211; Hygiene Sanitation and Water Supply</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />

        <meta content="" name="description" />
        <meta content="" name="author" />

        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/global/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ URL::asset('assets/global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('assets/pages/css/login-4.min.css')}}" rel="stylesheet" type="text/css" />
        <link href="{{ URL::asset('custom/css/theming_custom.css')}}" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="favicon.ico" />
        <style type="text/css">

            .login .content {
                margin-top: 30px;
                background-color: rgba(255,255,255,0.2);
            }

            .login .content h3, .login .content h4 {
                    color: #051f0c;
            }
            .login .content label, .login .content p {
                color: #0d1b0f;
            }
        </style>
    </head>

    <body class=" login">




        <div class="content">
            <div class="logo">
                <a href="{{ url('/login') }}">
                    <img src="{{ URL::asset('assets/pages/img/logo.png') }}" alt="HYSAWA &#8211; Hygiene Sanitation and Water Supply." />
                </a>
            </div>

            <form class="login-form" action="{{ url('/login') }}" method="post">
                {{ csrf_field() }}

                <h3 class="form-title">Login to your account</h3>

                <div class="alert alert-danger display-hide">
                    <button class="close" data-close="alert"></button>
                    <span> Enter any username and password. </span>
                </div>
                @if ($errors->has('email'))
                    <div class="alert alert-danger">
                        <button class="close" data-close="alert"></button>
                        <span>{{ $errors->first('email') }}</span>
                    </div>
                @endif

                @if ($errors->has('password'))
                    <div class="alert alert-danger">
                        <button class="close" data-close="alert"></button>
                        <span>{{ $errors->first('password') }}</span>
                    </div>
                @endif

                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Username</label>
                    <div class="input-icon">
                        <i class="fa fa-user"></i>
                        <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Username" name="email"  value="{{old('email')}}"/>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label visible-ie8 visible-ie9">Password</label>
                    <div class="input-icon">
                        <i class="fa fa-lock"></i>
                        <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"  value="{{old('password')}}"/>
                    </div>
                </div>

                <div class="form-actions">
                    <label class="rememberme mt-checkbox mt-checkbox-outline">
                        <input type="checkbox" name="remember" value="1" /> Remember me
                        <span></span>
                    </label>
                    <button type="submit" class="btn green pull-right"> Login </button>
                </div>

            </form>
        </div>

        <div class="copyright"> &copy; <?php echo date('Y'); ?> HYSAWA &#8211; Hygiene Sanitation and Water Supply. </div>

        <!--[if lt IE 9]>
        <script src="{{ URL::asset('assets/global/plugins/respond.min.js') }}"></script>
        <script src="{{ URL::asset('assets/global/plugins/excanvas.min.js') }}"></script>
        <![endif]-->
        
        <!--
        <script src="{{ URL::asset('assets/global/plugins/jquery.min.js') }}"  type="text/javascript"></script> -->
        <script src="{{ URL::asset('assets/global/plugins/bootstrap/js/bootstrap.min.js') }}"  type="text/javascript"></script>
        <script src="{{ URL::asset('assets/global/plugins/js.cookie.min.js') }}"  type="text/javascript"></script>
        <script src="{{ URL::asset('assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}"  type="text/javascript"></script>
        <script src="{{ URL::asset('assets/global/plugins/jquery.blockui.min.js') }}"  type="text/javascript"></script>
        <script src="{{ URL::asset('assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}"  type="text/javascript"></script>
        <script src="{{ URL::asset('assets/global/plugins/jquery-validation/js/jquery.validate.min.js') }}"  type="text/javascript"></script>
        <script src="{{ URL::asset('assets/global/plugins/jquery-validation/js/additional-methods.min.js') }}"  type="text/javascript"></script>
        <script src="{{ URL::asset('assets/global/plugins/select2/js/select2.full.min.js') }}"  type="text/javascript"></script>
        <script src="{{ URL::asset('assets/global/plugins/backstretch/jquery.backstretch.min.js') }}"  type="text/javascript"></script>
        <script src="{{ URL::asset('assets/global/scripts/app.min.js') }}"  type="text/javascript"></script> 

        <script type="text/javascript">
            var Login = function () {

                var handleLogin = function() {
                    $('.login-form').validate({
                            errorElement: 'span', //default input error message container
                            errorClass: 'help-block', // default input error message class
                            focusInvalid: false, // do not focus the last invalid input
                            rules: {
                                username: {
                                    required: true
                                },
                                password: {
                                    required: true
                                },
                                remember: {
                                    required: false
                                }
                            },

                            messages: {
                                username: {
                                    required: "Username is required."
                                },
                                password: {
                                    required: "Password is required."
                                }
                            },

                            invalidHandler: function (event, validator) { //display error alert on form submit
                                $('.alert-danger', $('.login-form')).show();
                            },

                            highlight: function (element) { // hightlight error inputs
                                $(element)
                                    .closest('.form-group').addClass('has-error'); // set error class to the control group
                            },

                            success: function (label) {
                                label.closest('.form-group').removeClass('has-error');
                                label.remove();
                            },

                            errorPlacement: function (error, element) {
                                error.insertAfter(element.closest('.input-icon'));
                            },

                            submitHandler: function (form) {
                                form.submit();
                            }
                        });

                    $('.login-form input').keypress(function (e) {
                        if (e.which == 13) {
                            if ($('.login-form').validate().form()) {
                                $('.login-form').submit();
                            }
                            return false;
                        }
                    });
                }
                return {
                    init: function () {
                        handleLogin();
                        $.backstretch([
                            "{{ asset('images/bg1.jpg')}}",
                            "{{ asset('images/bg2.jpg')}}",
                            "{{ asset('images/bg3.jpg')}}",
                            "{{ asset('images/bg4.jpg')}}",
                            ], {
                              fade: 1000,
                              duration: 8000
                            }
                        );
                    }
                };

            }();

            jQuery(document).ready(function() {
                Login.init();
            });
        </script>
    </body>
</html>