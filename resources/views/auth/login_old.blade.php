@extends('layouts.app')
@section('content')
<div class="content">
    <div class="logo">
        <a href="#">
            <img src="{{ URL::asset('assets/pages/img/logo.png') }}" alt="" />
        </a>
    </div>
    <h3 class="form-title font-green">Sign In</h3>


    <form action="{{ url('/login') }}" class="login-form" method="post">
        {{ csrf_field() }}

        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span>Enter any username and password. </span>
        </div>

        @if ($errors->has('email'))
            <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                <span>{{ $errors->first('email') }}</span>
            </div>
        @endif

        @if ($errors->has('password'))
            <div class="alert alert-danger">
                <button class="close" data-close="alert"></button>
                <span>{{ $errors->first('password') }}</span>
            </div>
        @endif

        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Username</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email" value="{{ old('email') }}" required />
        </div>

        <div class="form-group">
                <label class="control-label visible-ie8 visible-ie9">Password</label>
                <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" value="{{ old('password') }}" required />
        </div>

        <div class="form-actions">
            <button type="submit" class="btn green uppercase">Login</button>
            <label class="rememberme check mt-checkbox mt-checkbox-outline">
                <input type="checkbox" name="remember" value="1" />Remember
                <span></span>
            </label>
            <a href="{{ url('/password/reset') }}" id="forget-password" class="forget-password">Forgot Password?</a>
        </div>

    </form>

    <div class="login-footer">
        <div class="row bs-reset">
            <div class="col-xs-12 bs-reset">
                <div class="login-copyright text-right">
                    <p style="text-align: justify">©<?php echo date('Y'); ?> HYSAWA &#8211; Hygiene Sanitation and Water Supply.</p>
                </div>
            </div>
        </div>
    </div>

</div>
@endsection