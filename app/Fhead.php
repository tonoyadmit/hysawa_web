<?php

namespace App;

use App\Fsubhead;
use Illuminate\Database\Eloquent\Model;

class Fhead extends Model
{
    protected $table = 'fhead';
    protected $guarded = [
      'id','created_by', 'updated_by'
    ];

    public function subhead()
    {
        return $this->hasMany(Fsubhead::class,'headid');
    }
}
