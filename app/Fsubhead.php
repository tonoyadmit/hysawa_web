<?php

namespace App;

use App\Fitemlist;
use Illuminate\Database\Eloquent\Model;

class Fsubhead extends Model
{
  protected $table = 'fsubhead';
  protected $guarded = [
  'id','created_by', 'updated_by'
  ];

  public function subItem()
  {
    return $this->hasMany(Fitemlist::class,'subid');
  }

}
