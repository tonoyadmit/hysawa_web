<?php

namespace App\API\Controllers;


use App\User;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class AuthController
{

    public function login(Request $request)
    {
        try{
            //\Log::info("Login API");
            \Log::info($request->all());

            $rules = ['email' => ['required', 'exists:users,email'], 'password' => ['required']];
            $payload = $request->only('email', 'password');

            $validator = \Validator::make($payload, $rules);

            if ($validator->fails()) {
                $content = [
                    'data' => [],
                    'error' =>  $validator->errors(),
                    'message' => 'Could not sign in.',
                    'status' => 'failure',
                    'status_code' => 422
                ] ;

                return response()->json($content, 200);
            }

            $user = User::where('email', $request->input('email'))->get()->first();

            if(!\Hash::check($request->input('password'), $user->password) )
            {
                $content = [
                    'data' => [],
                    'error' => 'invalid_credentials',
                    'message' => 'Invalid Credentials',
                    'status' => 'failure',
                    'status_code' => 401
                ] ;
                return response()->json($content, 200);
            }

            try {
                $credentials = $request->only('email', 'password');
                try {
                    if (! $token = \JWTAuth::attempt($credentials)) {
                        return \Response::json(['error' => 'invalid_credentials'], 401);
                    }
                } catch (JWTException $e) {
                    return \Response::json(['error' => 'could_not_create_token'], 500);
                }

            } catch (JWTException $e) {
                $content = [
                    'data' => [],
                    'error' => 'could_not_create_token',
                    'message' => 'Could not create token',
                    'status' => 'failure',
                    'status_code' => 500
                ];
                return response()->json($content, 200);
            }


            $content = [
                'data' => [
                    'token' => $token
                ],
                'error' => [],
                'message' => 'User Successfully Logged In',
                'status' => 'success',
                'status_code' => 200
            ];
            \Log::info("API - Logout Output");
            \Log::info($content);
            return response()->json($content, 200);
        }catch(\Exception $e){
            \Log::critical($e->getMessage());
            $content = ['data' => [],'error' => [],'message' => 'Internal Error!','status' => 'failure','status_code' => 500];
            return response()->json($content, 200);
        }
    }

    public function token(Request $request)
    {
        $token = \JWTAuth::getToken();

        if(!$token){
            $content = [
            'data' => [
            'token' => ''
            ],
            'error' => ['token' => 'Token not found.'],
            'message' => 'Token not provided.',
            'status' => 'success',
            'status_code' => 422
            ];
            return response()->json($content, 200);
        }

        try{
            $token = \JWTAuth::refresh($token);
            $content = [
                'data' => [
                    'token' => $token
                ],
                'error' => [],
                'message' => 'Your token is updated',
                'status' => 'success',
                'status_code' => 200
            ];
            return response()->json($content, 200);
        }catch(TokenInvalidException $e){
            $content = [
                'data' => [
                'token' => $request->input('token')
                ],
                'error' => $e->getMessage(),
                'message' => 'Invalid token.',
                'status' => 'error',
                'status_code' => 500
            ];
            return response()->json($content, 200);
        }catch(TokenExpiredException $e){
            $content = [
                'data' => [
                'token' => $request->input('token')
                ],
                'error' => $e->getMessage(),
                'message' => 'Token has expired and can no longer be refreshed.',
                'status' => 'error',
                'status_code' => 500
            ];
            return response()->json($content, 200);
        }catch(Exception $e){
            $content = [
                'data' => [
                    'token' => $request->input('token')
                ],
                'error' => $e->getMessage(),
                'message' => 'Internal error.',
                'status' => 'error',
                'status_code' => 500
            ];
            return response()->json($content, 200);
        }
    }

    public function logout(Request $request)
    {
        try{
            $token = \JWTAuth::getToken();
            if(!$token){
                $content = [
                    'data' => [],
                    'error' => [
                        'token' => 'Token not found.'
                    ],
                    'message' => 'Token not provided.',
                    'status' => 'success',
                    'status_code' => 422
                ];
                return response()->json($content, 200);
            }
            //$data = \JWTAuth::invalidate($token);
            $data = \JWTAuth::setToken($token)->invalidate();
            $content = [
                'data' => [],
                'error' => [],
                'message' => 'Token Is expired. Request for new Token.',
                'status' => 'success',
                'status_code' => 200
            ];
            return response()->json($content, 200);

        }catch(\Exception $e){
            \Log::critical($e->getMessage());
            $content = ['data' => [],'error' => [],'message' => 'Internal Error!','status' => 'failure','status_code' => 500];
            return response()->json($content, 200);
        }
    }
}