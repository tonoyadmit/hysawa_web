<?php

namespace App\API\Controllers;


use App\Model\MobAppDataList;
use App\Model\MobAppDataListItem;
use App\Model\MobAppQuestionLists;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class ReportController
{

  public function reportList(Request $request)
  {
    \Log::info("REPORT LIST API HIT");
    try{

      $user = \JWTAuth::parseToken()->authenticate();

      $content = [
          'data' => [ 'Event', 'Water', 'Sanitation'],
          'error' => [],
          'message' => 'Report',
          'status' => 'success',
          'status_code' => 200
      ];

      return response()->json($content, 200);
    }catch(\Exception $e){
            \Log::critical($e->getMessage());
            $content = ['data' => [],'error' => [],'message' => 'Internal Error!','status' => 'failure','status_code' => 500];
            return response()->json($content, 200);
        }
  }

  public function specificReport(Request $request)
  {
    \Log::info("SPECIFIC REPORT API HIT");

    try{

      \Log::info($request->all());

      $user = \JWTAuth::parseToken()->authenticate();
      $type = $request->input('type');

      $data = MobAppDataList::select('id', 'created_at')
                ->where('user_id', auth()->user()->id)
                ->where('type', $type)
                ->orderBy('created_at', 'DESC')
                ->get();

      $list = [];

      foreach($data as $item)
      {
        $list[] = ['id' => $item->id, 'date' => $item->created_at->format('Y-m-d H:i:s')];
      }

      $content = [
          'data' => [
            "type" =>  $type,
            "report" => $list,
          ],
          'error' => [],
          'message' => 'Specific Report List.',
          'status' => 'success',
          'status_code' => 200
      ];
      return response()->json($content, 200);
    }catch(\Exception $e){
      \Log::critical($e->getMessage());
      $content = ['data' => [],'error' => [],'message' => 'Internal Error!','status' => 'failure','status_code' => 500];
      \Log::info($content);
      return response()->json($content, 200);
    }

  }

  public function reportItem(Request $request)
  {
    \Log::info("REPORT ITEM API HIT");

    try{

      \Log::info($request->all());

      $user = \JWTAuth::parseToken()->authenticate();

      $type = $request->input('type');
      $id = $request->input('id');

      $datas = MobAppDataList::with('items.question')
                ->where('user_id', auth()->user()->id)
                ->where('type', $type)
                ->where('id', $id)
                ->get();

      $output = [];

      if(count($datas))
      {

        $data = $datas->first();

        $questions = [];

        foreach($data->items as $item)
        {
          $found = false;

          foreach($questions as &$q)
          {
            if($q['id'] == $item->question->id)
            {
              $found = true;
              $q['answer'] = $q['answer'].','.$item->value;
              break;
            }
          }

          if(!$found)
          {
            $questions[] = [
              'id' => $item->question->id,
              'type' => $item->question->type,
              'title' => $item->question->title,
              'input_type' => $item->question->input_type,
              'options' => $item->question->options,
              'answer' => $item->value
            ];
          }

        }

        $output = [
          'id'         => $data->id,
          'project_id' => $data->project_id,
          'latitude'   => $data->latitude,
          'longitude'  => $data->longitude,
          'questions'  => $questions,
          'created_at' => $data->created_at->format('Y-m-d H:i:s'),
          'image1'     => $data->image1 != "" ? asset($data->image1) : "",
          'image2'     => $data->image2 != "" ? asset($data->image2) : "",
          'image3'     => $data->image3 != "" ? asset($data->image3) : "",
          'inserted_at' => $data->inserted_at
        ];


        $content = [
          'data' => [
              "type" => $type,
              "specific_report_details" => $output
            ],
            'error' => [],
            'message' => 'Specific Report List.',
            'status' => 'success',
            'status_code' => 200
        ];

        return response()->json($content, 200);

      }else{

        $content = [
          'data' => [
              "type" => $type,
              "specific_report_details" => []
            ],
            'error' => [],
            'message' => 'Specific Report List.',
            'status' => 'success',
            'status_code' => 200
        ];

        \Log::info("Empty Data Found.");
        \Log::info($content);
        return response()->json($content, 200);
      }

    }catch(\Exception $e){
      \Log::critical($e->getMessage());
      $content = ['data' => [],'error' => [],'message' => 'Internal Error!','status' => 'failure','status_code' => 500];
      \Log::info($content);
      return response()->json($content, 200);
    }
  }
}