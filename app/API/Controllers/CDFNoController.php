<?php

namespace App\API\Controllers;

use App\Model\CDFNo;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class CDFNoController
{

  public function show(Request $request)
  {
    try{

      $user = \JWTAuth::parseToken()->authenticate();
      $lists = [];
      $cdfno = CDFNo::with('district', 'upazila', 'union')->find($request->input('cdf_no'));

      if(!count($cdfno))
      {
        return response()->json([
            'message' => 'CDF No Not Found.',
            'status' => 'false',
            'status_code' => 404
          ], 200);
      }

      $cdfno = $cdfno->first();

      if($cdfno->type == 'school')
      {
        return response()->json([
          'data' => [
            "District: ".!empty($cdfno->district) ? $cdfno->district->distname : "",
            "Upazila: ".!empty($cdfno->upazila) ? $cdfno->upazila->upname : "",
            "Union: ". !empty($cdfno->union) ? $cdfno->union->unname : "",
            "Village: ". !empty($cdfno->village) ? $cdfno->village : "",
            "Word: ". !empty($cdfno->word_no) ? $cdfno->word_no : "",
            "school Name: ". !empty($cdfno->school_title) ? $cdfno->school_title : "",
          ],
          'error' => [],
          'message' => 'CDF No Found.',
          'status' => 'success',
          'status_code' => 200
        ], 200);
      }

      return response()->json([
        'data' => [
          "District: ".!empty($cdfno->district) ? $cdfno->district->distname : "",
          "Upazila: ".!empty($cdfno->upazila) ? $cdfno->upazila->upname : "",
          "Union: ". !empty($cdfno->union) ? $cdfno->union->unname : "",
          "Village: ". !empty($cdfno->village) ? $cdfno->village : "",
          "Word: ". !empty($cdfno->word_no) ? $cdfno->word_no : "",
          "CDF Name: ". !empty($cdfno->owner) ? $cdfno->owner : ""
        ],
        'error' => [],
        'message' => 'CDF No Found.',
        'status' => 'success',
        'status_code' => 200
      ], 200);

    }catch(\Exception $e){
            \Log::critical($e->getMessage());
            $content = ['data' => [],'error' => [],'message' => 'Internal Error!','status' => 'failure','status_code' => 500];
            return response()->json($content, 200);
        }
    }

}