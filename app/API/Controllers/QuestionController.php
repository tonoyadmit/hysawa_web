<?php

namespace App\API\Controllers;

use App\Model\MobAppDataListItem;
use App\Model\MobAppQuestionLists;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class QuestionController
{

  public function index(Request $request, $project_id, $type)
  {
    try{
      $user = \JWTAuth::parseToken()->authenticate();
      $list = [];
      $questions = MobAppQuestionLists::where('type', $type)->orderBy('id')->get();

      foreach($questions as $q)
      {
        $list[] = [
          'id'          => $q->id,
          'question'    => $q->title,
          'group_id'    => $q->group_id,
          'input_type'  => $q->input_type,
          'options'     => $q->options,
          'type'        => $q->type
        ];
      }

      $content = [
          'data' => [
            'project_id' => $project_id,
            'type' => $type,
            'questions' => $list
          ],
          'error' => [],
          'message' => 'Question List.',
          'status' => 'success',
          'status_code' => 200
      ];

      return response()->json($content, 200);
    }catch(\Exception $e){
        \Log::critical($e->getMessage());
        $content = ['data' => [],'error' => [],'message' => 'Internal Error!','status' => 'failure','status_code' => 500];
        return response()->json($content, 200);
    }

  }

  public function update(Request $request)
  {
    \Log::info("REPORT LIST API HIT");

    try{

      \Log::info("Mobile Data Input");
      \Log::info($request->all());

      $user = \JWTAuth::parseToken()->authenticate();

      $content = [
        'user_id'     => $user->id,
        'distid'      => $user->distid,
        'upid'        => $user->upid,
        'unid'        => $user->unid,
        'proj_id'     => $user->proj_id,
        'region_id'   => $user->region_id,
        'latitude'    => $request->input('latitude'),
        'longitude'   => $request->input('longitude'),
        'answers'     => json_encode($request->input('answers')),
        'project_id'  => $request->input('project_id'),
        'created_at'  => date('Y-m-d H:i:s'),
        'type'        => $request->input('type'),
        'group_id'    => $request->input('group_id'),
        'inserted_at' => $request->input('inserted_at')
      ];

      if($request->hasFile('image1'))
      {
        $fileName = time() . '_1.' . $request->image1->getClientOriginalExtension();
        $img = \Image::make($request->image1)->save('upload/unimage/'.$fileName);
        $content['image1'] ='upload/unimage/'.$fileName;
      }

      if($request->hasFile('image2'))
      {
        $fileName = time() . '_2.' . $request->image2->getClientOriginalExtension();
        $img = \Image::make($request->image2)->save('upload/unimage/'.$fileName);
        $content['image2'] ='upload/unimage/'.$fileName;
      }

      if($request->hasFile('image3'))
      {
        $fileName = time() . '_3.' . $request->image2->getClientOriginalExtension();
        $img = \Image::make($request->image3)->save('upload/unimage/'.$fileName);
        $content['image3'] ='upload/unimage/'.$fileName;
      }

      $dataInsertId = \DB::table('mob_app_data_list')->insertGetId($content);

      $data = json_decode(json_encode($request->input('answers')), true);
      $data = json_decode($data);

      \Log::info($data);

      foreach($data as $item)
      {
        $KEY = "";
        $VALUE = "";

        foreach($item as $key => $ii)
        {
          if($key == "answer"){
            $VALUE = $ii;
          }elseif($key == "tag"){

          }elseif($key == "id"){
            $KEY = $ii;
          }
        }

        $vs = explode(',', $VALUE);

        if(count($vs)> 1)
        {
          foreach($vs as $v)
          {
            $dd = [
                'mobile_app_data_list_id' => $dataInsertId,
                'question_id'             => $KEY,
                'value'                   => $v,
                'created_at'  => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ];
            MobAppDataListItem::insert($dd);
          }

        }else
        {
          $dd2 = [
              'mobile_app_data_list_id' => $dataInsertId,
              'question_id'             => $KEY,
              'value'                   => $VALUE,
              'created_at'  => date('Y-m-d H:i:s'),
              'updated_at' => date('Y-m-d H:i:s')
          ];
          MobAppDataListItem::insert($dd2);
        }
      }

      if(!$dataInsertId)
      {
         $content = [
          'data' => [],
          'error' => [],
          'message' => 'Internal Server Error. Try Again',
          'status' => 'error',
          'status_code' => 500
        ];
      }
      else{
        $content = [
          'data' => [],
          'error' => [],
          'message' => 'Data Uploaded to Server.',
          'status' => 'success',
          'status_code' => 200
        ];
      }

      \Log::info($content);
      return response()->json($content, 200);

    }catch(\Exception $e){
      \Log::critical($e);
      $content = ['data' => [],'error' => [],'message' => 'Internal Error!','status' => 'failure','status_code' => 500];
      \Log::info($content);
      return response()->json($content, 200);
    }

  }
}