<?php

namespace App\API\Controllers;

use App\Model\TWLatList;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class WaterSanitationController
{
  public function show(Request $request)
  {
    try{

      $user = \JWTAuth::parseToken()->authenticate();
      $lists = [];
      $cdfno = TWLatList::with('district', 'upazila', 'union')->where('code', $request->input('code'))->get();

      if(!count($cdfno))
      {
        return response()->json([
            'data' => [
              'type'      => '',
              'district'  => '',
              'upazila'   => '',
              'union'     => '',
              'village'   => '',
              'area'      => ''
            ],
            'error' => [],
            'message' => 'Data Not Found.',
            'status' => 'false',
            'status_code' => 404
          ], 200);
      }

      $cdfno = $cdfno->first();

      return response()->json([
          'data' => [
            'type'     => !empty($cdfno->type) ? $cdfno->type: "",
            'district' => !empty($cdfno->district) ? $cdfno->district->distname : "",
            'upazila'  => !empty($cdfno->upazila) ? $cdfno->upazila->upname : "",
            'union'    => !empty($cdfno->union) ? $cdfno->union->unname : "",
            'village'  => !empty($cdfno->village) ? $cdfno->village : "",
            'area'     => !empty($cdfno->area) ? $cdfno->area: ""
          ],
          'error' => [],
          'message' => 'Data Found.',
          'status' => 'success',
          'status_code' => 200
        ], 200);
    }catch(\Exception $e){
            \Log::critical($e->getMessage());
            $content = ['data' => [],'error' => [],'message' => 'Internal Error!','status' => 'failure','status_code' => 500];
            return response()->json($content, 200);
        }
        
  }

}