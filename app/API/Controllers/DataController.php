<?php

namespace App\API\Controllers;

use App\Model\CDFNo;
use App\Model\MobAppQuestionLists;
use App\Model\Sanitation;
use App\Model\Water;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class DataController
{

  public function get(Request $request)
  {
    try{

      $user = \JWTAuth::parseToken()->authenticate();
      $user->load('project');
      $projects[] = [
        'id' => $user->project->id,
        'title' => $user->project->project
      ];

      $questions = MobAppQuestionLists::orderBy('type')->get();
      $questionOutput = [];
      foreach($questions as $q)
      {
        $data = [
          'id'          => $q->id,
          'question'    => $q->title,
          'group_id'    => $q->group_id,
          'input_type'  => $q->input_type,
          'type'        => $q->type,
          'options'     => $q->options
        ];
        $found = false;
        foreach($questionOutput as &$qo)
        {
          if( $qo['title'] == $q->type)
          {
            $qo['data'][] = $data;
            $found = true;
            break;
          }
        }

        if(!$found)
        {
          $questionOutput[] = [
            'title' => $q->type,
            'data' => [
              $data
            ]
          ];
        }
      }

      $cdfno = CDFNo::with('district', 'upazila', 'union')->where('union_id', $user->unid)->get();
      $cdfNoList = [];

      foreach($cdfno as $cdf)
      {

        if($cdf->type == "school")
        {
          $cdfNoList[] = [
            'id' => $cdf->cdf_no,
            'type' => !empty($cdf->type) ? $cdf->type : "",
            'value' => [
              "District: ".(!empty($cdf->district) ? $cdf->district->distname : ""),
              "Upazila: ".(!empty($cdf->upazila) ? $cdf->upazila->upname : ""),
              "Union: ". (!empty($cdf->union) ? $cdf->union->unname : ""),
              "Word: ". (!empty($cdf->word_no) ? $cdf->word_no : ""),
              "Village: ". (!empty($cdf->village) ? $cdf->village : ""),
              "school Name: ".( !empty($cdf->school_title) ? $cdf->school_title : "")
            ]
          ];
        }else
        {
          $cdfNoList[] = [
            'id' => $cdf->cdf_no,
            'type' => !empty($cdf->type) ? $cdf->type : "",
            'value' => [
              "District: ".(!empty($cdf->district) ? $cdf->district->distname : ""),
              "Upazila: ".(!empty($cdf->upazila) ? $cdf->upazila->upname : ""),
              "Union: ". (!empty($cdf->union) ? $cdf->union->unname : ""),
              "Village: ". (!empty($cdf->village) ? $cdf->village : ""),
              "Word: ". (!empty($cdf->word_no) ? $cdf->word_no : ""),
              "CDF Name: ". (!empty($cdf->owner) ? $cdf->owner : "")
            ]
          ];
        }


      }

      $waterCDFList = [];

      $waters = Water::
        with('district', 'upazila', 'union')
        ->where('unid', $user->unid)
        ->get();

      foreach($waters as $water)
      {
        $waterCDFList [] = [
          'id'       => $water->id,
          'type' => 'water',
          'value' => [
            'District: '. (!empty($water->district) ? $water->district->distname : ""),
            'Upazila: '. (!empty($water->upazila) ? $water->upazila->upname : ""),
            'Union: '. (!empty($water->union) ? $water->union->unname : ""),
            'Ward No: '. $water->Ward_no,
            'Village: '. $water->Village,
            'TubeWell No: '. $water->id,
            'Cdf No: '. $water->CDF_no, 
            'Landowner: '.$water->Landowner,
            'Caretaker: '.$water->Caretaker_male
          ]

        ];
      }

      $sanitationList = [];

      $sanitations = Sanitation::
        with('district', 'upazila', 'union')
        ->where('unid', $user->unid)
        ->distinct()
        ->groupBy('cdfno')
        ->get();

      foreach($sanitations as $sanitation)
      {
        $sanitationList [] = [
          'id'       => $sanitation->id,
          'type' => 'sanitation',
          'value' => [
            'District: '. (!empty($sanitation->district) ? $sanitation->district->distname : ""),
            'Upazila: '. (!empty($sanitation->upazila) ? $sanitation->upazila->upname : ""),
            'Union: '. (!empty($sanitation->union) ? $sanitation->union->unname : ""),
            'Village: '. $sanitation->village,
            'Latrine No: '. $sanitation->id,
            'Type: '.$sanitation->subtype,
            'Name: '.$sanitation->name
          ]
        ];
      }

      $content = [
        'projects'    => $projects,
        //'questions'   => $questions,
        'questions'   => $questionOutput,
        'cdfs'        => $cdfNoList,
        'waters'      => $waterCDFList,
        'sanitations' => $sanitationList,
        'message'     => 'Cacheable data',
        'status'      => 'success',
        'status_code' => 200
      ];

      \Log::info("GET DATA OUTPUT");
      \Log::info($content);

      return response()->json($content, 200);

    }catch(\Exception $e){
          \Log::critical($e->getMessage());
          $content = ['data' => [],'error' => [],'message' => 'Internal Error!','status' => 'failure','status_code' => 500];
          return response()->json($content, 200);
      }

  }
}