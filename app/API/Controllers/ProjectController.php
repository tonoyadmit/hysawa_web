<?php

namespace App\API\Controllers;

use App\User;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class ProjectController
{

  public function index(Request $request)
  {
    try{

      $user = \JWTAuth::parseToken()->authenticate();

      $projects = [];
      $user->load('project');
      $projects[] = [
        'id' => $user->project->id,
        'title' => $user->project->project
      ];
      $content = [
          'data' => $projects,
          'error' => [],
          'message' => 'Project List.',
          'status' => 'success',
          'status_code' => 200
      ];

      return response()->json($content, 200);
    }catch(\Exception $e){
            \Log::critical($e->getMessage());
            $content = ['data' => [],'error' => [],'message' => 'Internal Error!','status' => 'failure','status_code' => 500];
            return response()->json($content, 200);
        }
  }
}