<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantShop extends Model
{
    protected $table = 'merchant_shop';
    //protected $guarded = ['id', 'created_at', 'updated_at'];
}