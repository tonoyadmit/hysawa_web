<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportPeriod extends Model
{
   protected $table = 'rep_period';
   protected $guarded = [
      'id','created_at', 'updated_at'
   ];
}
