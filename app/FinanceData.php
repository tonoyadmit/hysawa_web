<?php

namespace App;

use App\Model\District;
use App\Model\Region;
use App\Model\Upazila;
use DB;
use Illuminate\Database\Eloquent\Model;

class FinanceData extends Model
{
    protected $table = 'fdata';
    protected $guarded = [
    'id','created_at', 'updated_at'
    ];
    static private $balance=0;
    static private $cbalance=0;

    public function Finance()
    {
        return $this->hasOne('App\Fhead','head','id');
    }

    public static function getBudget($id, $type, $type_value) {
        $data=DB::table('item_budget')
        ->select(DB::raw('SUM(budget) as budget'))
        ->where('itemid',$id)
        ->where(function($q) use ($type, $type_value){
            if(is_array($type_value)){
                $q->whereIn($type, $type_value);
            }else{
                $q->where($type, $type_value);
            }
        })
        ->first();
        self::$balance=(float)$data->budget;
        return $data->budget;
    }


    public static function getBudgetUN($id, $type, $type_value, $proj_id) {
        $data=DB::table('item_budget')
        ->select(DB::raw('SUM(budget) as budget'))
        ->where('itemid',$id)
        ->where('proid', $proj_id)
        ->where(function($q) use ($type, $type_value){
            if(is_array($type_value)){
                $q->whereIn($type, $type_value);
            }else{
                $q->where($type, $type_value);
            }
        })
        ->first();
        self::$balance=(float)$data->budget;
        return $data->budget;
    }

    public static function currentIncome($id,$startTime,$endTime, $type, $type_value) {

        $data=DB::table('fdata')
        ->select(DB::raw('SUM(amount) as cIncome'))
        ->where('item',$id)
        // ->where('unid', auth()->user()->unid)
        ->where($type, $type_value)
        ->where('trans_type','in')
        ->whereBetween('date', array($startTime, $endTime))
        ->first();
        return $data->cIncome;
    }

    public static function currentIncomeUN($id,$startTime,$endTime, $type, $type_value, $proj_id, $region_id) {

        $data=DB::table('fdata')
        ->select(DB::raw('SUM(amount) as cIncome'))
        ->where('item',$id)
        // ->where('unid', auth()->user()->unid)
        ->where($type, $type_value)
        ->where('proid', $proj_id)
        ->where('region_id', $region_id)
        ->where('trans_type','in')
        ->whereBetween('date', array($startTime, $endTime))
        ->first();
        return $data->cIncome;
    }

    public static function currentExpenditure($id,$startTime,$endTime, $type, $type_value) {
    
    
    
        $data=DB::table('fdata')
        ->select(DB::raw('SUM(amount) as cExpenditure'))
        ->where('item',$id)
        // ->where('unid', auth()->user()->unid)
        ->where($type, $type_value)
        ->where('trans_type','ex')
        ->whereBetween('date', array($startTime, $endTime))
        ->first();
       
        return $data->cExpenditure;
    }
    
    public static function currentExpenditureUN($id,$startTime,$endTime, $type, $type_value, $proj_id, $region_id) {
    
             $data=DB::table('fdata')
        ->select(DB::raw('SUM(amount) as cExpenditure'))
        ->where('item',$id)
        // ->where('unid', auth()->user()->unid)
        ->where($type, $type_value)
        ->where('proid', $proj_id)
        ->where('region_id', $region_id)
        ->where('trans_type','ex')
        ->whereBetween('date', array($startTime, $endTime))
        ->first();
        return $data->cExpenditure;
    }

    public static function comulativeIncome($id,$endTime, $type, $type_value) {
        $data=DB::table('fdata')
        ->select(DB::raw('SUM(amount) as cIncome'))
        ->where('item',$id)
        ->where('trans_type','in')
        ->whereDate('date','<=',$endTime)
        // ->where('unid', auth()->user()->unid)
        ->where($type, $type_value)
        ->first();
        return $data->cIncome;
    }

    public static function comulativeIncomeUN($id,$endTime, $type, $type_value, $proj_id, $region_id) {
        $data=DB::table('fdata')
        ->select(DB::raw('SUM(amount) as cIncome'))
        ->where('item',$id)
        ->where('trans_type','in')
        ->whereDate('date','<=',$endTime)
        // ->where('unid', auth()->user()->unid)
        ->where($type, $type_value)
        ->where('proid', $proj_id)
        ->where('region_id', $region_id)
        ->first();
        return $data->cIncome;
    }

    public static function comulativeExpenditure($id,$endTime, $type, $type_value) {
        $data=DB::table('fdata')
        ->select(DB::raw('SUM(amount) as cExpenditure'))
        ->where('item',$id)
        ->whereDate('date','<=',$endTime)
        // ->where('unid', auth()->user()->unid)
        ->where($type, $type_value)
        ->where('trans_type','ex')
        ->first();
        self::$cbalance=(float)$data->cExpenditure;
        return $data->cExpenditure;
    }

    public static function comulativeExpenditureUN($id,$endTime, $type, $type_value,  $proj_id, $region_id) {
        $data=DB::table('fdata')
        ->select(DB::raw('SUM(amount) as cExpenditure'))
        ->where('item',$id)
        ->whereDate('date','<=',$endTime)
        // ->where('unid', auth()->user()->unid)
        ->where($type, $type_value)
        ->where('proid', $proj_id)
        ->where('region_id', $region_id)
        ->where('trans_type','ex')
        ->first();
        self::$cbalance=(float)$data->cExpenditure;
        return $data->cExpenditure;
    }

    public static function demand($id, $type, $type_value) {

        if($type == "unid")
        {
            $data=DB::table('demand')
            ->select('amount')
            ->where('item',$id)
            ->where($type, $type_value)
            ->first();
            return $data->amount;
        }else{
            $unids = [];

            if($type == "region_id")
            {
                $unids = \App\Model\Union::where('region_id', auth()->user()->region_id)->get(['id']);

            }elseif($type == "dist")
            {
                $district = District::with('upazilas.unions')->find($type_value);

                foreach($district->upazilas as $upazila)
                {
                    foreach($upazila->unions as $union)
                    {
                        $unids[] = $union->id;
                    }
                }
            }elseif($type == "upid")
            {
                $district = Upazila::with('unions')->find($type_value);

                foreach($upazila->unions as $union)
                {
                    $unids[] = $union->id;
                }
            }

            $data=DB::table('demand')
            ->select('amount')
            ->where('item',$id)
            // ->where('unid', auth()->user()->unid)
            ->whereIn('unid', $unids)
            ->first();
            return $data->amount;
        }
    }

    public static function demandUN($id, $type, $type_value, $proj_id, $region_id) {

        if($type == "unid")
        {
            $data=DB::table('demand')
            ->select('amount')
            ->where('item',$id)
            ->where($type, $type_value)
            ->first();
            return $data->amount;
        }else{
            $unids = [];

            if($type == "region_id")
            {
                $unids = \App\Model\Union::where('region_id', auth()->user()->region_id)->get(['id']);

            }elseif($type == "dist")
            {
                $district = District::with('upazilas.unions')->find($type_value);

                foreach($district->upazilas as $upazila)
                {
                    foreach($upazila->unions as $union)
                    {
                        $unids[] = $union->id;
                    }
                }
            }elseif($type == "upid")
            {
                $district = Upazila::with('unions')->find($type_value);

                foreach($upazila->unions as $union)
                {
                    $unids[] = $union->id;
                }
            }

            $data=DB::table('demand')
            ->select('amount')
            ->where('item',$id)
            // ->where('unid', auth()->user()->unid)
            ->whereIn('unid', $unids)
            ->first();
            return $data->amount;
        }
    }

    public static function comulativeBudget()
    {
        return self::$balance-self::$cbalance;
    }
}
