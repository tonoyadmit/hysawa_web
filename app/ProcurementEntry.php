<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProcurementEntry extends Model
{
  protected $table = 'p_announce';
  protected $guarded = [
    'id','created_at', 'updated_at'
  ];
}
