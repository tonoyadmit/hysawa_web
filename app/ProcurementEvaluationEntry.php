<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class ProcurementEvaluationEntry extends Model
{
   protected $table = 'comp_sheet';
   protected $guarded = [
      'id','created_at', 'updated_at'
   ];
}
