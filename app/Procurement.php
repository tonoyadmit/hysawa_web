<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Procurement extends Model
{
   protected $table = 'procurement';
   protected $guarded = [
      'id','created_at', 'updated_at'
   ];
}
