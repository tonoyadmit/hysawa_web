<?php

namespace App\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';
    protected $api_namespace = "App\API\Controllers";
    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        //

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapApiRoutes();

        $this->mapWebRoutes();

        //
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/SuperAdmin.php'));

        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/DistrictAdmin.php'));

        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/UpazilaAdmin.php'));

        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/UpAdmin.php'));

        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/Admin.php'));

        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/TrainingAgency.php'));

        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/WaterQualityAgency.php'));

        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/DefaultUser.php'));

        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/ajax.php'));

        Route::middleware('web')
             ->namespace($this->namespace)
             ->group(base_path('routes/web.php'));

    }

    protected function mapApiRoutes()
    {
        Route::prefix('api')
             ->middleware('api')
             ->namespace($this->api_namespace)
             ->group(base_path('routes/api.php'));
    }
}
