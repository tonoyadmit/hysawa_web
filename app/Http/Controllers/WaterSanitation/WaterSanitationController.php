<?php

namespace App\Http\Controllers\WaterSanitation;

use App\AppStatus;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\ImplementStatus;
use App\Model\Download\SanitationDownload;
use App\Model\Search\SanitationSearch;
use App\Role;
use App\User;
use App\WaterSanitation;
use Auth;
use DB;
use Datatables;
use Entrust;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Validator;

class WaterSanitationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
      $this->middleware('role:superadministrator|systemadministrator|hubmanager');
    }


    public function index( Request $request )
    {
      if(!Entrust::can('view-water')) { abort(403); }
      $sanitations = WaterSanitation::take(100)->get();

      $old = [
        'starting_date' => '',
        'ending_date' => '',
        'created_by' => '',
        'village' => ''
      ];

      return view('sanitation.index', compact('sanitations', 'old'));
    }

    public function search(Request $request)
    {
      $sanitations = [];

      if($request->has('query')){
        $sanitations = (new SanitationSearch($request))->get();
      }else{
        return redirect()->route('water.index');
      }

      $old = [
        'query' => $request->input('query'),
        'starting_date' => $request->input('starting_date'),
        'ending_date' => $request->input('ending_date'),
        'created_by' => $request->input('created_by'),
        'village' => $request->input('village'),
        'download' => time()
      ];

      return view('sanitation.index', compact('sanitations', 'old'));
    }


    public function download(Request $request)
    {
      if($request->has('download'))
      {
        return (new SanitationDownload($request))->download();
      }
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      if(!Entrust::can('create-water')) { abort(403); }

      $mainype = array('Institutional latrine' => 'Institutional latrine','Public latrine' => 'Public latrine','Communal latrine' => 'Communal latrine');
      $subtype = array('Primary School' => 'Primary School','High School' => 'High School','Bazar' => 'Bazar','Madrasha' => 'Madrasha','Mosque' => 'Mosque','Slum' => 'Slum','Community' => 'Community');
      $wordOrderNO = array('1' => '1','2' => '2','3' => '3','4' => '4');
      $appstatus = AppStatus::pluck('app_status','id')->toArray();
      $ImplementStatus=ImplementStatus::pluck('imp_status','id')->toArray();
      //print_r($mainype);exit;
      return view('sanitation.insert', compact('subtype','appstatus','wordOrderNO','ImplementStatus','mainype'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      if(!Entrust::can('create-water')) { abort(403); }
//print_r($request->all());exit;
//      $validation = Validator::make($request->all(), [
//
//         'Village'          => 'required',
//         'TW_No'  => 'required',
//         'Landowner'           => 'required',
//         'Caretaker_male'          => 'required',
//         'Caretaker_female'          => 'required'
//      ]);
//
//      if($validation->fails()) {
//          return Redirect::back()->withErrors($validation)->withInput();
//      }

      $water = new WaterSanitation();
      $water->fill($request->except('_token'));

      $water->unid = auth()->user()->unid;
      $water->region_id = auth()->user()->region_id;
      $water->proj_id = auth()->user()->proj_id;
      $water->dist_id = auth()->user()->distid;
      $water->upid = auth()->user()->upid;

      $water->created_by = auth()->user()->id;
      $water->updated_by = auth()->user()->id;
//      dd($water->toArray());
      $water->save();

      Session::flash('message', "New Data saved successfully");
      return redirect('/water_sanitation/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      if(!Entrust::can('view-water')) { abort(403); }

      $water = WaterSanitation::findOrFail($id);
      return view('sanitation.view', compact('water'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      if(!Entrust::can('edit-water')) { abort(403); }

      $mainype = array('Institutional latrine' => 'Institutional latrine','Public latrine' => 'Public latrine','Communal latrine' => 'Communal latrine');
      $subtype = array('Primary School' => 'Primary School','High School' => 'High School','Bazar' => 'Bazar','Madrasha' => 'Madrasha','Mosque' => 'Mosque','Slum' => 'Slum','Community' => 'Community');
      $wordOrderNO = array('1' => '1','2' => '2','3' => '3','4' => '4');
      $appstatus = AppStatus::pluck('app_status','id')->toArray();
      $ImplementStatus=ImplementStatus::pluck('imp_status','id')->toArray();

      $WaterSanitation = WaterSanitation::where('id', '=', $id)->first();
      return view('sanitation.edit', compact('subtype','appstatus','wordOrderNO','ImplementStatus','mainype','WaterSanitation'));



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if(!Entrust::can('edit-water')) { abort(403); }

//      $validation = Validator::make($request->all(), [
//
//         'Village'          => 'required',
//         'TW_No'  => 'required',
//         'Landowner'           => 'required',
//         'Caretaker_male'          => 'required',
//         'Caretaker_female'          => 'required'
//      ]);
//
//
//      if($validation->fails()) {
//          return Redirect::back()->withErrors($validation)->withInput();
//      }

      $water = WaterSanitation::findOrFail($id);
      $water->fill($request->except('_token'));
      $water->updated_by = auth()->user()->id;
      $water->save();

      Session::flash('message', "One updated successfully");
      return redirect('/water');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



}
