<?php

namespace App\Http\Controllers\Order;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\SubOrder;
use App\Trip;
use App\SuborderTrip;
use App\Order;
use App\OrderProduct;

use Session;
use Redirect;
use Validator;

class QueuedShippingController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('role:vehiclemanager');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sub_orders = SubOrder::whereStatus(true)
                            ->where('sub_order_status', '=', '5')
                            ->orderBy('id', 'desc')
                            ->where('responsible_user_id', '=', auth()->user()->id)
                            ->paginate(6);

        $trips = Trip::whereStatus(true)->where('source_hub_id', '=', auth()->user()->reference_id)->where('trip_status', '=', '1')->lists('unique_trip_id', 'id')->toArray();

        return view('transfer-suborder.index', compact('sub_orders', 'trips'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // return $request->all();
        $validation = Validator::make($request->all(), [
            'order_id' => 'required',
            'sub_order_id' => 'required',
            'trip_id' => 'required',
            'remarks' => 'sometimes',
        ]);

        if($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        }

        // Insert Sub-Order & Trip
        $suborder_trip = new SuborderTrip();
        $suborder_trip->sub_order_id = $request->sub_order_id;
        $suborder_trip->trip_id = $request->trip_id;
        $suborder_trip->remarks = $request->remarks;
        $suborder_trip->created_by = auth()->user()->id;
        $suborder_trip->updated_by = auth()->user()->id;
        $suborder_trip->save();

        // Update Sub-Order
        $suborderUp = SubOrder::findOrFail($request->sub_order_id);
        $suborderUp->sub_order_status = '6';
        $suborderUp->save();

        // Update Order
        $order_due = SubOrder::where('sub_order_status', '!=', '6')->where('order_id', '=', $request->order_id)->count();
        if($order_due == 0){
            $orderUp = Order::findOrFail($request->order_id);
            $orderUp->order_status = '6';
            $orderUp->save();
        }

        Session::flash('message', "Sub-Order processed successfully");
        return redirect('/transfer-suborder');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
