<?php

namespace App\Http\Controllers\Order;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\SubOrder;
use App\Trip;
use App\SuborderTrip;
use App\Order;
use App\User;

use Session;
use Redirect;
use Validator;

class AssignDeliveryController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('role:superadministrator|systemadministrator|systemmoderator|hubmanager');
        $this->middleware('role:hubmanager');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sub_orders = SubOrder::whereStatus(true)->where('current_task_status', '=', '7')->orderBy('id', 'desc')->where('destination_hub_id', '=', auth()->user()->reference_id)->paginate(9);
        $deliveryman = User::whereStatus(true)->where('user_type_id', '=', '8')->where('reference_id', '=', auth()->user()->reference_id)->lists('name', 'id')->toArray();

        return view('assign-delivery.index', compact('sub_orders', 'deliveryman'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'order_id' => 'required',
            'sub_order_id' => 'required',
            'deliveryman_id' => 'required',
        ]);

        if($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        }

        // Update Sub-Order
        $suborderUp = SubOrder::findOrFail($request->sub_order_id);
        $suborderUp->current_task_status = '8';
        $suborderUp->delivery_assigned_by = auth()->user()->id;
        $suborderUp->deliveryman_id = $request->deliveryman_id;
        $suborderUp->delivery_status = '0';
        $suborderUp->save();

        // Update Order
        $suborderUp = Order::findOrFail($request->order_id);
        $suborderUp->order_status = '8';
        $suborderUp->save();

        Session::flash('message', "Delivery assigned successfully");
        return redirect('/assign-delivery');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
