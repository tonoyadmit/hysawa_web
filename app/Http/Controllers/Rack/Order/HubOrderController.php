<?php

namespace App\Http\Controllers\Order;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Country;
use App\Order;
use App\Store;
use App\State;
use App\City;
use App\Zone;
use App\ProductCategory;
use App\PickingLocations;
use App\OrderProduct;
use App\PickingTimeSlot;
use Auth;
use Session;
use Redirect;
use Validator;
use DB;
use Entrust;

class HubOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::whereStatus(true);
        $orders->where('hub_id', '=', auth()->user()->reference_id);
        $orders = $orders->orderBy('id', 'desc')->paginate(10);
        return view('hub-orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::whereStatus(true)->findOrFail($id);
        if($order->hub_id != auth()->user()->reference_id){
            abort(403);
        }
        return view('hub-orders.view', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $order = Order::findOrFail($id);

        // For Page One
        $prefix = Country::whereStatus(true)->lists('prefix', 'id')->toArray();
        $countries = Country::whereStatus(true)->lists('name', 'id')->toArray();
        $states = State::whereStatus(true)->where('country_id', '=', $order->delivery_country_id)->lists('name', 'id')->toArray();
        $cities = City::whereStatus(true)->where('state_id', '=', $order->delivery_state_id)->lists('name', 'id')->toArray();
        $zones = Zone::whereStatus(true)->where('city_id', '=', $order->delivery_city_id)->lists('name', 'id')->toArray();

        $stores = Store::whereStatus(true)->where('merchant_id', '=', $order->store->merchant->id)->lists('store_id', 'id')->toArray();

        // // For Page Two
        $categories = ProductCategory::select(array(
                                'product_categories.id AS id',
                                DB::raw("CONCAT(pc.name,' - ',product_categories.name) AS cat_name")
                            ))
                        ->leftJoin('product_categories AS pc', 'pc.id', '=', 'product_categories.parent_category_id')
                        // ->where('product_categories.parent_category_id', '!=', null)
                        ->where('product_categories.status', '=', '1')
                        ->where('pc.status', '=', '1')
                        ->lists('cat_name', 'id')
                        ->toArray();
        $warehouse = PickingLocations::whereStatus(true)->where('merchant_id', '=', $order->store->merchant->id)->lists('title', 'id')->toArray();
        $products = OrderProduct::where('order_id', '=', $id)->orderBy('id', 'desc')->get();
        $picking_time_slot = PickingTimeSlot::addSelect(DB::raw("CONCAT(day,' (',start_time,' - ',end_time,')') AS title"), "id")->whereStatus(true)->lists("title", "id")->toArray();

        // For Page Three
        $shipping_loc = Order::select(array(
                                    'countries.name AS country_title',
                                    'states.name AS state_title',
                                    'cities.name AS city_title',
                                    'zones.name AS zone_title'
                                ))
                                ->leftJoin('countries', 'countries.id', '=', 'orders.delivery_country_id')
                                ->leftJoin('states', 'states.id', '=', 'orders.delivery_state_id')
                                ->leftJoin('cities', 'cities.id', '=', 'orders.delivery_city_id')
                                ->leftJoin('zones', 'zones.id', '=', 'orders.delivery_zone_id')
                                ->where('orders.id', '=', $id)
                                ->first();

        // Call Charge Calculation API
        // return env('APP_URL').'api/charge-calculator?store_id='.$order->store->store_id.'&order_id='.$order->unique_order_id;
        $chargesJson = file_get_contents(env('APP_URL').'api/charge-calculator?store_id='.$order->store->store_id.'&order_id='.$order->unique_order_id);
        $charges = json_decode($chargesJson);
        if($charges->status != 'Success'){
            abort(403);
        }

        if($request->step){
            $step = $request->step;
        }else{
            $step = 1;
        }

        return view('hub-orders.edit', compact('prefix', 'countries', 'step', 'id', 'order', 'states', 'cities', 'zones', 'stores', 'categories', 'warehouse', 'products', 'picking_time_slot', 'shipping_loc', 'charges'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
                'store_id' => 'sometimes',
                'delivery_name' => 'sometimes',
                'delivery_email' => 'sometimes|email',
                'delivery_msisdn' => 'sometimes|between:10,25',
                'delivery_alt_msisdn' => 'sometimes|between:10,25',
                'delivery_country_id' => 'sometimes',
                'delivery_state_id' => 'sometimes',
                'delivery_city_id' => 'sometimes',
                'delivery_zone_id' => 'sometimes',
                'delivery_address1' => 'sometimes',
                'amount' => 'sometimes',
                'delivery_payment_amount' => 'sometimes',
            ]);

        if($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        }

        $order = Order::findOrFail($id);

        $order->fill($request->except('msisdn_country','alt_msisdn_country','step','include_delivery', 'amount_hidden'));
        if($request->include_delivery){
            if($request->include_delivery == '1'){
                $order->total_amount = $request->amount + $request->delivery_payment_amount;
            }
        }
        $order->save();

        if($request->step){
            if($request->step == 'complete'){
                // return $request->step;
                Session::flash('message', "Order information updated successfully");
                return redirect('/receive-picked');
            }else{
                $step = $request->step;
            }
        }else{
            $step = 1;
        }
        Session::flash('message', "Order information saved successfully");
        return redirect('/hub-order/'.$id.'/edit?step='.$step);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
