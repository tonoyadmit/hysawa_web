<?php

namespace App\Http\Controllers\Order;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\SubOrder;
use App\Trip;
use App\SuborderTrip;
use App\Order;
use App\OrderProduct;
use App\User;
use App\Rack;
use App\RackProduct;
use App\Shelf;
use App\ShelfProduct;

use Session;
use Redirect;
use Validator;
use DB;

class HubReceiveController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('role:superadministrator|systemadministrator|systemmoderator|hubmanager');
        $this->middleware('role:hubmanager');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sub_orders = SubOrder::whereStatus(true)->where('sub_order_status', '=', '6')->orderBy('id', 'desc')->where('destination_hub_id', '=', auth()->user()->reference_id)->paginate(9);
        $rider = User::whereStatus(true)->where('user_type_id', '=', '8')->where('reference_id', '=', auth()->user()->reference_id)->lists('name', 'id')->toArray();
        return view('hub-receive.index', compact('sub_orders', 'rider'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validation = Validator::make($request->all(), [
            'order_id' => 'required',
            // 'sub_order_id' => 'required',
            'remarks' => 'sometimes',
            'deliveryman_id' => 'required',
        ]);

        if($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        }

        // Update Sub-Order
        $suborder = SubOrder::findOrFail($id);
        $suborder->sub_order_status = '8';
        $suborder->delivery_assigned_by = auth()->user()->id;
        $suborder->deliveryman_id = $request->deliveryman_id;
        $suborder->delivery_status = '0';
        $suborder->save();

        // Update Order
        $order = Order::findOrFail($request->order_id);
        $order->order_status = '8';
        $order->save();

        // Release Trip
        $trip = SuborderTrip::where('sub_order_id', '=', $id)->where('status', '=', '1')->firstOrFail();
        $trip->status = '2';
        $trip->save();

        // Select Hubs
        $picking_hub_id = $suborder->source_hub_id;
        $delivery_hub_id = $suborder->destination_hub_id;

        // Decide Hub or Rack
        $sub_order_size = OrderProduct::select(array(
                                                    DB::raw("SUM(`width`) AS width"),
                                                    DB::raw("SUM(`height`) AS height"),
                                                    DB::raw("SUM(`length`) AS length"),
                                                ))
                                            ->where('sub_order_id', '=', $id)
                                            ->where('status', '!=', '0')
                                            ->first();

        $rackData = Rack::whereStatus(true)->where('hub_id', '=', $delivery_hub_id)->get();
        if($rackData->count() != 0){
            foreach ($rackData as $rack) {
                $rackUsed = RackProduct::select(array(
                                                    DB::raw("SUM(`width`) AS total_width"),
                                                    DB::raw("SUM(`height`) AS total_height"),
                                                    DB::raw("SUM(`length`) AS total_length"),
                                                ))
                                            ->join('order_product AS op', 'op.id', '=', 'rack_products.product_id')
                                            ->where('rack_products.status', '=', '1')
                                            ->where('rack_products.rack_id', '=', $rack->id)
                                            ->first();
                $available_width = $rack->width - $rackUsed->width;
                $available_height = $rack->height - $rackUsed->height;
                $available_length = $rack->length - $rackUsed->length;

                if($available_width >= $sub_order_size->width && $available_height >= $sub_order_size->height && $available_length >= $sub_order_size->length){
                    $rack_id = $rack->id;
                    $message = "Please keep the product on ".$rack->rack_title;
                    break;
                }else{
                    $rack_id = 0;
                    $message = "Dedicated rack hasn't enough space. Please use defult rack";
                }
            }

        }else{
            $rack_id = 0;
            $message = "No Rack defined for this delivery zone.";
        }

        // Insert product on rack
        $sub_order_products = OrderProduct::where('sub_order_id', '=', $id)
                                            ->where('status', '!=', '0')
                                            ->get();

        foreach ($sub_order_products as $product) {
            $rack_suborder = new RackProduct();
            $rack_suborder->rack_id = $rack_id;
            $rack_suborder->product_id = $product->id;
            $rack_suborder->status = '1';
            $rack_suborder->created_by = auth()->user()->id;
            $rack_suborder->updated_by = auth()->user()->id;
            $rack_suborder->save();
        }

        Session::flash('message', $message);
        return redirect('/hub-receive');

        // Session::flash('message', "Sub-Order received successfully");
        // return redirect('/hub-receive');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
