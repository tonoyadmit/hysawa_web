<?php

namespace App\Http\Controllers\Order;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Country;
use App\Order;
use App\SubOrder;
use App\Store;
use App\State;
use App\City;
use App\Zone;
use App\ProductCategory;
use App\PickingLocations;
use App\OrderProduct;
use App\PickingTimeSlot;
use Auth;
use Session;
use Redirect;
use Validator;
use DB;
use Entrust;

class MerchantOrderController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('role:merchantadmin|merchantsupport|storeadmin');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $orders = Order::whereStatus(true);
        if (Auth::user()->hasRole('storeadmin')) {
            $orders->where('store_id', '=', auth()->user()->reference_id);
        }else{
            $orders->whereHas('store',function($q){
                $q->where('merchant_id', '=', auth()->user()->reference_id);
            });
        }
        $orders = $orders->orderBy('id', 'desc')->paginate(10);

        return view('merchant-orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $prefix = Country::whereStatus(true)->lists('prefix', 'id')->toArray();
        $countries = Country::whereStatus(true)->lists('name', 'id')->toArray();

        if (Auth::user()->hasRole('storeadmin')) {
            $stores = Store::whereStatus(true)
                            ->where('id', '=', auth()->user()->reference_id)
                            ->lists('store_id', 'id')
                            ->toArray();
        }else{
            $stores = Store::whereStatus(true)->where('merchant_id', '=', auth()->user()->reference_id)->lists('store_id', 'id')->toArray();
        }

        return view('merchant-orders.insert', compact('prefix', 'countries', 'stores'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // return $request->all();
        $validation = Validator::make($request->all(), [
                'store_id' => 'required',
                'delivery_name' => 'required',
                'delivery_email' => 'required|email',
                'delivery_msisdn' => 'required|between:10,25',
                'delivery_alt_msisdn' => 'sometimes|between:10,25',
                'delivery_country_id' => 'required',
                'delivery_state_id' => 'required',
                'delivery_city_id' => 'required',
                'delivery_zone_id' => 'required',
                'delivery_address1' => 'required',
            ]);

        if($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        }

        $order = new Order();
        $order->fill($request->except('msisdn_country', 'alt_msisdn_country'));
        $order->created_by = auth()->user()->id;
        $order->updated_by = auth()->user()->id;
        $order->unique_order_id = "BL".time().rand(10,99);
        $order->save();

        // Create Sub-Order
        $sub_order = new SubOrder();
        $sub_order->unique_suborder_id = $order->unique_order_id."-1";
        $sub_order->order_id = $order->id;
        $sub_order->save();

        Session::flash('message', "Shipping information saved successfully");
        return redirect('/merchant-order/'.$order->id.'/edit?step=2');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $order = Order::whereStatus(true)->findOrFail($id);

        if(Auth::user()->hasRole('storeadmin')) {
            if($order->store_id != auth()->user()->reference_id){
                abort(403);
            }
        }else{
            if($order->store->merchant_id != auth()->user()->reference_id){
                abort(403);
            }
        }

        // $productStatus = $order->products[0]->status;
        // switch ($productStatus) {
        //     case "0":
        //         $pickStatus = 'error';
        //         foreach ($order->products as $row) {
        //             if($row->status != $productStatus){
        //                 $pickStatus = 'active';
        //             }
        //         }
        //         break;
        //     case "1":
        //         $pickStatus = '';
        //         foreach ($order->products as $row) {
        //             if($row->status != $productStatus){
        //                 $pickStatus = 'active';
        //             }
        //         }
        //         break;
        //     case "2":
        //         $pickStatus = 'done';
        //         foreach ($order->products as $row) {
        //             if($row->status != $productStatus){
        //                 $pickStatus = 'active';
        //             }
        //         }
        //         break;
        //     default:
        // }
        // dd($order->toArray());
        // return view('merchant-orders.view', compact('order', 'pickStatus'));
        return view('merchant-orders.view', compact('order'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        $order = Order::select(array(
                                        'orders.id',
                                        'orders.unique_order_id',
                                        'orders.store_id',
                                        'orders.delivery_name',
                                        'orders.delivery_email',
                                        'orders.delivery_msisdn',
                                        'orders.delivery_alt_msisdn',
                                        'orders.delivery_address1',
                                        'orders.delivery_zone_id',
                                        'orders.delivery_city_id',
                                        'orders.delivery_state_id',
                                        'orders.delivery_country_id',
                                        'stores.store_id AS store_name',
                                        'stores.merchant_id',
                                    ))
                    ->leftJoin('stores', 'stores.id', '=', 'orders.store_id')
                    ->where('orders.id', '=', $id)
                    ->where('orders.order_status', '=', '')
                    ->where('orders.status', '=', '1')
                    ->firstOrFail();

        if(Auth::user()->hasRole('storeadmin')) {
            if($order->store_id != auth()->user()->reference_id){
                abort(403);
            }
        }else{
            if($order->merchant_id != auth()->user()->reference_id){
                abort(403);
            }
        }

        if (Auth::user()->hasRole('storeadmin')) {
            $stores = Store::whereStatus(true)
                            ->where('id', '=', auth()->user()->reference_id)
                            ->lists('store_id', 'id')
                            ->toArray();
        }else{
            $stores = Store::whereStatus(true)->where('merchant_id', '=', auth()->user()->reference_id)->lists('store_id', 'id')->toArray();
        }

        // For Page One
        $prefix = Country::whereStatus(true)->lists('prefix', 'id')->toArray();
        $countries = Country::whereStatus(true)->lists('name', 'id')->toArray();
        $states = State::whereStatus(true)->where('country_id', '=', $order->delivery_country_id)->lists('name', 'id')->toArray();
        $cities = City::whereStatus(true)->where('state_id', '=', $order->delivery_state_id)->lists('name', 'id')->toArray();
        $zones = Zone::whereStatus(true)->where('city_id', '=', $order->delivery_city_id)->lists('name', 'id')->toArray();

        // For Page Two
        $categories = ProductCategory::select(array(
                                'product_categories.id AS id',
                                DB::raw("CONCAT(pc.name,' - ',product_categories.name) AS cat_name")
                            ))
                        ->leftJoin('product_categories AS pc', 'pc.id', '=', 'product_categories.parent_category_id')
                        // ->where('product_categories.category_type', '=', 'child')
                        ->where('product_categories.parent_category_id', '!=', null)
                        ->where('product_categories.status', '=', '1')
                        ->where('pc.status', '=', '1')
                        ->lists('cat_name', 'id')
                        ->toArray();
        $warehouse = PickingLocations::whereStatus(true)->where('merchant_id', '=', auth()->user()->reference_id)->lists('title', 'id')->toArray();
        $products = OrderProduct::where('order_id', '=', $id)->orderBy('id', 'desc')->get();
        $picking_time_slot = PickingTimeSlot::addSelect(DB::raw("CONCAT(day,' (',start_time,' - ',end_time,')') AS title"), "id")->whereStatus(true)->lists("title", "id")->toArray();

        // For Page Three
        $shipping_loc = Order::select(array(
                                    'countries.name AS country_title',
                                    'states.name AS state_title',
                                    'cities.name AS city_title',
                                    'zones.name AS zone_title'
                                ))
                                ->leftJoin('countries', 'countries.id', '=', 'orders.delivery_country_id')
                                ->leftJoin('states', 'states.id', '=', 'orders.delivery_state_id')
                                ->leftJoin('cities', 'cities.id', '=', 'orders.delivery_city_id')
                                ->leftJoin('zones', 'zones.id', '=', 'orders.delivery_zone_id')
                                ->where('orders.id', '=', $id)
                                ->first();

        // Call Charge Calculation API
        if($request->step == '3'){
            // return env('APP_URL').'api/charge-calculator?store_id='.$order->store_name.'&order_id='.$order->unique_order_id;
            // $chargesJson = file_get_contents(env('APP_URL').'api/charge-calculator?store_id='.$order->store_name.'&order_id='.$order->unique_order_id);
            // $charges = json_decode($chargesJson);
            // if($charges->status == 'Failed'){
            //     abort(403);
            // }
            $order = Order::where('orders.id', '=', $id)->first();
        }

        if($request->step){
            $step = $request->step;
        }else{
            $step = 1;
        }

        // return view('merchant-orders.edit', compact('prefix', 'countries', 'step', 'id', 'order', 'states', 'cities', 'zones', 'stores', 'categories', 'warehouse', 'products', 'picking_time_slot', 'shipping_loc', 'charges'));
        return view('merchant-orders.edit', compact('prefix', 'countries', 'step', 'id', 'order', 'states', 'cities', 'zones', 'stores', 'categories', 'warehouse', 'products', 'picking_time_slot', 'shipping_loc', 'order'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $validation = Validator::make($request->all(), [
                'store_id' => 'sometimes',
                'delivery_name' => 'sometimes',
                'delivery_email' => 'sometimes|email',
                'delivery_msisdn' => 'sometimes|between:10,25',
                'delivery_alt_msisdn' => 'sometimes|between:10,25',
                'delivery_country_id' => 'sometimes',
                'delivery_state_id' => 'sometimes',
                'delivery_city_id' => 'sometimes',
                'delivery_zone_id' => 'sometimes',
                'delivery_address1' => 'sometimes',
                'amount' => 'sometimes',
                'delivery_payment_amount' => 'sometimes',
            ]);

        if($validation->fails()) {
            return Redirect::back()->withErrors($validation)->withInput();
        }

        // Collection Pecentage
        $total_product_price = $request->amount_hidden;
        $collectable_product_price = $request->amount;
        $percent_of_collection = ($collectable_product_price/$total_product_price)*100;

        // Update Products
        $order = Order::where('orders.id', '=', $id)->first();
        foreach($order->products as $row){
            // echo ($percent_of_collection/100)*$row->sub_total;
            // echo '<br>';
            $payable_product_price = ($percent_of_collection/100)*$row->sub_total;
            $product = OrderProduct::findOrFail($row->id);
            $product->payable_product_price = $payable_product_price;
            $product->total_payable_amount = $payable_product_price + $row->total_delivery_charge;
            $product->save();
        }

        $order_update = Order::findOrFail($id);
        $order_update->fill($request->except('msisdn_country','alt_msisdn_country','step','include_delivery', 'amount_hidden', 'amount'));
        if($request->include_delivery && $request->include_delivery == '1'){
            $order_update->delivery_pay_by_cus = $request->include_delivery;
        }else{
            $order_update->delivery_pay_by_cus = '0';
        }
        $order_update->total_product_price = $total_product_price;
        $order_update->collectable_product_price = $collectable_product_price;
        $order_update->percent_of_collection = $percent_of_collection;
        $order_update->order_status = '1';
        $order_update->save();

        if($request->step){
            if($request->step == 'complete'){
                // return $request->step;
                Session::flash('message', "Order information updated successfully");
                return redirect('/merchant-order');
            }else{
                $step = $request->step;
            }
        }else{
            $step = 1;
        }
        Session::flash('message', "Order information saved successfully");
        return redirect('/merchant-order/'.$id.'/edit?step='.$step);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
