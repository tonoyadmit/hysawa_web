<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use App\User;
use Datatables;
use App\Role;

use App\UserType;
use App\dealerDetails;
use App\Zone;
use App\BikeDetails;
use App\ServiceLog;
use Validator;
use Session;
use Redirect;
use Image;
use DB;
use Auth;
use Entrust;
use Excel;


class BikeServiceReportController extends Controller
{
    public function report() {
        //dd('hi');
        if(!Entrust::can('user-list')) { abort(403); }
        //$dealers = DB::table('dealerinfo')->orderBy('id','DESC')->paginate(10);
        $colors = BikeDetails::select('color')->groupBy('color')->lists('color')->toArray();
        $divisions = dealerDetails::select('division')->groupBy('division')->lists('division')->toArray();
        //$divisions = json_decode(json_encode($divisions), true);
        //$divisions = $divisions->toArray();
        //$divisions=DB::table('dealerinfo')->groupBy('division')->get()->toArray();
        $zone=Zone::groupBy('zone')->lists('zone','zone')->toArray();
        $dealers=dealerDetails::groupBy('code')->lists('name','code')->toArray();
        $models=BikeDetails::groupBy('model')->lists('model')->toArray();
        //$dealerTypes = json_decode(json_encode($dealerTypes), true);
       //print_r($divisions);exit;
        return view('serviceReport.index',compact('divisions','dealerTypes','colors','dealers','models','zone'));
    }
    
    public function dataTableList(Request $request)
    {
        $requestData = $request->all();
        
        $tdate=$requestData['columns'][1]['search']['value'];
        $fdate=$requestData['columns'][0]['search']['value'];
        
        $today=date('Y-m-d H:i:s');
   
        $columns = array(

            0 => 'id'
        );


        
        $query = ServiceLog::select('servicelog.*','bikeinfo.doNo','bikeinfo.coNo','bikeinfo.model','bikeinfo.chesisNo','bikeinfo.engineNo','bikeinfo.color','customerinfo.cname','dealerinfo.name','dealerinfo.zone');
        $query->join('customerinfo','servicelog.customerInfo_id','=','customerinfo.id');
        $query->join('dealerinfo', 'servicelog.serviceDealerId', '=', 'dealerinfo.id');
        $query->join('bikeinfo', 'servicelog.bikeinfoId', '=', 'bikeinfo.id');
        $query2 = ServiceLog::select('servicelog.*','bikeinfo.doNo','bikeinfo.coNo','bikeinfo.model','bikeinfo.chesisNo','bikeinfo.engineNo','bikeinfo.color','customerinfo.cname','dealerinfo.name','dealerinfo.zone');
        $query2->join('customerinfo','servicelog.customerInfo_id','=','customerinfo.id');
        $query2->join('dealerinfo', 'servicelog.serviceDealerId', '=', 'dealerinfo.id');
        $query2->join('bikeinfo', 'servicelog.bikeinfoId', '=', 'bikeinfo.id');

        $parameter = array();

        if (!empty($requestData['columns'][2]['search']['value'])) {   //name
            //$parameter.=" AND dealerinfo.phoneNumber = '" . $requestData['columns'][0]['search']['value'] . "' ";
            $parameter["engineNo"]=$requestData['columns'][2]['search']['value'];
            $query->where('bikeinfo.engineNo','=',$requestData['columns'][2]['search']['value']);
            $query2->where('bikeinfo.engineNo','=',$requestData['columns'][2]['search']['value']);
        }
        if (!empty($requestData['columns'][3]['search']['value'])) {   //name
            $parameter["chesisNo"]=$requestData['columns'][3]['search']['value'];
            $query->where('bikeinfo.chesisNo','=',$requestData['columns'][3]['search']['value']);
            $query2->where('bikeinfo.chesisNo','=',$requestData['columns'][3]['search']['value']);
        }

        if (!empty($requestData['columns'][4]['search']['value'])) {   //name
            $parameter["serviceNO"]=$requestData['columns'][4];
            $query->where('servicelog.serviceNO','=',$requestData['columns'][4]['search']['value']);
            $query2->where('servicelog.serviceNO','=',$requestData['columns'][4]['search']['value']);
        }
        if (!empty($requestData['columns'][5]['search']['value'])) {   //name
            $parameter["color"]=$requestData['columns'][5]['search']['value'];
            $query->where('bikeinfo.color','=',$requestData['columns'][5]['search']['value']);
            $query2->where('bikeinfo.color','=',$requestData['columns'][5]['search']['value']);
        }
       
        if (!empty($requestData['columns'][6]['search']['value'])) {   //name
            $parameter["dealerCode"]=$requestData['columns'][6]['search']['value'];
            $query->where('dealerinfo.code','=',$requestData['columns'][6]['search']['value']);
            $query2->where('dealerinfo.code','=',$requestData['columns'][6]['search']['value']);
        }
        if (!empty($requestData['columns'][7]['search']['value'])) {   //name
            $parameter["model"]=$requestData['columns'][7]['search']['value'];
            $query->where('bikeinfo.model','=',$requestData['columns'][7]['search']['value']);
            $query2->where('bikeinfo.model','=',$requestData['columns'][7]['search']['value']);
        }
        if (!empty($requestData['columns'][8]['search']['value'])) {   //name
            $parameter["zone"]=$requestData['columns'][8]['search']['value'];
            $query->where('dealerinfo.zone','=',$requestData['columns'][8]['search']['value']);
            $query2->where('dealerinfo.zone','=',$requestData['columns'][8]['search']['value']);
        }
        $parameter["tdate"]=$requestData['columns'][1]['search']['value'];
        $parameter["fdate"]=$requestData['columns'][0]['search']['value'];
        
        if( !empty($requestData['columns'][1]['search']['value']) )
        {
            $tdate=$requestData['columns'][1]['search']['value'];
        }  else {
            $tdate=date('Y-m-d  H:i:s');
        }
        if ((!empty($fdate))&&!empty($tdate)) 
             {
                
               $query->whereBetween('servicelog.updated_at',array($fdate,$tdate));
                $query2->whereBetween('servicelog.updated_at',array($fdate,$tdate));
             
        }else
        {
            $query->whereDate('servicelog.updated_at','<',$today);
            $query2->whereDate('servicelog.updated_at','<',$today);
             $fdate='';
        }


        $query->where('bikeinfo.status','=',1);
        $query2->where('bikeinfo.status','=',1);
        
        $query->where('servicelog.serviceStatus','=',1);
        $query2->where('servicelog.serviceStatus','=',1);
        //var_dump($query->toSql());
        $query =$query->get();
        //$collection = collect($query);
        //var_dump($query->toSql());
        //$query = mysqli_query($con, $sql) or die("SQL Full: Error");
        $requestData['order'][0]['dir'] = 'DESC';
        $totalData = $query->count();
        $totalFiltered = $totalData;
//$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.
        //$queryData = base64_encode($parameter);
        //$sql.="ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ";  // adding length
        $query2->orderBy($columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir']);
        $query2->offset($requestData['start']);
        $query2->limit($requestData['length']);
        //$query = mysqli_query($con, $sql) or die("error no data found: get data");
        $query =$query2->get()->toarray();
        // print_r($parameter);exit;
        $data = array();
        $j = 0;
        foreach($query as $row) {
           
            $nestedData = array();
            $nestedData[] = ucwords($row["doNo"]);
            $nestedData[] = ucwords($row["coNo"]);
            $nestedData[] = ucwords($row["model"]);
            $nestedData[] = ucwords($row["engineNo"]);
            $nestedData[] = ucwords($row["chesisNo"]);
            $nestedData[] = ucwords($row["color"]);
            $nestedData[] = ucwords($row["updated_at"]);
            $nestedData[] = ucwords($row["name"]);
            $nestedData[] = ucwords($row["cname"]);
            $nestedData[] = ucwords($row["serviceNO"]);
            $nestedData[] =$row["zone"];
            $data[] = $nestedData;
        }
    $url= url('downloadServiceReport');
        //print_r($data);exit;
        $query = '<tbody class="employee-grid-error"><tr><th colspan="3"><a href="'.$url.'?queryinfo=' .  http_build_query($parameter) . '">Download CSV </a></th></tr></tbody>';
        //$query2 = '<tbody class="employee-grid-error"><tr><th colspan="3"><a href="download_sms_summaryLog.php?queryinfo=' . $queryData . '">Download Summary Report CSV </a></th></tr></tbody>';
        $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data,
            "query"=>$query
           
                // total data array
        );

        echo json_encode($json_data);
    }
    
    public function downloadExcel()

	{
            $today=date('Y-m-d H:i:s');
            if (Input::has('tdate'))
            {
               $tdate=Input::get('tdate'); 
            }else
            {
                 $tdate=date('Y-m-d  H:i:s');
            }
            if (Input::has('fdate'))
            {
                $fdate=Input::get('fdate'); 
            }
            
        $query = ServiceLog::select('servicelog.*','bikeinfo.doNo','bikeinfo.coNo','bikeinfo.model','bikeinfo.chesisNo','bikeinfo.engineNo','bikeinfo.color','customerinfo.cname','dealerinfo.name');
        $query->join('customerinfo','servicelog.customerInfo_id','=','customerinfo.id');
        $query->join('dealerinfo', 'servicelog.serviceDealerId', '=', 'dealerinfo.id');
        $query->join('bikeinfo', 'servicelog.bikeinfoId', '=', 'bikeinfo.id');
            if (Input::has('engineNo'))
            {
                $engineNo= Input::get('engineNo');
               $query->where('bikeinfo.engineNo','=',$engineNo); 
            }
            if (Input::has('chesisNo'))
            {
              $chesisNo= Input::get('chesisNo');
             $query->where('bikeinfo.chesisNo','=',$chesisNo);    
            }
            if (Input::has('serviceNO'))
            {
              $serviceNO= Input::get('serviceNO');
              $query->where('servicelog.serviceNO','=',$serviceNO);  
            }
            if (Input::has('color'))
            {
              $color= Input::get('color');
              $query->where('bikeinfo.color','=',$color);  
            }
            if (Input::has('dealerCode'))
            {   
                $dealerCode= Input::get('dealerCode');
                $query->where('dealerinfo.code','=',$dealerCode);  
            }
            if (Input::has('model'))
            {
               $model= Input::get('model');
               $query->where('bikeinfo.model','=',$model);  
            }
             if (Input::has('zone'))
            {
                $zone= Input::get('zone');
               $query->where('dealerinfo.zone','=',$zone);  
            }
            
            if ((!empty($fdate))&&!empty($tdate)) 
             {
                
               $query->whereBetween('servicelog.updated_at',array($fdate,$tdate));
                
             
            }else
            {
                $query->whereDate('servicelog.updated_at','<',$today);
                
                 $fdate='';
            }
            $query->where('bikeinfo.status','=',1);
            $query->where('servicelog.serviceStatus','=',1);
            //var_dump($query->toSql());exit;
            $data = $query->get()->toArray();
//print_r($data);exit;
            return Excel::create('BikeServiceReport', function($excel) use ($data) {

                    $excel->sheet('Report', function($sheet) use ($data)

            {

                            $sheet->fromArray($data);

            });

            })->download('csv');

	}
}
