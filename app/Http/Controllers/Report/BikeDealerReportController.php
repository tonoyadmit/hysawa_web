<?php

namespace App\Http\Controllers\Report;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;

use App\User;
use Datatables;
use App\Role;

use App\UserType;
use App\dealerDetails;
use App\BikeDetails;
use App\ServiceLog;
use Validator;
use Session;
use Redirect;
use Image;
use DB;
use Auth;
use Entrust;
use Excel;


class BikeDealerReportController extends Controller
{
    public function report() {
        if(!Entrust::can('user-list')) { abort(403); }
        //$dealers = DB::table('dealerinfo')->orderBy('id','DESC')->paginate(10);
        $colors = BikeDetails::select('color')->groupBy('color')->lists('color')->toArray();
        $divisions = dealerDetails::select('division')->groupBy('division')->lists('division')->toArray();
        //$divisions = json_decode(json_encode($divisions), true);
        //$divisions = $divisions->toArray();
        //$divisions=DB::table('dealerinfo')->groupBy('division')->get()->toArray();
        $dealers=dealerDetails::groupBy('code')->lists('name','code')->toArray();
        $models=BikeDetails::groupBy('model')->lists('model')->toArray();
        //$dealerTypes = json_decode(json_encode($dealerTypes), true);
       //print_r($divisions);exit;
        return view('dealerServicereport.index',compact('divisions','dealerTypes','colors','dealers','models'));
    }
    
    public function dataTableList(Request $request)
    {
        $requestData = $request->all();
        
        $tdate=$requestData['columns'][1]['search']['value'];
        $fdate=$requestData['columns'][0]['search']['value'];
        
        $today=date('Y-m-d H:i:s');
   
        $columns = array(

            0 => 'id'
        );

        
        //$reserves = DB::table('reserves')->selectRaw('*, count(*)')->groupBy('day');
        
        $query = BikeDetails::select('*');
        //$query->join('dealerinfo', 'bikeinfo.dealerCode', '=', 'dealerinfo.code');
        
        
        $query2 = BikeDetails::select('bikeinfo.id','name','dealerCode', DB::raw('count(*) as total'));
        $query2->join('dealerinfo', 'bikeinfo.dealerCode', '=', 'dealerinfo.code');

        $parameter = array();

        if (!empty($requestData['columns'][2]['search']['value'])) {   //name
            //$parameter.=" AND dealerinfo.phoneNumber = '" . $requestData['columns'][0]['search']['value'] . "' ";
            $parameter["dealer"]=$requestData['columns'][2]['search']['value'];
            $query->where('bikeinfo.dealerCode','=',$requestData['columns'][2]['search']['value']);
            $query2->where('bikeinfo.dealerCode','=',$requestData['columns'][2]['search']['value']);
        }
        if (!empty($requestData['columns'][3]['search']['value'])) {   //name
            $parameter["model"]=$requestData['columns'][3]['search']['value'];
            $query->where('bikeinfo.model','=',$requestData['columns'][3]['search']['value']);
            $query2->where('bikeinfo.model','=',$requestData['columns'][3]['search']['value']);
        }

        
        $parameter["tdate"]=$requestData['columns'][1]['search']['value'];
        $parameter["fdate"]=$requestData['columns'][0]['search']['value'];
        
        if( !empty($requestData['columns'][1]['search']['value']) )
        {
            $tdate=$requestData['columns'][1]['search']['value'];
        }  else {
            $tdate=date('Y-m-d  H:i:s');
        }
        if ((!empty($fdate))&&!empty($tdate)) 
             {
                
               $query->whereBetween('bikeinfo.updated_at',array($fdate,$tdate));
                $query2->whereBetween('bikeinfo.updated_at',array($fdate,$tdate));
             
        }else
        {
            $query->whereDate('bikeinfo.updated_at','<',$today);
            $query2->whereDate('bikeinfo.updated_at','<',$today);
             $fdate='';
        }


        $query->where('bikeinfo.status','=',1);
        $query2->where('bikeinfo.status','=',1);
        //var_dump($query->toSql());exit;
        $query->groupBy('dealerCode');
                
        $requestData['order'][0]['dir'] = 'DESC';
        //var_dump($query->toSql());exit;
        $totalData = count($query2->get()->toarray());
        $totalFiltered = $totalData;
//$totalFiltered = mysqli_num_rows($query); // when there is a search parameter then we have to modify total number filtered rows as per search result.
        //$queryData = base64_encode($parameter);
        //$sql.="ORDER BY " . $columns[$requestData['order'][0]['column']] . "   " . $requestData['order'][0]['dir'] . "   LIMIT " . $requestData['start'] . " ," . $requestData['length'] . " ";  // adding length
        $query2->orderBy('bikeinfo.'.$columns[$requestData['order'][0]['column']],$requestData['order'][0]['dir']);
        $query2->offset($requestData['start']);
        $query2->limit($requestData['length']);
        
        $query2->groupBy('dealerCode')->get()->toarray();
        $query =$query2->get()->toarray();
         //print_r($query2);exit;
        $data = array();
        $j = 0;
        foreach($query as $row) {
           
            $nestedData = array();
            $nestedData[] = ucwords($row["name"]);
            $nestedData[] = ucwords($row["dealerCode"]);
            $nestedData[] = ucwords($row["total"]);
            $nestedData[]=ServiceLog::getConfigVariables($row["id"],$tdate,$fdate);
            $data[] = $nestedData;
        }
    $url= url('downloadDealerReport');
        //print_r($data);exit;
        $query = '<tbody class="employee-grid-error"><tr><th colspan="3"><a href="'.$url.'?queryinfo=' .  http_build_query($parameter) . '">Download CSV </a></th></tr></tbody>';
        //$query2 = '<tbody class="employee-grid-error"><tr><th colspan="3"><a href="download_sms_summaryLog.php?queryinfo=' . $queryData . '">Download Summary Report CSV </a></th></tr></tbody>';
        $json_data = array(
            "draw" => intval($requestData['draw']), // for every request/draw by clientside , they send a number as a parameter, when they recieve a response/data they first check the draw number, so we are sending same number in draw.
            "recordsTotal" => intval($totalData), // total number of records
            "recordsFiltered" => intval($totalFiltered), // total number of records after searching, if there is no searching then totalFiltered = totalData
            "data" => $data,
            "query"=>$query
           
                // total data array
        );

        echo json_encode($json_data);
    }
    
    public function downloadExcel()

	{
            $today=date('Y-m-d H:i:s');
            if (Input::has('tdate'))
            {
               $tdate=Input::get('tdate'); 
            }else
            {
                 $tdate=date('Y-m-d  H:i:s');
            }
            if (Input::has('fdate'))
            {
                $fdate=Input::get('fdate'); 
            }
            
            $query = BikeDetails::select('bikeinfo.id','name','dealerCode', DB::raw('count(*) as total'));
            $query->join('dealerinfo', 'bikeinfo.dealerCode', '=', 'dealerinfo.code');
            
            if (Input::has('model'))
            {
              $color= Input::get('model');
              $query->where('bikeinfo.model','=',$color);  
            }
            if (Input::has('dealerCode'))
            {   
                $dealerCode= Input::get('dealerCode');
                $query->where('bikeinfo.dealerCode','=',$dealerCode);  
            }
          
            
            if ((!empty($fdate))&&!empty($tdate)) 
             {
                
               $query->whereBetween('bikeinfo.updated_at',array($fdate,$tdate));
                
             
            }else
            {
                $query->whereDate('bikeinfo.updated_at','<',$today);
                
                 $fdate='';
            }
            $query->where('bikeinfo.status','=',1);
            $data = $query->get()->toArray();
             foreach($data as $key=>$row) {
           
            $nestedData = array();
            $nestedData["Name"] = ucwords($row["name"]);
            $nestedData["Dealer Code"] = ucwords($row["dealerCode"]);
            $nestedData["Total Sell"] = ucwords($row["total"]);
            $nestedData["Total Service"]=ServiceLog::getConfigVariables($row["id"],$tdate,$fdate);
            $data2[] = $nestedData;
        }
            return Excel::create('BikeSellVSServiceReport', function($excel) use ($data2) {

                    $excel->sheet('BikeSellVSServiceReport', function($sheet) use ($data2)

            {

                            $sheet->fromArray($data2);

            });

            })->download('csv');

	}
}
