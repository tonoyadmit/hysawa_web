<?php

namespace App\Http\Controllers\TrainingAgency;

use App\Http\Controllers\Controller;
use App\ImplementStatus;
use App\Model\Download\WaterDownload;
use App\Model\Search\Request\WaterSearchRequest;
use App\Model\Search\WaterSearch;
use App\Water;
use App\WaterTech;
use Illuminate\Http\Request;
use DB;

class TrainingParticipantController extends Controller
{
    private $view_path = "core.training_agency.participant.";
    private $route_path = "training-agency.trainingParticipant.";

    public function index( Request $request )
    {
      $region_id=auth()->user()->region_id;

      $participantslists = DB::select(
        DB::raw("SELECT
            tbl_training.TrgTitle,
            tbl_training.TrgVenue,
            tbl_training.TrgFrom,
            tbl_training.TrgTo,
            tbl_trg_participants.partcipant_name,
            tbl_trg_participants.designation,
            fdistrict.distname,
            fupazila.upname,
            funion.unname,
            tbl_trg_participants.mobile_no,
            tbl_trg_participants.email_address,
            tbl_trg_participants.participant_id,
            tbl_trg_participants.region_id,
            tbl_training.Agency,
            (select title from trg_title where trg_title.id = tbl_training.TrgTitle) 
      as col1,
      (select VenueName from tbl_venue where tbl_venue.VenueID = tbl_training.TrgVenue) 
      as col2,
      (select agency_name from trg_agency where trg_agency.id = tbl_training.Agency) 
      as col3,
      (select desg from trg_desg where trg_desg.id = tbl_trg_participants.designation) 
      as col4
            FROM
            (
              (
                (tbl_trg_participants LEFT JOIN fdistrict ON tbl_trg_participants.district_id = fdistrict.id)
                  LEFT JOIN fupazila ON tbl_trg_participants.upazila_id = fupazila.id)
                    LEFT JOIN funion ON tbl_trg_participants.union_id = funion.id)

                LEFT JOIN tbl_training ON tbl_trg_participants.TrgCode = tbl_training.TrgCode
                WHERE
                tbl_trg_participants.region_id = '$region_id' order by participant_id desc"
            )
       );

      return view($this->view_path.'index', compact('participantslists'));
    }

    public function getUpzilla(Request $request)
    {
        $requestData = $request->all();
        $disid=$requestData['disid'];
        $upzilla=DB::table('fupazila')->where('disid',$disid)->pluck('upname','id')->toArray();
         return $upzilla;
    }

    public function getUnion(Request $request)
    {
        $requestData = $request->all();
        $disid=$requestData['disid'];
        $upid=$requestData['upid'];
        $union=DB::table('funion')->where('distid',$disid)->where('upid',$upid)->pluck('unname','id')->toArray();
        return $union;
    }

    public function create(Request $request)
    {
      $region_id=auth()->user()->region_id;
      $requestData = $request->all();
      $TrgCode=$requestData["TrgCode"];
      $participants=DB::table('tbl_trg_participants')
              ->leftJoin('fdistrict', 'tbl_trg_participants.district_id', '=', 'fdistrict.id')
              ->leftJoin('fupazila', 'tbl_trg_participants.upazila_id', '=', 'fupazila.id')
              ->leftJoin('funion', 'tbl_trg_participants.union_id', '=', 'funion.id')
              ->leftJoin('trg_desg', 'tbl_trg_participants.designation', '=', 'trg_desg.id')
              ->where('tbl_trg_participants.region_id',$region_id)->where('TrgCode',$TrgCode)
              ->get()->toArray();

      $TrgCode=$requestData["TrgCode"];
      $district=DB::table('fdistrict')->pluck('distname','id')->toArray();

      $des = DB::table('trg_desg')->pluck('desg','id')->toArray();
      return view($this->view_path.'insert', compact('district','des','TrgCode','participants'));
    }

    public function store(Request $request)
    {
      $data=$request->except('_token','_method');
      $data["updated_by"]=auth()->user()->id;
      $data["created_by"]=auth()->user()->id;
      $data["region_id"]=auth()->user()->region_id;
      $data["proj_id"]=auth()->user()->proj_id;

      $count = \DB::table('tbl_trg_participants')->insert($data);

      if($count){
        return redirect()->route($this->route_path.'index');
      }

      return redirect()->route($this->route_path.'index');

      \Session::flash('message', "New Data saved successfully");
      return redirect()->route($this->route_path.'index');
    }

    public function show($id)
    {
      $water = Water::findOrFail($id);
      return view($this->view_path.'view', compact('water'));
    }

    public function edit($id)
    {
        $region_id=auth()->user()->region_id;
        // $participants =DB::table('tbl_trg_participants')->where('TrgCode',$id)->first();
        $participants =DB::table('tbl_trg_participants')->where('participant_id',$id)->first();
        $district=DB::table('fdistrict')->where('region_id',$region_id)->pluck('distname','id')->toArray();
        $upazila=DB::table('fupazila')->where('id',$participants->upazila_id)->pluck('upname','id')->toArray();
        $union=DB::table('funion')->where('id',$participants->union_id)->pluck('unname','id')->toArray();
        $des=DB::table('trg_desg')->pluck('desg','id')->toArray();
       // dd($participants);
        return view($this->view_path.'edit', compact('district','upazila','union','des','TrgCode','participants'));
    }

    public function update(Request $request, $id)
    {
        $Info = \DB::table('tbl_trg_participants')
                    ->where('participant_id', $id)
                    ->get();

        $data=$request->except('_token','_method');
        $data["updated_by"]=auth()->user()->id;

        if(count($Info)>0){
           DB::table('tbl_trg_participants')
              ->where('participant_id',$id)
              ->update($data);
        }

        \Session::flash('message', "One updated successfully");
        return redirect()->route($this->route_path.'index');
    }
}
