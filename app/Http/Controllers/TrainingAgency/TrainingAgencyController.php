<?php

namespace App\Http\Controllers\TrainingAgency;

use App\Http\Controllers\Controller;
use App\ImplementStatus;
use App\Model\Download\WaterDownload;
use App\Model\Search\Request\WaterSearchRequest;
use App\Model\Search\WaterSearch;
use App\Water;
use App\WaterTech;
use Illuminate\Http\Request;
use DB;

class TrainingAgencyController extends Controller
{
  private $view_path = "core.training_agency.training.";
  private $route_path = "training-agency.trainingAgency.";

    public function index( Request $request )
    {
      $region_id=auth()->user()->region_id;
      // $traininglists = DB::select( DB::raw("SELECT * FROM tbl_training WHERE region_id = '$region_id'") );
     
      $traininglists = DB::select( DB::raw("SELECT *,
      (select title from trg_title where trg_title.id = tbl_training.TrgTitle) 
      as col1,
      (select VenueName from tbl_venue where tbl_venue.VenueID = tbl_training.TrgVenue) 
      as col2,
      (select agency_name from trg_agency where trg_agency.id = tbl_training.Agency) 
      as col3
       FROM tbl_training  WHERE region_id = '$region_id' order by TrgCode desc"));

      return view($this->view_path.'index', compact('traininglists'));
    }

    public function create()
    {
        $region_id=auth()->user()->region_id;
        $trainingTitle = DB::table('trg_title')->pluck('title','id')->toArray();
        $venueList = DB::table('tbl_venue')->pluck('VenueName','VenueID')->toArray();
        $agency=DB::table('trg_agency')->pluck('agency_name','id')->toArray();
        return view($this->view_path.'insert', compact('venueList','trainingTitle','agency'));
    }

    public function store(Request $request)
    {
      $data=$request->except('_token','_method');
      $data["updated_by"]=auth()->user()->id;
      $data["created_by"]=auth()->user()->id;
      $data["proj_id"]=auth()->user()->proj_id;
      $data["region_id"]=auth()->user()->region_id;

      $count = \DB::table('tbl_training')->insert($data);

      if($count){
        return redirect()->route($this->route_path.'index');
      }

      return redirect()->route($this->route_path.'index');
      \Session::flash('message', "New Data saved successfully");
      return redirect()->route($this->route_path.'index');
    }

    public function show($id)
    {
      $water = Water::findOrFail($id);
      return view($this->view_path.'view', compact('water'));
    }

    public function edit($id)
    {
      $region_id     = auth()->user()->region_id;
      $trainingTitle = DB::table('trg_title')->pluck('title','id')->toArray();
      $venueList     = DB::table('tbl_venue')->pluck('VenueName','VenueID')->toArray();
      $agency        = DB::table('trg_agency')->pluck('agency_name','id')->toArray();
      $traininglists = DB::table('tbl_training')->WHERE('TrgCode',$id)->first();
      return view($this->view_path.'edit', compact('traininglists','venueList','trainingTitle','agency'));
    }

    public function update(Request $request, $id)
    {
        $Info = \DB::table('tbl_training')
                    ->where('TrgCode', $id)
                    ->get();
        $data=$request->except('_token','_method');
        $data["updated_by"]=auth()->user()->id;

        if(COUNT($Info)>0){
          DB::table('tbl_training')
            ->where('TrgCode', $Info->first()->TrgCode)
            ->update($data);
        }

      \Session::flash('message', "One updated successfully");
      return redirect()->route($this->route_path.'index');
    }
}
