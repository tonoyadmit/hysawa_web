<?php

namespace App\Http\Controllers\TrainingAgency;

use App\Http\Controllers\Controller;
use DB;

class DashboardController extends Controller {

  private $view_path = "core.training_agency.";

  public function dashboard()
  {
    $regionDatas=DB::table('tbl_training')
              ->select(DB::raw('region.region_name, COUNT(tbl_training.TrgCode) AS CountOfTrgCode, SUM(tbl_training.TrgParticipantNo) AS SumOfTrgParticipantNo, region.region_id'))
              ->leftJoin('region', 'tbl_training.region_id', '=', 'region.id')
              ->groupBy('region.region_name')
              ->get()->toArray();
    foreach ($regionDatas as $regionData){
        //$regionData->region_name;
        $regionDatasArray=array();
        $countData=DB::table('tbl_trg_participants')
                    ->select(DB::raw('COUNT(participant_id) AS rpcount'))
                    ->where('region_id',$regionData->region_id)
                    ->first();
        $regionDatasArray['region_name']=$regionData->region_name;
        $regionDatasArray['CountOfTrgCode']=$regionData->CountOfTrgCode;
        $regionDatasArray['SumOfTrgParticipantNo']=$regionData->SumOfTrgParticipantNo;
        $regionDatasArray['rpcount']=$countData->rpcount;
        $regionDatatotals[]=$regionDatasArray;
    }

    $agentDatas=DB::table('tbl_trg_participants')
              ->select(DB::raw('tbl_training.Agency, COUNT(tbl_trg_participants.participant_id) AS CountOfparticipant_id'))
              ->leftJoin('tbl_training', 'tbl_trg_participants.TrgCode', '=', 'tbl_training.TrgCode')
              ->groupBy('tbl_training.Agency')
              ->get()->toArray();
    foreach ($agentDatas as $agentData){
        //$regionData->region_name;
        $agentDatasArray=array();
        $countagencyData=DB::table('tbl_training')
                    ->select(DB::raw('COUNT(TrgCode) AS tcount,SUM(TrgParticipantNo) AS tpcount'))
                    ->where('Agency',$agentData->Agency)
                    ->first();
        $agentDatasArray['Agency']=$agentData->Agency;
        $agentDatasArray['tcount']=$countagencyData->tcount;
        $agentDatasArray['tpcount']=$countagencyData->tpcount;
        $agentDatasArray['CountOfparticipant_id']=$agentData->CountOfparticipant_id;
        $agentDatatotals[]=$agentDatasArray;
    }

    $trainingTitles=DB::table('tbl_training')
              ->select(DB::raw('count(tbl_trg_participants.participant_id) as CountOfparticipant_id, tbl_training.TrgTitle'))
              ->leftJoin('tbl_trg_participants', 'tbl_training.TrgCode', '=', 'tbl_trg_participants.TrgCode')
              ->groupBy('tbl_training.TrgTitle')
              ->get()->toArray();

    foreach ($trainingTitles as $trainingTitle) {
      $trainingDatasArray=array();

      $COURSEData=DB::table('tbl_training')
                  ->select(DB::raw('COUNT(TrgCode) AS ttcount'))
                  ->where('TrgTitle',$trainingTitle->TrgTitle)
                  ->first();

      $COURSEData2=DB::table('tbl_trg_participants')
                  ->select(DB::raw('sum(CASE WHEN designation =  "Chairman"  THEN 1 ELSE 0 END) AS Chairman,sum(CASE WHEN designation =  "Secretary"  THEN 1 ELSE 0 END) AS Secretary,sum(CASE WHEN designation LIKE  "%fem%"  THEN 1 ELSE 0 END) AS fem,sum(CASE WHEN designation =  "Male member"  THEN 1 ELSE 0 END) AS Malemember,sum(CASE WHEN designation =  "Community organizer"  THEN 1 ELSE 0 END) AS Communityorganizer,sum(CASE WHEN designation like  "%SO%"  THEN 1 ELSE 0 END) AS SO,sum(CASE WHEN designation like  "%Accountant%"  THEN 1 ELSE 0 END) AS Accountant,sum(CASE WHEN designation like  "%HYSAWA%"  THEN 1 ELSE 0 END) AS HYSAWA,sum(CASE WHEN designation like  "%PNGO%"  THEN 1 ELSE 0 END) AS PNGO'))
                  ->leftJoin('tbl_training', 'tbl_trg_participants.TrgCode', '=', 'tbl_training.TrgCode')
                  ->where('TrgTitle',$trainingTitle->TrgTitle)
                  ->first();

      $trainingDatasArray['Chairman']=$COURSEData2->Chairman;
      $trainingDatasArray['Secretary']=$COURSEData2->Secretary;
      $trainingDatasArray['fem']=$COURSEData2->fem;
      $trainingDatasArray['Malemember']=$COURSEData2->Malemember;
      $trainingDatasArray['Communityorganizer']=$COURSEData2->Communityorganizer;
      $trainingDatasArray['SO']=$COURSEData2->SO;
      $trainingDatasArray['Accountant']=$COURSEData2->Accountant;
      $trainingDatasArray['HYSAWA']=$COURSEData2->HYSAWA;
      $trainingDatasArray['PNGO']=$COURSEData2->PNGO;
      $trainingDatasArray['ttcount']=$COURSEData->ttcount;
      $trainingDatasArray['TrgTitle']=$trainingTitle->TrgTitle;
      $trainingDatasArray['CountOfparticipant_id']=$trainingTitle->CountOfparticipant_id;
      $titleDatatotals[]=$trainingDatasArray;
    }

    return view($this->view_path.'dashboard',compact('titleDatatotals','agentDatatotals','regionDatatotals'));
  }


}