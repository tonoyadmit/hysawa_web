<?php

namespace App\Http\Controllers\Water;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\ImplementStatus;
use App\Model\Download\WaterDownload;
use App\Model\Search\WaterSearch;
use App\Role;
use App\User;
use App\Water;
use App\WaterTech;
use Auth;
use DB;
use Datatables;
use Entrust;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Validator;

class WaterController extends Controller
{

    public function __construct()
    {
      $this->middleware('role:superadministrator|systemadministrator|hubmanager');
    }

    public function index( Request $request )
    {
      if(!Entrust::can('view-water')) { abort(403); }

      $waters = Water::take(100)->get();
      $old = [
        'starting_date' => '',
        'ending_date' => '',
        'created_by' => '',
        'ward_no' => '',
        'village' => ''
      ];

      return view('water.index', compact('waters', 'old'));
    }


    public function search(Request $request)
    {
      $waters = [];

      if($request->has('query')){
        $waters = (new WaterSearch($request))->get();
      }else{
        return redirect()->route('water.index');
      }

      $old = [
        'query' => $request->input('query'),
        'starting_date' => $request->input('starting_date'),
        'ending_date' => $request->input('ending_date'),
        'created_by' => $request->input('created_by'),
        'ward_no' => $request->input('ward_no'),
        'village' => $request->input('village'),
        'download' => time()
      ];

      return view('water.index', compact('waters', 'old'));
    }


    public function download(Request $request)
    {
      if($request->has('download'))
      {
        return (new WaterDownload($request))->download();
      }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      if(!Entrust::can('create-water')) { abort(403); }

      $up = array(
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
        '6' => '6',
        '7' => '7',
        '8' => '8',
        '9' => '9');

      $wordOrderNO = array(
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4');

      $tech = WaterTech::pluck('techtype','id')->toArray();

      $ImplementStatus=ImplementStatus::pluck('imp_status','id')->toArray();

      return view('water.insert', compact('up','tech','wordOrderNO','ImplementStatus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      if(!Entrust::can('create-shelf')) { abort(403); }
      //print_r($request->all());exit;
      $validation = Validator::make($request->all(), [

         'Village'          => 'required',
         'TW_No'  => 'required',
         'Landowner'           => 'required',
         'Caretaker_male'          => 'required',
         'Caretaker_female'          => 'required'
      ]);

      if($validation->fails()) {
          return Redirect::back()->withErrors($validation)->withInput();
      }

      $water = new Water();
      $water->fill($request->except('_token'));

      $water->unid = auth()->user()->unid;
      $water->region_id = auth()->user()->region_id;
      $water->proj_id = auth()->user()->proj_id;
      $water->distid = auth()->user()->distid;
      $water->upid = auth()->user()->upid;

      $water->created_by = auth()->user()->id;
      $water->updated_by = auth()->user()->id;
      $water->save();

      Session::flash('message', "New Data saved successfully");
      return redirect('/water/create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      if(!Entrust::can('view-water')) { abort(403); }

      $water = Water::findOrFail($id);
      return view('water.view', compact('water'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      if(!Entrust::can('edit-water')) { abort(403); }

      $up = array('1' => '1','2' => '2','3' => '3','4' => '4','5' => '5','6' => '6','7' => '7','8' => '8','9' => '9');
      $wordOrderNO = array('1' => '1','2' => '2','3' => '3','4' => '4');
      $tech = WaterTech::pluck('techtype','id')->toArray();
      $ImplementStatus=ImplementStatus::pluck('imp_status','id')->toArray();
      $water = Water::where('id', '=', $id)->first();

      return view('water.edit', compact('water','up','wordOrderNO','tech','ImplementStatus','water'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      if(!Entrust::can('edit-water')) { abort(403); }

      $validation = Validator::make($request->all(), [

         'Village'          => 'required',
         'TW_No'  => 'required',
         'Landowner'           => 'required',
         'Caretaker_male'          => 'required',
         'Caretaker_female'          => 'required'
      ]);


      if($validation->fails()) {
          return Redirect::back()->withErrors($validation)->withInput();
      }

      $water = Water::findOrFail($id);
      $water->fill($request->except('_token'));
      $water->updated_by = auth()->user()->id;
      $water->save();

      Session::flash('message', "One updated successfully");
      return redirect('/water');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }



}
