<?php

namespace App\Http\Controllers\UpazilaAdmin\MonthlyReport;

use App\Http\Controllers\Controller;
use App\Model\District;
use App\Model\Report\District\MonthlyReportGenerator;
use App\Model\Union;
use App\Model\Upazila;
use App\ReportData;
use DB;
use Illuminate\Http\Request;

class PeriodController extends Controller
{
  private $view_path = "core.upazila.monthly-report.water.";

  public function index(Request $request)
  {
    $user = \Auth::user();
    $unions = Union::where('upid', $user->upid)->get();
    return view($this->view_path.'index', compact('unions'));
  }

  public function show(Request $request)
  {
    //dd($request->all());
    try{
      $report = new MonthlyReportGenerator($request, 'core.upazila.monthly-report.water.');
      return $report->getReport();
    }catch(\Exception $e){
      \Log::info($e->getMessage());
      \Session::flash('error', "Data Not Found.");
      return redirect()->back();
      return response()->json(['status' => 'false', 'message' => 'Something Went Wrong']);
    }
  }

  public function getPeriods(Request $request)
  {
    $periods= \DB::table('rep_period')
              ->select('rep_data.rep_id', 'rep_period.period', 'rep_data.id', 'rep_data.update')
              ->join('rep_data', 'rep_period.rep_id', '=', 'rep_data.rep_id')
              ->where(function($q) use($request) {
                  $q->where('rep_data.unid', $request->input('union_id'));
                })
              ->get();
    $str = '<option value="all">All</option>';
    foreach($periods as $p){
      $str .= '<option value="'.$p->id.'">'.$p->period.'</option>';
    }
    return response()->json(['status' => true, 'data' => $str]);
  }
}
