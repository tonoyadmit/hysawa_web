<?php

namespace App\Http\Controllers\UpazilaAdmin\MonthlyReport;

use App\Http\Controllers\Controller;
use App\Model\District;
use App\Model\Union;
use App\Model\Upazila;
use App\ReportData;
use DB;
use Illuminate\Http\Request;

class PeriodController2 extends Controller
{
  private $view_path = "core.upazila.monthly-report.water.";

  public function index(Request $request)
  {
    $user = \Auth::user();
    $unions = Union::where('upid', $user->upid)->get();
    return view($this->view_path.'index', compact('unions'));
  }

  public function show(Request $request)
  {
    if($request->rep_data_id == "all")
    {
      return $this->showAll($request);
    }

    $ReportData = ReportData::findOrFail($request->input('rep_data_id'));

    $uid = $request->input('union_id');
    $requestData=$request->all();
    $id = $request->input("rep_data_id");
    $rid = $ReportData->rep_id;

    $row_lastdata=DB::select(DB::raw("
      SELECT
      Sum(rep_data.cdf_no) AS SumOfcdf_no,
      Sum(sa_approved),
      Sum(sa_completed),
      Sum(sa_benef),
      Sum(sa_renovated),
      Sum(ws_approved),
      Sum(ws_completed),
      Sum(ws_beneficiary),
      Sum(ws_hc_benef),
      Sum(ws_50),
      Sum(rep_data.cdf_pop) AS SumOfcdf_pop,
      Sum(rep_data.cdf_male) AS SumOfcdf_male,
      Sum(rep_data.cdf_female) AS SumOfcdf_female,
      Sum(rep_data.cdf_pop_hc) AS SumOfcdf_pop_hc,
      Sum(rep_data.cdf_hh) AS SumOfcdf_hh,
      Sum(rep_data.cdf_hh_hc) AS SumOfcdf_hh_hc,
      Sum(rep_data.cdf_pop_disb) AS SumOfcdf_pop_disb,
      Sum(rep_data.cdf_pop_safety) AS SumOfcdf_pop_safety,
      Sum(rep_data.cdf_cf_tot) AS SumOfcdf_cf_tot,
      Sum(rep_data.cdf_cf_male) AS SumOfcdf_cf_male,
      Sum(rep_data.cdf_cf_female) AS SumOfcdf_cf_female,
      Sum(rep_data.cb_trg) AS SumOfcb_trg,
      Sum(rep_data.cb_trg_up_total) AS SumOfcb_trg_up_total,
      Sum(rep_data.cb_trg_up_male) AS SumOfcb_trg_up_male,
      Sum(rep_data.cb_trg_up_female) AS SumOfcb_trg_up_female,
      Sum(rep_data.cb_trg_stf_total) AS SumOfcb_trg_stf_total,
      Sum(rep_data.cb_trg_stf_male) AS SumOfcb_trg_stf_male,
      Sum(rep_data.cb_trg_stf_female) AS SumOfcb_trg_stf_female,
      Sum(rep_data.cb_trg_vol_total) AS SumOfcb_trg_vol_total,
      Sum(rep_data.cb_trg_vol_male) AS SumOfcb_trg_vol_male,
      Sum(rep_data.cb_trg_vol_female) AS SumOfcb_trg_vol_female,
      Sum(rep_data.CHY_hw_ses) AS SumOfCHY_hw_ses,
      Sum(rep_data.CHY_hw_male) AS SumOfCHY_hw_male,
      Sum(rep_data.CHY_hw_female) AS SumOfCHY_hw_female,
      Sum(rep_data.CHY_mn_ses) AS SumOfCHY_mn_ses,
      Sum(rep_data.CHY_mn_female) AS SumOfCHY_mn_female,
      Sum(rep_data.CHY_sa_ses) AS SumOfCHY_sa_ses,
      Sum(rep_data.CHY_sa_hh) AS SumOfCHY_sa_hh,
      Sum(rep_data.CHY_fh_ses) AS SumOfCHY_fh_ses,
      Sum(rep_data.CHY_fh_hh) AS SumOfCHY_fh_hh,
      Sum(rep_data.CHY_gb_new) AS SumOfCHY_gb_new,
      Sum(rep_data.CHY_gb_rep) AS SumOfCHY_gb_rep,
      Sum(rep_data.CHY_dr) AS SumOfCHY_dr,
      Sum(rep_data.CHY_dr_pop) AS SumOfCHY_dr_pop,
      Sum(rep_data.CHY_vd) AS SumOfCHY_vd,
      Sum(rep_data.CHY_vd_pop) AS SumOfCHY_vd_pop,
      Sum(rep_data.HHS_new) AS SumOfHHS_new,
      Sum(rep_data.HHS_new_hc) AS SumOfHHS_new_hc,
      Sum(rep_data.HHS_rep) AS SumOfHHS_rep,
      Sum(rep_data.HHS_rep_hc) AS SumOfHHS_rep_hc,
      Sum(rep_data.scl_tot) AS SumOfscl_tot,
      Sum(rep_data.scl_tot_std) AS SumOfscl_tot_std,
      Sum(rep_data.scl_boys) AS SumOfscl_boys,
      Sum(rep_data.scl_girls) AS SumOfscl_girls,
      Sum(rep_data.scl_pri) AS SumOfscl_pri,
      Sum(rep_data.scl_pri_std) AS SumOfscl_pri_std,
      Sum(rep_data.scl_high) AS SumOfscl_high,
      Sum(rep_data.scl_high_std) AS SumOfscl_high_std,
      Sum(rep_data.scl_mad) AS SumOfscl_mad,
      Sum(rep_data.scl_mad_std) AS SumOfscl_mad_std,
      Sum(rep_data.scl_hp_ses) AS SumOfscl_hp_ses,
      Sum(rep_data.scl_hp_boys) AS SumOfscl_hp_boys,
      Sum(rep_data.scl_hp_girls) AS SumOfscl_hp_girls,
      Sum(rep_data.scl_mn_ses) AS SumOfscl_mn_ses,
      Sum(rep_data.scl_mn_girls) AS SumOfscl_mn_girls,
      Sum(rep_data.scl_dr) AS SumOfscl_dr,
      Sum(rep_data.scl_dr_std) AS SumOfscl_dr_std,
      Sum(rep_data.scl_vd) AS SumOfscl_vd,
      Sum(rep_data.scl_vd_std) AS SumOfscl_vd_std,
      Sum(rep_data.up_stf_tot) AS SumOfup_stf_tot,
      Sum(rep_data.up_stf_male) AS SumOfup_stf_male,
      Sum(rep_data.up_stf_female) AS SumOfup_stf_female,
      Sum(rep_data.up_pngo) AS SumOfup_pngo,
      Sum(rep_data.up_cont) AS SumOfup_cont,
      Sum(rep_data.TW_maintenance) AS SumOfTW_maintenance,
      Sum(rep_data.wsp) AS SumOfwsp,
      Sum(rep_data.CT_trg),
      Sum(rep_data.pdb)
      FROM rep_data
      WHERE rep_id < $rid  AND rep_data.unid = $uid")
    );

    $row_lastdata = json_decode(json_encode((array) $row_lastdata), true);
    $row_lastdata=$row_lastdata[0];

    $row_totaldata =
      DB::select(
        DB::raw(
          "SELECT
            Sum(rep_data.cdf_no) AS SumOfcdf_no,
            Sum(sa_approved),
            Sum(sa_completed),
            Sum(sa_benef),
            Sum(sa_renovated),
            Sum(ws_approved),
            Sum(ws_completed),
            Sum(ws_beneficiary),
            Sum(ws_hc_benef),
            Sum(ws_50),
            Sum(rep_data.cdf_pop) AS SumOfcdf_pop,
            Sum(rep_data.cdf_male) AS SumOfcdf_male,
            Sum(rep_data.cdf_female) AS SumOfcdf_female,
            Sum(rep_data.cdf_pop_hc) AS SumOfcdf_pop_hc,
            Sum(rep_data.cdf_hh) AS SumOfcdf_hh,
            Sum(rep_data.cdf_hh_hc) AS SumOfcdf_hh_hc,
            Sum(rep_data.cdf_pop_disb) AS SumOfcdf_pop_disb,
            Sum(rep_data.cdf_pop_safety) AS SumOfcdf_pop_safety,
            Sum(rep_data.cdf_cf_tot) AS SumOfcdf_cf_tot,
            Sum(rep_data.cdf_cf_male) AS SumOfcdf_cf_male,
            Sum(rep_data.cdf_cf_female) AS SumOfcdf_cf_female,
            Sum(rep_data.cb_trg) AS SumOfcb_trg,
            Sum(rep_data.cb_trg_up_total) AS SumOfcb_trg_up_total,
            Sum(rep_data.cb_trg_up_male) AS SumOfcb_trg_up_male,
            Sum(rep_data.cb_trg_up_female) AS SumOfcb_trg_up_female,
            Sum(rep_data.cb_trg_stf_total) AS SumOfcb_trg_stf_total,
            Sum(rep_data.cb_trg_stf_male) AS SumOfcb_trg_stf_male,
            Sum(rep_data.cb_trg_stf_female) AS SumOfcb_trg_stf_female,
            Sum(rep_data.cb_trg_vol_total) AS SumOfcb_trg_vol_total,
            Sum(rep_data.cb_trg_vol_male) AS SumOfcb_trg_vol_male,
            Sum(rep_data.cb_trg_vol_female) AS SumOfcb_trg_vol_female,
            Sum(rep_data.CHY_hw_ses) AS SumOfCHY_hw_ses,
            Sum(rep_data.CHY_hw_male) AS SumOfCHY_hw_male,
            Sum(rep_data.CHY_hw_female) AS SumOfCHY_hw_female,
            Sum(rep_data.CHY_mn_ses) AS SumOfCHY_mn_ses,
            Sum(rep_data.CHY_mn_female) AS SumOfCHY_mn_female,
            Sum(rep_data.CHY_sa_ses) AS SumOfCHY_sa_ses,
            Sum(rep_data.CHY_sa_hh) AS SumOfCHY_sa_hh,
            Sum(rep_data.CHY_fh_ses) AS SumOfCHY_fh_ses,
            Sum(rep_data.CHY_fh_hh) AS SumOfCHY_fh_hh,
            Sum(rep_data.CHY_gb_new) AS SumOfCHY_gb_new,
            Sum(rep_data.CHY_gb_rep) AS SumOfCHY_gb_rep,
            Sum(rep_data.CHY_dr) AS SumOfCHY_dr,
            Sum(rep_data.CHY_dr_pop) AS SumOfCHY_dr_pop,
            Sum(rep_data.CHY_vd) AS SumOfCHY_vd,
            Sum(rep_data.CHY_vd_pop) AS SumOfCHY_vd_pop,
            Sum(rep_data.HHS_new) AS SumOfHHS_new,
            Sum(rep_data.HHS_new_hc) AS SumOfHHS_new_hc,
            Sum(rep_data.HHS_rep) AS SumOfHHS_rep,
            Sum(rep_data.HHS_rep_hc) AS SumOfHHS_rep_hc,
            Sum(rep_data.scl_tot) AS SumOfscl_tot,
            Sum(rep_data.scl_tot_std) AS SumOfscl_tot_std,
            Sum(rep_data.scl_boys) AS SumOfscl_boys,
            Sum(rep_data.scl_girls) AS SumOfscl_girls,
            Sum(rep_data.scl_pri) AS SumOfscl_pri,
            Sum(rep_data.scl_pri_std) AS SumOfscl_pri_std,
            Sum(rep_data.scl_high) AS SumOfscl_high,
            Sum(rep_data.scl_high_std) AS SumOfscl_high_std,
            Sum(rep_data.scl_mad) AS SumOfscl_mad,
            Sum(rep_data.scl_mad_std) AS SumOfscl_mad_std,
            Sum(rep_data.scl_hp_ses) AS SumOfscl_hp_ses,
            Sum(rep_data.scl_hp_boys) AS SumOfscl_hp_boys,
            Sum(rep_data.scl_hp_girls) AS SumOfscl_hp_girls,
            Sum(rep_data.scl_mn_ses) AS SumOfscl_mn_ses,
            Sum(rep_data.scl_mn_girls) AS SumOfscl_mn_girls,
            Sum(rep_data.scl_dr) AS SumOfscl_dr,
            Sum(rep_data.scl_dr_std) AS SumOfscl_dr_std,
            Sum(rep_data.scl_vd) AS SumOfscl_vd,
            Sum(rep_data.scl_vd_std) AS SumOfscl_vd_std,
            Sum(rep_data.up_stf_tot) AS SumOfup_stf_tot,
            Sum(rep_data.up_stf_male) AS SumOfup_stf_male,
            Sum(rep_data.up_stf_female) AS SumOfup_stf_female,
            Sum(rep_data.up_pngo) AS SumOfup_pngo,
            Sum(rep_data.up_cont) AS SumOfup_cont,
            Sum(rep_data.TW_maintenance) AS SumOfTW_maintenance,
            Sum(rep_data.wsp) AS SumOfwsp,
            Sum(rep_data.CT_trg),
            Sum(rep_data.pdb)

            FROM rep_data
            WHERE
            rep_id <= $rid AND rep_data.unid = $uid" ));

      $row_totaldata = json_decode(json_encode((array) $row_totaldata), true);
      $row_totaldata = $row_totaldata[0];

      $row_getbaseline = DB::select( DB::raw("SELECT * FROM rep_data WHERE rep_id = 99 and unid=$uid" ));
      $row_getbaseline = json_decode(json_encode((array) $row_getbaseline), true);
      $row_getbaseline = @$row_getbaseline[0];

      $row_gettargets = DB::select( DB::raw("SELECT * FROM rep_data WHERE rep_id = 100  and unid=$uid" ));
      $row_gettargets = json_decode(json_encode((array) $row_gettargets), true);
      $row_gettargets = @$row_gettargets[0];
      $union = Union::with('upazila.district.region')->find($request->input('union_id'));
      $period = \DB::table('rep_period')->where('rep_id', $ReportData->rep_id)->first();

      $ReportData = DB::select( DB::raw("SELECT * FROM rep_data WHERE id = $id  and unid=$uid" ));
      $ReportData = json_decode(json_encode((array) $ReportData), true);
      $ReportData = @$ReportData[0];

    if($request->input('report_type') == "print")
    {
      return view($this->view_path.'print', compact(
        'ReportData',
        'row_lastdata',
        'row_totaldata',
        'row_getbaseline',
        'row_gettargets',
        'union',
        'period',
        'ReportData'
      ));
    }

    $user = \Auth::user();
    $unions = Union::where('upid', $user->upid)->get();

    return view($this->view_path.'show', compact('ReportData','row_lastdata','row_totaldata','row_getbaseline','row_gettargets', 'union', 'period', 'unions'));
  }


  private function showAll(Request $request)
  {
    $periods= \DB::table('rep_period')
              ->select('rep_data.rep_id', 'rep_period.period', 'rep_data.id', 'rep_data.update')
              ->leftjoin('rep_data', 'rep_period.rep_id', '=', 'rep_data.rep_id')
              ->where(function($q) use($request) {
                $q->where('rep_data.unid', auth()->user()->unid);
              })->get()->pluck('rep_id');

    $last_month_report_id = \DB::table('rep_period')
                                ->where('id', '!=', 99)
                                ->where('id', '!=', 100)
                                ->orderBy('id', 'DESC')
                                ->first()->id;

    //dd($last_month_report_id);
    //$ReportData = ReportData::findOrFail($request->input('rep_data_id'));

    $uid         = $request->input('union_id');
    $requestData = $request->all();
    $id          = $last_month_report_id; //$request->input("rep_data_id");
    $rid         = $last_month_report_id; //$ReportData->rep_id;

    $row_lastdata=DB::select(DB::raw("
      SELECT
      Sum(rep_data.cdf_no) AS SumOfcdf_no,
      Sum(sa_approved),
      Sum(sa_completed),
      Sum(sa_benef),
      Sum(sa_renovated),
      Sum(ws_approved),
      Sum(ws_completed),
      Sum(ws_beneficiary),
      Sum(ws_hc_benef),
      Sum(ws_50),
      Sum(rep_data.cdf_pop) AS SumOfcdf_pop,
      Sum(rep_data.cdf_male) AS SumOfcdf_male,
      Sum(rep_data.cdf_female) AS SumOfcdf_female,
      Sum(rep_data.cdf_pop_hc) AS SumOfcdf_pop_hc,
      Sum(rep_data.cdf_hh) AS SumOfcdf_hh,
      Sum(rep_data.cdf_hh_hc) AS SumOfcdf_hh_hc,
      Sum(rep_data.cdf_pop_disb) AS SumOfcdf_pop_disb,
      Sum(rep_data.cdf_pop_safety) AS SumOfcdf_pop_safety,
      Sum(rep_data.cdf_cf_tot) AS SumOfcdf_cf_tot,
      Sum(rep_data.cdf_cf_male) AS SumOfcdf_cf_male,
      Sum(rep_data.cdf_cf_female) AS SumOfcdf_cf_female,
      Sum(rep_data.cb_trg) AS SumOfcb_trg,
      Sum(rep_data.cb_trg_up_total) AS SumOfcb_trg_up_total,
      Sum(rep_data.cb_trg_up_male) AS SumOfcb_trg_up_male,
      Sum(rep_data.cb_trg_up_female) AS SumOfcb_trg_up_female,
      Sum(rep_data.cb_trg_stf_total) AS SumOfcb_trg_stf_total,
      Sum(rep_data.cb_trg_stf_male) AS SumOfcb_trg_stf_male,
      Sum(rep_data.cb_trg_stf_female) AS SumOfcb_trg_stf_female,
      Sum(rep_data.cb_trg_vol_total) AS SumOfcb_trg_vol_total,
      Sum(rep_data.cb_trg_vol_male) AS SumOfcb_trg_vol_male,
      Sum(rep_data.cb_trg_vol_female) AS SumOfcb_trg_vol_female,
      Sum(rep_data.CHY_hw_ses) AS SumOfCHY_hw_ses,
      Sum(rep_data.CHY_hw_male) AS SumOfCHY_hw_male,
      Sum(rep_data.CHY_hw_female) AS SumOfCHY_hw_female,
      Sum(rep_data.CHY_mn_ses) AS SumOfCHY_mn_ses,
      Sum(rep_data.CHY_mn_female) AS SumOfCHY_mn_female,
      Sum(rep_data.CHY_sa_ses) AS SumOfCHY_sa_ses,
      Sum(rep_data.CHY_sa_hh) AS SumOfCHY_sa_hh,
      Sum(rep_data.CHY_fh_ses) AS SumOfCHY_fh_ses,
      Sum(rep_data.CHY_fh_hh) AS SumOfCHY_fh_hh,
      Sum(rep_data.CHY_gb_new) AS SumOfCHY_gb_new,
      Sum(rep_data.CHY_gb_rep) AS SumOfCHY_gb_rep,
      Sum(rep_data.CHY_dr) AS SumOfCHY_dr,
      Sum(rep_data.CHY_dr_pop) AS SumOfCHY_dr_pop,
      Sum(rep_data.CHY_vd) AS SumOfCHY_vd,
      Sum(rep_data.CHY_vd_pop) AS SumOfCHY_vd_pop,
      Sum(rep_data.HHS_new) AS SumOfHHS_new,
      Sum(rep_data.HHS_new_hc) AS SumOfHHS_new_hc,
      Sum(rep_data.HHS_rep) AS SumOfHHS_rep,
      Sum(rep_data.HHS_rep_hc) AS SumOfHHS_rep_hc,
      Sum(rep_data.scl_tot) AS SumOfscl_tot,
      Sum(rep_data.scl_tot_std) AS SumOfscl_tot_std,
      Sum(rep_data.scl_boys) AS SumOfscl_boys,
      Sum(rep_data.scl_girls) AS SumOfscl_girls,
      Sum(rep_data.scl_pri) AS SumOfscl_pri,
      Sum(rep_data.scl_pri_std) AS SumOfscl_pri_std,
      Sum(rep_data.scl_high) AS SumOfscl_high,
      Sum(rep_data.scl_high_std) AS SumOfscl_high_std,
      Sum(rep_data.scl_mad) AS SumOfscl_mad,
      Sum(rep_data.scl_mad_std) AS SumOfscl_mad_std,
      Sum(rep_data.scl_hp_ses) AS SumOfscl_hp_ses,
      Sum(rep_data.scl_hp_boys) AS SumOfscl_hp_boys,
      Sum(rep_data.scl_hp_girls) AS SumOfscl_hp_girls,
      Sum(rep_data.scl_mn_ses) AS SumOfscl_mn_ses,
      Sum(rep_data.scl_mn_girls) AS SumOfscl_mn_girls,
      Sum(rep_data.scl_dr) AS SumOfscl_dr,
      Sum(rep_data.scl_dr_std) AS SumOfscl_dr_std,
      Sum(rep_data.scl_vd) AS SumOfscl_vd,
      Sum(rep_data.scl_vd_std) AS SumOfscl_vd_std,
      Sum(rep_data.up_stf_tot) AS SumOfup_stf_tot,
      Sum(rep_data.up_stf_male) AS SumOfup_stf_male,
      Sum(rep_data.up_stf_female) AS SumOfup_stf_female,
      Sum(rep_data.up_pngo) AS SumOfup_pngo,
      Sum(rep_data.up_cont) AS SumOfup_cont,
      Sum(rep_data.TW_maintenance) AS SumOfTW_maintenance,
      Sum(rep_data.wsp) AS SumOfwsp,
      Sum(rep_data.CT_trg),
      Sum(rep_data.pdb)
      FROM rep_data
      WHERE rep_id < $rid  AND rep_data.unid = $uid")
    );

    $row_lastdata = json_decode(json_encode((array) $row_lastdata), true);
    $row_lastdata=$row_lastdata[0];

    $row_totaldata = DB::select(DB::raw(
          "SELECT
            Sum(rep_data.cdf_no) AS SumOfcdf_no,
            Sum(sa_approved),
            Sum(sa_completed),
            Sum(sa_benef),
            Sum(sa_renovated),
            Sum(ws_approved),
            Sum(ws_completed),
            Sum(ws_beneficiary),
            Sum(ws_hc_benef),
            Sum(ws_50),
            Sum(rep_data.cdf_pop) AS SumOfcdf_pop,
            Sum(rep_data.cdf_male) AS SumOfcdf_male,
            Sum(rep_data.cdf_female) AS SumOfcdf_female,
            Sum(rep_data.cdf_pop_hc) AS SumOfcdf_pop_hc,
            Sum(rep_data.cdf_hh) AS SumOfcdf_hh,
            Sum(rep_data.cdf_hh_hc) AS SumOfcdf_hh_hc,
            Sum(rep_data.cdf_pop_disb) AS SumOfcdf_pop_disb,
            Sum(rep_data.cdf_pop_safety) AS SumOfcdf_pop_safety,
            Sum(rep_data.cdf_cf_tot) AS SumOfcdf_cf_tot,
            Sum(rep_data.cdf_cf_male) AS SumOfcdf_cf_male,
            Sum(rep_data.cdf_cf_female) AS SumOfcdf_cf_female,
            Sum(rep_data.cb_trg) AS SumOfcb_trg,
            Sum(rep_data.cb_trg_up_total) AS SumOfcb_trg_up_total,
            Sum(rep_data.cb_trg_up_male) AS SumOfcb_trg_up_male,
            Sum(rep_data.cb_trg_up_female) AS SumOfcb_trg_up_female,
            Sum(rep_data.cb_trg_stf_total) AS SumOfcb_trg_stf_total,
            Sum(rep_data.cb_trg_stf_male) AS SumOfcb_trg_stf_male,
            Sum(rep_data.cb_trg_stf_female) AS SumOfcb_trg_stf_female,
            Sum(rep_data.cb_trg_vol_total) AS SumOfcb_trg_vol_total,
            Sum(rep_data.cb_trg_vol_male) AS SumOfcb_trg_vol_male,
            Sum(rep_data.cb_trg_vol_female) AS SumOfcb_trg_vol_female,
            Sum(rep_data.CHY_hw_ses) AS SumOfCHY_hw_ses,
            Sum(rep_data.CHY_hw_male) AS SumOfCHY_hw_male,
            Sum(rep_data.CHY_hw_female) AS SumOfCHY_hw_female,
            Sum(rep_data.CHY_mn_ses) AS SumOfCHY_mn_ses,
            Sum(rep_data.CHY_mn_female) AS SumOfCHY_mn_female,
            Sum(rep_data.CHY_sa_ses) AS SumOfCHY_sa_ses,
            Sum(rep_data.CHY_sa_hh) AS SumOfCHY_sa_hh,
            Sum(rep_data.CHY_fh_ses) AS SumOfCHY_fh_ses,
            Sum(rep_data.CHY_fh_hh) AS SumOfCHY_fh_hh,
            Sum(rep_data.CHY_gb_new) AS SumOfCHY_gb_new,
            Sum(rep_data.CHY_gb_rep) AS SumOfCHY_gb_rep,
            Sum(rep_data.CHY_dr) AS SumOfCHY_dr,
            Sum(rep_data.CHY_dr_pop) AS SumOfCHY_dr_pop,
            Sum(rep_data.CHY_vd) AS SumOfCHY_vd,
            Sum(rep_data.CHY_vd_pop) AS SumOfCHY_vd_pop,
            Sum(rep_data.HHS_new) AS SumOfHHS_new,
            Sum(rep_data.HHS_new_hc) AS SumOfHHS_new_hc,
            Sum(rep_data.HHS_rep) AS SumOfHHS_rep,
            Sum(rep_data.HHS_rep_hc) AS SumOfHHS_rep_hc,
            Sum(rep_data.scl_tot) AS SumOfscl_tot,
            Sum(rep_data.scl_tot_std) AS SumOfscl_tot_std,
            Sum(rep_data.scl_boys) AS SumOfscl_boys,
            Sum(rep_data.scl_girls) AS SumOfscl_girls,
            Sum(rep_data.scl_pri) AS SumOfscl_pri,
            Sum(rep_data.scl_pri_std) AS SumOfscl_pri_std,
            Sum(rep_data.scl_high) AS SumOfscl_high,
            Sum(rep_data.scl_high_std) AS SumOfscl_high_std,
            Sum(rep_data.scl_mad) AS SumOfscl_mad,
            Sum(rep_data.scl_mad_std) AS SumOfscl_mad_std,
            Sum(rep_data.scl_hp_ses) AS SumOfscl_hp_ses,
            Sum(rep_data.scl_hp_boys) AS SumOfscl_hp_boys,
            Sum(rep_data.scl_hp_girls) AS SumOfscl_hp_girls,
            Sum(rep_data.scl_mn_ses) AS SumOfscl_mn_ses,
            Sum(rep_data.scl_mn_girls) AS SumOfscl_mn_girls,
            Sum(rep_data.scl_dr) AS SumOfscl_dr,
            Sum(rep_data.scl_dr_std) AS SumOfscl_dr_std,
            Sum(rep_data.scl_vd) AS SumOfscl_vd,
            Sum(rep_data.scl_vd_std) AS SumOfscl_vd_std,
            Sum(rep_data.up_stf_tot) AS SumOfup_stf_tot,
            Sum(rep_data.up_stf_male) AS SumOfup_stf_male,
            Sum(rep_data.up_stf_female) AS SumOfup_stf_female,
            Sum(rep_data.up_pngo) AS SumOfup_pngo,
            Sum(rep_data.up_cont) AS SumOfup_cont,
            Sum(rep_data.TW_maintenance) AS SumOfTW_maintenance,
            Sum(rep_data.wsp) AS SumOfwsp,
            Sum(rep_data.CT_trg),
            Sum(rep_data.pdb)

            FROM rep_data
            WHERE
            rep_id <= $rid AND rep_data.unid = $uid" ));

      $row_totaldata = json_decode(json_encode((array) $row_totaldata), true);
      $row_totaldata = $row_totaldata[0];

      $row_getbaseline = DB::select( DB::raw("SELECT * FROM rep_data WHERE rep_id = 99 and unid=$uid" ));
      $row_getbaseline = json_decode(json_encode((array) $row_getbaseline), true);
      $row_getbaseline = @$row_getbaseline[0];

      $row_gettargets = DB::select( DB::raw("SELECT * FROM rep_data WHERE rep_id = 100  and unid=$uid" ));
      $row_gettargets = json_decode(json_encode((array) $row_gettargets), true);
      $row_gettargets = @$row_gettargets[0];


      $union = Union::with('upazila.district.region')->find($request->input('union_id'));
      $period = "";//\DB::table('rep_period')->where('rep_id', $ReportData->rep_id)->first();
      // $ReportData = \DB::table('rep_data')->where('unid', $uid)->where('id', $id)->get()->toArray();

      $ReportData = DB::select( DB::raw("SELECT * FROM rep_data WHERE id = $id  and unid=$uid" ));
      $ReportData = json_decode( json_encode( (array) $ReportData), true);
      $ReportData = @$ReportData[0];
      // $ReportData = $ReportData->toArray();

    if($request->input('report_type') == "print")
    {
      return view($this->view_path.'print', compact(
        'ReportData',
        'row_lastdata',
        'row_totaldata',
        'row_getbaseline',
        'row_gettargets',
        'union',
        'period',
        'ReportData'
      ));
    }

    $user = \Auth::user();
    $unions = Union::where('upid', $user->upid)->get();


    return view($this->view_path.'show', compact('ReportData','row_lastdata','row_totaldata','row_getbaseline','row_gettargets', 'union', 'period', 'unions'));
  }


  public function getPeriods(Request $request)
  {
    $periods= \DB::table('rep_period')
              ->select('rep_data.rep_id', 'rep_period.period', 'rep_data.id', 'rep_data.update')
              ->leftjoin('rep_data', 'rep_period.rep_id', '=', 'rep_data.rep_id')
              ->where(function($q) use($request) {
                $q->where('rep_data.unid', $request->input('union_id'));
              })->get();

    $str = "<option value='all' selected='selected'>All</option>";

    foreach($periods as $p)
    {
      $str .= '<option value="'.$p->id.'">'.$p->period.'</option>';
    }
    return response()->json(['status' => true, 'data' => $str]);
  }
}
