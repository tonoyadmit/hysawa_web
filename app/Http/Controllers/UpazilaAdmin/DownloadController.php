<?php

namespace App\Http\Controllers\UpazilaAdmin;

use App\Http\Controllers\Controller;

class DownloadController extends Controller {

  private $view_path = "core.upazila.download.";

  public function index()
  {
    return view($this->view_path.'index');
  }
}