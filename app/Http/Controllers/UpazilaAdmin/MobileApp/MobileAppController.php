<?php

namespace App\Http\Controllers\UpazilaAdmin\MobileApp;

use App\Http\Controllers\Controller;
use App\Model\MobAppDataList;
use App\Model\Search\MobAppDataListSearch;
use Illuminate\Http\Request;

class MobileAppController extends Controller {

  private $view_path = "core.upazila.mobile-app.";

  public function index(Request $request)
  {
    $request->request->add([
        'project_id'   => auth()->user()->proj_id,
        'region_id'    => auth()->user()->region_id,
        'district_id'  => auth()->user()->distid,
        'upazila_id'   => auth()->user()->upid,
    ]);

    if($request->has('print'))
    {
      // $datas = MobAppDataList::with('union.upazila.district')
      //             ->with('items.question')
      //             ->with('user')
      //             ->where('upid', auth()->user()->upid)
      //             ->orderBy('created_at', 'DESC')
      //             ->get();

      $datas = (new MobAppDataListSearch($request, false))->get();

      return view($this->view_path.'print',compact('datas'));
    }

    $old = [
      'project_id'   => auth()->user()->proj_id,
      'region_id'    => auth()->user()->region_id,
      'district_id'  => auth()->user()->distid,
      'upazila_id'   => auth()->user()->upid,

      'union_id'      => '',
      'starting_date' => '',
      'ending_date'   => '',
      'cdfno'         => '',
      'type'          => '',
      'user_id'       => ''
    ];

    if($request->has('query'))
    {
      $old = [
        'query'        => $request->input('query'),
        'project_id'   => auth()->user()->proj_id,
        'region_id'    => auth()->user()->region_id,
        'district_id'  => auth()->user()->distid,
        'upazila_id'   => auth()->user()->upid,
        'union_id'     => $request->input('union_id'),
        'starting_date' => $request->input('starting_date'),
        'ending_date'  => $request->input('ending_date'),
        'cdfno'        => $request->input('cdfno'),
        'type'         => $request->input('type'),
        'user_id'      => $request->input('user_id')
      ];
    }

    $datas = (new MobAppDataListSearch($request, true))->get();
    return view($this->view_path.'index', compact('datas', 'old'));
  }

  public function show($id)
  {
    $data = MobAppDataList::with('union.upazila.district')
                  ->with('items.question')
                  ->with('user')
                  ->where('upid', auth()->user()->upid)
                  ->find($id);
    return view($this->view_path.'show',compact('data'));
  }
}