<?php

namespace App\Http\Controllers\UpazilaAdmin\Finance;

use App\Fhead;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Model\FinanceReportData;
use App\Model\Union;
use App\Model\Upazila;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    private $view_path = "core.upazila.finance.";
    private $route_path = "";

    public function index( Request $request )
    {
      $old = [
        'starting_date' => '',
        'ending_date' => '',
        'union_id' => ''
      ];

      if($request->has('query'))
      {
        $old = [
          'starting_date'  => $request->input('starting_date'),
          'ending_date'    => $request->input('ending_date'),
          'union_id' => $request->input('union_id'),
          'query'          => $request->input('query')
        ];

        $d = Fhead::with('subhead.subItem')->get()->toArray();

        $type = "upid";
        $value = auth()->user()->upid;

        if(isset($request->union_id) && $request->union_id == "all"){
          $type = "upid";
          $value = auth()->user()->upid;
        }elseif(isset($request->union_id) && is_numeric($request->union_id)){
          $type = "unid";
          $value = $request->union_id;
        }else{
          \Session::flash('error', 'Invalid Request. Please choose correctly.');
          return redirect()->back();
        }

        $data  = FinanceReportData::getData($type,  $value, $request->input('starting_date'));
        $data2 = FinanceReportData::getData2($type, $value, $request->input('ending_date'));

        if(count($data)){
           $data= $data[0];
        }

        if(count($data2)){
           $data2= $data2[0];
        }
      }

      return view($this->view_path.'index', compact('old', 'data', 'd','data2'));
    }

    public function print(Request $request)
    {
      $upazila = Upazila::with('district')->where('id', \Auth::user()->upid)->first();


        $old = [
        'region_id' => '',
        'starting_date' => '',
        'ending_date' => '',
        'union_id' => ''
      ];

      if($request->has('query'))
      {
        $old = [
          'starting_date'  => $request->input('starting_date'),
          'ending_date'    => $request->input('ending_date'),
          'union_id' => $request->input('union_id'),
          'query'          => $request->input('query')
        ];

        $d = Fhead::with('subhead.subItem')->get()->toArray();

        $type = "upid";
        $value = auth()->user()->upid;

        if(isset($request->union_id) && $request->union_id == "all"){
          $type = "upid";
          $value = auth()->user()->upid;
        }elseif(isset($request->union_id) && is_numeric($request->union_id)){
          $type = "unid";
          $value = $request->union_id;
        }else{
          \Session::flash('error', 'Invalid Request. Please choose correctly.');
          return redirect()->back();
        }

        $data  = FinanceReportData::getData($type,  $value, $request->input('starting_date'));
        $data2 = FinanceReportData::getData2($type, $value, $request->input('ending_date'));

        if(count($data)){
           $data= $data[0];
        }
        if(count($data2)){
           $data2= $data2[0];
        }
      }
      return view($this->view_path.'print', compact('old', 'data', 'd','data2'));
    }
}
