<?php

namespace App\Http\Controllers\UpazilaAdmin;

use App\Http\Controllers\Controller;

class DashboardController extends Controller {

  private $view_path = "core.upazila.";

  public function dashboard()
  {
    return view($this->view_path.'dashboard');
  }
}