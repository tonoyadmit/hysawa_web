<?php

namespace App\Http\Controllers\UpazilaAdmin\Water;

use App\Http\Controllers\Controller;
use App\ImplementStatus;
use App\Model\Download\Water2Download;
use App\Model\Download\WaterDownload;
use App\Model\Search\Request\WaterSearchRequest;
use App\Model\Search\Water2Search;
use App\Model\Search\WaterSearch;
use App\Model\Upazila;
use App\Water;
use App\WaterTech;
use Illuminate\Http\Request;

class WaterController extends Controller
{
  private $view_path = "core.upazila.water.";
  private $route_path = "upazila-admin.water.";

  public function index( Request $request )
  {
    $old = [
      'starting_date' => '',
      'ending_date' => '',
      'created_by' => '',
      'ward_no' => '',
      'village' => '',
      'union_id' => '',
      'app_status' => '',
      'date_type' => '',
      'imp_status' => '',
      'Tw_no' => '',
      'CDF_no' => ''
    ];

    if($request->has('query'))
    {
      $old = [
        'query' => $request->input('query'),
        'starting_date' => $request->input('starting_date'),
        'ending_date' => $request->input('ending_date'),
        'created_by' => $request->input('created_by'),
        'ward_no' => $request->input('ward_no'),
        'village' => $request->input('village'),
        'union_id' => $request->input('union_id'),
        'app_status' => $request->input('app_status'),
        'date_type' => $request->input('date_type'),
        'imp_status' => $request->input('imp_status'),
        'Tw_no' => $request->input('Tw_no'),
        'CDF_no' => $request->input('CDF_no')
      ];
    }

    $request->request->add(['district_id' => \Auth::user()->distid]);
    $request->request->add(['upazila_id' => \Auth::user()->upid]);

    if($request->has('print'))
    {
      $waters = (new Water2Search($request))->get();
      if(!count($waters))
      {
        \Session::flash('error', 'Data Not Found.');
        return redirect()->back();
        return response()->json(['status' => 'error', 'message' => 'Data Not Found!']);
      }
      return view($this->view_path.'print', compact('waters', 'old'));
    }

    if($request->has('download'))
    {
      $v = (new Water2Search($request))->get();
      if(!count($v))
      {
        \Session::flash('error', 'Data Not Found.');
        return redirect()->back();
        return response()->json(['status' => 'error', 'message' => 'Data Not Found!']);
      }
      return (new Water2Download($v))->download();
    }

    $waters = (new Water2Search($request, true))->get();
    //dd($waters->toArray());
    return view($this->view_path.'index', compact('waters', 'old'));
  }

  public function edit($id)
  {
    $water = Water::
                    //->where('distid', \Auth::user()->distid)
                    //where('upid', \Auth::user()->upid)
                    findOrFail($id);;
    return view(
      $this->view_path.'edit',
      compact('water')
    );
  }

  public function update(Request $request, $id)
  {
    $water = Water::findOrFail($id);
    $water->fill($request->except('_token'));

    // if($water->created_at == null || $water->dist_approval_by != "")
    // {
    //   \Session::flash('errorMessage', "Invalid Data to Update.");
    //   return redirect()->route($this->route_path.'index');
    // }

    $water->up_approval_by = auth()->user()->id;
    $water->up_approval_at = date('Y-m-d H:i:s');

    $water->update();
    \Session::flash('message', "Updated successfully");
    return redirect()->route($this->route_path.'index');
  }

  public function download(Request $request)
  {
    $waterSearchRequest = new WaterSearchRequest;
    $waterSearchRequest->region_id = \Auth::user()->region_id;
    $waterSearchRequest->distid = \Auth::user()->distid;
    $waterSearchRequest->upid =  \Auth::user()->upid;

    $waterSearchRequest->unid = $request->input('union_id');
    $waterSearchRequest->ward_no = $request->input('ward_no');

    $waterSearchRequest->starting_date = $request->input('starting_date');
    $waterSearchRequest->ending_date = $request->input('ending_date');
    $waterSearchRequest->created_by = $request->input('created_by');
    $waterSearchRequest->village = $request->input('village');
    $waterSearchRequest->app_status = $request->input('app_status');

    $waterSearchRequest->date_type = $request->input('date_type');
    $waterSearchRequest->imp_status = $request->input('imp_status');
    $waterSearchRequest->Tw_no = $request->input('Tw_no');
    $waterSearchRequest->CDF_no = $request->input('CDF_no');

    return (new WaterDownload($waterSearchRequest))->download();
  }
}
