<?php

namespace App\Http\Controllers\UpazilaAdmin\Sanitation;

use App\AppStatus;
use App\Http\Controllers\Controller;
use App\ImplementStatus;
use App\Model\Download\Sanitation2Download;
use App\Model\Download\SanitationDownload;
use App\Model\Sanitation;
use App\Model\Search\Request\SanitationSearchRequest;
use App\Model\Search\Sanitation2Search;
use App\Model\Search\SanitationSearch;
use App\Model\Upazila;
use App\WaterSanitation;
use Illuminate\Http\Request;


class SanitationController extends Controller
{
  private $view_path = "core.upazila.sanitation.";
  private $route_path = "upazila-admin.sanitation.";

  public function index( Request $request )
  {
    $sanitations = "";

    $old = [
      'starting_date' => '',
      //'ending_date' => '',
      'created_by' => '',
      'village' => '',
      'upid' => \Auth::user()->upid,
      'app_status' => '',
      'imp_status' => '',
      'union_id' => 'all'
    ];

    if($request->has('query')){
      $old = [
        'query' => $request->input('query'),
        'starting_date' => $request->input('starting_date'),
        //'ending_date' => $request->input('ending_date'),
        'created_by' => $request->input('created_by'),
        'village' => $request->input('village'),
        'upid' => \Auth::user()->upid,
        'app_status'    => $request->input('app_status'),
        'imp_status'    => $request->input('imp_status'),
        'union_id' => $request->input('union_id')
      ];
    }

    $request->request->add(['upazila_id' => \Auth::user()->upid]);

    if($request->has('union_id') && $request->union_id != "all")
    {
      $request->request->add(['union_id' => $request->union_id]);
    }else{
      $request->request->add(['union_id' => '']);
    }

    if($request->has('print'))
    {
      $sanitations = (new Sanitation2Search($request))->get();
      if(!count($sanitations))
      {
        \Session::flash('error', 'Data Not Found.');
        return redirect()->back();
        return response()->json(['status' => 'error', 'message' => 'Data Not Found!']);
      }
      return view($this->view_path.'print', compact('sanitations', 'old'));
    }

    if($request->has('download'))
    {
      $v = (new Sanitation2Search($request))->get();
      if(!count($v))
      {
        \Session::flash('error', 'Data Not Found.');
        return redirect()->back();
        return response()->json(['status' => 'error', 'message' => 'Data Not Found!']);
      }
      return (new Sanitation2Download())->download($v);
    }

    $sanitations = (new Sanitation2Search($request, true))->get();
    return view($this->view_path.'index', compact('sanitations', 'old'));
  }

  public function show($id)
  {
    $water = WaterSanitation::findOrFail($id);
    return view($this->view_path.'view', compact('water'));
  }

  public function edit($id)
  {
    $mainype = array(
      'Institutional latrine' => 'Institutional latrine',
      'Public latrine' => 'Public latrine',
      'Communal latrine' => 'Communal latrine');

    $subtype = array(
      'Primary School' => 'Primary School',
      'High School' => 'High School',
      'Bazar' => 'Bazar',
      'Madrasha' => 'Madrasha',
      'Mosque' => 'Mosque',
      'Slum' => 'Slum',
      'Community' => 'Community');

    $wordOrderNO = array(
      '1' => '1',
      '2' => '2',
      '3' => '3',
      '4' => '4');

    $appstatus = AppStatus::all();
    $ImplementStatus=ImplementStatus::pluck('imp_status','imp_status')->toArray();

    $WaterSanitation = WaterSanitation::where('id', '=', $id)->first();
    return view($this->view_path.'edit', compact('subtype','appstatus','wordOrderNO','ImplementStatus','mainype','WaterSanitation'));
  }

  public function update(Request $request, $id)
  {
    $water = Sanitation::findOrFail($id);
    $water->fill($request->except('_token'));

//    if($water->created_at == null || $water->dist_approval_by != "")
//    {
//      \Session::flash('error', "Invalid Data to Update.");
//      return redirect()->route($this->route_path.'index');
//    }
    $water->app_date = date('Y-m-d');
    $water->up_approval_by = auth()->user()->id;
    $water->up_approval_at = date('Y-m-d H:i:s');

    $water->save();

    \Session::flash('message', "One updated successfully");
     return redirect()->route($this->route_path.'index');
  }

}
