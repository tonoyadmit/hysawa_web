<?php

namespace App\Http\Controllers\WaterQualityAgency\Water;

use App\Http\Controllers\Controller;
use App\Model\Search\WaterQualitySearch;
use App\Model\Water;
use App\Model\WaterQualityResult; 

use Illuminate\Http\Request;
use DB;


class WaterController extends Controller
{
  private $view_path = "core.water_quality_agency.water.";
  private $route_path = "water-quality-agency.water.";

  public function index( Request $request )
  {
      $old = [
        'region_id' => '',
        'district_id' => '',
        'upazila_id' => '',
        'union_id' => '',
        'TW_No' => '',
        'CDF_no' => ''
      ];

      if($request->has('query'))
      {
        $old = [
          'query' => $request->input('query'),
          'region_id' => $request->input('region_id'),
          'district_id' => $request->input('district_id'),
          'upazila_id' => $request->input('upazila_id'),
          'union_id' => $request->input('union_id'),
          'TW_No' => $request->input('TW_No'),
          'CDF_no' => $request->input('CDF_no')
        ];
      }

      $waters = (new WaterQualitySearch($request, true))->get();

      return view($this->view_path.'index', compact('waters', 'old'));
  }

  public function show(Request $request, $id)
  {
    $water = Water::with('region', 'district', 'upazila', 'union', 'qualityResults')->find($id);
    return view($this->view_path.'show', compact('water'));

    // $rowSpan = count($waters);

    // $str = "";
    // $str .= '<table class="table">';
    // $str .= '<tr>';
    //   $str .= '<th>Description</th>';
    //   $str .= '<th style="font-size: 12px; white-space: nowrap;" >Arsenic</th>';
    //   $str .= '<th style="font-size: 12px; white-space: nowrap;" >Iron (Fe)</th>';
    //   $str .= '<th style="font-size: 12px; white-space: nowrap;" >Mn</th>';
    //   $str .= '<th style="font-size: 12px; white-space: nowrap;" >Cl</th>';
    //   $str .= '<th style="font-size: 12px; white-space: nowrap;" >Ph</th>';
    //   $str .= '<th style="font-size: 12px; white-space: nowrap;" >Pb</th>';
    //   $str .= '<th style="font-size: 12px; white-space: nowrap;" >Zinc</th>';
    //   $str .= '<th style="font-size: 12px; white-space: nowrap;" >Fc</th>';
    //   $str .= '<th style="font-size: 12px; white-space: nowrap;" >Td</th>';
    //   $str .= '<th style="font-size: 12px; white-space: nowrap;" >Turbidity</th>';
    //   $str .= '<th>Action</th>';
    // $str .= '</tr>';

    // $first = false;

    // foreach($waters as $water)
    // {
    //   $str .= '<tr>';

    //     if($first == false)
    //     {
    //       $region = isset($water->region) ? $water->region->region_name : "";
    //       $district = isset($water->district) ? $water->district->distname : "";
    //       $upazila = isset($water->upazila) ? $water->upazila->upname : "";
    //       $union = isset($water->union) ? $water->union->unname : "";

    //       $str .= '<td rowspan="$rowSpan">';
    //         $str .= "Region : ". $region ;
    //         $str .= "District: ". $district ;
    //         $str .= "Upazila: ". $upazila ;
    //         $str .= "Union: ". $union ;
    //         $str .= "Word: ". $water->Ward_no;
    //         $str .= "Village: ". $water->Village ;
    //         $str .= "CDF No: ". $water->CDF_no ;
    //         $str .= "TubeWell No: ". $water->TW_No ;
    //         $str .= "APP Date: ". $water->App_date ;
    //         $str .= "Tend Lot: ". $water->Tend_lot ;
    //         $str .= "Technology_Type: ". $water->Technology_Type;
    //         $str .= "Landowner: ". $water->Landowner ;
    //         $str .= "Caretaker Male: ". $water->Caretaker_male ;
    //         $str .= "Caretaker Female: ". $water->Caretaker_female ;
    //       $str .= '</td>';

    //       $first = true;
    //     }

    //     $str .= '<td style="font-size: 12px;" >'.$water->wq_Arsenic.'</td>';
    //     $str .= '<td style="font-size: 12px;" >'.$water->wq_fe.'</td>';

    //     $str .= '<td style="font-size: 12px;" >'.$water->wq_mn.'</td>';
    //     $str .= '<td style="font-size: 12px;" >'.$water->wq_cl.'</td>';
    //     $str .= '<td style="font-size: 12px;" >'.$water->wq_ph.'</td>';
    //     $str .= '<td style="font-size: 12px;" >'.$water->wq_pb.'</td>';
    //     $str .= '<td style="font-size: 12px;" >'.$water->wq_zinc.'</td>';

    //     $str .= '<td style="font-size: 12px;" >'.$water->wq_fc.'</td>';
    //     $str .= '<td style="font-size: 12px;" >'.$water->wq_td.'</td>';
    //     $str .= '<td style="font-size: 12px;">'.$water->wq_turbidity.'</td>';
    //   $str .= '</tr>';
    // }
    // $str .= '</table>';
    // return $str;
  }


  // public function edit($id)
  // {
  //   $water = Water::where('id', '=', $id)
  //                   ->first();
  //   return view($this->view_path.'edit',compact('water'));
  // }



  // public function update(Request $request, $id)
  // {
  //   $water = Water::findOrFail($id);

  //   $newWater = $water->replicate();
  //   $newWater->fill($request->except('_token'));
  //   $newWater->created_by = auth()->user()->id;
  //   $newWater->created_at = date('Y-m-d H:i:s');

  //   dd($newWater->toArray());
  //   $newWater->create();

  //   \Session::flash('message', "Info added successfully");
  //   return redirect()->route($this->route_path.'index');
  // }

  public function store(Request $request, $id)
  {
    //dd($request->all());

    $water = Water::findOrFail($id);

    $wqa = new WaterQualityResult;
    $wqa->water_id = $water->id;
    $wqa->arsenic = $request->input('arsenic');
    $wqa->fe = $request->input('fe');
    $wqa->mn = $request->input('mn');
    $wqa->cl = $request->input('cl');
    $wqa->ph = $request->input('ph');
    $wqa->pb = $request->input('pb');
    $wqa->zinc = $request->input('zinc');
    $wqa->fc = $request->input('fc');
    $wqa->td = $request->input('td');
    $wqa->lat = $request->input('lat');
    $wqa->lon = $request->input('lon');
    $wqa->turbidity = $request->input('turbidity');
    $wqa->as_lab = $request->input('as_lab');
    $wqa->fe_lab = $request->input('fe_lab');
    $wqa->mn_lab = $request->input('mn_lab');
    $wqa->cl_lab = $request->input('cl_lab');
    $wqa->created_at = date('Y-m-d H:i:s');
    $wqa->created_by = auth()->user()->id;
    $wqa->report_date = $request->input('report_date');
    $wqa->save();

    return redirect()->route('water-quality-agency.water.show', $water->id);
  }

  public function edit(Request $request, $id)
  {
    $waterReport =$waterReport = WaterQualityResult::with('water.region', 'water.district', 'water.upazila', 'water.union')->find($id);
    $water = $waterReport->water; 
    return view($this->view_path.'edit', compact('waterReport', 'water'));
  }

  public function update(Request $request, $id)
  {
    $wqa = WaterQualityResult::findOrFail($id);
    $wqa->arsenic = $request->input('arsenic');
    $wqa->fe = $request->input('fe');
    $wqa->mn = $request->input('mn');
    $wqa->cl = $request->input('cl');
    $wqa->ph = $request->input('ph');
    $wqa->pb = $request->input('pb');
    $wqa->zinc = $request->input('zinc');
    $wqa->fc = $request->input('fc');
    $wqa->td = $request->input('td');
    $wqa->lat = $request->input('lat');
    $wqa->lon = $request->input('lon');
    $wqa->turbidity = $request->input('turbidity');
    $wqa->as_lab = $request->input('as_lab');
    $wqa->fe_lab = $request->input('fe_lab');
    $wqa->mn_lab = $request->input('mn_lab');
    $wqa->cl_lab = $request->input('cl_lab');
    $wqa->created_at = date('Y-m-d H:i:s');
    $wqa->created_by = auth()->user()->id;
    $wqa->report_date = $request->input('report_date');
    $wqa->update();
    return redirect()->route('water-quality-agency.water.show', $wqa->water->id);
  }
  
  public function printDetails(Request $request, $id)
  {
      $water = Water::with('region', 'district', 'upazila', 'union', 'qualityResults')->find($id);
      $waterReport = $water->qualityResults; 
     
      return view($this->view_path.'print_details', compact('water', 'waterReport')); 
  }
  
  public function print(Request $request, $id)
  {
      
  }

  public function printdata( Request $request )
  {
      $old = [  
        'region_id' => '',
        'district_id' => '',
        'upazila_id' => '',
        'union_id' => '',
        'TW_No' => '',
        'CDF_no' => ''
      ];

      if($request->has('query'))
      {
        $old = [
          'query' => $request->input('query'),
          'region_id' => $request->input('region_id'),
          'district_id' => $request->input('district_id'),
          'upazila_id' => $request->input('upazila_id'),
          'union_id' => $request->input('union_id'),
          'TW_No' => $request->input('TW_No'),
          'CDF_no' => $request->input('CDF_no')
        ];
      }

      // $water = (new WaterQualitySearch($request, true))->get();

      // return view($this->view_path.'printdata', compact('water', 'old'));


     // $water = Water::with('region', 'district', 'upazila', 'union', 'TW_No', 'CDF_no');
    //  $waters =  DB::select("select * from tbl_water where region_id ='".$request->input('region_id')."' OR distid='".$request->input('district_id')."' OR upid='".$request->input('upazila_id')."' OR unid='".$request->input('union_id')."' OR TW_No='".$request->input('TW_No')."' OR CDF_no='".$request->input('CDF_no')."'");
    if(!empty($request->input('region_id')) && !empty($request->input('district_id')) && !empty($request->input('upazila_id')) && !empty($request->input('union_id')) && !empty($request->input('App_date'))){
      $waters =  DB::select("select * from tbl_water where region_id ='".$request->input('region_id')."' AND distid='".$request->input('district_id')."' AND upid='".$request->input('upazila_id')."' AND unid='".$request->input('union_id')."' AND App_date='".$request->input('App_date')."'");
      $location =  DB::select("select * from tbl_water where region_id ='".$request->input('region_id')."' AND distid='".$request->input('district_id')."' AND upid='".$request->input('upazila_id')."' AND unid='".$request->input('union_id')."'
      order by id DESC limit 1
     ");
     }else if(!empty($request->input('region_id')) && !empty($request->input('district_id')) && !empty($request->input('upazila_id')) && !empty($request->input('union_id'))){
      $waters =  DB::select("select * from tbl_water where region_id ='".$request->input('region_id')."' AND distid='".$request->input('district_id')."' AND upid='".$request->input('upazila_id')."' AND unid='".$request->input('union_id')."'");
      $location =  DB::select("select * from tbl_water where region_id ='".$request->input('region_id')."' AND distid='".$request->input('district_id')."' AND upid='".$request->input('upazila_id')."' AND unid='".$request->input('union_id')."'
      order by id DESC limit 1
     ");
     }else if(!empty($request->input('region_id'))){
      $waters =  DB::select("select * from tbl_water where region_id ='".$request->input('region_id')."'");
      $location =  DB::select("select * from tbl_water where region_id ='".$request->input('region_id')."'
      order by id DESC limit 1
     ");
     }
     else if (!empty($request->input('CDF_no')) && !empty($request->input('TW_No')) ){
      $waters =  DB::select("select * from tbl_water where TW_No='".$request->input('TW_No')."' AND CDF_no='".$request->input('CDF_no')."'");
      $location = DB::select("select * from tbl_water where TW_No = '".$request->input('TW_No')."' AND TW_No='".$request->input('TW_No')."'
      order by id DESC limit 1
     ");
     }else  if(!empty($request->input('CDF_no'))) {
      $waters =  DB::select("select * from tbl_water
      where CDF_no='".$request->input('CDF_no')."'

      ");

      $location =  DB::select("select * from tbl_water where CDF_no = '".$request->input('CDF_no')."'
       order by id DESC limit 1
      ");

     } else if(!empty($request->input('TW_No'))){
      $waters =  DB::select("select * from tbl_water where id='".$request->input('TW_No')."'");
      $location = DB::select("select * from tbl_water where id = '".$request->input('TW_No')."'
      order by id DESC limit 1
     ");

     }

      return view($this->view_path.'printdata', compact('waters','old','location'));

  }
  
}
