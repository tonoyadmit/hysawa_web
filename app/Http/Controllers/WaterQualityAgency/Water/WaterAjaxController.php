<?php

namespace App\Http\Controllers\WaterQualityAgency\Water;

use App\Http\Controllers\Controller;
use App\ImplementStatus;
use App\Model\Download\WaterDownload;
use App\Model\Search\Request\WaterSearchRequest;
use App\Model\Search\WaterSearch;
use App\Water;
use App\WaterTech;
use Illuminate\Http\Request;

class WaterAjaxController extends Controller
{
    public function getUpazilas(Request $request )
    {
      if(!$request->ajax() || !$request->has('district_id'))
      {
        return ['status' => false];
      }

      $upazilas = \DB::table('fupazila')->where('disid', $request->input('district_id'))->get();
      $str = "";
      foreach($upazilas as $up)
      {
        $str .= '<option value="'.$up->id.'">'.$up->upname.'</option>';
      }

      // \Log::info($str);
      if($str != "")
      {
        $str = '<option value="">Choose an Option</option>'.$str;
      }

      return [
        'status' => true,
        'upazila_list' => $str
      ];
    }


    public function getUnions(Request $request)
    {
      if(!$request->ajax() || !$request->has('upazila_id'))
      {
        return ['status' => false];
      }

      $unions = \DB::table('funion')->where('upid', $request->input('upazila_id'))->get();
      $str = "";
      foreach($unions as $un)
      {
        $str .= '<option value="'.$un->id.'">'.$un->unname.'</option>';
      }

      if($str != "")
      {
        $str = '<option value="">Choose an Option</option '. $str;
      }

      return [
        'status' => true,
        'union_list' => $str
      ];
    }
}
