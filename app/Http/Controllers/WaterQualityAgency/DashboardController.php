<?php

namespace App\Http\Controllers\WaterQualityAgency;

use App\Http\Controllers\Controller;

class DashboardController extends Controller {

  private $view_path = "core.water_quality_agency.";

  public function dashboard()
  {
    return redirect()->route('water-quality-agency.water.index');
    return view($this->view_path.'dashboard');
  }
}