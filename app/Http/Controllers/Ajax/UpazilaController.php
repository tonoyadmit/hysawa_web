<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UpazilaController extends Controller
{
    public function index(Request $request )
    {
      if(!$request->ajax()){
        return ['status' => false];
      }

      $not_all = false;
      $choose_one = false;

      if($request->has('not_all') && $request->input('not_all') == 'true'){
        $not_all = true;
      }

      if($request->has('choose_one') && $request->input('choose_one') == 'true'){
        $choose_one = true;
      }

      $upazilas = \DB::table('fupazila')->get();

      $str = "";
      foreach($upazilas as $up){
        $str .= '<option value="'.$up->id.'">'.$up->upname.'</option>';
      }

      // if($str != ""){
      //   $str = '<option value="">Choose an Option</option>'.$str;
      // }

      if($str != "" && !$not_all){
        $str = '<option value="all" selected="selected">All</option>'. $str;
      }

      if($str != "" && $choose_one){
        $str = '<option value="" selected="selected">Chosse One Option</option>'. $str;
      }

      return ['status' => true, 'upazila_list' => $str];
    }

    public function fromDistrict(Request $request)
    {
      if(!$request->ajax() || !$request->has('district_id') )
      {
        return ['status' => false];
      }

      $not_all = false;
      $choose_one = false;

      if($request->has('not_all') && $request->input('not_all') == 'true')
      {
        $not_all = true;
      }

      if($request->has('choose_one') && $request->input('choose_one') == 'true')
      {
        $choose_one = true;
      }

      $upazilas = \DB::table('fupazila')->where('disid', $request->district_id)->get();

      $str = "";
      foreach($upazilas as $up){
        $str .= '<option value="'.$up->id.'">'.$up->upname.'</option>';
      }

      if($str != "" && !$not_all){
        $str = '<option value="all" selected="selected">All</option>'. $str;
      }

      if($str != "" && $choose_one)
      {
        $str = '<option value="" selected="selected">Chosse One Option</option>'. $str;
      }

      return ['status' => true, 'upazila_list' => $str];
    }
}
