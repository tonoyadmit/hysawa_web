<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DistrictController extends Controller
{
    public function index(Request $request )
    {
      if(!$request->ajax()){
        return ['status' => false];
      }

      $not_all = false;
      $choose_one = false;

      if($request->has('not_all') && $request->input('not_all') == 'true'){
        $not_all = true;
      }

      if($request->has('choose_one') && $request->input('choose_one') == 'true'){
        $choose_one = true;
      }

      $districts = \DB::table('fdistrict')->get();
      $str = "";
      foreach($districts as $district){
        $str .= '<option value="'.$district->id.'">'.$district->distname.'</option>';
      }

      // if($str != ""){
      //   $str = '<option value="">Choose an Option</option>'. $str;
      // }

      if($str != "" && !$not_all)
      {
        $str = '<option value="all" selected="selected">All</option>'. $str;
      }

      if($str != "" && $choose_one)
      {
        $str = '<option value="" selected="selected">Chosse One Option</option>'. $str;
      }

      return [
        'status' => true,
        'district_list' => $str
      ];
    }

    public function fromRegion(Request $request)
    {
      // \Log::info($request->all());
      if(!$request->ajax() || !$request->has('region_id'))
      {
        return ['status' => false];
      }

      $not_all = false;
      $choose_one = false;

      if($request->has('not_all') && $request->input('not_all') == 'true'){
        $not_all = true;
      }

      if($request->has('choose_one') && $request->input('choose_one') == 'true'){
        $choose_one = true;
      }


      $districts = \DB::table('fdistrict')->where('region_id', $request->input('region_id'))->get();
      $str = "";
      foreach($districts as $district)
      {
        $str .= '<option value="'.$district->id.'">'.$district->distname.'</option>';
      }

      if($str != "" && !$not_all)
      {
        $str = '<option value="all" selected="selected">All</option>'. $str;
      }

      if($str != "" && $choose_one)
      {
        $str = '<option value="" selected="selected">Chosse One Option</option>'. $str;
      }

      return [
        'status' => true,
        'district_list' => $str
      ];
    }
}
