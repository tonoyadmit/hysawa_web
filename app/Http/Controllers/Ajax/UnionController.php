<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UnionController extends Controller
{
    public function index(Request $request )
    {
      if(!$request->ajax())
      {
        return ['status' => false];
      }

      $not_all = false;
      $choose_one = false;

      if($request->has('not_all') && $request->input('not_all') == 'true')
      {
        $not_all = true;
      }

      if($request->has('choose_one') && $request->input('choose_one') == 'true')
      {
        $choose_one = true;
      }

      $unions = \DB::table('funion')->get();
      $str = "";
      foreach($unions as $un)
      {
        $str .= '<option value="'.$un->id.'">'.$un->unname.'</option>';
      }

      if($str != "" && !$not_all)
      {
        $str = '<option value="all" selected="selected">All</option>'. $str;
      }

      if($str != "" && $choose_one)
      {
        $str = '<option value="" selected="selected">Chosse One Option</option>'. $str;
      }

      return [
        'status' => true,
        'union_list' => $str
      ];
    }

    public function fromUpazila(Request $request)
    {

      if(!$request->ajax() || !$request->has('upazila_id'))
      {
        return ['status' => false];
      }

      $not_all = false;
      $choose_one = false;

      if($request->has('not_all') && $request->input('not_all') == 'true')
      {
        $not_all = true;
      }

      if($request->has('choose_one') && $request->input('choose_one') == 'true')
      {
        $choose_one = true;
      }

      $unions = \DB::table('funion')->where('upid', $request->input('upazila_id'))->get();
      $str = "";
      foreach($unions as $un)
      {
        $str .= '<option value="'.$un->id.'">'.$un->unname.'</option>';
      }

      if($str != "" && !$not_all){
        $str = '<option value="all" selected="selected">All</option>'. $str;
      }

      if($str != "" && $choose_one){
        $str = '<option value="" selected="selected">Chosse One Option</option>'. $str;
      }

      return [
        'status' => true,
        'union_list' => $str
      ];

    }
}
