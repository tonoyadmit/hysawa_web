<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class RegionController extends Controller
{
    public function index(Request $request )
    {
      $not_all = false;
      $choose_one = false;

      if($request->has('not_all') && $request->input('not_all') == 'true')
      {
        $not_all = true;
      }

      if($request->has('choose_one') && $request->input('choose_one') == 'true')
      {
        $choose_one = true;
      }

      if(!$request->ajax())
      {
        return ['status' => false];
      }

      $regions = \DB::table('region')->get();
      $str = "";
      foreach($regions as $region)
      {
        $str .= '<option value="'.$region->region_id.'">'.$region->region_name.'</option>';
      }

      // if($str != "")
      // {
      //   $str = '<option value="">Choose an Option</option>'.$str;
      // }

      if($str != "" && !$not_all)
      {
        $str = '<option value="all" selected="selected">All</option>'. $str;
      }

      if($str != "" && $choose_one)
      {
        $str = '<option value="" selected="selected">Chosse One Option</option>'. $str;
      }


      return [
        'status' => true,
        'region_list' => $str
      ];
    }

    public function fromProject(Request $request)
    {
      if(!$request->ajax())
      {
        return ['status' => false];
      }

      $not_all = false;
      $choose_one = false;

      if($request->has('not_all') && $request->input('not_all') == 'true')
      {
        $not_all = true;
      }

      if($request->has('choose_one') && $request->input('choose_one') == 'true')
      {
        $choose_one = true;
      }

      $regions = \DB::table('tbl_water')
            ->where('tbl_water.region_id', '!=', "")
            ->where('tbl_water.region_id', '!=', 0)
            ->where('tbl_water.proj_id', $request->project_id)
            ->select('region.region_id', 'region.region_name')
            ->leftjoin('region','region.region_id','=','tbl_water.region_id')
            ->groupBy('tbl_water.region_id')
            ->get();

      $str = "";

      foreach($regions as $region){
        $str .= '<option value="'.$region->region_id.'">'.$region->region_name.'</option>';
      }

      // if($str != "")
      // {
      //   $str = '<option value="">Choose an Option</option>'. $str;
      // }

      if($str != "" && !$not_all)
      {
        $str = '<option value="all" selected="selected">All</option>'. $str;
      }

      if($str != "" && $choose_one)
      {
        $str = '<option value="" selected="selected">Chosse One Option</option>'. $str;
      }

      return [
        'status' => true,
        'region_list' => $str
      ];
    }
}
