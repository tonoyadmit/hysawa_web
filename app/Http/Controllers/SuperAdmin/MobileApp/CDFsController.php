<?php

namespace App\Http\Controllers\SuperAdmin\MobileApp;

use App\Http\Controllers\Controller;
use App\Model\CDFNo;
use Illuminate\Http\Request;

class CDFsController extends Controller {

  private $view_path = "core.superadmin.cdf.";
  private $route_path = "superadmin.cdfs.";


  public function index()
  {
    $cdfs = CDFNo::orderBy('created_at', 'DESC')->paginate(40);
    return view($this->view_path.'index', compact('cdfs'));
  }

  public function store(Request $request)
  {

    if(!$request->has('cdf_no')){
      \Session::flash('error', "CDF Not Found.");
      return redirect()->back();
    }

    if($request->type == "cdf")
    {
      if( !( $request->has('owner') && $request->input('owner') != "") )
      {
        \Session::flash('error', "Owner Info missing");
        return redirect()->back();
      }

      if( !( $request->has('word_no') && $request->input('word_no') != "") )
      {
        \Session::flash('error', "Word Info missing");
        return redirect()->back();
      }

    }else
    {
      if( !( $request->has('school_title') && $request->input('school_title') != "") )
      {
        \Session::flash('error', "School Title is missing");
        return redirect()->back();
      }
    }


    try{

    //   $cdf = CDFNo::where('cdf_no', $request->cdf_no)->get();

    //   if(count($cdf))
    //   {
    //     \Session::flash('error', "This CDF is already exists.");
    //     return redirect()->route($this->route_path.'index');
    //   }

      CDFNo::insert([
        'cdf_no' => $request->input('cdf_no'),
        'project_id' => $request->input('project_id'),
        'region_id' => $request->input('region_id'),
        'district_id' => $request->input('district_id'),
        'upazila_id' => $request->input('upazila_id'),
        'union_id' => $request->input('union_id'),
        'village' => $request->input('village'),
        'area' => $request->input('area'),
        'created_by' => auth()->user()->id,
        'updated_by' => auth()->user()->id,
        'created_at' => date('Y-m-d H:i:s'),
        'type' => $request->input('type'),
        'school_title' => $request->input('school_title'),
        'word_no' => $request->input('word_no'),
        'owner' => $request->input('owner')
      ]);

      \Session::flash('success', "New CDF info added");
      return redirect()->route($this->route_path."index");
    }catch(\Exception $e){
      \Log::critical($e->getMessage());

      \Session::flash('error', "Something Went wrong. Please Try Again.");
      return redirect()->route($this->route_path."index");
    }
  }

  public function edit(Request $request, $id)
  {
    $cdf = CDFNo::with('district', 'upazila', 'union')->find($id);

    if(!count($cdf))
    {
      \Session::flash('error', "CDF Not Found.");
      return redirect()->route($this->route_path.'index');
    }

    return view($this->view_path.'edit', compact('cdf'));
  }

  public function update(Request $request, $id)
  {

    $cdf = CDFNo::find($id);

    if(!count($cdf)){
      \Session::flash('error', "CDF Not Found.");
      return redirect()->route($this->route_path.'index');
    }

    if($request->type == "cdf")
    {
      if( !( $request->has('owner') && $request->input('owner') != "") )
      {
        \Session::flash('error', "Owner Info missing");
        return redirect()->back();
      }

      if( !( $request->has('word_no') && $request->input('word_no') != "") )
      {
        \Session::flash('error', "Word Info missing");
        return redirect()->back();
      }

    }else
    {
      if( !( $request->has('school_title') && $request->input('school_title') != "") )
      {
        \Session::flash('error', "School Title is missing");
        return redirect()->back();
      }
    }

    try{

      CDFNo::where('id', $id)
        ->update([
          'cdf_no' => $request->input('cdf_no'),
          'project_id' => $request->input('project_id'),
          'region_id' => $request->input('region_id'),
          'district_id' => $request->input('district_id'),
          'upazila_id' => $request->input('upazila_id'),
          'union_id' => $request->input('union_id'),
          'village' => $request->input('village'),
          'area' => $request->input('area'),
          'updated_by' => auth()->user()->id,
          'updated_at' => date('Y-m-d H:i:s'),

          'type' => $request->input('type'),
          'school_title' => $request->input('school_title'),
          'word_no' => $request->input('word_no'),
          'owner' => $request->input('owner')
        ]);

        \Session::flash('message', "CDF Info Updated");
        return redirect()->route($this->route_path."index");

    }catch(\Exception $e){
      \Log::critical($e->getMessage());
      \Session::flash('error', "Something went Wrong! ");
      return redirect()->route($this->route_path."index");
    }
  }
}