<?php

namespace App\Http\Controllers\SuperAdmin\MobileApp;

use App\Http\Controllers\Controller;
use App\Model\MobAppDataList;
use App\Model\Search\MobAppDataListSearch;
use Illuminate\Http\Request;
use DB;

class MobileAppController extends Controller {

  private $view_path = "core.superadmin.mobile-app.";
  private $route_path = "superadmin.mobile-app.";

  public function index(Request $request)
  {
    if($request->has('print'))
    {
      // $datas = MobAppDataList::with('union.upazila.district')
      //             ->with('items.question')
      //             ->with('user')
      //             //->where('upid', auth()->user()->upid)
      //             ->orderBy('created_at', 'DESC')
      //             ->get();
      $datas = (new MobAppDataListSearch($request, false))->get();
      return view($this->view_path.'print',compact('datas'));
    }


    $old = [
      'project_id'   => '',
      'region_id'    => '',
      'district_id'  => '',
      'upazila_id'   => '',
      'union_id'     => '',
      'starting_date' => '',
      'ending_date'  => '',
      'cdfno'        => '',
      'type'         => '',
      'user_id'      => ''
    ];

    if($request->has('query'))
    {
      $old = [
        'query'        => $request->input('query'),
        'project_id'   => $request->input('project_id'),
        'region_id'    => $request->input('region_id'),
        'district_id'  => $request->input('district_id'),
        'upazila_id'   => $request->input('upazila_id'),
        'union_id'     => $request->input('union_id'),
        'starting_date' => $request->input('starting_date'),
        'ending_date'  => $request->input('ending_date'),
        'cdfno'        => $request->input('cdfno'),
        'type'         => $request->input('type'),
        'user_id'      => $request->input('user_id')
      ];
    }

    $datas = (new MobAppDataListSearch($request, true))->get();
    return view($this->view_path.'index', compact('datas', 'old'));

  }

  public function show($id)
  {
    $data = MobAppDataList::with('union.upazila.district')
                  ->with('items.question')
                  ->with('user')
                  ->find($id);
                   //dd($data->toArray());
    return view($this->view_path.'show',compact('data'));
  }
  
    public function getUserRep(Request $request)
  {
    $user = \Auth::user();
    $user->load('roles');

    if ($request->has('query')) {

      if(!empty($request->project_id) && !empty($request->region_id) && !empty($request->starting_date) && !empty($request->ending_date)){
               
$Submittedrep = DB::select("SELECT users.id, users.type, users.distid, users.upid, users.unid, users.proj_id, users.region_id, users.email, COUNT(*) AS Total FROM users 
INNER JOIN mob_app_data_list 
ON users.id = mob_app_data_list.user_id
WHERE mob_app_data_list.proj_id=".$request->project_id." and mob_app_data_list.region_id = ".$request->region_id." and mob_app_data_list.created_at BETWEEN '".$request->starting_date."' AND '".$request->ending_date."'
GROUP BY users.id
");

$NotSubmittedMonth = DB::select("SELECT users.id, users.type, users.distid, users.upid, users.unid, users.proj_id, users.region_id, users.email, COUNT(*) AS Total FROM users 
INNER JOIN mob_app_data_list 
ON users.id = mob_app_data_list.user_id
WHERE mob_app_data_list.proj_id=".$request->project_id." and mob_app_data_list.region_id = ".$request->region_id." 
GROUP BY users.id
");

$Unsubmitted = DB::select("SELECT DISTINCT users.id, users.type, users.distid, users.upid, users.unid, users.proj_id, users.region_id, users.email 
FROM users
WHERE users.unid != 0 and users.unid IS NOT NULL and users.region_id=".$request->region_id." and users.proj_id = ".$request->project_id." and users.id NOT IN (select DISTINCT mob_app_data_list.user_id from mob_app_data_list
where mob_app_data_list.proj_id=".$request->project_id." and mob_app_data_list.region_id=".$request->region_id.")
");

}else if(!empty($request->project_id) && !empty($request->region_id)){
               
  // $Submittedrep = DB::select("SELECT users.id, users.type, users.distid, users.upid, users.unid, users.proj_id, users.region_id, users.email, COUNT(*) AS Total FROM users 
  // INNER JOIN mob_app_data_list 
  // ON users.id = mob_app_data_list.user_id
  // WHERE mob_app_data_list.proj_id=".$request->project_id." and mob_app_data_list.region_id = ".$request->region_id."
  // GROUP BY users.id
  // ");
  
  $NotSubmittedMonth = DB::select("SELECT users.id, users.type, users.distid, users.upid, users.unid, users.proj_id, users.region_id, users.email, COUNT(*) AS Total FROM users 
  INNER JOIN mob_app_data_list 
  ON users.id = mob_app_data_list.user_id
  WHERE mob_app_data_list.proj_id=".$request->project_id." and mob_app_data_list.region_id = ".$request->region_id." 
  GROUP BY users.id
  ");
  
  $Unsubmitted = DB::select("SELECT DISTINCT users.id, users.type, users.distid, users.upid, users.unid, users.proj_id, users.region_id, users.email 
  FROM users
  WHERE users.unid != 0 and users.unid IS NOT NULL and users.region_id=".$request->region_id." and users.proj_id = ".$request->project_id." and users.id NOT IN (select DISTINCT mob_app_data_list.user_id from mob_app_data_list
  where mob_app_data_list.proj_id=".$request->project_id." and mob_app_data_list.region_id=".$request->region_id.")
  ");
  
  }


$old = [
        'district_id' => '',
        'project_id'   => '',
        'upazila_id'  => '',
        'union_id'    => '',
      ];

if ($request->has('query')) {
$old = [
        'query'       => $request->input('query'),
        'project_id'   => $request->project_id,
        'region_id' => $request->region_id,
        'starting_date'  => $request->starting_date,
        'ending_date'    => $request->ending_date
      ];
}
    }else{

      
  
    }


   



    return view($this->view_path.'userrep', compact('Submittedrep', 'old','Unsubmitted','NotSubmittedMonth'));

  }



}