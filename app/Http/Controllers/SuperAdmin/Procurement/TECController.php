<?php

namespace App\Http\Controllers\SuperAdmin\Procurement;

use App\Http\Controllers\Controller;
use App\Model\District;
use App\Model\Download\Superadmin\Procurement\ProcurementDownload;
use App\Model\Download\Superadmin\Procurement\TECDownload;
use App\Model\Search\TECSearch;
use Illuminate\Http\Request;

class TECController extends Controller
{
    private $view_path = "core.superadmin.procurement.tec.";
    private $route_path = "superadmin.procurement.tec.";

    public function index(Request $request)
    {
      $old = [
          'district_id' => '',
          'project_id'   => '',
          'upazila_id'  => '',
          'union_id'    => '',
        ];

      if($request->has('query'))
      {
        $old = [
          'query'       => $request->input('query'),
          'project_id'   => $request->input('project_id'),
          'district_id' => $request->input('district_id'),
          'upazila_id'  => $request->input('upazila_id'),
          'union_id'    => $request->input('union_id')
        ];
      }

      $tecs = (new TECSearch($request, true))->get();

      //dd($tecs->toArray());

      return view($this->view_path.'index', compact('tecs', 'old'));
    }

    public function download(Request $request)
    {
      return (new TECDownload($request))->download();
    }

    public function print(Request $request)
    {
      $tecs = (new TECSearch($request))->get();
       return view($this->view_path.'print', compact('tecs', 'old'));
    }
}
