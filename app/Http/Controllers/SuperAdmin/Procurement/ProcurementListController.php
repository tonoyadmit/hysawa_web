<?php

namespace App\Http\Controllers\SuperAdmin\Procurement;

use App\Http\Controllers\Controller;
use App\Model\Download\Superadmin\Procurement\ProcurementListDownload;
use App\Model\Search\ProcurementListSearch;
use Illuminate\Http\Request;

class ProcurementListController extends Controller
{
    private $view_path = "core.superadmin.procurement.procurement_list.";

    public function index(Request $request)
    {
      $old = [
        'district_id' => '',
        'project_id'   => '',
        'upazila_id'  => '',
        'union_id'    => '',
      ];

      if($request->has('query'))
      {
        $old = [
          'query'       => $request->input('query'),
          'project_id'   => $request->input('project_id'),
          'district_id' => $request->input('district_id'),
          'upazila_id'  => $request->input('upazila_id'),
          'union_id'    => $request->input('union_id')
        ];
      }

      $lists = (new ProcurementListSearch($request, true))->get();
      return view($this->view_path.'index', compact('lists', 'old'));
    }

    public function download(Request $request)
    {
      return (new ProcurementListDownload($request))->download();
    }

    public function print(Request $request)
    {
      $lists = (new ProcurementListSearch($request))->get();

      //dd($lists->toArray());
      return view($this->view_path.'print', compact('lists', 'old'));
    }
}
