<?php

namespace App\Http\Controllers\SuperAdmin\Procurement;

use App\Http\Controllers\Controller;
use App\Model\Download\Superadmin\Procurement\PECDownload;
use App\Model\Search\PECSearch;
use Illuminate\Http\Request;

class PECController extends Controller
{
    private $view_path = "core.superadmin.procurement.pec.";
    private $route_path = "superadmin.procurement.pec.";

    public function index(Request $request)
    {
      $old = [
        'district_id' => '',
        'project_id'   => '',
        'upazila_id'  => '',
        'union_id'    => '',
      ];

      if($request->has('query'))
      {
        $old = [
          'query'       => $request->input('query'),
          'project_id'   => $request->input('project_id'),
          'district_id' => $request->input('district_id'),
          'upazila_id'  => $request->input('upazila_id'),
          'union_id'    => $request->input('union_id')
        ];
      }

      $pecs = (new PECSearch($request, true))->get();

      //dd($pecs->toArray());

      return view($this->view_path.'index', compact('pecs', 'old'));
    }

    public function download(Request $request)
    {
      return (new PECDownload($request))->download();
    }

    public function print(Request $request)
    {
      $pecs = (new PECSearch($request))->get();
       return view($this->view_path.'print', compact('pecs', 'old'));
    }
}
