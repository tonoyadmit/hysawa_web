<?php

namespace App\Http\Controllers\SuperAdmin\Procurement;

use App\Http\Controllers\Controller;
use App\Model\Download\Superadmin\Procurement\ProcurementEvaluationDownload;
use App\Model\Search\ProcurementEvaluationSearch;
use Illuminate\Http\Request;

class ProcurementEvaluationController extends Controller
{
    private $view_path = "core.superadmin.procurement.procurement_evaluation.";

    public function index(Request $request)
    {
      $old = [
        'district_id' => '',
        'project_id'   => '',
        'upazila_id'  => '',
        'union_id'    => '',
      ];

      if($request->has('query'))
      {
        $old = [
          'query'       => $request->input('query'),
          'project_id'   => $request->input('project_id'),
          'district_id' => $request->input('district_id'),
          'upazila_id'  => $request->input('upazila_id'),
          'union_id'    => $request->input('union_id')
        ];
      }

      $lists = (new ProcurementEvaluationSearch($request, true))->get();
      return view($this->view_path.'index', compact('lists', 'old'));
    }

    public function download(Request $request)
    {
      return (new ProcurementEvaluationDownload($request))->download();
    }

    public function print(Request $request)
    {
      $lists = (new ProcurementEvaluationSearch($request))->get();
      return view($this->view_path.'print', compact('lists', 'old'));
    }
}
