<?php

namespace App\Http\Controllers\SuperAdmin\Procurement;

use App\Http\Controllers\Controller;
use App\Model\District;
use App\Model\Download\Superadmin\Procurement\ProcurementDownload;
use App\Model\Search\ContractorSearch;
use Illuminate\Http\Request;

class ContractorController extends Controller
{
    private $view_path = "core.superadmin.procurement.contractor.";
    private $route_path = "superadmin.procurement.contractors.";

    public function index(Request $request)
    {
      if($request->has('download'))
      {
        return (new ProcurementDownload)->download();
      }

      $old = [
          'district_id' => '',
          'category'    => '',
          'con_name'    => ''
        ];

      if($request->has('query'))
      {
        $old = [
          'query'       => $request->input('query'),
          'district_id' => $request->input('district_id'),
          'category'    => $request->input('category'),
          'con_name'    => $request->input('con_name')
        ];
      }

      $contractors = (new ContractorSearch($request, true))->get();

      return view($this->view_path.'index', compact('contractors', 'old'));
    }

    public function create()
    {
      $districts = District::orderBy('distname')->get();
      return view($this->view_path.'create', compact('districts'));
    }

    public function store(Request $request)
    {
      \DB::table('procurement')->insert([
        'distid' => $request->input('distid'),
        'con_name' => $request->input('name'),
        'con_add' => $request->input('address'),
        'contact' => $request->input('contact'),
        'phone' => $request->input('phone'),
        'category' => $request->input('cont_type'),
        'remarks' => $request->input('remark'),
      ]);

      \Session::flash('message', "New Data saved successfully");
      return redirect()->route($this->route_path."index");
    }


    public function edit($id)
    {
      $districts = District::orderBy('distname')->get();
      $contractor = \DB::table('procurement')->where('id', $id)->first();
      return view($this->view_path.'edit', compact('districts', 'contractor'));
    }


    public function update(Request $request, $id)
    {
      \DB::table('procurement')->where('id', $id)
        ->update([
          'distid' => $request->input('distid'),
          'con_name' => $request->input('name'),
          'con_add' => $request->input('address'),
          'contact' => $request->input('contact'),
          'phone' => $request->input('phone'),
          'category' => $request->input('cont_type'),
          'remarks' => $request->input('remark'),
        ]);

      \Session::flash('message', "Data Updated");
      return redirect()->route($this->route_path."index");
    }
}
