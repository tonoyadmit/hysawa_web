<?php

namespace App\Http\Controllers\SuperAdmin\WaterSanitation;

use App\AppStatus;
use App\Http\Controllers\Controller;
use App\ImplementStatus;
use App\Model\Download\SanitationDownload;
use App\Model\Search\SanitationSearch;
use App\WaterSanitation;
use Illuminate\Http\Request;


class WaterSanitationController extends Controller
{
  private $view_path = "core.superadmin.sanitation.";
  private $route_path = "superadmin.sanitations.";

  public function index( Request $request )
  {
    $sanitations = WaterSanitation::take(100)->get();

    $old = [
      'starting_date' => '',
      'ending_date' => '',
      'created_by' => '',
      'village' => ''
    ];

    return view($this->view_path.'index', compact('sanitations', 'old'));
  }

  public function search(Request $request)
  {
    $sanitations = [];

    if($request->has('query')){
      $sanitations = (new SanitationSearch($request))->get();
    }else{
      return redirect()->route('water.index');
    }

    $old = [
    'query' => $request->input('query'),
    'starting_date' => $request->input('starting_date'),
    'ending_date' => $request->input('ending_date'),
    'created_by' => $request->input('created_by'),
    'village' => $request->input('village'),
    'download' => time()
    ];

    return view($this->view_path.'index', compact('sanitations', 'old'));
  }

  public function download(Request $request)
  {
    if($request->has('download'))
    {
      return (new SanitationDownload($request))->download();
    }
  }

  public function create()
  {
    $mainype = array(
      'Institutional latrine' => 'Institutional latrine',
      'Public latrine' => 'Public latrine',
      'Communal latrine' => 'Communal latrine');

    $subtype = array(
      'Primary School' => 'Primary School',
      'High School' => 'High School',
      'Bazar' => 'Bazar',
      'Madrasha' => 'Madrasha',
      'Mosque' => 'Mosque',
      'Slum' => 'Slum',
      'Community' => 'Community');

    $wordOrderNO = array('1' => '1','2' => '2','3' => '3','4' => '4');
    $appstatus = AppStatus::pluck('app_status','id')->toArray();
    $ImplementStatus=ImplementStatus::pluck('imp_status','id')->toArray();

    return view($this->view_path.'insert', compact('subtype','appstatus','wordOrderNO','ImplementStatus','mainype'));
  }

  public function store(Request $request)
  {
    $water = new WaterSanitation();
    $water->fill($request->except('_token'));

    $water->unid      = auth()->user()->unid;
    $water->region_id = auth()->user()->region_id;
    $water->proj_id   = auth()->user()->proj_id;
    $water->dist_id   = auth()->user()->distid;
    $water->upid      = auth()->user()->upid;

    $water->created_by = auth()->user()->id;
    $water->updated_by = auth()->user()->id;
    $water->save();

    Session::flash('message', "New Data saved successfully");
    return redirect($this->route_path.'create');
  }

  public function show($id)
  {
    $water = WaterSanitation::findOrFail($id);
    return view($this->view_path.'view', compact('water'));
  }

  public function edit($id)
  {
    $mainype = array('Institutional latrine' => 'Institutional latrine','Public latrine' => 'Public latrine','Communal latrine' => 'Communal latrine');
    $subtype = array('Primary School' => 'Primary School','High School' => 'High School','Bazar' => 'Bazar','Madrasha' => 'Madrasha','Mosque' => 'Mosque','Slum' => 'Slum','Community' => 'Community');
    $wordOrderNO = array('1' => '1','2' => '2','3' => '3','4' => '4');
    $appstatus = AppStatus::pluck('app_status','id')->toArray();
    $ImplementStatus=ImplementStatus::pluck('imp_status','id')->toArray();

    $WaterSanitation = WaterSanitation::where('id', '=', $id)->first();
    return view($this->view_path.'edit', compact('subtype','appstatus','wordOrderNO','ImplementStatus','mainype','WaterSanitation'));
  }

  public function update(Request $request, $id)
  {
    $water = WaterSanitation::findOrFail($id);
    $water->fill($request->except('_token'));
    $water->updated_by = auth()->user()->id;
    $water->save();

    \Session::flash('message', "One updated successfully");
    return redirect($this->route_path.'index');
  }

}
