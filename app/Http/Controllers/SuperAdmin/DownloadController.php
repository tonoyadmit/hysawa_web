<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;

class DownloadController extends Controller {

  private $view_path = "core.superadmin.download.";

  public function index()
  {
    return view($this->view_path.'index');
  }
}