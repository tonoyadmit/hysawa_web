<?php

namespace App\Http\Controllers\SuperAdmin\Location;

use App\Http\Controllers\Controller;
use App\Model\District;
use App\Model\Search\LocationDistrictSearch;
use Illuminate\Http\Request;

class DistrictController extends Controller {

  private $view_path = "core.superadmin.location.district.";
  private $route_path = "superadmin.location.district.";

  public function index(Request $request)
  {
    try{
      $old = [
        'region_id'    => '',
        'distname'       => ''
      ];

      if($request->has('query'))
      {
        $old = [
          'query'       => $request->input('query'),
          'region_id'   => $request->input('region_id'),
          'distname'      => $request->input('distname')
        ];
      }

      $datas = (new LocationDistrictSearch($request, true))->get();
      return view($this->view_path.'index', compact('datas', 'old'));

    }catch(\Exception $e)
    {
      \Log::critical($e->getMessage());
      \Session::flash('error', 'Something Went wrong.');
    }
  }

  public function create(Request $request)
  {
    return view($this->view_path.'create');
  }

  public function store(Request $request)
  {
    try{

      $p = new District;
      $p->distname = $request->input('distname');
      $p->region_id = $request->input('region_id');
      $p->distcode = $request->input('distcode');
      $p->created_by = auth()->user()->id;
      $p->save();

      \Session::flash('success', "District Added.");
      return redirect()->route($this->route_path.'index');

    }catch(\Exception $e)
    {
      \Log::critical($e->getMessage());
      \Session::flash('error', "Something went wrong.");
      return redirect()->back();
    }

  }

  public function edit(Request $request, $id)
  {
    try{
      $district = District::findOrFail($id);
      return view($this->view_path.'edit', compact('district'));

    }catch(\Exception $e)
    {
      \Log::critical($e->getMessage());
      \Session::flash('error', "District Not Found.");
      return redirect()->back();
    }
  }

  public function update(Request $request, $id)
  {
    try{
      $district = District::findOrFail($id);

      $district->distname = $request->input('distname');
      $district->region_id = $request->input('region_id');
      $district->distcode = $request->input('distcode');
      $district->updated_by = auth()->user()->id;

      $district->update();

      \Session::flash('success', "District Updated.");
      return redirect()->route($this->route_path.'index');

    }catch(\Exception $e)
    {
      \Log::critical($e->getMessage());
      \Session::flash('error', "District Not Found.");
      return redirect()->back();
    }
  }

}