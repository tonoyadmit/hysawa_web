<?php

namespace App\Http\Controllers\SuperAdmin\Location;

use App\Http\Controllers\Controller;
use App\Model\Search\LocationUnionSearch;
use App\Model\Union;
use Illuminate\Http\Request;

class UnionController extends Controller {

  private $view_path = "core.superadmin.location.union.";
  private $route_path = "superadmin.location.union.";

  public function index(Request $request)
  {

    $old = [
      'project_id'   => '',
      'region_id'    => '',
      'district_id'  => '',
      'upazila_id'   => '',
      'unname'       => ''
    ];

    if($request->has('query'))
    {
      $old = [
        'query'       => $request->input('query'),
        'project_id'  => $request->input('project_id'),
        'region_id'   => $request->input('region_id'),
        'district_id' => $request->input('district_id'),
        'upazila_id'  => $request->input('upazila_id'),
        'unname'      => $request->input('unname')
      ];
    }

    $datas = (new LocationUnionSearch($request, true))->get();

    //dd($datas->toArray());

    return view($this->view_path.'index', compact('datas', 'old'));
  }

  public function create(Request $request)
  {
    return view($this->view_path.'create');
  }

  public function store(Request $request)
  {
    //dd($request->all());
    $union = new Union;
    //'uncode', 'distid', 'upid', 'proid', 'unname', 'region_id'
    $union->unname = $request->input('unname');
    $union->uncode = "";
    $union->proid = $request->input('project_id');
    $union->region_id = $request->input('region_id');
    $union->distid = $request->input('district_id');
    $union->upid = $request->input('upazila_id');

    $union->save();

    return redirect()->route($this->route_path.'index');
  }

  public function edit(Request $request, $id)
  {
    $union = Union::with('region', 'project', 'upazila.district')->findOrFail($id);
    return view($this->view_path.'edit', compact('union'));
  }

  public function update(Request $request, $id)
  {
    // dd($request->all());
    //'uncode', 'distid', 'upid', 'proid', 'unname', 'region_id'
    $union = Union::findOrFail($id);
    $union->unname = $request->input('unname');
    $union->proid = $request->input('project_id');
    $union->region_id = $request->input('region_id');
    $union->distid = $request->input('district_id');
    $union->upid = $request->input('upazila_id');
    $union->update();
    return redirect()->route($this->route_path.'index');
  }

  public function destroy($id)
  {
    return redirect()->route($this->route_path.'index');
  }
}