<?php

namespace App\Http\Controllers\SuperAdmin\Monitoring;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Model\Download\Superadmin\Project\Sanitation\FieldVisitDownload;
use DB;
use Illuminate\Http\Request;
use Image;

class ReportController extends Controller {

  private $view_path = "core.superadmin.monitoring.report.";
  private $route_path = "superadmin.monitoring.upcapacity.reportlist.";

  public function index(Request $request)
  {
    if($request->has('download'))
    {
      return (new FieldVisitDownload)->download();
    }
    $tours = DB::table('tour')->get()->toArray();
    return view($this->view_path.'index',compact('tours'));
  }

  public function create()
  {
    $district=DB::table('fdistrict')->pluck('distname','id')->toArray();
    return view($this->view_path.'insert', compact('district'));
  }

  public function show($id)
  {
    $info=DB::table('tour')->where('id',$id)->first();
    return view($this->view_path.'view',compact('info'));
  }

    public function store(Request $request)
    {
        print_r($request->file('anex'));exit;
      $data=$request->except('_token','_method','anex','distid','upid','_wysihtml5_mode');
      $data["updated_by"]=auth()->user()->id;
      $data["created_by"]=auth()->user()->id;
      if($request->hasFile('anex')) {
            echo $extension = $request->file('anex')->getClientOriginalExtension();exit;
            $fileName = $id.'.'.$extension;
            $url = 'uploads/users/';

            $img = Image::make($request->file('anex'))->resize(200, 200)->save($url.$fileName);
            // return env('APP_URL').$url.$fileName;
            $data["anex"] = env('APP_URL').$url.$fileName;
        }


      $count = \DB::table('tour')->insert($data);

      if($count){
        \Session::flash('message', "New Data saved successfully");
        return redirect()->route($this->route_path.'index');

      }
      \Session::flash('message', "Try Again");
      return redirect()->route($this->route_path.'create');
    }


    public function download()
    {
      return (new FieldVisitDownload)->download();
    }

}