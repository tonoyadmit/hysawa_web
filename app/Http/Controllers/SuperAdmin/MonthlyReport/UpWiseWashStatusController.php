<?php

namespace App\Http\Controllers\SuperAdmin\MonthlyReport;

use App\Model\WaterSummary2;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UpWiseWashStatusController extends Controller
{
  private $view_path = "core.superadmin.monthly-report.wash.";

  public function index( Request $request )
  {
    $datas = \DB::select(\DB::Raw("
        SELECT
        t.distname,
        t.upname,
        t.unname,
        cdf_no,
        cdf_pop,
        cdf_hh,
        (HHS_new+HHS_rep) AS 'Hygieniclatrine',
        ((HHS_new+HHS_rep)/cdf_hh*100) AS 'per1',
        sa_b,
        ws_50,
        (ws_50/cdf_pop*100) AS 'per2',
        wat_b,
        (HHS_new_100+HHS_rep_100) AS 'HygienicLatrine',
        ((HHS_new_100+HHS_rep_100)/cdf_hh*100) AS 'per3',
        ws_50_100 AS 'WaterAccessPop',
        (ws_50_100/cdf_pop*100) AS 'per4'

        FROM (
          SELECT
            project.project,
            fdistrict.distname,
            fupazila.upname,
            funion.unname,
            rep_data.unid,sa_b,wat_b,

            SUM(CASE WHEN rep_data.rep_id <99 THEN cdf_no ELSE 0 END) AS 'cdf_no',
            SUM(CASE WHEN rep_data.rep_id <99 THEN cdf_pop ELSE 0 END) AS 'cdf_pop',
            SUM(CASE WHEN rep_data.rep_id <99 THEN cdf_hh ELSE 0 END) AS 'cdf_hh',
            SUM(CASE WHEN rep_data.rep_id =99 THEN HHS_new ELSE 0 END) AS 'HHS_new',
            SUM(CASE WHEN rep_data.rep_id =99 THEN HHS_rep ELSE 0 END) AS 'HHS_rep',
            SUM(CASE WHEN ws_50 = 99 THEN ws_50 ELSE 0 END) AS 'ws_50',
            SUM(CASE WHEN rep_data.rep_id <100 THEN HHS_new ELSE 0 END) AS 'HHS_new_100',
            SUM(CASE WHEN rep_data.rep_id <100 THEN HHS_rep ELSE 0 END) AS 'HHS_rep_100',
            SUM(CASE WHEN rep_data.rep_id <100 THEN ws_50 ELSE 0 END) AS 'ws_50_100'

            FROM rep_data, fdistrict, fupazila, funion, project, mnedata

            WHERE rep_data.distid = fdistrict.id

            AND
                rep_data.upid = fupazila.id AND
                rep_data.unid =funion.id AND
                project.id = rep_data.proj_id AND
                mnedata.unid = rep_data.unid

            GROUP BY rep_data.unid

            ORDER BY project.project, fdistrict.distname, fupazila.upname, funion.unname
        ) AS t
        GROUP BY t.unid
        ORDER BY t.distname, t.upname, t.unname
      "));

    //dd($datas);

    return view($this->view_path.'index', compact('datas'));
  }
}
