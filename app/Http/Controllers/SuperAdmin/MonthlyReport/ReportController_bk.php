<?php

namespace App\Http\Controllers\SuperAdmin\MonthlyReport;

use App\Http\Controllers\Controller;
use App\Model\District;
use App\Model\Project;
use App\Model\Region;
use App\Model\Union;
use App\Model\Upazila;
use Illuminate\Http\Request;
use DB;


class ReportController_bk extends Controller
{
  private $view_path = "core.superadmin.monthly-report.report.";

  public function index( Request $request )
  {

    //dd($request->all());

    $old = [
        'district_id' => "",
        'upazila_id'  => "",
        'union_id'    => "",
        'project_id'  => "",
        'region_id'   => "",
        'year'        => "",
        "month"       => "",

        "projectTitle" => "",
        "regionTitle"  => "",
        "districtTitle"=> "",
        "upazilaTitle" => "",
        "unionTitle"   => ""
    ];

    $districts = District::orderBy('distname')->get();
    $upazilas = "";
    $unions = "";
    $projects = Project::orderBy('project')->get();
    $regions = Region::orderBy("region_name")->get();

    $datas = "";

    $row_gettargets = "";
    $row_getbaseline = "";
    $row_lastdata = "";
    $row_currentmonth = "";

    if($request->has('query'))
    {
        $sql = "";
        $sql2 = "";

        if($request->has('district_id') && $request->district_id != "")
        {

            $upazilas = Upazila::where('disid', $request->district_id)->get();

            $sql .= " AND distid = ".$request->district_id;
            $sql2 .= " AND distid = ".$request->district_id;
        }

        if($request->has('upazila_id') && $request->upazila_id != "")
        {
            $unions = Union::where('upid', $request->upazila_id)->get();

            $sql .= " AND upid = ".$request->upazila_id;
        }

        $projectTitle  = "";
        if($request->has('project_id') && $request->project_id != "")
        {
            foreach($projects as $project)
            {
                if($project->id == $request->project_id)
                {
                    $projectTitle = $project->project;
                    break;
                }
            }

            $sql .= " AND proj_id = ".$request->project_id;
            $sql2 .= " AND proj_id = ".$request->project_id;
        }

        $regionTitle   = "";
        if($request->has('region_id') && $request->region_id != "")
        {
            foreach($regions as $region)
            {
                if($region->id == $request->region_id)
                {
                    $regionTitle = $region->region_name;
                    break;
                }
            }

            $sql .= " AND region_id = ".$request->region_id;
            $sql2 .= " AND region_id = ".$request->region_id;
        }

        $districtTitle = "";
        if($request->has('district_id') && $request->district_id != "")
        {
            foreach($districts as $district)
            {
                if($district->id == $request->district_id)
                {
                    $districtTitle = $district->distname;
                    break;
                }
            }
        }

        $upazilaTitle  = "";
        if($request->has('upazila_id') && $request->upazila_id != "")
        {
            foreach($upazilas as $up)
            {
                if($up->id == $request->upazila_id)
                {
                    $upazilaTitle = $up->upname;
                    break;
                }
            }

            $sql .= " AND upid = ".$request->upazila_id;
            $sql2 .= " AND upid = ".$request->upazila_id;
        }

        $unionTitle    = "";
        if($request->has('union_id') && $request->union_id != "")
        {
            foreach($unions as $un)
            {
                if($un->id == $request->union_id)
                {
                    $unionTitle = $un->unname;
                    break;
                }
            }
            $sql .= " AND unid = ".$request->union_id;
            $sql2 .= " AND unid = ".$request->union_id;
        }

        $old = [
            'district_id' => $request->input('district_id'),
            'upazila_id'  => $request->input('upazila_id'),
            'union_id'    => $request->input('union_id'),
            'project_id'  => $request->input('project_id'),
            'region_id'   => $request->input('region_id'),
            'year'        => $request->input('year'),
            "month"       => $request->input('month'),
            "projectTitle" => $projectTitle,
            "regionTitle"  => $regionTitle,
            "districtTitle"=> $districtTitle,
            "upazilaTitle" => $upazilaTitle,
            "unionTitle"   => $unionTitle,
            'query'        => $request->input('query')

        ];

        if($request->has('year') && $request->year != "")
        {
            $sql .= " AND year(`update`) = ".$request->year." ";
        }

        if($request->has('month') && $request->month != "")
        {
            $sql .= " AND month(`update`) = ".$request->month." ";
        }

        // echo $sql; exit();
        // generate report here

        //\DB::enableQueryLog();

        $row_lastdata=DB::select(
            DB::raw("
            SELECT
            Sum(rep_data.cdf_no) AS SumOfcdf_no,
            Sum(sa_approved),
            Sum(sa_completed),
            Sum(sa_benef),
            Sum(sa_renovated),
            Sum(ws_approved),
            Sum(ws_completed),
            Sum(ws_beneficiary),
            Sum(ws_hc_benef),
            Sum(ws_50),
            Sum(rep_data.cdf_pop) AS SumOfcdf_pop,
            Sum(rep_data.cdf_male) AS SumOfcdf_male,
            Sum(rep_data.cdf_female) AS SumOfcdf_female,
            Sum(rep_data.cdf_pop_hc) AS SumOfcdf_pop_hc,
            Sum(rep_data.cdf_hh) AS SumOfcdf_hh,
            Sum(rep_data.cdf_hh_hc) AS SumOfcdf_hh_hc,
            Sum(rep_data.cdf_pop_disb) AS SumOfcdf_pop_disb,
            Sum(rep_data.cdf_pop_safety) AS SumOfcdf_pop_safety,
            Sum(rep_data.cdf_cf_tot) AS SumOfcdf_cf_tot,
            Sum(rep_data.cdf_cf_male) AS SumOfcdf_cf_male,
            Sum(rep_data.cdf_cf_female) AS SumOfcdf_cf_female,
            Sum(rep_data.cb_trg) AS SumOfcb_trg,
            Sum(rep_data.cb_trg_up_total) AS SumOfcb_trg_up_total,
            Sum(rep_data.cb_trg_up_male) AS SumOfcb_trg_up_male,
            Sum(rep_data.cb_trg_up_female) AS SumOfcb_trg_up_female,
            Sum(rep_data.cb_trg_stf_total) AS SumOfcb_trg_stf_total,
            Sum(rep_data.cb_trg_stf_male) AS SumOfcb_trg_stf_male,
            Sum(rep_data.cb_trg_stf_female) AS SumOfcb_trg_stf_female,
            Sum(rep_data.cb_trg_vol_total) AS SumOfcb_trg_vol_total,
            Sum(rep_data.cb_trg_vol_male) AS SumOfcb_trg_vol_male,
            Sum(rep_data.cb_trg_vol_female) AS SumOfcb_trg_vol_female,
            Sum(rep_data.CHY_hw_ses) AS SumOfCHY_hw_ses,
            Sum(rep_data.CHY_hw_male) AS SumOfCHY_hw_male,
            Sum(rep_data.CHY_hw_female) AS SumOfCHY_hw_female,
            Sum(rep_data.CHY_mn_ses) AS SumOfCHY_mn_ses,
            Sum(rep_data.CHY_mn_female) AS SumOfCHY_mn_female,
            Sum(rep_data.CHY_sa_ses) AS SumOfCHY_sa_ses,
            Sum(rep_data.CHY_sa_hh) AS SumOfCHY_sa_hh,
            Sum(rep_data.CHY_fh_ses) AS SumOfCHY_fh_ses,
            Sum(rep_data.CHY_fh_hh) AS SumOfCHY_fh_hh,
            Sum(rep_data.CHY_gb_new) AS SumOfCHY_gb_new,
            Sum(rep_data.CHY_gb_rep) AS SumOfCHY_gb_rep,
            Sum(rep_data.CHY_dr) AS SumOfCHY_dr,
            Sum(rep_data.CHY_dr_pop) AS SumOfCHY_dr_pop,
            Sum(rep_data.CHY_vd) AS SumOfCHY_vd,
            Sum(rep_data.CHY_vd_pop) AS SumOfCHY_vd_pop,
            Sum(rep_data.HHS_new) AS SumOfHHS_new,
            Sum(rep_data.HHS_new_hc) AS SumOfHHS_new_hc,
            Sum(rep_data.HHS_rep) AS SumOfHHS_rep,
            Sum(rep_data.HHS_rep_hc) AS SumOfHHS_rep_hc,
            Sum(rep_data.scl_tot) AS SumOfscl_tot,
            Sum(rep_data.scl_tot_std) AS SumOfscl_tot_std,
            Sum(rep_data.scl_boys) AS SumOfscl_boys,
            Sum(rep_data.scl_girls) AS SumOfscl_girls,
            Sum(rep_data.scl_pri) AS SumOfscl_pri,
            Sum(rep_data.scl_pri_std) AS SumOfscl_pri_std,
            Sum(rep_data.scl_high) AS SumOfscl_high,
            Sum(rep_data.scl_high_std) AS SumOfscl_high_std,
            Sum(rep_data.scl_mad) AS SumOfscl_mad,
            Sum(rep_data.scl_mad_std) AS SumOfscl_mad_std,
            Sum(rep_data.scl_hp_ses) AS SumOfscl_hp_ses,
            Sum(rep_data.scl_hp_boys) AS SumOfscl_hp_boys,
            Sum(rep_data.scl_hp_girls) AS SumOfscl_hp_girls,
            Sum(rep_data.scl_mn_ses) AS SumOfscl_mn_ses,
            Sum(rep_data.scl_mn_girls) AS SumOfscl_mn_girls,
            Sum(rep_data.scl_dr) AS SumOfscl_dr,
            Sum(rep_data.scl_dr_std) AS SumOfscl_dr_std,
            Sum(rep_data.scl_vd) AS SumOfscl_vd,
            Sum(rep_data.scl_vd_std) AS SumOfscl_vd_std,
            Sum(rep_data.up_stf_tot) AS SumOfup_stf_tot,
            Sum(rep_data.up_stf_male) AS SumOfup_stf_male,
            Sum(rep_data.up_stf_female) AS SumOfup_stf_female,
            Sum(rep_data.up_pngo) AS SumOfup_pngo,
            Sum(rep_data.up_cont) AS SumOfup_cont,
            Sum(rep_data.TW_maintenance) AS SumOfTW_maintenance,
            Sum(rep_data.wsp) AS SumOfwsp,
            Sum(rep_data.CT_trg),
            Sum(rep_data.pdb)
            FROM rep_data WHERE rep_id = 100 ". $sql
        ));

        //dd($row_lastdata);


        $row_lastdata = json_decode(json_encode((array) $row_lastdata), true);
        $row_gettargets=$row_lastdata[0];

        //dd($row_lastdata);

        //\Log::info(\DB::getQueryLog());
        //dd(\DB::getQueryLog());

        // baseline

        $row_lastdata=DB::select(
            DB::raw("
            SELECT
            Sum(rep_data.cdf_no) AS SumOfcdf_no,
            Sum(sa_approved),
            Sum(sa_completed),
            Sum(sa_benef),
            Sum(sa_renovated),
            Sum(ws_approved),
            Sum(ws_completed),
            Sum(ws_beneficiary),
            Sum(ws_hc_benef),
            Sum(ws_50),
            Sum(rep_data.cdf_pop) AS SumOfcdf_pop,
            Sum(rep_data.cdf_male) AS SumOfcdf_male,
            Sum(rep_data.cdf_female) AS SumOfcdf_female,
            Sum(rep_data.cdf_pop_hc) AS SumOfcdf_pop_hc,
            Sum(rep_data.cdf_hh) AS SumOfcdf_hh,
            Sum(rep_data.cdf_hh_hc) AS SumOfcdf_hh_hc,
            Sum(rep_data.cdf_pop_disb) AS SumOfcdf_pop_disb,
            Sum(rep_data.cdf_pop_safety) AS SumOfcdf_pop_safety,
            Sum(rep_data.cdf_cf_tot) AS SumOfcdf_cf_tot,
            Sum(rep_data.cdf_cf_male) AS SumOfcdf_cf_male,
            Sum(rep_data.cdf_cf_female) AS SumOfcdf_cf_female,
            Sum(rep_data.cb_trg) AS SumOfcb_trg,
            Sum(rep_data.cb_trg_up_total) AS SumOfcb_trg_up_total,
            Sum(rep_data.cb_trg_up_male) AS SumOfcb_trg_up_male,
            Sum(rep_data.cb_trg_up_female) AS SumOfcb_trg_up_female,
            Sum(rep_data.cb_trg_stf_total) AS SumOfcb_trg_stf_total,
            Sum(rep_data.cb_trg_stf_male) AS SumOfcb_trg_stf_male,
            Sum(rep_data.cb_trg_stf_female) AS SumOfcb_trg_stf_female,
            Sum(rep_data.cb_trg_vol_total) AS SumOfcb_trg_vol_total,
            Sum(rep_data.cb_trg_vol_male) AS SumOfcb_trg_vol_male,
            Sum(rep_data.cb_trg_vol_female) AS SumOfcb_trg_vol_female,
            Sum(rep_data.CHY_hw_ses) AS SumOfCHY_hw_ses,
            Sum(rep_data.CHY_hw_male) AS SumOfCHY_hw_male,
            Sum(rep_data.CHY_hw_female) AS SumOfCHY_hw_female,
            Sum(rep_data.CHY_mn_ses) AS SumOfCHY_mn_ses,
            Sum(rep_data.CHY_mn_female) AS SumOfCHY_mn_female,
            Sum(rep_data.CHY_sa_ses) AS SumOfCHY_sa_ses,
            Sum(rep_data.CHY_sa_hh) AS SumOfCHY_sa_hh,
            Sum(rep_data.CHY_fh_ses) AS SumOfCHY_fh_ses,
            Sum(rep_data.CHY_fh_hh) AS SumOfCHY_fh_hh,
            Sum(rep_data.CHY_gb_new) AS SumOfCHY_gb_new,
            Sum(rep_data.CHY_gb_rep) AS SumOfCHY_gb_rep,
            Sum(rep_data.CHY_dr) AS SumOfCHY_dr,
            Sum(rep_data.CHY_dr_pop) AS SumOfCHY_dr_pop,
            Sum(rep_data.CHY_vd) AS SumOfCHY_vd,
            Sum(rep_data.CHY_vd_pop) AS SumOfCHY_vd_pop,
            Sum(rep_data.HHS_new) AS SumOfHHS_new,
            Sum(rep_data.HHS_new_hc) AS SumOfHHS_new_hc,
            Sum(rep_data.HHS_rep) AS SumOfHHS_rep,
            Sum(rep_data.HHS_rep_hc) AS SumOfHHS_rep_hc,
            Sum(rep_data.scl_tot) AS SumOfscl_tot,
            Sum(rep_data.scl_tot_std) AS SumOfscl_tot_std,
            Sum(rep_data.scl_boys) AS SumOfscl_boys,
            Sum(rep_data.scl_girls) AS SumOfscl_girls,
            Sum(rep_data.scl_pri) AS SumOfscl_pri,
            Sum(rep_data.scl_pri_std) AS SumOfscl_pri_std,
            Sum(rep_data.scl_high) AS SumOfscl_high,
            Sum(rep_data.scl_high_std) AS SumOfscl_high_std,
            Sum(rep_data.scl_mad) AS SumOfscl_mad,
            Sum(rep_data.scl_mad_std) AS SumOfscl_mad_std,
            Sum(rep_data.scl_hp_ses) AS SumOfscl_hp_ses,
            Sum(rep_data.scl_hp_boys) AS SumOfscl_hp_boys,
            Sum(rep_data.scl_hp_girls) AS SumOfscl_hp_girls,
            Sum(rep_data.scl_mn_ses) AS SumOfscl_mn_ses,
            Sum(rep_data.scl_mn_girls) AS SumOfscl_mn_girls,
            Sum(rep_data.scl_dr) AS SumOfscl_dr,
            Sum(rep_data.scl_dr_std) AS SumOfscl_dr_std,
            Sum(rep_data.scl_vd) AS SumOfscl_vd,
            Sum(rep_data.scl_vd_std) AS SumOfscl_vd_std,
            Sum(rep_data.up_stf_tot) AS SumOfup_stf_tot,
            Sum(rep_data.up_stf_male) AS SumOfup_stf_male,
            Sum(rep_data.up_stf_female) AS SumOfup_stf_female,
            Sum(rep_data.up_pngo) AS SumOfup_pngo,
            Sum(rep_data.up_cont) AS SumOfup_cont,
            Sum(rep_data.TW_maintenance) AS SumOfTW_maintenance,
            Sum(rep_data.wsp) AS SumOfwsp,
            Sum(rep_data.CT_trg),
            Sum(rep_data.pdb)
            FROM rep_data WHERE rep_id = 99 ". $sql
        ));

        $row_lastdata = json_decode(json_encode((array) $row_lastdata), true);
        $row_getbaseline=$row_lastdata[0];



        $row_lastdata=DB::select(
            DB::raw("
            SELECT
            Sum(rep_data.cdf_no) AS SumOfcdf_no,
            Sum(sa_approved),
            Sum(sa_completed),
            Sum(sa_benef),
            Sum(sa_renovated),
            Sum(ws_approved),
            Sum(ws_completed),
            Sum(ws_beneficiary),
            Sum(ws_hc_benef),
            Sum(ws_50),
            Sum(rep_data.cdf_pop) AS SumOfcdf_pop,
            Sum(rep_data.cdf_male) AS SumOfcdf_male,
            Sum(rep_data.cdf_female) AS SumOfcdf_female,
            Sum(rep_data.cdf_pop_hc) AS SumOfcdf_pop_hc,
            Sum(rep_data.cdf_hh) AS SumOfcdf_hh,
            Sum(rep_data.cdf_hh_hc) AS SumOfcdf_hh_hc,
            Sum(rep_data.cdf_pop_disb) AS SumOfcdf_pop_disb,
            Sum(rep_data.cdf_pop_safety) AS SumOfcdf_pop_safety,
            Sum(rep_data.cdf_cf_tot) AS SumOfcdf_cf_tot,
            Sum(rep_data.cdf_cf_male) AS SumOfcdf_cf_male,
            Sum(rep_data.cdf_cf_female) AS SumOfcdf_cf_female,
            Sum(rep_data.cb_trg) AS SumOfcb_trg,
            Sum(rep_data.cb_trg_up_total) AS SumOfcb_trg_up_total,
            Sum(rep_data.cb_trg_up_male) AS SumOfcb_trg_up_male,
            Sum(rep_data.cb_trg_up_female) AS SumOfcb_trg_up_female,
            Sum(rep_data.cb_trg_stf_total) AS SumOfcb_trg_stf_total,
            Sum(rep_data.cb_trg_stf_male) AS SumOfcb_trg_stf_male,
            Sum(rep_data.cb_trg_stf_female) AS SumOfcb_trg_stf_female,
            Sum(rep_data.cb_trg_vol_total) AS SumOfcb_trg_vol_total,
            Sum(rep_data.cb_trg_vol_male) AS SumOfcb_trg_vol_male,
            Sum(rep_data.cb_trg_vol_female) AS SumOfcb_trg_vol_female,
            Sum(rep_data.CHY_hw_ses) AS SumOfCHY_hw_ses,
            Sum(rep_data.CHY_hw_male) AS SumOfCHY_hw_male,
            Sum(rep_data.CHY_hw_female) AS SumOfCHY_hw_female,
            Sum(rep_data.CHY_mn_ses) AS SumOfCHY_mn_ses,
            Sum(rep_data.CHY_mn_female) AS SumOfCHY_mn_female,
            Sum(rep_data.CHY_sa_ses) AS SumOfCHY_sa_ses,
            Sum(rep_data.CHY_sa_hh) AS SumOfCHY_sa_hh,
            Sum(rep_data.CHY_fh_ses) AS SumOfCHY_fh_ses,
            Sum(rep_data.CHY_fh_hh) AS SumOfCHY_fh_hh,
            Sum(rep_data.CHY_gb_new) AS SumOfCHY_gb_new,
            Sum(rep_data.CHY_gb_rep) AS SumOfCHY_gb_rep,
            Sum(rep_data.CHY_dr) AS SumOfCHY_dr,
            Sum(rep_data.CHY_dr_pop) AS SumOfCHY_dr_pop,
            Sum(rep_data.CHY_vd) AS SumOfCHY_vd,
            Sum(rep_data.CHY_vd_pop) AS SumOfCHY_vd_pop,
            Sum(rep_data.HHS_new) AS SumOfHHS_new,
            Sum(rep_data.HHS_new_hc) AS SumOfHHS_new_hc,
            Sum(rep_data.HHS_rep) AS SumOfHHS_rep,
            Sum(rep_data.HHS_rep_hc) AS SumOfHHS_rep_hc,
            Sum(rep_data.scl_tot) AS SumOfscl_tot,
            Sum(rep_data.scl_tot_std) AS SumOfscl_tot_std,
            Sum(rep_data.scl_boys) AS SumOfscl_boys,
            Sum(rep_data.scl_girls) AS SumOfscl_girls,
            Sum(rep_data.scl_pri) AS SumOfscl_pri,
            Sum(rep_data.scl_pri_std) AS SumOfscl_pri_std,
            Sum(rep_data.scl_high) AS SumOfscl_high,
            Sum(rep_data.scl_high_std) AS SumOfscl_high_std,
            Sum(rep_data.scl_mad) AS SumOfscl_mad,
            Sum(rep_data.scl_mad_std) AS SumOfscl_mad_std,
            Sum(rep_data.scl_hp_ses) AS SumOfscl_hp_ses,
            Sum(rep_data.scl_hp_boys) AS SumOfscl_hp_boys,
            Sum(rep_data.scl_hp_girls) AS SumOfscl_hp_girls,
            Sum(rep_data.scl_mn_ses) AS SumOfscl_mn_ses,
            Sum(rep_data.scl_mn_girls) AS SumOfscl_mn_girls,
            Sum(rep_data.scl_dr) AS SumOfscl_dr,
            Sum(rep_data.scl_dr_std) AS SumOfscl_dr_std,
            Sum(rep_data.scl_vd) AS SumOfscl_vd,
            Sum(rep_data.scl_vd_std) AS SumOfscl_vd_std,
            Sum(rep_data.up_stf_tot) AS SumOfup_stf_tot,
            Sum(rep_data.up_stf_male) AS SumOfup_stf_male,
            Sum(rep_data.up_stf_female) AS SumOfup_stf_female,
            Sum(rep_data.up_pngo) AS SumOfup_pngo,
            Sum(rep_data.up_cont) AS SumOfup_cont,
            Sum(rep_data.TW_maintenance) AS SumOfTW_maintenance,
            Sum(rep_data.wsp) AS SumOfwsp,
            Sum(rep_data.CT_trg),
            Sum(rep_data.pdb)
            FROM rep_data
            WHERE rep_id NOT IN (99,100)  ". $sql
        ));

        $row_lastdata = json_decode(json_encode((array) $row_lastdata), true);
        $row_currentmonth=$row_lastdata[0];


        $row_lastdata=DB::select(
            DB::raw("
            SELECT
            Sum(rep_data.cdf_no) AS SumOfcdf_no,
            Sum(sa_approved),
            Sum(sa_completed),
            Sum(sa_benef),
            Sum(sa_renovated),
            Sum(ws_approved),
            Sum(ws_completed),
            Sum(ws_beneficiary),
            Sum(ws_hc_benef),
            Sum(ws_50),
            Sum(rep_data.cdf_pop) AS SumOfcdf_pop,
            Sum(rep_data.cdf_male) AS SumOfcdf_male,
            Sum(rep_data.cdf_female) AS SumOfcdf_female,
            Sum(rep_data.cdf_pop_hc) AS SumOfcdf_pop_hc,
            Sum(rep_data.cdf_hh) AS SumOfcdf_hh,
            Sum(rep_data.cdf_hh_hc) AS SumOfcdf_hh_hc,
            Sum(rep_data.cdf_pop_disb) AS SumOfcdf_pop_disb,
            Sum(rep_data.cdf_pop_safety) AS SumOfcdf_pop_safety,
            Sum(rep_data.cdf_cf_tot) AS SumOfcdf_cf_tot,
            Sum(rep_data.cdf_cf_male) AS SumOfcdf_cf_male,
            Sum(rep_data.cdf_cf_female) AS SumOfcdf_cf_female,
            Sum(rep_data.cb_trg) AS SumOfcb_trg,
            Sum(rep_data.cb_trg_up_total) AS SumOfcb_trg_up_total,
            Sum(rep_data.cb_trg_up_male) AS SumOfcb_trg_up_male,
            Sum(rep_data.cb_trg_up_female) AS SumOfcb_trg_up_female,
            Sum(rep_data.cb_trg_stf_total) AS SumOfcb_trg_stf_total,
            Sum(rep_data.cb_trg_stf_male) AS SumOfcb_trg_stf_male,
            Sum(rep_data.cb_trg_stf_female) AS SumOfcb_trg_stf_female,
            Sum(rep_data.cb_trg_vol_total) AS SumOfcb_trg_vol_total,
            Sum(rep_data.cb_trg_vol_male) AS SumOfcb_trg_vol_male,
            Sum(rep_data.cb_trg_vol_female) AS SumOfcb_trg_vol_female,
            Sum(rep_data.CHY_hw_ses) AS SumOfCHY_hw_ses,
            Sum(rep_data.CHY_hw_male) AS SumOfCHY_hw_male,
            Sum(rep_data.CHY_hw_female) AS SumOfCHY_hw_female,
            Sum(rep_data.CHY_mn_ses) AS SumOfCHY_mn_ses,
            Sum(rep_data.CHY_mn_female) AS SumOfCHY_mn_female,
            Sum(rep_data.CHY_sa_ses) AS SumOfCHY_sa_ses,
            Sum(rep_data.CHY_sa_hh) AS SumOfCHY_sa_hh,
            Sum(rep_data.CHY_fh_ses) AS SumOfCHY_fh_ses,
            Sum(rep_data.CHY_fh_hh) AS SumOfCHY_fh_hh,
            Sum(rep_data.CHY_gb_new) AS SumOfCHY_gb_new,
            Sum(rep_data.CHY_gb_rep) AS SumOfCHY_gb_rep,
            Sum(rep_data.CHY_dr) AS SumOfCHY_dr,
            Sum(rep_data.CHY_dr_pop) AS SumOfCHY_dr_pop,
            Sum(rep_data.CHY_vd) AS SumOfCHY_vd,
            Sum(rep_data.CHY_vd_pop) AS SumOfCHY_vd_pop,
            Sum(rep_data.HHS_new) AS SumOfHHS_new,
            Sum(rep_data.HHS_new_hc) AS SumOfHHS_new_hc,
            Sum(rep_data.HHS_rep) AS SumOfHHS_rep,
            Sum(rep_data.HHS_rep_hc) AS SumOfHHS_rep_hc,
            Sum(rep_data.scl_tot) AS SumOfscl_tot,
            Sum(rep_data.scl_tot_std) AS SumOfscl_tot_std,
            Sum(rep_data.scl_boys) AS SumOfscl_boys,
            Sum(rep_data.scl_girls) AS SumOfscl_girls,
            Sum(rep_data.scl_pri) AS SumOfscl_pri,
            Sum(rep_data.scl_pri_std) AS SumOfscl_pri_std,
            Sum(rep_data.scl_high) AS SumOfscl_high,
            Sum(rep_data.scl_high_std) AS SumOfscl_high_std,
            Sum(rep_data.scl_mad) AS SumOfscl_mad,
            Sum(rep_data.scl_mad_std) AS SumOfscl_mad_std,
            Sum(rep_data.scl_hp_ses) AS SumOfscl_hp_ses,
            Sum(rep_data.scl_hp_boys) AS SumOfscl_hp_boys,
            Sum(rep_data.scl_hp_girls) AS SumOfscl_hp_girls,
            Sum(rep_data.scl_mn_ses) AS SumOfscl_mn_ses,
            Sum(rep_data.scl_mn_girls) AS SumOfscl_mn_girls,
            Sum(rep_data.scl_dr) AS SumOfscl_dr,
            Sum(rep_data.scl_dr_std) AS SumOfscl_dr_std,
            Sum(rep_data.scl_vd) AS SumOfscl_vd,
            Sum(rep_data.scl_vd_std) AS SumOfscl_vd_std,
            Sum(rep_data.up_stf_tot) AS SumOfup_stf_tot,
            Sum(rep_data.up_stf_male) AS SumOfup_stf_male,
            Sum(rep_data.up_stf_female) AS SumOfup_stf_female,
            Sum(rep_data.up_pngo) AS SumOfup_pngo,
            Sum(rep_data.up_cont) AS SumOfup_cont,
            Sum(rep_data.TW_maintenance) AS SumOfTW_maintenance,
            Sum(rep_data.wsp) AS SumOfwsp,
            Sum(rep_data.CT_trg),
            Sum(rep_data.pdb)
            FROM rep_data
            WHERE rep_id NOT IN (99,100)  ". $sql2
        ));

        $row_lastdata = json_decode(json_encode((array) $row_lastdata), true);
        $row_totaldata=$row_lastdata[0];


        //\Log::info(\DB::getQueryLog());
        //dd($row_gettargets);
        // $ReportData = \DB::select( \DB::raw("SELECT * FROM rep_data WHERE id = $id  and unid=$uid" ));
        // $ReportData = json_decode(json_encode((array) $ReportData), true);
        // $ReportData = @$ReportData[0];

    }

    $row_lastdata = $row_gettargets;

    if($request->has('print'))
    {
        return view($this->view_path.'print', compact(
            'old',
            'districts',
            'upazilas',
            'unions',
            'projects',
            'regions',
            'datas',

            'row_gettargets',
            'row_getbaseline',
            'row_totaldata',
            'row_lastdata',
            'row_currentmonth'
        ));
    }


    return view($this->view_path.'index', compact(
        'old',
        'districts',
        'upazilas',
        'unions',
        'projects',
        'regions',
        'datas',

        'row_gettargets',
        'row_getbaseline',
        'row_totaldata',
        'row_lastdata',
        'row_currentmonth'
    ));
  }
}
