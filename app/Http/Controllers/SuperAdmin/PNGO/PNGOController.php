<?php

namespace App\Http\Controllers\SuperAdmin\PNGO;

use App\Http\Controllers\Controller;
use App\ImplementStatus;
use App\Model\Download\Superadmin\PNGO\PNGODownload;
use App\Model\Search\PNGOSearch;
use DB;
use Illuminate\Http\Request;

class PNGOController extends Controller
{
    private $view_path = "core.superadmin.pngo.";
    private $route_path = "superadmin.pngo.pngolist.";

    public function index( Request $request )
    {
      if($request->has('download'))
      {
        return (new PNGODownload)->download();
      }

      $old = [
        'project_id'  => '',
        'region_id'   => '',
        'district_id' => '',
        'upazila_id'  => '',
        'union_id'    => ''
      ];

      $pngolistss = "";

      if($request->has('query'))
      {
        $old = [
          'query'       => $request->input('query'),
          'project_id'  => $request->input('project_id'),
          'region_id'   => $request->input('region_id'),
          'district_id' => $request->input('district_id'),
          'upazila_id'  => $request->input('upazila_id'),
          'union_id'    => $request->input('union_id')
        ];
        $pngolistss = (new PNGOSearch($request, true))->get();
      }
      else
      {
        $pngolistss = (new PNGOSearch($request, true))->get();
      }

      return view($this->view_path.'index', compact('pngolistss', 'old'));
    }


    public function create()
    {
        $district=DB::table('fdistrict')->pluck('distname','id')->toArray();
        return view($this->view_path.'insert', compact('district'));
    }

    public function store(Request $request)
    {
      $data=$request->except('_token','_method');
      $data["updated_by"]=auth()->user()->id;
      $data["created_by"]=auth()->user()->id;
      $count = \DB::table('osm_pngo')->insert($data);

      if($count){
        \Session::flash('message', "New Data saved successfully");
        return redirect()->route($this->route_path.'index');
      }
      \Session::flash('message', "Try Again");
      return redirect()->route($this->route_path.'create');
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
      $district=DB::table('fdistrict')->pluck('distname','id')->toArray();
      $pngolists = DB::table('osm_pngo')->WHERE('id',$id)->first();
      $upnamelist = DB::table('fupazila')->where('disid',$pngolists->distid)->pluck('upname','id')->toArray();
      $unnamelist = DB::table('funion')->where('distid',$pngolists->distid)->where('upid',$pngolists->upid)->pluck('unname','id')->toArray();
      return view($this->view_path.'edit', compact('district','pngolists','unnamelist','upnamelist'));
    }

    public function update(Request $request, $id)
    {
        $Info = \DB::table('osm_pngo')
                    ->where('id', $id)
                    ->get();
        $data=$request->except('_token','_method');
        $data["updated_by"]=auth()->user()->id;

        if(COUNT($Info)>0){
          DB::table('osm_pngo')
            ->where('id', $Info->first()->id)
            ->update($data);
        }

      \Session::flash('message', "One updated successfully");
      return redirect()->route($this->route_path.'index');
    }
}
