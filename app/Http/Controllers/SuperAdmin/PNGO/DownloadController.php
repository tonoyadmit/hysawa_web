<?php

namespace App\Http\Controllers\SuperAdmin\PNGO;

use App\Http\Controllers\Controller;
use App\Model\Download\Superadmin\PNGO\PNGODownload;
use Illuminate\Http\Request;

class DownloadController extends Controller
{
    public function get()
    {
      return (new PNGODownload)->download();
    }
}
