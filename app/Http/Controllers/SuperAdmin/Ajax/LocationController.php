<?php

namespace App\Http\Controllers\SuperAdmin\Ajax;

use App\Http\Controllers\Controller;
use App\ImplementStatus;
use App\Model\Download\WaterDownload;
use App\Model\Search\Request\WaterSearchRequest;
use App\Model\Search\WaterSearch;
use App\Water;
use App\WaterTech;
use Illuminate\Http\Request;

class LocationController extends Controller
{
    public function getDistricts(Request $request )
    {
      $not_all = false;
      $choose_one = false;

      if($request->has('not_all') && $request->input('not_all') == true){
        $not_all = true;
      }

      if(!$request->ajax() || !$request->has('region_id')){
        return ['status' => false];
      }

      if($request->has('choose_one') && $request->input('choose_one') == true){
        $choose_one = true;
      }

      $districts = \DB::table('fdistrict')->where('region_id', $request->input('region_id'))->get();
      $str = "";
      foreach($districts as $d)
      {
        $str .= '<option value="'.$d->id.'">'.$d->distname.'</option>';
      }

      if($str != "" && !$not_all)
      {
        $str = '<option value="all" selected="selected">All</option>'.$str;
      }

      if($str != "" && $choose_one)
      {
        $str = '<option value="" selected="selected">Chosse One Option</option>'. $str;
      }


      return [
        'status' => true,
        'district_list' => $str
      ];
    }

    public function getUpazilas(Request $request )
    {
      $not_all = false;
      $choose_one = false;

      if($request->has('not_all') && $request->input('not_all') == true)
      {
        $not_all = true;
      }

      if(!$request->ajax() || !$request->has('district_id'))
      {
        return ['status' => false];
      }

      if($request->has('choose_one') && $request->input('choose_one') == true)
      {
        $choose_one = true;
      }


      $upazilas = \DB::table('fupazila')->where('disid', $request->input('district_id'))->get();
      $str = "";
      foreach($upazilas as $up)
      {
        $str .= '<option value="'.$up->id.'">'.$up->upname.'</option>';
      }

      if($str != "" && !$not_all)
      {
        $str = '<option value="all" selected="selected">All</option>'.$str;
      }

      if($str != "" && $choose_one)
      {
        $str = '<option value="" selected="selected">Chosse One Option</option>'. $str;
      }


      return [
        'status' => true,
        'upazila_list' => $str
      ];
    }

    public function getUnions(Request $request)
    {
      $not_all = false;
      $choose_one = false;

      if($request->has('not_all') && $request->input('not_all') == true)
      {
        $not_all = true;
      }

      if($request->has('choose_one') && $request->input('choose_one') == true)
      {
        $choose_one = true;
      }

      if(!$request->ajax() || !$request->has('upazila_id'))
      {
        return ['status' => false];
      }

      $unions = \DB::table('funion')->where('upid', $request->input('upazila_id'))->get();
      $str = "";
      foreach($unions as $un)
      {
        $str .= '<option value="'.$un->id.'">'.$un->unname.'</option>';
      }

      if($str != "" && !$not_all)
      {
        $str = '<option value="all" selected="selected">All</option>'. $str;
      }

      if($str != "" && $choose_one)
      {
        $str = '<option value="" selected="selected">Chosse One Option</option>'. $str;
      }

      return [
        'status' => true,
        'union_list' => $str
      ];
    }
}
