<?php

namespace App\Http\Controllers\SuperAdmin\SubProject;

use App\Http\Controllers\Controller;

use App\Model\Download\Superadmin\Project\Water\WaterApprovalDownload;
use App\Model\Download\Superadmin\Project\Water\WaterDistrictDownload;
use App\Model\Download\Superadmin\Project\Water\WaterRegionDownload;
use App\Model\Download\Superadmin\Project\Water\WaterUnionDownload;

use App\Model\WaterSummary2;

use Illuminate\Http\Request;

class WaterController extends Controller
{
  private $view_path = "core.superadmin.sub-projects.water.";
  private $route_path = "superadmin.sub-projects.waters.";

  public function index( Request $request )
  {
    // $regionSummary   = WaterSummary2::regionSummary();
    // $districtSummary = WaterSummary2::districtSummary();
    // $unionSummary    = WaterSummary2::unionSummary();
    // $summary         = WaterSummary2::approvalSummary();

    if($request->has('query') )
    {

      $type = $this->getType($request);

      \Log::info($type);

      if($request->has('output') && $request->output == "download")
      {
        if($type == "region"){
          return (new WaterRegionDownload)->download();
        }elseif($type == "singleRegion"){
          return (new WaterUnionDownload)->download('region_id', $request->input('region_id'));
        }elseif($type == "district"){
          return (new WaterDistrictDownload)->download();
        }elseif($type == "singleDistrict"){
          return (new WaterUnionDownload)->download('distid', $request->input('district_id'));
        }elseif($type == "upazila"){
          return (new WaterUnionDownload)->download('upid', $request->input('upazila_id'));
        }elseif($type == "union"){
          return (new WaterUnionDownload)->download('unid', $request->input('union_id'));
        }

      }else{
        if($type == 'region'){
          $regionSummary   = WaterSummary2::regionSummary();

          if($request->output == "print"){
            return view($this->view_path.'print', compact('regionSummary'))->with('type', 'region');
          }else{
            return view($this->view_path.'region', compact('regionSummary'));
          }

        }elseif($type == "district"){
          $districtSummary = WaterSummary2::districtSummary();

          if($request->output == "print"){
            return view($this->view_path.'print', compact('districtSummary'))->with('type', 'district');
          }else{
            return view($this->view_path.'district', compact('districtSummary'));
          }

        }elseif($type == "singleRegion"){

          $unionSummary    = \DB::select(\DB::Raw($this->getQuery('region_id', $request->input('region_id'))));

          if($request->output == "print"){
            return view($this->view_path.'print', compact('unionSummary'))->with('type', 'union');
          }else{
            return view($this->view_path.'union', compact('unionSummary'));
          }

        }elseif($type == "singleDistrict"){
          $unionSummary    = \DB::select(\DB::Raw($this->getQuery('distid', $request->input('district_id'))));

          if($request->output == "print"){
            return view($this->view_path.'print', compact('unionSummary'))->with('type', 'union');
          }else{
            return view($this->view_path.'union', compact('unionSummary'));
          }

        }elseif($type == "upazila"){

          $unionSummary    = \DB::select(\DB::Raw($this->getQuery('upid', $request->input('upazila_id'))));

          if($request->output == "print"){
            return view($this->view_path.'print', compact('unionSummary'))->with('type', 'union');
          }else{
            return view($this->view_path.'union', compact('unionSummary'));
          }

        }elseif($type == "union"){

          $unionSummary    = \DB::select(\DB::Raw($this->getQuery('unid', $request->input('union_id'))));
          if($request->output == "print"){
            return view($this->view_path.'print', compact('unionSummary'))->with('type', 'union');
          }else{
            return view($this->view_path.'union', compact('unionSummary'));
          }

        }
      }
    }

    return view($this->view_path.'index'); //, compact('regionSummary', 'districtSummary', 'unionSummary', 'summary'));
  }

  private function getType(Request $request)
  {
    if($request->has('region_id') && $request->region_id == "all") return "region";
    if($request->has('district_id') && $request->district_id == "all") return "district";

    if($request->has('union_id') && $request->union_id != "" && $request->union_id != "all") return "union";
    if($request->has('upazila_id') && $request->upazila_id != "" && $request->upazila_id != "all") return "upazila";
    if($request->has('district_id') && $request->district_id != "" && $request->district_id != "all") return "singleDistrict";



    //if($request->has('upazila_id') && $request->upazila_id != "") return "upazila";
   // if($request->has('district_id') && $request->district_id != "") return "singleDistrict";
    //if($request->has('union_id') && $request->upazila_id != "all") return "union";

    if($request->has('region_id') && $request->region_id != "") return "singleRegion";

  }


  private function getQuery($type = null, $value = null)
  {
    if($type == null && $value)
    {
      return "
        SELECT
            fdistrict.distname,
            fupazila.upname,
            funion.unname,
            tbl_water.unid,
            tbl_water.App_date,
            tbl_water.Tend_lot,

            SUM(tbl_water.HH_benefited) AS 'hhcount',
            Sum(tbl_water.HCHH_benefited) AS hchhcount,
            Sum(tbl_water.HCHH_benefited) / Sum(tbl_water.HH_benefited)*100 AS hcPcount,
            (Sum(tbl_water.HCHH_benefited)/Sum(tbl_water.HH_benefited)*100)*0.1 + (100-(Sum(tbl_water.HCHH_benefited)/Sum(tbl_water.HH_benefited)*100))*0.2 AS cccount,

            COUNT(tbl_water.id) AS 'subappcount',
            sum(IF(app_status = 'Approved',1,0)) AS 'sumappcount',
            sum(IF(app_status = 'Submitted',1, 0)) AS 'Submitted',
            sum(IF(app_status = 'Recomended',1,0)) AS 'Recomended',
            sum(IF(app_status = 'Cancelled',1,0)) AS 'Cancelled',
            sum(IF(app_status = 'Rejected',1,0)) AS 'Rejected',
            sum(IF(app_status = 'Tendering in process',1,0)) AS 'TenderingInProcess',
            sum(IF(imp_status = 'Under Implementation', 1,0)) AS 'UnderImplementation',
            sum(IF(imp_status = 'Completed', 1, 0)) AS 'Completed',
            SUM(CASE WHEN wq_Arsenic IS NULL THEN 1 ELSE 0 END) AS 'wq_Arsenic',
            sum(IF(platform = 'yes', 1, 0)) AS 'platform',
            sum(IF(depth != '0', 1, 0)) AS 'depth',
            sum(IF(x_coord != '', 1, 0)) AS 'x_coord'

            FROM

            tbl_water

            INNER JOIN funion ON funion.id = tbl_water.unid
            INNER JOIN fupazila ON fupazila.id = funion.upid
            INNER JOIN fdistrict ON fdistrict.id = fupazila.disid

            GROUP BY
                tbl_water.unid,
                tbl_water.Tend_lot,
                tbl_water.App_date";
    }

    return "
      SELECT

        fdistrict.distname,
        fupazila.upname,
        funion.unname,
        tbl_water.unid,
        tbl_water.App_date,
        tbl_water.Tend_lot,

        SUM(tbl_water.HH_benefited) AS 'hhcount',
        Sum(tbl_water.HCHH_benefited) AS hchhcount,
        Sum(tbl_water.HCHH_benefited) / Sum(tbl_water.HH_benefited)*100 AS hcPcount,
        (Sum(tbl_water.HCHH_benefited)/Sum(tbl_water.HH_benefited)*100)*0.1 + (100-(Sum(tbl_water.HCHH_benefited)/Sum(tbl_water.HH_benefited)*100))*0.2 AS cccount,

        COUNT(tbl_water.id) AS 'subappcount',
        sum(IF(app_status = 'Approved',1,0)) AS 'sumappcount',
        sum(IF(app_status = 'Submitted',1, 0)) AS 'Submitted',
        sum(IF(app_status = 'Recomended',1,0)) AS 'Recomended',
        sum(IF(app_status = 'Cancelled',1,0)) AS 'Cancelled',
        sum(IF(app_status = 'Rejected',1,0)) AS 'Rejected',
        sum(IF(app_status = 'Tendering in process',1,0)) AS 'TenderingInProcess',
        sum(IF(imp_status = 'Under Implementation', 1,0)) AS 'UnderImplementation',
        sum(IF(imp_status = 'Completed', 1, 0)) AS 'Completed',
        SUM(CASE WHEN wq_Arsenic IS NULL THEN 1 ELSE 0 END) AS 'wq_Arsenic',
        sum(IF(platform = 'yes', 1, 0)) AS 'platform',
        sum(IF(depth != '0', 1, 0)) AS 'depth',
        sum(IF(x_coord != '', 1, 0)) AS 'x_coord'

        FROM

        tbl_water

        INNER JOIN funion ON funion.id = tbl_water.unid
        INNER JOIN fupazila ON fupazila.id = funion.upid
        INNER JOIN fdistrict ON fdistrict.id = fupazila.disid

        WHERE tbl_water.$type = $value

        GROUP BY
            tbl_water.unid,
            tbl_water.Tend_lot,
            tbl_water.App_date
    ";


  }

  public function regions()
  {
    $regionSummary   = WaterSummary2::regionSummary();
    return view($this->view_path.'region', compact('regionSummary'));
  }

  public function districts()
  {
    $districtSummary = WaterSummary2::districtSummary();
    return view($this->view_path.'district', compact('districtSummary'));
  }

  public function unions()
  {
    $unionSummary    = WaterSummary2::unionSummary();
    return view($this->view_path.'union', compact('unionSummary'));
  }

  public function approval(Request $request)
  {
    //$summary         = WaterSummary2::approvalSummary();

    $datas = "";

    if($request->has('query') )
    {
      $type = $this->getType($request);

      if($_REQUEST['approval_date'] != "" && ($_REQUEST['region_id'] == "all" && $_REQUEST['district_id'] == "" && $_REQUEST['upazila_id'] == "" && $_REQUEST['union_id'] == ""))
      {
        //dd("here");

        $unionSummary    = WaterSummary2::approvalUnionSummary( $request->input('approval_date'));

        if($request->output == "print"){
          return view($this->view_path.'approval.print', compact('unionSummary'))->with('type', 'union');
        }else{
          return view($this->view_path.'approval.union', compact('unionSummary'));
        }
      }elseif($type == 'region'){
        $regionSummary   = WaterSummary2::approvalRegionSummary($request->input('approval_date'));

        if($request->output == "print"){
          return view($this->view_path.'approval.print', compact('regionSummary'))->with('type', 'region');
        }else{
          return view($this->view_path.'approval.region', compact('regionSummary'));
        }

      }elseif($type == "district"){
        $districtSummary = WaterSummary2::approvalDistrictSummary($request->input('approval_date'));

        if($request->output == "print"){
          return view($this->view_path.'approval.print', compact('districtSummary'))->with('type', 'district');
        }else{
          return view($this->view_path.'approval.district', compact('districtSummary'));
        }

      }elseif($type == "singleRegion"){

        $unionSummary    = WaterSummary2::approvalUnionSummary( $request->input('approval_date'), 'region_id', $request->input('region_id')); 

        if($request->output == "print"){
          return view($this->view_path.'approval.print', compact('unionSummary'))->with('type', 'union');
        }else{
          return view($this->view_path.'approval.union', compact('unionSummary'));
        }

      }elseif($type == "singleDistrict"){
        $unionSummary    = WaterSummary2::approvalUnionSummary( $request->input('approval_date'), 'distid', $request->input('district_id')); 

        //$unionSummary    = \DB::select(\DB::Raw($this->getQuery('distid', $request->input('district_id'))));

        if($request->output == "print"){
          return view($this->view_path.'approval.print', compact('unionSummary'))->with('type', 'union');
        }else{
          return view($this->view_path.'approval.union', compact('unionSummary'));
        }

      }elseif($type == "upazila"){

        //$unionSummary    = \DB::select(\DB::Raw($this->getQuery('upid', $request->input('upazila_id'))));

        $unionSummary    = WaterSummary2::approvalUnionSummary( $request->input('approval_date'), 'upid', $request->input('upazila_id'));

        if($request->output == "print"){
          return view($this->view_path.'approval.print', compact('unionSummary'))->with('type', 'union');
        }else{
          return view($this->view_path.'approval.union', compact('unionSummary'));
        }

      }elseif($type == "union"){

        //$unionSummary    = \DB::select(\DB::Raw($this->getQuery('unid', $request->input('union_id'))));

        $unionSummary    = WaterSummary2::approvalUnionSummary( $request->input('approval_date'), 'unid', $request->input('union_id'));

        if($request->output == "print"){
          return view($this->view_path.'approval.print', compact('unionSummary'))->with('type', 'union');
        }else{
          return view($this->view_path.'approval.union', compact('unionSummary'));
        }


      }
    }

    return view($this->view_path.'approval.index', compact('datas'));
  }

  public function download(Request $request, $type)
  {
    if($type == "region"){
      return (new WaterRegionDownload)->download();
    }elseif($type == "district"){
      return (new WaterDistrictDownload)->download();
    }elseif($type == "union"){
      return (new WaterUnionDownload)->download();
    }elseif($type == "approval"){
      return (new WaterApprovalDownload)->download();
    }
  }

}
