<?php

namespace App\Http\Controllers\SuperAdmin\SubProject;

use App\AppStatus;
use App\Http\Controllers\Controller;
use App\ImplementStatus;
use App\Model\Download\Sanitation2Download;
use App\Model\Download\Superadmin\Project\Sanitation\SanitationDownload;
use App\Model\Download\Superadmin\Project\Sanitation\SummaryDownload;
use App\Model\Sanitation;
use App\Model\Search\Sanitation2Search;
use Illuminate\Http\Request;

class SanitationController extends Controller
{
  private $view_path = "core.superadmin.sub-projects.sanitation.";
  private $route_path = "superadmin.sub-projects.sanitation.";

  public function index( Request $request )
  {
    //dd($request->all());

     $old = [
        'district_id'  => '',
        'upazila_id'   => '',
        'union_id'     => '',
        'region_id'    => '',
        'project_id'   => '',

        'cdf_no'       => '',
        'village'      => '',
        'starting_date' => '',
        'app_status'   => '',
        'impl_status'  => ''
    ];

    if($request->has('query'))
    {
        $old = [
            'query' => $request->input('query'),
            'district_id'  => $request->input('district_id'),
            'upazila_id'   => $request->input('upazila_id'),
            'union_id'     => $request->input('union_id'),
            'region_id'    => $request->input('region_id'),
            'project_id'   => $request->input('project_id'),
            'cdf_no'       => $request->input('cdf_no'),
            'village'      => $request->input('village'),
            'starting_date' => $request->input('starting_date'),
            'app_status'   => $request->input('app_status'),
            'impl_status'  => $request->input('impl_status')
        ];
    }

    if($request->has('download'))
    {
      if($request->download == "list"){
        $datas = (new Sanitation2Search($request, false))->get();
        if(!count($datas)){ return response()->json(['status' => 'error', 'message' => 'No Data Found']);}
        return (new Sanitation2Download())->download($datas);
      }elseif($request->download == "summary"){
        return (new SummaryDownload)->download();
      }
    }

    if($request->has('print'))
    {
      $datas = (new Sanitation2Search($request, false))->get();
      if(!count($datas)){ return response()->json(['status' => 'error', 'message' => 'No Data Found']);}
      return view($this->view_path.'print', compact('datas'));
    }

    $datas = (new Sanitation2Search($request, true))->get();

   //dd($datas->toArray());
    return view($this->view_path.'index', compact('datas', 'old'));
  }

  public function summary(Request $request )
  {
    $datas = \DB::select(\DB::Raw("
      SELECT
      region.region_name,
      COUNT(sanitation.id) AS 'subappcount',

      sum(IF(app_status = 'Approved',1,0)) AS 'Approved',
      sum(IF(app_status = 'Completed', 1, 0)) AS 'Completed',

      sum(IF(app_status = 'Approved' AND subtype LIKE '%School%' ,1, 0)) AS 'School',
      sum(IF(app_status = 'Approved' AND subtype LIKE '%Madrasha%' ,1, 0)) AS 'Madrasha',
      sum(IF(app_status = 'Approved' AND subtype LIKE '%Mosque%' ,1, 0)) AS 'Mosque',
      sum(IF(app_status = 'Approved' AND subtype IN ( 'Community', 'Bazar', 'Slum') ,1, 0)) AS 'Community',
      sum(IF(app_status = 'Approved' AND ( malechamber + femalechamber < 3) ,1, 0)) AS 'TwoChamber',
      sum(IF(app_status = 'Approved' AND ( malechamber + femalechamber > 2 ) ,1, 0)) AS 'ThreeChamber',
      sum(IF(app_status = 'Approved' AND cons_type = 'New' ,1, 0)) AS 'New',
      sum(IF(app_status = 'Approved' AND cons_type = 'Renovation' ,1, 0)) AS 'Renovation'

      FROM
        sanitation
      INNER JOIN
        region ON region.id = sanitation.region_id
      GROUP BY
        sanitation.region_id
      ORDER BY
        region.region_name" )
      );

    return view($this->view_path.'summary', compact('datas'));
  }

  public function download($type)
  {
    if($type == "list"){
      return (new SanitationDownload)->download();
    }elseif($type == "summary"){
      return (new SummaryDownload)->download();
    }
  }

  public function edit($id)
  {

    $mainype = array(
      'Institutional latrine' => 'Institutional latrine',
      'Public latrine' => 'Public latrine',
      'Communal latrine' => 'Communal latrine');

    $subtype = array(
      'Primary School' => 'Primary School',
      'High School' => 'High School',
      'Bazar' => 'Bazar',
      'Madrasha' => 'Madrasha',
      'Mosque' => 'Mosque',
      'Slum' => 'Slum',
      'Community' => 'Community');

    $wordOrderNO = array(
      '1' => '1',
      '2' => '2',
      '3' => '3',
      '4' => '4');

    $appstatus = AppStatus::pluck('app_status','app_status')->toArray();
    $ImplementStatus=ImplementStatus::pluck('imp_status','imp_status')->toArray();


    $sanitation = Sanitation::findOrFail($id);
    return view($this->view_path.'edit', compact('sanitation', 'mainype', 'subtype', 'wordOrderNO', 'appstatus', 'ImplementStatus'));
  }

  public function update(Request $request, $id){
    $sanitation = Sanitation::findOrFail($id);
    $sanitation->fill($request->except('_token'));
    $sanitation->updated_by = auth()->user()->id;
    $sanitation->updated_at = date('Y-m-d H:i:s');
    $sanitation->update();
    \Session::flash('message', "One updated successfully");
    return redirect()->route('superadmin.sub-projects.sanitation.edit', $sanitation->id);
  }
}