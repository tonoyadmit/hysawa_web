<?php

namespace App\Http\Controllers\SuperAdmin\Water;

use App\Http\Controllers\Controller;
use App\ImplementStatus;
use App\Model\Download\Superadmin\Project\Water\WaterDownload;
use App\Model\Search\Request\WaterSearchRequest;
use App\Model\Search\WaterSearch;
use App\Water;
use App\WaterTech;
use Illuminate\Http\Request;

class WaterController extends Controller
{
  private $view_path = "core.superadmin.water.";
  private $route_path = "superadmin.water.";

  public function index( Request $request )
  {
    $waters = Water::take(100)->get();
    $old = [
      'starting_date' => '',
      'ending_date' => '',
      'created_by' => '',
      'distid' => '',
      'upid' => '',
      'unid' => '',
      'ward_no' => '',
      'village' => ''
    ];

    return view($this->view_path.'index', compact('waters', 'old'));
  }

  public function search(Request $request)
  {
    $waters = [];

    if($request->has('query')){

      $waterSearchRequest = new WaterSearchRequest;
      $waterSearchRequest->starting_date = $request->input('starting_date');
      $waterSearchRequest->ending_date = $request->input('ending_date');
      $waterSearchRequest->created_by = $request->input('created_by');
      $waterSearchRequest->distid = $request->input('distid');
      $waterSearchRequest->upid = $request->input('upid');
      $waterSearchRequest->unid = $request->input('unid');
      $waterSearchRequest->ward_no = $request->input('ward_no');
      $waterSearchRequest->village = $request->input('village');

      $waters = (new WaterSearch($waterSearchRequest))->get();

    }else{
      return redirect()->route($this->route_path.'index');
    }

    $old = [
      'query' => $request->input('query'),
      'starting_date' => $request->input('starting_date'),
      'ending_date' => $request->input('ending_date'),
      'created_by' => $request->input('created_by'),
      'distid' => $request->input('distid'),
      'upid' => $request->input('upid'),
      'unid' => $request->input('unid'),
      'ward_no' => $request->input('ward_no'),
      'village' => $request->input('village'),
      'download' => time()
    ];

    return view($this->view_path.'index', compact('waters', 'old'));
  }

  public function download(Request $request)
  {
    if($request->has('download'))
    {
      return (new WaterDownload($request))->download();
    }
  }

  public function create()
  {
    $up = array(
      '1' => '1',
      '2' => '2',
      '3' => '3',
      '4' => '4',
      '5' => '5',
      '6' => '6',
      '7' => '7',
      '8' => '8',
      '9' => '9');

    $wordOrderNO = array(
      '1' => '1',
      '2' => '2',
      '3' => '3',
      '4' => '4');

    $tech = WaterTech::pluck('techtype','id')->toArray();
    $ImplementStatus=ImplementStatus::pluck('imp_status','id')->toArray();
    return view($this->view_path.'insert', compact('up','tech','wordOrderNO','ImplementStatus'));
  }

  public function store(Request $request)
  {
    $validation = Validator::make($request->all(), [
       'Village'            => 'required',
       'TW_No'              => 'required',
       'Landowner'          => 'required',
       'Caretaker_male'     => 'required',
       'Caretaker_female'   => 'required'
    ]);

    if($validation->fails()) {
        \Session::flash('error', "Input Has Error.");
        return redirect()->back()->withErrors($validation)->withInput();
    }

    $water = new Water();
    $water->fill($request->except('_token'));

    $water->unid = auth()->user()->unid;
    $water->region_id = auth()->user()->region_id;
    $water->proj_id = auth()->user()->proj_id;
    $water->distid = auth()->user()->distid;
    $water->upid = auth()->user()->upid;

    $water->created_by = auth()->user()->id;
    $water->updated_by = auth()->user()->id;
    $water->save();

    \Session::flash('message', "New Data saved successfully");
    return redirect()->route($this->route_path.'create');
  }

  public function show($id)
  {
    $water = Water::findOrFail($id);
    return view($this->view_path.'view', compact('water'));
  }

  public function edit($id)
  {
    if(!Entrust::can('edit-water')) { abort(403); }

    $up = array('1' => '1','2' => '2','3' => '3','4' => '4','5' => '5','6' => '6','7' => '7','8' => '8','9' => '9');
    $wordOrderNO = array('1' => '1','2' => '2','3' => '3','4' => '4');
    $tech = WaterTech::pluck('techtype','id')->toArray();
    $ImplementStatus=ImplementStatus::pluck('imp_status','id')->toArray();
    $water = Water::where('id', '=', $id)->first();

    return view($this->view_path.'edit', compact('water','up','wordOrderNO','tech','ImplementStatus','water'));
  }

  public function update(Request $request, $id)
  {
    $validation = Validator::make($request->all(), [

       'Village'          => 'required',
       'TW_No'  => 'required',
       'Landowner'           => 'required',
       'Caretaker_male'          => 'required',
       'Caretaker_female'          => 'required'
    ]);

    if($validation->fails()) {
        return redirect()->back()->withErrors($validation)->withInput();
    }

    $water = Water::findOrFail($id);
    $water->fill($request->except('_token'));
    $water->updated_by = auth()->user()->id;
    $water->save();

    \Session::flash('message', "One updated successfully");
    return redirect()->route($this->route_path.'index');
  }

  public function map(Request $request)
  {
    return view('core.superadmin.map.water');
  }

  public function map2(Request $request)
  {
    return view('core.superadmin.map.water2');
  }
   public function mobile_map(Request $request)
  {
  
    return view('core.superadmin.map.mobmap');
  }

  public function mobile_map_2(Request $request)
  {
  
    return view('core.superadmin.map.mobmap_2');
  }
}