<?php

namespace App\Http\Controllers\SuperAdmin\Finance\ChartsOfAccount;

use App\Http\Controllers\Controller;
use App\Model\Download\Superadmin\Finance\Accounts\AccountItemDownload;
use Illuminate\Http\Request;

class ItemController extends Controller {

  private $view_path = "core.superadmin.finance.chartsOfAccount.item.";
  private $route_path = "superadmin.finance.charts-of-account.items.";


  public function index(Request $request)
  {

    if($request->has('download'))
    {
      return (new AccountItemDownload)->download();
    }

    $heads = \DB::table('fhead')->orderBy('headname')->get();
    $items = \DB::table('fitem')
            ->select(
              'fitem.id as id',
              'fhead.headname as head',
              'fsubhead.sname as subhead',
              'fitem.itemname as name'
              )
            ->leftjoin('fhead', 'fitem.headid', '=', 'fhead.id')
            ->leftjoin('fsubhead', 'fitem.subid', '=', 'fsubhead.id')
            ->orderBy('fhead.headname', 'fsubhead.sname')
            ->get();

    return view($this->view_path.'index', compact('heads', 'items'));
  }

  public function store(Request $request)
  {
    $oldItem = \DB::table('fitem')
                        ->where('headid', $request->input('headid'))
                        ->where('subid', $request->input('subid'))
                        ->where('itemname', $request->input('itemname'))
                        ->get();

    if(count($oldItem))
    {
      \Session::flash('error', "This Item is already exists.");
      return redirect()->route($this->route_path.'index');
    }

    $item = \DB::table('fitem')
                ->insert([
                    'headid' => $request->input('headid'),
                    'subid' => $request->input('subid'),
                    'itemname' => $request->input('itemname')
                  ]);

    \Session::flash('success', "New Item added.");
    return redirect()->route($this->route_path."index");
  }

  public function edit(Request $request, $id)
  {
    $heads = \DB::table('fhead')->orderBy('headname')->get();

    $item = \DB::table('fitem')
                  ->where('id', $id)
                  ->get();

    if(!count($item))
    {
      \Session::flash('error', "Item Not Found.");
      return redirect()->route($this->route_path.'index');
    }

    $item = $item->first();

    $subHeads = \DB::table('fsubhead')
            ->where('headid', $item->headid)
            ->get();


    return view($this->view_path.'edit', compact('heads', 'subHeads', 'item'));
  }

  public function update(Request $request, $id)
  {
    $items = \DB::table('fitem')
            ->where('id', $id)
            ->get();

    if(!count($items))
    {
      \Session::flash('error', "Item Not Found.");
      return redirect()->route($this->route_path.'index');
    }

    $oldItem = \DB::table('fitem')
                        ->where('headid', $request->input('headid'))
                        ->where('subid', $request->input('subid'))
                        ->where('itemname', $request->input('itemname'))
                        ->get();

    if(count($oldItem))
    {
      \Session::flash('error', "This Item is already exists.");
      return redirect()->route($this->route_path.'index');
    }

    \DB::table('fitem')->where('id', $id)
                      ->update([
                        'headid' => $request->input('headid'),
                        'subid' => $request->input('subid'),
                        'itemname' => $request->input('itemname')
                        ]);
    \Session::flash('success', "Item info updated.");
    return redirect()->route($this->route_path.'index');
  }
}