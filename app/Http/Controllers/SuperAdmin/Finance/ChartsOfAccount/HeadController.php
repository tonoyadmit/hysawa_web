<?php

namespace App\Http\Controllers\SuperAdmin\Finance\ChartsOfAccount;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HeadController extends Controller {

  private $view_path = "core.superadmin.finance.chartsOfAccount.head.";
  private $route_path = "superadmin.finance.charts-of-account.heads.";

  public function index()
  {
    $heads = \DB::table('fhead')->orderBy('headname')->get();
    return view($this->view_path.'index', compact('heads'));
  }

  public function store(Request $request)
  {
    $oldHead = \DB::table('fhead')->where('headname', $request->input('headname'))->get();
    if(count($oldHead)){
      \Session::flash('error', "This head is already exists.");
      return redirect()->route($this->route_path.'index');
    }
    $item = \DB::table('fhead')->insert(['headname' => $request->input('headname')]);
    \Session::flash('success', "New Head added.");
    return redirect()->route($this->route_path."index");
  }

  public function edit(Request $request, $id)
  {
    $head = \DB::table('fhead')->where('id', $id)->get();
    if(!count($head)){
      \Session::flash('error', "Head Item Not Found.");
      return redirect()->route($this->route_path.'index');
    }
    $head = $head->first();
    return view($this->view_path.'edit', compact('head'));
  }

  public function update(Request $request, $id)
  {
    $head = \DB::table('fhead')->where('id', $id)->get();
    if(!count($head)){
      \Session::flash('error', "Head Item Not Found.");
      return redirect()->route($this->route_path.'index');
    }
    \DB::table('fhead')->where('id', $id)->update(['headname' => $request->input('headname')]);
    \Session::flash('success', "Head info updated.");
    return redirect()->route($this->route_path.'index');
  }
}