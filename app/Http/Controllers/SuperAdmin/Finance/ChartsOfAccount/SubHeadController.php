<?php

namespace App\Http\Controllers\SuperAdmin\Finance\ChartsOfAccount;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class SubHeadController extends Controller {

  private $view_path = "core.superadmin.finance.chartsOfAccount.sub_head.";
  private $route_path = "superadmin.finance.charts-of-account.sub-heads.";


  public function index()
  {
    $heads = \DB::table('fhead')->orderBy('headname')->get();
    $subHeads = \DB::table('fsubhead')
            ->select('fsubhead.id as id', 'fsubhead.sname as sname', 'fhead.headname as headname')
            ->leftjoin('fhead', 'fsubhead.headid', '=', 'fhead.id')
            ->orderBy('fhead.headname')
            ->get();

    return view($this->view_path.'index', compact('heads', 'subHeads'));
  }

  public function store(Request $request)
  {
    $oldSubHead = \DB::table('fsubhead')
                        ->where('headid', $request->input('headid'))
                        ->where('sname', $request->input('sname'))
                        ->get();

    if(count($oldSubHead))
    {
      \Session::flash('error', "This Sub-head is already exists.");
      return redirect()->route($this->route_path.'index');
    }

    $item = \DB::table('fsubhead')
                ->insert([
                    'headid' => $request->input('headid'),
                    'sname' => $request->input('sname'),
                  ]);
    \Session::flash('success', "New Sub-Head added.");
    return redirect()->route($this->route_path."index");
  }

  public function edit(Request $request, $id)
  {
    $heads = \DB::table('fhead')->orderBy('headname')->get();

    $subHeads = \DB::table('fsubhead')
            ->select(
              'fsubhead.id as id',
              'fsubhead.sname as sname',
              'fhead.headname as headname',
              'fsubhead.headid as headid')
            ->where('fsubhead.id', $id)
            ->join('fhead', 'fsubhead.headid', '=', 'fhead.id')
            //->where('fsubhead.id', $id)
            ->get();
    if(!count($subHeads))
    {
      \Session::flash('error', "Sub-Head Item Not Found.");
      return redirect()->route($this->route_path.'index');
    }

    $subHead = $subHeads->first();

    return view($this->view_path.'edit', compact('heads', 'subHead'));
  }

  public function update(Request $request, $id)
  {
    $subHeads = \DB::table('fsubhead')
            ->where('fsubhead.id', $id)
            ->get();

    if(!count($subHeads))
    {
      \Session::flash('error', "Sub-Head Item Not Found.");
      return redirect()->route($this->route_path.'index');
    }

    $oldSubHead = \DB::table('fsubhead')
                        ->where('headid', $request->input('headid'))
                        ->where('sname', $request->input('sname'))
                        ->get();

    if(count($oldSubHead))
    {
      \Session::flash('error', "This Sub-head is already exists.");
      return redirect()->route($this->route_path.'index');
    }

    \DB::table('fsubhead')->where('id', $id)
                      ->update([
                        'headid' => $request->input('headid'),
                        'sname' => $request->input('sname'),
                        ]);
    \Session::flash('success', "Sub-Head info updated.");
    return redirect()->route($this->route_path.'index');
  }
}