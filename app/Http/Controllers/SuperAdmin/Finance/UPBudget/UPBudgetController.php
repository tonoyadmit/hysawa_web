<?php

namespace App\Http\Controllers\SuperAdmin\Finance\UPBudget;

use App\Http\Controllers\Controller;
use App\Model\District;
use App\Model\Download\Superadmin\Finance\UPBudget\UPBudgetDownload;
use App\Model\Union;
use App\Model\Upazila;
use Illuminate\Http\Request;

class UPBudgetController extends Controller {

  private $view_path = "core.superadmin.finance.up-budget.";
  private $route_path = "superadmin.finance.up-budget.";


  public function index(Request $request)
  {
    //dd($request->all());

    if($request->has('download'))
    {
      return (new UPBudgetDownload)->download();
    }

    $users = [];
    $districts = District::all();
    $unions = "";

    $old = [
      'district_id' => '',
      'upazila_id'  => '',
      'union_id'    => '',
      'project_id'  => '',
      'districtName'  => '',
      'upazilaName'   => '',
      'unionName'     => ''
    ];

    $budgets = "";

    if($request->has('query'))
    {
      $old = [
        'district_id' => $request->district_id,
        'upazila_id'  => $request->upazila_id,
        'union_id'    => $request->union_id,
        'districtName'  => '',
        'upazilaName'   => '',
        'unionName'     => '',
        'query'       => ''
      ];

      if($request->has('union_id') && $request->union_id != "")
      {
        $union_id = $request->union_id;

        $budgets = \DB::select(\DB::Raw("

          SELECT
            fhead.headname as head,
            fsubhead.sname as subhead,
            item_budget.id,
            item_budget.budget,
            item_budget.ubid,
            item_budget.s_year,
            item_budget.e_year,
            item_budget.ubid,
            fitem.itemname,
            fitem.headid,

            funion.distid,
            funion.upid,
            funion.unname

          FROM
            item_budget
          LEFT JOIN funion ON funion.id = item_budget.ubid
          LEFT JOIN fitem ON fitem.id = item_budget.itemid
          LEFT JOIN fsubhead ON fsubhead.id = fitem.subid
          LEFT JOIN fhead    ON fhead.id = fsubhead.headid

          WHERE
            item_budget.itemid = fitem.id AND
            item_budget.ubid = $union_id

          "));

        //dd($budgets);

        $union  = Union::find($request->union_id);
        $unions = Union::where('upid', $union->upid)->get();
        $old['unionName'] = $union->unname;

        $upazila  = Upazila::find($request->upazila_id);
        $upazilas = Upazila::where('disid', $upazila->disid)->get();
        $old['upazilaName'] = $upazila->upname;

        $district = District::find($request->district_id);
        $old['districtName'] = $district->distname;
      }

    }

    return view($this->view_path.'index', compact('districts', 'upazilas', 'unions', 'old', 'budgets'));
  }

  public function create()
  {
    $district = "";
    return view($this->view_path.'create', compact('district'));
  }
}