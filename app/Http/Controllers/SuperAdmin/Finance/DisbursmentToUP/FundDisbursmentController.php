<?php

namespace App\Http\Controllers\SuperAdmin\Finance\DisbursmentToUP;

use App\Budget;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FundDisbursmentController extends Controller {

  private $view_path = "core.superadmin.finance.disbursmentToUP.fundDisbursment.";
  private $route_path = "superadmin.";


  public function index()
  {
    $budgets = Budget::
       select(
        'budget.entry_date as entry_date',
        'budget.desdate as date',

        'fhead.headname as head',
        'fsubhead.sname as subhead',
        'fitem.itemname as item',

        'fdistrict.distname as district',
        'fupazila.upname as upazila',
        'funion.unname as union',

        'budget.vou as voucher',
        'budget.mode as mode',
        'budget.desbus as amount',
        'budget.remarks as remarks',

        'budget.id as id')

        ->leftjoin('fitem', 'budget.itemid', '=', 'fitem.id')
        ->leftjoin('fsubhead',  'fitem.subid',   '=', 'fsubhead.id')
        ->leftjoin('fhead',    'fsubhead.headid',   '=', 'fhead.id')

        ->leftjoin('fdistrict', 'budget.distid', '=', 'fdistrict.id')
        ->leftjoin('fupazila',  'budget.upid',   '=', 'fupazila.id')
        ->leftjoin('funion',    'budget.unid',   '=', 'funion.id')

        ->orderBy('budget.entry_date', 'DESC')->paginate(10);

    return view($this->view_path.'index_date', compact('budgets'));
  }

  public function index2(Request $request)
  {

    $budgets = Budget::
       select(
        'budget.entry_date as entry_date',
        'budget.desdate as date',

        'fhead.headname as head',
        'fsubhead.sname as subhead',
        'fitem.itemname as item',

        'fdistrict.distname as district',
        'fupazila.upname as upazila',
        'funion.unname as union',

        'budget.vou as voucher',
        'budget.mode as mode',
        'budget.desbus as amount',
        'budget.remarks as remarks',

        'budget.id as id')

        ->leftjoin('fitem', 'budget.itemid', '=', 'fitem.id')
        ->leftjoin('fsubhead',  'fitem.subid',   '=', 'fsubhead.id')
        ->leftjoin('fhead',    'fsubhead.headid',   '=', 'fhead.id')

        ->leftjoin('fdistrict', 'budget.distid', '=', 'fdistrict.id')
        ->leftjoin('fupazila',  'budget.upid',   '=', 'fupazila.id')
        ->leftjoin('funion',    'budget.unid',   '=', 'funion.id')

        ->orderBy('budget.unid', 'DESC')->paginate(10);

    return view($this->view_path.'index_up', compact('budgets'));
  }
}