<?php

namespace App\Http\Controllers\SuperAdmin\Finance\DisbursmentToUP;

use App\Budget;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DisbursmentController extends Controller {

  private $view_path = "core.superadmin.finance.disbursmentToUP.disbursment.";
  private $route_path = "superadmin.finance.disbursment-to-up.disbursment.";


  public function index(Request $request)
  {
    $districts = \DB::table('fdistrict')->get();

    $budgets = "";
    $old = [];

    if($request->has('query'))
    {

      $budgets = Budget::
        select(
        'fhead.headname as head',
        'fsubhead.sname as subhead',
        'fitem.itemname as item',
        'budget.desbus as amount',
        'budget.vou as voucher',
        'budget.desdate as date',
        'budget.remarks as remarks',
        'budget.id as id')
        ->where(function($q) use ($request){
          $q->where('unid', $request->input('union_id'));
        })

       ->leftjoin('fitem', 'budget.itemid', '=', 'fitem.id')
       ->leftjoin('fsubhead',  'fitem.subid',   '=', 'fsubhead.id')
       ->leftjoin('fhead',    'fsubhead.headid',   '=', 'fhead.id')

       ->orderBy('budget.id', 'DESC')->paginate(15);

    }else{
       $budgets = Budget::
       select(
        'fhead.headname as head',
        'fsubhead.sname as subhead',
        'fitem.itemname as item',
        'budget.desbus as amount',
        'budget.vou as voucher',
        'budget.desdate as date',
        'budget.remarks as remarks',
        'budget.id as id')
       ->leftjoin('fitem', 'budget.itemid', '=', 'fitem.id')
       ->leftjoin('fsubhead',  'fitem.subid',   '=', 'fsubhead.id')
       ->leftjoin('fhead',    'fsubhead.headid',   '=', 'fhead.id')
       ->orderBy('budget.id', 'DESC')->paginate(15);
    }

    return view($this->view_path.'index', compact('districts', 'budgets', 'old'));
  }

  public function create()
  {
    $districts = \DB::table('fdistrict')->get();
    $heads = \DB::table('fhead')->get();

    return view($this->view_path.'create', compact('districts', 'heads'));
  }

  public function store(Request $request)
  {
      $budget = new Budget;

      $budget->itemid = $request->input('itemid');
      $budget->desbus = $request->input('amount');
      $budget->mode = $request->input('mode');
      $budget->vou = $request->input('vou');
      $budget->desdate = date('Y-m-d', strtotime($request->input('date')));
      $budget->remarks = $request->input('remarks');

      $budget->distid = $request->input('distname');
      $budget->upid = $request->input('Upname');
      $budget->unid = $request->input('union_id');

      $union = \DB::table('funion')->where('id', $request->input('union_id'))->get();

      if(count($union) )
      {
        $union = $union->first();
      }

      $budget->proid = $union->proid;
      $budget->region_id = $union->region_id;

      $budget->userid = \Auth::user()->id;
      $budget->entry_date =date('Y-m-d');

      $budget->save();


    return redirect()->route($this->route_path."index");
  }


  public function edit(Request $request, $id)
  {
    $budget = Budget::findOrFail($id);

    $districts = \DB::table('fdistrict')->get();
    $heads = \DB::table('fhead')->get();

    $item = \DB::table('fitem')->find($budget->itemid);

    $headId    = $item->headid;
    $subHeadId = $item->subid;

    $subHeads = \DB::table('fsubhead')->where('headid', $headId)->get();
    $items    = \DB::table('fitem')->where('subid', $subHeadId)->get();
    $upazilas = \DB::table('fupazila')->where('disid', $budget->distid)->get();
    $unions   = \DB::table('funion')->where('upid', $budget->upid)->get();

    return view($this->view_path.'edit', compact(
      'districts',
      'heads',
      'budget',
      'headId',
      'subHeadId',
      'subHeads',
      'items',
      'upazilas',
      'unions'));
  }


  public function update(Request $request, $id)
  {
     // \Log::info($request->all());
      $budget = Budget::findOrFail($id);

      $budget->itemid = $request->input('itemid');
      $budget->desbus = $request->input('amount');
      $budget->mode = $request->input('mode');
      $budget->vou = $request->input('vou');
      $budget->desdate = date('Y-m-d', strtotime($request->input('date')));
      $budget->remarks = $request->input('remarks');

      $budget->distid = $request->input('distname');
      $budget->upid = $request->input('Upname');
      $budget->unid = $request->input('union_id');

      $union = \DB::table('funion')->where('id', $request->input('union_id'))->get();

      if(count($union) )
      {
        $union = $union->first();
      }

      $budget->proid = $union->proid;
      $budget->region_id = $union->region_id;

      $budget->update();

      //\Log::info($budget->toArray());

      return redirect()->route($this->route_path."index");
  }

}