<?php

namespace App\Http\Controllers\SuperAdmin\Finance\Report;

use App\Fhead;
use App\Http\Controllers\Controller;
use App\Model\District;
use App\Model\Download\Superadmin\Finance\Report\UpazilaDownload;
use App\Model\Union;
use App\Model\Upazila;
use Illuminate\Http\Request;

class UpazilaReportController extends Controller {

  private $view_path = "core.superadmin.finance.report.upazila.";

  public function index(Request $request)
  {

    if($request->has('download'))
    {
      return (new UpazilaDownload)->download($request);
    }

    $districts = District::orderBy('distname')->get();

    $old = [
      'district_id' => '',
      'upazila_id' => '',
      'starting_date' => '',
      'ending_date' => '',
      'districtName' => '',
      'upazilaName' => ''
    ];

    $UNIds = [];

    if($request->has('query'))
    {
      $upazila = Upazila::with('district')->where('id', $request->input('upazila_id'))->first();
      $old = [
        'query' => $request->input('query'),
        'district_id'    => $request->input('district_id'),
        'upazila_id'     => $request->input('upazila_id'),
        'starting_date'  => $request->input('starting_date'),
        'ending_date'    => $request->input('ending_date'),
        'districtName'   => $upazila->district->distname,
        'upazilaName'    => $upazila->upname,
      ];

      $UNIds = Union::where('upid', $request->input('upazila_id'))->get(['id']);

      $d=Fhead::with('subhead.subItem')->get()->toArray();

      $data=\DB::select(\DB::raw("
        SELECT
          bIncome,
          bExpenditure,
          cIncome,
          cExpenditure,
          bIncome-bExpenditure AS opening_bank,
          cIncome-cExpenditure AS opening_cash,
          Income-Expenditure AS total
        FROM(
          SELECT *,
          SUM(IF(trans_type = 'in' AND MODE='bank', amount, 0)) AS 'bIncome',
          SUM(IF(trans_type = 'ex' AND MODE='bank', amount, 0)) AS 'bExpenditure',
          SUM(IF(trans_type = 'in' AND MODE='cash', amount, 0)) AS 'cIncome',
          SUM(IF(trans_type = 'ex' AND MODE='cash', amount, 0)) AS 'cExpenditure',
          SUM(IF(trans_type = 'in', amount, 0)) AS 'Income',
          SUM(IF(trans_type = 'ex', amount, 0)) AS 'Expenditure',
          SUM(amount) AS Total
          FROM
          fdata WHERE upid =".$request->input('upazila_id')."   AND
          DATE < '".$request->input('starting_date')."') AS t"));

      $data2=\DB::select(\DB::raw("
        SELECT
          bIncome,
          bExpenditure,
          cIncome,
          cExpenditure,
          bIncome-bExpenditure AS opening_bank,
          cIncome-cExpenditure AS opening_cash,
          Income-Expenditure AS total
        FROM(
          SELECT *,
          SUM(IF(trans_type = 'in' AND MODE='bank', amount, 0)) AS 'bIncome',
          SUM(IF(trans_type = 'ex' AND MODE='bank', amount, 0)) AS 'bExpenditure',
          SUM(IF(trans_type = 'in' AND MODE='cash', amount, 0)) AS 'cIncome',
          SUM(IF(trans_type = 'ex' AND MODE='cash', amount, 0)) AS 'cExpenditure',
          SUM(IF(trans_type = 'in', amount, 0)) AS 'Income',
          SUM(IF(trans_type = 'ex', amount, 0)) AS 'Expenditure',
          SUM(amount) AS Total
          FROM
            fdata
          WHERE
            upid =".$request->input('upazila_id')." AND
            DATE < '".$request->input('ending_date')."') AS t"));

      if(count($data))
      {
         $data= $data[0];
      }
      if(count($data2))
      {
         $data2= $data2[0];
      }

      if($request->has('print'))
      {
        return view($this->view_path.'print', compact('districts', 'upazila', 'old', 'UNIds', 'd', 'data', 'data2'));
      }
    }
    return view($this->view_path.'index', compact('districts', 'upazila', 'old', 'UNIds', 'd', 'data', 'data2'));
  }
}