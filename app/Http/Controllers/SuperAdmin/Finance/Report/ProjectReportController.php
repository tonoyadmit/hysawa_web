<?php

namespace App\Http\Controllers\SuperAdmin\Finance\Report;

use App\Http\Controllers\Controller;
use App\Model\Demand;
use App\Model\Download\Superadmin\Finance\Report\ProjectDownload;
use App\Model\FinanceData;
use App\Model\ItemBudget;
use App\Model\Project;
use Illuminate\Http\Request;

class ProjectReportController extends Controller {

  private $view_path = "core.superadmin.finance.report.project.";

  public function index(Request $request)
  {

    if($request->has('download') && $request->download == "download")
    {
      return (new ProjectDownload)->download($request);
    }

    $projects = Project::orderBy('project')->get();

    $financeDataMain = "";
    $itemBudgets = "";
    $demands = "";

    $old = [
      'project_id' => '',
      'starting_date' => '',
      'ending_date' => '',
      'projectName' => ''
    ];

    if($request->has('query'))
    {
      $project = Project::where('id', $request->input('project_id'))->first();

      $old = [
        'project_id'    => $request->input('project_id'),
        'starting_date'  => $request->input('starting_date'),
        'ending_date'    => $request->input('ending_date'),
        'projectName'   => $project->project
      ];
      $demands         = Demand::orderBy('item')->get();
      $itemBudgets     = ItemBudget::where('distid', $request->input('district_id'))->get();
      $financeDataMain = FinanceData::where('proid', $request->input('project_id'))->get();
    }
    return view($this->view_path.'index', compact('projects', 'old', 'financeDataMain', "itemBudgets", 'demands' ));
  }
}