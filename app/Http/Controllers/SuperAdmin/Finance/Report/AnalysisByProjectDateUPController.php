<?php

namespace App\Http\Controllers\SuperAdmin\Finance\Report;

use App\Http\Controllers\Controller;
use App\Model\Demand;
use App\Model\Download\Superadmin\Finance\Report\AnalysisDownload;
use App\Model\FinanceData;
use App\Model\ItemBudget;
use App\Model\Project;
use Illuminate\Http\Request;

class AnalysisByProjectDateUPController extends Controller {

  private $view_path = "core.superadmin.finance.report.analysis.";

  public function index(Request $request)
  {

    if($request->has('download'))
    {
      return (new AnalysisDownload)->download($request);
    }

    $projects = Project::orderBy('project')->get();

    $old = [
      'project_id' => '',
      'starting_date' => '',
      'ending_date' => '',
      'projectName' => ''
    ];

    $unions = "";
    $sql = "";
    $sql2 = "";
    if($request->has('query'))
    {
      $project = Project::where('id', $request->input('project_id'))->first();

      $old = [
        'project_id'     => $request->input('project_id'),
        'starting_date'  => $request->input('starting_date'),
        'ending_date'    => $request->input('ending_date'),
        'projectName'    => $project->project
      ];

      $sql = "";
      $sql2 = "";

      if($request->has('project_id') && $request->project_id != "" &&
        $request->has('starting_date') && $request->starting_date != "" &&
        $request->has('ending_date') && $request->ending_date != ""
      )
      {
        $sql .= " and desdate between '$request->starting_date' and '$request->ending_date' and proid=$request->project_id";
        $sql2 .= " and date between '$request->starting_date' and '$request->ending_date' and proid=$request->project_id";
      }

      $unions = \DB::table('osm_mou')
                    ->select(
                      "fdistrict.id",
                      "fdistrict.distname",
                      "fupazila.id",
                      "fupazila.upname",
                      "funion.id as unid",
                      "funion.unname")
                    ->leftjoin('fdistrict', 'osm_mou.distid', '=', 'fdistrict.id')
                    ->leftjoin('fupazila', 'osm_mou.upid', '=', 'fupazila.id')
                    ->leftjoin('funion', 'osm_mou.unid', '=', 'funion.id')
                    ->where('osm_mou.projid', $request->project_id)
                    ->get();
    }

    return view($this->view_path.'index', compact('projects', 'old', 'unions', 'sql', 'sql2' ));
  }
}