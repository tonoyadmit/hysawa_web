<?php

namespace App\Http\Controllers\SuperAdmin\Finance\Report;

use App\Fhead;
use App\Http\Controllers\Controller;
use App\Model\Demand;
use App\Model\District;
use App\Model\Download\Superadmin\Finance\Report\DistrictDownload;
use App\Model\FinanceData;
use App\Model\ItemBudget;
use Illuminate\Http\Request;

class DistrictReportController extends Controller {

  private $view_path = "core.superadmin.finance.report.district.";
  private $route_path = "superadmin.";


  public function index(Request $request)
  {

    if($request->has('download'))
    {
      return (new DistrictDownload)->download($request);
    }

    $districts = District::orderBy('distname')->get();

    $old = [
      'district_id' => '',
      'starting_date' => '',
      'ending_date' => '',
      'districtName' => ''
    ];

    $demands = "";
    $itemBudgets = "";
    $financeDataMain = "";

    if($request->has('query'))
    {
      $district = District::where('id', $request->input('district_id'))->first();
      $old = [
        'query' => $request->input('query'),
        'district_id'    => $request->input('district_id'),
        'starting_date'  => $request->input('starting_date'),
        'ending_date'    => $request->input('ending_date'),
        'districtName'   => $district->distname
      ];

      $districtForId = District::with('upazilas.unions')->where('id', $request->input('district_id'))->get();

      $unIds = [];
      foreach($districtForId as $d)
      {
        foreach($d->upazilas as $up)
        {
          foreach($up->unions as $un)
          {
            $unIds[] = $un->id;
          }
        }
      }

      // get the demand data
      $demands         = Demand::whereIn('unid', $unIds)->get();
      $itemBudgets     = ItemBudget::where('distid', $request->input('district_id'))->get();
      $financeDataMain = FinanceData::where('distid', $request->input('district_id'))->get();

      $d=Fhead::with('subhead.subItem')->get()->toArray();

      $data=\DB::select(\DB::raw("
        SELECT
          bIncome,
          bExpenditure,
          cIncome,
          cExpenditure,
          bIncome-bExpenditure AS opening_bank,
          cIncome-cExpenditure AS opening_cash,
          Income-Expenditure AS total
        FROM(
          SELECT *,
          SUM(IF(trans_type = 'in' AND MODE='bank', amount, 0)) AS 'bIncome',
          SUM(IF(trans_type = 'ex' AND MODE='bank', amount, 0)) AS 'bExpenditure',
          SUM(IF(trans_type = 'in' AND MODE='cash', amount, 0)) AS 'cIncome',
          SUM(IF(trans_type = 'ex' AND MODE='cash', amount, 0)) AS 'cExpenditure',
          SUM(IF(trans_type = 'in', amount, 0)) AS 'Income',
          SUM(IF(trans_type = 'ex', amount, 0)) AS 'Expenditure',
          SUM(amount) AS Total
          FROM
          fdata WHERE distid =".$request->input('district_id')."   AND
          DATE < '".$request->input('starting_date')."') AS t"));

      $data2=\DB::select(\DB::raw("
        SELECT
          bIncome,
          bExpenditure,
          cIncome,
          cExpenditure,
          bIncome-bExpenditure AS opening_bank,
          cIncome-cExpenditure AS opening_cash,
          Income-Expenditure AS total
        FROM(
          SELECT *,
          SUM(IF(trans_type = 'in' AND MODE='bank', amount, 0)) AS 'bIncome',
          SUM(IF(trans_type = 'ex' AND MODE='bank', amount, 0)) AS 'bExpenditure',
          SUM(IF(trans_type = 'in' AND MODE='cash', amount, 0)) AS 'cIncome',
          SUM(IF(trans_type = 'ex' AND MODE='cash', amount, 0)) AS 'cExpenditure',
          SUM(IF(trans_type = 'in', amount, 0)) AS 'Income',
          SUM(IF(trans_type = 'ex', amount, 0)) AS 'Expenditure',
          SUM(amount) AS Total
          FROM
            fdata
          WHERE
            distid =".$request->input('district_id')." AND
            DATE < '".$request->input('ending_date')."') AS t"));

      if(count($data))
      {
         $data= $data[0];
      }

      if(count($data2))
      {
         $data2= $data2[0];
      }

      if($request->has('print'))
      {
        //dd($request->all());
        return view($this->view_path.'print', compact(
            'districts',
            'upazila',
            'old',
            'demands',
            'itemBudgets',
            'financeDataMain',
            'd',
            'data',
            'data2'));
      }
    }
    return view($this->view_path.'index', compact(
        'districts', 'old', 'demands', 'itemBudgets', 'financeDataMain','d',
            'data',
            'data2'));
  }
}