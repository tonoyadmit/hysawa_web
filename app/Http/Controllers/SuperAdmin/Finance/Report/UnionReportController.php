<?php

namespace App\Http\Controllers\SuperAdmin\Finance\Report;

use App\Fhead;
use App\Http\Controllers\Controller;
use App\Model\District;
use App\Model\Download\Superadmin\Finance\Report\UnionDownload;
use App\Model\Union;
use Illuminate\Http\Request;

class UnionReportController extends Controller {

  private $view_path = "core.superadmin.finance.report.union.";

  public function index(Request $request)
  {

    if($request->has('download'))
    {
      return (new UnionDownload)->download($request);
    }

  	$districts = District::orderBy('distname')->get();
  	$upazilas = "";
  	$unions = "";
  	$old = [
  		'district_id' => '',
  		'upazila_id' => '',
  		'union_id' => '',
  		'starting_date' => '',
  		'ending_date' => '',
  		'districtName' => '',
  		'upazilaName' => '',
  		'unionName' => ''
  	];

  	if($request->has('query'))
  	{
  		$union = Union::with('upazila.district')->where('id', $request->input('union_id'))->first();

  		$old = [
        'query'          => $request->input('query'),
	  		'district_id'    => $request->input('district_id'),
	  		'upazila_id'     => $request->input('upazila_id'),
	  		'union_id'       => $request->input('union_id'),
	  		'starting_date'  => $request->input('starting_date'),
        'ending_date'    => $request->input('ending_date'),
        'project_id'    => $request->input('project_id'),
        'region_id'    => $request->input('region_id'),
	  		'districtName'   => $union->upazila->district->distname,
	  		'upazilaName'    => $union->upazila->upname,
	  		'unionName'      => $union->unname
	  	];


      $d=Fhead::with('subhead.subItem')->get()->toArray();

        $data=\DB::select(\DB::raw("
          SELECT
            bIncome,
            bExpenditure,
            cIncome,
            cExpenditure,
            bIncome-bExpenditure AS opening_bank,
            cIncome-cExpenditure AS opening_cash,
            Income-Expenditure AS total
          FROM(
            SELECT *,
            SUM(IF(trans_type = 'in' AND MODE='bank', amount, 0)) AS 'bIncome',
            SUM(IF(trans_type = 'ex' AND MODE='bank', amount, 0)) AS 'bExpenditure',
            SUM(IF(trans_type = 'in' AND MODE='cash', amount, 0)) AS 'cIncome',
            SUM(IF(trans_type = 'ex' AND MODE='cash', amount, 0)) AS 'cExpenditure',
            SUM(IF(trans_type = 'in', amount, 0)) AS 'Income',
            SUM(IF(trans_type = 'ex', amount, 0)) AS 'Expenditure',
            SUM(amount) AS Total
            FROM
            fdata WHERE unid =".$old['union_id']." AND 	proid =".$old['project_id']." AND region_id=".$old['region_id']."  AND
            DATE < '".$request->input('starting_date')."'
          ) AS t"));

        $data2=\DB::select(\DB::raw("
          SELECT
            bIncome,
            bExpenditure,
            cIncome,
            cExpenditure,
            bIncome-bExpenditure AS opening_bank,
            cIncome-cExpenditure AS opening_cash,
            Income-Expenditure AS total
          FROM(
            SELECT *,
            SUM(IF(trans_type = 'in' AND MODE='bank', amount, 0)) AS 'bIncome',
            SUM(IF(trans_type = 'ex' AND MODE='bank', amount, 0)) AS 'bExpenditure',
            SUM(IF(trans_type = 'in' AND MODE='cash', amount, 0)) AS 'cIncome',
            SUM(IF(trans_type = 'ex' AND MODE='cash', amount, 0)) AS 'cExpenditure',
            SUM(IF(trans_type = 'in', amount, 0)) AS 'Income',
            SUM(IF(trans_type = 'ex', amount, 0)) AS 'Expenditure',
            SUM(amount) AS Total
            FROM
              fdata
            WHERE
              unid =".$old['union_id']." AND 	proid =".$old['project_id']." AND region_id=".$old['region_id']." AND
              DATE < '".$request->input('ending_date')."') AS t"));

          if(count($data))
          {
             $data= $data[0];
          }
          if(count($data2))
          {
             $data2= $data2[0];
          }

          if($request->has('print'))
          {
            return view($this->view_path.'print', compact('old', 'd', 'data', 'data2'));
          }
          
  	}
    return view($this->view_path.'index', compact('districts','old', 'd', 'data', 'data2'));
  }
}