<?php

namespace App\Http\Controllers\SuperAdmin\Finance\Report;

use App\Fhead;
use App\Http\Controllers\Controller;
use App\Model\Demand;
use App\Model\Download\Superadmin\Finance\Report\ConsolidatedDownload;
use App\Model\FinanceData;
use App\Model\Head;
use App\Model\ItemBudget;
use Illuminate\Http\Request;

class ConsolidatedReportController extends Controller {

  private $view_path = "core.superadmin.finance.report.consolidated.";

  public function index(Request $request)
  {


    // $old = [
    //   'query' => '',
    //   'starting_date' => '',
    //   'ending_date' => ''
    // ];

    // $demands = "";
    // $itemBudgets = "";
    // $financeDataMain = "";
    // $heads = "";

    // if($request->has('query'))
    // {
    //   $old = [
    //     'query' => $request->input('query'),
    //     'starting_date'  => $request->input('starting_date'),
    //     'ending_date'    => $request->input('ending_date'),
    //   ];
    //   $demands         = Demand::orderBy('item')->get();
    //   $itemBudgets     = ItemBudget::orderBy('itemid')->get();
    //   $financeDataMain = FinanceData::orderBy('item')->get();

    //   $heads = Head::with('subheads.items.financeDatas')
    //                 ->where('id', '!=', 11)
    //                 ->where('id', '!=', 15)
    //                 ->orderBy('id')
    //                 ->get();
    // }

    // return view($this->view_path.'index', compact('demands', 'itemBudgets', 'financeDataMain', 'old', 'heads'));


    $old = [
      'region_id' => '',
      'starting_date' => '',
      'ending_date' => ''
    ];
    $data = "";
    $data2 = "";

    if($request->has('query'))
    {
      $old = [
        'starting_date'  => $request->input('starting_date'),
        'ending_date'    => $request->input('ending_date'),
        'query'          => $request->input('query')
      ];

      $d = Fhead::with('subhead.subItem')->get()->toArray();

      $data = \DB::select(\DB::raw("
        SELECT
          bIncome,
          bExpenditure,
          cIncome,
          cExpenditure,
          bIncome-bExpenditure AS opening_bank,
          cIncome-cExpenditure AS opening_cash,
          Income-Expenditure AS total
        FROM(
          SELECT *,
                SUM(IF(trans_type = 'in' AND MODE='bank', amount, 0)) AS 'bIncome',
                SUM(IF(trans_type = 'ex' AND MODE='bank', amount, 0)) AS 'bExpenditure',
                SUM(IF(trans_type = 'in' AND MODE='cash', amount, 0)) AS 'cIncome',
                SUM(IF(trans_type = 'ex' AND MODE='cash', amount, 0)) AS 'cExpenditure',
                SUM(IF(trans_type = 'in', amount, 0)) AS 'Income',
                SUM(IF(trans_type = 'ex', amount, 0)) AS 'Expenditure',
                SUM(amount) AS Total
          FROM
            fdata
          WHERE
            DATE < '".$request->input('starting_date')."'
        ) AS t"));

      $data2 = \DB::select(\DB::raw("
        SELECT
          bIncome,
          bExpenditure,
          cIncome,
          cExpenditure,
          bIncome-bExpenditure AS opening_bank,
          cIncome-cExpenditure AS opening_cash,
          Income-Expenditure AS total
        FROM(
          SELECT *,
            SUM(IF(trans_type = 'in' AND MODE='bank', amount, 0)) AS 'bIncome',
            SUM(IF(trans_type = 'ex' AND MODE='bank', amount, 0)) AS 'bExpenditure',
            SUM(IF(trans_type = 'in' AND MODE='cash', amount, 0)) AS 'cIncome',
            SUM(IF(trans_type = 'ex' AND MODE='cash', amount, 0)) AS 'cExpenditure',
            SUM(IF(trans_type = 'in', amount, 0)) AS 'Income',
            SUM(IF(trans_type = 'ex', amount, 0)) AS 'Expenditure',
            SUM(amount) AS Total
          FROM
            fdata
          WHERE
            DATE < '".$request->input('ending_date')."'
        ) AS t"));

        if(count($data)){
           $data= $data[0];
        }
        if(count($data2)){
           $data2= $data2[0];
        }
    }

    //dd($data2);

    if($request->has('print'))
    {
      return view($this->view_path.'print', compact('old', 'data', 'd', 'data2'));
    }

    if($request->has('download'))
    {
      return (new ConsolidatedDownload)->download($request);
    }


    return view($this->view_path.'index', compact('old', 'data', 'd', 'data2'));
  }
}