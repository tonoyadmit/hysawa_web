<?php

namespace App\Http\Controllers\SuperAdmin\Finance\Report;

use App\Http\Controllers\Controller;
use App\Model\Download\Superadmin\Finance\Report\SummaryDownload;
use App\Model\Union;
use Illuminate\Http\Request;

class SummaryByUnionReportController extends Controller {

  private $view_path = "core.superadmin.finance.report.summary.";
  private $route_path = "superadmin.";

  public function index(Request $request)
  {
    //\DB::enableQueryLog();
    if($request->has('download'))
    {
      return (new SummaryDownload)->download();
    }

    $unions = Union::with('upazila.district')
                  ->with('financeDatas')
                  ->orderBy('distid')
                  ->orderBy('upid')
                  ->paginate(10);

    return view($this->view_path.'index', compact('unions'));
  }
}