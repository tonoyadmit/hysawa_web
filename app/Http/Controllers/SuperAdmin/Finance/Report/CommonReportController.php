<?php

namespace App\Http\Controllers\SuperAdmin\Finance\Report;

use App\Http\Controllers\Controller;
use App\Model\Report\Finance\FinanceReportGenerator;
use Illuminate\Http\Request;

class CommonReportController extends Controller {

  private $view_path = "core.superadmin.finance.report.common.";

  public function index(Request $request)
  {
    $old = [
      'district_id' => '',
      'upazila_id' => '',
      'union_id' => '',
      'starting_date' => '',
      'ending_date' => ''
    ];

    if($request->has('query'))
    {
      $old = [
        'query'          => $request->input('query'),
        'district_id'    => $request->input('district_id'),
        'upazila_id'     => $request->input('upazila_id'),
        'union_id'       => $request->input('union_id'),
        'starting_date'  => $request->input('starting_date'),
        'ending_date'    => $request->input('ending_date'),
      ];

      try{
        $report = new FinanceReportGenerator($request);
      }catch(\Exception $e){
        \Log::critical($e->getMessage());
        \Session::flash('error', 'Data Not Found!');
      }

      if($request->has('download')){
        return $report->download();
      }

      if($request->has('print')){
        return view($this->view_path.'print', compact('report'));
      }

    }



    return view($this->view_path.'index', compact( 'old', 'report' ));
  }
}