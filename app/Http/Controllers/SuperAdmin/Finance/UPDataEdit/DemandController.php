<?php

namespace App\Http\Controllers\SuperAdmin\Finance\UPDataEdit;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class DemandController extends Controller {

  private $view_path = "core.superadmin.finance.upDataEdit.demand.";
  private $route_path = "superadmin.";


  public function index()
  {
    return view($this->view_path.'index', compact(''));
  }
}