<?php

namespace App\Http\Controllers\SuperAdmin\Finance\UpDemandM;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Item;
use App\User;
use Datatables;
use App\Role;
use App\Fhead;
use App\Fsubhead;
use App\Fitemlist;
use App\FinanceData;
use App\FundList;
use App\FinanceDemand;
use Validator;
use Session;
use Redirect;
use DB;
use Auth;
use Entrust;

use App\Model\Search\FundListDataListSearch;


class FundListMController extends Controller
{
  private $view_path = "core.superadmin.finance.up-demand-m.fundlist.";
  private $route_path = "superadmin.up-demand-m.survey_details.";

  public function index( Request $request )
  {
    
    if ($request->has('query')) {
      // $FinanceDemands = FundList::where('unid', '=', $request->union_id)
      // ->where('distid', '=',$request->district_id)
      // ->where('upid', '=', $request->upazila_id)
      // ->where('proj_id', '=', $request->project_id)
      // ->orderBy('id', 'desc')
      // ->get();

      $s = $request->starting_date." 00:00:00";
      $f= $request->ending_date." 23:59:00";

       if(!empty($request->project_id) && !empty($request->district_id) && !empty($request->upazila_id) && !empty($request->union_id) && !empty($request->starting_date) && !empty($request->ending_date)){
       
        // echo "1";
        // exit();

      // $FinanceDemands = \DB::select(\DB::Raw("select * from mob_survey where proj_id = ".$request->project_id." AND
       // distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."') ORDER BY id desc"));
        
     $FinanceDemands = DB::table('mob_survey')
     ->where('proj_id', '=',$request->project_id)
     ->where('distid', '=',$request->district_id)
     ->where('upid', '=',$request->upazila_id)
     ->where('unid', '=',$request->union_id)
     ->where('date', '>=',$s)
     ->where('date', '<=',$f)
     ->orderBy('id', 'desc')
     ->paginate(10);

     
        $rep_datas = \DB::select(\DB::Raw("select (select 
count(functional) from mob_survey where functional=1) as fc1, 
(select count(functional) from mob_survey where functional=2 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as fc2,
(select count(functional) from mob_survey where functional=3 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as fc3, 
(select count(p1) from mob_survey where p1=1 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as p1, 
(select count(p2) from mob_survey where p2=2 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as p2, 
(select count(p3) from mob_survey where p3=3 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as p3, 
(select count(p4) from mob_survey where p4=4 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as p4, 
(select count(p5) from mob_survey where p5=5 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as p5, 
(select count(p6) from mob_survey where p6=6 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as p6, 
(select count(r1) from mob_survey where r1=1 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as r1, 
(select count(r2) from mob_survey where r2=2 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as r2, 
(select count(r3) from mob_survey where r3=3 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as r3, 
(select count(r4) from mob_survey where r4=4 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as r4
from mob_survey LIMIT 1"));


        }

        elseif(!empty($request->starting_date) && !empty($request->ending_date)){
       
          // echo "1";
          // exit();
  
        // $FinanceDemands = \DB::select(\DB::Raw("select * from mob_survey where proj_id = ".$request->project_id." AND
         // distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."') ORDER BY id desc"));
          
       $FinanceDemands = DB::table('mob_survey')
       ->where('date', '>=',$s)
       ->where('date', '<=',$f)
       ->orderBy('id', 'desc')
       ->paginate(10);
  
       
          $rep_datas = \DB::select(\DB::Raw("select (select 
  count(functional) from mob_survey where functional=1) as fc1, 
  (select count(functional) from mob_survey where functional=2 and (date BETWEEN '".$s."' AND '".$f."')) as fc2,
  (select count(functional) from mob_survey where functional=3 and  (date BETWEEN '".$s."' AND '".$f."')) as fc3, 
  (select count(p1) from mob_survey where p1=1 and  (date BETWEEN '".$s."' AND '".$f."')) as p1, 
  (select count(p2) from mob_survey where p2=2 and  (date BETWEEN '".$s."' AND '".$f."')) as p2, 
  (select count(p3) from mob_survey where p3=3 and  (date BETWEEN '".$s."' AND '".$f."')) as p3, 
  (select count(p4) from mob_survey where p4=4 and  (date BETWEEN '".$s."' AND '".$f."')) as p4, 
  (select count(p5) from mob_survey where p5=5 and  (date BETWEEN '".$s."' AND '".$f."')) as p5, 
  (select count(p6) from mob_survey where p6=6 and  (date BETWEEN '".$s."' AND '".$f."')) as p6, 
  (select count(r1) from mob_survey where r1=1 and  (date BETWEEN '".$s."' AND '".$f."')) as r1, 
  (select count(r2) from mob_survey where r2=2 and  (date BETWEEN '".$s."' AND '".$f."')) as r2, 
  (select count(r3) from mob_survey where r3=3 and  (date BETWEEN '".$s."' AND '".$f."')) as r3, 
  (select count(r4) from mob_survey where r4=4 and  (date BETWEEN '".$s."' AND '".$f."')) as r4
  from mob_survey LIMIT 1"));
  
  
          }


      elseif(!empty($request->project_id) && !empty($request->district_id) && !empty($request->upazila_id) && !empty($request->union_id)){
        // echo "2";
        // exit();

     // $FinanceDemands = \DB::select(\DB::Raw("select * from mob_survey where proj_id = ".$request->project_id." AND
         // distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." ORDER BY id desc"));
          
          $FinanceDemands = DB::table('mob_survey')
          ->where('proj_id', '=',$request->project_id)
          ->where('distid', '=',$request->district_id)
          ->where('upid', '=',$request->upazila_id)
          ->where('unid', '=',$request->union_id)
          ->orderBy('id', 'desc')
          ->paginate(10);
     
          $rep_datas = \DB::select(\DB::Raw("select (select 
          count(functional) from mob_survey where functional=1) as fc1, 
          (select count(functional) from mob_survey where functional=2 and proj_id = ".$request->project_id." AND
          distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as fc2,
          (select count(functional) from mob_survey where functional=3 and proj_id = ".$request->project_id." AND
          distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as fc3, 
          (select count(p1) from mob_survey where p1=1 and proj_id = ".$request->project_id." AND
          distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as p1, 
          (select count(p2) from mob_survey where p2=2 and proj_id = ".$request->project_id." AND
          distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as p2, 
          (select count(p3) from mob_survey where p3=3 and proj_id = ".$request->project_id." AND
          distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as p3, 
          (select count(p4) from mob_survey where p4=4 and proj_id = ".$request->project_id." AND
          distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as p4, 
          (select count(p5) from mob_survey where p5=5 and proj_id = ".$request->project_id." AND
          distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as p5, 
          (select count(p6) from mob_survey where p6=6 and proj_id = ".$request->project_id." AND
          distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as p6, 
          (select count(r1) from mob_survey where r1=1 and proj_id = ".$request->project_id." AND
          distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as r1, 
          (select count(r2) from mob_survey where r2=2 and proj_id = ".$request->project_id." AND
          distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as r2, 
          (select count(r3) from mob_survey where r3=3 and proj_id = ".$request->project_id." AND
          distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as r3, 
          (select count(r4) from mob_survey where r4=4 and proj_id = ".$request->project_id." AND
          distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as r4
          from mob_survey LIMIT 1"));
          
       }

     elseif(!empty($request->project_id) && !empty($request->district_id) && !empty($request->upazila_id)){
      // echo "3";
      // exit();

     //  $FinanceDemands = \DB::select(\DB::Raw("select * from mob_survey where proj_id = ".$request->project_id." AND
       // distid = ".$request->district_id." AND upid = ".$request->upazila_id." ORDER BY id desc"));
    
    $FinanceDemands = DB::table('mob_survey')
    ->where('proj_id', '=',$request->project_id)
    ->where('distid', '=',$request->district_id)
    ->where('upid', '=',$request->upazila_id)
    ->orderBy('id', 'desc')
    ->paginate(10);


    $rep_datas = \DB::select(\DB::Raw("select (select 
    count(functional) from mob_survey where functional=1) as fc1, 
    (select count(functional) from mob_survey where functional=2 and proj_id = ".$request->project_id." AND
    distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as fc2,
    (select count(functional) from mob_survey where functional=3 and proj_id = ".$request->project_id." AND
    distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as fc3, 
    (select count(p1) from mob_survey where p1=1 and proj_id = ".$request->project_id." AND
    distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as p1, 
    (select count(p2) from mob_survey where p2=2 and proj_id = ".$request->project_id." AND
    distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as p2, 
    (select count(p3) from mob_survey where p3=3 and proj_id = ".$request->project_id." AND
    distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as p3, 
    (select count(p4) from mob_survey where p4=4 and proj_id = ".$request->project_id." AND
    distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as p4, 
    (select count(p5) from mob_survey where p5=5 and proj_id = ".$request->project_id." AND
    distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as p5, 
    (select count(p6) from mob_survey where p6=6 and proj_id = ".$request->project_id." AND
    distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as p6, 
    (select count(r1) from mob_survey where r1=1 and proj_id = ".$request->project_id." AND
    distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as r1, 
    (select count(r2) from mob_survey where r2=2 and proj_id = ".$request->project_id." AND
    distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as r2, 
    (select count(r3) from mob_survey where r3=3 and proj_id = ".$request->project_id." AND
    distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as r3, 
    (select count(r4) from mob_survey where r4=4 and proj_id = ".$request->project_id." AND
    distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as r4
    from mob_survey LIMIT 1"));
  
      }
      
      elseif(!empty($request->project_id) && !empty($request->district_id)){
        // echo "4";
        // exit();
      
     // $FinanceDemands = \DB::select(\DB::Raw("select * from mob_survey where proj_id = ".$request->project_id." AND
       // distid = ".$request->district_id." ORDER BY id desc"));
        
        $FinanceDemands = DB::table('mob_survey')
        ->where('proj_id', '=',$request->project_id)
        ->where('distid', '=',$request->district_id)
        ->orderBy('id', 'desc')
        ->paginate(10);


        $rep_datas = \DB::select(\DB::Raw("select (select 
count(functional) from mob_survey where functional=1) as fc1, 
(select count(functional) from mob_survey where functional=2 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as fc2,
(select count(functional) from mob_survey where functional=3 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as fc3, 
(select count(p1) from mob_survey where p1=1 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as p1, 
(select count(p2) from mob_survey where p2=2 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as p2, 
(select count(p3) from mob_survey where p3=3 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as p3, 
(select count(p4) from mob_survey where p4=4 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as p4, 
(select count(p5) from mob_survey where p5=5 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as p5, 
(select count(p6) from mob_survey where p6=6 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as p6, 
(select count(r1) from mob_survey where r1=1 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as r1, 
(select count(r2) from mob_survey where r2=2 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as r2, 
(select count(r3) from mob_survey where r3=3 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as r3, 
(select count(r4) from mob_survey where r4=4 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as r4
from mob_survey LIMIT 1"));


      }
     
      elseif(!empty($request->project_id)){

        // echo "5";
        // exit();

       // $FinanceDemands = \DB::select(\DB::Raw("select * from mob_survey where proj_id = ".$request->project_id." ORDER BY id desc"));

        $FinanceDemands = DB::table('mob_survey')
        ->where('proj_id', '=',$request->project_id)
        ->orderBy('id', 'desc')
        ->paginate(10);

        $rep_datas = \DB::select(\DB::Raw("select (select 
        count(functional) from mob_survey where functional=1) as fc1, 
        (select count(functional) from mob_survey where functional=2 and proj_id = ".$request->project_id.") as fc2,
        (select count(functional) from mob_survey where functional=3 and proj_id = ".$request->project_id.") as fc3, 
        (select count(p1) from mob_survey where p1=1 and proj_id = ".$request->project_id.") as p1, 
        (select count(p2) from mob_survey where p2=2 and proj_id = ".$request->project_id.") as p2, 
        (select count(p3) from mob_survey where p3=3 and proj_id = ".$request->project_id.") as p3, 
        (select count(p4) from mob_survey where p4=4 and proj_id = ".$request->project_id.") as p4, 
        (select count(p5) from mob_survey where p5=5 and proj_id = ".$request->project_id.") as p5, 
        (select count(p6) from mob_survey where p6=6 and proj_id = ".$request->project_id.") as p6, 
        (select count(r1) from mob_survey where r1=1 and proj_id = ".$request->project_id.") as r1, 
        (select count(r2) from mob_survey where r2=2 and proj_id = ".$request->project_id.") as r2, 
        (select count(r3) from mob_survey where r3=3 and proj_id = ".$request->project_id.") as r3, 
        (select count(r4) from mob_survey where r4=4 and proj_id = ".$request->project_id.") as r4
        from mob_survey LIMIT 1"));

      }
      elseif(!empty($request->district_id)){
        // echo "4";
        // exit();
      
     // $FinanceDemands = \DB::select(\DB::Raw("select * from mob_survey where proj_id = ".$request->project_id." AND
       // distid = ".$request->district_id." ORDER BY id desc"));
        
        $FinanceDemands = DB::table('mob_survey')
        ->where('distid', '=',$request->district_id)
        ->orderBy('id', 'desc')
        ->paginate(10);


        $rep_datas = \DB::select(\DB::Raw("select (select 
count(functional) from mob_survey where functional=1) as fc1, 
(select count(functional) from mob_survey where functional=2 and
distid = ".$request->district_id.") as fc2,
(select count(functional) from mob_survey where functional=3 and 
distid = ".$request->district_id.") as fc3, 
(select count(p1) from mob_survey where p1=1 and
distid = ".$request->district_id.") as p1, 
(select count(p2) from mob_survey where p2=2 and
distid = ".$request->district_id.") as p2, 
(select count(p3) from mob_survey where p3=3 and
distid = ".$request->district_id.") as p3, 
(select count(p4) from mob_survey where p4=4 and
distid = ".$request->district_id.") as p4, 
(select count(p5) from mob_survey where p5=5 and
distid = ".$request->district_id.") as p5, 
(select count(p6) from mob_survey where p6=6 and
distid = ".$request->district_id.") as p6, 
(select count(r1) from mob_survey where r1=1 and
distid = ".$request->district_id.") as r1, 
(select count(r2) from mob_survey where r2=2 and
distid = ".$request->district_id.") as r2, 
(select count(r3) from mob_survey where r3=3 and
distid = ".$request->district_id.") as r3, 
(select count(r4) from mob_survey where r4=4 and
distid = ".$request->district_id.") as r4
from mob_survey LIMIT 1"));


      }

      

$old = [
        'district_id' => '',
        'project_id'   => '',
        'upazila_id'  => '',
        'union_id'    => '',
        'starting_date'    => '',
        'ending_date'    => '',
      ];

if ($request->has('query')) {
$old = [
        'query'       => $request->input('query'),
        'project_id'   => $request->project_id,
        'district_id' => $request->district_id,
        'upazila_id'  => $request->upazila_id,
        'union_id'    => $request->union_id,
        'starting_date'    => $request->starting_date,
        'ending_date'    => $request->ending_date
      ];
}
    }else{



    //  $FinanceDemands = \DB::select(\DB::Raw("select * from mob_survey ORDER BY id desc"));

     $FinanceDemands = DB::table('mob_survey')
     ->orderBy('id', 'desc')
     ->paginate(10);

      $rep_datas = \DB::select(\DB::Raw("select (select 
      count(functional) from mob_survey where functional=1) as fc1, 
      (select count(functional) from mob_survey where functional=2) as fc2,
      (select count(functional) from mob_survey where functional=3) as fc3, 
      (select count(p1) from mob_survey where p1=1) as p1, 
      (select count(p2) from mob_survey where p2=2) as p2, 
      (select count(p3) from mob_survey where p3=3) as p3, 
      (select count(p4) from mob_survey where p4=4) as p4, 
      (select count(p5) from mob_survey where p5=5) as p5, 
      (select count(p6) from mob_survey where p6=6) as p6, 
      (select count(r1) from mob_survey where r1=1) as r1, 
      (select count(r2) from mob_survey where r2=2) as r2, 
      (select count(r3) from mob_survey where r3=3) as r3, 
      (select count(r4) from mob_survey where r4=4) as r4
      from mob_survey LIMIT 1"));



$old = [
        'district_id' => '',
        'project_id'   => '',
        'upazila_id'  => '',
        'union_id'    => '',
      ];

if ($request->has('query')) {
$old = [
        'query'       => $request->input('query'),
        'project_id'   => $request->input('project_id'),
        'district_id' => $request->input('district_id'),
        'upazila_id'  => $request->input('upazila_id'),
        'union_id'    => $request->input('union_id')
      ];
}






    }


      $tecs = (new FundListDataListSearch($request, true))->get();
    //$datas = (new FundListDataListSearch($request, true))->get();
 //    return view($this->view_path.'index', compact('datas', 'old')); 


    return view($this->view_path.'index', compact('FinanceDemands','tecs', 'old','rep_datas'));
  }

  public function create()
  {
    $items = Item::with('head')->groupBy('headid')->get();
    $head = [];
    foreach($items as $item){
      $head[$item->head->id] = $item->head->headname;
    }
    return view($this->view_path.'insert', compact('head'));
  }

  public function store(Request $request)
  {
    $validation = Validator::make($request->all(), [

      'fdate' => 'required',
      'tdate' => 'required'
      ]);

    if($validation->fails()) {
      return Redirect::back()->withErrors($validation)->withInput();
    }

    $FinanceData = new FundList();
    $FinanceData->fill($request->except('_token'));
    $FinanceData->unid = auth()->user()->unid;
    $FinanceData->userid=auth()->user()->id;
    $FinanceData->created_by = auth()->user()->id;
    $FinanceData->updated_by = auth()->user()->id;

    $FinanceData->distid = auth()->user()->distid;
    $FinanceData->upid = auth()->user()->upid;
    $FinanceData->region_id = auth()->user()->region_id;
    $FinanceData->proj_id = auth()->user()->proj_id;

    $FinanceData->save();

    Session::flash('message', "New Data saved successfully");
    return redirect()->route($this->route_path.'index');
  }

  public function edit($id)
  {
    // $head = Fhead::pluck('headname','id')->toArray();
    $FinanceData = FundList::findOrFail($id);
    // $subhead = Fsubhead::where('headid', '=',$FinanceData["head"])->pluck('sname','id')->toArray();
    // $subitem = Fitemlist::where('headid', '=',$FinanceData["head"])->where('subid', '=',$FinanceData["subhead"])->pluck('itemname','id')->toArray();

    return view($this->view_path.'edit', compact('FinanceData'));;
  }

  public function update(Request $request, $id)
  {
    $validation = Validator::make($request->all(), [
      // 'head'          => 'required',
      // 'subhead'  => 'required',
      // 'item'           => 'required',
      'fdate'          => 'required',
      'tdate'          => 'required'
      ]);

    if($validation->fails()) {
      return Redirect::back()->withErrors($validation)->withInput();
    }

    $FinanceData = FundList::findOrFail($id);
    $FinanceData->fill($request->except('_token'));
    $FinanceData->updated_by = auth()->user()->id;
    $FinanceData->save();

    \Session::flash('message', "One updated successfully");
    return redirect()->route($this->route_path.'index');
  }

  public function l(Request $request)
  {
    $requestData = $request->all();
    return $subhead = Fsubhead::where('headid', '=',$requestData["head"])->pluck('sname','id')->toArray();
  }

  public function subitem(Request $request)
  {
    $requestData = $request->all();
    return $subhead = Fitemlist::where('headid', '=',$requestData["head"])->where('subid', '=',$requestData["subhead"])->pluck('itemname','id')->toArray();
  }

  public function print(Request $request)
  {

    $s = $request->starting_date." 00:00:00";
    $f= $request->ending_date." 23:59:00";

     if(!empty($request->project_id) && !empty($request->district_id) && !empty($request->upazila_id) && !empty($request->union_id) && !empty($request->starting_date) && !empty($request->ending_date)){
     
      // echo "1";
      // exit();

    // $FinanceDemands = \DB::select(\DB::Raw("select * from mob_survey where proj_id = ".$request->project_id." AND
     // distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."') ORDER BY id desc"));
      
   $FinanceDemands = DB::table('mob_survey')
   ->where('proj_id', '=',$request->project_id)
   ->where('distid', '=',$request->district_id)
   ->where('upid', '=',$request->upazila_id)
   ->where('unid', '=',$request->union_id)
   ->where('date', '>=',$s)
   ->where('date', '<=',$f)
   ->orderBy('id', 'desc')
   ->paginate(1000);


   
      $rep_datas = \DB::select(\DB::Raw("select (select 
count(functional) from mob_survey where functional=1) as fc1, 
(select count(functional) from mob_survey where functional=2 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as fc2,
(select count(functional) from mob_survey where functional=3 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as fc3, 
(select count(p1) from mob_survey where p1=1 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as p1, 
(select count(p2) from mob_survey where p2=2 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as p2, 
(select count(p3) from mob_survey where p3=3 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as p3, 
(select count(p4) from mob_survey where p4=4 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as p4, 
(select count(p5) from mob_survey where p5=5 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as p5, 
(select count(p6) from mob_survey where p6=6 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as p6, 
(select count(r1) from mob_survey where r1=1 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as r1, 
(select count(r2) from mob_survey where r2=2 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as r2, 
(select count(r3) from mob_survey where r3=3 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as r3, 
(select count(r4) from mob_survey where r4=4 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."')) as r4
from mob_survey LIMIT 1"));


      }

      elseif(!empty($request->starting_date) && !empty($request->ending_date)){
       
        // echo "1";
        // exit();

      // $FinanceDemands = \DB::select(\DB::Raw("select * from mob_survey where proj_id = ".$request->project_id." AND
       // distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." AND (date BETWEEN '".$s."' AND '".$f."') ORDER BY id desc"));
        
     $FinanceDemands = DB::table('mob_survey')
     ->where('date', '>=',$s)
     ->where('date', '<=',$f)
     ->orderBy('id', 'desc')
     ->paginate(1000);

     
        $rep_datas = \DB::select(\DB::Raw("select (select 
count(functional) from mob_survey where functional=1) as fc1, 
(select count(functional) from mob_survey where functional=2 and (date BETWEEN '".$s."' AND '".$f."')) as fc2,
(select count(functional) from mob_survey where functional=3 and  (date BETWEEN '".$s."' AND '".$f."')) as fc3, 
(select count(p1) from mob_survey where p1=1 and  (date BETWEEN '".$s."' AND '".$f."')) as p1, 
(select count(p2) from mob_survey where p2=2 and  (date BETWEEN '".$s."' AND '".$f."')) as p2, 
(select count(p3) from mob_survey where p3=3 and  (date BETWEEN '".$s."' AND '".$f."')) as p3, 
(select count(p4) from mob_survey where p4=4 and  (date BETWEEN '".$s."' AND '".$f."')) as p4, 
(select count(p5) from mob_survey where p5=5 and  (date BETWEEN '".$s."' AND '".$f."')) as p5, 
(select count(p6) from mob_survey where p6=6 and  (date BETWEEN '".$s."' AND '".$f."')) as p6, 
(select count(r1) from mob_survey where r1=1 and  (date BETWEEN '".$s."' AND '".$f."')) as r1, 
(select count(r2) from mob_survey where r2=2 and  (date BETWEEN '".$s."' AND '".$f."')) as r2, 
(select count(r3) from mob_survey where r3=3 and  (date BETWEEN '".$s."' AND '".$f."')) as r3, 
(select count(r4) from mob_survey where r4=4 and  (date BETWEEN '".$s."' AND '".$f."')) as r4
from mob_survey LIMIT 1"));


        }

    elseif(!empty($request->project_id) && !empty($request->district_id) && !empty($request->upazila_id) && !empty($request->union_id)){
      // echo "2";
      // exit();

   // $FinanceDemands = \DB::select(\DB::Raw("select * from mob_survey where proj_id = ".$request->project_id." AND
       // distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id." ORDER BY id desc"));
        
        $FinanceDemands = DB::table('mob_survey')
        ->where('proj_id', '=',$request->project_id)
        ->where('distid', '=',$request->district_id)
        ->where('upid', '=',$request->upazila_id)
        ->where('unid', '=',$request->union_id)
        ->orderBy('id', 'desc')
        ->paginate(1000);

   
        $rep_datas = \DB::select(\DB::Raw("select (select 
        count(functional) from mob_survey where functional=1) as fc1, 
        (select count(functional) from mob_survey where functional=2 and proj_id = ".$request->project_id." AND
        distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as fc2,
        (select count(functional) from mob_survey where functional=3 and proj_id = ".$request->project_id." AND
        distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as fc3, 
        (select count(p1) from mob_survey where p1=1 and proj_id = ".$request->project_id." AND
        distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as p1, 
        (select count(p2) from mob_survey where p2=2 and proj_id = ".$request->project_id." AND
        distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as p2, 
        (select count(p3) from mob_survey where p3=3 and proj_id = ".$request->project_id." AND
        distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as p3, 
        (select count(p4) from mob_survey where p4=4 and proj_id = ".$request->project_id." AND
        distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as p4, 
        (select count(p5) from mob_survey where p5=5 and proj_id = ".$request->project_id." AND
        distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as p5, 
        (select count(p6) from mob_survey where p6=6 and proj_id = ".$request->project_id." AND
        distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as p6, 
        (select count(r1) from mob_survey where r1=1 and proj_id = ".$request->project_id." AND
        distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as r1, 
        (select count(r2) from mob_survey where r2=2 and proj_id = ".$request->project_id." AND
        distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as r2, 
        (select count(r3) from mob_survey where r3=3 and proj_id = ".$request->project_id." AND
        distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as r3, 
        (select count(r4) from mob_survey where r4=4 and proj_id = ".$request->project_id." AND
        distid = ".$request->district_id." AND upid = ".$request->upazila_id." AND unid = ".$request->union_id.") as r4
        from mob_survey LIMIT 1"));
        
     }

   elseif(!empty($request->project_id) && !empty($request->district_id) && !empty($request->upazila_id)){
    // echo "3";
    // exit();

   //  $FinanceDemands = \DB::select(\DB::Raw("select * from mob_survey where proj_id = ".$request->project_id." AND
     // distid = ".$request->district_id." AND upid = ".$request->upazila_id." ORDER BY id desc"));
  
  $FinanceDemands = DB::table('mob_survey')
  ->where('proj_id', '=',$request->project_id)
  ->where('distid', '=',$request->district_id)
  ->where('upid', '=',$request->upazila_id)
  ->orderBy('id', 'desc')
  ->paginate(1000);



  $rep_datas = \DB::select(\DB::Raw("select (select 
  count(functional) from mob_survey where functional=1) as fc1, 
  (select count(functional) from mob_survey where functional=2 and proj_id = ".$request->project_id." AND
  distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as fc2,
  (select count(functional) from mob_survey where functional=3 and proj_id = ".$request->project_id." AND
  distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as fc3, 
  (select count(p1) from mob_survey where p1=1 and proj_id = ".$request->project_id." AND
  distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as p1, 
  (select count(p2) from mob_survey where p2=2 and proj_id = ".$request->project_id." AND
  distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as p2, 
  (select count(p3) from mob_survey where p3=3 and proj_id = ".$request->project_id." AND
  distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as p3, 
  (select count(p4) from mob_survey where p4=4 and proj_id = ".$request->project_id." AND
  distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as p4, 
  (select count(p5) from mob_survey where p5=5 and proj_id = ".$request->project_id." AND
  distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as p5, 
  (select count(p6) from mob_survey where p6=6 and proj_id = ".$request->project_id." AND
  distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as p6, 
  (select count(r1) from mob_survey where r1=1 and proj_id = ".$request->project_id." AND
  distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as r1, 
  (select count(r2) from mob_survey where r2=2 and proj_id = ".$request->project_id." AND
  distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as r2, 
  (select count(r3) from mob_survey where r3=3 and proj_id = ".$request->project_id." AND
  distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as r3, 
  (select count(r4) from mob_survey where r4=4 and proj_id = ".$request->project_id." AND
  distid = ".$request->district_id." AND upid = ".$request->upazila_id.") as r4
  from mob_survey LIMIT 1"));

    }
    
    elseif(!empty($request->project_id) && !empty($request->district_id)){
      // echo "4";
      // exit();
    
   // $FinanceDemands = \DB::select(\DB::Raw("select * from mob_survey where proj_id = ".$request->project_id." AND
     // distid = ".$request->district_id." ORDER BY id desc"));
      
      $FinanceDemands = DB::table('mob_survey')
      ->where('proj_id', '=',$request->project_id)
      ->where('distid', '=',$request->district_id)
      ->orderBy('id', 'desc')
      ->paginate(1000);



      $rep_datas = \DB::select(\DB::Raw("select (select 
count(functional) from mob_survey where functional=1) as fc1, 
(select count(functional) from mob_survey where functional=2 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as fc2,
(select count(functional) from mob_survey where functional=3 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as fc3, 
(select count(p1) from mob_survey where p1=1 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as p1, 
(select count(p2) from mob_survey where p2=2 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as p2, 
(select count(p3) from mob_survey where p3=3 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as p3, 
(select count(p4) from mob_survey where p4=4 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as p4, 
(select count(p5) from mob_survey where p5=5 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as p5, 
(select count(p6) from mob_survey where p6=6 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as p6, 
(select count(r1) from mob_survey where r1=1 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as r1, 
(select count(r2) from mob_survey where r2=2 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as r2, 
(select count(r3) from mob_survey where r3=3 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as r3, 
(select count(r4) from mob_survey where r4=4 and proj_id = ".$request->project_id." AND
distid = ".$request->district_id.") as r4
from mob_survey LIMIT 1"));


    }
   
    elseif(!empty($request->project_id)){

      // echo "5";
      // exit();

     // $FinanceDemands = \DB::select(\DB::Raw("select * from mob_survey where proj_id = ".$request->project_id." ORDER BY id desc"));

      $FinanceDemands = DB::table('mob_survey')
      ->where('proj_id', '=',$request->project_id)
      ->orderBy('id', 'desc')
      ->paginate(1000);

      $rep_datas = \DB::select(\DB::Raw("select (select 
      count(functional) from mob_survey where functional=1) as fc1, 
      (select count(functional) from mob_survey where functional=2 and proj_id = ".$request->project_id.") as fc2,
      (select count(functional) from mob_survey where functional=3 and proj_id = ".$request->project_id.") as fc3, 
      (select count(p1) from mob_survey where p1=1 and proj_id = ".$request->project_id.") as p1, 
      (select count(p2) from mob_survey where p2=2 and proj_id = ".$request->project_id.") as p2, 
      (select count(p3) from mob_survey where p3=3 and proj_id = ".$request->project_id.") as p3, 
      (select count(p4) from mob_survey where p4=4 and proj_id = ".$request->project_id.") as p4, 
      (select count(p5) from mob_survey where p5=5 and proj_id = ".$request->project_id.") as p5, 
      (select count(p6) from mob_survey where p6=6 and proj_id = ".$request->project_id.") as p6, 
      (select count(r1) from mob_survey where r1=1 and proj_id = ".$request->project_id.") as r1, 
      (select count(r2) from mob_survey where r2=2 and proj_id = ".$request->project_id.") as r2, 
      (select count(r3) from mob_survey where r3=3 and proj_id = ".$request->project_id.") as r3, 
      (select count(r4) from mob_survey where r4=4 and proj_id = ".$request->project_id.") as r4
      from mob_survey LIMIT 1"));

    }
    elseif(!empty($request->district_id)){
      // echo "4";
      // exit();
    
   // $FinanceDemands = \DB::select(\DB::Raw("select * from mob_survey where proj_id = ".$request->project_id." AND
     // distid = ".$request->district_id." ORDER BY id desc"));
      
      $FinanceDemands = DB::table('mob_survey')
      ->where('distid', '=',$request->district_id)
      ->orderBy('id', 'desc')
      ->paginate(1000);


      $rep_datas = \DB::select(\DB::Raw("select (select 
count(functional) from mob_survey where functional=1) as fc1, 
(select count(functional) from mob_survey where functional=2 and
distid = ".$request->district_id.") as fc2,
(select count(functional) from mob_survey where functional=3 and 
distid = ".$request->district_id.") as fc3, 
(select count(p1) from mob_survey where p1=1 and
distid = ".$request->district_id.") as p1, 
(select count(p2) from mob_survey where p2=2 and
distid = ".$request->district_id.") as p2, 
(select count(p3) from mob_survey where p3=3 and
distid = ".$request->district_id.") as p3, 
(select count(p4) from mob_survey where p4=4 and
distid = ".$request->district_id.") as p4, 
(select count(p5) from mob_survey where p5=5 and
distid = ".$request->district_id.") as p5, 
(select count(p6) from mob_survey where p6=6 and
distid = ".$request->district_id.") as p6, 
(select count(r1) from mob_survey where r1=1 and
distid = ".$request->district_id.") as r1, 
(select count(r2) from mob_survey where r2=2 and
distid = ".$request->district_id.") as r2, 
(select count(r3) from mob_survey where r3=3 and
distid = ".$request->district_id.") as r3, 
(select count(r4) from mob_survey where r4=4 and
distid = ".$request->district_id.") as r4
from mob_survey LIMIT 1"));


    }

    
    return view($this->view_path.'print', compact('FinanceDemands','tecs', 'old','rep_datas'));
  }


}
