<?php

namespace App\Http\Controllers\SuperAdmin\Finance\UpDemand;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Item;
use App\User;
use Datatables;
use App\Role;
use App\Fhead;
use App\Fsubhead;
use App\Fitemlist;
use App\FinanceData;
use App\FundList;
use App\FinanceDemand;
use Validator;
use Session;
use Redirect;
use DB;
use Auth;
use Entrust;

use App\Model\Search\FundListDataListSearch;


class FundListController extends Controller
{
  private $view_path = "core.superadmin.finance.up-demand.fundlist.";
  private $route_path = "superadmin.up-demand.fund_list.";

  public function index( Request $request )
  {
    if ($request->has('query')) {
      $FinanceDemands = FundList::where('unid', '=', $request->union_id)
      ->where('distid', '=',$request->district_id)
      ->where('upid', '=', $request->upazila_id)
      ->where('proj_id', '=', $request->project_id)
      ->orderBy('id', 'desc')
      ->get();

$old = [
        'district_id' => '',
        'project_id'   => '',
        'upazila_id'  => '',
        'union_id'    => '',
      ];

if ($request->has('query')) {
$old = [
        'query'       => $request->input('query'),
        'project_id'   => $request->project_id,
        'district_id' => $request->district_id,
        'upazila_id'  => $request->upazila_id,
        'union_id'    => $request->union_id
      ];
}
    }else{
      $FinanceDemands = FundList::where('unid', '=', 0)
      ->where('region_id', '=', 0)
      ->where('proj_id', '=', 0)
      ->orderBy('id', 'desc')
      ->get();

$old = [
        'district_id' => '',
        'project_id'   => '',
        'upazila_id'  => '',
        'union_id'    => '',
      ];

if ($request->has('query')) {
$old = [
        'query'       => $request->input('query'),
        'project_id'   => $request->input('project_id'),
        'district_id' => $request->input('district_id'),
        'upazila_id'  => $request->input('upazila_id'),
        'union_id'    => $request->input('union_id')
      ];
}
    }


      $tecs = (new FundListDataListSearch($request, true))->get();
    //$datas = (new FundListDataListSearch($request, true))->get();
 //    return view($this->view_path.'index', compact('datas', 'old')); 


    return view($this->view_path.'index', compact('FinanceDemands','tecs', 'old'));
  }

  public function create()
  {
    $items = Item::with('head')->groupBy('headid')->get();
    $head = [];
    foreach($items as $item){
      $head[$item->head->id] = $item->head->headname;
    }
    return view($this->view_path.'insert', compact('head'));
  }

  public function store(Request $request)
  {
    $validation = Validator::make($request->all(), [

      'fdate' => 'required',
      'tdate' => 'required'
      ]);

    if($validation->fails()) {
      return Redirect::back()->withErrors($validation)->withInput();
    }

    $FinanceData = new FundList();
    $FinanceData->fill($request->except('_token'));
    $FinanceData->unid = auth()->user()->unid;
    $FinanceData->userid=auth()->user()->id;
    $FinanceData->created_by = auth()->user()->id;
    $FinanceData->updated_by = auth()->user()->id;

    $FinanceData->distid = auth()->user()->distid;
    $FinanceData->upid = auth()->user()->upid;
    $FinanceData->region_id = auth()->user()->region_id;
    $FinanceData->proj_id = auth()->user()->proj_id;

    $FinanceData->save();

    Session::flash('message', "New Data saved successfully");
    return redirect()->route($this->route_path.'index');
  }

  public function edit($id)
  {
    // $head = Fhead::pluck('headname','id')->toArray();
    $FinanceData = FundList::findOrFail($id);
    // $subhead = Fsubhead::where('headid', '=',$FinanceData["head"])->pluck('sname','id')->toArray();
    // $subitem = Fitemlist::where('headid', '=',$FinanceData["head"])->where('subid', '=',$FinanceData["subhead"])->pluck('itemname','id')->toArray();

    return view($this->view_path.'edit', compact('FinanceData'));;
  }

  public function update(Request $request, $id)
  {
    $validation = Validator::make($request->all(), [
      // 'head'          => 'required',
      // 'subhead'  => 'required',
      // 'item'           => 'required',
      'fdate'          => 'required',
      'tdate'          => 'required'
      ]);

    if($validation->fails()) {
      return Redirect::back()->withErrors($validation)->withInput();
    }

    $FinanceData = FundList::findOrFail($id);
    $FinanceData->fill($request->except('_token'));
    $FinanceData->updated_by = auth()->user()->id;
    $FinanceData->save();

    \Session::flash('message', "One updated successfully");
    return redirect()->route($this->route_path.'index');
  }

  public function l(Request $request)
  {
    $requestData = $request->all();
    return $subhead = Fsubhead::where('headid', '=',$requestData["head"])->pluck('sname','id')->toArray();
  }

  public function subitem(Request $request)
  {
    $requestData = $request->all();
    return $subhead = Fitemlist::where('headid', '=',$requestData["head"])->where('subid', '=',$requestData["subhead"])->pluck('itemname','id')->toArray();
  }

  public function print()
  {

    $FinanceDemands = FinanceDemand::with('getHead', 'getSubhead', 'getItem')
    ->where('unid', '=',auth()->user()->unid)
    ->where('region_id', '=',auth()->user()->region_id)
    ->where('proj_id', '=',auth()->user()->proj_id)
    ->orderBy('id', 'desc')
    ->get();
   return view($this->view_path.'print', compact('FinanceDemands'));
    //return view($this->view_path.'print')->with('FinanceDemands',(new FinanceDemand($request))->get());
 
  }


}
