<?php

namespace App\Http\Controllers\SuperAdmin\UP;

use App\Http\Controllers\Controller;
use App\Model\District;
use App\Model\Download\Superadmin\UP\MOUDownload;
use App\Model\Project;
use App\Model\Region;
use Illuminate\Http\Request;


class MOUController extends Controller {

  private $view_path = "core.superadmin.up.mou.";
  private $route_path = "superadmin.up.mou.";

  public function index( Request $request )
  {

    if($request->has('download'))
    {
      return (new MOUDownload)->download();
    }


    $projects = \DB::select(\DB::Raw("
      SELECT project.id,
        project.project,
        count(osm_mou.moudate) as total
      FROM
        osm_mou, project
      WHERE
        project.id = osm_mou.projid
      GROUP BY
        project.id
      ORDER BY
        project.project
    "));

    $regions = \DB::select(\DB::Raw("
      SELECT project.project, region.region_id, region.region_name,count(osm_mou.moudate) as total
      FROM osm_mou, region, project
      WHERE region.region_id = osm_mou.region_id and project.id = osm_mou.projid
      GROUP BY project.id, region.region_id
      ORDER BY project.project"));
    $districts = \DB::select(\DB::Raw("
      SELECT project.project, fdistrict.id, fdistrict.distname,count(osm_mou.moudate) as total
      FROM osm_mou, fdistrict, project
      WHERE fdistrict.id = osm_mou.distid and project.id = osm_mou.projid
      GROUP BY project.project, fdistrict.id
      ORDER BY project.project"));

    $unions =\DB::select(\DB::Raw("
      SELECT
        project.project,
        fdistrict.distname,
        fupazila.upname,
        funion.unname,
        osm_mou.moudate,
        osm_mou.remarks,
        osm_mou.id
      FROM
        osm_mou,
        fdistrict,
        fupazila,
        funion,
        project
      WHERE
        osm_mou.distid = fdistrict.id AND
        osm_mou.upid = fupazila.id AND
        osm_mou.unid =funion.id and
        project.id = osm_mou.projid
      ORDER BY project.project, fdistrict.distname, fupazila.upname, funion.unname"));

    return view($this->view_path.'index', compact('projects', 'regions', 'districts', 'unions'));
  }

  public function create()
  {
    $districts = District::all();
    $projects  = Project::all();
    $regions   = Region::all();
    return view($this->view_path.'create', compact('districts', 'projects', 'regions'));
  }

  public function store(Request $request)
  {
    \DB::table('osm_mou')->insert([
        'moudate'     => $request->input('contractdate'),
        'region_id'   => $request->input('region_id'),
        'projid'      => $request->input('project_id'),
        'distid'      => $request->input('district_id'),
        'upid'        => $request->input('upazila_id'),
        'unid'        => $request->input('union_id'),
        'remarks'     => $request->input('remarks')
      ]);
    return redirect()->route($this->route_path.'index');
  }

  public function project(Request $request, $id)
  {
    $project = Project::find($id);

    $projects = \DB::select(\DB::Raw("
      SELECT
      region.region_name, fdistrict.distname, fupazila.upname, funion.unname,osm_mou.moudate
      FROM osm_mou, region, fdistrict, fupazila, funion
      WHERE
      osm_mou.projid = $id and
      osm_mou.region_id = region.region_id and
      osm_mou.distid = fdistrict.id and
      osm_mou.upid = fupazila.id and
      osm_mou.unid = funion.id
    "));



    return view($this->view_path.'project', compact('project', 'projects'));
  }

  public function region(Request $request, $id)
  {
    $region = Region::find($id);
    $regions = \DB::select(\DB::Raw("
      SELECT project.project, fdistrict.distname, fupazila.upname, funion.unname,osm_mou.moudate
      FROM osm_mou, project, fdistrict, fupazila, funion
      WHERE osm_mou.region_id = $id and  osm_mou.projid = project.id and osm_mou.distid = fdistrict.id and osm_mou.upid = fupazila.id and osm_mou.unid = funion.id
      "));
    return view($this->view_path.'region', compact('region', 'regions'));
  }

  public function district(Request $request, $id)
  {
    $district = District::find($id);
    $districts = \DB::select(\DB::Raw("
      SELECT project.project, fdistrict.distname, fupazila.upname, funion.unname,osm_mou.moudate
      FROM osm_mou, project, fdistrict, fupazila, funion
      WHERE osm_mou.distid = $id and  osm_mou.projid = project.id and osm_mou.distid = fdistrict.id and osm_mou.upid = fupazila.id and osm_mou.unid = funion.id

      "));
    return view($this->view_path.'district', compact('district', 'districts'));
  }

  public function unionEdit(Request $request, $id)
  {

    $unions =\DB::select(\DB::Raw("
      SELECT
        project.project,
        fdistrict.distname,
        fupazila.upname,
        funion.unname,
        osm_mou.moudate,
        osm_mou.remarks,
        osm_mou.id
      FROM
        osm_mou,
        fdistrict,
        fupazila,
        funion,
        project
      WHERE
        osm_mou.distid = fdistrict.id AND
        osm_mou.upid = fupazila.id AND
        osm_mou.unid =funion.id AND
        project.id = osm_mou.projid AND
        osm_mou.id=$id
      ORDER BY
      project.project, fdistrict.distname, fupazila.upname, funion.unname"));

    $mou = "";

    if(!count($unions))
    {
      return redirect()->route($this->route_path.'index');
    }

    $mou = $unions[0];

    //dd($mou);
    return view($this->view_path.'union_edit', compact('mou'));
  }

  public function unionUpdate(Request $request, $id)
  {
    // dd($request->all());
    \DB::table('osm_mou')->where('id', $id)->update([
        'moudate'     => $request->input('contractdate'),
        'remarks'     => $request->input('remarks')
      ]);
    return redirect()->route($this->route_path.'index');
  }
}
