<?php

namespace App\Http\Controllers\SuperAdmin\UP;

use App\Http\Controllers\Controller;
use App\Model\District;
use App\Model\Download\Superadmin\UP\FunctionariesDownload;
use App\Model\Project;
use App\Model\ProjectStuff;
use App\Model\Region;
use App\Model\Union;
use App\Model\Upazila;
use Illuminate\Http\Request;


class FunctionariesController extends Controller {

  private $view_path = "core.superadmin.up.functionaries.";
  private $route_path = "superadmin.up.functionaries.";

  public function index(Request $request)
  {

    if($request->has('download') && $request->download != "")
    {
      return (new FunctionariesDownload)->download($request);
    }

    $users = [];
    $districts = District::all();
    $projects = Project::all();
    $unions = "";

    $old = [
      'district_id' => '',
      'upazila_id'  => '',
      'union_id'    => '',
      'project_id'  => '',
      'districtName'  => '',
      'upazilaName'   => '',
      'unionName'     => ''
    ];

    if($request->has('query'))
    {
      $old = [
        'district_id' => $request->district_id,
        'upazila_id'  => $request->upazila_id,
        'union_id'    => $request->union_id,
        'project_id'  => $request->project_id,
        'districtName'  => '',
        'upazilaName'   => '',
        'unionName'     => '',
        'download'      => 'download'
      ];


      $users = ProjectStuff::with('union','upazila','district')->where(function($q) use ($request){
        if($request->has('union_id') && $request->union_id != "")
        {
          $q->where('unid', $request->union_id);
        }
        if($request->has('upazila_id') && $request->upazila_id != "")
        {
          $q->where('upid', $request->upazila_id);
        }
        if($request->has('district_id') && $request->district_id != "")
        {
          $q->where('distid', $request->district_id);
        }
        if($request->has('project_id') && $request->project_id != "")
        {
          $q->where('proid', $request->project_id);
        }
      })->paginate(15);

    }

    //dd($users);

    return view($this->view_path.'index', compact('users', 'districts', 'upazilas', 'unions', 'projects', 'old'));
  }
}
