<?php

namespace App\Http\Controllers\SuperAdmin\UP;

use App\Http\Controllers\Controller;
use App\Model\District;
use App\Model\Download\Superadmin\UP\ProjectStuffDownload;
use App\Model\Project;
use App\Model\Region;
use App\Model\Union;
use App\Model\UnionStuff;
use App\Model\Upazila;
use Illuminate\Http\Request;


class StuffController extends Controller {

  private $view_path = "core.superadmin.up.stuff.";
  private $route_path = "superadmin.up.stuff.";



  public function index(Request $request)
  {

    if($request->has('download'))
    {
      return (new ProjectStuffDownload)->download();
    }

    $users = [];
    $districts = District::orderBy('distname')->get();
    $projects = Project::all();
    $unions = "";

    $old = [
      'district_id' => '',
      'upazila_id'  => '',
      'union_id'    => '',
      'project_id'  => '',
      'districtName'  => '',
      'upazilaName'   => '',
      'unionName'     => '',
    ];

    if($request->has('query'))
    {
      $old = [
        'district_id' => $request->district_id,
        'upazila_id'  => $request->upazila_id,
        'union_id'    => $request->union_id,
        'project_id'  => $request->project_id,
        'districtName'  => '',
        'upazilaName'   => '',
        'unionName'     => ''
      ];

      $users = UnionStuff::with('union','upazila','district')->where(function($q) use ($request){
        if($request->has('union_id') && $request->union_id != "")
        {
          $q->where('unid', $request->union_id);
        }
        if($request->has('upazila_id') && $request->upazila_id != "")
        {
          $q->where('upid', $request->upazila_id);
        }
        if($request->has('district_id') && $request->district_id != "")
        {
          $q->where('distid', $request->district_id);
        }
        if($request->has('project_id') && $request->project_id != "")
        {
          $q->where('proid', $request->project_id);
        }
      })->paginate(15);
    }

    //dd($users);

    return view($this->view_path.'index', compact('users', 'districts', 'upazilas', 'unions', 'projects', 'old'));

  }

}
