<?php

namespace App\Http\Controllers\SuperAdmin\Training;

use App\Http\Controllers\Controller;
use App\ImplementStatus;
use App\Model\Download\Superadmin\Training\TrainingDownload;
use App\Model\Download\WaterDownload;
use App\Model\Search\Request\WaterSearchRequest;
use App\Model\Search\TrainingSearch;
use App\Model\Search\WaterSearch;
use App\Water;
use App\WaterTech;
use DB;
use Illuminate\Http\Request;


class TrainingController extends Controller {

  private $view_path = "core.superadmin.training.training.";
  private $route_path = "superadmin.training.training.";

  public function index( Request $request )
  {
    if($request->has('download'))
    {
      return (new TrainingDownload)->download();
    }

    $old = [
      'Agency'      => "",
      'TrgStatus'   => "",
      'TrgFrom'     => "",
      'TrgTo'       => "",
      //'Organizedby' => "",
      'TrgTitle'    => "",
      'region_id'   => ""
    ];

    if($request->has('query'))
    {
      $old = [
        'query'       => $request->input('query'),
        'Agency'      => $request->input('Agency'),
        'TrgStatus'   => $request->input('TrgStatus'),
        'TrgFrom'     => $request->input('TrgFrom'),
        'TrgTo'       => $request->input('TrgTo'),
        //'Organizedby' => $request->input('Organizedby'),
        'TrgTitle'    => $request->input('TrgTitle'),
        'region_id'   => $request->input('region_id')
      ];
    }

    $traininglists = (new TrainingSearch($request, true))->get();
    return view($this->view_path.'index', compact('traininglists', 'old'));
  }

  public function create()
  {
      $trainingTitle = DB::table('trg_title')->pluck('title','id')->toArray();
      $venueList     = DB::table('tbl_venue')->pluck('VenueName','VenueID')->toArray();
      $agency        = DB::table('trg_agency')->pluck('agency_name','id')->toArray();
      return view($this->view_path.'insert', compact('venueList','trainingTitle','agency'));
  }

  public function store(Request $request)
  {
    $data=$request->except('_token','_method');
    $data["updated_by"]=auth()->user()->id;
    $data["created_by"]=auth()->user()->id;

    $count = \DB::table('tbl_training')->insert($data);

    if($count){
      return redirect()->route($this->route_path.'index');
    }

    \Session::flash('message', "New Data saved successfully");
    return redirect()->route($this->route_path.'index');
  }

  public function edit($id)
  {
    $trainingTitle = DB::table('trg_title')->pluck('title','id')->toArray();
    $venueList     = DB::table('tbl_venue')->pluck('VenueName','VenueID')->toArray();
    $agency        = DB::table('trg_agency')->pluck('agency_name','id')->toArray();
    $traininglists = DB::table('tbl_training')->WHERE('TrgCode',$id)->first();
    return view($this->view_path.'edit', compact('traininglists','venueList','trainingTitle','agency'));
  }

  public function update(Request $request, $id)
  {
      $Info = \DB::table('tbl_training')
                  ->where('TrgCode', $id)
                  ->get();
      $data=$request->except('_token','_method');
      $data["updated_by"]=auth()->user()->id;

      if(COUNT($Info)>0){
        DB::table('tbl_training')
          ->where('TrgCode', $Info->first()->TrgCode)
          ->update($data);
      }

    \Session::flash('message', "One updated successfully");
    return redirect()->route($this->route_path.'index');
  }
}