<?php

namespace App\Http\Controllers\SuperAdmin\Training;

use App\Http\Controllers\Controller;
use App\ImplementStatus;
use App\Model\Download\Superadmin\Training\ParticipantDownload;
use App\Model\Download\WaterDownload;
use App\Model\Search\Request\WaterSearchRequest;
use App\Model\Search\WaterSearch;
use App\Water;
use App\WaterTech;
use DB;
use Illuminate\Http\Request;

class TrainingParticipantController extends Controller {

  private $view_path = "core.superadmin.training.participant.";
  private $route_path = "superadmin.training.training-participant.";

  public function index( Request $request )
  {
    if($request->has('download'))
    {
      return (new ParticipantDownload)->download();
    }

    $participantslists = DB::select(

      DB::raw("SELECT
        tbl_training.TrgTitle,
        tbl_training.TrgVenue,
        tbl_training.TrgFrom,
        tbl_training.TrgTo,
        tbl_trg_participants.partcipant_name,
        tbl_trg_participants.designation,
        fdistrict.distname,
        fupazila.upname,
        funion.unname,
        tbl_trg_participants.mobile_no,
        tbl_trg_participants.email_address,
        tbl_trg_participants.participant_id,
        tbl_trg_participants.region_id,
        tbl_training.Agency
        FROM (((tbl_trg_participants
          LEFT JOIN fdistrict ON tbl_trg_participants.district_id = fdistrict.id)
          LEFT JOIN fupazila ON tbl_trg_participants.upazila_id = fupazila.id)
          LEFT JOIN funion ON tbl_trg_participants.union_id = funion.id)
          LEFT JOIN tbl_training ON tbl_trg_participants.TrgCode = tbl_training.TrgCode
        "
        )
      );

    return view($this->view_path.'index', compact('participantslists'));
  }

  public function create(Request $request)
  {
    $requestData = $request->all();
    $TrgCode=$requestData["TrgCode"];
    $participants=DB::table('tbl_trg_participants')
    ->leftJoin('fdistrict', 'tbl_trg_participants.district_id', '=', 'fdistrict.id')
    ->leftJoin('fupazila', 'tbl_trg_participants.upazila_id', '=', 'fupazila.id')
    ->leftJoin('funion', 'tbl_trg_participants.union_id', '=', 'funion.id')
    ->where('TrgCode',$TrgCode)
    ->get()->toArray();

    $TrgCode=$requestData["TrgCode"];
    $district=DB::table('fdistrict')->pluck('distname','id')->toArray();

    $des = DB::table('trg_desg')->pluck('desg','id')->toArray();
    return view($this->view_path.'insert', compact('district','des','TrgCode','participants'));
  }

  public function store(Request $request)
  {
    $data=$request->except('_token','_method');
    $data["updated_by"]=auth()->user()->id;
    $data["created_by"]=auth()->user()->id;

    $count = \DB::table('tbl_trg_participants')->insert($data);

    if($count){
      return redirect()->route($this->route_path.'index');
    }
    return redirect()->route($this->route_path.'index');
    \Session::flash('message', "New Data saved successfully");
    return redirect()->route($this->route_path.'index');
  }

  public function edit($id)
  {
    $participants =DB::table('tbl_trg_participants')->where('participant_id',$id)->first();
    $district=DB::table('fdistrict')
                ->pluck('distname','id')->toArray();
    $des=DB::table('trg_desg')->pluck('desg','id')->toArray();
    return view($this->view_path.'edit', compact('district','des','TrgCode','participants'));
  }

  public function update(Request $request, $id)
  {
    $Info = \DB::table('tbl_trg_participants')
                ->where('participant_id', $id)
                ->get();

    $data=$request->except('_token','_method');

    $data["updated_by"]=auth()->user()->id;

    if(count($Info)>0){
      DB::table('tbl_trg_participants')
      ->where('participant_id',$id)
      ->update($data);
    }

    \Session::flash('message', "One updated successfully");
    return redirect()->route($this->route_path.'index');
  }

  public function getUpzilla(Request $request)
  {
    $requestData = $request->all();
    $disid = $requestData['disid'];
    $upzilla = DB::table('fupazila')->where('disid',$disid)->pluck('upname','id')->toArray();
    return $upzilla;
  }

  public function getUnion(Request $request)
  {
    $requestData = $request->all();
    $disid=$requestData['disid'];
    $upid=$requestData['upid'];
    $union=DB::table('funion')
              ->where('distid',$disid)
              ->where('upid',$upid)
              ->pluck('unname','id')
              ->toArray();

    return $union;
  }
}