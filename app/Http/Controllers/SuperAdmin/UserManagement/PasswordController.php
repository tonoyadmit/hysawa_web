<?php

namespace App\Http\Controllers\SuperAdmin\UserManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
class PasswordController extends Controller
{
  private $view_path = "core.superadmin.user-management.password.";
  private $route_path = "superadmin.user-management.";

  public function get($id)
  {
    $user = User::with(
        'userType',
        'region',
        'project',
        'district',
        'upazila',
        'union')->findOrFail($id);

    //dd($user->toArray());
    return view($this->view_path.'show', compact('user'));
  }

  public function change(Request $request, $id)
  {
    //dd($request->all());
    $user = User::findOrFail($id);

    if( (
        $request->password == "" ||
        $request->password != $request->confirm_password) )
    {
      return redirect()->route($this->route_path.'password-change', $user->id);
    }
      $user->password = \Hash::make($request->password) ;
      $user->updated_at = date('Y-m-d H:i:s');
      $user->updated_by = \Auth::user()->id;
      $user->update();


    return redirect()->route($this->route_path.'index');
  }
}
