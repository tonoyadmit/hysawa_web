<?php

namespace App\Http\Controllers\SuperAdmin\UserManagement;

use App\Http\Controllers\Controller;
use App\Model\District;
use App\Model\Download\Superadmin\User\UserDownload;
use App\Model\Project;
use App\Model\Region;
use App\Model\Union;
use App\Model\Upazila;
use App\Role;
use App\User;
use App\UserType;
use Illuminate\Http\Request;

class UserController extends Controller
{
  private $view_path = "core.superadmin.user-management.";
  private $route_path = "superadmin.user-management.";

  public function index(Request $request )
  {

    if($request->has('download') && $request->input('download') != "")
    {
      dd("here");
      return (new UserDownload)->download($request);
    }

    $projects  = Project::all();
    $districts = District::all();
    $regions   = Region::all();
    $user_types = UserType::all();

    $roles = \DB::table('roles')->orderBy('display_name')->get();

    $upazilas  = "";
    $unions    = "";
    $users     = "";

    $old = [
      'types'       => '',
      'project_id'  => '',
      'district_id' => '',
      'upazila_id'  => '',
      'union_id'    => '',
      'region_id'   => '',
      'name'        => '',
      'download'    => ''
    ];

    if($request->has('query'))
    {
      $old['query']       = $request->input('query');
      $old['types']       = $request->input('types');
      $old['project_id']  = $request->input('project_id');
      $old['district_id'] = $request->input('district_id');
      $old['upazila_id']  = $request->input('upazila_id');
      $old['union_id']    = $request->input('union_id');
      $old['region_id']   = $request->input('region_id');
      $old['name']        = $request->input('name');
      $old['download']    = "download";

      if($request->input('district_id') != "")
      {
        $upazilas = Upazila::where('disid', $request->district_id)->get();
      }

      if($request->input('upazila_id') != "")
      {
        $unions = Union::where('upid', $request->upazila_id)->get();
      }

      $users = User::with(
        'roles',
        'region',
        'project',
        'district',
        'upazila',
        'union')->where(function($query) use ($request){

          if($request->has('role_id') && $request->role_id != "")
          {
            $role_id = $request->role_id;

            $query->whereHas('roles', function($query2) use($role_id) {

              $query2->where('roles.id', $role_id);
            });

          }

          if($request->has('region_id') && $request->region_id != "")
          {
            $query->where('region_id', $request->region_id);
          }

          if($request->has('project_id') && $request->project_id != "")
          {
            $query->where('proj_id', $request->project_id);
          }

          if($request->has('district_id') && $request->district_id != "")
          {
            $query->where('distid', $request->district_id);
          }

          if($request->has('upazila_id') && $request->upazila_id != "")
          {
            $query->where('upid', $request->upazila_id);
          }

          if($request->has('union_id') && $request->union_id != "")
          {
            $query->where('unid', $request->union_id);
          }

          if($request->has('name') && $request->name != "")
          {
            $query->where('name', 'LIKE', '%'.$request->name.'%');
          }
        })->orderBy('id', 'DESC')->paginate(15);

    }else{
      $users = User::with(
        'userType',
        'region',
        'project',
        'district',
        'upazila',
        'union',
        'roles')->orderBy('id', 'DESC')->paginate(15);
    }

    return view($this->view_path.'index', compact(
      'users',
      'old',
      'projects',
      'districts',
      'regions',
      'roles',
      'upazilas',
      'unions'
    ));
  }

  public function create()
  {
    $projects  = Project::all();
    $regions   = Region::all();
    $districts = District::all();
    $roles = \DB::table('roles')->get();
    return view($this->view_path.'create', compact('projects', 'regions', 'districts', 'roles'));
  }

  public function store(Request $request)
  {
    $user = new User;

    $user->name        = $request->input('name');
    $user->email       = $request->input('email');
    $user->msisdn      = $request->input('msisdn');
    $user->supervisor  = $request->input('supervisor');
    $user->status      = $request->input('status');

    $user->distid      = $request->input('district_id');
    $user->upid        = $request->input('upazila_id');
    $user->unid        = $request->input('union_id');
    $user->proj_id     = $request->input('project_id');
    $user->region_id   = $request->input('region_id');

    $user->user_type_id = 1;
    $user->created_at  = date('Y-m-d H:i:s');
    $user->created_by  = \Auth::user()->id;

    $user->password = \Hash::make('password');
    $user->save();

    $role = \DB::table('roles')->find($request->role_id);
    $user->roles()->attach($role->id);

    return redirect()->route($this->route_path.'index');
  }

  public function show(Request $request, $id)
  {
    $user = User::with(
        'userType',
        'region',
        'project',
        'district',
        'upazila',
        'union')->findOrFail($id);
    return view($this->view_path.'show', compact('user'));
  }

  public function edit(Request $request, $id)
  {
    $user = User::findOrFail($id);
    $regions = Region::all();
    $projects = Project::all();
    $districts = District::all();
    $roles = \DB::table('roles')->get();

    $upazilas = "";
    $unions = "";

    if($user->distid != "")
    {
      $upazilas = Upazila::where('disid', $user->distid)->get();
    }

    if($user->upid != "" )
    {
      $unions = Union::where('upid', $user->upid)->get();
    }

    return view($this->view_path.'edit', compact(
      'user',
      'regions',
      'projects',
      'districts',
      'upazilas',
      'unions',
      'roles'
      ));
  }

  public function update(Request $request, $id)
  {

    $user              = User::findOrFail($id);

    $user->name        = $request->input('name');
    $user->email       = $request->input('email');
    $user->msisdn      = $request->input('msisdn');
    $user->supervisor  = $request->input('supervisor');
    $user->status      = $request->input('status');

    $user->distid      = $request->input('district_id');
    $user->upid        = $request->input('upazila_id');
    $user->unid        = $request->input('union_id');
    $user->proj_id     = $request->input('project_id');
    $user->region_id   = $request->input('region_id');


    $user->updated_at  = date('Y-m-d H:i:s');
    $user->updated_by  = \Auth::user()->id;

    $user->update();

    if($request->role_id != "")
    {
      $role = Role::find($request->role_id);
      $user->roles()->sync($role->id);
    }

    return redirect()->route($this->route_path.'index');
  }
}
