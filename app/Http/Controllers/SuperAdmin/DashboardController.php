<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;

class DashboardController extends Controller {

  private $view_path = "core.superadmin.";

  public function dashboard()
  {
    return view($this->view_path.'dashboard');
  }
}