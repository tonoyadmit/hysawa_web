<?php

namespace App\Http\Controllers\UpAdmin\WaterSanitation;

use App\AppStatus;
use App\Http\Controllers\Controller;
use App\ImplementStatus;
use App\Model\Download\SanitationDownload;
use App\Model\Search\Model\Sanitation as ModelSanitation;
use App\Model\Search\Request\SanitationSearchRequest;
use App\Model\Search\SanitationSearch;
use App\WaterSanitation;
use Illuminate\Http\Request;
use Session;
use Auth;
use Illuminate\Support\Facades\Validator;

class WaterSanitationController extends Controller
{
  private $view_path = "core.up.sanitation.";
  private $route_path = "up-admin.water_sanitation.";

  public function index( Request $request )
  {

    $sanitations = "";

    $old = [
      'starting_date' => '',
      'ending_date' => '',
      'created_by' => '',
      'village' => '',
      'imp_status'=> '',
      'app_status'=> '',
      'unid'       => \Auth::user()->unid,
      'proj_id'       => \Auth::user()->proj_id,
      'region_id'       => \Auth::user()->region_id
    ];

    $appstatus = AppStatus::pluck('app_status','app_status')->toArray();
    $ImplementStatus=ImplementStatus::pluck('imp_status','imp_status')->toArray();
    
     $validator = Validator::make($request->all(),[
      'ch_com_tel'=>'required | max:11| min:11',
  ]);
  
    if($request->has('query')){

      $sanitationSearchRequest = new SanitationSearchRequest;
      $sanitationSearchRequest->starting_date = $request->input('starting_date');
      $sanitationSearchRequest->ending_date = $request->input('ending_date');
      $sanitationSearchRequest->created_by = $request->input('created_by');
      $sanitationSearchRequest->village = $request->input('village');
      $sanitationSearchRequest->imp_status = $request->input('imp_status');
      $sanitationSearchRequest->app_status = $request->input('app_status');

      $sanitationSearchRequest->unid = \Auth::user()->unid ;
      $sanitationSearchRequest->proj_id = \Auth::user()->proj_id;
      $sanitationSearchRequest->region_id = \Auth::user()->region_id;
      $sanitationSearchRequest->upid = \Auth::user()->upid ;
      $sanitationSearchRequest->dist_id = \Auth::user()->distid ;

      if($request->has('page'))
      {
        $sanitationSearchRequest->page = $request->input('page');
      }

      $old = [
        'query'         => $request->input('query'),
        'starting_date' => $request->input('starting_date'),
        'ending_date'   => $request->input('ending_date'),
        'created_by'    => $request->input('created_by'),
        'village'       => $request->input('village'),
        'imp_status'       => $request->input('imp_status'),
        'app_status'       => $request->input('app_status'),
        'download'      => time(),
        'unid'       => \Auth::user()->unid,
        'proj_id'       => \Auth::user()->proj_id,
        'region_id'       => \Auth::user()->region_id
      ];
      $sanitations = (new SanitationSearch($sanitationSearchRequest))->get();
    }else{
       $sanitations = WaterSanitation::where('unid', \Auth::user()->unid)
            ->where('upid', \Auth::user()->upid)
            ->where('dist_id', \Auth::user()->distid)
            ->where('proj_id', \Auth::user()->proj_id)
            ->where('region_id', \Auth::user()->region_id)
            ->orderBy('id','DESC')->paginate(15);
    }

    return view($this->view_path.'index', compact('sanitations', 'old','ImplementStatus','appstatus'));
  }

  public function download(Request $request)
  {
     
    return (new SanitationDownload($request))->download();
  }

  public function print(Request $request)
  {
    
    return view($this->view_path.'print')->with('sanitations', (new ModelSanitation($request))->get());
  }

  public function create()
  {
    $mainype = array(
      'Institutional latrine' => 'Institutional latrine',
      'Public latrine' => 'Public latrine',
      'Communal latrine' => 'Communal latrine');

    $subtype = array(
      'Primary School' => 'Primary School',
      'High School' => 'High School',
      'Bazar' => 'Bazar',
      'Madrasha' => 'Madrasha',
      'Mosque' => 'Mosque',
      'Slum' => 'Slum',
      'Community' => 'Community');

    $wordOrderNO = array('1' => '1','2' => '2','3' => '3','4' => '4');
    $appstatus = AppStatus::pluck('app_status','app_status')->toArray();
    $ImplementStatus=ImplementStatus::pluck('imp_status','imp_status')->toArray();

    return view($this->view_path.'insert', compact('subtype','appstatus','wordOrderNO','ImplementStatus','mainype'));
  }

  public function store(Request $request)
  {
    $water = new WaterSanitation();
    $water->fill($request->except('_token'));

    $water->unid = auth()->user()->unid;
    $water->region_id = auth()->user()->region_id;
    $water->proj_id = auth()->user()->proj_id;
    $water->dist_id = auth()->user()->distid;
    $water->upid = auth()->user()->upid;

    $water->created_by = auth()->user()->id;
    $water->updated_by = auth()->user()->id;
    $water->save();

 $validator = Validator::make($request->all(),[
      'ch_com_tel'=>'required | max:11| min:11',
  ]);
  if($validator->fails()){
    \Session::flash('warning','This field is Required ! You Inserted An Empty field!');
    return redirect()->back()->withErrors("Mobile Number will be 11 Digit")->withInput();
   }else {
    Session::flash('message', "New Data saved successfully");
    return redirect()->route($this->route_path.'create');
  }
}
  public function show($id)
  {
    $water = WaterSanitation::findOrFail($id);
    return view($this->view_path.'view', compact('water'));
  }

  public function edit($id)
  {

    $mainype = array(
      'Institutional latrine' => 'Institutional latrine',
      'Public latrine' => 'Public latrine',
      'Communal latrine' => 'Communal latrine');

    $subtype = array(
      'Primary School' => 'Primary School',
      'High School' => 'High School',
      'Bazar' => 'Bazar',
      'Madrasha' => 'Madrasha',
      'Mosque' => 'Mosque',
      'Slum' => 'Slum',
      'Community' => 'Community');

    $wordOrderNO = array(
      '1' => '1',
      '2' => '2',
      '3' => '3',
      '4' => '4'
    );

    $appstatus = AppStatus::pluck('app_status','app_status')->toArray();
    $ImplementStatus=ImplementStatus::pluck('imp_status','imp_status')->toArray();

    $WaterSanitation = WaterSanitation::where('id', '=', $id)->first();
    return view($this->view_path.'edit', compact('subtype','appstatus','wordOrderNO','ImplementStatus','mainype','WaterSanitation'));
  }

  public function update(Request $request, $id)
  {
       $validator = Validator::make($request->all(),[
      'ch_com_tel'=>'required | max:11| min:11',
  ]);
    if($validator->fails()){
      \Session::flash('warning','This field is Required ! You Inserted An Empty field!');
      return redirect()->back()->withErrors("Mobile Number will be 11 Digit")->withInput();
     }else {
    $water = WaterSanitation::findOrFail($id);
    $water->fill($request->except('_token'));
    $water->updated_by = auth()->user()->id;
    $water->save();

    Session::flash('message', "One updated successfully");
    return redirect()->route($this->route_path.'index');
  }
  }

  public function destroy($id)
  {

  }
}
