<?php

namespace App\Http\Controllers\UpAdmin\Procurement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Fhead;
use App\Fsubhead;
use App\Fitemlist;
use App\ProposalEvaluationComittee;

class ProposalEvaluationComitteeController extends Controller
{
  private $view_path = "core.up.procurement.proposalevalution.";
  private $route_path = "up-admin.proposalEvaluationComittee.";

  public function index( Request $request )
  {
    $proposalEvaluationComittees = ProposalEvaluationComittee::where('unid', '=',auth()->user()->unid)->get();
    return view($this->view_path.'index', compact('proposalEvaluationComittees'));
  }

  public function create()
  {
    return view($this->view_path."insert");
  }

  public function store(Request $request)
  {
    // $validation = Validator::make($request->all(), [
    //    'name'          => 'required',
    // ]);

    // if($validation->fails()) {
    //     return Redirect::back()->withErrors($validation)->withInput();
    // }

    $proposalEvaluationComittee = new ProposalEvaluationComittee();
    $proposalEvaluationComittee->fill($request->except('_token'));

    $proposalEvaluationComittee->unid = auth()->user()->unid;
    $proposalEvaluationComittee->proid = auth()->user()->proj_id;
    $proposalEvaluationComittee->distid = auth()->user()->distid;
    $proposalEvaluationComittee->upid = auth()->user()->upid;

    $proposalEvaluationComittee->created_by = auth()->user()->id;
    $proposalEvaluationComittee->updated_by = auth()->user()->id;
    $proposalEvaluationComittee->save();

    \Session::flash('message', "New Data saved successfully");
    return redirect()->route($this->route_path."index");
  }

  public function show($id)
  {
    $head = Fhead::pluck('headname','id')->toArray();
    $FinanceData = ProposalEvaluationComittee::findOrFail($id);
    return view($this->view_path."show", compact('FinanceData','head'));
  }

  public function edit($id)
  {
    $proposalEvaluationComittee = ProposalEvaluationComittee::findOrFail($id);
    return view($this->view_path."edit", compact('proposalEvaluationComittee'));
  }

  public function update(Request $request, $id)
  {
    // $validation = Validator::make($request->all(), [
    //     'name'          => 'required',
    // ]);

    // if($validation->fails()) {
    //     return Redirect::back()->withErrors($validation)->withInput();
    // }

    $proposalEvaluationComittee = ProposalEvaluationComittee::findOrFail($id);
    $proposalEvaluationComittee->fill($request->except('_token'));
    $proposalEvaluationComittee->updated_by = auth()->user()->id;
    $proposalEvaluationComittee->save();

    \Session::flash('message', "One updated successfully");
    return redirect()->route($this->route_path.'index');
  }

}
