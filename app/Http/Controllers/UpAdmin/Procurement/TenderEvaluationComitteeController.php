<?php

namespace App\Http\Controllers\UpAdmin\Procurement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Fhead;
use App\Fsubhead;
use App\Fitemlist;
use App\TenderEvaluationComittee;
class TenderEvaluationComitteeController extends Controller
{
  private $view_path = "core.up.procurement.procurementsevalution.";
  private $route_path = "up-admin.tenderEvaluationComittee.";

    public function index( Request $request )
    {
      $tenderEvaluationComittees = TenderEvaluationComittee::where('unid', '=',auth()->user()->unid)->get();
      return view($this->view_path."index", compact('tenderEvaluationComittees'));
    }

    public function create()
    {
      return view($this->view_path."insert");
    }

    public function store(Request $request)
    {
      // $validation = Validator::make($request->all(), [
      //    'name'          => 'required',
      // ]);

      // if($validation->fails()) {
      //   return Redirect::back()->withErrors($validation)->withInput();
      // }

      $tenderEvaluationComittee = new TenderEvaluationComittee();
      $tenderEvaluationComittee->fill($request->except('_token'));

      $tenderEvaluationComittee->unid = auth()->user()->unid;
      $tenderEvaluationComittee->proid = auth()->user()->proj_id;
      $tenderEvaluationComittee->distid = auth()->user()->distid;
      $tenderEvaluationComittee->upid = auth()->user()->upid;

      $tenderEvaluationComittee->created_by = auth()->user()->id;
      $tenderEvaluationComittee->updated_by = auth()->user()->id;
      $tenderEvaluationComittee->save();

      \Session::flash('message', "New Data saved successfully");
      return redirect()->route($this->route_path."index");
    }

    public function show($id)
    {
      $head = Fhead::pluck('headname','id')->toArray();
      $FinanceData = Bank::findOrFail($id);
      return view($this->view_path."view", compact('FinanceData','head'));
    }

    public function edit($id)
    {
      $tenderEvaluationComittee = TenderEvaluationComittee::findOrFail($id);
      return view($this->view_path."edit", compact('tenderEvaluationComittee'));
    }

    public function update(Request $request, $id)
    {
      // $validation = Validator::make($request->all(), [
      //     'name'          => 'required',
      // ]);

      // if($validation->fails()) {
      //     return redirect()->back()->withErrors($validation)->withInput();
      // }

      $tenderEvaluationComittee = TenderEvaluationComittee::findOrFail($id);
      $tenderEvaluationComittee->fill($request->except('_token'));
      $tenderEvaluationComittee->updated_by = auth()->user()->id;
      $tenderEvaluationComittee->save();
      Session::flash('message', "One updated successfully");
      return redirect()->route($this->route_path."index");
    }
}
