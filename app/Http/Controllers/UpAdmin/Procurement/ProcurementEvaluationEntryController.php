<?php

namespace App\Http\Controllers\UpAdmin\Procurement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Fhead;
use App\ProcurementEntry;
use App\Procurement;
use App\ProcurementEvaluationEntry;

class ProcurementEvaluationEntryController extends Controller
{
  private $view_path = "core.up.procurement.tender.";
  private $route_path = "up-admin.ProcurementEvaluationEntry.";


  public function index( Request $request )
  {
    $unid=auth()->user()->unid;
    $procurementEvaluationEntrys = ProcurementEvaluationEntry::where('unid', '=',auth()->user()->unid)->get();
    return view($this->view_path.'index', compact('procurementEvaluationEntrys','projectList','contractorLists'));
  }

  public function create()
  {
    $unid=auth()->user()->unid;
    $distid=auth()->user()->distid;
    $projectList=ProcurementEntry::where('unid', '=', $unid)->pluck('package','id')->toArray();
    $contractorLists=Procurement::where('distid', '=', $unid)->pluck('con_name','id')->toArray();
    return view($this->view_path.'insert',compact('projectList','contractorLists'));
  }

  public function store(Request $request)
  {

    $procurementEvaluationEntry = new ProcurementEvaluationEntry();
    $procurementEvaluationEntry->fill($request->except('_token'));

    $procurementEvaluationEntry->unid = auth()->user()->unid;
    $procurementEvaluationEntry->proid = auth()->user()->proj_id;
    $procurementEvaluationEntry->distid = auth()->user()->distid;
    $procurementEvaluationEntry->upid = auth()->user()->upid;

    $procurementEvaluationEntry->created_by = auth()->user()->id;
    $procurementEvaluationEntry->updated_by = auth()->user()->id;
    $procurementEvaluationEntry->save();

    \Session::flash('message', "New Data saved successfully");
    return redirect()->route($this->route_path.'index');
  }

  public function show($id)
  {
    $head = Fhead::pluck('headname','id')->toArray();
    $FinanceData = ProcurementEvaluationEntry::findOrFail($id);
    return view($this->view_path.'view', compact('FinanceData','head'));
  }

  public function edit($id)
  {
    $unid=auth()->user()->unid;
    $distid=auth()->user()->distid;

    $projectList=ProcurementEntry::where('unid', '=', $unid)->pluck('package','id')->toArray();
    $contractorLists=Procurement::where('distid', '=', $unid)->pluck('con_name','id')->toArray();
    $procurementEvaluationEntry = ProcurementEvaluationEntry::findOrFail($id);
    return view($this->view_path.'edit', compact('procurementEvaluationEntry','projectList','contractorLists'));
  }

  public function update(Request $request, $id)
  {
    $procurementEvaluationEntry = ProcurementEvaluationEntry::findOrFail($id);
    $procurementEvaluationEntry->fill($request->except('_token'));
    $procurementEvaluationEntry->updated_by = auth()->user()->id;
    $procurementEvaluationEntry->save();

    Session::flash('message', "One updated successfully");
    return redirect()->route($this->route_path.'index');
  }
}
