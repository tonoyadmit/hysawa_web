<?php

namespace App\Http\Controllers\UpAdmin\Procurement;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Fhead;
use App\Fsubhead;
use App\Fitemlist;
use App\ProcurementEntry;

class ProcurementEntryController extends Controller
{
    private $view_path = "core.up.procurement.procurementslist.";
    private $route_path = "up-admin.procurementEntry.";

    public function index( Request $request )
    {
      $procurementEntrys = ProcurementEntry::where('unid', '=',auth()->user()->unid)->get();
      return view($this->view_path.'index', compact('procurementEntrys'));
    }

    public function create()
    {
      return view($this->view_path.'insert' );
    }

    public function store(Request $request)
    {
      $procurementEntry = new ProcurementEntry();
      $procurementEntry->fill($request->except('_token'));
      $procurementEntry->unid = \Auth::user()->unid;
      $procurementEntry->proid = \Auth::user()->proj_id;
      $procurementEntry->distid = \Auth::user()->distid;
      $procurementEntry->upid = \Auth::user()->upid;
      $procurementEntry->created_by = \Auth::user()->id;
      $procurementEntry->updated_by = \Auth::user()->id;
      $procurementEntry->save();
      \Session::flash('message', "New Data saved successfully");

      return redirect()->route($this->route_path."index");
    }

    public function show($id)
    {
      $head = Fhead::pluck('headname','id')->toArray();
      $FinanceData = ProposalEvaluationComittee::findOrFail($id);
      return view($this->view_path.'view', compact('FinanceData','head'));
    }

    public function edit($id)
    {
      $procurementEntry = ProcurementEntry::findOrFail($id);
      return view($this->view_path."edit", compact('procurementEntry'));
    }

    public function update(Request $request, $id)
    {
      $procurementEntry = ProcurementEntry::findOrFail($id);
      $procurementEntry->fill($request->except('_token'));
      $procurementEntry->updated_by = auth()->user()->id;
      $procurementEntry->save();
      \Session::flash('message', "One updated successfully");
      return redirect()->route($this->route_path.'index');
    }
}
