<?php

namespace App\Http\Controllers\UpAdmin;

use App\Http\Controllers\Controller;

class DashboardController extends Controller {

  private $view_path = "core.up.";

  public function dashboard()
  {
    return view($this->view_path.'dashboard');
  }
}