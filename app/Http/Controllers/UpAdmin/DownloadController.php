<?php

namespace App\Http\Controllers\UpAdmin;

use App\Http\Controllers\Controller;

class DownloadController extends Controller {

  private $view_path = "core.up.download.";

  public function index()
  {
    return view($this->view_path.'index');
  }
}