<?php

namespace App\Http\Controllers\UpAdmin\Finance;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class FinanceController extends Controller
{
    private $view_path = "";
    private $route_path = "";

    public function index( Request $request )
    {
      return view('finance.report.index');
    }
}
