<?php

namespace App\Http\Controllers\UpAdmin\Finance;

use App\Fhead;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Model\Union;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    private $view_path = "core.up.finance.report.";
    private $route_path = "";

    public function index( Request $request )
    {
      $old = [
        'union_id' => '',
        'starting_date' => '',
        'ending_date' => '',
        'unionName' => ''
      ];

      if($request->has('query'))
      {
        $union = Union::with('upazila.district')->where('id', \Auth::user()->unid)->first();

        $old = [
          'union_id'       => \Auth::user()->unid,
          'starting_date'  => $request->input('starting_date'),
          'ending_date'    => $request->input('ending_date'),
          'unionName'      => $union->unname,
          'query'          => $request->input('query')
        ];


        //
        $d=Fhead::with('subhead.subItem')->get()->toArray();

        //dd($d);
        $data=\DB::select(\DB::raw("
            SELECT bIncome,bExpenditure,cIncome,cExpenditure,bIncome-bExpenditure AS opening_bank,cIncome-cExpenditure AS opening_cash,Income-Expenditure AS total
            FROM(
              SELECT *,SUM(IF(trans_type = 'in' AND MODE='bank', amount, 0)) AS 'bIncome',SUM(IF(trans_type = 'ex' AND MODE='bank', amount, 0)) AS 'bExpenditure',SUM(IF(trans_type = 'in' AND MODE='cash', amount, 0)) AS 'cIncome',
        SUM(IF(trans_type = 'ex' AND MODE='cash', amount, 0)) AS 'cExpenditure',
        SUM(IF(trans_type = 'in', amount, 0)) AS 'Income',
        SUM(IF(trans_type = 'ex', amount, 0)) AS 'Expenditure',

        SUM(amount) AS Total
        FROM
        fdata WHERE unid =".auth()->user()->unid." AND proid =".auth()->user()->proj_id." AND region_id =".auth()->user()->region_id."  AND DATE < '".$request->input('starting_date')."'

        ) AS t"));
        $data2=\DB::select(\DB::raw("
            SELECT bIncome,bExpenditure,cIncome,cExpenditure,bIncome-bExpenditure AS opening_bank,cIncome-cExpenditure AS opening_cash,Income-Expenditure AS total
            FROM(
              SELECT *,SUM(IF(trans_type = 'in' AND MODE='bank', amount, 0)) AS 'bIncome',SUM(IF(trans_type = 'ex' AND MODE='bank', amount, 0)) AS 'bExpenditure',SUM(IF(trans_type = 'in' AND MODE='cash', amount, 0)) AS 'cIncome',
        SUM(IF(trans_type = 'ex' AND MODE='cash', amount, 0)) AS 'cExpenditure',
        SUM(IF(trans_type = 'in', amount, 0)) AS 'Income',
        SUM(IF(trans_type = 'ex', amount, 0)) AS 'Expenditure',

        SUM(amount) AS Total
        FROM
        fdata WHERE unid =".auth()->user()->unid." AND proid =".auth()->user()->proj_id." AND region_id =".auth()->user()->region_id."   AND DATE <= '".$request->input('ending_date')."'

        ) AS t"));
          if(count($data)){
             $data= $data[0];
          }
          if(count($data2)){
             $data2= $data2[0];
          }


      }
      return view($this->view_path.'index', compact('old', 'data', 'd','data2'));
    }

    public function print(Request $request)
    {
      $union = Union::with('upazila.district')->where('id', \Auth::user()->unid)->first();

        $old = [
          'union_id'       => \Auth::user()->unid,
          'starting_date'  => $request->input('starting_date'),
          'ending_date'    => $request->input('ending_date'),
          'unionName'      => $union->unname,
          'query'          => $request->input('query')
        ];


        //
        $d=Fhead::with('subhead.subItem')->get()->toArray();

        //dd($d);
        $data=\DB::select(\DB::raw("
            SELECT bIncome,bExpenditure,cIncome,cExpenditure,bIncome-bExpenditure AS opening_bank,cIncome-cExpenditure AS opening_cash,Income-Expenditure AS total
            FROM(
              SELECT *,SUM(IF(trans_type = 'in' AND MODE='bank', amount, 0)) AS 'bIncome',SUM(IF(trans_type = 'ex' AND MODE='bank', amount, 0)) AS 'bExpenditure',SUM(IF(trans_type = 'in' AND MODE='cash', amount, 0)) AS 'cIncome',
        SUM(IF(trans_type = 'ex' AND MODE='cash', amount, 0)) AS 'cExpenditure',
        SUM(IF(trans_type = 'in', amount, 0)) AS 'Income',
        SUM(IF(trans_type = 'ex', amount, 0)) AS 'Expenditure',

        SUM(amount) AS Total
        FROM
        fdata WHERE unid =".auth()->user()->unid." AND proid =".auth()->user()->proj_id." AND region_id =".auth()->user()->region_id."  AND DATE < '".$request->input('starting_date')."'

        ) AS t"));
        $data2=\DB::select(\DB::raw("
            SELECT bIncome,bExpenditure,cIncome,cExpenditure,bIncome-bExpenditure AS opening_bank,cIncome-cExpenditure AS opening_cash,Income-Expenditure AS total
            FROM(
              SELECT *,SUM(IF(trans_type = 'in' AND MODE='bank', amount, 0)) AS 'bIncome',SUM(IF(trans_type = 'ex' AND MODE='bank', amount, 0)) AS 'bExpenditure',SUM(IF(trans_type = 'in' AND MODE='cash', amount, 0)) AS 'cIncome',
        SUM(IF(trans_type = 'ex' AND MODE='cash', amount, 0)) AS 'cExpenditure',
        SUM(IF(trans_type = 'in', amount, 0)) AS 'Income',
        SUM(IF(trans_type = 'ex', amount, 0)) AS 'Expenditure',

        SUM(amount) AS Total
        FROM
        fdata WHERE unid =".auth()->user()->unid." AND proid =".auth()->user()->proj_id." AND region_id =".auth()->user()->region_id."   AND DATE <= '".$request->input('ending_date')."'

        ) AS t"));
          if(count($data)){
             $data= $data[0];
          }
          if(count($data2)){
             $data2= $data2[0];
          }
      return view($this->view_path.'print', compact('old', 'data', 'd','data2'));
    }
}
