<?php

namespace App\Http\Controllers\UpAdmin\Finance;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Item;
use App\User;
use Datatables;
use App\Role;
use App\Fhead;
use App\Fsubhead;
use App\Fitemlist;
use App\FinanceData;
use App\FundList;
use App\FinanceDemand;
use Validator;
use Session;
use Redirect;
use DB;
use Auth;
use Entrust;

class FundListController extends Controller
{
  private $view_path = "core.up.finance.fundlist.";
  private $route_path = "up-admin.fund_list.";

  public function index( Request $request )
  {
    $FinanceDemands = FundList::where('unid', '=',auth()->user()->unid)
                                    ->where('region_id', '=',auth()->user()->region_id)
                                    ->where('proj_id', '=',auth()->user()->proj_id)
                                    ->orderBy('id', 'desc')
                                    ->get();
    return view($this->view_path.'index', compact('FinanceDemands'));
  }

  public function create()
  {
    $items = Item::with('head')->groupBy('headid')->get();
    $head = [];
    foreach($items as $item){
      $head[$item->head->id] = $item->head->headname;
    }
    return view($this->view_path.'insert', compact('head'));
  }

  public function store(Request $request)
  {
    $validation = Validator::make($request->all(), [

      'fdate' => 'required',
      'tdate' => 'required'
      ]);

    if($validation->fails()) {
      return Redirect::back()->withErrors($validation)->withInput();
    }

    $FinanceData = new FundList();
    $FinanceData->fill($request->except('_token'));
    $FinanceData->unid = auth()->user()->unid;
    $FinanceData->userid=auth()->user()->id;
    $FinanceData->created_by = auth()->user()->id;
    $FinanceData->updated_by = auth()->user()->id;

    $FinanceData->distid = auth()->user()->distid;
    $FinanceData->upid = auth()->user()->upid;
    $FinanceData->region_id = auth()->user()->region_id;
    $FinanceData->proj_id = auth()->user()->proj_id;

    $FinanceData->save();

    Session::flash('message', "New Data saved successfully");
    return redirect()->route($this->route_path.'index');
  }

  public function edit($id)
  {
    // $head = Fhead::pluck('headname','id')->toArray();
    $FinanceData = FundList::findOrFail($id);
    // $subhead = Fsubhead::where('headid', '=',$FinanceData["head"])->pluck('sname','id')->toArray();
    // $subitem = Fitemlist::where('headid', '=',$FinanceData["head"])->where('subid', '=',$FinanceData["subhead"])->pluck('itemname','id')->toArray();

    return view($this->view_path.'edit', compact('FinanceData'));;
  }

  public function update(Request $request, $id)
  {
    $validation = Validator::make($request->all(), [
      // 'head'          => 'required',
      // 'subhead'  => 'required',
      // 'item'           => 'required',
      'fdate'          => 'required',
      'tdate'          => 'required'
      ]);

    if($validation->fails()) {
      return Redirect::back()->withErrors($validation)->withInput();
    }

    $FinanceData = FundList::findOrFail($id);
    $FinanceData->fill($request->except('_token'));
    $FinanceData->updated_by = auth()->user()->id;
    $FinanceData->save();

    \Session::flash('message', "One updated successfully");
    return redirect()->route($this->route_path.'index');
  }

  public function l(Request $request)
  {
    $requestData = $request->all();
    return $subhead = Fsubhead::where('headid', '=',$requestData["head"])->pluck('sname','id')->toArray();
  }

  public function subitem(Request $request)
  {
    $requestData = $request->all();
    return $subhead = Fitemlist::where('headid', '=',$requestData["head"])->where('subid', '=',$requestData["subhead"])->pluck('itemname','id')->toArray();
  }

  public function print()
  {

    $FinanceDemands = FinanceDemand::with('getHead', 'getSubhead', 'getItem')
    ->where('unid', '=',auth()->user()->unid)
    ->where('region_id', '=',auth()->user()->region_id)
    ->where('proj_id', '=',auth()->user()->proj_id)
    ->orderBy('id', 'desc')
    ->get();
   return view($this->view_path.'print', compact('FinanceDemands'));
    //return view($this->view_path.'print')->with('FinanceDemands',(new FinanceDemand($request))->get());
 
  }


}
