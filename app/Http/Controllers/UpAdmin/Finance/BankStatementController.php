<?php

namespace App\Http\Controllers\UpAdmin\Finance;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Item;
use App\User;
use Datatables;
use App\Role;
use App\Fhead;
use App\Fsubhead;
use App\Fitemlist;
use App\FinanceData;
use App\Bank;
use Validator;
use Session;
use Redirect;
use DB;
use Auth;
use Entrust;

class BankStatementController extends Controller
{
    private $view_path = "core.up.finance.bankstatement.";
    private $route_path = "up-admin.finace_bankstatement.";

    public function index( Request $request )
    {
      $FinanceDemands = Bank::where('userid', '=',auth()->user()->id)
      ->where('region_id', '=',auth()->user()->region_id)
      ->where('proj_id', '=',auth()->user()->proj_id)
              ->orderBy('id', 'desc')
              ->get();
      return view($this->view_path.'index', compact('FinanceDemands'));
    }

    public function create()
    {
      return view('core.up.finance.bankstatement.insert');
    }

    public function store(Request $request)
    {
      $validation = Validator::make($request->all(), [
        'balance' => 'required',
        'date' => 'required'
      ]);

      if($validation->fails()) {
          return redirect()->back()->withErrors($validation)->withInput();
      }

      $FinanceData = new Bank();
      $FinanceData->fill($request->except('_token'));
      $FinanceData->unid = auth()->user()->unid;
      $FinanceData->userid=auth()->user()->id;
      $FinanceData->created_by = auth()->user()->id;
      $FinanceData->updated_by = auth()->user()->id;

      $FinanceData->distid = auth()->user()->distid;
      $FinanceData->upid = auth()->user()->upid;
      $FinanceData->region_id = auth()->user()->region_id;
      $FinanceData->proj_id = auth()->user()->proj_id;

      $FinanceData->save();
      \Session::flash('message', "New Data saved successfully");
      return redirect()->route($this->route_path.'index');
    }

    public function show($id)
    {
      $items = Item::with('head')->groupBy('headid')->get();
      $head = [];
      foreach($items as $item){
        $head[$item->head->id] = $item->head->headname;
      }
      $FinanceData = Bank::findOrFail($id);
      return view($this->view_path.'view', compact('FinanceData','head'));
    }

    public function edit($id)
    {
      $FinanceData = Bank::findOrFail($id);
      return view($this->view_path.'edit', compact('FinanceData'));
    }

    public function update(Request $request, $id)
    {
      $validation = Validator::make($request->all(), [
          'balance'          => 'required',
          'date'          => 'required'
      ]);

      if($validation->fails()) {
          return Redirect::back()->withErrors($validation)->withInput();
      }

      $FinanceData = Bank::findOrFail($id);
      $FinanceData->fill($request->except('_token'));
      $FinanceData->updated_by = auth()->user()->id;
      $FinanceData->save();

      \Session::flash('message', "One updated successfully");
      return redirect()->route($this->route_path.'index');
    }

    public function subhead(Request $request)
    {
      $requestData = $request->all();
      return $subhead = Fsubhead::where('headid', '=', $requestData["head"])->pluck('sname','id')->toArray();
    }
    public function subitem(Request $request)
    {
      $requestData = $request->all();
      return $subhead = Fitemlist::where('headid', '=',$requestData["head"])->where('subid', '=',$requestData["subhead"])->pluck('itemname','id')->toArray();
    }

    public function disbursement()
    {
      $uid= auth()->user()->unid;
      $results = DB::select( DB::raw("
        SELECT
          fhead.headname,
          fsubhead.sname,
          fitem.itemname,
          budget.mode,
          budget.desdate,
          budget.desbus,
          budget.remarks
        FROM budget
        INNER JOIN fitem ON fitem.id=budget.itemid
        INNER JOIN fsubhead ON fitem.subid=fsubhead.id
        INNER JOIN fhead ON fitem.headid=fhead.id where unid='$uid' order by budget.desdate desc"));

      return view('core.up.finance.disbursement.index', compact('results'));
    }
}
