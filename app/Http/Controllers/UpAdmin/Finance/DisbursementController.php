<?php

namespace App\Http\Controllers\UpAdmin\Finance;

use App\Bank;
use App\Fhead;
use App\FinanceData;
use App\Fitemlist;

use App\Fsubhead;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Model\Head;
use App\Role;
use App\User;
use Auth;
use DB;
use Datatables;
use Entrust;
use Illuminate\Http\Request;
use Redirect;
use Session;
use Validator;

class DisbursementController extends Controller
{
    private $view_path = "core.up.finance.disbursement.";
    private $route_path = "up-admin.finace_bankstatement.";

    public function index(Request $request)
    {
      $heads = Head::all();
      $subheads = "";
      $items = "";

      $uid= auth()->user()->unid;

      $old = [
        'starting_date' => '',
        'ending_date' => '',
        'head_id' => '',
        'subhead_id' => '',
        'item_id' => '',
        'query' => ''
      ];

      $results = "";

      if($request->has('query'))
      {
        $old['query'] = $request->input('query');
        $old['starting_date'] = $request->input('starting_date');
        $old['ending_date'] = $request->input('ending_date');
        $old['head_id'] = $request->input('head_id');
        $old['subhead_id'] = $request->input('subhead_id');
        $old['item_id'] = $request->input('item_id');

        if($request->input('head_id') && $request->head_id != "")
        {
          $subheads = Fsubhead::where('headid', $request->head_id)->get();
        }

        if($request->input('subhead_id') && $request->subhead_id != "")
        {
          $items = Fitemlist::where('subid', $request->subhead_id)->get();
        }

        $results = DB::table('budget')->select( \DB::Raw("
            fhead.headname,
            fsubhead.sname,
            fitem.itemname,
            budget.mode,
            budget.desdate,
            budget.desbus,
            budget.remarks"))
          ->leftjoin('fitem', 'fitem.id', '=', 'budget.itemid')
          ->leftjoin('fsubhead', 'fitem.subid', '=', 'fsubhead.id')
          ->leftjoin('fhead', 'fitem.headid', '=', 'fhead.id')
          ->where('unid', $uid)
          ->where(function($q) use ($request){

            if($request->has('starting_date') && $request->input('starting_date') != "")
            {
              $q->where('budget.desdate', '>=', $request->starting_date);
            }

            if($request->has('ending_date') && $request->input('ending_date') != "")
            {
              $q->where('budget.desdate', '<=', $request->ending_date);
            }

            if($request->has('head_id') && $request->input('head_id') != "")
            {
              $q->where('fhead.id', '=', $request->head_id);
            }

            if($request->has('subhead_id') && $request->input('subhead_id') != "")
            {
              $q->where('fsubhead.id', '=', $request->subhead_id);
            }

            if($request->has('item_id') && $request->input('item_id') != "")
            {
              $q->where('itemid', '=', $request->input('item_id'));
            }

          })->orderBy('budget.desdate', 'DESC')->paginate(15);

          return view($this->view_path.'index', compact('old', 'heads', 'subheads', 'items'))->with('results', $results);

      }else{
        $results = DB::table('budget')->select( \DB::Raw("
            fhead.headname,
            fsubhead.sname,
            fitem.itemname,
            budget.mode,
            budget.desdate,
            budget.desbus,
            budget.remarks"))
          ->leftjoin('fitem', 'fitem.id', '=', 'budget.itemid')
          ->leftjoin('fsubhead', 'fitem.subid', '=', 'fsubhead.id')
          ->leftjoin('fhead', 'fitem.headid', '=', 'fhead.id')
          ->where('unid', $uid)
          ->orderBy('budget.desdate', 'DESC')
          ->paginate(15);

        return view($this->view_path.'index', compact('results', 'old', 'heads', 'subheads', 'items'));
      }
    }
}
