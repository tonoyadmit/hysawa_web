<?php

namespace App\Http\Controllers\UpAdmin\Finance;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Item;
use App\User;
use Datatables;
use App\Role;
use App\Fhead;
use App\Fsubhead;
use App\Fitemlist;
use App\FinanceData;
use App\Model\FinanceData as MFinanceData;
use Validator;
use Session;
use Redirect;
use DB;
use Auth;
use Entrust;

class ExpenditureController extends Controller
{
  private $view_path = "core.up.finance.expenditure.";
  private $route_path = "up-admin.finace_expenditure.";

  public function index( Request $request )
  {
    $FinanceDatas = MFinanceData::with('district', 'upazila', 'union', 'getHead', 'getSubhead', 'getItem')
                      ->where('userid', '=',\Auth::user()->id)
                      ->where('trans_type', '=', 'ex')
                      ->orderBy('id', 'desc')
                      ->get();

    return view($this->view_path.'index', compact('FinanceDatas'));
  }

  public function create()
  {
    $items = Item::with('head')->groupBy('headid')->get();
      $head = [];
      foreach($items as $item){
        $head[$item->head->id] = $item->head->headname;
      }
    return view($this->view_path.'insert', compact('head'));
  }

  public function store(Request $request)
  {
    $validation = Validator::make($request->all(), [
      'head' => 'required',
      'subhead' => 'required',
      'item' => 'required',
      'mode' => 'required',
      'vou' => 'required',
      'amount' => 'required',
      'date' => 'required'
      ]);

    if($validation->fails()) {
      return Redirect::back()->withErrors($validation)->withInput();
    }

    $FinanceData = new FinanceData();
    $FinanceData->fill($request->except('_token'));
    $FinanceData->unid = auth()->user()->unid;
    $FinanceData->region_id = auth()->user()->region_id;
    $FinanceData->proid = auth()->user()->proj_id;
    $FinanceData->distid = auth()->user()->distid;
    $FinanceData->upid = auth()->user()->upid;
    $FinanceData->trans_type='ex';
    $FinanceData->userid=auth()->user()->id;
    $FinanceData->created_by = auth()->user()->id;
    $FinanceData->updated_by = auth()->user()->id;
    $FinanceData->save();

    \Session::flash('message', "New Data saved successfully");
    return redirect()->route($this->route_path.'index');
  }

  public function show($id)
  {
    $items = Item::with('head')->groupBy('headid')->get();
    $head = [];
    foreach($items as $item){
      $head[$item->head->id] = $item->head->headname;
    }
    $FinanceData = FinanceData::findOrFail($id);
    return view($this->view_path.'view', compact('FinanceData','head'));
  }

  public function edit($id)
  {
    $items = Item::with('head')->groupBy('headid')->get();
    $head = [];
    foreach($items as $item){
      $head[$item->head->id] = $item->head->headname;
    }

    $FinanceData = FinanceData::findOrFail($id);
    $subhead = Fsubhead::where('headid', '=',$FinanceData["head"])->pluck('sname','id')->toArray();
    $subitem = Fitemlist::where('headid', '=',$FinanceData["head"])->where('subid', '=',$FinanceData["subhead"])->pluck('itemname','id')->toArray();

    return view($this->view_path.'edit', compact('FinanceData','head','subhead','subitem'));;
  }

  public function update(Request $request, $id)
  {
    $validation = Validator::make($request->all(), [
      'head' => 'required',
      'subhead' => 'required',
      'item' => 'required',
      'mode' => 'required',
      'vou' => 'required',
      'amount' => 'required',
      'date' => 'required'
      ]);

    if($validation->fails()) {
      return Redirect::back()->withErrors($validation)->withInput();
    }
    $FinanceData = FinanceData::findOrFail($id);
    $FinanceData->fill($request->except('_token'));
    $FinanceData->updated_by = auth()->user()->id;
    $FinanceData->save();
    \Session::flash('message', "One updated successfully");
    return redirect()->route($this->route_path.'index');
  }

  public function subhead(Request $request)
  {
    $requestData = $request->all();
    return $subhead = Fsubhead::where('headid', '=',$requestData["head"])->pluck('sname','id')->toArray();

  }
  public function subitem(Request $request)
  {
    $requestData = $request->all();
    return $subhead = Fitemlist::where('headid', '=',$requestData["head"])->where('subid', '=',$requestData["subhead"])->pluck('itemname','id')->toArray();
  }
}
