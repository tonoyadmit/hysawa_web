<?php

namespace App\Http\Controllers\UpAdmin\Water;

use App\Http\Controllers\Controller;
use App\ImplementStatus;
use App\AppStatus;
use App\Model\Download\WaterDownload;
use App\Model\Search\Model\Water as ModelWater;
use App\Model\Search\Request\WaterSearchRequest;
use App\Model\Search\WaterSearch;
use App\Water;
use App\WaterTech;
use Illuminate\Http\Request;
use Validator;
use Auth;

class WaterController extends Controller
{
    private $view_path = "core.up.water.";
    private $route_path = "up-admin.water.";

    public function index( Request $request )
    {

      $waters = Water::where('unid', \Auth::user()->unid)->where('proj_id', \Auth::user()->proj_id)->where('region_id', \Auth::user()->region_id)->orderBy('id','DESC')->paginate(15);
      $ImplementStatus=ImplementStatus::pluck('imp_status','imp_status')->toArray();
      $approveStatus=AppStatus::pluck('app_status','app_status')->toArray();
      //print_r($approveStatus);exit;
      $old = [
        'starting_date' => '',
        'ending_date'   => '',
        'created_by'    => '',
        'ward_no'       => '',
        'village'       => '',
        'imp_status'=> '',
        'app_status'=> '',
        'unid'       => \Auth::user()->unid,
        'proj_id'       => \Auth::user()->proj_id,
        'region_id'       => \Auth::user()->region_id
      ];

      if($request->has('query'))
      {
        $old = [
          'query'         => $request->input('query'),
          'starting_date' => $request->input('starting_date'),
          'ending_date'   => $request->input('ending_date'),
          'created_by'    => $request->input('created_by'),
          'ward_no'       => $request->input('ward_no'),
          'village'       => $request->input('village'),
          'download'      => time(),
          'imp_status'       => $request->input('imp_status'),
          'app_status'       => $request->input('app_status'),
          'unid'       => \Auth::user()->unid,
          'proj_id'       => \Auth::user()->proj_id,
          'region_id'       => \Auth::user()->region_id
        ];

        //return 'Union id = '.Auth::user()->unid.'Porj id = '.\Auth::user()->proj_id.'Region id = '.\Auth::user()->region_id.'upozila id = '.\Auth::user()->upid.' district id = '.\Auth::user()->distid;
        $waterSearchRequest = new WaterSearchRequest;
        $waterSearchRequest->starting_date = $request->input('starting_date');
        $waterSearchRequest->ending_date = $request->input('ending_date');
        $waterSearchRequest->created_by = $request->input('created_by');
        $waterSearchRequest->unid = \Auth::user()->unid;
        $waterSearchRequest->proj_id = \Auth::user()->proj_id;
        $waterSearchRequest->region_id = \Auth::user()->region_id;
        $waterSearchRequest->upid = \Auth::user()->upid ;
        $waterSearchRequest->distid = \Auth::user()->distid ;
        $waterSearchRequest->ward_no = $request->input('ward_no');
        $waterSearchRequest->village = $request->input('village');
        $waterSearchRequest->imp_status = $request->input('imp_status');
        $waterSearchRequest->app_status = $request->input('app_status');


        $waters = (new WaterSearch($waterSearchRequest))->get();

        
      }

      return view($this->view_path.'index', compact('waters', 'old','ImplementStatus','approveStatus'));
    }

    public function print(Request $request)
    {

      return view($this->view_path.'print')->with('waters',(new ModelWater($request))->get());
    }

    public function download(Request $request)
    {
      return (new WaterDownload( (new ModelWater($request))->get() ))->download();
    }






    public function create()
    {

      $up = array(
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4',
        '5' => '5',
        '6' => '6',
        '7' => '7',
        '8' => '8',
        '9' => '9');

      $wordOrderNO = array(
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4');

      $tech = WaterTech::pluck('techtype','techtype')->toArray();
      $ImplementStatus=ImplementStatus::pluck('imp_status','imp_status')->toArray();
      return view($this->view_path.'insert', compact('up','tech','wordOrderNO','ImplementStatus'));
    }

    public function store(Request $request)
    {

      //if(!\Entrust::can('create-shelf')) { abort(403); }

      $validation = Validator::make($request->all(), [

         'Village'          => 'required',
         'TW_No'  => 'required',
         'Landowner'           => 'required',
         'Caretaker_male'          => 'required',
         'Caretaker_female'          => 'required'
      ]); 

      if($validation->fails()) {
          return redirect()->back()->withErrors($validation)->withInput();
      }

      $water = new Water();
      $water->fill($request->except('_token'));

      $water->unid = auth()->user()->unid;
      $water->region_id = auth()->user()->region_id;
      $water->proj_id = auth()->user()->proj_id;
      $water->distid = auth()->user()->distid;
      $water->upid = auth()->user()->upid;

      $water->created_by = auth()->user()->id;
      $water->updated_by = auth()->user()->id;
      $water->save();

      \Session::flash('message', "New Data saved successfully");
      return redirect()->route($this->route_path.'create');
    }

    public function show($id)
    {
      //if(!Entrust::can('view-water')) { abort(403); }

      $water = Water::findOrFail($id);
      return view($this->view_path.'view', compact('water'));
    }

    public function edit($id)
    {
      //if(!Entrust::can('edit-water')) { abort(403); }

      $up = array('1' => '1','2' => '2','3' => '3','4' => '4','5' => '5','6' => '6','7' => '7','8' => '8','9' => '9');
      $wordOrderNO = array('1' => '1','2' => '2','3' => '3','4' => '4');
      $tech = WaterTech::pluck('techtype','techtype')->toArray();
      $ImplementStatus=ImplementStatus::pluck('imp_status','imp_status')->toArray();
      $water = Water::where('id', '=', $id)->first();

      return view($this->view_path.'edit', compact('water','up','wordOrderNO','tech','ImplementStatus','water'));
    }

    public function update(Request $request, $id)
    {
      $validation = Validator::make($request->all(), [

         'Village'            => 'required',
         'TW_No'              => 'required',
         'Landowner'          => 'required',
         'Caretaker_male'     => 'required',
         'Caretaker_female'   => 'required'
      ]);


      if($validation->fails()) {
          return redirect()->back()->withErrors($validation)->withInput();
      }

      $water = Water::findOrFail($id);
      $water->fill($request->except('_token'));
      $water->updated_by = auth()->user()->id;
      $water->save();

      \Session::flash('message', "One updated successfully");

      return redirect()->route($this->route_path.'index');
    }

    public function destroy($id)
    {
        //
    }


}
