<?php

namespace App\Http\Controllers\UpAdmin\Training;

use App\Http\Controllers\Controller;

class TrainingController extends Controller {

  private $view_path = "core.up.training.";

  public function index()
  {
    $uid= \Auth::user()->unid;
    $paricipants=$reportperiods=\DB::select( \DB::raw("
        SELECT
        trg_title.title,
        tbl_venue.VenueName,
        tbl_training.TrgFrom,
        tbl_training.TrgTo,
        tbl_trg_participants.partcipant_name,
        trg_desg.desg,
        fdistrict.distname,
        fupazila.upname,
        funion.unname,
        tbl_trg_participants.mobile_no,
        tbl_trg_participants.email_address,
        tbl_trg_participants.participant_id
        FROM
          (((tbl_trg_participants
            LEFT JOIN fdistrict ON tbl_trg_participants.district_id = fdistrict.id)
            LEFT JOIN fupazila ON tbl_trg_participants.upazila_id = fupazila.id)
            LEFT JOIN funion ON tbl_trg_participants.union_id = funion.id)
            LEFT JOIN tbl_training ON tbl_trg_participants.TrgCode = tbl_training.TrgCode
            LEFT JOIN tbl_venue ON tbl_venue.VenueID = tbl_training.TrgVenue 
            LEFT JOIN trg_title ON trg_title.id = tbl_training.TrgTitle 
            LEFT JOIN trg_desg ON trg_desg.id = tbl_trg_participants.designation 
            
        WHERE
        tbl_trg_participants.union_id = $uid and tbl_trg_participants.proj_id = ".auth()->user()->proj_id."" ));

    return view($this->view_path.'index', compact('paricipants'));
  }
}