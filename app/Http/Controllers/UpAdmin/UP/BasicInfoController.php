<?php

namespace App\Http\Controllers\UpAdmin\UP;

use App\Http\Controllers\Controller;
use App\Model\Union;
use Illuminate\Http\Request;

class BasicInfoController extends Controller {

  private $view_path = "core.up.up.basic.";
  private $route_path = "up-admin.up.basic-info.";

  public function index()
  {
    $upBasicInfo = \DB::table('up_basic')
                  ->where('distid', \Auth::user()->distid)
                  ->where('upid', \Auth::user()->upid)
                  ->where('unid', \Auth::user()->unid)
                  ->where('proid', \Auth::user()->proj_id)
                  ->get();

    $up = Union::with('upazila.district')->find(\Auth::user()->unid);


    //dd($up->toArray());

    if(count($upBasicInfo))
    {
      $upBasicInfo = $upBasicInfo->first();
    }else{
      $upBasicInfo = "";
    }


    return view($this->view_path.'index', compact('upBasicInfo', 'up'));
  }

  public function update(Request $request)
  {

    if(\Auth::user()->unid == "")
    {
      return redirect()->route($this->route_path.'index');
    }

    $upBasicInfo = \DB::table('up_basic')
                    ->where('distid', \Auth::user()->distid)
                    ->where('upid', \Auth::user()->upid)
                    ->where('unid', \Auth::user()->unid)
                    ->where('proid', \Auth::user()->proj_id)
                    ->get();

    $data = [

      'tpop' => $request->input('tpop'),
      'nmale' => $request->input('nmale'),
      'nfem' => $request->input('nfem'),
      'nchil' => $request->input('nchil'),
      'nhouse' => $request->input('nhouse'),
      'hpoor' => $request->input('hpoor'),
      'sprimary' => $request->input('sprimary'),
      'secondary' => $request->input('secondary'),
      'sother' => $request->input('sother'),
      'ncollege' => $request->input('ncollege'),
      'pmale' => $request->input('pmale'),
      // 'pfemale' => $request->input('pfemale'),
      //'rhindu' => $request->input('rhindu'),
      //'rmuslim' => $request->input('rmuslim'),
      //'rother' => $request->input('rother'),
      'agriculture' => $request->input('agriculture'),
      'business' => $request->input('business'),
      'ocother' => $request->input('ocother'),
      'npond' => $request->input('npond'),
      'nhospital' => $request->input('nhospital'),
      'nbazar' => $request->input('nbazar'),
      'mosque' => $request->input('mosque'),
      'mondir' => $request->input('mondir'),
      'uother' => $request->input('uother'),
      'pwater' => $request->input('pwater'),
      'ditch' => $request->input('ditch'),
      'dproject' => $request->input('dproject'),

      'service' => $request->input('service'),
      'water_other' => $request->input('water_other'),
      'water_shallow_tw' => $request->input('water_shallow_tw'),
      'water_deep_tw' => $request->input('water_deep_tw'),
      'latrine' => $request->input('latrine'),
      'launch_ferry_ghat' => $request->input('launch_ferry_ghat'),
      'household_hh' => $request->input('household_hh'),
      'hardcore_poor_hh' => $request->input('hardcore_poor_hh'),


      'distid' => \Auth::user()->distid,
      'upid' => \Auth::user()->upid,
      'unid' => \Auth::user()->unid,
      'proid' => \Auth::user()->proj_id,

    ];

    $count = true;


    if(!count($upBasicInfo))
    {
      $count = \DB::table('up_basic')->insert($data);

      if($count)
      {
        return redirect()->route($this->route_path.'index');
      }

      return redirect()->route($this->route_path.'index');
    }

    $count = \DB::table('up_basic')
              ->where('id', $upBasicInfo->first()->id)
              ->update($data);

    if($count)
    {
      return redirect()->route($this->route_path.'index');
    }

    return redirect()->route($this->route_path.'index');

  }
}