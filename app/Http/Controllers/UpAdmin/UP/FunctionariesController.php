<?php

namespace App\Http\Controllers\UpAdmin\UP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FunctionariesController extends Controller {

  private $view_path = "core.up.up.functionaries.";
  private $route_path = "up-admin.up.functionaries.";

  public function index()
  {
    $stuffs = \DB::table('union_staff')
              ->where('distid', \Auth::user()->distid)
              ->where('upid', \Auth::user()->upid)
              ->where('unid', \Auth::user()->unid)
              ->where('proid', \Auth::user()->proj_id)
              ->get();

    return view($this->view_path.'index', compact('stuffs'));
  }

  public function store(Request $request)
  {
    $stuff = [
      'name' => $request->input('name'),
      'des' => $request->input('des'),
      'phone' => $request->input('phone'),
      'email' => $request->input('email'),

      'distid' => \Auth::user()->distid,
      'upid' => \Auth::user()->upid,
      'unid' => \Auth::user()->unid,
      'proid' => \Auth::user()->proj_id,
    ];

    $count = \DB::table('union_staff')->insert($stuff);

    if($count){
      return redirect()->route($this->route_path.'index');
    }

    return redirect()->route($this->route_path.'index');
  }

  public function edit(Request $request, $id)
  {
    $stuff = \DB::table('union_staff')->where('id', $id)->get();
    if(!count($stuff))

    {
      return redirect()->route($this->route_path.'index');
    }

    $stuff = $stuff->first();

    return view($this->view_path.'edit', compact('stuff'));
  }

  public function update(Request $request, $id)
  {
    $stuff = \DB::table('union_staff')
                  ->where('id', $id)
                  ->get();
    if(!count($stuff))
    {
      return redirect()->route($this->route_path.'index');
    }

    $stuff = [
      'name' => $request->input('name'),
      'des' => $request->input('des'),
      'phone' => $request->input('phone'),
      'email' => $request->input('email'),

      'distid' => \Auth::user()->distid,
      'upid' => \Auth::user()->upid,
      'unid' => \Auth::user()->unid,
      'proid' => \Auth::user()->proj_id,
    ];

    $count = \DB::table('union_staff')->where('id', $id)->update($stuff);

    if($count){
      return redirect()->route($this->route_path.'index');
    }

    return redirect()->route($this->route_path.'index');
  }
}