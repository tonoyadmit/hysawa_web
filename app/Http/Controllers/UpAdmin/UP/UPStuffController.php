<?php

namespace App\Http\Controllers\UpAdmin\UP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class UPStuffController extends Controller {

  private $view_path = "core.up.up.upstaff.";
  private $route_path = "up-admin.up.staff.";

  public function index()
  {
    $stuffs = \DB::table('project_staff')
              ->where('distid', \Auth::user()->distid)
              ->where('upid', \Auth::user()->upid)
              ->where('unid', \Auth::user()->unid)
              ->where('proid', \Auth::user()->proj_id)
              ->get();

    return view($this->view_path.'index', compact('stuffs'));
  }

  public function store(Request $request)
  {
    $stuff = [
      'name' => $request->input('name'),
      'des' => $request->input('des'),
      'word' => $request->input('word'),
      'phone' => $request->input('phone'),
      'email' => $request->input('email'),
      'sex' => $request->input('sex'),
      'type' => $request->input('type'),

      'distid' => \Auth::user()->distid,
      'upid' => \Auth::user()->upid,
      'unid' => \Auth::user()->unid,
      'proid' => \Auth::user()->proj_id,
    ];

    $count = \DB::table('project_staff')->insert($stuff);

    if($count){
      return redirect()->route($this->route_path.'index');
    }

    return redirect()->route($this->route_path.'index');
  }

  public function edit(Request $request, $id)
  {
    $stuff = \DB::table('project_staff')->where('id', $id)->get();
    if(!count($stuff))

    {
      return redirect()->route($this->route_path.'index');
    }

    $stuff = $stuff->first();

    return view($this->view_path.'edit', compact('stuff'));
  }

  public function update(Request $request, $id)
  {
    $stuff = \DB::table('project_staff')
                  ->where('id', $id)
                  ->get();
    if(!count($stuff))
    {
      return redirect()->route($this->route_path.'index');
    }

    $stuff = [
      'name' => $request->input('name'),
      'des' => $request->input('des'),
      'word' => $request->input('word'),
      'phone' => $request->input('phone'),
      'email' => $request->input('email'),
      'sex' => $request->input('sex'),
      'type' => $request->input('type'),

      'distid' => \Auth::user()->distid,
      'upid' => \Auth::user()->upid,
      'unid' => \Auth::user()->unid,
      'proid' => \Auth::user()->proj_id,
    ];

    $count = \DB::table('project_staff')->where('id', $id)->update($stuff);

    if($count){
      return redirect()->route($this->route_path.'index');
    }

    return redirect()->route($this->route_path.'index');
  }
}