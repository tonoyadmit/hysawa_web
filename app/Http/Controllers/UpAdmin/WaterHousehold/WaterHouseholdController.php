<?php

namespace App\Http\Controllers\UpAdmin\WaterHousehold;

use App\AppStatus;
use App\Http\Controllers\Controller;
use App\ImplementStatus;
use App\Model\Download\HouseholdDownload;
use App\Model\Search\Model\Household as ModelSanitation;
use App\Model\Search\Request\SanitationHouseholdRequest;
use App\Model\Search\SanitationHousehold;
use App\WaterHousehold;
use Illuminate\Http\Request;
use Session;
use Auth;
use Illuminate\Support\Facades\Validator;


class WaterHouseholdController extends Controller
{
  private $view_path = "core.up.household.";
  private $route_path = "up-admin.water_household.";

  public function index( Request $request )
  {

    $sanitations = "";

    $old = [
      'starting_date' => '',
      'ending_date' => '',
      'created_by' => '',
      'village' => '',
      'imp_status'=> '',
      'app_status'=> '',
      'unid'       => \Auth::user()->unid,
      'proj_id'       => \Auth::user()->proj_id,
      'region_id'       => \Auth::user()->region_id
    ];

    $appstatus = AppStatus::pluck('app_status','app_status')->toArray();
    $ImplementStatus=ImplementStatus::pluck('imp_status','imp_status')->toArray();
    
    if($request->has('query')){

      $SanitationHouseholdRequest = new SanitationHouseholdRequest;
      $SanitationHouseholdRequest->starting_date = $request->input('starting_date');
      $SanitationHouseholdRequest->ending_date = $request->input('ending_date');
      $SanitationHouseholdRequest->created_by = $request->input('created_by');
      $SanitationHouseholdRequest->village = $request->input('village');
      $SanitationHouseholdRequest->imp_status = $request->input('imp_status');
      $SanitationHouseholdRequest->app_status = $request->input('app_status');

      $SanitationHouseholdRequest->unid = \Auth::user()->unid ;
      $SanitationHouseholdRequest->proj_id = \Auth::user()->proj_id;
      $SanitationHouseholdRequest->region_id = \Auth::user()->region_id;
      $SanitationHouseholdRequest->upid = \Auth::user()->upid ;
      $SanitationHouseholdRequest->dist_id = \Auth::user()->distid ;

      if($request->has('page'))
      {
        $SanitationHouseholdRequest->page = $request->input('page');
      }

      $old = [
        'query'         => $request->input('query'),
        'starting_date' => $request->input('starting_date'),
        'ending_date'   => $request->input('ending_date'),
        'created_by'    => $request->input('created_by'),
        'village'       => $request->input('village'),
        'imp_status'       => $request->input('imp_status'),
        'app_status'       => $request->input('app_status'),
        'download'      => time(),
        'unid'       => \Auth::user()->unid,
        'proj_id'       => \Auth::user()->proj_id,
        'region_id'       => \Auth::user()->region_id
      ];
      $sanitations = (new SanitationHousehold($SanitationHouseholdRequest))->get();
    }else{
       $sanitations = WaterHousehold::where('unid', \Auth::user()->unid)
            ->where('upid', \Auth::user()->upid)
            ->where('dist_id', \Auth::user()->distid)
            ->where('proj_id', \Auth::user()->proj_id)
            ->where('region_id', \Auth::user()->region_id)
            ->orderBy('id','DESC')->paginate(15);
    }

    return view($this->view_path.'index', compact('sanitations', 'old','ImplementStatus','appstatus'));
  }

  public function download(Request $request)
  {
     
    return (new HouseholdDownload($request))->download();
  }

  public function print(Request $request)
  {
    
    return view($this->view_path.'print')->with('sanitations', (new ModelSanitation($request))->get());
  }

  public function create()
  {
    $mainype = array(
      'Institutional latrine' => 'Institutional latrine',
      'Public latrine' => 'Public latrine',
      'Communal latrine' => 'Communal latrine');

    $subtype = array(
      'Primary School' => 'Primary School',
      'High School' => 'High School',
      'Bazar' => 'Bazar',
      'Madrasha' => 'Madrasha',
      'Mosque' => 'Mosque',
      'Slum' => 'Slum',
      'Community' => 'Community');

    $wordOrderNO = array('1' => '1','2' => '2','3' => '3','4' => '4');
    $appstatus = AppStatus::pluck('app_status','app_status')->toArray();
    $ImplementStatus=ImplementStatus::pluck('imp_status','imp_status')->toArray();

    return view($this->view_path.'insert', compact('subtype','appstatus','wordOrderNO','ImplementStatus','mainype'));
  }

  public function store(Request $request)
  {
    $water = new WaterHousehold();
    $water->fill($request->except('_token'));

    $water->unid = auth()->user()->unid;
    $water->region_id = auth()->user()->region_id;
    $water->proj_id = auth()->user()->proj_id;
    $water->dist_id = auth()->user()->distid;
    $water->upid = auth()->user()->upid;

    $water->created_by = auth()->user()->id;
    $water->updated_by = auth()->user()->id;
    $water->save();
 $validator = Validator::make($request->all(),[
      'mobile'=>'required | max:11| min:11',
  ]);

if($validator->fails()){
    \Session::flash('warning','This field is Required ! You Inserted An Empty field!');
    return redirect()->back()->withErrors($validator)->withInput();
   }else {

    Session::flash('message', "New Data saved successfully");
    return redirect()->route($this->route_path.'create');
  }
}
  public function show($id)
  {
    $water = WaterHousehold::findOrFail($id);
    return view($this->view_path.'view', compact('water'));
  }

  public function edit($id)
  {

    $mainype = array(
      'Institutional latrine' => 'Institutional latrine',
      'Public latrine' => 'Public latrine',
      'Communal latrine' => 'Communal latrine');

    $subtype = array(
      'Primary School' => 'Primary School',
      'High School' => 'High School',
      'Bazar' => 'Bazar',
      'Madrasha' => 'Madrasha',
      'Mosque' => 'Mosque',
      'Slum' => 'Slum',
      'Community' => 'Community');

    $wordOrderNO = array(
      '1' => '1',
      '2' => '2',
      '3' => '3',
      '4' => '4'
    );

    $appstatus = AppStatus::pluck('app_status','app_status')->toArray();
    $ImplementStatus=ImplementStatus::pluck('imp_status','imp_status')->toArray();

    $WaterHousehold = WaterHousehold::where('id', '=', $id)->first();
    return view($this->view_path.'edit', compact('subtype','appstatus','wordOrderNO','ImplementStatus','mainype','WaterHousehold'));
  }

  public function update(Request $request, $id)
  {

    $validator = Validator::make($request->all(),[
      'mobile'=>'required | max:11| min:11',
  ]);

  if($validator->fails()){
    \Session::flash('warning','This field is Required ! You Inserted An Empty field!');
    return redirect()->back()->withErrors($validator)->withInput();
   }else {

    $water = WaterHousehold::findOrFail($id);
    $water->fill($request->except('_token'));
    $water->updated_by = auth()->user()->id;
    $water->save();

    Session::flash('message', "One updated successfully");
    return redirect()->route($this->route_path.'index');
     }
  }
  public function destroy($id)
  {

  }
}
