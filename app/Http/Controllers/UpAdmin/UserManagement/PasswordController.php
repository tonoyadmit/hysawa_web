<?php

namespace App\Http\Controllers\UpAdmin\UserManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Auth;
class PasswordController extends Controller
{
  private $view_path = "core.up.user-management.password.";
  private $route_path = "up-admin.dashboard";

  public function get()
  {
    $user = User::with(
        'userType',
        'region',
        'project',
        'district',
        'upazila',
        'union')->findOrFail(\Auth::user()->id);

    //dd($user->toArray());
    return view($this->view_path.'show', compact('user'));
  }

  public function change(Request $request)
  {
    //dd($request->all());
    $user = User::findOrFail(\Auth::user()->id);

    if( (
        $request->password == "" ||
        $request->password != $request->confirm_password) )
    {
      return redirect()->route($this->route_path);
    }
      $user->password = \Hash::make($request->password) ;
      $user->updated_at = date('Y-m-d H:i:s');
      $user->updated_by = \Auth::user()->id;
      $user->update();


    return redirect()->route($this->route_path);
  }
}
