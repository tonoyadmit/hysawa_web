<?php

namespace App\Http\Controllers\UpAdmin\MobileApp;

use App\Http\Controllers\Controller;
use App\Model\MobAppDataList;
use App\Model\Search\MobAppDataListSearch;
use Illuminate\Http\Request;

class MobileAppController extends Controller {

  private $view_path = "core.up.mobile-app.";

  public function index(Request $request)
  {

 

    $request->request->add([
      'project_id'   => auth()->user()->proj_id,
      'region_id'    => auth()->user()->region_id,
      'district_id'  => auth()->user()->distid,
      'upazila_id'   => auth()->user()->upid,
      'union_id'     => auth()->user()->unid
    ]);

    $old = [
      'project_id'   => auth()->user()->proj_id,
      'region_id'    => auth()->user()->region_id,
      'district_id'  => auth()->user()->distid,
      'upazila_id'   => auth()->user()->upid,
      'union_id'     => auth()->user()->unid,

      'starting_date' => '',
      'ending_date'   => '',
      'cdfno'         => '',
      'type'          => ''
    ];

    if($request->has('query'))
    {
      $old = [
        'query'        => $request->input('query'),
        'project_id'   => auth()->user()->proj_id,
        'region_id'    => auth()->user()->region_id,
        'district_id'  => auth()->user()->distid,
        'upazila_id'   => auth()->user()->upid,
        'union_id'     => auth()->user()->unid,
        'starting_date' => $request->input('starting_date'),
        'ending_date'  => $request->input('ending_date'),
        'cdfno'        => $request->input('cdfno'),
        'type'         => $request->input('type')
      ];
    }

    $datas = (new MobAppDataListSearch($request, true))->get();
    return view($this->view_path.'index', compact('datas', 'old'));
  }

  public function show($id)
  {
    $data = MobAppDataList::with('union.upazila.district')
                  ->with('items.question')
                  ->with('user')
                  ->where('unid', auth()->user()->unid)
                  ->where('region_id', auth()->user()->region_id)
                  ->where('proj_id', auth()->user()->proj_id)
                  ->find($id);
    return view($this->view_path.'show',compact('data'));
  }


  public function print(Request $request)
    {


    $request->request->add([
      'project_id'   => auth()->user()->proj_id,
      'region_id'    => auth()->user()->region_id,
      'district_id'  => auth()->user()->distid,
      'upazila_id'   => auth()->user()->upid,
      'union_id'     => auth()->user()->unid
    ]);

    $old = [
      'project_id'   => auth()->user()->proj_id,
      'region_id'    => auth()->user()->region_id,
      'district_id'  => auth()->user()->distid,
      'upazila_id'   => auth()->user()->upid,
      'union_id'     => auth()->user()->unid,

      'starting_date' => '',
      'ending_date'   => '',
      'cdfno'         => '',
      'type'          => ''
    ];



      if($request->has('print'))
      {
        $old = [
          'query'        => $request->input('query'),
          'project_id'   => auth()->user()->proj_id,
          'region_id'    => auth()->user()->region_id,
          'district_id'  => auth()->user()->distid,
          'upazila_id'   => auth()->user()->upid,
          'union_id'     => auth()->user()->unid,
          'starting_date' => $request->input('starting_date'),
          'ending_date'  => $request->input('ending_date'),
          'cdfno'        => $request->input('cdfno'),
          'type'         => $request->input('type')
        ];
      
      }

      if( $request->input('type') != ""){
      $datas = MobAppDataList::with('union.upazila.district')
      ->with('items.question')
      ->with('user')
      ->where('unid', auth()->user()->unid)
      ->where('region_id', auth()->user()->region_id)
      ->where('proj_id', auth()->user()->proj_id)
      ->where('type',  $request->input('type'))
      ->orderBy('created_at', 'DESC')
      ->get();

      } else{
        $datas = MobAppDataList::with('union.upazila.district')
        ->with('items.question')
        ->with('user')
        ->where('unid', auth()->user()->unid)
        ->where('region_id', auth()->user()->region_id)
        ->where('proj_id', auth()->user()->proj_id)
        ->orderBy('created_at', 'DESC')
        ->get();
  
      }
  

      return view($this->view_path.'print',compact('datas', 'old'));
    }



}