<?php

namespace App\Http\Controllers\UpAdmin\MobileApp;

use App\Http\Controllers\Controller;
use App\Model\CDFNo;
use Illuminate\Http\Request;

class CDFsController extends Controller {

  private $view_path = "core.up.cdf.";
  private $route_path = "up-admin.cdfs.";


  public function index(Request $request)
  {

    if($request->has('print') && ($request->print == "school" || $request->print == "cdf") )
    {
      $cdfs = CDFNo::
        where('type', $request->print)
      ->where('project_id', auth()->user()->proj_id)
      ->where('region_id', auth()->user()->region_id)
      ->where('district_id', auth()->user()->distid)
      ->where('upazila_id', auth()->user()->upid)
      ->where('union_id', auth()->user()->unid)
      ->orderBy('created_at', 'DESC')
      ->get();
      return view($this->view_path.'print', compact('cdfs'));
    }

    $cdfs = CDFNo::where('project_id', auth()->user()->proj_id)
      ->where('region_id', auth()->user()->region_id)
      ->where('district_id', auth()->user()->distid)
      ->where('upazila_id', auth()->user()->upid)
      ->where('union_id', auth()->user()->unid)
      ->orderBy('created_at', 'DESC')
      ->paginate(4000);

    return view($this->view_path.'index', compact('cdfs'));
  }

  public function store(Request $request)
  {
    if(!$request->has('cdf_no')){
      \Session::flash('error', "CDF Not Found.");
      return redirect()->back();
    }

    if($request->type == "school")
    {
      if( !( $request->has('school_title') && $request->input('school_title') != "") )
      {
        \Session::flash('error', "School Title is missing");
        return redirect()->back();
      }
    }

    try{
    //   $cdf = CDFNo::where('cdf_no', $request->cdf_no)->get();

    //   if(count($cdf)){
    //     \Session::flash('error', "This CDF is already exists.");
    //     return redirect()->route($this->route_path.'index');
    //   }

      CDFNo::insert([
        'cdf_no' => $request->input('cdf_no'),
        'project_id' => auth()->user()->proj_id,
        'region_id' => auth()->user()->region_id,
        'district_id' => auth()->user()->distid,
        'upazila_id' => auth()->user()->upid,
        'union_id' => auth()->user()->unid,
        'village' => $request->input('village'),
        'area' => $request->input('area'),
        'created_by' => auth()->user()->id,
        'updated_by' => auth()->user()->id,
        'created_at' => date('Y-m-d H:i:s'),

        'type' => $request->input('type'),
        'school_title' => $request->input('school_title'),
        'word_no' => $request->input('word_no'),
        'owner' => $request->input('owner')
      ]);

      \Session::flash('message', "New CDF info added");
      return redirect()->route($this->route_path."index");
    }catch(\Exception $e){
      \Log::critical($e->getMessage());
      \Session::flash('error', "Something Went wrong. Please Try Again.");
      return redirect()->route($this->route_path."index");
    }
  }

  public function edit(Request $request, $id)
  {
    $cdf = CDFNo::with('district', 'upazila', 'union')->find($id);
    if(empty($cdf)){
      \Session::flash('error', "CDF Not Found.");
      return redirect()->route($this->route_path.'index');
    }

    return view($this->view_path.'edit', compact('cdf'));
  }

  public function update(Request $request, $id)
  {
    $cdf = CDFNo::find($id);

    if(empty($cdf)){
      \Session::flash('error', "CDF Not Found.");
      return redirect()->route($this->route_path.'index');
    }

    if($request->type == "school")
    {
      if( !( $request->has('school_title') && $request->input('school_title') != "") ){
        \Session::flash('error', "School Title is missing");
        return redirect()->back();
      }
    }

    try{
      CDFNo::where('id', $id)
        ->update([
          'cdf_no' => $cdf->cdf_no,
          'village' => $request->input('village'),
          'word_no' => $request->input('word_no'),

          'updated_by' => auth()->user()->id,
          'updated_at' => date('Y-m-d H:i:s'),

          'type' => $request->input('type'),
          'school_title' => $request->input('school_title'),
          'owner' => $request->input('owner')
        ]);

      \Session::flash('message', "CDF Info Updated");
      return redirect()->route($this->route_path."index");
    }catch(\Exception $e){
      \Log::critical($e->getMessage());
      \Session::flash('error', "Something went wrong!");
      return redirect()->route($this->route_path."index");
    }
  }
}