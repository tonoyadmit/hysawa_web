<?php

namespace App\Http\Controllers\UpAdmin\MonthlyReport;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\User;
use Datatables;
use App\Role;
use App\ImplementStatus;
use App\ReportPeriod;
use App\ReportData;
use App\AppStatus;
use Validator;
use Session;
use Redirect;
use DB;
use Auth;
use Entrust;
use App\Model\District;
use App\Model\Union;
use App\Model\Upazila;

class MonthlyReportControllerBK extends Controller
{
    private $view_path = "core.up.MonthlyReport.";
    private $route_path = "up-admin.report_period.";

    public function index( Request $request )
    {
      $uid= auth()->user()->unid;
        $report=\DB::select(\DB::raw("SELECT MONTHNAME(CURDATE() + INTERVAL -6 MONTH) AS `month`,YEAR(CURDATE() + INTERVAL -1 MONTH) AS `year`"));

        $cyear=$report[0]->year;
        $cmonth=$report[0]->month;
        $report=\DB::select(\DB::raw(" SELECT rep_period.rep_id FROM  rep_period WHERE  SUBSTRING_INDEX(period,' ',-1)='$cmonth'AND SUBSTRING_INDEX(period,' ',1)='$cyear'"));

        $cid=$report[0]->rep_id;
        $lid=$cid+12;
//        $cyear=$report[0]["year"];
//        $cmonth=$report[0]["month"];

      $reportperiods=\DB::select(\DB::raw("
        SELECT rep_data.rep_id, rep_period.period, rep_data.id, rep_data.`updated_at`
        FROM rep_data, rep_period
        WHERE rep_data.rep_id = rep_period.rep_id
        AND rep_data.unid=$uid AND (rep_period.rep_id BETWEEN $cid AND $lid OR rep_period.rep_id =100 OR rep_period.rep_id =99)"));

      return view($this->view_path ."index", compact('reportperiods'));
    }

    public function create()
    {
      $mainype = array('Institutional latrine' => 'Institutional latrine','Public latrine' => 'Public latrine','Communal latrine' => 'Communal latrine');
      $subtype = array('Primary School' => 'Primary School','High School' => 'High School','Bazar' => 'Bazar','Madrasha' => 'Madrasha','Mosque' => 'Mosque','Slum' => 'Slum','Community' => 'Community');
      $wordOrderNO = array('1' => '1','2' => '2','3' => '3','4' => '4');
      $appstatus = AppStatus::pluck('app_status','id')->toArray();
      $ImplementStatus=ImplementStatus::pluck('imp_status','id')->toArray();
      //print_r($mainype);exit;
      return view($this->view_path.'insert', compact('subtype','appstatus','wordOrderNO','ImplementStatus','mainype'));
    }

    public function setReportMonth()
    {
        $uid= auth()->user()->unid;

        $report=\DB::select(\DB::raw("SELECT MONTHNAME(CURDATE() + INTERVAL -6 MONTH) AS `month`,YEAR(CURDATE() + INTERVAL -1 MONTH) AS `year`"));
        $cyear=$report[0]->year;
        $cmonth=$report[0]->month;
        $report=\DB::select(\DB::raw(" SELECT rep_period.rep_id FROM  rep_period WHERE  SUBSTRING_INDEX(period,' ',-1)='$cmonth'AND SUBSTRING_INDEX(period,' ',1)='$cyear'"));

        $cid=$report[0]->rep_id;
        $lid=$cid+12;

      $reportperiods=DB::select( DB::raw("SELECT  id,period FROM rep_period WHERE id NOT IN (SELECT rep_id FROM rep_data WHERE unid=$uid) AND (rep_period.rep_id BETWEEN $cid AND $lid OR rep_period.rep_id =100 OR rep_period.rep_id =99)" ));
      $arrreportperiods = [];
      foreach($reportperiods as $row)
      {
          $arrreportperiods[$row->id] = $row->period;
      }
      return view($this->view_path.'setReportMonthInsert',compact('arrreportperiods'));
    }


    public function setReportMonthStore(Request $request,$id)
    {
      $uid= auth()->user()->unid;
      $ReportData =new  ReportData;
      $ReportData->fill($request->except('_token'));
      $ReportData->distid = auth()->user()->distid;
      $ReportData->upid = auth()->user()->upid;
      $ReportData->unid = $uid;
      $ReportData->created_by = auth()->user()->id;
      $ReportData->updated_by = auth()->user()->id;
      $ReportData->save();
      \Session::flash('message', "New Data saved successfully");
      return redirect()->route($this->route_path.'index'); //'/setReportMonth');
    }


    public function entryForm( Request $request )
    {
      $uid= auth()->user()->unid;
      $requestData=$request->all();
      $id=$request->input("id");
      $rid=$request->input("rep_id");
      $row_lastdata=DB::select(
        DB::raw("
        SELECT
        Sum(rep_data.cdf_no) AS SumOfcdf_no,
        Sum(sa_approved),
        Sum(sa_completed),
        Sum(sa_benef),
        Sum(sa_renovated),
        Sum(ws_approved),
        Sum(ws_completed),
        Sum(ws_beneficiary),
        Sum(ws_hc_benef),
        Sum(ws_50),
        Sum(rep_data.cdf_pop) AS SumOfcdf_pop,
        Sum(rep_data.cdf_male) AS SumOfcdf_male,
        Sum(rep_data.cdf_female) AS SumOfcdf_female,
        Sum(rep_data.cdf_pop_hc) AS SumOfcdf_pop_hc,
        Sum(rep_data.cdf_hh) AS SumOfcdf_hh,
        Sum(rep_data.cdf_hh_hc) AS SumOfcdf_hh_hc,
        Sum(rep_data.cdf_pop_disb) AS SumOfcdf_pop_disb,
        Sum(rep_data.cdf_pop_safety) AS SumOfcdf_pop_safety,
        Sum(rep_data.cdf_cf_tot) AS SumOfcdf_cf_tot,
        Sum(rep_data.cdf_cf_male) AS SumOfcdf_cf_male,
        Sum(rep_data.cdf_cf_female) AS SumOfcdf_cf_female,
        Sum(rep_data.cb_trg) AS SumOfcb_trg,
        Sum(rep_data.cb_trg_up_total) AS SumOfcb_trg_up_total,
        Sum(rep_data.cb_trg_up_male) AS SumOfcb_trg_up_male,
        Sum(rep_data.cb_trg_up_female) AS SumOfcb_trg_up_female,
        Sum(rep_data.cb_trg_stf_total) AS SumOfcb_trg_stf_total,
        Sum(rep_data.cb_trg_stf_male) AS SumOfcb_trg_stf_male,
        Sum(rep_data.cb_trg_stf_female) AS SumOfcb_trg_stf_female,
        Sum(rep_data.cb_trg_vol_total) AS SumOfcb_trg_vol_total,
        Sum(rep_data.cb_trg_vol_male) AS SumOfcb_trg_vol_male,
        Sum(rep_data.cb_trg_vol_female) AS SumOfcb_trg_vol_female,
        Sum(rep_data.CHY_hw_ses) AS SumOfCHY_hw_ses,
        Sum(rep_data.CHY_hw_male) AS SumOfCHY_hw_male,
        Sum(rep_data.CHY_hw_female) AS SumOfCHY_hw_female,
        Sum(rep_data.CHY_mn_ses) AS SumOfCHY_mn_ses,
        Sum(rep_data.CHY_mn_female) AS SumOfCHY_mn_female,
        Sum(rep_data.CHY_sa_ses) AS SumOfCHY_sa_ses,
        Sum(rep_data.CHY_sa_hh) AS SumOfCHY_sa_hh,
        Sum(rep_data.CHY_fh_ses) AS SumOfCHY_fh_ses,
        Sum(rep_data.CHY_fh_hh) AS SumOfCHY_fh_hh,
        Sum(rep_data.CHY_gb_new) AS SumOfCHY_gb_new,
        Sum(rep_data.CHY_gb_rep) AS SumOfCHY_gb_rep,
        Sum(rep_data.CHY_dr) AS SumOfCHY_dr,
        Sum(rep_data.CHY_dr_pop) AS SumOfCHY_dr_pop,
        Sum(rep_data.CHY_vd) AS SumOfCHY_vd,
        Sum(rep_data.CHY_vd_pop) AS SumOfCHY_vd_pop,
        Sum(rep_data.HHS_new) AS SumOfHHS_new,
        Sum(rep_data.HHS_new_hc) AS SumOfHHS_new_hc,
        Sum(rep_data.HHS_rep) AS SumOfHHS_rep,
        Sum(rep_data.HHS_rep_hc) AS SumOfHHS_rep_hc,
        Sum(rep_data.scl_tot) AS SumOfscl_tot,
        Sum(rep_data.scl_tot_std) AS SumOfscl_tot_std,
        Sum(rep_data.scl_boys) AS SumOfscl_boys,
        Sum(rep_data.scl_girls) AS SumOfscl_girls,
        Sum(rep_data.scl_pri) AS SumOfscl_pri,
        Sum(rep_data.scl_pri_std) AS SumOfscl_pri_std,
        Sum(rep_data.scl_high) AS SumOfscl_high,
        Sum(rep_data.scl_high_std) AS SumOfscl_high_std,
        Sum(rep_data.scl_mad) AS SumOfscl_mad,
        Sum(rep_data.scl_mad_std) AS SumOfscl_mad_std,
        Sum(rep_data.scl_hp_ses) AS SumOfscl_hp_ses,
        Sum(rep_data.scl_hp_boys) AS SumOfscl_hp_boys,
        Sum(rep_data.scl_hp_girls) AS SumOfscl_hp_girls,
        Sum(rep_data.scl_mn_ses) AS SumOfscl_mn_ses,
        Sum(rep_data.scl_mn_girls) AS SumOfscl_mn_girls,
        Sum(rep_data.scl_dr) AS SumOfscl_dr,
        Sum(rep_data.scl_dr_std) AS SumOfscl_dr_std,
        Sum(rep_data.scl_vd) AS SumOfscl_vd,
        Sum(rep_data.scl_vd_std) AS SumOfscl_vd_std,
        Sum(rep_data.up_stf_tot) AS SumOfup_stf_tot,
        Sum(rep_data.up_stf_male) AS SumOfup_stf_male,
        Sum(rep_data.up_stf_female) AS SumOfup_stf_female,
        Sum(rep_data.up_pngo) AS SumOfup_pngo,
        Sum(rep_data.up_cont) AS SumOfup_cont,
        Sum(rep_data.TW_maintenance) AS SumOfTW_maintenance,
        Sum(rep_data.wsp) AS SumOfwsp,
        Sum(rep_data.CT_trg),
        Sum(rep_data.pdb)
        FROM rep_data
        WHERE rep_id < $rid  AND rep_data.unid = $uid"));

      $row_lastdata = json_decode(json_encode((array) $row_lastdata), true);
      $row_lastdata=$row_lastdata[0];

      $row_totaldata=DB::select( DB::raw("SELECT Sum(rep_data.cdf_no) AS SumOfcdf_no, Sum(sa_approved), Sum(sa_completed), Sum(sa_benef), Sum(sa_renovated), Sum(ws_approved), Sum(ws_completed), Sum(ws_beneficiary), Sum(ws_hc_benef), Sum(ws_50), Sum(rep_data.cdf_pop) AS SumOfcdf_pop, Sum(rep_data.cdf_male) AS SumOfcdf_male, Sum(rep_data.cdf_female) AS SumOfcdf_female, Sum(rep_data.cdf_pop_hc) AS SumOfcdf_pop_hc, Sum(rep_data.cdf_hh) AS SumOfcdf_hh, Sum(rep_data.cdf_hh_hc) AS SumOfcdf_hh_hc, Sum(rep_data.cdf_pop_disb) AS SumOfcdf_pop_disb, Sum(rep_data.cdf_pop_safety) AS SumOfcdf_pop_safety, Sum(rep_data.cdf_cf_tot) AS SumOfcdf_cf_tot, Sum(rep_data.cdf_cf_male) AS SumOfcdf_cf_male, Sum(rep_data.cdf_cf_female) AS SumOfcdf_cf_female, Sum(rep_data.cb_trg) AS SumOfcb_trg, Sum(rep_data.cb_trg_up_total) AS SumOfcb_trg_up_total, Sum(rep_data.cb_trg_up_male) AS SumOfcb_trg_up_male, Sum(rep_data.cb_trg_up_female) AS SumOfcb_trg_up_female, Sum(rep_data.cb_trg_stf_total) AS SumOfcb_trg_stf_total, Sum(rep_data.cb_trg_stf_male) AS SumOfcb_trg_stf_male, Sum(rep_data.cb_trg_stf_female) AS SumOfcb_trg_stf_female, Sum(rep_data.cb_trg_vol_total) AS SumOfcb_trg_vol_total, Sum(rep_data.cb_trg_vol_male) AS SumOfcb_trg_vol_male, Sum(rep_data.cb_trg_vol_female) AS SumOfcb_trg_vol_female, Sum(rep_data.CHY_hw_ses) AS SumOfCHY_hw_ses, Sum(rep_data.CHY_hw_male) AS SumOfCHY_hw_male, Sum(rep_data.CHY_hw_female) AS SumOfCHY_hw_female, Sum(rep_data.CHY_mn_ses) AS SumOfCHY_mn_ses, Sum(rep_data.CHY_mn_female) AS SumOfCHY_mn_female, Sum(rep_data.CHY_sa_ses) AS SumOfCHY_sa_ses, Sum(rep_data.CHY_sa_hh) AS SumOfCHY_sa_hh, Sum(rep_data.CHY_fh_ses) AS SumOfCHY_fh_ses, Sum(rep_data.CHY_fh_hh) AS SumOfCHY_fh_hh, Sum(rep_data.CHY_gb_new) AS SumOfCHY_gb_new, Sum(rep_data.CHY_gb_rep) AS SumOfCHY_gb_rep, Sum(rep_data.CHY_dr) AS SumOfCHY_dr, Sum(rep_data.CHY_dr_pop) AS SumOfCHY_dr_pop, Sum(rep_data.CHY_vd) AS SumOfCHY_vd, Sum(rep_data.CHY_vd_pop) AS SumOfCHY_vd_pop, Sum(rep_data.HHS_new) AS SumOfHHS_new, Sum(rep_data.HHS_new_hc) AS SumOfHHS_new_hc, Sum(rep_data.HHS_rep) AS SumOfHHS_rep, Sum(rep_data.HHS_rep_hc) AS SumOfHHS_rep_hc, Sum(rep_data.scl_tot) AS SumOfscl_tot, Sum(rep_data.scl_tot_std) AS SumOfscl_tot_std, Sum(rep_data.scl_boys) AS SumOfscl_boys, Sum(rep_data.scl_girls) AS SumOfscl_girls, Sum(rep_data.scl_pri) AS SumOfscl_pri, Sum(rep_data.scl_pri_std) AS SumOfscl_pri_std, Sum(rep_data.scl_high) AS SumOfscl_high, Sum(rep_data.scl_high_std) AS SumOfscl_high_std, Sum(rep_data.scl_mad) AS SumOfscl_mad, Sum(rep_data.scl_mad_std) AS SumOfscl_mad_std, Sum(rep_data.scl_hp_ses) AS SumOfscl_hp_ses, Sum(rep_data.scl_hp_boys) AS SumOfscl_hp_boys, Sum(rep_data.scl_hp_girls) AS SumOfscl_hp_girls, Sum(rep_data.scl_mn_ses) AS SumOfscl_mn_ses, Sum(rep_data.scl_mn_girls) AS SumOfscl_mn_girls, Sum(rep_data.scl_dr) AS SumOfscl_dr, Sum(rep_data.scl_dr_std) AS SumOfscl_dr_std, Sum(rep_data.scl_vd) AS SumOfscl_vd, Sum(rep_data.scl_vd_std) AS SumOfscl_vd_std, Sum(rep_data.up_stf_tot) AS SumOfup_stf_tot, Sum(rep_data.up_stf_male) AS SumOfup_stf_male, Sum(rep_data.up_stf_female) AS SumOfup_stf_female, Sum(rep_data.up_pngo) AS SumOfup_pngo, Sum(rep_data.up_cont) AS SumOfup_cont, Sum(rep_data.TW_maintenance) AS SumOfTW_maintenance, Sum(rep_data.wsp) AS SumOfwsp,Sum(rep_data.CT_trg),Sum(rep_data.pdb) FROM rep_data WHERE rep_id <= $rid AND rep_data.unid=$uid" ));

      $row_totaldata = json_decode(json_encode((array) $row_totaldata), true);
      $row_totaldata=$row_totaldata[0];

      $row_getbaseline=DB::select( DB::raw("SELECT * FROM rep_data WHERE rep_id = 99 and unid=$uid" ));
      $row_getbaseline = json_decode(json_encode((array) $row_getbaseline), true);
      $row_getbaseline = @$row_getbaseline[0];

      $row_gettargets=DB::select( DB::raw("SELECT * FROM rep_data WHERE rep_id = 100  and unid=$uid" ));
      $row_gettargets = json_decode(json_encode((array) $row_gettargets), true);
      $row_gettargets = @$row_gettargets[0];

      $ReportData = ReportData::findOrFail($id);

      return view($this->view_path.'insert', compact('ReportData','row_lastdata','row_totaldata','row_getbaseline','row_gettargets'));
    }

    public function entryFormStore( Request $request, $id )
    {
      $ReportData = ReportData::findOrFail($id);
      $ReportData->fill($request->except('_token'));
      $ReportData->updated_by = auth()->user()->id;
      $ReportData->save();

      Session::flash('message', "One updated successfully");
       return redirect()->route($this->route_path.'index');



    }


    public function print(Request $request)
    {
      $uid= auth()->user()->unid;
      $requestData=$request->all();
      $id=$request->input("id");
      $rid=$request->input("rep_id");
      $row_lastdata=DB::select(
        DB::raw("
        SELECT
        Sum(rep_data.cdf_no) AS SumOfcdf_no,
        Sum(sa_approved),
        Sum(sa_completed),
        Sum(sa_benef),
        Sum(sa_renovated),
        Sum(ws_approved),
        Sum(ws_completed),
        Sum(ws_beneficiary),
        Sum(ws_hc_benef),
        Sum(ws_50),
        Sum(rep_data.cdf_pop) AS SumOfcdf_pop,
        Sum(rep_data.cdf_male) AS SumOfcdf_male,
        Sum(rep_data.cdf_female) AS SumOfcdf_female,
        Sum(rep_data.cdf_pop_hc) AS SumOfcdf_pop_hc,
        Sum(rep_data.cdf_hh) AS SumOfcdf_hh,
        Sum(rep_data.cdf_hh_hc) AS SumOfcdf_hh_hc,
        Sum(rep_data.cdf_pop_disb) AS SumOfcdf_pop_disb,
        Sum(rep_data.cdf_pop_safety) AS SumOfcdf_pop_safety,
        Sum(rep_data.cdf_cf_tot) AS SumOfcdf_cf_tot,
        Sum(rep_data.cdf_cf_male) AS SumOfcdf_cf_male,
        Sum(rep_data.cdf_cf_female) AS SumOfcdf_cf_female,
        Sum(rep_data.cb_trg) AS SumOfcb_trg,
        Sum(rep_data.cb_trg_up_total) AS SumOfcb_trg_up_total,
        Sum(rep_data.cb_trg_up_male) AS SumOfcb_trg_up_male,
        Sum(rep_data.cb_trg_up_female) AS SumOfcb_trg_up_female,
        Sum(rep_data.cb_trg_stf_total) AS SumOfcb_trg_stf_total,
        Sum(rep_data.cb_trg_stf_male) AS SumOfcb_trg_stf_male,
        Sum(rep_data.cb_trg_stf_female) AS SumOfcb_trg_stf_female,
        Sum(rep_data.cb_trg_vol_total) AS SumOfcb_trg_vol_total,
        Sum(rep_data.cb_trg_vol_male) AS SumOfcb_trg_vol_male,
        Sum(rep_data.cb_trg_vol_female) AS SumOfcb_trg_vol_female,
        Sum(rep_data.CHY_hw_ses) AS SumOfCHY_hw_ses,
        Sum(rep_data.CHY_hw_male) AS SumOfCHY_hw_male,
        Sum(rep_data.CHY_hw_female) AS SumOfCHY_hw_female,
        Sum(rep_data.CHY_mn_ses) AS SumOfCHY_mn_ses,
        Sum(rep_data.CHY_mn_female) AS SumOfCHY_mn_female,
        Sum(rep_data.CHY_sa_ses) AS SumOfCHY_sa_ses,
        Sum(rep_data.CHY_sa_hh) AS SumOfCHY_sa_hh,
        Sum(rep_data.CHY_fh_ses) AS SumOfCHY_fh_ses,
        Sum(rep_data.CHY_fh_hh) AS SumOfCHY_fh_hh,
        Sum(rep_data.CHY_gb_new) AS SumOfCHY_gb_new,
        Sum(rep_data.CHY_gb_rep) AS SumOfCHY_gb_rep,
        Sum(rep_data.CHY_dr) AS SumOfCHY_dr,
        Sum(rep_data.CHY_dr_pop) AS SumOfCHY_dr_pop,
        Sum(rep_data.CHY_vd) AS SumOfCHY_vd,
        Sum(rep_data.CHY_vd_pop) AS SumOfCHY_vd_pop,
        Sum(rep_data.HHS_new) AS SumOfHHS_new,
        Sum(rep_data.HHS_new_hc) AS SumOfHHS_new_hc,
        Sum(rep_data.HHS_rep) AS SumOfHHS_rep,
        Sum(rep_data.HHS_rep_hc) AS SumOfHHS_rep_hc,
        Sum(rep_data.scl_tot) AS SumOfscl_tot,
        Sum(rep_data.scl_tot_std) AS SumOfscl_tot_std,
        Sum(rep_data.scl_boys) AS SumOfscl_boys,
        Sum(rep_data.scl_girls) AS SumOfscl_girls,
        Sum(rep_data.scl_pri) AS SumOfscl_pri,
        Sum(rep_data.scl_pri_std) AS SumOfscl_pri_std,
        Sum(rep_data.scl_high) AS SumOfscl_high,
        Sum(rep_data.scl_high_std) AS SumOfscl_high_std,
        Sum(rep_data.scl_mad) AS SumOfscl_mad,
        Sum(rep_data.scl_mad_std) AS SumOfscl_mad_std,
        Sum(rep_data.scl_hp_ses) AS SumOfscl_hp_ses,
        Sum(rep_data.scl_hp_boys) AS SumOfscl_hp_boys,
        Sum(rep_data.scl_hp_girls) AS SumOfscl_hp_girls,
        Sum(rep_data.scl_mn_ses) AS SumOfscl_mn_ses,
        Sum(rep_data.scl_mn_girls) AS SumOfscl_mn_girls,
        Sum(rep_data.scl_dr) AS SumOfscl_dr,
        Sum(rep_data.scl_dr_std) AS SumOfscl_dr_std,
        Sum(rep_data.scl_vd) AS SumOfscl_vd,
        Sum(rep_data.scl_vd_std) AS SumOfscl_vd_std,
        Sum(rep_data.up_stf_tot) AS SumOfup_stf_tot,
        Sum(rep_data.up_stf_male) AS SumOfup_stf_male,
        Sum(rep_data.up_stf_female) AS SumOfup_stf_female,
        Sum(rep_data.up_pngo) AS SumOfup_pngo,
        Sum(rep_data.up_cont) AS SumOfup_cont,
        Sum(rep_data.TW_maintenance) AS SumOfTW_maintenance,
        Sum(rep_data.wsp) AS SumOfwsp,
        Sum(rep_data.CT_trg),
        Sum(rep_data.pdb)
        FROM rep_data
        WHERE rep_id < $rid  AND rep_data.unid = $uid"));

      $row_lastdata = json_decode(json_encode((array) $row_lastdata), true);
      $row_lastdata=$row_lastdata[0];

      $row_totaldata=DB::select( DB::raw("SELECT Sum(rep_data.cdf_no) AS SumOfcdf_no, Sum(sa_approved), Sum(sa_completed), Sum(sa_benef), Sum(sa_renovated), Sum(ws_approved), Sum(ws_completed), Sum(ws_beneficiary), Sum(ws_hc_benef), Sum(ws_50), Sum(rep_data.cdf_pop) AS SumOfcdf_pop, Sum(rep_data.cdf_male) AS SumOfcdf_male, Sum(rep_data.cdf_female) AS SumOfcdf_female, Sum(rep_data.cdf_pop_hc) AS SumOfcdf_pop_hc, Sum(rep_data.cdf_hh) AS SumOfcdf_hh, Sum(rep_data.cdf_hh_hc) AS SumOfcdf_hh_hc, Sum(rep_data.cdf_pop_disb) AS SumOfcdf_pop_disb, Sum(rep_data.cdf_pop_safety) AS SumOfcdf_pop_safety, Sum(rep_data.cdf_cf_tot) AS SumOfcdf_cf_tot, Sum(rep_data.cdf_cf_male) AS SumOfcdf_cf_male, Sum(rep_data.cdf_cf_female) AS SumOfcdf_cf_female, Sum(rep_data.cb_trg) AS SumOfcb_trg, Sum(rep_data.cb_trg_up_total) AS SumOfcb_trg_up_total, Sum(rep_data.cb_trg_up_male) AS SumOfcb_trg_up_male, Sum(rep_data.cb_trg_up_female) AS SumOfcb_trg_up_female, Sum(rep_data.cb_trg_stf_total) AS SumOfcb_trg_stf_total, Sum(rep_data.cb_trg_stf_male) AS SumOfcb_trg_stf_male, Sum(rep_data.cb_trg_stf_female) AS SumOfcb_trg_stf_female, Sum(rep_data.cb_trg_vol_total) AS SumOfcb_trg_vol_total, Sum(rep_data.cb_trg_vol_male) AS SumOfcb_trg_vol_male, Sum(rep_data.cb_trg_vol_female) AS SumOfcb_trg_vol_female, Sum(rep_data.CHY_hw_ses) AS SumOfCHY_hw_ses, Sum(rep_data.CHY_hw_male) AS SumOfCHY_hw_male, Sum(rep_data.CHY_hw_female) AS SumOfCHY_hw_female, Sum(rep_data.CHY_mn_ses) AS SumOfCHY_mn_ses, Sum(rep_data.CHY_mn_female) AS SumOfCHY_mn_female, Sum(rep_data.CHY_sa_ses) AS SumOfCHY_sa_ses, Sum(rep_data.CHY_sa_hh) AS SumOfCHY_sa_hh, Sum(rep_data.CHY_fh_ses) AS SumOfCHY_fh_ses, Sum(rep_data.CHY_fh_hh) AS SumOfCHY_fh_hh, Sum(rep_data.CHY_gb_new) AS SumOfCHY_gb_new, Sum(rep_data.CHY_gb_rep) AS SumOfCHY_gb_rep, Sum(rep_data.CHY_dr) AS SumOfCHY_dr, Sum(rep_data.CHY_dr_pop) AS SumOfCHY_dr_pop, Sum(rep_data.CHY_vd) AS SumOfCHY_vd, Sum(rep_data.CHY_vd_pop) AS SumOfCHY_vd_pop, Sum(rep_data.HHS_new) AS SumOfHHS_new, Sum(rep_data.HHS_new_hc) AS SumOfHHS_new_hc, Sum(rep_data.HHS_rep) AS SumOfHHS_rep, Sum(rep_data.HHS_rep_hc) AS SumOfHHS_rep_hc, Sum(rep_data.scl_tot) AS SumOfscl_tot, Sum(rep_data.scl_tot_std) AS SumOfscl_tot_std, Sum(rep_data.scl_boys) AS SumOfscl_boys, Sum(rep_data.scl_girls) AS SumOfscl_girls, Sum(rep_data.scl_pri) AS SumOfscl_pri, Sum(rep_data.scl_pri_std) AS SumOfscl_pri_std, Sum(rep_data.scl_high) AS SumOfscl_high, Sum(rep_data.scl_high_std) AS SumOfscl_high_std, Sum(rep_data.scl_mad) AS SumOfscl_mad, Sum(rep_data.scl_mad_std) AS SumOfscl_mad_std, Sum(rep_data.scl_hp_ses) AS SumOfscl_hp_ses, Sum(rep_data.scl_hp_boys) AS SumOfscl_hp_boys, Sum(rep_data.scl_hp_girls) AS SumOfscl_hp_girls, Sum(rep_data.scl_mn_ses) AS SumOfscl_mn_ses, Sum(rep_data.scl_mn_girls) AS SumOfscl_mn_girls, Sum(rep_data.scl_dr) AS SumOfscl_dr, Sum(rep_data.scl_dr_std) AS SumOfscl_dr_std, Sum(rep_data.scl_vd) AS SumOfscl_vd, Sum(rep_data.scl_vd_std) AS SumOfscl_vd_std, Sum(rep_data.up_stf_tot) AS SumOfup_stf_tot, Sum(rep_data.up_stf_male) AS SumOfup_stf_male, Sum(rep_data.up_stf_female) AS SumOfup_stf_female, Sum(rep_data.up_pngo) AS SumOfup_pngo, Sum(rep_data.up_cont) AS SumOfup_cont, Sum(rep_data.TW_maintenance) AS SumOfTW_maintenance, Sum(rep_data.wsp) AS SumOfwsp,Sum(rep_data.CT_trg),Sum(rep_data.pdb) FROM rep_data WHERE rep_id <= $rid AND rep_data.unid=$uid" ));

      $row_totaldata = json_decode(json_encode((array) $row_totaldata), true);
      $row_totaldata=$row_totaldata[0];

      $row_getbaseline=DB::select( DB::raw("SELECT * FROM rep_data WHERE rep_id = 99 and unid=$uid" ));
      $row_getbaseline = json_decode(json_encode((array) $row_getbaseline), true);
      $row_getbaseline = @$row_getbaseline[0];

      $row_gettargets=DB::select( DB::raw("SELECT * FROM rep_data WHERE rep_id = 100  and unid=$uid" ));
      $row_gettargets = json_decode(json_encode((array) $row_gettargets), true);
      $row_gettargets = @$row_gettargets[0];

      //$ReportData = ReportData::findOrFail($id);

      $ReportData=DB::select( DB::raw("SELECT * FROM rep_data WHERE id = $id  and unid=$uid" ));
      $ReportData = json_decode(json_encode((array) $ReportData), true);
      $ReportData = @$ReportData[0];

      $user = \Auth::user();
      $unions = Union::where('upid', $user->upid)->get();
      $union = Union::with('upazila.district.region')->find($uid);

     $period = \DB::table('rep_period')->where('rep_id', $rid)->first();

      return view($this->view_path.'print', compact('ReportData','row_lastdata','row_totaldata','row_getbaseline','row_gettargets','unions','union','period'));
    }
}
