<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use Datatables;
use App\Role;

use App\UserType;
use Validator;
use Session;
use Redirect;
use Image;
use DB;
use Auth;
use Entrust;

class UserController extends Controller
{

/**
* Create a new controller instance.
*
* @return void
*/
public function __construct()
{
    $this->middleware('role:superadministrator|administrator');
}

/**
* Display a listing of the resource.
*
* @return \Illuminate\Http\Response
*/
public function index( Request $request )
{
    if(!Entrust::can('user-list')) { abort(403); }
//$req  =  User::all();
//print_r($req);exit;
    $users = User::with('roles')->orderBy('id','DESC')->paginate(10);
//print_r($users);
    return view('users.index',compact('users'));

}

/**
* Show the form for creating a new resource.
*
* @return \Illuminate\Http\Response
*/
public function create(Request $request)
{
    if(!Entrust::can('create-user')) { abort(403); }
    $roles = Role::pluck('display_name','id');

    return view('users.insert', compact('prefix', 'roles'));
}

/**
* Store a newly created resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @return \Illuminate\Http\Response
*/
public function store(Request $request)
{
    if(!Entrust::can('create-user')) { abort(403); }
    $validation = Validator::make($request->all(), [
        'name' => 'required',
        'email' => 'required|email|unique:users',
        'password' => 'required|between:5,15',
        'msisdn' => 'required|between:10,25',
        'roles' => 'required',

        ]);
//print_r($request->all());exit;
    if($validation->fails()) {
        return Redirect::back()->withErrors($validation)->withInput();
    }

    $user = new User();
    $user->fill($request->except('roles','password'));
    $user->password = bcrypt($request->password);
    $user->created_by = auth()->user()->id;
    $user->updated_by = auth()->user()->id;
// $user->user_type_id = \App\UserType::whereTitle("Guest")->first()->id;
//        $user->user_type_id = \App\Role::whereName("guest")->first()->id;
//        $url = 'uploads/users/';
    $user->user_type_id=$request->all()["roles"];

    $user->api_token = str_random(60);
    $user->save();

// foreach ($request->input('roles') as $key => $value) {
    $user->attachRole($request->input('roles'));
// }

    Session::flash('message', "Basic information saved successfully");
    return redirect()->route('user.index');



}

/**
* Display the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function show($id)
{
    if(!Entrust::can('view-user')) { abort(403); }
    $user = User::select(array(
        'users.id',
        'users.name',
        'users.email',
        'users.msisdn',
        'users.status',
        'roles.name as roleName',
        'roles.display_name',
        ))
    ->leftJoin('role_user', 'role_user.user_id', '=', 'users.id')
    ->leftJoin('roles', 'roles.id', '=', 'role_user.role_id')
    ->where('users.id', '=', $id)
    ->first();



    return view('users.view', compact('user'));
}

/**
* Show the form for editing the specified resource.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function edit(Request $request, $id)
{
    if(!Entrust::can('edit-user')) { abort(403); }
    $user = User::where('id', '=', $id)->first();
    $roles = Role::pluck('display_name', 'id')->toArray();
    $role_type = Role::where('id', '=', $user->user_type_id)->pluck('id');
//print_r($user);exit;
    $role_type =$role_type[0];

    return view('users.edit', compact('roles','user','role_type'));
}

/**
* Update the specified resource in storage.
*
* @param  \Illuminate\Http\Request  $request
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function update(Request $request, $id)
{
    if(!Entrust::can('edit-user')) { abort(403); }

    $validation = Validator::make($request->all(), [
        'name' => 'sometimes',
        'email' => 'sometimes|email|unique:users,email,'.$id,
        'msisdn' => 'sometimes|between:10,25',
        'user_type_id' => 'sometimes',

        ]);

    if($validation->fails()) {
        return Redirect::back()->withErrors($validation)->withInput();
    }

    $user = User::with('roles')->find($id);
    $roles = $user->roles->first()->toArray();
    //print_r($roles);exit;
    $rid=$roles["id"];
    $user = User::findOrFail($id);
    $user->user_type_id=$request->all()["roles"];
    $user->fill($request->except('roles'));
    $user->updated_by = auth()->user()->id;

    $user->save();

    $user->roles()->detach($rid);
    $user->attachRole($request->input('roles'));


    Session::flash('message', "Basic information Update successfully");
    return redirect()->route('user.index');


}

/**
* Remove the specified resource from storage.
*
* @param  int  $id
* @return \Illuminate\Http\Response
*/
public function destroy($id)
{
//
}

public function userList(){
    if(!Entrust::can('view-user')) { abort(403); }
    $query = User::select(array(
        'users.id',
        'users.photo',
        'users.name',
        'users.email',
        'users.msisdn',
        'users.alt_msisdn',
        'roles.display_name',
        'users.status',
        ));

    $query->leftJoin('roles', 'roles.id', '=', 'users.user_type_id');

    if (Auth::user()->hasRole('merchantadmin')) {
        $query->where('users.reference_id', '=', auth()->user()->reference_id);
    }

    $query->where('users.user_type_id', '>', auth()->user()->user_type_id);
    $query->orderBy('users.id', 'desc');

    $requesters = $query->get();

    return Datatables::of($requesters)
    ->remove_column('id')
    ->editColumn('photo',
        '<img class="table-thumb" class="img-circle" src="{{ $photo }}" alt="">'
        )
    ->editColumn('status', '@if($status=="1")
        <span class="label label-success"> Active </span>
        @else
        <span class="label label-danger"> Inactive </span>
        @endif')
    ->add_column('action',
        '<div class="btn-group pull-right">
        <button class="btn green btn-xs btn-outline dropdown-toggle" data-toggle="dropdown">Tools
            <i class="fa fa-angle-down"></i>
        </button>
        <ul class="dropdown-menu pull-right">
            <li>
                <a href="user/{{ $id }}">
                    <i class="fa fa-file-o"></i> View </a>
                </li>
                <li>
                    <a href="user/{{ $id }}/edit">
                        <i class="fa fa-pencil"></i> Update </a>
                    </li>
                </ul>
            </div>'
            )
    ->make();

}

public function storeUserList(){
    if(!Entrust::can('view-user')) { abort(403); }
    $query = User::select(array(
        'users.id',
        'users.photo',
        'users.name',
        'users.email',
        'users.msisdn',
        'users.alt_msisdn',
        'roles.display_name',
        'users.status',
        ));

    $query->leftJoin('roles', 'roles.id', '=', 'users.user_type_id');
    $query->leftJoin('stores', 'stores.id', '=', 'users.reference_id');
    $query->where('stores.merchant_id', '=', auth()->user()->reference_id);
    $query->where('users.user_type_id', '=', '12');

    $query->orderBy('users.id', 'desc');

    $requesters = $query->get();

    return Datatables::of($requesters)
    ->remove_column('id')
    ->editColumn('photo',
        '<img class="table-thumb" class="img-circle" src="{{ $photo }}" alt="">'
        )
    ->editColumn('status', '@if($status=="1")
        <span class="label label-success"> Active </span>
        @else
        <span class="label label-danger"> Inactive </span>
        @endif')
    ->add_column('action',
        '<div class="btn-group pull-right">
        <button class="btn green btn-xs btn-outline dropdown-toggle" data-toggle="dropdown">Tools
            <i class="fa fa-angle-down"></i>
        </button>
        <ul class="dropdown-menu pull-right">
            <li>
                <a href="user/{{ $id }}">
                    <i class="fa fa-file-o"></i> View </a>
                </li>
                <li>
                    <a href="user/{{ $id }}/edit">
                        <i class="fa fa-pencil"></i> Update </a>
                    </li>
                </ul>
            </div>'
            )
    ->make();

}

public function references($user_type_id){

// $user_type_title = UserType::whereStatus(true)->where('id', '=', $user_type_id)->first()->title;
    $user_type_title = Role::where('id', '=', $user_type_id)->first()->name;

    switch ($user_type_title) {
        case "hubmanager":
        case "vehiclemanager":
        case "delivery-pickupman":
        $reference_list = Hub::whereStatus(true)->addSelect('title AS title', 'id')->get();
        break;
        case "merchantadmin":
        case "merchantaccounts":
        case "merchantsupport":
        $query = Merchant::whereStatus(true)->addSelect('name AS title', 'id');
        if (Auth::user()->hasRole('merchantadmin')) {
            $query->where('id', '=', auth()->user()->reference_id);
        }
        $reference_list = $query->get();
        break;
        case "storeadmin":
        $query = Store::where('stores.status', '=', '1')
        ->select(array(
            'stores.id AS id',
            DB::raw("CONCAT(merchants.name,' - ',stores.store_id) AS title")
            ))
        ->leftJoin('merchants', 'merchants.id', '=', 'stores.merchant_id');
            // ->addSelect('store_id AS title', 'id')
        if (Auth::user()->hasRole('merchantadmin')) {
            $query->where('stores.merchant_id', '=', auth()->user()->reference_id);
        }
        $reference_list = $query->get();
        break;
        default:
        $reference_list = null;
    }

    return $_GET['callback']."(".json_encode($reference_list).")";
}
}
