<?php

namespace App\Http\Controllers\DefaultUser;

use App\Http\Controllers\Controller;

class DashboardController extends Controller {

  private $view_path = "core.default_user.";

  public function dashboard()
  {
    return view($this->view_path.'dashboard');
  }
}