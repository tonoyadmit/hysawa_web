<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class DashboardController extends Controller {

  private $view_path = "core.admin.";

  public function dashboard()
  {
    return view($this->view_path.'dashboard');
  }
}