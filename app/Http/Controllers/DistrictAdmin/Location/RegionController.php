<?php

namespace App\Http\Controllers\DistrictAdmin\Location;

use App\Http\Controllers\Controller;
use App\Model\Project;
use App\Model\Region;
use App\Model\Search\LocationUnionSearch;
use App\Model\Union;
use Illuminate\Http\Request;

class RegionController extends Controller {

  private $view_path = "core.district.location.region.";
  private $route_path = "district-admin.location.region.";

  public function index(Request $request)
  {
    $regions = Region::paginate(15);
    return view($this->view_path.'index', compact('regions'));
  }

  public function create(Request $request)
  {
    return view($this->view_path.'create');
  }

  public function store(Request $request)
  {
    try{
      $p = new Region;
      $p->region_name = $request->input('region_name');
      $p->region_id = time();
      $p->save();
    }catch(\Exception $e){
      \Log::critical($e->getMessage());
      \Session::flash('error', "Something went wrong.");
      return redirect()->back();
    }
    \Session::flash('message', "Region Added.");
    return redirect()->route($this->route_path.'index');
  }

  public function edit(Request $request, $id)
  {
    try{
      $region = Region::findOrFail($id);
      return view($this->view_path.'edit', compact('region'));
    }catch(\Exception $e){
      \Log::critical($e->getMessage());
      \Session::flash('error', "Something went wrong.");
      return redirect()->back();
    }
  }

  public function update(Request $request, $id)
  {

    try{
      $region = Region::findOrFail($id);
      $region->region_name = $request->input('region_name');
      $region->update();
    }catch(\Exception $e){
      \Log::critical($e->getMessage());
      \Session::flash('error', "Something went wrong.");
      return redirect()->back();
    }

    \Session::flash('message', "Region Updated.");
    return redirect()->route($this->route_path.'index');
  }


}