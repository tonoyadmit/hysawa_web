<?php

namespace App\Http\Controllers\DistrictAdmin\Location;

use App\Http\Controllers\Controller;
use App\Model\Search\LocationUpazilaSearch;
use App\Model\Upazila;
use Illuminate\Http\Request;

class UpazilaController extends Controller {

  private $view_path = "core.district.location.upazila.";
  private $route_path = "district-admin.location.upazila.";

  public function index(Request $request)
  {
    $old = [
      'region_id'    => '',
      'district_id'  => '',
      'upname'       => ''
    ];

    if($request->has('query'))
    {
      $old = [
        'query'       => $request->input('query'),
        'region_id'   => $request->input('region_id'),
        'upazila_id'  => $request->input('upazila_id'),
        'upname'      => $request->input('upname')
      ];
    }
    $datas = (new LocationUpazilaSearch($request, true))->get();
    return view($this->view_path.'index', compact('datas', 'old'));
  }

  public function create(Request $request)
  {
    return view($this->view_path.'create');
  }

  public function store(Request $request)
  {
    try{
      $upazila = new Upazila;
      $upazila->upname = $request->input('upname');
      $upazila->disid  = $request->input('district_id');
      $upazila->upcode = $request->input('upcode');
      $upazila->save();
      \Session::flash('message', 'Upazila Added');
      return redirect()->route($this->route_path.'index');

    }catch(\Exception $e)
    {
      \Log::critical($e->getMessage());
      \Session::flash('error', 'Something went wrong.');
      return redirect()->back();
    }
  }

  public function edit(Request $request, $id)
  {
    try{
      $upazila = Upazila::with('district.region')->findOrFail($id);
      return view($this->view_path.'edit', compact('upazila'));
    }catch(\Exception $e)
    {
      \Log::critical($e->getMessage());
      \Session::flash('error', 'Something went wrong.');
      return redirect()->back();
    }
  }

  public function update(Request $request, $id)
  {
    try{
      $upazila = Upazila::findOrFail($id);

      $upazila->upname = $request->input('upname');
      $upazila->disid  = $request->input('district_id');
      $upazila->upcode = $request->input('upcode');

      $upazila->update();
      \Session::flash('message', 'Upazila Info updated');
      return redirect()->route($this->route_path.'index');

    }catch(\Exception $e)
    {
      \Log::critical($e->getMessage());
      \Session::flash('error', 'Something went wrong.');
      return redirect()->back();
    }

  }
}