<?php

namespace App\Http\Controllers\DistrictAdmin\Location;

use App\Http\Controllers\Controller;
use App\Model\Project;
use App\Model\Search\LocationUnionSearch;
use App\Model\Union;
use Illuminate\Http\Request;

class ProjectController extends Controller {

  private $view_path = "core.district.location.project.";
  private $route_path = "district-admin.location.project.";

  public function index(Request $request)
  {
    $projects = Project::paginate(15);
    return view($this->view_path.'index', compact('projects'));
  }

  public function create(Request $request)
  {
    return view($this->view_path.'create');
  }

  public function store(Request $request)
  {
    try{

      $p = new Project;
      $p->project = $request->input('project');
      $p->save();

      \Session::flash('success', "Project Added.");
      return redirect()->route($this->route_path.'index');

    }catch(\Exception $e)
    {
      \Log::critical($e->getMessage());
      \Session::flash('error', "Something went wrong.");
      return redirect()->back();
    }

  }

  public function edit(Request $request, $id)
  {
    try{
      $project = Project::findOrFail($id);
      return view($this->view_path.'edit', compact('project'));

    }catch(\Exception $e)
    {
      \Log::critical($e->getMessage());
      \Session::flash('error', "Project Not Found.");
      return redirect()->back();
    }
  }

  public function update(Request $request, $id)
  {
    try{
      $project = Project::findOrFail($id);

      $project->project = $request->input('project');
      $project->update();

      \Session::flash('success', "Project Updated.");
      return redirect()->route($this->route_path.'index');

    }catch(\Exception $e)
    {
      \Log::critical($e->getMessage());
      \Session::flash('error', "Project Not Found.");
      return redirect()->back();
    }
  }

}