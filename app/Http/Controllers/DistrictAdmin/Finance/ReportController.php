<?php

namespace App\Http\Controllers\DistrictAdmin\Finance;

use App\Fhead;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Model\FinanceReportData;
use App\Model\Union;
use App\Model\Upazila;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    private $view_path = "core.district.finance.";
    private $route_path = "";

    public function index( Request $request )
    {
      $old = [
        'region_id' => '',
        'starting_date' => '',
        'ending_date' => '',
        'district_id' => ''
      ];

      if($request->has('query'))
      {
        $old = [
          'region_id'     => \Auth::user()->region_id,
          'starting_date'  => $request->input('starting_date'),
          'ending_date'    => $request->input('ending_date'),
          'district_id' => $request->input('district_id'),
          'query'          => $request->input('query')
        ];

        $d = Fhead::with('subhead.subItem')->get()->toArray();

        $type = "region_id";
        $value = auth()->user()->region_id;


        if(isset($request->union_id) && $request->union_id == "all")
        {
          $type = "unid";
          $value = $request->upazila_id;
        }elseif(isset($request->upazila_id) && $request->upazila_id == "all")
        {
          $type = "distid";
          $value = $request->district_id;
        }elseif(isset($request->district_id) && $request->district_id == "all")
        {
          $type = "region_id";
          $value = auth()->user()->region_id;
        }elseif(isset($request->union_id) && is_numeric($request->union_id))
        {
          $type = "unid";
          $value = $request->union_id;
        }elseif(isset($request->upazila_id) && is_numeric($request->upazila_id ))
        {
          $type = "upid";
          $value = $request->upazila_id;
        }elseif(isset($request->district_id) && is_numeric($request->district_id))
        {
          $type = "distid";
          $value = $request->district_id;
        }else{
          \Session::flash('error', 'Invalid Request. Please choose correctly.');
          return redirect()->back();
        }

        // if (union == all) // get upazila report
        // if ($upazila == all) // get district report
        // if (district == all ) // get region report

        // if (union == nemeric && not_empty()) // get union report
        // if (upazila == nemeric && not_empty()) // get upazila report
        // if (district == nemeric && not_empty()) // get district report

        $data  = FinanceReportData::getData($type,  $value, $request->input('starting_date'));
        $data2 = FinanceReportData::getData2($type, $value, $request->input('ending_date'));

        if(count($data)){
           $data= $data[0];
        }

        if(count($data2)){
           $data2= $data2[0];
        }


      }
      return view($this->view_path.'index', compact('old', 'data', 'd','data2'));
    }

    public function print(Request $request)
    {
        $old = [
          'region_id'      => \Auth::user()->region_id,
          'starting_date'  => $request->input('starting_date'),
          'ending_date'    => $request->input('ending_date'),
          'district_id'    => $request->input('district_id'),
          'query'          => $request->input('query')
        ];

        $d=Fhead::with('subhead.subItem')->get()->toArray();

        $type = "region_id";
        $value = auth()->user()->region_id;

        if(isset($request->union_id) && $request->union_id == "all")
        {
          $type = "unid";
          $value = $request->upazila_id;
        }elseif(isset($request->upazila_id) && $request->upazila_id == "all")
        {
          $type = "distid";
          $value = $request->district_id;
        }elseif(isset($request->district_id) && $request->district_id == "all")
        {
          $type = "region_id";
          $value = auth()->user()->region_id;
        }elseif(isset($request->union_id) && is_numeric($request->union_id))
        {
          $type = "unid";
          $value = $request->union_id;
        }elseif(isset($request->upazila_id) && is_numeric($request->upazila_id ))
        {
          $type = "upid";
          $value = $request->upazila_id;
        }elseif(isset($request->district_id) && is_numeric($request->district_id))
        {
          $type = "distid";
          $value = $request->district_id;
        }else{
          \Session::flash('error', 'Invalid Request. Please choose correctly.');
          return redirect()->back();
        }

        $data  = FinanceReportData::getData($type,  $value, $request->input('starting_date'));
        $data2 = FinanceReportData::getData2($type, $value, $request->input('ending_date'));

        if(count($data)){
           $data= $data[0];
        }
        if(count($data2)){
           $data2= $data2[0];
        }

      return view($this->view_path.'print', compact('old', 'data', 'd', 'data2'));
    }
}
