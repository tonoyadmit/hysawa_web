<?php

namespace App\Http\Controllers\DistrictAdmin\Finance\UpDemandM;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Item;
use App\User;
use Datatables;
use App\Role;
use App\Fhead;
use App\Fsubhead;
use App\Fitemlist;
use App\FinanceData;
use App\FinanceDemand;
use App\FundList;
use Validator;
use Session;
use Redirect;
use DB;
use Auth;
use Entrust;

class DemandMController extends Controller
{
  private $view_path = "core.district.finance.up-demand-m.demand.";
  private $route_path = "district-admin.up-demand-m.survey_details.";

  public function index( Request $request )
  {
   // return select * from `demand` where `unid` = auth()->user()->unid and `region_id` is null and `proj_id` = ? and `fundlistid` = ? order by `id` desc
  

   
    // return $request->input('fid');
    $FinanceDemands = \DB::select(\DB::Raw("select * from mob_survey where id = ".$request->input('id').""));
    $TubDetails = \DB::select(\DB::Raw("select * from  tbl_water where id = ".$request->input('tubid').""));
    return view($this->view_path.'index', compact('FinanceDemands','TubDetails'));
  }

  public function create(Request $request)
  {
    $items = Item::with('head')->groupBy('headid')->get();
    $head = [];
    foreach($items as $item){
      $head[$item->head->id] = $item->head->headname;
    }
    return view($this->view_path.'insert', compact('head'));
  }

  public function store(Request $request)
  {
    $validation = Validator::make($request->all(), [

      'head' => 'required',
      'subhead' => 'required',
      'item' => 'required',
      'amount' => 'required'
      ]);

    if($validation->fails()) {
      return Redirect::back()->withErrors($validation)->withInput();
    }


    $FinanceData = new FinanceDemand();
    $FinanceData->fill($request->except('_token'));
    $FinanceData->unid = auth()->user()->unid;
    $FinanceData->userid=auth()->user()->id;
    $FinanceData->created_by = auth()->user()->id;
    $FinanceData->updated_by = auth()->user()->id;

    $FinanceData->distid = auth()->user()->distid;
    $FinanceData->upid = auth()->user()->upid;
    $FinanceData->region_id = auth()->user()->region_id;
    $FinanceData->proj_id = auth()->user()->proj_id;
    $FinanceData->fundlistid =   $request->input('fid');
    
    $FinanceData->save();


      // return $request->input('fid');
 
    Session::flash('message', "New Data saved successfully");
    return redirect()->route($this->route_path.'index','fid='.$request->input('fid'));
  }

  public function edit($id)
  {
    $head = Fhead::pluck('headname','id')->toArray();
    $FinanceData = FinanceDemand::findOrFail($id);
    $subhead = Fsubhead::where('headid', '=',$FinanceData["head"])->pluck('sname','id')->toArray();
    $subitem = Fitemlist::where('headid', '=',$FinanceData["head"])->where('subid', '=',$FinanceData["subhead"])->pluck('itemname','id')->toArray();

    return view($this->view_path.'edit', compact('FinanceData','head','subhead','subitem'));;
  }

  public function update(Request $request, $id)
  {
    $validation = Validator::make($request->all(), [
      'head'          => 'required',
      'subhead'  => 'required',
      'item'           => 'required',
      'amount'          => 'required'
      ]);

    if($validation->fails()) {
      return Redirect::back()->withErrors($validation)->withInput();
    }

    $FinanceData = FinanceDemand::findOrFail($id);
    $FinanceData->fill($request->except('_token'));
    $FinanceData->updated_by = auth()->user()->id;
    $FinanceData->save();

    \Session::flash('message', "One updated successfully");
    return redirect()->route($this->route_path.'index','fid='.$FinanceData->fundlistid);
  }

  public function l(Request $request)
  {
    $requestData = $request->all();
    return $subhead = Fsubhead::where('headid', '=',$requestData["head"])->pluck('sname','id')->toArray();
  }

  public function subitem(Request $request)
  {
    $requestData = $request->all();
    return $subhead = Fitemlist::where('headid', '=',$requestData["head"])->where('subid', '=',$requestData["subhead"])->pluck('itemname','id')->toArray();
  }

  public function print( Request $request )
  {

    $FinanceDemands = FinanceDemand::with('getHead', 'getSubhead', 'getItem')
    ->where('fundlistid', '=', $request->input('fid'))
    ->orderBy('id', 'desc')
    ->get();

   
   // $fdate = FundList::where('id','=',$request->input('fundlistid'))->get();

     $fdate = FundList::where('id','=',$request->input('fid'))->get();
    

   return view($this->view_path.'print', compact('FinanceDemands','fdate'));
    //return view($this->view_path.'print')->with('FinanceDemands',(new FinanceDemand($request))->get());
 
  }


}
