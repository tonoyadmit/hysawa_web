<?php

namespace App\Http\Controllers\DistrictAdmin\SubProject;

use App\Http\Controllers\Controller;

use App\Model\Download\Superadmin\Project\Water\WaterApprovalDownload;
use App\Model\Download\Superadmin\Project\Water\WaterDistrictDownload;
use App\Model\Download\Superadmin\Project\Water\WaterRegionDownload;
use App\Model\Download\Superadmin\Project\Water\WaterUnionDownload;

use App\Model\HouseSanitationSummary2;

use Illuminate\Http\Request;

class HouseSanitationController extends Controller
{
  private $view_path = "core.district.sub-projects.house_sanitation.";
  private $route_path = "district-admin.sub-projects.house_sanitation.";

  public function index( Request $request )
  {
    // $regionSummary   = HouseSanitationSummary2::regionSummary();
    // $districtSummary = HouseSanitationSummary2::districtSummary();
    // $unionSummary    = HouseSanitationSummary2::unionSummary();
    // $summary         = HouseSanitationSummary2::approvalSummary();

    if($request->has('query') )
    {

      $type = $this->getType($request);

      \Log::info($type);

      if($request->has('output') && $request->output == "download")
      {
        if($type == "region"){
          return (new WaterRegionDownload)->download();
        }elseif($type == "singleRegion"){
          return (new WaterUnionDownload)->download('region_id', $request->input('region_id'));
        }elseif($type == "district"){
          return (new WaterDistrictDownload)->download();
        }elseif($type == "singleDistrict"){
          return (new WaterUnionDownload)->download('dist_id', $request->input('district_id'));
        }elseif($type == "upazila"){
          return (new WaterUnionDownload)->download('upid', $request->input('upazila_id'));
        }elseif($type == "union"){
          return (new WaterUnionDownload)->download('unid', $request->input('union_id'));
        }

      }else{
        if($type == 'region'){
          $regionSummary   = HouseSanitationSummary2::regionSummary();

          if($request->output == "print"){
            return view($this->view_path.'print', compact('regionSummary'))->with('type', 'region');
          }else{
            return view($this->view_path.'region', compact('regionSummary'));
          }

        }elseif($type == "district"){
          $districtSummary = HouseSanitationSummary2::districtSummary();

          if($request->output == "print"){
            return view($this->view_path.'print', compact('districtSummary'))->with('type', 'district');
          }else{
            return view($this->view_path.'district', compact('districtSummary'));
          }

        }elseif($type == "singleRegion"){

          $unionSummary    = \DB::select(\DB::Raw($this->getQuery('region_id', $request->input('region_id'))));

          if($request->output == "print"){
            return view($this->view_path.'print', compact('unionSummary'))->with('type', 'union');
          }else{
            return view($this->view_path.'union', compact('unionSummary'));
          }

        }elseif($type == "singleDistrict"){
          $unionSummary    = \DB::select(\DB::Raw($this->getQuery('dist_id', $request->input('district_id'))));

          if($request->output == "print"){
            return view($this->view_path.'print', compact('unionSummary'))->with('type', 'union');
          }else{
            return view($this->view_path.'union', compact('unionSummary'));
          }

        }elseif($type == "upazila"){

          $unionSummary    = \DB::select(\DB::Raw($this->getQuery('upid', $request->input('upazila_id'))));

          if($request->output == "print"){
            return view($this->view_path.'print', compact('unionSummary'))->with('type', 'union');
          }else{
            return view($this->view_path.'union', compact('unionSummary'));
          }

        }elseif($type == "union"){

          $unionSummary    = \DB::select(\DB::Raw($this->getQuery('unid', $request->input('union_id'))));
          if($request->output == "print"){
            return view($this->view_path.'print', compact('unionSummary'))->with('type', 'union');
          }else{
            return view($this->view_path.'union', compact('unionSummary'));
          }

        }
      }
    }

    return view($this->view_path.'index'); //, compact('regionSummary', 'districtSummary', 'unionSummary', 'summary'));
  }

  private function getType(Request $request)
  {
    if($request->has('region_id') && $request->region_id == "all") return "region";
    if($request->has('district_id') && $request->district_id == "all") return "district";

    if($request->has('union_id') && $request->union_id != "" && $request->union_id != "all") return "union";
    if($request->has('upazila_id') && $request->upazila_id != "" && $request->upazila_id != "all") return "upazila";
    if($request->has('district_id') && $request->district_id != "" && $request->district_id != "all") return "singleDistrict";



    //if($request->has('upazila_id') && $request->upazila_id != "") return "upazila";
   // if($request->has('district_id') && $request->district_id != "") return "singleDistrict";
    //if($request->has('union_id') && $request->upazila_id != "all") return "union";

    if($request->has('region_id') && $request->region_id != "") return "singleRegion";

  }


  private function getQuery($type = null, $value = null)
  {
    if($type == null && $value)
    {
      return "
        SELECT
            fdistrict.distname,
            fupazila.upname,
            funion.unname,
            sanitation.unid,
            sanitation.app_date,

  
            COUNT(sanitation.id) AS 'subappcount',
            sum(IF(app_status = 'Approved',1,0)) AS 'sumappcount',
            sum(IF(app_status = 'Submitted',1, 0)) AS 'Submitted',
            sum(IF(app_status = 'Recomended',1,0)) AS 'Recomended',
            sum(IF(app_status = 'Cancelled',1,0)) AS 'Cancelled',
            sum(IF(app_status = 'Rejected',1,0)) AS 'Rejected',

            sum(IF(ownership_type LIKE '%Personal%' ,1, 0)) AS 'Personal',
            sum(IF(ownership_type LIKE '%Joint%' ,1, 0)) AS 'Joint',
            sum(IF(ownership_type LIKE '%Other%' ,1, 0)) AS 'Other',
     
            sum(IF(imp_status = 'Completed', 1, 0)) AS 'Completed',
            sum(IF(latitude != '', 1, 0)) AS 'latitude'

            FROM

            sanitation

            INNER JOIN funion ON funion.id = sanitation.unid
            INNER JOIN fupazila ON fupazila.id = funion.upid
            INNER JOIN fdistrict ON fdistrict.id = fupazila.disid

            GROUP BY
            sanitation.unid,
            sanitation.app_date";
    }

    return "
      SELECT

        fdistrict.distname,
        fupazila.upname,
        funion.unname,
        sanitation.unid,
        sanitation.app_date,

        COUNT(sanitation.id) AS 'subappcount',
        sum(IF(app_status = 'Approved',1,0)) AS 'sumappcount',
        sum(IF(app_status = 'Submitted',1, 0)) AS 'Submitted',
        sum(IF(app_status = 'Recomended',1,0)) AS 'Recomended',
        sum(IF(app_status = 'Cancelled',1,0)) AS 'Cancelled',
        sum(IF(app_status = 'Rejected',1,0)) AS 'Rejected',

        sum(IF(ownership_type LIKE '%Personal%' ,1, 0)) AS 'Personal',
        sum(IF(ownership_type LIKE '%Joint%' ,1, 0)) AS 'Joint',
        sum(IF(ownership_type LIKE '%Other%' ,1, 0)) AS 'Other',
 
        sum(IF(imp_status = 'Completed', 1, 0)) AS 'Completed',
        sum(IF(latitude != '', 1, 0)) AS 'latitude'

        FROM

        sanitation

        INNER JOIN funion ON funion.id = sanitation.unid
        INNER JOIN fupazila ON fupazila.id = funion.upid
        INNER JOIN fdistrict ON fdistrict.id = fupazila.disid

        WHERE sanitation.$type = $value

        GROUP BY
        sanitation.unid,
        sanitation.app_date
    ";


  }

  public function regions()
  {
    $regionSummary   = HouseSanitationSummary2::regionSummary();
    return view($this->view_path.'region', compact('regionSummary'));
  }

  public function districts()
  {
    $districtSummary = HouseSanitationSummary2::districtSummary();
    return view($this->view_path.'district', compact('districtSummary'));
  }

  public function unions()
  {
    $unionSummary    = HouseSanitationSummary2::unionSummary();
    return view($this->view_path.'union', compact('unionSummary'));
  }

  public function approval(Request $request)
  {
    //$summary         = HouseSanitationSummary2::approvalSummary();

    $datas = "";

    if($request->has('query') )
    {
      $type = $this->getType($request);

      if($_REQUEST['approval_date'] != "" && ($_REQUEST['region_id'] == "all" && $_REQUEST['district_id'] == "" && $_REQUEST['upazila_id'] == "" && $_REQUEST['union_id'] == ""))
      {
        //dd("here");

        $unionSummary    = HouseSanitationSummary2::approvalUnionSummary( $request->input('approval_date'));

        if($request->output == "print"){
          return view($this->view_path.'approval.print', compact('unionSummary'))->with('type', 'union');
        }else{
          return view($this->view_path.'approval.union', compact('unionSummary'));
        }
      }elseif($type == 'region'){
        $regionSummary   = HouseSanitationSummary2::approvalRegionSummary($request->input('approval_date'));

        if($request->output == "print"){
          return view($this->view_path.'approval.print', compact('regionSummary'))->with('type', 'region');
        }else{
          return view($this->view_path.'approval.region', compact('regionSummary'));
        }

      }elseif($type == "district"){
        $districtSummary = HouseSanitationSummary2::approvalDistrictSummary($request->input('approval_date'));

        if($request->output == "print"){
          return view($this->view_path.'approval.print', compact('districtSummary'))->with('type', 'district');
        }else{
          return view($this->view_path.'approval.district', compact('districtSummary'));
        }

      }elseif($type == "singleRegion"){

        $unionSummary    = HouseSanitationSummary2::approvalUnionSummary( $request->input('approval_date'), 'region_id', $request->input('region_id')); 

        if($request->output == "print"){
          return view($this->view_path.'approval.print', compact('unionSummary'))->with('type', 'union');
        }else{
          return view($this->view_path.'approval.union', compact('unionSummary'));
        }

      }elseif($type == "singleDistrict"){
        $unionSummary    = HouseSanitationSummary2::approvalUnionSummary( $request->input('approval_date'), 'dist_id', $request->input('district_id')); 

        //$unionSummary    = \DB::select(\DB::Raw($this->getQuery('dist_id', $request->input('district_id'))));

        if($request->output == "print"){
          return view($this->view_path.'approval.print', compact('unionSummary'))->with('type', 'union');
        }else{
          return view($this->view_path.'approval.union', compact('unionSummary'));
        }

      }elseif($type == "upazila"){

        //$unionSummary    = \DB::select(\DB::Raw($this->getQuery('upid', $request->input('upazila_id'))));

        $unionSummary    = HouseSanitationSummary2::approvalUnionSummary( $request->input('approval_date'), 'upid', $request->input('upazila_id'));

        if($request->output == "print"){
          return view($this->view_path.'approval.print', compact('unionSummary'))->with('type', 'union');
        }else{
          return view($this->view_path.'approval.union', compact('unionSummary'));
        }

      }elseif($type == "union"){

        //$unionSummary    = \DB::select(\DB::Raw($this->getQuery('unid', $request->input('union_id'))));

        $unionSummary    = HouseSanitationSummary2::approvalUnionSummary( $request->input('approval_date'), 'unid', $request->input('union_id'));

        if($request->output == "print"){
          return view($this->view_path.'approval.print', compact('unionSummary'))->with('type', 'union');
        }else{
          return view($this->view_path.'approval.union', compact('unionSummary'));
        }


      }
    }

    return view($this->view_path.'approval.index', compact('datas'));
  }

  public function download(Request $request, $type)
  {
    if($type == "region"){
      return (new WaterRegionDownload)->download();
    }elseif($type == "district"){
      return (new WaterDistrictDownload)->download();
    }elseif($type == "union"){
      return (new WaterUnionDownload)->download();
    }elseif($type == "approval"){
      return (new WaterApprovalDownload)->download();
    }
  }

}
