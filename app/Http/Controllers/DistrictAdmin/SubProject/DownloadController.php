<?php

namespace App\Http\Controllers\DistrictAdmin\SubProject;

use App\Http\Controllers\Controller;
use App\Model\Download\Superadmin\Project\Sanitation\SanitationDownload;
use App\Model\Download\Superadmin\Project\Sanitation\SummaryDownload;
use Illuminate\Http\Request;

class DownloadController extends Controller
{
  public function index($type)
  {
    if($type == "list"){
      return (new SanitationDownload)->download();
    }elseif($type == "summary"){
      return (new SummaryDownload)->download();
    }
  }
}