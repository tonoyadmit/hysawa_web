<?php

namespace App\Http\Controllers\DistrictAdmin\WaterHousehold;

use App\AppStatus;
use App\Http\Controllers\Controller;
use App\ImplementStatus;
use App\Model\Download\Household2Download;
use App\Model\Download\HouseholdDownload;
use App\Model\Search\Model\Household as ModelSanitation;
use App\Model\Search\Request\SanitationHouseholdRequest;
use App\Model\Search\Household2Search;
use App\Model\Search\SanitationHousehold;
use App\WaterHousehold;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class WaterHouseholdController extends Controller
{
  private $view_path = "core.district.household.";
  private $route_path = "";

  public function index(Request $request )
  {
    $old = [
      'starting_date' => '',
      'ending_date' => '',
      'created_by' => '',
      'village' => '',
      'app_status' => '',
      'imp_status' => '',
      'district_id' => '',
      'upazila_id' => '',
      'union_id' => ''
    ];

    if($request->has('query'))
    {
      $old = [
        'query'         => $request->input('query'),
        'starting_date' => $request->input('starting_date'),
        'ending_date'   => $request->input('ending_date'),
        'created_by'    => $request->input('created_by'),
        'village'       => $request->input('village'),
        'app_status'    => $request->input('app_status'),
        'imp_status'    => $request->input('imp_status'),
        'district_id'   => $request->input('district_id'),
        'upazila_id'    => $request->input('upazila_id'),
        'union_id'      => $request->input('union_id'),
      ];
    }

    $request->request->add(['region_id' => \Auth::user()->region_id]);

    if($request->has('district_id') && $request->district_id == "all"){
      $request->request->add(['district_id' => '', 'upazila_id' => '', 'union_id' => '']);
    }elseif($request->has('upazila_id') && $request->upazila_id == "all"){
      $request->request->add(['upazila_id' => '', 'union_id' => '']);
    }elseif($request->has('union_id') && $request->union_id == "all"){
      $request->request->add(['union_id' => '']);
    }


    if($request->has('print'))
    {
      $sanitations = (new Household2Search($request))->get();
      if(!count($sanitations)){
        \Session::flash('error', 'Data Not Found.');
        return redirect()->back();
        //return response()->json(['status' => 'error', 'message' => 'Data Not Found!']);
      }
      return view($this->view_path.'print', compact('sanitations', 'old'));
    }

    if($request->has('download'))
    {
      $v = (new Household2Search($request))->get();
      if(!count($v))
      {
        return response()->json(['status' => 'error', 'message' => 'Data Not Found!']);
      }
      return (new Household2Download())->download($v);
    }

    $sanitations = (new Household2Search($request, true))->get();
    return view($this->view_path.'index', compact('sanitations', 'old'));
  }

  public function create()
  {
    $mainype = array(
      'Institutional latrine' => 'Institutional latrine',
      'Public latrine' => 'Public latrine',
      'Communal latrine' => 'Communal latrine');

    $subtype = array(
      'Primary School' => 'Primary School',
      'High School' => 'High School',
      'Bazar' => 'Bazar',
      'Madrasha' => 'Madrasha',
      'Mosque' => 'Mosque',
      'Slum' => 'Slum',
      'Community' => 'Community');

    $wordOrderNO = array('1' => '1','2' => '2','3' => '3','4' => '4');
    $appstatus = AppStatus::pluck('app_status','id')->toArray();
    $ImplementStatus=ImplementStatus::pluck('imp_status','id')->toArray();

    return view($this->view_path.'insert', compact('subtype','appstatus','wordOrderNO','ImplementStatus','mainype'));
  }

  public function store(Request $request)
  {
    $water = new WaterHousehold();
    $water->fill($request->except('_token'));

    $water->unid = auth()->user()->unid;
    $water->region_id = auth()->user()->region_id;
    $water->proj_id = auth()->user()->proj_id;
    $water->dist_id = auth()->user()->distid;
    $water->upid = auth()->user()->upid;

    $water->created_by = auth()->user()->id;
    $water->updated_by = auth()->user()->id;
    $water->save();

    Session::flash('message', "New Data saved successfully");
    //return redirect('/water_household/create');
    return redirect()->route('district-admin.water_household.create');
  }

  public function show($id)
  {
    $water = WaterHousehold::findOrFail($id);
    return view($this->view_path.'view', compact('water'));
  }

  public function edit($id)
  {

    $mainype = array(
      'Institutional latrine' => 'Institutional latrine',
      'Public latrine' => 'Public latrine',
      'Communal latrine' => 'Communal latrine');

    $subtype = array(
      'Primary School' => 'Primary School',
      'High School' => 'High School',
      'Bazar' => 'Bazar',
      'Madrasha' => 'Madrasha',
      'Mosque' => 'Mosque',
      'Slum' => 'Slum',
      'Community' => 'Community');

    $wordOrderNO = array(
      '1' => '1',
      '2' => '2',
      '3' => '3',
      '4' => '4'
    );

    $appstatus = AppStatus::pluck('app_status','app_status')->toArray();
    $ImplementStatus=ImplementStatus::pluck('imp_status','imp_status')->toArray();

    $WaterHousehold = WaterHousehold::where('id', '=', $id)->first();
    return view($this->view_path.'edit', compact('subtype','appstatus','wordOrderNO','ImplementStatus','mainype','WaterHousehold'));
  }

  public function update(Request $request, $id)
  {

    $validator = Validator::make($request->all(),[
      'mobile'=>'required | max:11| min:11',
  ]);

  if($validator->fails()){
    \Session::flash('warning','This field is Required ! You Inserted An Empty field!');
    return redirect()->back()->withErrors($validator)->withInput();
}else {
    $water = WaterHousehold::findOrFail($id);
    $water->fill($request->except('_token'));
    $water->updated_by = auth()->user()->id;
    $water->save();

    \Session::flash('message', "One updated successfully");
    return redirect()->route('district-admin.water_household.index');
}
  }

  public function destroy($id)
  {

  }
}
