<?php

namespace App\Http\Controllers\DistrictAdmin\Water;

use App\Http\Controllers\Controller;
use App\ImplementStatus;
use App\Model\Download\Water2Download;
use App\Model\Download\WaterDownload;
use App\Model\Search\Request\WaterSearchRequest;
use App\Model\Search\Water2Search;
use App\Model\Search\WaterSearch;
use App\Water;
use App\WaterTech;
use Illuminate\Http\Request;
use DB;

class WaterController extends Controller
{
    private $view_path  = "core.district.water.";
    private $route_path = "district-admin.water.";

    public function index( Request $request )
    {
      $old = [
        'starting_date' => '',
        'ending_date' => '',
        'created_by' => '',
        'ward_no' => '',
        'village' => '',
        'district_id' => '',
        'upazila_id' => '',
        'union_id' => '',
        'app_status' => '',
        'imp_status' => ''
      ];

      if($request->has('query'))
      {
        $old = [
          'query' => $request->input('query'),
          'starting_date' => $request->input('starting_date'),
          'ending_date' => $request->input('ending_date'),
          'created_by' => $request->input('created_by'),
          'ward_no' => $request->input('ward_no'),
          'village' => $request->input('village'),

          'district_id' => $request->input('district_id'),
          'upazila_id' => $request->input('upazila_id'),
          'union_id' => $request->input('union_id'),
          'app_status' => $request->input('app_status'),
          'imp_status' => $request->input('imp_status')
        ];
      }

      $request->request->add(['region_id' => \Auth::user()->region_id]);

      if($request->has('print'))
      {
        $waters = (new Water2Search($request))->get();
        if(!count($waters))
        {
          return response()->json(['status' => 'error', 'message' => 'Data Not Found!']);
        }
        return view($this->view_path.'print', compact('waters', 'old'));
      }

      if($request->has('download'))
      {
        $v = (new Water2Search($request))->get();
        if(!count($v))
        {
          return response()->json(['status' => 'error', 'message' => 'Data Not Found!']);
        }
        return (new Water2Download($v))->download();
      }

      $waters = (new Water2Search($request, true))->get();

      return view($this->view_path.'index', compact('waters', 'old'));
    }

    public function edit($id)
    {
      $water = Water::where('region_id', \Auth::user()->region_id)->findOrFail($id);
      return view(
        $this->view_path.'edit',
        compact('water')
      );
    }

    public function update(Request $request, $id)
    {
      $water = Water::findOrFail($id);
      $water->fill($request->except('_token'));

      // if($water->created_at == null || $water->up_approval_by != "")
      // {
      //   \Session::flash('errorMessage', "Invalid Data to Update.");
      //   return redirect()->route($this->route_path.'index');
      // }

      $water->dist_approval_by = auth()->user()->id;
      $water->dist_approval_at = date('Y-m-d H:i:s');

      $water->update();

      \Session::flash('message', "Updated successfully");
      return redirect()->route($this->route_path.'index');
    }


  public function printdata( Request $request )
  {
    
      $old = [  
        'region_id' => '',
        'district_id' => '',
        'upazila_id' => '',
        'union_id' => ''
      ];

      if($request->has('query'))
      {
        $old = [
          'query' => $request->input('query'),
          'district_id' => $request->input('district_id'),
          'upazila_id' => $request->input('upazila_id'),
          'union_id' => $request->input('union_id')
        ];
      }

      // $water = (new WaterQualitySearch($request, true))->get();

      // return view($this->view_path.'printdata', compact('water', 'old'));


     // $water = Water::with('region', 'district', 'upazila', 'union', 'TW_No', 'CDF_no');
    //  $waters =  DB::select("select * from tbl_water where region_id ='".$request->input('region_id')."' OR distid='".$request->input('district_id')."' OR upid='".$request->input('upazila_id')."' OR unid='".$request->input('union_id')."' OR TW_No='".$request->input('TW_No')."' OR CDF_no='".$request->input('CDF_no')."'");
    
    
    if(!empty($request->input('district_id')) && !empty($request->input('upazila_id')) && !empty($request->input('union_id')) && !empty($request->input('starting_date')) && !empty($request->input('app_status'))){
      $waters =  DB::select("select * from tbl_water
      INNER JOIN
      water_quality_results
      ON
      tbl_water.id = water_quality_results.water_id
      where tbl_water.distid='".$request->input('district_id')."' AND tbl_water.upid='".$request->input('upazila_id')."' AND tbl_water.unid='".$request->input('union_id')."' AND tbl_water.App_date='".$request->input('starting_date')."' AND tbl_water.app_status='".$request->input('app_status')."'
      ORDER BY
      tbl_water.CDF_no ASC
      ");
      $location =  DB::select("select * from tbl_water where distid='".$request->input('district_id')."' AND upid='".$request->input('upazila_id')."' AND unid='".$request->input('union_id')."'
      order by id DESC limit 1");
     

     }else if(!empty($request->input('district_id')) && !empty($request->input('upazila_id')) && !empty($request->input('union_id')) && !empty($request->input('app_status'))){
      $waters =  DB::select("select * from tbl_water
      INNER JOIN
      water_quality_results
      ON
      tbl_water.id = water_quality_results.water_id
      where tbl_water.distid='".$request->input('district_id')."' AND tbl_water.upid='".$request->input('upazila_id')."' AND tbl_water.unid='".$request->input('union_id')."' AND tbl_water.app_status='".$request->input('app_status')."'
      ORDER BY
      tbl_water.CDF_no, tbl_water.id ASC
      ");
      $location =  DB::select("select * from tbl_water where distid='".$request->input('district_id')."' AND upid='".$request->input('upazila_id')."' AND unid='".$request->input('union_id')."'
      order by id DESC limit 1");
     

     }else if(!empty($request->input('district_id')) && !empty($request->input('upazila_id')) && !empty($request->input('union_id'))){
      $waters =  DB::select("select * from tbl_water
      INNER JOIN
      water_quality_results
      ON
      tbl_water.id = water_quality_results.water_id
      where tbl_water.distid='".$request->input('district_id')."' AND tbl_water.upid='".$request->input('upazila_id')."' AND tbl_water.unid='".$request->input('union_id')."'
      ORDER BY
      tbl_water.CDF_no ASC
      ");
      $location =  DB::select("select * from tbl_water where distid='".$request->input('district_id')."' AND upid='".$request->input('upazila_id')."' AND unid='".$request->input('union_id')."'
      order by id DESC limit 1");
     

     }

      return view($this->view_path.'printdata', compact('waters','old','location'));

  }


}
