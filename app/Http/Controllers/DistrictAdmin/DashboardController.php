<?php

namespace App\Http\Controllers\DistrictAdmin;

use App\Http\Controllers\Controller;

class DashboardController extends Controller {

  private $view_path = "core.district.";

  public function dashboard()
  {
    return view($this->view_path.'dashboard');
  }
}