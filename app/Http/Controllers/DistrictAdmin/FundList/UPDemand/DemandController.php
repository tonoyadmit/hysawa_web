<?php

namespace App\Http\Controllers\DistrictAdmin\FundList\UPDemand;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\Item;
use App\User;
use Datatables;
use App\Role;
use App\Fhead;
use App\Fsubhead;
use App\Fitemlist;
use App\FinanceData;
use App\FinanceDemand;
use App\FundList;
use Validator;
use Session;
use Redirect;
use DB;
use Auth;
use Entrust;
use Illuminate\Support\Facades\URL;
use \Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use File;

class DemandController extends Controller
{
  private $view_path = "core.district.up-demand.demand.";
  private $route_path = "district-admin.fundlist.up-demand.finace_demand.";

  public function index( Request $request )
  {
   // return select * from `demand` where `unid` = auth()->user()->unid and `region_id` is null and `proj_id` = ? and `fundlistid` = ? order by `id` desc
  
   
    // return $request->input('fid');
    $FinanceDemands = FinanceDemand::with('getHead', 'getSubhead', 'getItem')
                                    ->where('fundlistid', '=', $request->input('fid'))
                                    ->orderBy('id', 'desc')
                                    ->get();

                                    
    $Comments = DB::select("select * from fund_commentlist where fid = '".$request->input('fid')."'
    order by id DESC");

    $Filelists = DB::select("select * from fund_filelist where fid = '".$request->input('fid')."'
    order by id DESC");

    return view($this->view_path.'index', compact('FinanceDemands', 'Comments', 'Filelists'));
  }

  public function create(Request $request)
  {
    $items = Item::with('head')->groupBy('headid')->get();
    $head = [];
    foreach($items as $item){
      $head[$item->head->id] = $item->head->headname;
    }
    return view($this->view_path.'insert', compact('head'));
  }

  public function store(Request $request)
  {
    $validation = Validator::make($request->all(), [

      'head' => 'required',
      'subhead' => 'required',
      'item' => 'required',
      'amount' => 'required'
      ]);

    if($validation->fails()) {
      return Redirect::back()->withErrors($validation)->withInput();
    }


    $FinanceData = new FinanceDemand();
    $FinanceData->fill($request->except('_token'));
    $FinanceData->unid = auth()->user()->unid;
    $FinanceData->userid=auth()->user()->id;
    $FinanceData->created_by = auth()->user()->id;
    $FinanceData->updated_by = auth()->user()->id;

    $FinanceData->distid = auth()->user()->distid;
    $FinanceData->upid = auth()->user()->upid;
    $FinanceData->region_id = auth()->user()->region_id;
    $FinanceData->proj_id = auth()->user()->proj_id;
    $FinanceData->fundlistid =   $request->input('fid');
    
    $FinanceData->save();


      // return $request->input('fid');
 
    Session::flash('message', "New Data saved successfully");
    return redirect()->route($this->route_path.'index','fid='.$request->input('fid'));
  }


  public function store_comment(Request $request)
  {
//  return 1;
    // $validation = Validator::make($request->all(), [

    //   'comment' => 'required'

    //   ]);

    // if($validation->fails()) {
    //   return Redirect::back()->withErrors($validation)->withInput();
    // }


   // $comments =  DB::select("INSERT INTO fund_commentlist (fid, comment, names, type) VALUES ('1', 'a', '1','a')  ");

   if($request->input('comment')!=null){
    DB::table('fund_commentlist')->insert(
      array(
             'fid'     =>   $request->input('fid'), 
             'comment'   =>   $request->input('comment'),
             'names'  => auth()->user()->name
      )
 );
   }

if($request->hasFile('files')){
 $file = $request->file('files');

   
      //Display File Name
     $file->getClientOriginalName();
   
    // dd($file);
   
      //Display File Extension
     $file->getClientOriginalExtension();

   
      //Display File Real Path
     $file->getRealPath();

   
      //Display File Size
      $file->getSize();
 
   
      //Display File Mime Type
     $file->getMimeType();
   
     //$namecount =  DB::select("select id from fund_filelist ORDER BY id DESC LIMIT 1");
   
    
     // $chekimg =  DB::table('fund_filelist')->where('fid', '=', $request->input('fid'))->get();
          
      $chekimgs =   DB::select("select names from fund_filelist where fid = ". $request->input('fid'));

      if($chekimgs != null){
      foreach($chekimgs as $chekimg) {
    
        if($chekimg->names != null){
     
          $res = explode(".", $chekimg->names);

          $destinationPath = 'uploads/fundchecklist';

           $filname =  $chekimg->names; 

          \File::delete(public_path('/uploads/fundchecklist/'.$chekimg->names));
     
          $file->move($destinationPath, $res[0].'.' . $file->getClientOriginalExtension());
          
          DB::table('fund_filelist')
            ->where('fid',  $request->input('fid'))
            ->update(['names' => $res[0].'.' . $file->getClientOriginalExtension()]);

 
       }

      }

    }else{
   //   dd($chekimgs != null);
 
        $namecount =   DB::table('fund_filelist')->orderBy('id', 'DESC')->first();

        $total =  $namecount->id + 1;
   
      // dd($total);
         //Move Uploaded File
         $destinationPath = 'uploads/fundchecklist';
         $file->move($destinationPath, $total.'.' . $file->getClientOriginalExtension());
     
         

        DB::table('fund_filelist')->insert(
          array(
                 'fid'     =>   $request->input('fid'), 
                 'names'  => $total.'.' . $file->getClientOriginalExtension()
          )
     );
      
    }
    
 
     
     


}

 
    Session::flash('message', "New Data saved successfully");
    return redirect()->route('district-admin.fundlist.up-demand.fund_list.index');


  }


  public function edit($id)
  {
    $head = Fhead::pluck('headname','id')->toArray();
    $FinanceData = FinanceDemand::findOrFail($id);
    $subhead = Fsubhead::where('headid', '=',$FinanceData["head"])->pluck('sname','id')->toArray();
    $subitem = Fitemlist::where('headid', '=',$FinanceData["head"])->where('subid', '=',$FinanceData["subhead"])->pluck('itemname','id')->toArray();

    return view($this->view_path.'edit', compact('FinanceData','head','subhead','subitem'));;
  }

  public function update(Request $request, $id)
  {
    $validation = Validator::make($request->all(), [
      'head'          => 'required',
      'subhead'  => 'required',
      'item'           => 'required',
      'amount'          => 'required'
      ]);

    if($validation->fails()) {
      return Redirect::back()->withErrors($validation)->withInput();
    }

    $FinanceData = FinanceDemand::findOrFail($id);
    $FinanceData->fill($request->except('_token'));
    $FinanceData->updated_by = auth()->user()->id;
    $FinanceData->save();

    \Session::flash('message', "One updated successfully");
    return redirect()->route($this->route_path.'index','fid='.$FinanceData->fundlistid);
  }

  public function l(Request $request)
  {
    $requestData = $request->all();
    return $subhead = Fsubhead::where('headid', '=',$requestData["head"])->pluck('sname','id')->toArray();
  }

  public function subitem(Request $request)
  {
    $requestData = $request->all();
    return $subhead = Fitemlist::where('headid', '=',$requestData["head"])->where('subid', '=',$requestData["subhead"])->pluck('itemname','id')->toArray();
  }

  public function print( Request $request )
  {

    $FinanceDemands = FinanceDemand::with('getHead', 'getSubhead', 'getItem')
    ->where('fundlistid', '=', $request->input('fundlistid'))
    ->orderBy('id', 'desc')
    ->get();

    

   
   // $fdate = FundList::where('id','=',$request->input('fundlistid'))->get();

     $fundlistdata = FundList::where('id','=',$request->input('fundlistid'))->get();
    
     $Comments = DB::select("select * from fund_commentlist where fid = '".$request->input('fundlistid')."'
     order by id DESC");


   return view($this->view_path.'print', compact('FinanceDemands','fundlistdata', 'Comments'));
    //return view($this->view_path.'print')->with('FinanceDemands',(new FinanceDemand($request))->get());
 
  }


}
