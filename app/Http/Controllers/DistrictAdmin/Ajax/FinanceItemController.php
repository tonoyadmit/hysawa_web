<?php

namespace App\Http\Controllers\DistrictAdmin\Ajax;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class FinanceItemController extends Controller {

  public function subheads(Request $request)
  {

    if(!$request->ajax() || !$request->has('head_id'))
    {
      return ['status' => false];
    }

    $subheads = \DB::table('fsubhead')->where('headid', $request->input('head_id'))->get();

    $str = "";
    foreach($subheads as $subhead)
    {
      $str .= '<option value="'.$subhead->id.'">'.$subhead->sname.'</option>';
    }

    if($str != "")
    {
      $str = '<option value="">Choose an Option</option>'.$str;
    }

    return [
      'status' => true,
      'subhead_list' => $str
      ];
  }


  public function items(Request $request)
  {

    if(!$request->ajax() || !$request->has('subhead_id'))
    {
      return ['status' => false];
    }

    $items = \DB::table('fitem')->where('subid', $request->input('subhead_id'))->get();

    $str = "";

    foreach($items as $item)
    {
      $str .= '<option value="'.$item->id.'">'.$item->itemname.'</option>';
    }

    if($str != "")
    {
      $str = '<option value="">Choose an Option</option>'.$str;
    }

    return [
      'status' => true,
      'item_list' => $str
    ];

  }


}