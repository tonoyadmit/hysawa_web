<?php

namespace App\Http\Controllers\DistrictAdmin\Ajax;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\ArrayToXml\ArrayToXml;

class MapController extends Controller
{
  public function water(Request $request )
  {
    $waters = \DB::table('tbl_water')
        ->select('fupazila.upname',
          'funion.unname',
          'tbl_water.TW_No',
          'tbl_water.y_coord',
          'tbl_water.x_coord',
          'tbl_water.Village',
          'tbl_water.Landowner',
          'tbl_water.imp_status',
          'tbl_water.Caretaker_male',
          'tbl_water.HH_benefited',
          'tbl_water.wq_Arsenic',
          'tbl_water.wq_fe',
          'tbl_water.wq_cl',
          'tbl_water.depth')
          ->leftjoin('funion', 'tbl_water.unid', '=', 'funion.id')
          ->leftjoin('fupazila', 'tbl_water.upid', '=', 'fupazila.id')
          ->where('tbl_water.x_coord', '!=', "")
          ->where('tbl_water.y_coord', '!=', "")
          ->where(function($q){
            $q->where('tbl_water.imp_status', '=', 'Completed')
              ->orWhere('tbl_water.imp_status', '=', 'Submitted');
          })
          // ->take(50)
          ->get();

    $wa = [];

    $index = 0;
    foreach($waters as $water)
    {
      $wa[] = [
        'upname' => $water->upname == null ? "" : $this->parseToXML($water->upname) ,
        'unname' => $water->unname == null ? "" : $this->parseToXML($water->unname) ,
        'Landowner' => $water->Landowner == null ? "" : $this->parseToXML($water->Landowner) ,
        'Village' => $water->Village == null ? "" : $this->parseToXML($water->Village) ,

        'TW_No' => $water->TW_No == null ? "" : $this->parseToXML($water->TW_No) ,
        'Caretaker_male' => $water->Caretaker_male == null ? "" : $this->parseToXML($water->Caretaker_male ),
        'HH_benefited' => $water->HH_benefited == null ? "" : $this->parseToXML($water->HH_benefited) ,
        'wq_Arsenic' => $water->wq_Arsenic == null ? "" : $this->parseToXML($water->wq_Arsenic) ,

        'wq_fe'      => $water->wq_fe == null ? "" : $this->parseToXML($water->wq_fe) ,
        'wq_cl'      => $water->wq_cl == null ? "" : $this->parseToXML($water->wq_cl) ,
        'depth'      => $water->depth == null ? "" : $this->parseToXML($water->depth) ,
        'y_coord'    => $water->y_coord == null ? "" : $this->parseToXML($water->y_coord ),

        'x_coord'    => $water->x_coord == null ? "" : $this->parseToXML($water->x_coord) ,
        'imp_status' => $water->imp_status == null ? "" : $this->parseToXML($water->imp_status)
      ];
    }

    return response()->json($wa);
  }

  private function parseToXML($htmlStr)
  {
    $xmlStr=str_replace('<','&lt;',$htmlStr);
    $xmlStr=str_replace('>','&gt;',$xmlStr);
    $xmlStr=str_replace('"','&quot;',$xmlStr);
    $xmlStr=str_replace("'",'&#39;',$xmlStr);
    $xmlStr=str_replace("&",'&amp;',$xmlStr);

    $xmlStr = stripslashes($xmlStr);

    return $xmlStr;
  }

}