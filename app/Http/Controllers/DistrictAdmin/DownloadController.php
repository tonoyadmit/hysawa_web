<?php

namespace App\Http\Controllers\DistrictAdmin;

use App\Http\Controllers\Controller;

class DownloadController extends Controller {

  private $view_path = "core.district.download.";

  public function index()
  {
    return view($this->view_path.'index');
  }
}