<?php

namespace App\Http\Controllers\DistrictAdmin\MonthlyReport;

use App\Http\Controllers\Controller;
use App\Model\District;
use App\Model\Report\District\MonthlyReportGenerator;
use App\Model\Union;
use App\Model\Upazila;
use App\ReportData;
use DB;
use Illuminate\Http\Request;

class PeriodController extends Controller
{
  private $view_path = "core.district.monthly-report.water.";

  public function index(Request $request)
  {
    $user = \Auth::user();
    $user->load('roles');
    $districts = District::where('region_id', $user->region_id)->get();
    return view($this->view_path.'period.index', compact('districts'));
  }

  public function show(Request $request)
  {
    try{
      $report = new MonthlyReportGenerator($request);
      return $report->getReport();
    }catch(\Exception $e)
    {
      \Log::info($e->getMessage());

      \Session::flash('error', "Data Not Found.");
      return redirect()->back();

      return response()->json(['status' => 'false', 'message' => 'Something Went Wrong']);
    }
  }

  public function getPeriods(Request $request)
  {
    $periods= \DB::table('rep_period')
              ->select('rep_data.rep_id', 'rep_period.period', 'rep_data.id', 'rep_data.update')
              ->join('rep_data', 'rep_period.rep_id', '=', 'rep_data.rep_id')
              ->where(function($q) use($request) {
                  $q->where('rep_data.unid', $request->input('union_id'));
                })
              ->get();
    $str = '<option value="all">All</option>';
    foreach($periods as $p)
    {
      $str .= '<option value="'.$p->id.'">'.$p->period.'</option>';
    }
    return response()->json(['status' => true, 'data' => $str]);
  }
  
  
   public function getMonthlyRep(Request $request)
  {
    $user = \Auth::user();
    $user->load('roles');
    
    if ($request->has('query')) {

      if(!empty($request->project_id) && !empty($request->rep_data_id)){
               
 

$FinanceDemands = DB::select("SELECT rep_data.id, rep_data.unid, funion.unname, fupazila.upname, fdistrict.distname, rep_period.period  from rep_data

INNER JOIN funion
ON rep_data.unid = funion.id

INNER JOIN fupazila
ON rep_data.upid = fupazila.id

INNER JOIN fdistrict
ON rep_data.distid = fdistrict.id

INNER JOIN rep_period
ON rep_data.rep_id = rep_period.id

WHERE rep_data.`rep_id`=".$request->rep_data_id." and rep_data.`proj_id`=".$request->project_id." and rep_data.region_id=".$user->region_id."
");

$Unsubmitted = DB::select("SELECT osm_mou.unid, funion.unname, fupazila.upname, fdistrict.distname FROM `osm_mou`

INNER JOIN funion
ON osm_mou.unid = funion.id

INNER JOIN fupazila
ON osm_mou.upid = fupazila.id

INNER JOIN fdistrict
ON osm_mou.distid = fdistrict.id

WHERE 

osm_mou.unid NOT IN( select unid from rep_data where rep_data.rep_id=".$request->rep_data_id." and rep_data.proj_id = ".$request->project_id."
) 
and 
osm_mou.projid = ".$request->project_id." and osm_mou.region_id=".$user->region_id."

");

}

$old = [
        'district_id' => '',
        'project_id'   => '',
        'upazila_id'  => '',
        'union_id'    => '',
      ];

if ($request->has('query')) {
$old = [
        'query'       => $request->input('query'),
        'project_id'   => $request->project_id,
        'district_id' => $request->district_id,
        'upazila_id'  => $request->upazila_id,
        'union_id'    => $request->union_id
      ];
}
    }else{
    
   
    }


      
    //$datas = (new FundListDataListSearch($request, true))->get();
 //    return view($this->view_path.'index', compact('datas', 'old')); 


    return view($this->view_path.'period.monthlyrep', compact('FinanceDemands', 'old','Unsubmitted'));

  }




}
