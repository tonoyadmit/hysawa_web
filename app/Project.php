<?php

namespace App;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
  protected $table = 'roles';
  protected $fillable = ["project"];

  public function users()
  {
    return $this->belongsTo(User::class, 'project_user', 'project_id', 'user_id');
  }
}
