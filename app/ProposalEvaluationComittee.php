<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProposalEvaluationComittee extends Model
{
   protected $table = 'pec';
   protected $guarded = [
      'id','created_at', 'updated_at'
   ];
}
