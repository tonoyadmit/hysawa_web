<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MerchantUser extends Model
{
  protected $table = 'merchants_user';

  public function MerchantUser()
  {
    return $this->hasMany('App\Merchant','id','merchantsId');
  }
}
