<?php

namespace App;


use App\User;

use App\Model\District;
use App\Model\FinanceData;
use App\Model\Head;
use App\Model\ItemBudget;
use App\Model\Project;
use App\Model\Region;
use App\Model\SubHead;
use App\Model\Union;
use App\Model\Upazila;
use Illuminate\Database\Eloquent\Model;

class FundList extends Model
{
   protected $table = 'fundlist';
   protected $fillable = [
    'fdate',
    'tdate',
    'region_id',

    'distid',
    'upid',
    'unid',
    'proj_id',


    'userid'

  ];
   protected $guarded = [
      'id'
   ];
  
  public $timestamps = false;

  public function union()
  {
    return $this->belongsTo(Union::class, 'unid', 'id');
  }

  public function project()
  {
    return $this->belongsTo(Project::class, 'proj_id', 'id');
  }




}
