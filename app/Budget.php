<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class Budget extends Model
{
   protected $table = 'budget';
   public $timestamps = false;
   protected $fillable = [
    'itemid',
    'desbus',
    'mode',
    'vou',
    'desdate',
    'remarks',
    'distid',
    'upid',
    'unid',
    'userid',
    'proid',
    'entry_date',
    'region_id',
    'dup_id'
  ];

  protected $guarded = [
    'id'
      // 'id','created_at', 'updated_at'
   ];
}
