<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportData extends Model
{
   protected $table = 'rep_data';
   protected $guarded = [
      'id','created_at', 'updated_at'
   ];
}
