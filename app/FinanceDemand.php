<?php

namespace App;

use App\Model\Head;
use App\Model\Item;
use App\Model\SubHead;
use Illuminate\Database\Eloquent\Model;

class FinanceDemand extends Model
{
  protected $table = 'demand';

  protected $guarded = [
    'id','created_at', 'updated_at'
  ];

  public function getHead()
  {
    return $this->belongsTo(Head::class, 'head');
  }

  public function getSubhead()
  {
    return $this->belongsTo(SubHead::class, 'subhead');
  }

  public function getItem()
  {
    return $this->belongsTo(Item::class, 'item');
  }
  public function get()
  {
    return $this->demand;
  }
}
