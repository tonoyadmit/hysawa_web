<?php

namespace App\Model\Search;

use App\Model\Water;
use Illuminate\Http\Request;

class Water2Search
{
  private $datas;
  private $request;
  private $pagination;

  public function __construct(Request $request, $pagination = false)
  {
    $this->request = $request;
    $this->datas;
    $this->pagination = $pagination;
    $this->process();
  }

  private function process()
  {
    $request = $this->request;

    $q = Water::with('region', 'district', 'upazila', 'union', 'project')->where(function($query) use($request) {

      $request->date_type = "App_date";

      $region_id = $request->region_id;

      $starting_date = $request->starting_date;
      $ending_date = $request->ending_date;

      $created_by = $request->created_by;

      $distid = $request->district_id;
      $upid = $request->upazila_id;
      $unid = $request->union_id;

      $ward_no = $request->ward_no;
      $village = $request->village;
      $app_status = $request->app_status;
      $imp_status = $request->imp_status;

      $Tw_no = $request->Tw_no;
      $CDF_no = $request->CDF_no;

      if(!empty($region_id))
      {
        $query->where('region_id', '=', $region_id);
      }

      if($request->date_type != "")
      {
        if(!empty($starting_date) && !empty($ending_date))
        {
          $query->where($request->date_type, '>=', $starting_date)
                ->where($request->date_type, '<=', $ending_date);

        }elseif(!empty($starting_date))
        {
          $query->where($request->date_type, '=', $starting_date);
        }
      }

        $role = \Auth::user()->roles->first()->name;
        
        if($role == "upazila_admin")

        {
            $query->where('proj_id', auth()->user()->proj_id);
        }
          $query->where('proj_id', auth()->user()->proj_id);
        
      if(!empty($created_by))
      {
        $query->where('created_by', $created_by);
      }

      if(!empty($distid))
      {
        if($distid == "all"){
          $upid = "";
          $unid = "";

          

          if($role == "district_admin")
          {
            $query->where('region_id', auth()->user()->region_id);
          }

        }else{
          $query->where('distid', $distid);
        }
      }

      if(!empty($upid))
      {
        if($upid == "all"){

          $role = \Auth::user()->roles->first()->name;

          if($role == "upazila_admin"){
            $query->where('distid', auth()->user()->distid);
          }else{
            $query->where('distid', $distid);
          }
          $unid = "";
        }else{
          $query->where('upid', $upid);
        }

      }

      if(!empty($unid))
      {
        if($unid == "all"){
          $role = \Auth::user()->roles->first()->name;

          if($role == "upazila_admin"){
            $query->where('upid', auth()->user()->upid);
          }else{
            $query->where('upid', $upid);
          }



        }else{
          $query->where('unid', $unid);
        }
      }



      if(!empty($ward_no))
      {
        $query->where('Ward_no', $ward_no);
      }

      if(!empty($village))
      {
        $query->where('Village', 'like', "%$village%");
      }

      if(!empty($app_status))
      {
        $query->where('app_status', $app_status);
      }

      if(!empty($imp_status))
      {
        $query->where('imp_status', $imp_status);
      }

      if(!empty($Tw_no))
      {
        $query->where('TW_No', $Tw_no);
      }

      if(!empty($CDF_no))
      {
        $query->where('CDF_no', $CDF_no);
      }
    })
    ->orderBy('created_at', 'DESC');

    if($this->pagination){
      $this->datas = $q->paginate(15);
    }else{
      $this->datas = $q->get();
    }
  }

  public function get()
  {
    return $this->datas;
  }
}
