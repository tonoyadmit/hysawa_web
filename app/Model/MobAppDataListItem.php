<?php

namespace App\Model;

use App\Model\MobAppDataList;
use App\Model\MobAppQuestionLists;
use Illuminate\Database\Eloquent\Model;

class MobAppDataListItem extends Model
{
  protected $table = 'mobile_app_data_list_items';

  protected $guarded = [
    'id','created_at', 'updated_at'
  ];

  public function mobAppDataList()
  {
    return $this->belongsTo(MobAppDataList::class, 'mobile_app_data_list_id', 'id');
  }

  public function question()
  {
    return $this->belongsTo(MobAppQuestionLists::class, 'question_id', 'id');
  }
}
