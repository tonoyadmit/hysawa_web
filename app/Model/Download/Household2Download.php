<?php

namespace App\Model\Download;

use Illuminate\Http\Request;

class Household2Download
{
  public function download($datas)
  {
    $rows = $datas;
    \Excel::create('Sanitation Report '.time(), function($excel) use($rows) {
      $excel->sheet('Sheetname', function($sheet) use($rows) {
        $sheet->setOrientation('landscape');
        $sheet->row(1, array(
          'Region',
          'Project',
          'District',
          'Upazila',
          'Union',
          'CDF_No',
          'village',

          
          'hh_name',
          'father_husband',
          'age',
          'occupation',
          'mobile',
          'economic_status',
          'social_safetynet',
          'male',
          'female',
          'children',
          'disable',
          'ownership_type',
          'latrine_type',
          'latrine_details',
          'total_cost',
          'contribute_amount',
          
          'latitude	',
          'longitude',
          'app_date',
          'app_status',
          'imp_status',
          'created_by',
          'updated_by',
          'created_at',
          'updated_at'
          )
        );

        $rowIndex = 2;
        foreach($rows as $row)
        {
          $region   = !empty($row->region) ? $row->region->region_name : "";
          $project  = !empty($row->project) ? $row->project->project : "";
          $district = !empty($row->district) ? $row->district->distname : "";
          $upazila  = !empty($row->upazila) ? $row->upazila->upname : "";
          $union    = !empty($row->union) ? $row->union->unname: "";

          $sheet->row($rowIndex, [
            $region,
            $project,
            $district,
            $upazila,
            $union,
            $row->cdfno,
            $row->village,

            $row->hh_name,
            $row->father_husband,
            $row->age,
            $row->occupation,
            $row->mobile,
            $row->economic_status,
            $row->social_safetynet,
            $row->male,
            $row->female,
            $row->children,
            $row->disable,
            $row->ownership_type,
            $row->latrine_type,
            $row->latrine_details,
            $row->total_cost,
            $row->contribute_amount,

            $row->app_date,
            $row->app_status,
            $row->imp_status,
            $row->created_by,
            $row->updated_by,
            $row->created_at,
            $row->updated_at
          ]);
          $rowIndex++;
        }
        });
    })->download('csv');;
  }
}
