<?php

namespace App;
use Illuminate\Database\Eloquent\Model;

class TenderEvaluationComittee extends Model
{
   protected $table = 'tec';
   protected $guarded = [
      'id','created_at', 'updated_at'
   ];
}
