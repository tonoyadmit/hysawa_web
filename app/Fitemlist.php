<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Fitemlist extends Model
{
   protected $table = 'fitem';
   protected $guarded = [
      'id','created_by', 'updated_by'
   ];
}
