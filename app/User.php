<?php

namespace App;

use App\Model\District;
use App\Model\Project;
use App\Model\Region;
use App\Model\Union;
use App\Model\Upazila;
use App\UserType;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject as JWTSubject;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable implements JWTSubject
{
    use EntrustUserTrait; // add this trait to your user model

    protected $fillable = [
        'name',
        'email',
        'supervisor',
        'type',
        'distid',
        'upid',
        'unid',
        'proj_id',
        'region_id',
        'api_token',
        'msisdn',
        'user_type_id',
        'status',
        'created_by',
        'updated_by'
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [
        'id', 'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    /**
     * Get the identifier that will be stored in the subject claim of the JWT
     *
     * @return mixed
     */
    public function getJWTIdentifier(){
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT
     *
     * @return array
     */
    public function getJWTCustomClaims(){
        return [];
    }

    public function userType()
    {
        return $this->belongsTo(UserType::class, 'user_type_id', 'id');
    }


    public function region()
    {
        return $this->belongsTo(Region::class, 'region_id', 'region_id');
    }


    public function project()
    {
        return $this->belongsTo(Project::class, 'proj_id', 'id');
    }


    public function district()
    {
        return $this->belongsTo(District::class, 'distid', 'id');
    }


    public function upazila()
    {
        return $this->belongsTo(Upazila::class, 'upid', 'id');
    }

    public function union()
    {
        return $this->belongsTo(Union::class, 'unid', 'id');
    }
    
    public function getProjectTitle()
    {
        if($this->proj_id != ""){
            $p = Project::find($this->proj_id); 
            if(count($p)){
                return $p->project;
            }
        }
        return "";
    }
}
