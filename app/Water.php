<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Water extends Model
{
   protected $table = 'tbl_water';

   protected $guarded = [
      'id','created_by', 'updated_by'
   ];
}
