<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateTblWaterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tbl_water', function(Blueprint $table){
            $table->timestamp('up_approval_at')->nullable();
            $table->timestamp('dist_approval_at')->nullable();
            $table->integer('up_approval_by')->unsigned()->nullable();
            $table->integer('dist_approval_by')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
