<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobileAppDataListItemsTable extends Migration
{
    public function up()
    {
        Schema::create('mobile_app_data_list_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mobile_app_data_list_id')->unsigned();
            $table->string('question_id');
            $table->string('value');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('mobile_app_data_list_items');
    }
}
