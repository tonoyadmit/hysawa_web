<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMobileAppDataListsTable extends Migration
{
    public function up()
    {
        Schema::table('mob_app_data_list', function (Blueprint $table) {
            $table->dropColumn('answers');
        });

        Schema::table('mob_app_data_list', function (Blueprint $table) {
            $table->text('answers')->nullable();
        });
    }

    public function down()
    {
        //
    }
}
