<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobAppQuestionListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mob_app_question_lists', function (Blueprint $table) {
            $table->increments('id');
            $table->string('category', 500)->nullable();
            $table->string('question', 500)->nullable();
            $table->string('field', 50)->nullable();
            $table->integer('parent_id')->nullable();
            $table->integer('sl')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mob_app_question_lists');
    }
}
