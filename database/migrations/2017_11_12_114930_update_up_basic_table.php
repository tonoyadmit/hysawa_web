<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUpBasicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('up_basic', function (Blueprint $table) {
            $table->integer('service')->default(0);
            $table->integer('water_other')->default(0);
            $table->integer('water_shallow_tw')->default(0);
            $table->integer('water_deep_tw')->default(0);
            $table->integer('latrine')->default(0);
            $table->integer('launch_ferry_ghat')->default(0);
            $table->integer('household_hh')->default(0);
            $table->integer('hardcore_poor_hh')->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
