<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUpBasicTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bank', function(Blueprint $table){
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();

            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
        });

        // Schema::table('', function(Blueprint $table){
        //     $table->timestamp('created_at')->nullable();
        //     $table->timestamp('deleted_at')->nullable();

        //     $table->integer('deleted_at')->unsigned()->nullable();
        //     $table->integer('deleted_at')->unsigned()->nullable();
        // });

        // Schema::table('', function(Blueprint $table){
        //     $table->timestamp('created_at')->nullable();
        //     $table->timestamp('deleted_at')->nullable();

        //     $table->integer('deleted_at')->unsigned()->nullable();
        //     $table->integer('deleted_at')->unsigned()->nullable();
        // });

        // Schema::table('', function(Blueprint $table){
        //     $table->timestamp('created_at')->nullable();
        //     $table->timestamp('deleted_at')->nullable();

        //     $table->integer('deleted_at')->unsigned()->nullable();
        //     $table->integer('deleted_at')->unsigned()->nullable();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
