<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMobAppQuestionListsUpdate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('mob_app_data_list', function (Blueprint $table) {
            $table->integer('group_id');
            $table->integer('project_id');
        });

        Schema::table('mob_app_question_lists', function (Blueprint $table) {
            $table->integer('group_id');
            $table->string('input_type')->default('text');
            $table->string('options')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
