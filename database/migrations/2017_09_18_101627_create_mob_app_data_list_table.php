<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMobAppDataListTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mob_app_data_list', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('user_id')->unsigned();

            $table->string('distid')->nullable();
            $table->string('upid')->nullable();
            $table->string('unid')->nullable();
            $table->string('proj_id')->nullable();

            $table->string('region_id')->nullable();

            $table->decimal('latitude', 11, 7);
            $table->decimal('longitude', 11, 7);

            $table->integer('question_id')->unsigned();
            $table->integer('value');

            $table->string('image1')->nullable();
            $table->string('image2')->nullable();
            $table->string('image3')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('mob_app_data_list');
    }
}
