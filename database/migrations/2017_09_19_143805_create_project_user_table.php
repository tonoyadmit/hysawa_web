<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectUserTable extends Migration
{
    public function up()
    {
        Schema::create('project_user', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('project_id')->unsigned();
        });
    }

    public function down()
    {
        Schema::table('project_user', function(Blueprint $table){
            $table->dropForeign('project_user_user_id_foreign');
            $table->dropForeign('project_user_project_id_foreign');
        });

        Schema::dropIfExists('project_user');
    }
}
