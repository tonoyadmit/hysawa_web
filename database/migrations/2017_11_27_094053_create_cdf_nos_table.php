<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCdfNosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cdf_nos', function (Blueprint $table) {
            $table->increments('id');

            $table->integer('district_id')->unsigned();
            $table->integer('upazila_id')->unsigned();
            $table->integer('union_id')->unsigned();
            $table->integer('cdf_no')->unsigned();
            $table->integer('updated_by')->unsigned();
            $table->integer('created_by')->unsigned();
            $table->string('village');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cdf_nos');
    }
}
