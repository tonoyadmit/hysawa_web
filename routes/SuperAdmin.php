<?php

Route::group(['middleware' => ['auth', 'role:superadministrator']], function() {

    Route::group([
    'namespace' => 'SuperAdmin',
    'as' => 'superadmin.',
    'prefix' => 'superadmin'], function(){

        Route::group(['prefix' => 'ajax', 'as' => 'ajax.', 'namespace' => 'Ajax'], function(){
            Route::get('/unions', ['as' => 'union', 'uses' => 'LocationController@getUnions']);
            Route::get('/upazilas', ['as' => 'upazila', 'uses' => 'LocationController@getUpazilas']);
            Route::get('/districts', ['as' => 'district', 'uses' => 'LocationController@getDistricts']);



            Route::get('/finance/subheads', ['as' => 'subheads', 'uses' => 'FinanceItemController@subheads']);
            Route::get('/finance/items', ['as' => 'items', 'uses' => 'FinanceItemController@items']);

            Route::get('/map/waters', ['as' => 'map.water', 'uses' => 'MapController@water']);
            Route::get('/map/mobiles', ['as' => 'map.mobile', 'uses' => 'MapController@mobile']);
            Route::get('/map/mobiles_2', ['as' => 'map.mobile_2', 'uses' => 'MapController@mobile_2']);
        });

        Route::group(['namespace' => 'Location', 'as' => 'location.'], function(){


            Route::group(['prefix' => 'region', 'as' => 'region.'], function(){

                Route::post('edit/{id}', ['as' => 'update', 'uses' => 'RegionController@update']);
                Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'RegionController@edit']);

                Route::post('/create', ['as' => 'store', 'uses' => 'RegionController@store']);
                Route::get('/', ['as' => 'index', 'uses' => 'RegionController@index']);
            });

            Route::group(['prefix' => 'project', 'as' => 'project.'], function(){

                Route::post('edit/{id}', ['as' => 'update', 'uses' => 'ProjectController@update']);
                Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'ProjectController@edit']);

                Route::post('/create', ['as' => 'store', 'uses' => 'ProjectController@store']);
                Route::get('/', ['as' => 'index', 'uses' => 'ProjectController@index']);
            });

            Route::group(['prefix' => 'district', 'as' => 'district.'], function(){
                Route::post('edit/{id}', ['as' => 'update', 'uses' => 'DistrictController@update']);
                Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'DistrictController@edit']);

                Route::post('/create', ['as' => 'store', 'uses' => 'DistrictController@store']);
                Route::get('/create', ['as' => 'create', 'uses' => 'DistrictController@create']);
                Route::get('/', ['as' => 'index', 'uses' => 'DistrictController@index']);
            });


            Route::group(['prefix' => 'upazila', 'as' => 'upazila.'], function(){

                Route::post('edit/{id}', ['as' => 'update', 'uses' => 'UpazilaController@update']);
                Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'UpazilaController@edit']);

                Route::post('/create', ['as' => 'store', 'uses' => 'UpazilaController@store']);
                Route::get('/create', ['as' => 'create', 'uses' => 'UpazilaController@create']);
                Route::get('/', ['as' => 'index', 'uses' => 'UpazilaController@index']);
            });

            Route::group(['prefix' => 'union', 'as' => 'union.'], function(){

                Route::post('/edit/{id}', ['as' => 'update', 'uses' => 'UnionController@update']);
                Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'UnionController@edit']);

                Route::post('/create', ['as' => 'store', 'uses' => 'UnionController@store']);
                Route::get('/create', ['as' => 'create', 'uses' => 'UnionController@create']);
                Route::get('/', ['as' => 'index', 'uses' => 'UnionController@index']);
            });
        });

        Route::group(['namespace' => 'WaterSanitation'], function() {
            Route::get('/water_sanitation/download', ['as' => 'water_sanitation.download', 'uses' => 'WaterSanitationController@download']);
            Route::post('/water_sanitation/search', ['as' => 'water_sanitation.search', 'uses' => 'WaterSanitationController@search']);
            Route::resource('/water_sanitation', 'WaterSanitationController');
        });

        Route::group(['namespace' => 'Water'], function() {
            Route::get('/water/map-2', ['as' => 'water.map.show2', 'uses' => 'WaterController@map2']);
            Route::get('/water/map', ['as' => 'water.map.show', 'uses' => 'WaterController@map']);
            Route::get('/water/mobile_map', ['as' => 'water.map.mobile_map', 'uses' => 'WaterController@mobile_map']);
            Route::get('/water/mobile_map_2', ['as' => 'water.map.mobile_map_2', 'uses' => 'WaterController@mobile_map_2']);
            Route::get('water/download', ['as' => 'water.download', 'uses' => 'WaterController@download']);
            Route::post('/water/search', ['as' => 'water.search', 'uses' => 'WaterController@search']);
            Route::resource('/water', 'WaterController');
        });

        Route::group(['namespace' => 'SubProject', 'prefix' => 'sub-projects', 'as' => 'sub-projects.'], function(){

            Route::group(['prefix' => 'sanitation', 'as' => 'sanitation.'], function(){
                Route::get('get-download/{type}',   ['as' => 'get-dl', 'uses' => 'DownloadController@index']);
                Route::get('/summary', ['as' => 'summary', 'uses' => 'SanitationController@summary']);

                Route::post('/edit/{id}', ['as' => 'update', 'uses' => 'SanitationController@update']);
                Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'SanitationController@edit']);
                Route::get('/', ['as' => 'index', 'uses' => 'SanitationController@index']);
            });

            Route::group(['prefix' => 'water', 'as' => 'waters.'], function(){

                Route::get('/download/{type}',   ['as' => 'download', 'uses' => 'WaterController@download']);
                Route::get('/approval-summary',  ['as' => 'approval', 'uses' => 'WaterController@approval']);
                Route::get('/unions-summary',    ['as' => 'unions', 'uses' => 'WaterController@unions']);
                Route::get('/districts-summary', ['as' => 'districts', 'uses' => 'WaterController@districts']);
                Route::get('/regions-summary',   ['as' => 'regions', 'uses' => 'WaterController@regions']);
                Route::get('/',                  ['as' => 'index', 'uses' => 'WaterController@index']);
            });
        });

        Route::group(['namespace' => 'MonthlyReport', 'prefix' => 'monthly-report', 'as' => 'monthly-report.'], function(){

            Route::get('/report/show',    ['as' => 'report.show', 'uses' => 'ReportController@show']);
            Route::get('/report/period', ['as' => 'report.get-period', 'uses' => 'ReportController@getPeriods']);
            Route::get('/report',        ['as' => 'report.index', 'uses' => 'ReportController@index']);
            Route::get('/report/monthlyrep',        ['as' => 'report.monthlyrep', 'uses' => 'ReportController@getMonthlyRep']);
            // Route::get('/report', ['as' => 'report.index', 'uses' => 'ReportController@index']);
            Route::get('up-wise-wash-status', ['as' => 'wash', 'uses' => 'UpWiseWashStatusController@index']);
        });

        Route::group(['namespace' => 'Monitoring','prefix' => 'monitoring', 'as' => 'monitoring.'], function() {
            Route::group(['prefix' => 'upcapacity', 'as' => 'upcapacity.'], function(){
                Route::get('/', ['as' => 'index', 'uses' => 'UpCapacityController@index']);
                Route::resource('/reportlist', 'ReportController');
            });
        });

        Route::group(['namespace' => 'PNGO','prefix' => 'pngo', 'as' => 'pngo.'], function() {
            // Route::get('/download', ['as' => 'download', 'uses' => 'DownloadController@get']);
            Route::get('/', ['as' => 'index', 'uses' => 'PNGOController@index']);
            Route::resource('/pngolist', 'PNGOController');
        });

        Route::group(['namespace' => 'Finance', 'prefix' => 'finance', 'as' => 'finance.'], function(){

            Route::group(['namespace' => 'ChartsOfAccount', 'prefix' => 'charts-of-account', 'as' => 'charts-of-account.'], function(){

                Route::group(['prefix' => 'heads', 'as' => 'heads.'], function(){
                    Route::post('/update/{id}', ['as' => 'update', 'uses' => 'HeadController@update']);
                    Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'HeadController@edit']);
                    Route::post('/store', ['as' => 'store', 'uses' => 'HeadController@store']);
                    Route::get('/', ['as' => 'index', 'uses' => 'HeadController@index']);
                });

                Route::group(['prefix' => 'sub-heads', 'as' => 'sub-heads.'], function(){
                    Route::post('/update/{id}', ['as' => 'update', 'uses' => 'SubHeadController@update']);
                    Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'SubHeadController@edit']);
                    Route::post('/store', ['as' => 'store', 'uses' => 'SubHeadController@store']);
                    Route::get('/', ['as' => 'index', 'uses' => 'SubHeadController@index']);
                });

                Route::group(['prefix' => 'items', 'as' => 'items.'], function(){
                    Route::post('/update/{id}', ['as' => 'update', 'uses' => 'ItemController@update']);
                    Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'ItemController@edit']);
                    Route::post('/store', ['as' => 'store', 'uses' => 'ItemController@store']);
                    Route::get('/', ['as' => 'index', 'uses' => 'ItemController@index']);
                });
            });

            Route::group(['namespace' => 'DisbursmentToUP', 'prefix' => 'disbursment-to-up', 'as' => 'disbursment-to-up.'], function(){

                Route::group(['prefix' => 'disbursment', 'as' => 'disbursment.'], function(){

                    Route::post('/update/{id}', ['as' => 'update', 'uses' => 'DisbursmentController@update']);
                    Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'DisbursmentController@edit']);

                    Route::post('/store', ['as' => 'store', 'uses' => 'DisbursmentController@store']);

                    Route::get('/create', ['as' => 'create', 'uses' => 'DisbursmentController@create']);
                    Route::get('/', ['as' => 'index', 'uses' => 'DisbursmentController@index']);
                });

                Route::group(['prefix' => 'fund-disbursment', 'as' => 'fund-disbursment.'], function(){
                    Route::get('/entry-track-by-up', ['as' => 'index2', 'uses' => 'FundDisbursmentController@index2']);
                    Route::get('/entry-track-by-date', ['as' => 'index', 'uses' => 'FundDisbursmentController@index']);
                });
            });

            Route::group(['namespace' => 'Report', 'prefix' => 'report', 'as' => 'report.'], function(){

                Route::group(['prefix' => 'analysis-by-project-date-up', 'as' => 'analysis.'], function(){
                    Route::get('/', ['as' => 'index', 'uses' => 'AnalysisByProjectDateUPController@index']);
                });

                Route::group(['prefix' => 'consolidated-report', 'as' => 'consolidated.'], function(){
                    Route::get('/', ['as' => 'index', 'uses' => 'ConsolidatedReportController@index']);
                });

                Route::group(['prefix' => 'district-report', 'as' => 'district.'], function(){
                    Route::get('/', ['as' => 'index', 'uses' => 'DistrictReportController@index']);
                });

                Route::group(['prefix' => 'project-report', 'as' => 'project.'], function(){
                    Route::get('/', ['as' => 'index', 'uses' => 'ProjectReportController@index']);
                });

                Route::group(['prefix' => 'summary-by-union-report', 'as' => 'summary.'], function(){
                    Route::get('/', ['as' => 'index', 'uses' => 'SummaryByUnionReportController@index']);
                });

                Route::group(['prefix' => 'union-report', 'as' => 'union.'], function(){
                    Route::get('/', ['as' => 'index', 'uses' => 'UnionReportController@index']);
                });

                Route::group(['prefix' => 'upazila-report', 'as' => 'upazila.'], function(){
                    Route::get('/', ['as' => 'index', 'uses' => 'UpazilaReportController@index']);
                });

                Route::group(['prefix' => 'common-report', 'as' => 'common.'], function(){
                    Route::get('/', ['as' => 'index', 'uses' => 'CommonReportController@index']);
                });

            });

            Route::group(['namespace' => 'UPBudget', 'prefix' => 'up-budget', 'as' => 'up-budget.'], function(){

                Route::post('/update/{id}', ['as' => 'update', 'uses' => 'UPBudgetController@update']);
                Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'UPBudgetController@edit']);
                Route::post('/store', ['as' => 'store', 'uses' => 'UPBudgetController@store']);
                Route::get('/create', ['as' => 'create', 'uses' => 'UPBudgetController@create']);
                Route::get('/', ['as' => 'index', 'uses' => 'UPBudgetController@index']);

            });

            Route::group(['namespace' => 'UPDataEdit', 'prefix' => 'up-data-edit', 'as' => 'up-data-edit.'], function(){

                Route::group(['prefix' => 'demand', 'as' => 'demand.'], function(){
                    Route::get('/', ['as' => 'index', 'uses' => 'DemandController@index']);
                });

                Route::group(['prefix' => 'expenditure', 'as' => 'expenditure.'], function(){
                    Route::get('/', ['as' => 'index', 'uses' => 'ExpenditureController@index']);
                });

                Route::group(['prefix' => 'income', 'as' => 'income.'], function(){
                    Route::get('/', ['as' => 'index', 'uses' => 'IncomeController@index']);
                });
            });

            //------------------Demand Adding----------------
            Route::group(['namespace' => 'UPDemand', 'prefix' => 'up-demand', 'as' => 'up-demand.'], function(){

            Route::get('finace_demand/print', ['uses' => 'DemandController@print', 'as' => 'finace_demand.print']);
            Route::resource('/finace_demand', 'DemandController');

            Route::resource('/fund_list', 'FundListController');
            
            });

                //------------------MDemand Adding----------------
                 Route::group(['namespace' => 'UPDemandM', 'prefix' => 'up-demand-m', 'as' => 'up-demand-m.'], function(){

                   // Route::get('finace_demand/print', ['uses' => 'DemandMController@print', 'as' => 'finace_demand.print']);
                    Route::resource('/survey_details', 'DemandMController');
                    Route::resource('/survey_list', 'FundListMController');
                    // Route::resource('/print_data', 'FundListMController');
                    Route::get('print_data', ['uses' => 'FundListMController@print', 'as' => 'print_data.print']);
                    
                    });

        });

        Route::group(['namespace' => 'Training', 'prefix' => 'training', 'as' => 'training.'], function(){
            Route::get('/', ['as' => 'summary', 'uses' => 'SummaryController@summary']);
            Route::resource('/training', 'TrainingController');
            Route::get('getUpzilla', ['as' => 'getUpzilla', 'uses' => 'TrainingParticipantController@getUpzilla']);
            Route::get('getUnion',   ['as' => 'getUnion',   'uses' => 'TrainingParticipantController@getUnion', ]);
            Route::resource('/training-participant', 'TrainingParticipantController');
        });

        Route::group(['namespace' => 'Procurement', 'prefix' => 'procurement', 'as' => 'procurement.'], function(){
            Route::group(['prefix' => 'contractors', 'as' => 'contractors.'], function(){

                Route::post('/update/{id}', ['as' => 'update', 'uses' => 'ContractorController@update']);
                Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'ContractorController@edit']);

                Route::post('/store', ['as' => 'store', 'uses' => 'ContractorController@store']);
                Route::get('/create', ['as' => 'create', 'uses' => 'ContractorController@create']);
                Route::get('/', ['as' => 'index', 'uses' => 'ContractorController@index']);
            });

            Route::group(['prefix' => 'pec', 'as' => 'pec.'], function(){
                Route::get('/print',    ['as' => 'print', 'uses' => 'PECController@print']);
                Route::get('/download', ['as' => 'download', 'uses' => 'PECController@download']);
                Route::get('/',         ['as' => 'index', 'uses' => 'PECController@index']);
            });

            Route::group(['prefix' => 'tec', 'as' => 'tec.'], function(){
                Route::get('/print',    ['as' => 'print', 'uses' => 'TECController@print']);
                Route::get('/download', ['as' => 'download', 'uses' => 'TECController@download']);
                Route::get('/',         ['as' => 'index', 'uses' => 'TECController@index']);
            });

            Route::group(['prefix' => 'list', 'as' => 'list.'], function(){
                Route::get('/print',    ['as' => 'print', 'uses' => 'ProcurementListController@print']);
                Route::get('/download', ['as' => 'download', 'uses' => 'ProcurementListController@download']);
                Route::get('/',         ['as' => 'index', 'uses' => 'ProcurementListController@index']);
            });

            Route::group(['prefix' => 'evaluations', 'as' => 'evaluations.'], function(){
                Route::get('/print',    ['as' => 'print', 'uses' => 'ProcurementEvaluationController@print']);
                Route::get('/download', ['as' => 'download', 'uses' => 'ProcurementEvaluationController@download']);
                Route::get('/',         ['as' => 'index', 'uses' => 'ProcurementEvaluationController@index']);
            });
        });

        Route::group(['namespace' => 'UP', 'prefix' => 'up', 'as' => 'up.'], function(){

            Route::group(['prefix' => 'stuff', 'as' => 'stuff.'], function(){
                Route::get('/',         ['as' => 'index', 'uses' => 'StuffController@index']);
            });

            Route::group(['prefix' => 'functionaries', 'as' => 'functionaries.'], function(){
                Route::get('/', ['as' => 'index', 'uses' => 'FunctionariesController@index']);
            });

            Route::group(['prefix' => 'mou', 'as' => 'mou.'], function(){
                Route::post('/union/{id}/update', ['as' => 'update', 'uses' => 'MOUController@unionUpdate']);
                Route::get('/union/{id}/edit', ['as' => 'edit', 'uses' => 'MOUController@unionEdit']);

                Route::get('/district/{id}', ['as' => 'district', 'uses' => 'MOUController@district']);
                Route::get('/region/{id}', ['as' => 'region', 'uses' => 'MOUController@region']);
                Route::get('/project/{id}', ['as' => 'project', 'uses' => 'MOUController@project']);

                Route::post('/store', ['as' => 'store', 'uses' => 'MOUController@store']);
                Route::get('/create', ['as' => 'create', 'uses' => 'MOUController@create']);
                Route::get('/', ['as' => 'index', 'uses' => 'MOUController@index']);
            });
        });

        Route::group(['prefix' => 'download', 'as' => 'download.'], function(){
            Route::get('/', ['as' => 'index', 'uses' => 'DownloadController@index']);
        });

        Route::group(['namespace' => 'MobileApp'], function(){

            Route::group(['prefix' => 'mobile-app', 'as' => 'mobile-app.'], function(){
                Route::get('/show/{id}', ['as' => 'show', 'uses' => 'MobileAppController@show']);
                Route::get('/', ['as' => 'index', 'uses' => 'MobileAppController@index']);
                Route::get('/user_report', ['as' => 'userrep', 'uses' => 'MobileAppController@getUserRep']);
            });

            Route::group(['prefix' => 'cdfs', 'as' => 'cdfs.'], function(){
                Route::post('/update/{id}', ['as' => 'update', 'uses' => 'CDFsController@update']);
                Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'CDFsController@edit']);
                Route::post('/store', ['as' => 'store', 'uses' => 'CDFsController@store']);
                Route::get('/', ['as' => 'index', 'uses' => 'CDFsController@index']);
            });
        });


        Route::group(['namespace' => 'UserManagement', 'prefix' => 'user-management', 'as' => 'user-management.'], function(){

            Route::post('/status-change/{id}', ['as' => 'status-change', 'uses' => 'StatusController@change']);

            Route::get('/password-change/{id}', ['as' => "password-change", 'uses' => 'PasswordController@get']);
            Route::post('/password-change/{id}', ['as' => "password-change.post", 'uses' => 'PasswordController@change']);

            Route::post('/update/{id}', ['as' => 'update', 'uses' => 'UserController@update']);
            Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'UserController@edit']);

            Route::get('/show/{id}', ['as' => 'show', 'uses' => 'UserController@show']);

            Route::post('/store', ['as' => 'store',  'uses' => 'UserController@store']);
            Route::get('/create', ['as' => 'create', 'uses' => 'UserController@create']);

            Route::get('/',       ['as' => 'index',  'uses' => 'UserController@index']);
        });

        Route::get('/', ['as' => 'dashboard', 'uses' => 'DashboardController@dashboard']);
    });
});