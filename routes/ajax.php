<?php

//ajax.region.index
Route::group(['prefix' => 'ajax', 'as' => 'ajax.', 'namespace' => 'Ajax'], function(){

  Route::get('/projects',  ['as' => 'project.index',  'uses' => 'ProjectController@index']);
  Route::get('/regions',   ['as' => 'region.index',   'uses' => 'RegionController@index']);
  Route::get('/districts', ['as' => 'district.index', 'uses' => 'DistrictController@index']);
  Route::get('/upazilas',  ['as' => 'upazila.index',  'uses' => 'UpazilaController@index']);
  Route::get('/unions',    ['as' => 'union.index',    'uses' => 'UnionController@index']);

  Route::get('/regions-from-project',   ['as' => 'regions-from-project',   'uses' => 'RegionController@fromProject']);
  Route::get('/districts-from-region',  ['as' => 'districts-from-region',  'uses' => 'DistrictController@fromRegion']);
  Route::get('/upazilas-from-district', ['as' => 'upazilas-from-district', 'uses' => 'UpazilaController@fromDistrict']);
  Route::get('/unions-from-upazila',    ['as' => 'unions-from-upazila',    'uses' => 'UnionController@fromUpazila']);
});
