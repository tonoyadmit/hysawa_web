<?php

Route::group(['middleware' => ['auth', 'role:upazila_admin']], function() {

    Route::group([
        'namespace' => 'UpazilaAdmin',
        'as' => 'upazila-admin.',
        'prefix' => 'upazila'], function(){

          Route::group(['namespace' => 'MonthlyReport'], function() {
            Route::get('/period/show',    ['as' => 'periods.show', 'uses' => 'PeriodController@show']);
            Route::get('/periods/period', ['as' => 'periods.get-period', 'uses' => 'PeriodController@getPeriods']);
            Route::get('/periods',        ['as' => 'periods.index', 'uses' => 'PeriodController@index']);
          });

          Route::group(['namespace' => 'Sanitation', 'as' => 'sanitation.', 'prefix' => 'sanitation'], function(){
            Route::post('/update/{id}', ['as' => 'update1', 'uses' => 'SanitationController@update']);
            Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'SanitationController@edit']);
            Route::get('/download', ['as' => 'download', 'uses' => 'SanitationController@download']);
            Route::get('/', ['as' => 'index', 'uses' => 'SanitationController@index']);
          });

          Route::group(['namespace' => 'Water', 'as' => 'water.', 'prefix' => 'water'], function(){
            Route::post('/update/{id}', ['as' => 'update', 'uses' => 'WaterController@update']);
            Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'WaterController@edit']);
            Route::get('/download', ['as' => 'download', 'uses' => 'WaterController@download']);
            Route::get('/', ['as' => 'index', 'uses' => 'WaterController@index']);
          });

          Route::group(['namespace' => 'Finance'], function() {
            Route::get('/finance/report/print', ['as' => 'finance.report.print', 'uses' => 'ReportController@print']);
            Route::get('/finance/report', ['as' => 'finance.report.index', 'uses' => 'ReportController@index']);
          });

          Route::group(['prefix' => 'download', 'as' => 'download.'], function(){
              Route::get('/', ['as' => 'index', 'uses' => 'DownloadController@index']);
          });

          Route::group(['namespace' => 'MobileApp'], function(){
            Route::group(['prefix' => 'mobile-app', 'as' => 'mobile-app.'], function(){
                Route::get('/show/{id}', ['as' => 'show', 'uses' => 'MobileAppController@show']);
                Route::get('/', ['as' => 'index', 'uses' => 'MobileAppController@index']);
            });
        });

          Route::get('/', ['as' => 'dashboard', 'uses' => 'DashboardController@dashboard']);
    });
});