<?php
Route::group(['middleware' => ['auth', 'role:district_admin']], function() {

    Route::group([
    'namespace' => 'DistrictAdmin',
    'as' => 'district-admin.',
    'prefix' => 'district'], function(){

        Route::group(['prefix' => 'ajax', 'as' => 'ajax.', 'namespace' => 'Ajax'], function(){
            Route::get('/district-unions', ['as' => 'district-union', 'uses' => 'LocationController@getUnions']);
            Route::get('/district-upazilas', ['as' => 'district-upazila', 'uses' => 'LocationController@getUpazilas']);
            Route::get('/district-districts', ['as' => 'district-district', 'uses' => 'LocationController@getDistricts']);

            Route::get('/finance/subheads', ['as' => 'subheads', 'uses' => 'FinanceItemController@subheads']);
            Route::get('/finance/items', ['as' => 'items', 'uses' => 'FinanceItemController@items']);

            Route::get('/map/waters', ['as' => 'map.water', 'uses' => 'MapController@water']);
        });

        Route::group(['namespace' => 'Location', 'as' => 'location.'], function(){


            Route::group(['prefix' => 'region', 'as' => 'region.'], function(){

                Route::post('edit/{id}', ['as' => 'update', 'uses' => 'RegionController@update']);
                Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'RegionController@edit']);

                Route::post('/create', ['as' => 'store', 'uses' => 'RegionController@store']);
                Route::get('/', ['as' => 'index', 'uses' => 'RegionController@index']);
            });

            Route::group(['prefix' => 'project', 'as' => 'project.'], function(){

                Route::post('edit/{id}', ['as' => 'update', 'uses' => 'ProjectController@update']);
                Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'ProjectController@edit']);

                Route::post('/create', ['as' => 'store', 'uses' => 'ProjectController@store']);
                Route::get('/', ['as' => 'index', 'uses' => 'ProjectController@index']);
            });

            Route::group(['prefix' => 'district', 'as' => 'district.'], function(){
                Route::post('edit/{id}', ['as' => 'update', 'uses' => 'DistrictController@update']);
                Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'DistrictController@edit']);

                Route::post('/create', ['as' => 'store', 'uses' => 'DistrictController@store']);
                Route::get('/create', ['as' => 'create', 'uses' => 'DistrictController@create']);
                Route::get('/', ['as' => 'index', 'uses' => 'DistrictController@index']);
            });


            Route::group(['prefix' => 'upazila', 'as' => 'upazila.'], function(){

                Route::post('edit/{id}', ['as' => 'update', 'uses' => 'UpazilaController@update']);
                Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'UpazilaController@edit']);

                Route::post('/create', ['as' => 'store', 'uses' => 'UpazilaController@store']);
                Route::get('/create', ['as' => 'create', 'uses' => 'UpazilaController@create']);
                Route::get('/', ['as' => 'index', 'uses' => 'UpazilaController@index']);
            });

            Route::group(['prefix' => 'union', 'as' => 'union.'], function(){

                Route::post('/edit/{id}', ['as' => 'update', 'uses' => 'UnionController@update']);
                Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'UnionController@edit']);

                Route::post('/create', ['as' => 'store', 'uses' => 'UnionController@store']);
                Route::get('/create', ['as' => 'create', 'uses' => 'UnionController@create']);
                Route::get('/', ['as' => 'index', 'uses' => 'UnionController@index']);
            });
        });


        Route::group(['namespace' => 'MonthlyReport'], function() {
            Route::get('/period/show',    ['as' => 'periods.show', 'uses' => 'PeriodController@show']);
            Route::get('/periods/period', ['as' => 'periods.get-period', 'uses' => 'PeriodController@getPeriods']);
            Route::get('/periods',        ['as' => 'periods.index', 'uses' => 'PeriodController@index']);
            Route::get('/periods/monthlyrep',        ['as' => 'periods.monthlyrep', 'uses' => 'PeriodController@getMonthlyRep']);
        });

        Route::group(['namespace' => 'WaterSanitation'], function() {
            Route::get('water_sanitation/print',    ['as' => 'water_sanitation.print',    'uses' => 'WaterSanitationController@print']);
            Route::get('/water_sanitation/download',['as' => 'water_sanitation.download', 'uses' => 'WaterSanitationController@download']);
            Route::get('/water_sanitation/search',  ['as' => 'water_sanitation.search',   'uses' => 'WaterSanitationController@search']);

            Route::resource('/water_sanitation', 'WaterSanitationController');
        });

       Route::group(['namespace' => 'WaterHousehold'], function() {
            Route::get('water_household/print',    ['as' => 'water_household.print',    'uses' => 'WaterHouseholdController@print']);
            Route::get('/water_household/download',['as' => 'water_household.download', 'uses' => 'WaterHouseholdController@download']);
            Route::get('/water_household/search',  ['as' => 'water_household.search',   'uses' => 'WaterHouseholdController@search']);

            Route::resource('/water_household', 'WaterHouseholdController');
        });
        
        Route::group(['namespace' => 'Water'], function() {

            Route::group(['prefix' => 'ajax', 'as' => 'water.ajax.'], function(){
              Route::get('/unions', ['as' => 'union', 'uses' => 'WaterAjaxController@getUnions']);
              Route::get('/upazilas', ['as' => 'upazila', 'uses' => 'WaterAjaxController@getUpazilas']);
            });

            Route::get('water/print',    ['as' => 'water.print',    'uses' => 'WaterController@print']);
            Route::get('water/download', ['as' => 'water.download', 'uses' => 'WaterController@download']);
            Route::get('/water/search',  ['as' => 'water.search',   'uses' => 'WaterController@search']);
                Route::get('/water/printdata', ['as' => 'water.printdata', 'uses' => 'WaterController@printdata']);

            Route::resource('/water', 'WaterController');
        });

        Route::group(['namespace' => 'Finance'], function() {
            Route::get('/finance/report/print', ['as' => 'finance.report.print', 'uses' => 'ReportController@print']);
            Route::get('/finance/report', ['as' => 'finance.report.index', 'uses' => 'ReportController@index']);
            
            
               //------------------MDemand Adding----------------
               Route::group(['namespace' => 'UPDemandM', 'prefix' => 'up-demand-m', 'as' => 'up-demand-m.'], function(){

                // Route::get('finace_demand/print', ['uses' => 'DemandMController@print', 'as' => 'finace_demand.print']);
                 Route::resource('/survey_details', 'DemandMController');
                 Route::resource('/survey_list', 'FundListMController');
                 // Route::resource('/print_data', 'FundListMController');
                 Route::get('print_data', ['uses' => 'FundListMController@print', 'as' => 'print_data.print']);
                 
                 });

        });

        Route::group(['namespace' => 'SubProject', 'prefix' => 'sub-projects', 'as' => 'sub-projects.'], function(){

            // Route::group(['prefix' => 'sanitation', 'as' => 'sanitation.'], function(){
            //     Route::get('get-download/{type}',   ['as' => 'get-dl', 'uses' => 'DownloadController@index']);
            //     Route::get('/summary', ['as' => 'summary', 'uses' => 'SanitationController@summary']);

            //     Route::post('/edit/{id}', ['as' => 'update', 'uses' => 'SanitationController@update']);
            //     Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'SanitationController@edit']);
            //     Route::get('/', ['as' => 'index', 'uses' => 'SanitationController@index']);
            // });

            Route::group(['prefix' => 'water', 'as' => 'waters.'], function(){

                Route::get('/download/{type}',   ['as' => 'download', 'uses' => 'WaterController@download']);
                Route::get('/approval-summary',  ['as' => 'approval', 'uses' => 'WaterController@approval']);
                Route::get('/unions-summary',    ['as' => 'unions', 'uses' => 'WaterController@unions']);
                Route::get('/districts-summary', ['as' => 'districts', 'uses' => 'WaterController@districts']);
                Route::get('/regions-summary',   ['as' => 'regions', 'uses' => 'WaterController@regions']);
                Route::get('/',                  ['as' => 'index', 'uses' => 'WaterController@index']);
            });
            
          
           Route::group(['prefix' => 'sanitation', 'as' => 'sanitation.'], function(){

                Route::get('/download/{type}',   ['as' => 'download', 'uses' => 'SanitationController@download']);
                Route::get('/approval-summary',  ['as' => 'approval', 'uses' => 'SanitationController@approval']);
                Route::get('/unions-summary',    ['as' => 'unions', 'uses' => 'SanitationController@unions']);
                Route::get('/districts-summary', ['as' => 'districts', 'uses' => 'SanitationController@districts']);
                Route::get('/regions-summary',   ['as' => 'regions', 'uses' => 'SanitationController@regions']);
                Route::get('/',                  ['as' => 'index', 'uses' => 'SanitationController@index']);
            });
            
               Route::group(['prefix' => 'house_sanitation', 'as' => 'house_sanitation.'], function(){

                Route::get('/download/{type}',   ['as' => 'download', 'uses' => 'HouseSanitationController@download']);
                Route::get('/approval-summary',  ['as' => 'approval', 'uses' => 'HouseSanitationController@approval']);
                Route::get('/unions-summary',    ['as' => 'unions', 'uses' => 'HouseSanitationController@unions']);
                Route::get('/districts-summary', ['as' => 'districts', 'uses' => 'HouseSanitationController@districts']);
                Route::get('/regions-summary',   ['as' => 'regions', 'uses' => 'HouseSanitationController@regions']);
                Route::get('/',                  ['as' => 'index', 'uses' => 'HouseSanitationController@index']);
            });
            
        });

        Route::group(['namespace' => 'FundList', 'prefix' => 'fundlist', 'as' => 'fundlist.'], function(){

            Route::group(['namespace' => 'ChartsOfAccount', 'prefix' => 'charts-of-account', 'as' => 'charts-of-account.'], function(){

                Route::group(['prefix' => 'heads', 'as' => 'heads.'], function(){
                    Route::post('/update/{id}', ['as' => 'update', 'uses' => 'HeadController@update']);
                    Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'HeadController@edit']);
                    Route::post('/store', ['as' => 'store', 'uses' => 'HeadController@store']);
                    Route::get('/', ['as' => 'index', 'uses' => 'HeadController@index']);
                });

                Route::group(['prefix' => 'sub-heads', 'as' => 'sub-heads.'], function(){
                    Route::post('/update/{id}', ['as' => 'update', 'uses' => 'SubHeadController@update']);
                    Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'SubHeadController@edit']);
                    Route::post('/store', ['as' => 'store', 'uses' => 'SubHeadController@store']);
                    Route::get('/', ['as' => 'index', 'uses' => 'SubHeadController@index']);
                });

                Route::group(['prefix' => 'items', 'as' => 'items.'], function(){
                    Route::post('/update/{id}', ['as' => 'update', 'uses' => 'ItemController@update']);
                    Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'ItemController@edit']);
                    Route::post('/store', ['as' => 'store', 'uses' => 'ItemController@store']);
                    Route::get('/', ['as' => 'index', 'uses' => 'ItemController@index']);
                });
            });

            Route::group(['namespace' => 'DisbursmentToUP', 'prefix' => 'disbursment-to-up', 'as' => 'disbursment-to-up.'], function(){

                Route::group(['prefix' => 'disbursment', 'as' => 'disbursment.'], function(){

                    Route::post('/update/{id}', ['as' => 'update', 'uses' => 'DisbursmentController@update']);
                    Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'DisbursmentController@edit']);

                    Route::post('/store', ['as' => 'store', 'uses' => 'DisbursmentController@store']);

                    Route::get('/create', ['as' => 'create', 'uses' => 'DisbursmentController@create']);
                    Route::get('/', ['as' => 'index', 'uses' => 'DisbursmentController@index']);
                });

                Route::group(['prefix' => 'fund-disbursment', 'as' => 'fund-disbursment.'], function(){
                    Route::get('/entry-track-by-up', ['as' => 'index2', 'uses' => 'FundDisbursmentController@index2']);
                    Route::get('/entry-track-by-date', ['as' => 'index', 'uses' => 'FundDisbursmentController@index']);
                });
            });

            Route::group(['namespace' => 'Report', 'prefix' => 'report', 'as' => 'report.'], function(){

                Route::group(['prefix' => 'analysis-by-project-date-up', 'as' => 'analysis.'], function(){
                    Route::get('/', ['as' => 'index', 'uses' => 'AnalysisByProjectDateUPController@index']);
                });

                Route::group(['prefix' => 'consolidated-report', 'as' => 'consolidated.'], function(){
                    Route::get('/', ['as' => 'index', 'uses' => 'ConsolidatedReportController@index']);
                });

                Route::group(['prefix' => 'district-report', 'as' => 'district.'], function(){
                    Route::get('/', ['as' => 'index', 'uses' => 'DistrictReportController@index']);
                });

                Route::group(['prefix' => 'project-report', 'as' => 'project.'], function(){
                    Route::get('/', ['as' => 'index', 'uses' => 'ProjectReportController@index']);
                });

                Route::group(['prefix' => 'summary-by-union-report', 'as' => 'summary.'], function(){
                    Route::get('/', ['as' => 'index', 'uses' => 'SummaryByUnionReportController@index']);
                });

                Route::group(['prefix' => 'union-report', 'as' => 'union.'], function(){
                    Route::get('/', ['as' => 'index', 'uses' => 'UnionReportController@index']);
                });

                Route::group(['prefix' => 'upazila-report', 'as' => 'upazila.'], function(){
                    Route::get('/', ['as' => 'index', 'uses' => 'UpazilaReportController@index']);
                });

                Route::group(['prefix' => 'common-report', 'as' => 'common.'], function(){
                    Route::get('/', ['as' => 'index', 'uses' => 'CommonReportController@index']);
                });

            });

            Route::group(['namespace' => 'UPBudget', 'prefix' => 'up-budget', 'as' => 'up-budget.'], function(){

                Route::post('/update/{id}', ['as' => 'update', 'uses' => 'UPBudgetController@update']);
                Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'UPBudgetController@edit']);
                Route::post('/store', ['as' => 'store', 'uses' => 'UPBudgetController@store']);
                Route::get('/create', ['as' => 'create', 'uses' => 'UPBudgetController@create']);
                Route::get('/', ['as' => 'index', 'uses' => 'UPBudgetController@index']);

            });

            Route::group(['namespace' => 'UPDataEdit', 'prefix' => 'up-data-edit', 'as' => 'up-data-edit.'], function(){

                Route::group(['prefix' => 'demand', 'as' => 'demand.'], function(){
                    Route::get('/', ['as' => 'index', 'uses' => 'DemandController@index']);
                });

                Route::group(['prefix' => 'expenditure', 'as' => 'expenditure.'], function(){
                    Route::get('/', ['as' => 'index', 'uses' => 'ExpenditureController@index']);
                });

                Route::group(['prefix' => 'income', 'as' => 'income.'], function(){
                    Route::get('/', ['as' => 'index', 'uses' => 'IncomeController@index']);
                });
            });

            //------------------Demand Adding----------------
            Route::group(['namespace' => 'UPDemand', 'prefix' => 'up-demand', 'as' => 'up-demand.'], function(){

            Route::get('finace_demand/print', ['uses' => 'DemandController@print', 'as' => 'finace_demand.print']);
            Route::resource('/finace_demand', 'DemandController');

            Route::resource('/fund_list', 'FundListController');
            
            Route::post('/store_comment', ['as' => 'store_comment', 'uses' => 'DemandController@store_comment']);



            Route::get('getSubItem', ['as' => 'income.getSubItem', 'uses' => 'IncomeController@subitem']);
            Route::get('getSubhead', ['as' => 'income.getSubhead', 'uses' => 'IncomeController@subhead']);

            Route::get('balancedisbursement', ['as' => 'balancedisbursement', 'uses' => 'DisbursementController@index']);

            Route::get('/finance/report/print', ['as' => 'finance.report.print', 'uses' => 'ReportController@print']);
            Route::get('/finance/report', ['as' => 'finance.report.index', 'uses' => 'ReportController@index']);

            Route::resource('/finace_income', 'IncomeController');
            Route::resource('/finace_expenditure', 'ExpenditureController');

            Route::resource('/finace_demand', 'DemandController');
            Route::resource('/finace_demand/report', 'DemandController');
            Route::resource('/finace_bankstatement', 'BankStatementController');
            
            });


        });
          

        Route::group(['prefix' => 'download', 'as' => 'download.'], function(){
            Route::get('/', ['as' => 'index', 'uses' => 'DownloadController@index']);
        });

        Route::group(['namespace' => 'MobileApp'], function(){

            Route::group(['prefix' => 'mobile-app', 'as' => 'mobile-app.'], function(){
                Route::get('/show/{id}', ['as' => 'show', 'uses' => 'MobileAppController@show']);
                Route::get('/delete/{id}', ['as' => 'delete', 'uses' => 'MobileAppController@delete']);
                Route::get('/', ['as' => 'index', 'uses' => 'MobileAppController@index']);
                Route::get('/user_report', ['as' => 'userrep', 'uses' => 'MobileAppController@getUserRep']);
            });
        });


        Route::get('/', ['as' => 'dashboard', 'uses' => 'DashboardController@dashboard']);
    });

});