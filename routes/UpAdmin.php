<?php

use App\Model\WaterSummary;

Route::group(['middleware' => ['auth', 'role:up_admin']], function() {

    Route::group([
    'namespace' => 'UpAdmin',
    'as' => 'up-admin.',
    'prefix' => 'up'], function(){

        Route::group(['namespace' => 'MonthlyReport'], function() {
            Route::get('entryForm', ['uses' => 'MonthlyReportController@entryForm', 'as' => 'entryForm']);
            Route::post('entryFormStore/{id}', ['uses' => 'MonthlyReportController@entryFormStore', 'as' => 'entryFormStore']);

            Route::post('setReportMonthStore/{id}', ['uses' => 'MonthlyReportController@setReportMonthStore', 'as' => 'setReportMonthStore']);
            Route::get('setReportMonth', ['uses' => 'MonthlyReportController@setReportMonth', 'as' => 'setReportMonth']);

            Route::get('/print', ['as' => 'monthly-report.print', 'uses' => 'MonthlyReportController@print']);

            Route::resource('/report_period', 'MonthlyReportController');
        });

        Route::group(['namespace' => 'WaterSanitation'], function() {
            Route::get('water_sanitation/print',    ['as' => 'water_sanitation.print',    'uses' => 'WaterSanitationController@print']);
            Route::get('/water_sanitation/download',['as' => 'water_sanitation.download', 'uses' => 'WaterSanitationController@download']);
            Route::get('/water_sanitation/search',  ['as' => 'water_sanitation.search',   'uses' => 'WaterSanitationController@search']);

            Route::resource('/water_sanitation', 'WaterSanitationController');
        });

       Route::group(['namespace' => 'WaterHousehold'], function() {
            Route::get('water_household/print',    ['as' => 'water_household.print',    'uses' => 'WaterHouseholdController@print']);
            Route::get('/water_household/download',['as' => 'water_household.download', 'uses' => 'WaterHouseholdController@download']);
            Route::get('/water_household/search',  ['as' => 'water_household.search',   'uses' => 'WaterHouseholdController@search']);

            Route::resource('/water_household', 'WaterHouseholdController');
        });
        
        Route::group(['namespace' => 'Water'], function() {
            Route::get('water/print', ['uses' => 'WaterController@print', 'as' => 'water.print']);
            Route::get('water/download', ['uses' => 'WaterController@download', 'as' => 'water.download']);
            Route::get('/water/search', ['as' => 'water.search', 'uses' => 'WaterController@search']);

            Route::resource('/water', 'WaterController');
        });

        Route::group(['namespace' => 'Finance'], function() {
            Route::get('getSubItem', ['as' => 'income.getSubItem', 'uses' => 'IncomeController@subitem']);
            Route::get('getSubhead', ['as' => 'income.getSubhead', 'uses' => 'IncomeController@subhead']);

            Route::get('balancedisbursement', ['as' => 'balancedisbursement', 'uses' => 'DisbursementController@index']);

            Route::get('/finance/report/print', ['as' => 'finance.report.print', 'uses' => 'ReportController@print']);
            Route::get('/finance/report', ['as' => 'finance.report.index', 'uses' => 'ReportController@index']);

            Route::resource('/finace_income', 'IncomeController');
            Route::resource('/finace_expenditure', 'ExpenditureController');

            Route::get('finace_demand/print', ['uses' => 'DemandController@print', 'as' => 'finace_demand.print']);
            Route::resource('/finace_demand', 'DemandController');
            Route::resource('/finace_demand/report', 'DemandController');
            Route::resource('/finace_bankstatement', 'BankStatementController');

            Route::resource('/fund_list', 'FundListController');

        });

        Route::group(['namespace' => 'Training',  'prefix' => 'participants', 'as' => 'participants.'], function(){
            Route::get('/', ['as' => 'index', 'uses' => 'TrainingController@index']);
        });

        Route::group(['namespace' => 'Procurement'], function() {
            Route::resource('/tenderEvaluationComittee', 'TenderEvaluationComitteeController');
            Route::resource('/proposalEvaluationComittee', 'ProposalEvaluationComitteeController');
            Route::resource('/procurementEntry', 'ProcurementEntryController');
            Route::resource('/ProcurementEvaluationEntry', 'ProcurementEvaluationEntryController');
        });

        Route::group(['namespace' => 'UP', 'as' => 'up.'], function(){
            Route::group(['prefix' => 'up-basic-info', 'as' => 'basic-info.'], function(){
                Route::post('/update', ['as' => 'update', 'uses' => 'BasicInfoController@update']);
                Route::get('/', ['as' => 'index', 'uses' => 'BasicInfoController@index']);
            });

            Route::group(['prefix' => 'functionaries', 'as' => 'functionaries.'], function(){
                Route::post('/update/{id}', ['as' => 'update', 'uses' => 'FunctionariesController@update']);
                Route::post('/store', ['as' => 'store', 'uses' => 'FunctionariesController@store']);
                Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'FunctionariesController@edit']);
                Route::get('/', ['as' => 'index', 'uses' => 'FunctionariesController@index']);
            });

            Route::group(['prefix' => 'staff', 'as' => 'staff.'], function(){
                Route::post('/update/{id}', ['as' => 'update', 'uses' => 'UPStuffController@update']);
                Route::post('/store', ['as' => 'store', 'uses' => 'UPStuffController@store']);
                Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'UPStuffController@edit']);
                Route::get('/', ['as' => 'index', 'uses' => 'UPStuffController@index']);
            });
        });

        Route::group(['prefix' => 'download', 'as' => 'download.'], function(){
            Route::get('/', ['as' => 'index', 'uses' => 'DownloadController@index']);
        });

        Route::group(['namespace' => 'MobileApp'], function(){
            Route::group(['prefix' => 'mobile-app', 'as' => 'mobile-app.'], function(){
                Route::get('/show/{id}', ['as' => 'show', 'uses' => 'MobileAppController@show']);
                Route::get('/', ['as' => 'index', 'uses' => 'MobileAppController@index']);
                Route::get('/print', ['as' => 'print', 'uses' => 'MobileAppController@print']);
            });

            Route::group(['prefix' => 'cdfs', 'as' => 'cdfs.'], function(){
                Route::post('/update/{id}', ['as' => 'update', 'uses' => 'CDFsController@update']);
                Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'CDFsController@edit']);
                Route::post('/store', ['as' => 'store', 'uses' => 'CDFsController@store']);
                Route::get('/', ['as' => 'index', 'uses' => 'CDFsController@index']);
            });
        });


        Route::group(['namespace' => 'UserManagement', 'prefix' => 'user-management', 'as' => 'user-management.'], function(){
            Route::get('/password-change', ['as' => "password-change", 'uses' => 'PasswordController@get']);
            Route::post('/password-change', ['as' => "password-change.post", 'uses' => 'PasswordController@change']);
        });

        Route::get('/', ['as' => 'dashboard', 'uses' => 'DashboardController@dashboard']);
    });
});