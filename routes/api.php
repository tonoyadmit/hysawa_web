<?php

Route::group(['prefix' => 'v1'], function(){

    Route::group(['middleware' => 'jwt.auth'], function(){

      Route::post('/report_list', 'ReportController@reportList');

      Route::post('/specific_report', 'ReportController@specificReport');
      Route::post('/specific_report_item_details', 'ReportController@reportItem');

      Route::post('/get-data', 'DataController@get');

      Route::post('/water-sanitation/show', 'WaterSanitationController@show');
      Route::post('/cdf-no/show', 'CDFNoController@show');

      Route::post('/questions/upload', 'QuestionController@update');
      Route::get('/questions/{project_id}/{type}', 'QuestionController@index');

      Route::get('/projects', 'ProjectController@index');

      Route::post('/logout', 'AuthController@logout');
    });

    Route::post('/login', 'AuthController@login');
    Route::post('/token', 'AuthController@token');

    Route::get('/hi-server', function(){
      return response()->json([ 'data' => 'Current server time is: '.time()], 200);
    });
});