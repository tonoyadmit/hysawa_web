<?php

Route::group(['middleware' => ['auth']], function() {

    Route::group([
        'namespace' => 'DefaultUser',
        'as' => 'default-user.',
        'prefix' => 'default'], function(){
          Route::get('/', ['as' => 'dashboard', 'uses' => 'DashboardController@dashboard']);
    });
});