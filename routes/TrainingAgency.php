<?php

Route::group(['middleware' => ['auth', 'role:training_agency']], function() {

    Route::group([
        'namespace' => 'TrainingAgency',
        'as' => 'training-agency.',
        'prefix' => 'training-agency'], function(){
          Route::get('/', ['as' => 'dashboard', 'uses' => 'DashboardController@dashboard']);

          Route::resource('/trainingAgency', 'TrainingAgencyController');

          Route::get('getUpzilla', ['uses' => 'TrainingParticipantController@getUpzilla', 'as' => 'getUpzilla']);
          Route::get('getUnion', ['uses' => 'TrainingParticipantController@getUnion', 'as' => 'getUnion']);
          Route::resource('/trainingParticipant', 'TrainingParticipantController');
    });
});