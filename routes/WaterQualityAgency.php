<?php

Route::group(['middleware' => ['auth', 'role:water_quality_agency' ]], function() {

    Route::group([
        'namespace' => 'WaterQualityAgency',
        'as' => 'water-quality-agency.',
        'prefix' => 'water-quality-agency'], function(){

          Route::group(['namespace' => 'Water', 'as' => 'water.', 'prefix' => 'water'], function(){

            Route::group(['prefix' => 'ajax', 'as' => 'ajax.'], function(){
              Route::get('/unions', ['as' => 'union', 'uses' => 'WaterAjaxController@getUnions']);
              Route::get('/upazilas', ['as' => 'upazila', 'uses' => 'WaterAjaxController@getUpazilas']);
            });

            Route::get('/show/{id}', ['as' => 'show', 'uses' => 'WaterController@show']);

            Route::post('/create/{id}', ['as' => 'store', 'uses' => 'WaterController@store']);
            Route::get('/create/{id}', ['as' => 'create', 'uses' => 'WaterController@create']);

            Route::post('/update/{id}', ['as' => 'update', 'uses' => 'WaterController@update']);
            Route::get('/edit/{id}', ['as' => 'edit', 'uses' => 'WaterController@edit']);

            Route::get('/printdata', ['as' => 'printdata', 'uses' => 'WaterController@printdata']);

            Route::get('/', ['as' => 'index', 'uses' => 'WaterController@index']);
          });

          Route::get('/', ['as' => 'dashboard', 'uses' => 'DashboardController@dashboard']);
    });
});