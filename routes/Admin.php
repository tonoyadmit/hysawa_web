<?php

Route::group(['middleware' => ['auth']], function() {

    Route::group([
        'namespace' => 'Admin',
        'as' => 'admin.',
        'prefix' => 'admin'], function(){


          Route::get('/', ['as' => 'dashboard', 'uses' => 'DashboardController@dashboard']);
    });

});