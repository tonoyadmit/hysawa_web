<?php

use App\Model\MobAppDataList;
use App\Model\MobAppDataListItem;
use App\Model\MobAppQuestionLists;
use App\User;
Route::auth();
Route::group(['middleware' => ['auth']], function() {







    // Route::group(['namespace' => 'WaterSanitation'], function() {
    //     Route::post('dataTableList', ['uses' => 'WaterSanitationController@dataTableList', 'as' => 'dataTableList']);
    //     Route::get('water_sanitation/downloadExcel', ['uses' => 'WaterSanitationController@downloadExcel', 'as' => 'water_sanitation/downloadExcel']);

    //     Route::get('/water_sanitation/download', ['as' => 'water_sanitation.download', 'uses' => 'WaterSanitationController@download']);
    //     Route::post('/water_sanitation/search', ['as' => 'water_sanitation.search', 'uses' => 'WaterSanitationController@search']);

    //     Route::resource('/water_sanitation', 'WaterSanitationController');
    // });

    // Route::group(['namespace' => 'Water'], function() {
    //     Route::post('dataTableList', ['uses' => 'WaterController@dataTableList', 'as' => 'dataTableList']);
    //     Route::get('water/downloadExcel', ['uses' => 'WaterController@downloadExcel', 'as' => 'water/downloadExcel']);

    //     Route::get('water/download', ['uses' => 'WaterController@download', 'as' => 'water.download']);
    //     Route::post('/water/search', ['as' => 'water.index.post', 'uses' => 'WaterController@search']);

    //     Route::resource('/water', 'WaterController');
    // });

    // Route::group(['namespace' => 'User'], function() {
    //     Route::resource('/user', 'UserController');
    //     Route::get('/users/data', 'UserController@userList');
    //     Route::resource('/role', 'RoleController');
    //     Route::resource('/permission', 'PermissionController');
    // });
    //


});

Route::get('/', function () {
    if (Auth::check()) {

        $roles = \Auth::user()->roles;

        if(!count($roles))
        {
           return redirect()->route('login');
        }

        $role = $roles->first()->name;
        //\Log::info($role);

        if($role == "superadministrator"){
            return redirect()->route('superadmin.dashboard');
        }elseif($role == "district_admin"){
            return redirect()->route('district-admin.dashboard');
        }elseif($role == "upazila_admin"){
            return redirect()->route('upazila-admin.dashboard');
        }elseif($role == "up_admin"){
            return redirect()->route('up-admin.dashboard');
        }

        elseif($role == "administrator"){
            return redirect()->route('admin.dashboard');
        }elseif($role == "training_agency"){
            return redirect()->route('training-agency.dashboard');
        }elseif($role == "water_quality_agency"){
            return redirect()->route('water-quality-agency.dashboard');
        }elseif($role == "default_user"){
            return redirect()->route('default-user.dashboard');
        }

        return redirect()->route('default-user.dashboard');
    }
    return redirect()->route('login');
});

Route::get('logout', function (){
    \Auth::logout();
    return redirect('/');
});

//Route::get('/home', 'HomeController@index');


Route::get('/seed-data', function(){




    // $list = [3,15,27,39,51,63];

    // foreach(range(1, 85) as $id){
    //     $old = [];
    //     $rep_data = \DB::table('rep_period')->find($id);

    //     if(count($rep_data))
    //     {
    //       $d = explode(' ', $rep_data->period);

    //       if(count($d) >= 2)
    //       {
    //         $old['starting_date'] = date('Y-m-d', strtotime($d[1]." 01, ".$d[0]));
    //         $old['ending_date']   = date('Y-m-t', strtotime($old['starting_date']));
    //       }
    //     }

    //     \Log::info($old);

    // }




    // $img1 = "upload/unimage/1514481031_1.jpg";
    // $img2 = "upload/unimage/1514481031_1.jpg";
    // $img3 = "upload/unimage/1514481031_1.jpg";

    // $lat  = "23.7466304";
    // $long = "90.3976463";


    // $userList = [141, 142, 143, 144, 145, 146];

    // foreach($userList as $userId)
    // {
    //     $user = User::find($userId);

    //     foreach(range(1, 365) as $index)
    //     {
    //         $type = ['Event', 'Water', 'Sanitation'];
    //         $group_id = rand(1,3);
    //         $questions = MobAppQuestionLists::where('type', $type[$group_id -1])->orderBy('id')->get();

    //         $mData = new MobAppDataList;
    //         $mData->user_id = $user->id;
    //         $mData->distid = $user->distid;
    //         $mData->upid = $user->upid;
    //         $mData->unid = $user->unid;
    //         $mData->proj_id = $user->proj_id;
    //         $mData->region_id = $user->region_id;
    //         $mData->latitude = $lat;
    //         $mData->longitude = $long;

    //         $mData->image1 = $img1;
    //         $mData->image2 = $img2;
    //         $mData->image3 = $img3;
    //         $mData->group_id = $group_id;
    //         $mData->type = $type[$group_id-1];
    //         $mData->project_id = $user->proj_id;

    //         $mData->answers = "";
    //         $mData->created_at = date('Y-m-d H:i:s', strtotime("-$index day", time()));
    //         $mData->save();

    //         $qjson = [];

    //         foreach($questions as $q)
    //         {
    //             $ans = "";
    //             $multiple = false;

    //             if($q->input_type == "text"){
    //                 $ans = rand(100,6000);
    //                 MobAppDataListItem::insert([
    //                       'mobile_app_data_list_id' => $mData->id,
    //                       'question_id'             => $q->id,
    //                       'value'                   => $ans,
    //                       'created_at'              => $mData->created_at,
    //                       'updated_at'              => $mData->created_at
    //                     ]);

    //             }elseif($q->input_type == "number"){
    //                 $ans = rand(20,150);
    //                 MobAppDataListItem::insert([
    //                   'mobile_app_data_list_id' => $mData->id,
    //                   'question_id'             => $q->id,
    //                   'value'                   => $ans,
    //                   'created_at'              => $mData->created_at,
    //                   'updated_at'              => $mData->created_at
    //                 ]);
    //             }elseif($q->input_type == "select_single"){

    //                 $opts = explode(',', $q->options);
    //                 $optPos = rand(0, count($opts)-1);

    //                 $ans = $opts[$optPos];
    //                 MobAppDataListItem::insert([
    //                   'mobile_app_data_list_id' => $mData->id,
    //                   'question_id'             => $q->id,
    //                   'value'                   => $ans,
    //                   'created_at'              => $mData->created_at,
    //                   'updated_at'              => $mData->created_at
    //                 ]);

    //             }elseif($q->input_type == "select_multiple"){
    //                 $multiple = true;
    //                 $opts = explode(',', $q->options);
    //                 $total = rand(2, count($opts));
    //                 $ansArrKey = array_rand($opts, $total);
    //                 //\Log::info();
    //                 //\Log::info($ansArrKey);

    //                 $first = false;
    //                 foreach($ansArrKey as $i)
    //                 {
    //                     if(!$first){
    //                         $first = true;
    //                     }else{
    //                         $ans = $ans.",";
    //                     }

    //                     $ans = $ans.$opts[$i];

    //                     MobAppDataListItem::insert([
    //                       'mobile_app_data_list_id' => $mData->id,
    //                       'question_id'             => $q->id,
    //                       'value'                   => $opts[$i],
    //                       'created_at'              => $mData->created_at,
    //                       'updated_at'              => $mData->created_at
    //                     ]);
    //                 }
    //             }

    //             $qjson[] = ['answer' => $ans, 'id' => $q->id];
    //         }

    //         $mData->answers = json_encode($qjson);
    //         $mData->update();
    //     }
    // }



















    // $data = [];
    // $sl = 39;

    // $month = 2;
    // $year = 2016;

    // foreach(range(1, 47) as $index)
    // {
    //     $monthName = date("F", mktime(0, 0, 0, $month++, 10));
    //     $data[] = ['id' => $sl, 'rep_id' => $sl++, 'period' => $year.' '. $monthName];
    //     if($month == 13){
    //         $month = 1;
    //         $year++;
    //     }
    // }

    // // dd($data);

    // \DB::table('rep_period')->insert($data);









    // 'type',
    // 'title',
    // 'input_type',
    // 'options'

    // $data = [
    //     ['title' => 'CDF No/School No', 'type' => 'event', 'input_type' => 'text', 'created_at' => date('Y-m-d H:i:s'), 'options' => ""],

    //     ['title' => 'Type', 'type' => 'event', 'input_type' => 'select_single', 'created_at' => date('Y-m-d H:i:s'),
    //         'options' => 'Hand Wash,Latrine Maintenance,Garbage Disposal,Menstrual Hygiene,Water Safety,Food Hygiene,Climate Change Awareness,Volunteer Orientation'],

    //     ['title' => 'Location', 'type' => 'event', 'input_type' => 'select_single', 'options' => 'Community,School,UP', 'created_at' => date('Y-m-d H:i:s')],

    //     ['title' => 'Nos of Men/Boy', 'type' => 'event', 'input_type' => 'number', 'created_at' => date('Y-m-d H:i:s'), 'options' => ""],

    //     ['title' => 'Nos of Women/Girl', 'type' => 'event', 'input_type' => 'number', 'created_at' => date('Y-m-d H:i:s'), 'options' => ""],

    //     ['title' => 'Nos of Disabled', 'type' => 'event', 'input_type' => 'number', 'created_at' => date('Y-m-d H:i:s'), 'options' => ""],

    //     ['title' => 'ID No', 'type' => 'water', 'input_type' => 'text', 'created_at' => date('Y-m-d H:i:s'), 'options' => ""],

    //     ['title' => 'Type', 'type' => 'water', 'input_type' => 'select_single',
    //         'options' => 'Water - TW,Water - Sky H,Water - RO,Water - RWH,School Latrine,Public Latrine', 'created_at' => date('Y-m-d H:i:s')],

    //     ['title' => 'Functionality', 'type' => 'water', 'input_type' => 'select_single',
    //         'options' => 'Non Functional,Function,Functional with problems', 'created_at' => date('Y-m-d H:i:s')],

    //     ['title' => 'Problem Type', 'type' => 'water', 'input_type' => 'select_multiple',
    //         'options' => 'High Saline,High Iron,Platform Broken,Dirty,Maintenance Issue', 'created_at' => date('Y-m-d H:i:s')],

    //     ['title' => 'Problem Type', 'type' => 'water', 'input_type' => 'select_multiple',
    //         'options' => 'Major Repair,Minor repair,Awareness,Improved Management', 'created_at' => date('Y-m-d H:i:s')],


    //     ['title' => 'ID No', 'type' => 'sanitation', 'input_type' => 'text', 'created_at' => date('Y-m-d H:i:s'), 'options' => ""],

    //     ['title' => 'Type', 'type' => 'sanitation', 'input_type' => 'select_single',
    //         'options' => 'Water - TW,Water - Sky H,Water - RO,Water - RWH,School Latrine,Public Latrine', 'created_at' => date('Y-m-d H:i:s')],

    //     ['title' => 'Functionality', 'type' => 'sanitation', 'input_type' => 'select_single',
    //         'options' => 'Non Functional,Function,Functional with problems', 'created_at' => date('Y-m-d H:i:s')],

    //     ['title' => 'Problem Type', 'type' => 'sanitation', 'input_type' => 'select_multiple',
    //         'options' => 'High Saline,High Iron,Platform Broken,Dirty,Maintenance Issue', 'created_at' => date('Y-m-d H:i:s')],

    //     ['title' => 'Problem Type', 'type' => 'sanitation', 'input_type' => 'select_multiple',
    //         'options' => 'Major Repair,Minor repair,Awareness,Improved Management', 'created_at' => date('Y-m-d H:i:s')]
    // ];

    // App\Model\MobAppQuestionLists::insert($data);



    // $cdfNo = 101;
    // $distId = 7;
    // $upid = 16;
    // $union_id = 462;

    // foreach(range(1, 10) as $index)
    // {
    //     App\Model\CDFNo::insert([
    //         'cdf_no' => $cdfNo++,
    //         'district_id' => $distId,
    //         'upazila_id' => $upid,
    //         'union_id' => $union_id,
    //         'village' => "Village ".rand(1000,2000),
    //         'area' => "Para ". rand(1000, 2000),
    //         'created_by' => 1,
    //         'created_at' => date('Y-m-d H:i:s')

    //         ]);
    // }


    // $content = [
        // ['category' => 'CDF Information'],
        //     ['parent_id' => 1, 'sl' => 1, 'question' => 'Number of CDF:',                  'field' => 'cdf_no'],
        //     ['parent_id' => 1, 'sl' => 2, 'question' => 'Poulation under CDF:',            'field' => 'cdf_pop'],

        //     ['parent_id' => 1, 'sl' => 3, 'question' => 'Male population:',                'field' => 'cdf_male'],
        //     ['parent_id' => 1, 'sl' => 4, 'question' => 'Female population:',              'field' => 'cdf_female'],
        //     ['parent_id' => 1, 'sl' => 5, 'question' => 'Hardcode population:',            'field' => 'cdf_pop_hc'],
        //     ['parent_id' => 1, 'sl' => 6, 'question' => 'Number of household:',            'field' => 'cdf_hh'],
        //     ['parent_id' => 1, 'sl' => 7, 'question' => 'Number of hardcore household:',   'field' => 'cdf_hh_hc'],
        //     ['parent_id' => 1, 'sl' => 8, 'question' => 'Number of disable people:',       'field' => 'cdf_pop_disb'],

        //     ['parent_id' => 1, 'sl' => 9,  'question' => 'Number of people under social safetynet',       'field' => 'cdf_pop_safety'],
        //     ['parent_id' => 1, 'sl' => 10, 'question' => 'Number of Community Facilitators identified:',       'field' => 'cdf_cf_tot'],
        //     ['parent_id' => 1, 'sl' => 11, 'question' => 'Number of MALE Community Facilitators identified:',       'field' => 'cdf_cf_male'],
        //     ['parent_id' => 1, 'sl' => 12, 'question' => 'Number of FEMALE Community Facilitators identified',       'field' => 'cdf_cf_female'],


         // ['category' => 'UP Management Information'],
            // ['parent_id' => 2, 'sl' => 1, 'question' => 'Number of PNGO/Project staff recruited',              'field' => 'up_stf_tot'],
            // ['parent_id' => 2, 'sl' => 2, 'question' => 'Number of Male PNGO/Project staff recruited',         'field' => 'up_stf_male'],
            // ['parent_id' => 2, 'sl' => 3, 'question' => 'Number of Female PNGO/Project staff recruited',       'field' => 'up_stf_female'],
            // ['parent_id' => 2, 'sl' => 4, 'question' => 'Number of PNGO engaged by UP',                        'field' => 'up_pngo'],
            // ['parent_id' => 2, 'sl' => 5, 'question' => 'Number of hardware contratctor engaged by UP',        'field' => 'up_cont'],

        // ['category' => 'CDF HYGIENE Information'],
            // ['parent_id' => 3, 'sl' => 1, 'question' => 'Number of handwashing sessions',                          'field' => 'CHY_hw_ses'],
            // ['parent_id' => 3, 'sl' => 2, 'question' => 'Number of MALE participated in handwashing sessions',     'field' => 'CHY_hw_male'],
            // ['parent_id' => 3, 'sl' => 3, 'question' => 'Number of FEMALE participated in handwashing sessions',   'field' => 'CHY_hw_female'],
            // ['parent_id' => 3, 'sl' => 4, 'question' => 'Number of menstrual hygiene sessions',              'field' => 'CHY_mn_ses'],
            // ['parent_id' => 3, 'sl' => 5, 'question' => 'Number of FEMALE participated inmenstrual hygiene sessions', 'field' => 'CHY_mn_female'],

            // ['parent_id' => 3, 'sl' => 6, 'question' => 'Number of hygienic latrine sessions',           'field' => 'CHY_sa_ses'],
            // ['parent_id' => 3, 'sl' => 7, 'question' => 'Number of household participated in hygienic latrine sessions','field' => 'CHY_sa_hh'],
            // ['parent_id' => 3, 'sl' => 8, 'question' => 'Number of Food hygiene sessions',           'field' => 'CHY_fh_ses'],
            // ['parent_id' => 3, 'sl' => 9, 'question' => 'Number of household participated in Food hygiene sessions',   'field' => 'CHY_fh_hh'],
            // ['parent_id' => 3, 'sl' => 10, 'question' => 'Number of household attended TW Maintenance sessions',       'field' => 'TW_maintenance'],

            // ['parent_id' => 3, 'sl' => 11, 'question' => 'Number of household attended WSP sessions',              'field' => 'WSP'],
            // ['parent_id' => 3, 'sl' => 12, 'question' => 'Number of new garbage hole build',             'field' => 'CHY_gb_new'],
            // ['parent_id' => 3, 'sl' => 13, 'question' => 'Number of  garbage hole repaired',             'field' => 'CHY_gb_rep'],
            // ['parent_id' => 3, 'sl' => 14, 'question' => 'Number of DRAMA shows in the community',       'field' => 'CHY_dr'],
            // ['parent_id' => 3, 'sl' => 15, 'question' => 'Number of people attended in DRAMA shows',     'field' => 'CHY_dr_pop'],
            // ['parent_id' => 3, 'sl' => 16, 'question' => 'Number of VIDEO shows in the community',       'field' => 'CHY_vd'],
            // ['parent_id' => 3, 'sl' => 17, 'question' => 'Number of people attended in VIDEO shows',     'field' => 'CHY_vd_pop'],

        // ['category' => 'School HYGIENE Information'],
        //     ['parent_id' => 4, 'sl' => 1, 'question' => 'Total Schools','field' => 'scl_tot'],
        //     ['parent_id' => 4, 'sl' => 2, 'question' => 'Total students','field' => 'scl_tot_std'],
        //     ['parent_id' => 4, 'sl' => 3, 'question' => 'Total boys','field' => 'scl_boys'],
        //     ['parent_id' => 4, 'sl' => 4, 'question' => 'Total girls','field' => 'scl_girls'],
        //     ['parent_id' => 4, 'sl' => 5, 'question' => 'Primary schools','field' => 'scl_pri'],

        //     ['parent_id' => 4, 'sl' => 6, 'question' => 'Number of students in Primary schools','field' => 'scl_pri_std'],
        //     ['parent_id' => 4, 'sl' => 7, 'question' => 'High schools','field' => 'scl_high'],
        //     ['parent_id' => 4, 'sl' => 8, 'question' => 'Number of students in high schools','field' => 'scl_high_std'],
        //     ['parent_id' => 4, 'sl' => 9, 'question' => 'Total Madrasha','field' => 'scl_mad'],
        //     ['parent_id' => 4, 'sl' => 10, 'question' => 'Number of students in Madrasha','field' => 'scl_mad_std'],

        //     ['parent_id' => 4, 'sl' => 11, 'question' => 'Total hygiene promotion sessions','field' => 'scl_hp_ses'],
        //     ['parent_id' => 4, 'sl' => 12, 'question' => 'Number of boys participated in hygiene promotion sessions','field' => 'scl_hp_boys'],
        //     ['parent_id' => 4, 'sl' => 13, 'question' => 'Number of Girls participated in hygiene promotion sessions','field' => 'scl_hp_girls'],
        //     ['parent_id' => 4, 'sl' => 14, 'question' => 'Total Menstrual hygiene  sessions','field' => 'scl_mn_ses'],
        //     ['parent_id' => 4, 'sl' => 15, 'question' => 'Number of Girls participated in Menstrual hygiene  session','field' => 'scl_mn_girls'],

        //     ['parent_id' => 4, 'sl' => 16, 'question' => 'Number of Drama played','field' => 'scl_dr'],
        //     ['parent_id' => 4, 'sl' => 17, 'question' => 'Number of students watched drama','field' => 'scl_dr_std'],
        //     ['parent_id' => 4, 'sl' => 18, 'question' => 'Number of Video shows','field' => 'scl_vd'],
        //     ['parent_id' => 4, 'sl' => 19, 'question' => 'Number of students watched video shows','field' => 'scl_vd_std'],

        // // ['category' => 'Household Sanitation Information'],
        //     ['parent_id' => 5, 'sl' => 1, 'question' => 'Number of new HH latrine built','field' => 'HHS_new'],
        //     ['parent_id' => 5, 'sl' => 2, 'question' => 'Number of new HH latrine built by hardcore','field' => 'HHS_new_hc'],
        //     ['parent_id' => 5, 'sl' => 3, 'question' => 'Number of  HH latrine built improved','field' => 'HHS_rep'],
        //     ['parent_id' => 5, 'sl' => 4, 'question' => 'Number of  HH latrine built improved by hardcore','field' => 'HHS_rep_hc'],

        // // ['category' => 'Public/Institutional Sanitation Information'],
        //     ['parent_id' => 6, 'sl' => 1, 'question' => 'Number of New Sanitation schemes APPROVED','field' => 'sa_approved'],
        //     ['parent_id' => 6, 'sl' => 2, 'question' => 'Number of New Sanitation schemes COMPLETED','field' => 'sa_completed'],
        //     ['parent_id' => 6, 'sl' => 3, 'question' => 'Number of  Sanitation schemes REPAIRED','field' => 'sa_renovated'],
        //     ['parent_id' => 6, 'sl' => 4, 'question' => 'Number of Beneficiries from New or Renavated Sanitation schemes','field' => 'sa_benef'],


        // ['category' => 'Capacity Building/Training Information'],
        //    ['parent_id' => 7, 'sl' => 1, 'question' => 'Number training courses organized by HYSAWA','field' => 'cb_trg'],
        //    ['parent_id' => 7, 'sl' => 2, 'question' => 'Number of UP functionaries recieved training from HYSAWA','field' => 'cb_trg_up_total'],
        //    ['parent_id' => 7, 'sl' => 3, 'question' => 'Number of MALE UP functionaries recieved training from HYSAWA','field' => 'cb_trg_up_male'],
        //    ['parent_id' => 7, 'sl' => 4, 'question' => 'Number of FEMALE UP functionaries recieved training from HYSAWA','field' => 'cb_trg_up_female'],
        //    ['parent_id' => 7, 'sl' => 5, 'question' => 'Number of UP/PNGO staff recieved training from HYSAWA','field' => 'cb_trg_stf_total'],

        //    ['parent_id' => 7, 'sl' => 6, 'question' => 'Number of UP/PNGO MALE staff recieved training from HYSAWA','field' => 'cb_trg_stf_male'],
        //    ['parent_id' => 7, 'sl' => 7, 'question' => 'Number of UP/PNGO FEMALE staff recieved training from HYSAWA','field' => 'cb_trg_stf_female'],
        //    ['parent_id' => 7, 'sl' => 8, 'question' => 'Number of Community facilitators recieved training from UP','field' => 'cb_trg_vol_total'],
        //    ['parent_id' => 7, 'sl' => 9, 'question' => 'Number of MALE Community facilitators recieved training from UP','field' => 'cb_trg_vol_male'],
        //    ['parent_id' => 7, 'sl' => 10, 'question' => 'Number of FEMALE Community facilitators recieved training from UP','field' => 'cb_trg_vol_female'],

        // // ['category' => 'Water Supply Information'],
        //    ['parent_id' => 8, 'sl' => 1, 'question' => 'Number of Water supply schemes APPROVED','field' => 'ws_approved'],
        //    ['parent_id' => 8, 'sl' => 2, 'question' => 'Number of Water supply schemes COMPLETED','field' => 'ws_completed'],
        //    ['parent_id' => 8, 'sl' => 3, 'question' => 'Number of people benefited from Completed schemes','field' => 'ws_beneficiary'],
        //    ['parent_id' => 8, 'sl' => 4, 'question' => 'Number of HARDCORE people benefited from Completed schemes','field' => 'ws_hc_benef'],
        //    ['parent_id' => 8, 'sl' => 5, 'question' => 'Number of people have access to safe water within 50 metere or 150 ft','field' => 'ws_50'],

        //    ['parent_id' => 8, 'sl' => 6, 'question' => 'Number of CARETAKER trainined','field' => 'CT_trg'],
        //    ['parent_id' => 8, 'sl' => 7, 'question' => 'Number of MECHANICS trainined','field' => 'Pdb'],

  //  ];


    // \DB::table('mob_app_question_lists')->insert($content);

    // $user = \DB::table('users')->where('id', 1)->first();

   // $user = User::find(2);



    // $user1 = $user->replicate();
    // $user1->name = "Super Admin";
    // $user1->email = "superadmin@sslwireless.com";
    // $user1->save();
    // dd($user1);
    // $user1->save();

    //    \DB::table('users')->insert($user1);


    // $user2 = $user->replicate();
    // $user2->name = "District Admin";
    // $user2->email = "dist@sslwireless.com";
    // $user2->distid = 0;
    // $user2->upid = 0;
    // $user2->unid = 0;
    // $user2->proj_id = 2;
    // $user2->region_id = 7;
    // $user2->save();


    // $user3 = $user->replicate();
    // $user3->name = "Upazila Admin";
    // $user3->email = "up@ssl.com";
    // $user3->distid = 9;
    // $user3->upid = 18;
    // $user3->unid = 0;
    // $user3->proj_id = 2;
    // $user3->region_id = 1;
    // $user3->save();

    // $user4 = $user->replicate();
    // $user4->name = "Union Admin";
    // $user4->email = "un@ssl.com";
    // $user4->distid = 6;
    // $user4->upid = 12;
    // $user4->unid = 555;
    // $user4->proj_id = 3;
    // $user4->region_id = 2;
    // $user4->save();

    // $user5 = $user->replicate();
    // $user5->name = "Water Agency Admin";
    // $user5->email = "wa@ssl.com";
    // $user5->distid = 9;
    // $user5->upid = 18;
    // $user5->unid = 0;
    // $user5->proj_id = 2;
    // $user5->region_id = 2;
    // $user5->save();

    // $user6 = $user->replicate();
    // $user6->name = "TA Admin";
    // $user6->email = "trg@ssl.com";
    // $user6->distid = 0;
    // $user6->upid = 0;
    // $user6->unid = 0;
    // $user6->proj_id = 2;
    // $user6->region_id = 7;
    // $user6->save();

    // \DB::table('project_user')->insert([ 'user_id' => 1, 'project_id' => 1]);
    // \DB::table('project_user')->insert([ 'user_id' => 1, 'project_id' => 2]);

    // $user = User::find(1);

    // dd($user->projects());


    // $data = \DB::table('mob_app_question_lists')->where('category', '!=', '')->get();

    // //dd($data->toArray());

    // $str = "";

    // foreach($data as $cat)
    // {
    //     $data2 = \DB::table('mob_app_question_lists')->where('parent_id', $cat->id)->get();

    //     foreach($data2 as $item)
    //     {
    //         // $str .= sprintf("%43s | %70s | \n", $cat->category, $item->question);
    //         //$item->group_id = $cat->id;

    //         \DB::table('mob_app_question_lists')
    //             ->where('id', $item->id)
    //             ->update(['group_id' => $cat->id]);

    //        // $item->update();
    //     }
    // }

    // $str .= "";
    //\Log::info($str);

    //echo $str;

    // foreach( as $project)
    // {
    //     echo $project->project.'<br/>';
    // }

    //$oldUsers = \DB::connection('mysql_old')->table('users')->get();

    //dd($oldUsers->toArray());

    // foreach($oldUsers as $user)
    // {
    //     $uu = App\User::where('email', $user->userid)->get();
    //     if(count($uu))
    //     {
    //         continue;
    //     }

    //     $u = new App\User;
    //     $u->email = $user->userid;
    //     $u->password = \Hash::make($user->password);
    //     $u->supervisor = $user->supervisor;
    //     $u->type = $user->type;
    //     $u->distid = $user->distid;
    //     $u->upid = $user->upid;
    //     $u->unid      = $user->unid;
    //     $u->proj_id   = $user->proj_id;
    //     $u->region_id = $user->region_id;

    //     $u->save();
    //     echo  $user->userid."\n";
    //     // assign role
    // }

    // $roles = App\Role::all();

    // $users         = App\User::with('roles')->where('type', '!=', '')->get();

    // $un_role       = App\Role::where('name', '=', 'up_admin')->get()->first();
    // $up_role       = App\Role::where('name', '=', 'upazila_admin')->get()->first();
    // $dist_role     = App\Role::where('name', '=', 'district_admin')->get()->first();
    // $training_role = App\Role::where('name', '=', 'training_agency')->get()->first();
    // $su_admin_role = App\Role::where('name', '=', 'superadministrator')->get()->first();
    // $wqa_role      = App\Role::where('name', '=', 'water_quality_agency')->get()->first();

    // //$water_q_role  = App\Role::where('name', '=', 'water_quality_agency')->get()->first();

    // foreach($users as $user)
    // {
    //     // if($user->roles()->count())
    //     // {
    //     //     continue;
    //     // }


    //     // if($user->type == "up")
    //     // {
    //     //     $user->roles()->attach($un_role->id);
    //     // }elseif($user->type == "engg")
    //     // {
    //     //     // if($user->hasRole(['upazila_admin']))
    //     //     // {
    //     //     //     continue;
    //     //     // }
    //     //     $user->roles()->attach($up_role->id);
    //     // }elseif($user->type == "trg")
    //     // {
    //     //     // if($user->hasRole(['training_agency'])){
    //     //     //     continue;
    //     //     // }
    //     //     $user->roles()->attach($training_role->id);
    //     // }
    //     // elseif(
    //     //     $user->type == "upfinance" ||
    //     //     $user->type == "sadmin" ||
    //     //     $user->type == "hradmin" ||
    //     //     $user->type == "fadmin")
    //     // {
    //     //     $user->roles()->attach($su_admin_role->id);
    //     // }
    //     // elseif($user->type == "region"){
    //     //     // if($user->hasRole(['district_admin'])){
    //     //     //     continue;
    //     //     // }
    //     //     $user->roles()->attach($dist_role->id);
    //     // }elseif(
    //     //     $user->type == "alivewq" ||
    //     //     $user->type == "chtwq" ||
    //     //     $user->type == "gupwq" ||
    //     //     $user->type == "pmidwq" ||
    //     //     $user->type == "pmidwq")
    //     // {
    //     //     $user->roles()->attach($wqa_role->id);
    //     // }
    // }

    // $list = App\Model\MobAppDataList::all();
    // $i = 0;

    // foreach($list as $item)
    // {
    //     $answer = json_decode($item->answers, true);

    //     \Log::info($answer);

    //      $answer = json_decode($answer, true);

    //     foreach($answer as $item1)
    //     {
    //         $KEY = "";
    //         $VALUE = "";

    //         foreach($item1 as $key => $ii)
    //         {
    //             if($key == "answer")
    //             {
    //                 $VALUE = $ii;
    //             }else{
    //                 $KEY = $ii;
    //             }

    //             //\Log::info($ii. ' ------------- '. $key );

    //           //  ++$i;
    //         }
    //         //\Log::info($i);

    //         // App\Model\MobAppDataListItem::insert([
    //         //     'mobile_app_data_list_id' => $item->id,
    //         //     'question_id'             => $KEY,
    //         //     'value'                   => $VALUE
    //         // ]);

    //     }
    // }
});

